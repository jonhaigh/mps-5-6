unit DataUn;

interface

uses
  System.SysUtils, System.Classes, Data.DB, VariablesUn, Datasnap.DBClient, Dialogs;

type
  TdmDataModule = class(TDataModule)
    dsProject: TClientDataSet;
    dsProjectName: TStringField;
    dsProjectComPort: TStringField;
    dsProjectProjectID: TIntegerField;
    dsMotors: TClientDataSet;
    dsMotorsUID: TIntegerField;
    dsMotorsAddress1: TStringField;
    dsMotorsAddress2: TStringField;
    dsMotorsAddress3: TStringField;
    dsMotorsAddress4: TStringField;
    dsMotorsAddress5: TStringField;
    dsMotorsAddress6: TStringField;
    dsMotorsAddress7: TStringField;
    dsMotorsAddress8: TStringField;
    dsMotorsBlackOut: TStringField;
    dsMotorsLVPort: TStringField;
    dsProjectLast: TIntegerField;
    dsMotorsProjectName: TStringField;
    dsProjectAdrType: TIntegerField;
    dsProjectCmdType: TIntegerField;
    dsMotorsAlias: TStringField;
    dsMotorsDirection: TStringField;
    dsMotorsInhibit: TStringField;
    dsMotorsStart: TStringField;
    dsMotorsLed: TStringField;
    dsMotorsMaint: TStringField;
    dsMotorsMtaInhibit: TStringField;
    dsMotorsRevision: TStringField;
    dsMotorsPort: TStringField;
    dsMotorsLvType: TStringField;
    dsMotorsForward: TStringField;
    dsMotorsBoType: TStringField;
    dsMWCs: TClientDataSet;
    dsMWCsUID: TIntegerField;
    dsMWCsProjectName: TStringField;
    dsMWCsFirmware: TStringField;
    dsMWCsNumberOfBoundChannels: TIntegerField;
    dsMWCChannels: TClientDataSet;
    dsMWCUID: TIntegerField;
    dsMWCBound: TBooleanField;
    dsMWCChannelsDeviceID: TIntegerField;
    dsMWCChannelsProjectName: TStringField;
    dsMWCsAdapterFirmware: TStringField;
    dsMWCChannelsChannelID: TIntegerField;
    dsMWCChannelsDeviceUID: TStringField;
    dsMWCChannelsdsThreshold1: TIntegerField;
    dsMWCChannelsThreshold2: TIntegerField;
    dsMWCChannelsThreshold3: TIntegerField;
    dsMWCChannelsThreshold4: TIntegerField;
    dsMWCChannelsNtThresholdIn: TIntegerField;
    dsMWCChannelsNtThresholdOut: TIntegerField;
    dsMWCChannelsnmPosition: TIntegerField;
    dsMWCChannelsDownTimeThreshold: TIntegerField;
    dsMWCChannelsUpTimeThreshold: TIntegerField;
    dsMWCChannelsOCLoop: TIntegerField;
    dsMWCChannelsHysteresisPercent: TIntegerField;
    dsMWCChannelsMTAInhibit: TIntegerField;
    dsMWCChannelsNmThreshold: TIntegerField;
    dsMWCChannelsShadePosition: TStringField;
    dsMWCChannelsZGN: TStringField;
    dsMWCsGroup2: TStringField;
    dsMWCsGroup3: TStringField;
    dsMWCsGroup4: TStringField;
    dsMWCsGroup1: TStringField;
    dsMWCsGroup1Channel: TStringField;
    dsMWCsGroup2Channel: TStringField;
    dsMWCsGroup3Channel: TStringField;
    dsMWCsGroup4Channel: TStringField;
    dsMWCsGroupNightPos: TStringField;
    dsMWCsBaseFirmRev: TStringField;
    dsMWCsGroupNightPos2: TStringField;
    dsMWCsGroupNightPos3: TStringField;
    dsMWCsGroupNightPos4: TStringField;
    dsDaylightSensor: TClientDataSet;
    IntegerField3: TIntegerField;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    IntegerField6: TIntegerField;
    IntegerField7: TIntegerField;
    StringField3: TStringField;
    IntegerField8: TIntegerField;
    IntegerField9: TIntegerField;
    IntegerField10: TIntegerField;
    IntegerField11: TIntegerField;
    IntegerField12: TIntegerField;
    IntegerField13: TIntegerField;
    IntegerField14: TIntegerField;
    IntegerField15: TIntegerField;
    IntegerField16: TIntegerField;
    StringField4: TStringField;
    Fwd: TStringField;
    dsMWCsEnableMessages: TStringField;
    dsMWCChannelsZGN2: TStringField;
    dsProjectZGNG: TIntegerField;
    dsDaylightSensorParamID: TIntegerField;
    dsMWCChannelsParamID: TIntegerField;
    dsMWCsdataH3: TIntegerField;
    dsMotorsPortAddrFwd: TIntegerField;
    dsMotorsAutoReturn: TIntegerField;
    dsMWCChannelsNetworkSwitchUID1: TIntegerField;
    dsMWCChannelsNetworkSwitchUID2: TIntegerField;
    dsMotorsMasterUID: TIntegerField;
    dsMotorsBlackOutMode: TIntegerField;
    dsDaylightSensorMasterSwitchUID1: TIntegerField;
    dsDaylightSensorMasterSwitchUID2: TIntegerField;
    dsSwitches: TClientDataSet;
    dsSwitchesProjectName: TStringField;
    dsSwitchesUID: TIntegerField;
    dsMotorsMotorType: TIntegerField;
    dsMotorsMotorNumber: TIntegerField;
    dsMotorsHostUID: TIntegerField;
    dsMNI: TClientDataSet;
    StringField14: TStringField;
    IntegerField17: TIntegerField;
    StringField23: TStringField;
    StringField24: TStringField;
    StringField25: TStringField;
    StringField26: TStringField;
    StringField27: TStringField;
    StringField28: TStringField;
    StringField29: TStringField;
    IntegerField19: TIntegerField;
    IntegerField20: TIntegerField;
    IntegerField21: TIntegerField;
    IntegerField22: TIntegerField;
    IntegerField23: TIntegerField;
    IntegerField24: TIntegerField;
    BooleanField1: TBooleanField;
    BooleanField2: TBooleanField;
    IntegerField25: TIntegerField;
    StringField30: TStringField;
    StringField31: TStringField;
    StringField32: TStringField;
    StringField33: TStringField;
    StringField34: TStringField;
    StringField35: TStringField;
    StringField36: TStringField;
    StringField37: TStringField;
    StringField38: TStringField;
    BooleanField3: TBooleanField;
    BooleanField4: TBooleanField;
    BooleanField5: TBooleanField;
    BooleanField6: TBooleanField;
    BooleanField7: TBooleanField;
    BooleanField8: TBooleanField;
    BooleanField9: TBooleanField;
    BooleanField10: TBooleanField;
    IntegerField26: TIntegerField;
    IntegerField27: TIntegerField;
    IntegerField28: TIntegerField;
    IntegerField29: TIntegerField;
    IntegerField30: TIntegerField;
    IntegerField31: TIntegerField;
    BooleanField11: TBooleanField;
    BooleanField12: TBooleanField;
    StringField39: TStringField;
    IntegerField32: TIntegerField;
    BooleanField13: TBooleanField;
    BooleanField14: TBooleanField;
    IntegerField33: TIntegerField;
    IntegerField34: TIntegerField;
    dsSwitchesSwName: TStringField;
    dsSwitchesDeviceID: TStringField;
    dsSwitchesButtonNames: TStringField;
    dsSwitchesButtonActions: TStringField;
    dsSwitchesButtonGroups: TStringField;
    dsSwitchesSwButtonData: TStringField;
    dsSwitchesPreSelect: TBooleanField;
    dsMotorsMotorPerson: TStringField;
    dsMWCsAlias: TStringField;
    dsMNI2: TClientDataSet;
    dsMNI2ProjectName: TStringField;
    dsMNI2UID: TIntegerField;
    dsMNI2BaseRev: TStringField;
    dsMNI2FirmwareRev: TStringField;
    dsMNI2Alias: TStringField;
    dsMNI2SwPort1FwdAddr: TStringField;
    dsMNI2SwPort2FwdAddr: TStringField;
    dsMNI2SwPort3FwdAddr: TStringField;
    dsMNI2SwPort4FwdAddr: TStringField;
    dsMNI2Sw1BtnPerson: TStringField;
    dsMNI2Sw2BtnPerson: TStringField;
    dsMNI2Sw3BtnPerson: TStringField;
    dsMNI2Sw4BtnPerson: TStringField;
    dsMNI2SwPort1Enabled: TBooleanField;
    dsMNI2SwPort2Enabled: TBooleanField;
    dsMNI2SwPort3Enabled: TBooleanField;
    dsMNI2SwPort4Enabled: TBooleanField;
    dsMNI2SwPort1FWDEnabled: TBooleanField;
    dsMNI2SwPort2FWDEnabled: TBooleanField;
    dsMNI2SwPort3FWDEnabled: TBooleanField;
    dsMNI2SwPort4FWDEnabled: TBooleanField;
    dsMNI2SwPort1Mode: TIntegerField;
    dsMNI2SwPort2Mode: TIntegerField;
    dsMNI2SwPort3Mode: TIntegerField;
    dsMNI2SwPort4Mode: TIntegerField;
    dsMNI2LEDsw1: TBooleanField;
    dsMNI2LEDsw2: TBooleanField;
    dsMNI2LEDsw3: TBooleanField;
    dsMNI2LEDsw4: TBooleanField;
    dsIQMLC2: TClientDataSet;
    StringField1: TStringField;
    IntegerField1: TIntegerField;
    StringField2: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    StringField12: TStringField;
    StringField13: TStringField;
    StringField15: TStringField;
    StringField16: TStringField;
    StringField17: TStringField;
    BooleanField17: TBooleanField;
    BooleanField18: TBooleanField;
    BooleanField19: TBooleanField;
    BooleanField20: TBooleanField;
    BooleanField21: TBooleanField;
    BooleanField22: TBooleanField;
    BooleanField23: TBooleanField;
    BooleanField24: TBooleanField;
    dsMNI2StopAlignEnabled: TBooleanField;
    dsMNI2StopAlignTravel: TStringField;
    dsMotorsEVBAngularAdjustmentTime: TIntegerField;
    dsIQMLC2AutoPortEnabled: TBooleanField;
    dsIQMLC2AutoPortFwdEnabled: TBooleanField;
    dsIQMLC2AutoPortZGN: TStringField;
    dsIQMLC2MasterPortEnabled: TBooleanField;
    dsIQMLC2MasterPortFwd: TBooleanField;
    dsIQMLC2AutoPortPersonality: TStringField;
    dsIQMLC2MasterPortPersonality: TStringField;
    dsIQMLC2MasterPortZGN: TStringField;
    dsIQMLC2SwLEDEnabled: TBooleanField;
    dsIQMLC2SwLED15MinutesEnabled: TBooleanField;
    dsIQMLC2SwModeAll: TStringField;
    dsIQMLC2StopMode: TStringField;
    dsIQMLC2MTAInhibit: TStringField;
    dsIQMLC2HardwareRev: TStringField;
    dsPowerPanel: TClientDataSet;
    StringField7: TStringField;
    IntegerField2: TIntegerField;
    StringField8: TStringField;
    StringField9: TStringField;
    StringField10: TStringField;
    StringField11: TStringField;
    StringField18: TStringField;
    StringField19: TStringField;
    StringField20: TStringField;
    StringField21: TStringField;
    StringField22: TStringField;
    StringField40: TStringField;
    StringField41: TStringField;
    BooleanField15: TBooleanField;
    BooleanField16: TBooleanField;
    BooleanField25: TBooleanField;
    BooleanField26: TBooleanField;
    BooleanField27: TBooleanField;
    BooleanField28: TBooleanField;
    BooleanField29: TBooleanField;
    BooleanField30: TBooleanField;
    IntegerField18: TIntegerField;
    IntegerField35: TIntegerField;
    IntegerField36: TIntegerField;
    IntegerField37: TIntegerField;
    BooleanField31: TBooleanField;
    BooleanField32: TBooleanField;
    BooleanField33: TBooleanField;
    BooleanField34: TBooleanField;
    BooleanField35: TBooleanField;
    StringField42: TStringField;
    dsMotorsDownSpeed: TIntegerField;
    dsMotorsUpSpeed: TIntegerField;
    dsMotorsRampDownTime: TIntegerField;
    dsMotorsRampUpTime: TIntegerField;
    dsPowerPanelSerialBaud: TStringField;
    dsPowerPanelSerialPerson: TIntegerField;
    dsPowerPanelSerialDisable: TStringField;
    dsMotorsRfMode: TStringField;
    dsMotorsRfpresent: TIntegerField;
    dsIQ3Channels: TClientDataSet;
    IntegerField38: TIntegerField;
    StringField43: TStringField;
    IntegerField39: TIntegerField;
    BooleanField36: TBooleanField;
    IntegerField40: TIntegerField;
    StringField44: TStringField;
    IntegerField41: TIntegerField;
    IntegerField42: TIntegerField;
    IntegerField43: TIntegerField;
    IntegerField44: TIntegerField;
    IntegerField45: TIntegerField;
    StringField45: TStringField;
    IntegerField46: TIntegerField;
    IntegerField47: TIntegerField;
    IntegerField48: TIntegerField;
    IntegerField49: TIntegerField;
    IntegerField50: TIntegerField;
    IntegerField51: TIntegerField;
    IntegerField52: TIntegerField;
    IntegerField53: TIntegerField;
    StringField46: TStringField;
    StringField47: TStringField;
    IntegerField54: TIntegerField;
    IntegerField55: TIntegerField;
    IntegerField56: TIntegerField;
    dsMotorsFirmwareRev: TStringField;
    dsIQ3s: TClientDataSet;
    IntegerField57: TIntegerField;
    StringField48: TStringField;
    StringField49: TStringField;
    IntegerField58: TIntegerField;
    StringField50: TStringField;
    StringField51: TStringField;
    StringField52: TStringField;
    StringField53: TStringField;
    StringField54: TStringField;
    StringField55: TStringField;
    StringField56: TStringField;
    StringField57: TStringField;
    StringField58: TStringField;
    StringField59: TStringField;
    StringField60: TStringField;
    StringField61: TStringField;
    StringField62: TStringField;
    StringField63: TStringField;
    StringField64: TStringField;
    StringField65: TStringField;
    IntegerField59: TIntegerField;
    StringField66: TStringField;
    dsMotorsInhibitTimer: TStringField;
    procedure DataModuleCreate(Sender: TObject);
    Function  DuplicateUIDCheckMDC(UID,HostID:String):boolean;
    Function  DuplicateUIDCheck(UID:String):boolean;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmDataModule: TdmDataModule;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

Function TdmDataModule.DuplicateUIDCheckMDC(UID,HostID:String):boolean;
begin
    dsMotors.Filtered := False;
    dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + UID + ' and not(HostUID = '+HostID+')';
    dsMotors.Filtered := True;
    //dsMotors.RecordCount
    if not(dsMotors.EOF)then
    begin
      DuplicateUIDCheckMDC:=True;
      exit;
    end;
    dsMWCs.Filtered := False;
    dsMWCs.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + UID;
    dsMWCs.Filtered := True;
    if not(dsMWCs.EOF)then
    begin
      DuplicateUIDCheckMDC:=True;
      exit;
    end;
    dsMNI.Filtered := False;
    dsMNI.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + UID;
    dsMNI.Filtered := True;
    if not(dsMNI.EOF)then
    begin
      DuplicateUIDCheckMDC:=True;
      exit;
    end;
    dsMNI2.Filtered := False;
    dsMNI2.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + UID;
    dsMNI2.Filtered := True;
    if not(dsMNI2.EOF)then
    begin
      DuplicateUIDCheckMDC:=True;
      exit;
    end;
    dsIQMLC2.Filtered := False;
    dsIQMLC2.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + UID;
    dsIQMLC2.Filtered := True;
    if not(dsIQMLC2.EOF)then
    begin
      DuplicateUIDCheckMDC:=True;
      exit;
    end;
    dsPowerPanel.Filtered := False;
    dsPowerPanel.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + UID;
    dsPowerPanel.Filtered := True;
    if not(dsPowerPanel.EOF)then
    begin
      DuplicateUIDCheckMDC:=True;
      exit;
    end;
    DuplicateUIDCheckMDC:=False;
end;

Function TdmDataModule.DuplicateUIDCheck(UID:String):boolean;      //For every device besides MDC motors
begin
    dsMotors.Filtered := False;
    dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + UID;
    dsMotors.Filtered := True;
    if not(dsMotors.EOF)then
    begin
      DuplicateUIDCheck:=True;
      exit;
    end;
    dsMWCs.Filtered := False;
    dsMWCs.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + UID;
    dsMWCs.Filtered := True;
    if not(dsMWCs.EOF)then
    begin
      DuplicateUIDCheck:=True;
      exit;
    end;
    dsMNI.Filtered := False;
    dsMNI.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + UID;
    dsMNI.Filtered := True;
    if not(dsMNI.EOF)then
    begin
      DuplicateUIDCheck:=True;
      exit;
    end;
    dsMNI2.Filtered := False;
    dsMNI2.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + UID;
    dsMNI2.Filtered := True;
    if not(dsMNI2.EOF)then
    begin
      DuplicateUIDCheck:=True;
      exit;
    end;
    dsIQMLC2.Filtered := False;
    dsIQMLC2.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + UID;
    dsIQMLC2.Filtered := True;
    if not(dsIQMLC2.EOF)then
    begin
      DuplicateUIDCheck:=True;
      exit;
    end;
    dsPowerPanel.Filtered := False;
    dsPowerPanel.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + UID;
    dsPowerPanel.Filtered := True;
    if not(dsPowerPanel.EOF)then
    begin
      DuplicateUIDCheck:=True;
      exit;
    end;
    DuplicateUIDCheck:=False;
end;

procedure TdmDataModule.DataModuleCreate(Sender: TObject);
begin
  try
    if not DirectoryExists(VariablesUn.gsDataPath) then
      ForceDirectories(VariablesUn.gsDataPath);

      if FileExists(VariablesUn.gsDataPath + 'dsProject.txt') then
      begin
        dsProject.LoadFromFile(VariablesUn.gsDataPath + 'dsProject.txt');
        //dsProject.Active := True;
      end;

      if FileExists(VariablesUn.gsDataPath + 'dsMotors.txt') then
      begin
        dsMotors.LoadFromFile(VariablesUn.gsDataPath + 'dsMotors.txt');
      end
      else
      begin
        dsMotors.CreateDataSet;
        dsMotors.Active := True;
      end;

      {JH}
      if FileExists(VariablesUn.gsDataPath + 'dsMWCs.txt') then
      begin
        dsMWCs.LoadFromFile(VariablesUn.gsDataPath + 'dsMWCs.txt');
      end
      else
      begin
        dsMWCs.CreateDataSet;
        dsMWCs.Active := True;
      end;
      {JH}
      if FileExists(VariablesUn.gsDataPath + 'dsIQ3s.txt') then
      begin
        dsIQ3s.LoadFromFile(VariablesUn.gsDataPath + 'dsIQ3s.txt');
      end
      else
      begin
        dsIQ3s.CreateDataSet;
        dsIQ3s.Active := True;
      end;
      {JH}
      if FileExists(VariablesUn.gsDataPath + 'dsMWCChannels.txt') then
      begin
        dsMWCChannels.LoadFromFile(VariablesUn.gsDataPath + 'dsMWCChannels.txt');
      end
      else
      begin
        dsMWCChannels.CreateDataSet;
        dsMWCChannels.Active := True;
      end;
      {JH}
      if FileExists(VariablesUn.gsDataPath + 'dsIQ3Channels.txt') then
      begin
        dsIQ3Channels.LoadFromFile(VariablesUn.gsDataPath + 'dsIQ3Channels.txt');
      end
      else
      begin
        dsIQ3Channels.CreateDataSet;
        dsIQ3Channels.Active := True;
      end;
       {JH}
      if FileExists(VariablesUn.gsDataPath + 'dsMNI2.txt') then
      begin
        dsMNI2.LoadFromFile(VariablesUn.gsDataPath + 'dsMNI2.txt');
      end
      else
      begin
        dsMNI2.CreateDataSet;
        dsMNI2.Active := True;
      end;

      if FileExists(VariablesUn.gsDataPath + 'dsMNI.txt') then
      begin
        dsMNI.LoadFromFile(VariablesUn.gsDataPath + 'dsMNI.txt');
      end
      else
      begin
        dsMNI.CreateDataSet;
        dsMNI.Active := True;
      end;

      if FileExists(VariablesUn.gsDataPath + 'dsIQMLC2.txt') then
      begin
        dsIQMLC2.LoadFromFile(VariablesUn.gsDataPath + 'dsIQMLC2.txt');
      end
      else
      begin
        dsIQMLC2.CreateDataSet;
        dsIQMLC2.Active := True;
      end;

      if FileExists(VariablesUn.gsDataPath + 'dsPowerPanel.txt') then
      begin
        dsPowerPanel.LoadFromFile(VariablesUn.gsDataPath + 'dsPowerPanel.txt');
      end
      else
      begin
        dsPowerPanel.CreateDataSet;
        dsPowerPanel.Active := True;
      end;
  except
    on E: Exception do
    begin
      MessageDlg('TDataModule1.DataModuleCreate: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  End;
end;

end.
