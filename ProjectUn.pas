unit ProjectUn;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, ComUn, VariablesUn, DiscoverUn,
  Vcl.Buttons, DataUn, Datasnap.DBClient, Data.DB, AdSelCom, NewProjectUn;

type
  TProjectFm = class(TForm)
    cbProject: TComboBox;
    Image1: TImage;
    Panel1: TPanel;
    Shape4: TShape;
    Shape3: TShape;
    Shape2: TShape;
    Shape5: TShape;
    Shape1: TShape;
    lblAlias: TLabel;
    lblProjectName: TLabel;
    btnSave: TButton;
    Label2: TLabel;
    cbComPort: TComboBox;
    SpeedButton1: TSpeedButton;
    btnCancel: TButton;
    pnlMenuBar: TPanel;
    Image3: TImage;
    btnExit: TSpeedButton;
    btnFind: TSpeedButton;
    btnMain: TSpeedButton;
    btnConfig: TSpeedButton;
    btnHelp: TSpeedButton;
    btnInfo: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure Image1MouseEnter(Sender: TObject);
    procedure pnlMenuBarMouseEnter(Sender: TObject);
    procedure pnlMenuBarMouseLeave(Sender: TObject);
    procedure btnMainMouseEnter(Sender: TObject);
    procedure btnConfigMouseEnter(Sender: TObject);
    procedure btnFindMouseEnter(Sender: TObject);
    procedure btnHelpClick(Sender: TObject);
    procedure btnInfoClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure btnMainClick(Sender: TObject);
    procedure btnConfigClick(Sender: TObject);
    procedure btnFindClick(Sender: TObject);
    procedure btnExitMouseEnter(Sender: TObject);
    procedure FormClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure cbProjectChange(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure cbComPortChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    gbSavePort: Boolean;
    procedure ShowProject;
    procedure SelectProject;
    procedure FindComPort;
    procedure ComPortSelect;
    procedure NewComPort;
  public
    { Public declarations }
    rh_ShowMain: TNotifyEvent;
    procedure rhPortClosed(Sender: TObject);
    procedure rhPortOpend(Sender: TObject);
  end;

var
  ProjectFm: TProjectFm;

implementation
uses AboutUn;
{$R *.dfm}

procedure TProjectFm.btnCancelClick(Sender: TObject);
begin
  btnMainClick(nil);
end;

procedure TProjectFm.btnConfigClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TProjectFm.btnConfigMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TProjectFm.btnHelpClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TProjectFm.btnInfoClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TProjectFm.FindComPort;
var
  I : Integer;
  S: string;
  begin
  try
    cbComPort.Items.Clear;
    for I := 1 to 50 do
    begin
     if IsPortAvailable(I) then
     begin
       S := Format('COM%d', [I]);
       //if ComportInUse= True then S:=S+'  [In use]';
       cbComPort.Items.Add(S);
     end;
    end;
  except
  on E: Exception do
  begin
    MessageDlg('TProjectFm.FindComPort: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;

procedure TProjectFm.FormClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TProjectFm.FormCreate(Sender: TObject);
begin
  VariablesUn.giPortIndexOld := -1;
  FindComPort;
end;

procedure TProjectFm.FormDestroy(Sender: TObject);
begin
    rh_ShowMain := nil;
end;

procedure TProjectFm.FormHide(Sender: TObject);
begin
  VariablesUn.giTop := Top;
  VariablesUn.giLeft := Left;
end;

procedure TProjectFm.FormShow(Sender: TObject);
begin
  Top := VariablesUn.giTop;
  Left := VariablesUn.giLeft;
  Screen.Cursor := crDefault;
  gbSavePort := False;
  lblProjectName.Color := clWhite;
  ShowProject;
  lblProjectName.Caption := VariablesUn.gsProjectName;
end;

procedure TProjectFm.Image1MouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TProjectFm.NewComPort;
var vsComPort: String;
    viIndex: Integer;
begin
   try
    if not (DataUn.dmDataModule.dsProject.State = dsInactive) then
    begin
      DataUn.dmDataModule.dsProject.Filtered := False;
      DataUn.dmDataModule.dsProject.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
      DataUn.dmDataModule.dsProject.Filtered := True;
      if not (dmDataModule.dsProject.Eof) then
      begin
        vsComPort := DataUn.dmDataModule.dsProject.FieldByName('ComPort').AsString;
        viIndex :=cbComPort.Items.IndexOf(vsComPort);
        cbComPort.ItemIndex := viIndex;
        cbComPortChange( ProjectFm.cbComPort);
      end;
      DataUn.dmDataModule.dsProject.Filtered := False;
    end;
    except
    on E: Exception do
    begin
      MessageDlg('TProjectFm.NewComPort: ' + E.Message, mtWarning, [mbOk], 0);
    end;
    End;
end;

procedure TProjectFm.pnlMenuBarMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TProjectFm.pnlMenuBarMouseLeave(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TProjectFm.rhPortClosed(Sender: TObject);
begin
 Shape1.Brush.Color := clGray;
 Shape2.Brush.Color := clGray;
 Shape3.Brush.Color := clGray;
 Shape4.Brush.Color := clGray;
 Shape5.Brush.Color := clGray;
end;

procedure TProjectFm.rhPortOpend(Sender: TObject);
begin
 Shape1.Brush.Color := clLime;
 Shape2.Brush.Color := clLime;
 Shape3.Brush.Color := clLime;
 Shape4.Brush.Color := clLime;
 Shape5.Brush.Color := clLime;

 if not VariablesUn.gbUSBfound then
 begin
  Shape1.Brush.Color := clYellow;
  Shape2.Brush.Color := clYellow;
  Shape3.Brush.Color := clYellow;
  Shape4.Brush.Color := clYellow;
  Shape5.Brush.Color := clYellow;
 end;
end;

procedure TProjectFm.SelectProject;
begin
  try
    if not (DataUn.dmDataModule.dsProject.State = dsInactive) then
    begin
      DataUn.dmDataModule.dsProject.Filtered := False;
      {
      DataUn.dmDataModule.dsProject.Filter := 'Last = ' + IntToStr(1);
      DataUn.dmDataModule.dsProject.Filtered := True;
      }
      DataUn.dmDataModule.dsProject.First;

      while not dmDataModule.dsProject.Eof do
      begin
        dmDataModule.dsProject.Edit;
        DataUn.dmDataModule.dsProject.FieldByName('Last').AsInteger := 0;
        DataUn.dmDataModule.dsProject.Post;
        DataUn.dmDataModule.dsProject.Next;
      end;
      dmDataModule.dsProject.MergeChangeLog;
      //DataUn.dmDataModule.dsProject.Filtered := False;
      DataUn.dmDataModule.dsProject.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
      DataUn.dmDataModule.dsProject.Filtered := True;
      if not dmDataModule.dsProject.Eof then
      begin
        dmDataModule.dsProject.Edit;
        DataUn.dmDataModule.dsProject.FieldByName('Last').AsInteger := 1;
        DataUn.dmDataModule.dsProject.Post;
        dmDataModule.dsProject.MergeChangeLog;
      end;
      DataUn.dmDataModule.dsProject.Filtered := False;
    end;
  except
  on E: Exception do
  begin
    MessageDlg('TProjectFm.SelectProject: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;

procedure TProjectFm.ShowProject;
begin
  try
   cbProject.OnChange := nil;
   cbProject.Items.Clear;
   if not (DataUn.dmDataModule.dsProject.State = dsInactive) then
   begin
    DataUn.dmDataModule.dsProject.Filtered := False;
    DataUn.dmDataModule.dsProject.First;
    while not DataUn.dmDataModule.dsProject.eof do
    begin
      if (DataUn.dmDataModule.dsProject.FieldByName('Last').AsInteger = 1) then
        cbProject.Items.Insert(0, DataUn.dmDataModule.dsProject.FieldByName('ProjectName').AsString)
      else
        cbProject.Items.Add(DataUn.dmDataModule.dsProject.FieldByName('ProjectName').AsString);

      DataUn.dmDataModule.dsProject.Next;
    end;
   end;
   if cbProject.Items.Count > 0 then
     cbProject.ItemIndex := 0;

   VariablesUn.gsProjectName := cbProject.Text;
   cbProject.OnChange := cbProjectChange;
 except
 on E: Exception do
 begin
  cbProject.OnChange := cbProjectChange;
  MessageDlg('TProjectFm.ShowProject: ' + E.Message, mtWarning, [mbOk], 0);
 end;
 End;
end;

procedure TProjectFm.btnExitClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  Application.Terminate;
end;

procedure TProjectFm.btnExitMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TProjectFm.btnFindClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  Hide;
  DiscoverFm.Show;
end;

procedure TProjectFm.btnFindMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TProjectFm.btnMainClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  ProjectFm.Hide;
  if Assigned (rh_ShowMain) then
     rh_ShowMain(Self);
end;

procedure TProjectFm.btnMainMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TProjectFm.cbComPortChange(Sender: TObject);
begin
  ComPortSelect;
end;

procedure TProjectFm.cbProjectChange(Sender: TObject);
begin
  ActiveControl := nil;
  VariablesUn.gsProjectName := cbProject.Text;
  lblProjectName.Caption := VariablesUn.gsProjectName;
  if cbProject.ItemIndex > -1 then
  begin
    SelectProject;
    NewComPort;
  end;
end;

procedure TProjectFm.ComPortSelect;
var vsPortNumber: String;
    viPos, viPort: Integer;
begin
  try
    ActiveControl := nil;
    viPos := pos('COM', cbComPort.Text);
    if viPos > 0 then
    begin
      vsPortNumber := Copy(cbComPort.Text, viPos + 3, 50);
      viPort := StrToInt(vsPortNumber);
      if IsPortAvailable(viPort) then
      begin
        ComFm.ComPort.Open := false;
        VariablesUn.Delay(200);
        ComFm.ComPort.ComNumber := viPort;
        ComFm.ComPort.Baud := 9600;

        if  not ComFm.ComPort.Open then
        begin
           ComFm.ComPort.Open := True;
           VariablesUn.Delay(100);
        end;
        gbSavePort := True;
        lblProjectName.Color := $00CCFFFF;   //$00A4FFFF;
        //ComUn.dmComModule.ComPort.ProcessCommunications;
        VariablesUn.giPortIndexOld := cbComPort.ItemIndex;
      end
      else
      begin
        cbComPort.ItemIndex := VariablesUn.giPortIndexOld;
        //MessageDlg('The selected port is in use. Please select another port.', mtWarning, [mbOk], 0);
      end;
    end;
    NewProjectFm.cbComPort.ItemIndex := cbComPort.ItemIndex;
  except
  on E: Exception do
  begin
    MessageDlg('TProjectFm.ComPortSelect: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;
end.
