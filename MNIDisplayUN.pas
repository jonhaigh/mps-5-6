unit MNIDisplayUN;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Imaging.pngimage,
  variablesUn,dataUn,Data.DB,Datasnap.DBClient,ComUn,MNIConfigUn,MWCDisplayUn,DiscoverUN;

type
  TMNIDisplayFm = class(TForm)
    pnlMenuBar: TPanel;
    btnInfoMCW: TImage;
    btnMenuMCW2: TImage;
    btnExitMCW: TImage;
    btnFind: TImage;
    Image5: TImage;
    Shape14: TShape;
    cmbProjectMNI: TComboBox;
    cmbDeviceSelector: TComboBox;
    cmbUidMNI: TComboBox;
    Label12: TLabel;
    Label11: TLabel;
    cmbMTRChannel: TComboBox;
    Image4: TImage;
    btnMenuMcw1: TImage;
    img0: TImage;
    btnStop: TImage;
    imgUp: TImage;
    imgIS1: TImage;
    imgIS2: TImage;
    imgIS3: TImage;
    imgDn: TImage;
    img10: TImage;
    img20: TImage;
    img30: TImage;
    img40: TImage;
    img50: TImage;
    img60: TImage;
    img70: TImage;
    img80: TImage;
    img90: TImage;
    img100: TImage;
    imgSetpoint1: TImage;
    imgSetpoint2: TImage;
    imgSetpoint3: TImage;
    btnAuto: TImage;
    imgSetpointSetOn: TImage;
    imgWinkUp: TImage;
    txtAlias: TEdit;
    Procedure LoadUID();
    procedure pnlMenuBarMouseEnter(Sender: TObject);
    procedure pnlMenuBarMouseLeave(Sender: TObject);
    procedure Image4Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure imgDnClick(Sender: TObject);
    procedure imgUpClick(Sender: TObject);
    procedure imgIS1Click(Sender: TObject);
    procedure imgIS2Click(Sender: TObject);
    procedure imgIS3Click(Sender: TObject);
    procedure cmbDeviceSelectorChange(Sender: TObject);
    procedure cmbProjectMNIChange(Sender: TObject);
    procedure cmbUidMNIChange(Sender: TObject);
    procedure btnMenuMcw1MouseEnter(Sender: TObject);
    procedure btnFindClick(Sender: TObject);
  private
    { Private declarations }
    giSentCom :Integer;
    giUID     :Integer;
    passFlag  :boolean;
    Procedure LoadProjects;
    Function  WaitforGenericResponseConfirmation(UID:Integer;Command:String;rh:TNotifyEvent):Boolean;
    procedure WaitforSetGenericResponse(UID:Integer;Command:String;rh:TNotifyEvent);
    procedure WaitforGenericResponse(UID:Integer;Command:String;rh:TNotifyEvent);
    Function  WaitforSetGenericResponseConfirmation(UID:Integer;Command:String;rh:TNotifyEvent):Boolean;
  public
    { Public declarations }
    rh_ShowMain:TNotifyEvent;
    Procedure rhShowMNIDisplay(Sender: TObject);
    Procedure rhMNIStatusRecieved(Sender: TObject);
    procedure rhPortOpend(Sender: TObject);
    procedure rhPortClosed(Sender: TObject);
  end;

var
  MNIDisplayFm: TMNIDisplayFm;

implementation

{$R *.dfm}

Procedure TMNIDisplayFm.LoadProjects;
var
  I:Integer;
begin


  cmbProjectMNI.Items.clear;
  //Polpulate project if it exists
  if DataUn.dmDataModule.dsProject.State <> dsInactive then
  begin
    DataUn.dmDataModule.dsProject.Filtered := False;
    DataUn.dmDataModule.dsProject.First;
    while not DataUn.dmDataModule.dsProject.eof do
    begin
      if (DataUn.dmDataModule.dsProject.FieldByName('Last').AsInteger = 1) then
         cmbProjectMNI.Items.Insert(0, DataUn.dmDataModule.dsProject.FieldByName('ProjectName').AsString)
      else
         cmbProjectMNI.Items.Add(DataUn.dmDataModule.dsProject.FieldByName('ProjectName').AsString);

      DataUn.dmDataModule.dsProject.Next;
    end;
    DataUn.dmDataModule.dsProject.Filtered := False;
    DataUn.dmDataModule.dsProject.First;
    for I := 0 to cmbProjectMNI.Items.Count-1 do
    begin
      if cmbprojectMNI.text = VariablesUn.gsProjectName then
      begin
        break;
      end
      else
      begin
        cmbprojectMNI.ItemIndex:=I;
      end;
    end;
  end;
end;

procedure TMNIDisplayFm.rhPortClosed(Sender: TObject);
begin
 Shape14.pen.Color := clRed;;

end;

procedure TMNIDisplayFm.rhPortOpend(Sender: TObject);
begin
 Shape14.pen.Color := clLime;

end;

Procedure TMNIDisplayFm.rhMNIStatusRecieved(Sender: TObject);
begin
  try
    if (giSentCom = VariablesUn.giCommand) and (giUID = VariablesUn.giUID_rx) then
    begin
      case VariablesUn.giCommand of
       13:
        begin
          PassFlag:=true;
        end;
       14:
        begin
          PassFlag:=true;
        end;
       15:
        begin
          PassFlag:=true;
        end;
       16:
        begin
          PassFlag:=true;
        end;
       18:
        begin
          PassFlag:=true;
        end;

      end;
    end;
  except
  on E: Exception do
  begin

  end;
 end;
end;

Function TMNIDisplayFm.WaitforGenericResponseConfirmation(UID:Integer;Command:String;rh:TNotifyEvent):Boolean;
var
  PauseCount:Integer;
begin
  passFlag:=false;
  PauseCount:=0;
  while not PassFlag do
  begin
      PauseCount:=PauseCount+1;
      delay(10);
      if (PauseCount=100) or (PauseCount=200) then
        ComFm.GetGenericStatus(UID,Command,rh);
      if PauseCount>300 then
      begin
        Result:=false;
        ShowMessage('Failed Command');
        Exit;
      end;
  end;
  Result:=True;
end;

Function TMNIDisplayFm.WaitforSetGenericResponseConfirmation(UID:Integer;Command:String;rh:TNotifyEvent):Boolean;
var
  PauseCount:Integer;
begin
  passFlag:=false;
  PauseCount:=0;
  while not PassFlag do
  begin
      PauseCount:=PauseCount+1;
      delay(10);
      if (PauseCount=100) or (PauseCount=200) then
        ComFm.SetGenericParam(UID,Command,rh);
      if PauseCount>300 then
      begin
        Result:=false;
        ShowMessage('Failed Command');
        Exit;
      end;
  end;
  Result:=True;
end;

procedure TMNIDisplayFm.WaitforGenericResponse(UID:Integer;Command:String;rh:TNotifyEvent);
var
  PauseCount:Integer;
begin
  passFlag:=false;
  PauseCount:=0;
  while not PassFlag do
  begin
      PauseCount:=PauseCount+1;
      delay(10);
      if (PauseCount=100) or (PauseCount=200) then
        ComFm.GetGenericStatus(UID,Command,rh);
      if PauseCount>300 then
      begin

        ShowMessage('Failed Command');
        Break;
      end;
  end;
end;

procedure TMNIDisplayFm.WaitforSetGenericResponse(UID:Integer;Command:String;rh:TNotifyEvent);
var
  PauseCount:Integer;
begin
  passFlag:=false;
  PauseCount:=0;
  while not PassFlag do
  begin
      PauseCount:=PauseCount+1;
      delay(10);
      if (PauseCount=100) or (PauseCount=200) then
        ComFm.SetGenericParam(UID,Command,rh);
      if PauseCount>300 then
      begin

        ShowMessage('Failed Command');
        Break;
      end;
  end;
end;

procedure TMNIDisplayFm.rhShowMNIDisplay(Sender: TObject);
begin
  show;
end;

Procedure TMNIDisplayFm.LoadUID();
var
  I:Integer;
begin
  cmbUidMNI.Clear;
  begin
    VariablesUn.gsChannelNum:=IntToStr(cmbMTRChannel.ItemIndex);
    if not (DataUn.dmDataModule.dsMNI.State = dsInactive) then
    begin
      DataUn.dmDataModule.dsMNI.Filtered := False;
      DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
      DataUn.dmDataModule.dsMNI.Filtered := True;
    end;
//    Setlength(viUIDs,0);
    while not dmDataModule.dsMNI.Eof do
    begin
     cmbUidMNI.Items.Add(IntToStr(DataUn.dmDataModule.dsMNI.FieldByName('UID').AsInteger));
//     SetLength(viUIDs,Length(viUIDs)+1);
//     viUIDs[Length(viUIDs)-1] := DataUn.dmDataModule.dsMNI.FieldByName('UID').AsInteger;
     dmDataModule.dsMNI.Next;
    end;
    cmbUidMNI.ItemIndex:=0;
  end;
  //cmbUidMNIChange(nil);
  //cmbSerialDevicesChange(nil);
end;



procedure TMNIDisplayFm.btnFindClick(Sender: TObject);
begin
  discoverfm.rh_ShowMNIConfig:= rhShowMNIDisplay;
  pnlMenuBar.Visible := False;
  hide;
  discoverfm.Show;
end;

procedure TMNIDisplayFm.btnMenuMcw1MouseEnter(Sender: TObject);
begin
  if btnMenuMcw1.enabled then
  Begin
    pnlMenuBar.Visible := True;
  End;
end;

procedure TMNIDisplayFm.cmbDeviceSelectorChange(Sender: TObject);
var
  I:Integer;
begin
  VariablesUn.giTop := Top;
  VariablesUn.giLeft := Left;
  if(cmbDeviceSelector.Text='MWC')then
  begin

    DataUn.dmDataModule.dsMWCChannels.Filtered := False;
    DataUn.dmDataModule.dsMWCChannels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsMWCChannels.Filtered := True;

    if not dmDataModule.dsMWCChannels.Eof then
    begin

      VariablesUn.giTop:=top;
      variablesUn.giLeft:=Left;
      Hide;
      MWCDisplayfm.show;
    end
    else ShowMessage('No MWC Devices Found');
  end
  else if(cmbDeviceSelector.Text='IQ2')then
  begin
    VariablesUn.giTop:=top;
    variablesUn.giLeft:=Left;
    if Assigned (rh_ShowMain) then
      rh_ShowMain(Self);
    Hide;
    VariablesUn.PassLoopFlag:=True;
  end;
end;


procedure TMNIDisplayFm.cmbProjectMNIChange(Sender: TObject);
begin
  DataUn.dmDataModule.dsMNI.Filtered := False;
  DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUIDMNI.Text;
  DataUn.dmDataModule.dsMNI.Filtered := True;

  if not (DataUn.dmDataModule.dsMNI.Eof) then
  begin
    DataUn.dmDataModule.dsMNI.Edit;
    DataUn.dmDataModule.dsMNI.FieldByName('Alias').AsString := txtAlias.Text;
    VariablesUn.gsAlias := txtAlias.Text;
    DataUn.dmDataModule.dsMNI.Post;
    DataUn.dmDataModule.dsMNI.MergeChangeLog;
  end;
  DataUn.dmDataModule.dsMNI.Filtered := False;
  DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
  DataUn.dmDataModule.dsMNI.Filtered := True;
  loadUID;
end;

procedure TMNIDisplayFm.cmbUidMNIChange(Sender: TObject);
begin
    DataUn.dmDataModule.dsMNI.Filtered := False;
    DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMNI.Text;
    DataUn.dmDataModule.dsMNI.Filtered := True;
    cmbMTRChannel.ItemIndex:=0;
    //Alias
    txtAlias.Text:=DataUn.dmDataModule.dsMNI.FieldByName('Alias').AsString;
end;

procedure TMNIDisplayFm.FormShow(Sender: TObject);
begin
  //cmbCmdType.ItemIndex := VariablesUn.giCmdType;
  top:=VariablesUn.gitop;
  left:=VariablesUn.giLeft;
  MNIConfigFm.rh_ShowMNIDisplay:= rhShowMNIDisplay;
  cmbDeviceSelector.ItemIndex:=cmbDeviceSelector.items.IndexOf('MNI');
  LoadUID;
  LoadProjects;
  cmbUidMNIChange(Nil);
end;

procedure TMNIDisplayFm.Image4Click(Sender: TObject);
begin
  variablesUn.giTop:=top;
  VariablesUn.giLeft:=Left;
  MNIConfigFm.Show;
  hide;
end;



procedure TMNIDisplayFm.imgDnClick(Sender: TObject);
var vsCommand: String;
    //viMotor:Integer;
begin
  try
    //SetSetpointsOff;

    if VariablesUn.IsNumber(cmbUIDMNI.Text) then
    begin
      if cmbMTRChannel.Text='1' then
      begin
        vsCommand := VariablesUn.UID_DN + '.0.0.1';
      end
      else if cmbMTRChannel.Text='2' then
      begin
        vsCommand := VariablesUn.UID_DN + '.0.0.2';
      end
      else if cmbMTRChannel.Text='3' then
      begin
        vsCommand := VariablesUn.UID_DN + '.0.0.4';
      end
      else if cmbMTRChannel.Text='4' then
      begin
        vsCommand := VariablesUn.UID_DN + '.0.0.8';
      end;
      giUID:= StrToInt(cmbUIDMNI.Text);
      giSentCom:=StrToInt(VariablesUn.UID_DN);
      ComFm.SetGenericParam(StrToInt(cmbUIDMNI.Text),vsCommand, rhMNIStatusRecieved);
      self.WaitforSetGenericResponse(StrToInt(cmbUIDMNI.Text),vsCommand, rhMNIStatusRecieved);


      VariablesUn.giShadePosition := -1;
      //giLastPos := -1;
      //GetPosition(cmbMotors.Text);
    end;
    except
    on E: Exception do
    begin
      MessageDlg('TIQMainFm.imgDn_upClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;


  end;

end;

procedure TMNIDisplayFm.imgIS1Click(Sender: TObject);
var
  vsCommand:String;
begin
  if VariablesUn.IsNumber(cmbUIDMNI.Text) then
  begin
    if cmbMTRChannel.Text='1' then
    begin
      vsCommand := VariablesUn.UID_G1 + '.0.0.1';
    end
    else if cmbMTRChannel.Text='2' then
    begin
      vsCommand := VariablesUn.UID_G1 + '.0.0.2';
    end
    else if cmbMTRChannel.Text='3' then
    begin
      vsCommand := VariablesUn.UID_G1 + '.0.0.4';
    end
    else if cmbMTRChannel.Text='4' then
    begin
      vsCommand := VariablesUn.UID_G1 + '.0.0.8';
    end;
    giUID:= StrToInt(cmbUIDMNI.Text);
    giSentCom:=StrToInt(VariablesUn.UID_G1);
    ComFm.SetGenericParam(StrToInt(cmbUIDMNI.Text),vsCommand, rhMNIStatusRecieved);
    self.WaitforSetGenericResponse(StrToInt(cmbUIDMNI.Text),vsCommand, rhMNIStatusRecieved);


    VariablesUn.giShadePosition := -1;
    //giLastPos := -1;
    //GetPosition(cmbMotors.Text);
  end;
end;

procedure TMNIDisplayFm.imgIS2Click(Sender: TObject);
var
  vsCommand:String;
begin
  if VariablesUn.IsNumber(cmbUIDMNI.Text) then
  begin
    if cmbMTRChannel.Text='1' then
    begin
      vsCommand := VariablesUn.UID_G2 + '.0.0.1';
    end
    else if cmbMTRChannel.Text='2' then
    begin
      vsCommand := VariablesUn.UID_G2 + '.0.0.2';
    end
    else if cmbMTRChannel.Text='3' then
    begin
      vsCommand := VariablesUn.UID_G2 + '.0.0.4';
    end
    else if cmbMTRChannel.Text='4' then
    begin
      vsCommand := VariablesUn.UID_G2 + '.0.0.8';
    end;
    giUID:= StrToInt(cmbUIDMNI.Text);
    giSentCom:=StrToInt(VariablesUn.UID_G2);
    ComFm.SetGenericParam(StrToInt(cmbUIDMNI.Text),vsCommand, rhMNIStatusRecieved);
    self.WaitforSetGenericResponse(StrToInt(cmbUIDMNI.Text),vsCommand, rhMNIStatusRecieved);


    VariablesUn.giShadePosition := -1;
    //giLastPos := -1;
    //GetPosition(cmbMotors.Text);
  end;
end;

procedure TMNIDisplayFm.imgIS3Click(Sender: TObject);
var
  vsCommand:String;
begin
  if VariablesUn.IsNumber(cmbUIDMNI.Text) then
  begin
    if cmbMTRChannel.Text='1' then
    begin
      vsCommand := VariablesUn.UID_G3 + '.0.0.1';
    end
    else if cmbMTRChannel.Text='2' then
    begin
      vsCommand := VariablesUn.UID_G3 + '.0.0.2';
    end
    else if cmbMTRChannel.Text='3' then
    begin
      vsCommand := VariablesUn.UID_G3 + '.0.0.4';
    end
    else if cmbMTRChannel.Text='4' then
    begin
      vsCommand := VariablesUn.UID_G3 + '.0.0.8';
    end;
    giUID:= StrToInt(cmbUIDMNI.Text);
    giSentCom:=StrToInt(VariablesUn.UID_G3);
    ComFm.SetGenericParam(StrToInt(cmbUIDMNI.Text),vsCommand, rhMNIStatusRecieved);
    self.WaitforSetGenericResponse(StrToInt(cmbUIDMNI.Text),vsCommand, rhMNIStatusRecieved);


    VariablesUn.giShadePosition := -1;
    //giLastPos := -1;
    //GetPosition(cmbMotors.Text);
  end;
end;

procedure TMNIDisplayFm.imgUpClick(Sender: TObject);
var
  vsCommand:String;
begin
  if VariablesUn.IsNumber(cmbUIDMNI.Text) then
  begin
    if cmbMTRChannel.Text='1' then
    begin
      vsCommand := VariablesUn.UID_TOP + '.0.0.1';
    end
    else if cmbMTRChannel.Text='2' then
    begin
      vsCommand := VariablesUn.UID_TOP + '.0.0.2';
    end
    else if cmbMTRChannel.Text='3' then
    begin
      vsCommand := VariablesUn.UID_TOP + '.0.0.4';
    end
    else if cmbMTRChannel.Text='4' then
    begin
      vsCommand := VariablesUn.UID_TOP + '.0.0.8';
    end;
    giUID:= StrToInt(cmbUIDMNI.Text);
    giSentCom:=StrToInt(VariablesUn.UID_TOP);
    ComFm.SetGenericParam(StrToInt(cmbUIDMNI.Text),vsCommand, rhMNIStatusRecieved);
    self.WaitforSetGenericResponse(StrToInt(cmbUIDMNI.Text),vsCommand, rhMNIStatusRecieved);


    VariablesUn.giShadePosition := -1;
    //giLastPos := -1;
    //GetPosition(cmbMotors.Text);
  end;
end;

procedure TMNIDisplayFm.pnlMenuBarMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TMNIDisplayFm.pnlMenuBarMouseLeave(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

end.
