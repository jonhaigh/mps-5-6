unit MNIConfigUn;

interface

uses
  StrUtils,Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids, Vcl.ComCtrls, Data.Bind.EngExt,
  Vcl.Bind.DBEngExt,Data.DB,Datasnap.DBClient,
  System.Bindings.Outputs, Vcl.Bind.Editors, Data.Bind.Components,VariablesUn,
  System.Generics.Collections,DiscoverUn,AboutUn,MNISwDialogUn,SwitchClassUn,NewProjectUn,
  Vcl.Imaging.jpeg, Vcl.Buttons, JvComponentBase, JvCaptionButton,PowerPanelConfigUn,
  MotorMaintenanceDisplay;

type
  TMNIConfigFm = class(TForm)
    Shape14: TShape;
    pnlMenuBar: TPanel;
    cmbProjectMNI: TComboBox;
    ScrollBox1: TScrollBox;
    Label12: TLabel;
    cmbUidMNI: TComboBox;
    pnlSerialParams: TPanel;
    Label2: TLabel;
    cmbSerialDevices: TComboBox;
    Panel3: TPanel;
    Image2: TImage;
    Label15: TLabel;
    Label16: TLabel;
    cmbSwitch: TComboBox;
    teSWFWDAddr: TEdit;
    Label17: TLabel;
    Panel4: TPanel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    cmbIRPerson: TComboBox;
    teIRFWDAddr: TEdit;
    cmbSWMode: TComboBox;
    Label21: TLabel;
    cbSWEnabled: TCheckBox;
    Label22: TLabel;
    cbSWFWDEnabled: TCheckBox;
    Panel5: TPanel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    teFirmwareRev: TEdit;
    Edit4: TEdit;
    txtAlias: TEdit;
    Label27: TLabel;
    btUpdateParams: TPanel;
    Label26: TLabel;
    cbIRPortEnabled: TCheckBox;
    Label28: TLabel;
    cbIRFWDEnabled: TCheckBox;
    cmbIRMode: TComboBox;
    Label29: TLabel;
    cmbSWPerson: TComboBox;
    Label30: TLabel;
    teUIDMNI: TEdit;
    btUpdateUID: TPanel;
    pnlSerialSettings: TPanel;
    cbSerialReceive: TCheckBox;
    cbSerialTransmit: TCheckBox;
    cbSerialMSGChkSum: TCheckBox;
    cbSerialRoutingField: TCheckBox;
    cbSerialUIDinZGN: TCheckBox;
    cmbSerialBaud: TComboBox;
    cmbSerialPerson: TComboBox;
    cmbSerialProtocol: TComboBox;
    Label10: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    label50: TLabel;
    Label8: TLabel;
    cmbDeviceID: TComboBox;
    btnAddSw: TButton;
    btnSerialRemoveSw: TButton;
    cbSerialEnabled: TCheckBox;
    Label5: TLabel;
    shpcmbSerialDevices: TShape;
    shpcmbSerialPerson: TShape;
    shpcmbSerialProtocol: TShape;
    shpcmbSerialBaud: TShape;
    shpcbSerialReceive: TShape;
    shpcbSerialTransmit: TShape;
    shpcbSerialUIDinZGN: TShape;
    shpcbSerialRoutingField: TShape;
    shpcbSerialMSGChkSum: TShape;
    shpcbIRFWDEnabled: TShape;
    shpcbIRPortEnabled: TShape;
    shpcmbIRPerson: TShape;
    shpcmbIRMode: TShape;
    shpcbSWEnabled: TShape;
    shpcbSWFWDEnabled: TShape;
    shpcmbSWPerson: TShape;
    shpcmbSWMode: TShape;
    shpcbSerialEnabled: TShape;
    sgMap: TStringGrid;
    cmbSerialMapCol1: TComboBox;
    teSerialMapGroup: TEdit;
    btnExit: TImage;
    btnSettings: TImage;
    btnInfo: TImage;
    btnFind: TImage;
    btnMenu: TImage;
    btnConfig: TImage;
    btnControl: TImage;
    JvCaptionButton1: TJvCaptionButton;
    btnLimitSetting: TImage;
    cmbDeviceSelector: TComboBox;
    procedure FormShow(Sender: TObject);
    procedure sgMapSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure cmbSerialMapCol1Change(Sender: TObject);
    procedure cmbSerialMapCol1Exit(Sender: TObject);
    procedure cmbSerialDevicesChange(Sender: TObject);
    procedure cmbDeviceSelectorChange(Sender: TObject);
    procedure btnFindClick(Sender: TObject);
    procedure btnFindMouseEnter(Sender: TObject);
    procedure btnInfoMouseEnter(Sender: TObject);
    procedure btnSettingsMouseEnter(Sender: TObject);
    procedure btnExitMouseEnter(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure btnSettingsClick(Sender: TObject);
    procedure btnInfoClick(Sender: TObject);
    procedure Image2MouseEnter(Sender: TObject);
    procedure pnlMenuBarMouseLeave(Sender: TObject);
    procedure cmbUidMNIChange(Sender: TObject);
    procedure cmbSwitchChange(Sender: TObject);
    procedure btUpdateParamsClick(Sender: TObject);
    procedure FormMouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure btUpdateUIDClick(Sender: TObject);
    procedure cmbSerialPersonChange(Sender: TObject);
    procedure btnAddSwClick(Sender: TObject);
    procedure cmbDeviceIDChange(Sender: TObject);
    procedure btnSerialSaveSwClick(Sender: TObject);
    procedure teSerialMapGroupExit(Sender: TObject);
    procedure sgMapSetEditText(Sender: TObject; ACol, ARow: Integer;
      const Value: string);
    procedure sgMapClick(Sender: TObject);
    procedure btnSerialRemoveSwClick(Sender: TObject);
    procedure teSerialMapGroupKeyPress(Sender: TObject; var Key: Char);
    procedure btUpdateParamsMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btUpdateParamsMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormHide(Sender: TObject);
    procedure cmbProjectMNIChange(Sender: TObject);
    procedure txtAliasDblClick(Sender: TObject);
    procedure cbSerialEnabledClick(Sender: TObject);
    procedure cmbSerialProtocolChange(Sender: TObject);
    procedure cmbSerialBaudChange(Sender: TObject);
    procedure cbSerialReceiveClick(Sender: TObject);
    procedure cbSerialTransmitClick(Sender: TObject);
    procedure cbSerialUIDinZGNClick(Sender: TObject);
    procedure cbSerialRoutingFieldClick(Sender: TObject);
    procedure cbSerialMSGChkSumClick(Sender: TObject);
    procedure cbIRPortEnabledClick(Sender: TObject);
    procedure cmbIRPersonChange(Sender: TObject);
    procedure cbIRFWDEnabledClick(Sender: TObject);
    procedure teIRFWDAddrChange(Sender: TObject);
    procedure cmbIRModeChange(Sender: TObject);
    procedure cbSWEnabledClick(Sender: TObject);
    procedure cmbSWPersonChange(Sender: TObject);
    procedure cbSWFWDEnabledClick(Sender: TObject);
    procedure cmbSWModeChange(Sender: TObject);
    procedure teSWFWDAddrChange(Sender: TObject);
    procedure txtAliasChange(Sender: TObject);
    procedure btnInfoMCWMouseEnter(Sender: TObject);
    procedure txtAliasEnterKey(Sender: TObject);
    procedure btnControlMouseEnter(Sender: TObject);
    procedure txtAliasKeyPress(Sender: TObject; var Key: Char);
    procedure teUIDMNIChange(Sender: TObject);
    procedure teUIDMNIExit(Sender: TObject);
    procedure JvCaptionButton1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure teUIDMNIKeyPress(Sender: TObject; var Key: Char);
    procedure btnLimitSettingClick(Sender: TObject);
    procedure btnLimitSettingMouseEnter(Sender: TObject);

Type
  TArrayOfBytes=Array[0..255] of Byte;
  private
    AdjustedLookupCount:Integer;
    giSentCom :Integer;
    giUID     :Integer;
    CurrentSwInfo : TStringlist;
    LastCol:Integer;
    LastRow:Integer;
    AllSwitches : TObjectList<TSwitchClassUn>;
    MapInBuffer:TStringList;
    MapInTemp:TStringlist;
    MapBuffer : Array of TArrayOfBytes;
    LastSerialProfile:String;
    ProperZGNFlg:boolean;
    LastUID:Integer;
    viUIDs : Array of Integer;
    timerDelayUid:Integer;
    vsOldAlias:String;
    PassFlag:boolean;
    procedure ComboBox_AutoWidth(const theComboBox: TCombobox);
    procedure DisableForm(Disable:Boolean);
    procedure SaveSwMapData(UID:Integer);
    Procedure LoadsgMap;
    Procedure LoadProjects;
    Procedure ClearGrid();//0=virtualMotors Grid 1=Mapping Grid
    Procedure LoadUID();
    procedure WaitforGenericResponse(UID:Integer;Command:String;rh:TNotifyEvent);
    Procedure ClearEdits;
    procedure FindSerialProfile;
    procedure Split(Delimiter: Char; Str: string; ListOfStrings: TStringlist);
    procedure MNIUpdateParam;
    Procedure SortStringListUIDS(UIDList:TStringList);
    Function  WaitforGenericResponseConfirmation(UID:Integer;Command:String;rh:TNotifyEvent):boolean;
    Function  SingleEditTest:boolean;
    Function  LoadAllSwitchData:Boolean;
    Function  CalcCheckSum:word;
    Function  SendMap(CheckSum:Word):Boolean;
    Function  SerialEditTest:Boolean;
    Function  SerialActionTest:Boolean;
    Function  SerialGroupTest:Boolean;
    Function  GetLookupCount:Integer;
    Function  GetDisabledMotorsIntConfig:Integer;
    Function  GetDisabledMotorsIntDS:Integer;
    Function  GetEnabledFWDMOIntDS:Integer;
    Function  GetEnabledFWDMOIntConfig:Integer;
    Function  GetPortModeIntConfig:Integer;
    Function  GetPortModeIntDS:Integer;
    Function  ConvertBaudRate:String;
    Function  SelectSwByID(ID:String):Integer;
    Function  SendMapPrep:Boolean;
  public
    rh_ShowMain: TNotifyEvent;
    rh_ShowMNIDisplay:TNotifyEvent;
    rh_ShowMNI2Config: TNotifyEvent;
    rh_showIQMLC2Config: TNotifyEvent;
    procedure rhShowMNI(Sender: TObject);
    procedure rhPortOpend(Sender: TObject);
    procedure rhPortClosed(Sender: TObject);
    procedure rhMNIStatusRecieved(sender: TObject);
  end;

var
  MNIConfigFm: TMNIConfigFm;

implementation

{$R *.dfm}

uses ComUn, DataUn, MWCDisplayUn, ScreenSetupUn, MWCConfigUn, MNI2ConfigUn,
  LimitSettingUn, IQ3RFDisplayUn;

Procedure TMNIConfigFm.SortStringListUIDS(UIDList:TStringList);
var
  I:Integer;
  First,Second:Integer;
  Swap:Boolean;
  TempStr:String;
begin
  Swap:=true;
  while Swap do
  begin
    Swap:=False;
    for I := 0 to UIDList.Count-2 do
    begin
      First:=StrToInt(UIDList[I]);
      Second:=StrToInt(UIDList[I+1]);
      if First > Second then
      begin
        TempStr:=UIDList[I];
        UIDList[I]:=UIDList[I+1];
        UIDList[I+1]:=TempStr;
        Swap:=true;
      end;
    end;
  end;
end;

Procedure TMNIConfigFm.LoadUID();
var
  I:Integer;
  comboUIDList:TStringList;
begin
  comboUIDList:=TStringList.Create;
  cmbUidMNI.Clear;
  begin
    if not (DataUn.dmDataModule.dsMNI.State = dsInactive) then
    begin
      DataUn.dmDataModule.dsMNI.Filtered := False;
      DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
      DataUn.dmDataModule.dsMNI.Filtered := True;
    end;
//    Setlength(viUIDs,0);
    while not dmDataModule.dsMNI.Eof do
    begin
     //cmbUidMNI.Items.Add(IntToStr(DataUn.dmDataModule.dsMNI.FieldByName('UID').AsInteger));
     comboUIDList.Add(IntToStr(DataUn.dmDataModule.dsMNI.FieldByName('UID').AsInteger));
     dmDataModule.dsMNI.Next;
    end;
    SortStringListUIDS(comboUIDList);
    cmbUidMNI.Items.Assign(comboUIDList);
    cmbUidMNI.ItemIndex:=0;
  end;
  cmbUidMNIChange(nil);
  cmbSerialDevicesChange(nil);
end;

procedure TMNIConfigFm.btnAddSwClick(Sender: TObject);
var
  I:Integer;
  SwitchInfoTemp:Tstringlist;
  CurrentSwitch : TSwitchClassUN;
  FilePath:String;
  myFile:Textfile;
begin

  CurrentSwInfo:=TStringlist.create();
  SwitchInfoTemp:=TStringlist.create();
  MNISwDialogFM.SwBrand:=cmbSerialDevices.Text;
  if(cmbSerialDevices.Text='Lutron QS')then
  begin
    MNISwDialogFM.AllSwitches:= AllSwitches;
    FilePath:= VariablesUn.gsDataPath+'Lutron.txt';
    AssignFile(myFile, FilePath);
    if fileexists(FilePath)then
    begin
      if MNISwDialogFM.showmodal=mrOK then
      begin
        ClearGrid();
        sgMap.enabled:=true;
        if(MNISwDialogFM.SwID<>'')then
        begin
          Split(',',MNISwDialogFM.SwitchesList[0],SwitchInfoTemp);
          //CurrentSwitch:=TSwitchClassUn.Create(SwitchInfoTemp,MNISwDialogFM.SwID);
          AllSwitches.add(TSwitchClassUn.Create(SwitchInfoTemp,MNISwDialogFM.SwID));
          AllSwitches.Last.Preselect:=MNISwDialogfm.Preselect;
          if AllSwitches.Last.Preselect then
          begin
            with cmbSerialMapCol1 do
            begin
              Clear;
              Items.Add('(Pre-Group)');
              Items.Add('(Pre-None)');
              Items.Add('(Pre-UP)');
              Items.Add('(Pre-UP Hold)');
              Items.Add('(Pre-PS1)');
              Items.Add('(Pre-PS2)');
              Items.Add('(Pre-PS3)');
              Items.Add('(Pre-Down)');
              Items.Add('(Pre-Down Hold)');
              Items.Add('(Pre-Stop)');
              Items.Add('(Pre-Ret Auto)');
            end;
          end
          else
          begin
            with cmbSerialMapCol1 do
            begin
              Clear;
              Items.Add('(UP)');
              Items.Add('(UP Hold)');
              Items.Add('(PS1)');
              Items.Add('(PS2)');
              Items.Add('(PS3)');
              Items.Add('(Down)');
              Items.Add('(Down Hold)');
              Items.Add('(Stop)');
              Items.Add('(Ret Auto)');
              Items.Add('(Custom 7B)');
            end;
          end;

          sgMap.RowCount:= AllSwitches.Last.ButtonCount+1;
          for I := 1 to AllSwitches.Last.ButtonCount do
          Begin
            sgMap.Cells[0,I]:='Button'+IntToStr(I);
          End;
          CmbDeviceID.Items.Add(AllSwitches.Last.DeviceID);
          CmbDeviceID.ItemIndex:=CmbDeviceID.Items.Count-1;
          CmbDeviceID.Visible:=true;
        end;
      end;
    end
    else
    begin
      ShowMessage('Missing Lutron.TXT. Please verify Lutron.txt and SwImages folder are present in c:\MPTdata.');
    end;
  end
  else
  begin
    ShowMessage('No Switches Found For Current Serial Port Profile');
  end;

  //MNISwDialogFM.Free;
end;
//splits any string with delimiter='.'
procedure TMNIConfigFm.Split(Delimiter: Char; Str: string; ListOfStrings: TStringlist) ;
begin
   ListOfStrings.Clear;
   ListOfStrings.Delimiter       := Delimiter;
   ListOfStrings.StrictDelimiter := True; // Requires D2006 or newer.
   ListOfStrings.DelimitedText   := Str;
end;

procedure TMNIConfigFm.teIRFWDAddrChange(Sender: TObject);
begin
  teIRFWDAddr.Brush.Color:=clLime;
  btUpdateParams.Color:=clLime;
end;


procedure TMNIConfigFm.teSerialMapGroupExit(Sender: TObject);
var
  SwIndex:Integer;
  OutPutList:Tstringlist;

begin
  SwIndex:= SelectSwByID(cmbDeviceID.text);
  OutPutList:=TStringlist.Create;
  Split('.', Trim(teSerialMapGroup.Text), OutPutList);
  ProperZGNFlg:=false;
  if(teSerialMapGroup.Text<>'')then
  begin
    if VariablesUn.IsG('.', Trim(teSerialMapGroup.Text), OutPutList)then
    begin
      ProperZGNFlg:=true;
      teSerialMapGroup.Text:='255.'+teSerialMapGroup.Text+'.255';
    end
    else if VariablesUn.IsZGN(teSerialMapGroup.Text) then
    begin
      ProperZGNFlg:=true;
    end
    else if AllSwitches[SwIndex].GetButtonActionByName('Button'+IntToStr(sgMap.Row))='(Custom 7B)' then
    begin
      ProperZGNFlg:=true;
    end
    else
    begin
      showmessage('ZGN Fail: Improper Format xx.xx.xx or xx. Max 255.');
      ProperZGNFlg:=false;
      teSerialMapGroup.SetFocus;
    end;
  end
  else
  begin
    teSerialMapGroup.Visible  := False;
  end;
  if ProperZGNFlg then
  begin
    //
    //sgMap.RowCount:= AllSwitches[SwIndex].ButtonCount;
    if(AllSwitches[SwIndex].GetButtonGroupByName('Button'+IntToStr(sgMap.Row))<>teSerialMapGroup.text)then
    begin
      AllSwitches[SwIndex].EditFlg:=true;
    end;
    if teSerialMapGroup.text<>'' then
    begin
      sgMap.Cells[sgMap.Col, sgMap.Row]  := teSerialMapGroup.text;
      AllSwitches[SwIndex].AddButtonGroup('Button'+IntToStr(sgMap.Row),teSerialMapGroup.text);
    end;
    teSerialMapGroup.text:='';
    teSerialMapGroup.Visible  := False;
    sgMap.SetFocus;
  end;
end;


procedure TMNIConfigFm.teSerialMapGroupKeyPress(Sender: TObject; var Key: Char);
begin
  if not (Key in [#8, '0'..'9',#46]) then
  begin
    // Discard the key
    if key=#$D then
    begin
      teSerialMapGroupExit(Nil);

    end;
    Key := #0;
  end;

end;

procedure TMNIConfigFm.teSWFWDAddrChange(Sender: TObject);
begin
  teSWFWDAddr.Brush.Color:=clLime;
  btUpdateParams.Color:=clLime;
end;

procedure TMNIConfigFm.teUIDMNIChange(Sender: TObject);
begin
  teUIDMNI.color:=cllime;
  btUpdateUID.Color:=clLime;
end;

procedure TMNIConfigFm.teUIDMNIExit(Sender: TObject);
begin
  if Length(teUIDMNI.Text)=5 then
  begin
    if StrToInt(teUIDMNI.Text)>65535 then
    begin
      Showmessage('Value cannot exceed 65535');
      teUIDMNI.SetFocus;
    end;
  end;
end;

procedure TMNIConfigFm.teUIDMNIKeyPress(Sender: TObject; var Key: Char);
begin
  if not (Key in [#8, '0'..'9']) then begin
    Key := #0;
  end;
end;

procedure TMNIConfigFm.txtAliasChange(Sender: TObject);
begin
  txtAlias.Brush.Color:=clLime;
  txtAlias.Refresh;
  btUpdateParams.Color:=clLime;
end;

procedure TMNIConfigFm.txtAliasDblClick(Sender: TObject);
begin

   NewProjectFm.rh_ShowLast:=rhShowMNI;
   NewProjectFm.Show;
   Hide;
end;

procedure TMNIConfigFm.txtAliasEnterKey(Sender: TObject);
var
  vsAlias,vsCommand:String;
  I:Integer;
begin
  try
    if VariablesUn.IsNumber(cmbUIDMNI.text) then
    begin
      if vsOldAlias<>trim(txtAlias.text) then
      begin

        vsOldAlias:=trim(txtAlias.text);
        giUID :=  StrToInt(cmbUIDMNI.text);
        vsAlias:=txtAlias.Text;
        vsAlias := vsAlias + StringOfChar ( Char(#32), 12 - Length(vsAlias) );
        vsCommand:=VariablesUn.UID_NAME+'.'+vsAlias;
        ComFm.SetGenericParam(giUID,vsCommand, rhMNIStatusRecieved);
        Delay(200);


        DataUn.dmDataModule.dsMNI.Filtered := False;
        DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUIDMNI.Text;
        DataUn.dmDataModule.dsMNI.Filtered := True;
        if not (DataUn.dmDataModule.dsMNI.Eof) then
        begin
          DataUn.dmDataModule.dsMNI.Edit;
          DataUn.dmDataModule.dsMNI.FieldByName('Alias').AsString := txtAlias.Text;
          DataUn.dmDataModule.dsMNI.Post;
          DataUn.dmDataModule.dsMNI.MergeChangeLog;
          DataUn.dmDataModule.dsMNI.SaveToFile(VariablesUn.gsDataPath + 'dsMNI.txt', dfXML);

        end;

        for I := 1 to 4 do
          begin
            DataUn.dmDataModule.dsMotors.Filtered := False;
            DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt+ ' and MotorNumber = '+IntToStr(I);
            DataUn.dmDataModule.dsMotors.Filtered := True;
            if not (DataUn.dmDataModule.dsMotors.Eof) then
            begin
              DataUn.dmDataModule.dsMotors.Edit;
              DataUn.dmDataModule.dsMotors.FieldByName('Alias').AsString := txtAlias.Text;
              VariablesUn.gsAlias := txtAlias.Text;
              DataUn.dmDataModule.dsMotors.Post;
              DataUn.dmDataModule.dsMotors.MergeChangeLog;
              DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);

            end;
          end;

      end;
    end
    else
    begin
      txtAlias.Text := '';
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.lblAliasChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  End;

end;


procedure TMNIConfigFm.txtAliasKeyPress(Sender: TObject; var Key: Char);
begin
//  if key = #$D then
//  begin
//    txtAliasEnterKey(self);
//  end;
end;

procedure TMNIConfigFm.btnControlMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TMNIConfigFm.btnExitClick(Sender: TObject);
begin
  DataUn.dmDataModule.dsMotors.Filtered := False;
  DataUn.dmDataModule.dsMWCChannels.Filtered := False;
  DataUn.dmDataModule.dsMWCs.Filtered := False;
  DataUn.dmDataModule.dsMNI.Filtered := False;
  DataUn.dmDataModule.dsMNI2.Filtered := False;
  DataUn.dmDataModule.dsIQMLC2.Filtered := False;
  DataUn.dmDataModule.dsPowerPanel.Filtered := False;

  DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
  DataUn.dmDataModule.dsMWCs.SaveToFile(VariablesUn.gsDataPath + 'dsMWCs.txt', dfXML);
  DataUn.dmDataModule.dsMWCChannels.SaveToFile(VariablesUn.gsDataPath + 'dsMWCChannels.txt', dfXML);
  DataUn.dmDataModule.dsMNI.SaveToFile(VariablesUn.gsDataPath + 'dsMNI.txt', dfXML);
  DataUn.dmDataModule.dsMNI2.SaveToFile(VariablesUn.gsDataPath + 'dsMNI2.txt', dfXML);
  DataUn.dmDataModule.dsIQMLC2.SaveToFile(VariablesUn.gsDataPath + 'dsIQMLC2.txt', dfXML);
  DataUn.dmDataModule.dsPowerPanel.SaveToFile(VariablesUn.gsDataPath + 'dsPowerPanel.txt', dfXML);
  SaveSwMapData(StrToInt(cmbUIDMNI.Text));
  pnlMenuBar.Visible := False;
  Application.Terminate;
end;

procedure TMNIConfigFm.rhShowMNI(Sender: TObject);
begin
  show;
end;

procedure TMNIConfigFm.btnExitMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TMNIConfigFm.btnFindClick(Sender: TObject);
begin
  VariablesUn.gitop:=top;
  VariablesUn.giLeft:=left;
  discoverfm.rh_ShowMNIConfig:= rhShowMNI;
  pnlMenuBar.Visible := False;
  Hide;
  DiscoverFm.Show;
end;

procedure TMNIConfigFm.btnFindMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TMNIConfigFm.btnInfoClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  Hide;
  AboutFm.rh_ShowForm := rhShowMNI;
  AboutFm.Show;
end;

procedure TMNIConfigFm.btnInfoMCWMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TMNIConfigFm.btnInfoMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TMNIConfigFm.btnLimitSettingClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  LimitSettingFm.rh_showmain := rhShowMNI;
  Hide;
  LimitSettingFm.Show;
end;

procedure TMNIConfigFm.btnLimitSettingMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TMNIConfigFm.btnSerialSaveSwClick(Sender: TObject);
var
  I:Integer;
begin
  for I := 1 to sgMap.RowCount-1 do
  begin
    if(sgMap.Cells[1,I]='')or(sgMap.Cells[2,I]='')then
    begin
      ShowMessage('Every Box Needs a value!');
      exit;
    end;
  end;
end;

procedure TMNIConfigFm.rhMNIStatusRecieved(sender: TObject);
var
  BinStr,DataSetField:String;
  I:Integer;
begin
  try
    if (giSentCom = VariablesUn.giCommand) and (giUID = VariablesUn.giUID_rx) then
    begin
      case VariablesUn.giCommand of
       11:
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsMNI.Filtered := False;
          DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUIDMNI.text;
          DataUn.dmDataModule.dsMNI.Filtered := True;
          DataUn.dmDataModule.dsMNI.Edit;
          for I := 0 to DataUn.dmDataModule.dsMNI.RecordCount-1 do
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('UID').AsString:=teUIDMNI.text;
          end;
          DataUn.dmDataModule.dsMNI.MergeChangeLog;
          //DataUn.dmDataModule.dsMNI.post;
          if DataUn.dmDataModule.dsSwitches.Active=true then
          begin
            DataUn.dmDataModule.dsSwitches.Filtered := False;
            DataUn.dmDataModule.dsSwitches.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUIDMNI.text;
            DataUn.dmDataModule.dsSwitches.Filtered := True;
            DataUn.dmDataModule.dsSwitches.Edit;
            for I := 0 to DataUn.dmDataModule.dsSwitches.RecordCount-1 do
            begin
              DataUn.dmDataModule.dsSwitches.FieldByName('UID').AsString:=teUIDMNI.text;
              DataUn.dmDataModule.dsSwitches.Next;
            end;
            DataUn.dmDataModule.dsSwitches.MergeChangeLog;
            DataUn.dmDataModule.dsSwitches.SaveToFile(VariablesUn.gsDataPath +'dsSwitch.txt', dfXML);
          end;
          DataUn.dmDataModule.dsMNI.SaveToFile(VariablesUn.gsDataPath +'dsMNI.txt', dfXML);
          DataUn.dmDataModule.dsMotors.Filtered := False;
          DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUIDMNI.text;
          DataUn.dmDataModule.dsMotors.Filtered := True;
          //DataUn.dmDataModule.dsMotors.Edit;

          while  not DataUn.dmDataModule.dsMotors.eof do
          begin
            DataUn.dmDataModule.dsMotors.Edit;
            DataUn.dmDataModule.dsMotors.FieldByName('UID').AsString:=teUIDMNI.text;
            DataUn.dmDataModule.dsMotors.MergeChangeLog;
            //DataUn.dmDataModule.dsMotors.Next;
          end;

          //DataUn.dmDataModule.dsMotors.MergeChangeLog;
          DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath +'dsMotors.txt', dfXML);
          teUIDMNI.color:=clWhite;
          btUpdateUID.Color:=clBtnFace;
        end;
       27:                   // Port Enabled
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsMNI.Filtered := False;
          DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(cmbProjectMNI.text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsMNI.Filtered := True;
          dmDataModule.dsMNI.Edit;
          if VariablesUn.GetBit(VariablesUn.giDataL,0)then
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SwPort1Enabled').AsBoolean := False;
          end
          else
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SwPort1Enabled').AsBoolean := true;
          end;
          if VariablesUn.GetBit(VariablesUn.giDataL,1)then
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SwPort2Enabled').AsBoolean := False;
          end
          else
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SwPort2Enabled').AsBoolean := true;
          end;
          if VariablesUn.GetBit(VariablesUn.giDataL,2)then
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SwPort3Enabled').AsBoolean := False;
          end
          else
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SwPort3Enabled').AsBoolean := true;
          end;
          if VariablesUn.GetBit(VariablesUn.giDataL,3)then
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SwPort4Enabled').AsBoolean := False;
          end
          else
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SwPort4Enabled').AsBoolean := true;
          end;
          if VariablesUn.GetBit(VariablesUn.giDataL,6)then
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('IRPortEnabled').AsBoolean := False;
          end
          else
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('IRPortEnabled').AsBoolean := true;
          end;
          if VariablesUn.GetBit(VariablesUn.giDataL,7)then
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('AuxPortEnabled').AsBoolean := False;
          end
          else
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('AuxPortEnabled').AsBoolean := true;
          end;

          DataUn.dmDataModule.dsMNI.MergeChangeLog;

        end;
       28:                   // Port FWD Enabled
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsMNI.Filtered := False;
          DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(cmbProjectMNI.text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsMNI.Filtered := True;
          dmDataModule.dsMNI.Edit;
          if VariablesUn.GetBit(VariablesUn.giDataL,0)then
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SwPort1FWDEnabled').AsBoolean := True;
          end
          else
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SwPort1FWDEnabled').AsBoolean := False;
          end;
          if VariablesUn.GetBit(VariablesUn.giDataL,1)then
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SwPort2FWDEnabled').AsBoolean := True;
          end
          else
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SwPort2FWDEnabled').AsBoolean := False;
          end;
          if VariablesUn.GetBit(VariablesUn.giDataL,2)then
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SwPort3FWDEnabled').AsBoolean := True;
          end
          else
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SwPort3FWDEnabled').AsBoolean := False;
          end;
          if VariablesUn.GetBit(VariablesUn.giDataL,3)then
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SwPort4FWDEnabled').AsBoolean := True;
          end
          else
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SwPort4FWDEnabled').AsBoolean := False;
          end;
          if VariablesUn.GetBit(VariablesUn.giDataL,6)then
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('IRPortFWDEnabled').AsBoolean := True;
          end
          else
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('IRPortFWDEnabled').AsBoolean := False;
          end;
          if VariablesUn.GetBit(VariablesUn.giDataL,7)then
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('AuxPortFWDEnabled').AsBoolean := True;
          end
          else
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('AuxPortFWDEnabled').AsBoolean := False;
          end;

          DataUn.dmDataModule.dsMNI.MergeChangeLog;

        end;
       29:                   // Button Personality
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsMNI.Filtered := False;
          DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(cmbProjectMNI.text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsMNI.Filtered := True;
          dmDataModule.dsMNI.Edit;
          if(variablesUn.giDataH=1) then
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SW1BtnPerson').AsString := IntToStr(VariablesUn.giDataL);
            DataUn.dmDataModule.dsMNI.MergeChangeLog;
          end
          else if(variablesUn.giDataH=2) then
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SW2BtnPerson').AsString := IntToStr(VariablesUn.giDataL);
            DataUn.dmDataModule.dsMNI.MergeChangeLog;
          end
          else if(variablesUn.giDataH=3) then
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SW3BtnPerson').AsString := IntToStr(VariablesUn.giDataL);
            DataUn.dmDataModule.dsMNI.MergeChangeLog;
          end
          else if(variablesUn.giDataH=4) then
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SW4BtnPerson').AsString := IntToStr(VariablesUn.giDataL);
            DataUn.dmDataModule.dsMNI.MergeChangeLog;
          end;

        end;
       36:                   // Port Mode
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsMNI.Filtered := False;
          DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(cmbProjectMNI.text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsMNI.Filtered := True;
          dmDataModule.dsMNI.Edit;
          if VariablesUn.GetBit(VariablesUn.giDataL,0)then
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SwPort1Mode').AsInteger := 1;
          end
          else
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SwPort1Mode').AsInteger := 0;
          end;
          if VariablesUn.GetBit(VariablesUn.giDataL,1)then
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SwPort2Mode').AsInteger := 1;
          end
          else
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SwPort2Mode').AsInteger := 0;
          end;
          if VariablesUn.GetBit(VariablesUn.giDataL,2)then
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SwPort3Mode').AsInteger := 1;
         end
          else
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SwPort3Mode').AsInteger := 0;
          end;
          if VariablesUn.GetBit(VariablesUn.giDataL,3)then
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SwPort4Mode').AsInteger := 1;
          end
          else
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SwPort4Mode').AsInteger := 0;
          end;
          if VariablesUn.GetBit(VariablesUn.giDataL,6)then
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('IRPortMode').AsInteger := 1;
          end
          else
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('IRPortMode').AsInteger := 0;
          end;
          if VariablesUn.GetBit(VariablesUn.giDataL,7)then
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('AuxPortMode').AsInteger := 1;
          end
          else
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('AuxPortMode').AsInteger := 0;
          end;

          DataUn.dmDataModule.dsMNI.MergeChangeLog;

        end;
       44:                   // switch 1 ZGN
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsMNI.Filtered := False;
          DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(cmbProjectMNI.text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsMNI.Filtered := True;
          dmDataModule.dsMNI.Edit;
          DataUn.dmDataModule.dsMNI.FieldByName('SwPort1FWDAddr').AsString := VariablesUn.BuildZGN();
          DataUn.dmDataModule.dsMNI.MergeChangeLog;

        end;
       45:                   // switch 2 ZGN
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsMNI.Filtered := False;
          DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(cmbProjectMNI.text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsMNI.Filtered := True;
          dmDataModule.dsMNI.Edit;
          DataUn.dmDataModule.dsMNI.FieldByName('SwPort2FWDAddr').AsString := VariablesUn.BuildZGN();
          DataUn.dmDataModule.dsMNI.MergeChangeLog;

        end;
       46:                   // switch 3 ZGN
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsMNI.Filtered := False;
          DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(cmbProjectMNI.text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsMNI.Filtered := True;
          dmDataModule.dsMNI.Edit;
          DataUn.dmDataModule.dsMNI.FieldByName('SwPort3FWDAddr').AsString := VariablesUn.BuildZGN();
          DataUn.dmDataModule.dsMNI.MergeChangeLog;

        end;
       47:                   // switch 4 ZGN
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsMNI.Filtered := False;
          DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(cmbProjectMNI.text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsMNI.Filtered := True;
          dmDataModule.dsMNI.Edit;
          DataUn.dmDataModule.dsMNI.FieldByName('SwPort4FWDAddr').AsString := VariablesUn.BuildZGN();
          DataUn.dmDataModule.dsMNI.MergeChangeLog;

        end;
       85:                   // Alias
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsMNI.Filtered := False;
          DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(cmbProjectMNI.text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsMNI.Filtered := True;
          dmDataModule.dsMNI.Edit;
          DataUn.dmDataModule.dsMNI.FieldByName('Alias').AsString := Trim(VariablesUn.gsMotorType);
          DataUn.dmDataModule.dsMNI.MergeChangeLog;

        end;
       131:                   // Host Port Personality
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsMNI.Filtered := False;
          DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(cmbProjectMNI.text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsMNI.Filtered := True;
          dmDataModule.dsMNI.Edit;
          if VariablesUn.giDataH=1 then
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('HostPortPerson').AsInteger := VariablesUn.gidataL;

          end
          else if VariablesUn.giDataH=2 then
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('HostPortBaud').AsInteger := VariablesUn.gidataL;

          end;
          DataUn.dmDataModule.dsMNI.MergeChangeLog;
        end;
       132:                   // Motor Personality
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsMNI.Filtered := False;
          DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(cmbProjectMNI.text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsMNI.Filtered := True;
          dmDataModule.dsMNI.Edit;
          if(variablesUn.giDataM=1)then
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('Motor1PortPerson').AsString := IntToStr(variablesUn.giDataL);
          end
          else if(variablesUn.giDataM=2)then
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('Motor2PortPerson').AsString := IntToStr(VariablesUn.giDataL);
          end
          else if(variablesUn.giDataM=3)then
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('Motor3PortPerson').AsString := IntToStr(VariablesUn.giDataL);
          end
          else if(variablesUn.giDataM=4)then
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('Motor4PortPerson').AsString := IntToStr(VariablesUn.giDataL);
          end;

          DataUn.dmDataModule.dsMNI.MergeChangeLog;

        end;
        134:                   // Serial Settings
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsMNI.Filtered := False;
          DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(cmbProjectMNI.text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsMNI.Filtered := True;
          dmDataModule.dsMNI.Edit;
          if(variablesUn.giDataH=2)then  //Baud Rate
          begin
              DataUn.dmDataModule.dsMNI.FieldByName('SerialBaud').AsInteger := variablesUn.giDataL;
          end
          else if(variablesUn.giDataH=7)then  //Serial Deisable
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SerialDisable').AsInteger := variablesUn.giDataL;
          end
          else if(variablesUn.giDataH=9)then  //Physical Layer
          begin
            if(VariablesUn.GetBit(variablesUn.giDataL,0))then
            begin
              DataUn.dmDataModule.dsMNI.FieldByName('SerialReceiver').AsBoolean := True;
            end;
            if(VariablesUn.GetBit(variablesUn.giDataL,1))then
            begin
              DataUn.dmDataModule.dsMNI.FieldByName('SerialTransmitter').AsBoolean := True;
            end;
            BinStr:=VariablesUn.IntToBin(variablesUn.giDataL);
            BinStr:=LeftStr(BinStr,6);
            BinStr:=RightStr(BinStr,4);
            DataUn.dmDataModule.dsMNI.FieldByName('PLPerson').AsInteger:=VariablesUn.BinToInt(BinStr);
          end
          else if(variablesUn.giDataH=10)then  //Out Going Message Structure
          begin
            if(VariablesUn.GetBit(variablesUn.giDataL,0))then
            begin
              DataUn.dmDataModule.dsMNI.FieldByName('SerialUIDInZGN').AsInteger := 1;
            end
            else
            begin
              DataUn.dmDataModule.dsMNI.FieldByName('SerialUIDInZGN').AsInteger := 0;
            end;
            if(VariablesUn.GetBit(variablesUn.giDataL,1))then
            begin
              DataUn.dmDataModule.dsMNI.FieldByName('SerialRoutingField').AsInteger := 1;
            end
            else
            begin
              DataUn.dmDataModule.dsMNI.FieldByName('SerialRoutingField').AsInteger := 0;
            end;
            if(VariablesUn.GetBit(variablesUn.giDataL,2))then
            begin
              DataUn.dmDataModule.dsMNI.FieldByName('SerialCheckSum').AsInteger := 1;
            end
            else
            begin
              DataUn.dmDataModule.dsMNI.FieldByName('SerialCheckSum').AsInteger := 0;
            end;
          end;

          DataUn.dmDataModule.dsMNI.MergeChangeLog;

        end;
       135:                  //IR Personality
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsMNI.Filtered := False;
          DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(cmbProjectMNI.text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsMNI.Filtered := True;
          dmDataModule.dsMNI.Edit;
          DataUn.dmDataModule.dsMNI.FieldByName('IRPortPerson').AsInteger := VariablesUn.giDataL;
          DataUn.dmDataModule.dsMNI.MergeChangeLog;
        end;
       137:                 //IR ZGN
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsMNI.Filtered := False;
          DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(cmbProjectMNI.text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsMNI.Filtered := True;
          dmDataModule.dsMNI.Edit;
          DataUn.dmDataModule.dsMNI.FieldByName('IRPortFwdAddr').AsString := VariablesUn.BuildZGN;
          DataUn.dmDataModule.dsMNI.MergeChangeLog;
        end;
       139:                   // Alias
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsMNI.Filtered := False;
          DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(cmbProjectMNI.text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsMNI.Filtered := True;
          dmDataModule.dsMNI.Edit;
          DataUn.dmDataModule.dsMNI.FieldByName('SystemMode').AsInteger := VariablesUn.giDataL;
          DataUn.dmDataModule.dsMNI.MergeChangeLog;

        end;
       202:                   // BaseCode Rev    SerialOutMess
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsMNI.Filtered := False;
          DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(cmbProjectMNI.text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsMNI.Filtered := True;
          dmDataModule.dsMNI.Edit;
          DataUn.dmDataModule.dsMNI.FieldByName('FirmwareRev').AsString := VariablesUn.BuildFirmwareRev;
          DataUn.dmDataModule.dsMNI.MergeChangeLog;

        end;
       207,208,209:                   // Map IN Out Functions
        begin
          PassFlag:=True;

        end;


      end;
    end
    else if giSentCom=1 then
    begin

      SetLength(viUIDs,Length(viUIDs)+1);
      viUIDs[Length(viUIDs)-1] := VariablesUn.giUID_rx;
      timerDelayUid:=timerDelayUid+1;

    end;
    //ComFm.rh_ReturnHandler := nil;
  except
  on E: Exception do
  begin
    ComFm.rh_ReturnHandler := nil;
    dmDataModule.dsMNI.Cancel;
    //imgMenu.OnMouseEnter := imgMenuMouseEnter;
    Screen.Cursor := crDefault;
    MessageDlg('TDiscoverFm.rhStatusRecieved: ' + E.Message, mtWarning, [mbOk], 0);
  end;
 end;
end;

Function TMNIConfigFm.SerialEditTest:Boolean;
var
  I,II:Integer;
  EditFlg:boolean;
begin
  EditFlg:=False;
  for I := 0 to AllSwitches.Count-1 do
  begin
    if(AllSwitches[I].EditFlg)then
    begin
      EditFlg:=true;
      //AllSwitches[I].EditFlg:=false;
    end;
  end;
  SerialEditTest:=EditFlg;
end;

Function TMNIConfigFm.SerialActionTest:Boolean;
var
  I,II:Integer;
  ActionFlg:boolean;
begin
  ActionFlg:=true;
  for I := 0 to AllSwitches.Count-1 do
  begin
    for II := 1 to AllSwitches[I].ButtonCount do
    begin
      If AllSwitches[I].GetButtonActionByName('Button'+IntToStr(II))='' then
      begin
        ActionFlg:=false;
        break;
      end;
    end;

  end;
  SerialActionTest:=ActionFlg;
end;
Function TMNIConfigFm.SerialGroupTest:Boolean;
var
  I,II:Integer;
  GroupFlg:boolean;
begin
  GroupFlg:=true;
  for I := 0 to AllSwitches.Count-1 do
  begin
    for II := 1 to AllSwitches[I].ButtonCount do
    begin
      If AllSwitches[I].GetButtonGroupByName('Button'+IntToStr(II))='' then
      begin
        GroupFlg:=false;
        break;
      end;
    end;

  end;
  SerialGroupTest:=GroupFlg;
end;


//This Function Counts how many button actions are available for all the switches. actions only include on press and on release (Up/Down)
Function TMNIConfigFm.GetLookupCount:Integer;
var
  ButtonCount,I,II,Count3,Count4:Integer;
begin
  ButtonCount:=0;
  if(cmbSerialDevices.Text='Lutron QS')then //Test for lutron
  begin
    for I := 0 to AllSwitches.Count-1 do   //Go through each switch
    begin
      for II := 1 to AllSwitches[I].ButtonCount do   //Go through each button on each switch
      begin
        if(AllSwitches[I].GetButtonUpbyName('Button'+IntToStr(II))<>'')then   //Get button up actions
        begin
          if not (AllSwitches[I].Preselect)then
          begin
            ButtonCount:=ButtonCount+1;     //if not a preselect switch add 1 for each up action
          end
          else
          begin

            if (AllSwitches[I].GetButtonActionByName('Button'+IntToStr(II))='(Pre-Group)')or(AllSwitches[I].GetButtonActionByName('Button'+IntToStr(II))='(Pre-None)') then
            begin
              ButtonCount:=ButtonCount+1;
            end
            else
            begin

              for Count4 := 1 to AllSwitches[I].ButtonCount do
              begin
                if(AllSwitches[I].GetButtonActionByName('Button'+IntToStr(Count4))='(Pre-Group)')then
                begin
                  ButtonCount:=ButtonCount+1;
                end;
              end;

            end;
//            end;

          end;
        end;
        if(AllSwitches[I].GetButtonDownbyName('Button'+IntToStr(II))<>'')then
        begin
          if not (AllSwitches[I].Preselect)then
          begin
            ButtonCount:=ButtonCount+1;
          end
          else
          begin
            if (AllSwitches[I].GetButtonActionByName('Button'+IntToStr(II))='(Pre-UP Hold)') or (AllSwitches[I].GetButtonActionByName('Button'+IntToStr(II))='(Pre-Down Hold)') then
            begin
              for Count4 := 1 to AllSwitches[I].ButtonCount do
              begin
                if(AllSwitches[I].GetButtonActionByName('Button'+IntToStr(Count4))='(Pre-Group)')then
                begin
                  ButtonCount:=ButtonCount+1;
                end;
              end;
            end
            else if (AllSwitches[I].GetButtonActionByName('Button'+IntToStr(II))='(Pre-Group)')or(AllSwitches[I].GetButtonActionByName('Button'+IntToStr(II))='(Pre-None)') then
            begin
              ButtonCount:=ButtonCount+1;
            end
            else
            begin

              for Count4 := 1 to AllSwitches[I].ButtonCount do
              begin
                if(AllSwitches[I].GetButtonActionByName('Button'+IntToStr(Count4))='(Pre-Group)')then
                begin
                  ButtonCount:=ButtonCount+1;
                end;
              end;

            end;

          end;
        end;
      end;
    end;
    GetLookupCount:=ButtonCount;
  end;
end;
//Function TMNIConfigFm.GetLookupCount:Integer;
//var
//  ButtonCount,I,II,Count3,Count4:Integer;
//begin
//  ButtonCount:=0;
//  if(cmbSerialDevices.Text='Lutron QS')then //Test for lutron
//  begin
//    for I := 0 to AllSwitches.Count-1 do   //Go through each switch
//    begin
//      for II := 1 to AllSwitches[I].ButtonCount do   //Go through each button on each switch
//      begin
//        if(AllSwitches[I].GetButtonUpbyName('Button'+IntToStr(II))<>'')then   //Get button up actions
//        begin
//          if not (AllSwitches[I].Preselect)then
//          begin
//            ButtonCount:=ButtonCount+1;     //if not a preselect switch add 1 for each up action
//          end
//          else
//          begin
//
//              if (AllSwitches[I].GetButtonActionByName('Button'+IntToStr(II))='(Pre-Group)')or(AllSwitches[I].GetButtonActionByName('Button'+IntToStr(II))='(Pre-None)') then
//              begin
//                ButtonCount:=ButtonCount+1;
//              end
//              else
//              begin
//                for Count4 := 1 to AllSwitches[I].ButtonCount do
//                begin
//                  if(AllSwitches[I].GetButtonActionByName('Button'+IntToStr(Count4))='(Pre-Group)')then
//                  begin
//                    ButtonCount:=ButtonCount+1;
//                  end;
//                end;
//              end;
////            end;
//
//          end;
//        end;
//        if(AllSwitches[I].GetButtonDownbyName('Button'+IntToStr(II))<>'')then
//        begin
//          if not (AllSwitches[I].Preselect)then
//          begin
//            ButtonCount:=ButtonCount+1;
//          end;
//        end;
//      end;
//    end;
//    GetLookupCount:=ButtonCount;
//  end;
//end;

Function TMNIConfigFm.SendMapPrep:Boolean;
var
  I,LookUpCount,BlockUpCount:Integer;
begin
  giUID:=StrToInt(cmbUidMNI.text);
  giSentCom := StrToInt(LeftStr(VariablesUn.MNI_WipeFlash,3));
  ComFm.SetGenericParam(giUID, VariablesUn.MNI_WipeFlash, rhMNIStatusRecieved);
  if(WaitforGenericResponseConfirmation(giUID, VariablesUn.MNI_WipeFlash, rhMNIStatusRecieved))then
  begin
    LookUpCount:=AdjustedLookupCount;
    if LookUpCount>512 then
    begin
      BlockUpCount:= (LookUpCount div 256);

    end
    else
    begin
      BlockUpCount:=1;
    end;
    giSentCom := StrToInt(LeftStr(VariablesUn.MNI_AllocateBlocks,3));
    ComFm.SetGenericParam(giUID, VariablesUn.MNI_AllocateBlocks+IntToStr(BlockUpCount), rhMNIStatusRecieved);
    if(WaitforGenericResponseConfirmation(giUID,  VariablesUn.MNI_AllocateBlocks+IntToStr(BlockUpCount), rhMNIStatusRecieved))then
    begin
      giSentCom := StrToInt(LeftStr(VariablesUn.MNI_EraseMap,3));
      ComFm.SetGenericParam(giUID, VariablesUn.MNI_EraseMap+IntToStr(BlockUpCount*16), rhMNIStatusRecieved);
      if(WaitforGenericResponseConfirmation(giUID,VariablesUn.MNI_EraseMap+IntToStr(BlockUpCount*16), rhMNIStatusRecieved))then
      begin
        SendMapPrep:=true;
      end
      else
      begin
        SendMapPrep:=False;
      end;
    end
    else
    begin
      SendMapPrep:=False;
    end;
  end
  else
  begin
    SendMapPrep:=False;
  end;

end;

Function  TMNIConfigFm.SendMap(CheckSum:Word):Boolean;
var
  I,EndPosition,LineIndex,LookUpItemCount,SwitchCounter:Integer;
  BlockCount,PageCount,Offset:Integer;
  multiArray : Array of Array of integer;
  MsgStrlen,MsgStr:String;
  tester:Tarrayofbytes;
begin
//  tester:=mapbuffer[6];
//  Mapbuffer[6]:=mapbuffer[7];
//  mapbuffer[7]:=tester;
  PageCount:=5;
  BlockCount:=0;
  giUID:=StrToInt(cmbUidMNI.text);  //CONTROL
  giSentCom := StrToInt(LeftStr(VariablesUn.MNI_ConfigBytes,3));
  ComFm.SetGenericParam(giUID, VariablesUn.MNI_ConfigBytes+IntToStr(Lo(CheckSum))+'.'+IntToStr(Hi(CheckSum))+'.0.0.1.'+IntToStr(Lo(AdjustedLookupCount))+'.'+IntToStr(Hi(AdjustedLookupCount))+MNI_ConfigBytesPostChkSum, rhMNIStatusRecieved);  if(WaitforGenericResponseConfirmation(giUID, VariablesUn.MNI_ConfigBytes+IntToStr(Lo(CheckSum))+'.'+IntToStr(Hi(CheckSum))+MNI_ConfigBytesPostChkSum, rhMNIStatusRecieved))then
  begin
    giSentCom := StrToInt(LeftStr(VariablesUn.MNI_ParseString,3)); //Parse
    ComFm.SetGenericParam(giUID, VariablesUn.MNI_ParseString, rhMNIStatusRecieved);
    if(WaitforGenericResponseConfirmation(giUID, VariablesUn.MNI_ParseString, rhMNIStatusRecieved))then
    begin
      for I := 0 to Length(MapBuffer)-1 do
      begin
        Offset:=0;
        if (I=251) or (I=507) or (I=763) or (I=1019) or (I=1275) or (I=1531) or (I=1787) then
        begin
          BlockCount:=BlockCount+1;
        end;
        MsgStr:='';
        for LineIndex := 0 to 23 do
        begin
          MsgStr:=MsgStr+IntToStr(MapBuffer[I][LineIndex]);
          if LineIndex<>23 then
          begin
            MsgStr:=MsgStr+'.';
          end;
        end;
        giSentCom := StrToInt(LeftStr(VariablesUn.MNI_WriteMem,3));
        ComFm.SetGenericParam(giUID, VariablesUn.MNI_WriteMem+IntToStr(BlockCount)+'.'+IntToStr(PageCount)+'.'+IntToStr(Offset)+'.24.'+MsgStr, rhMNIStatusRecieved);
        if(WaitforGenericResponseConfirmation(giUID, VariablesUn.MNI_WriteMem+IntToStr(BlockCount)+'.'+IntToStr(PageCount)+'.'+IntToStr(Offset)+'.'+MsgStr, rhMNIStatusRecieved))then
        begin
          MsgStr:='';
          Offset:=54;

          MsgStrlen:=IntToStr((MapBuffer[I][24])+1);
          for LineIndex := 24 to 50 do
          begin
//            if LineIndex<36 then
//            begin
//              MsgStr:=MsgStr+IntToStr(MapBuffer[I][LineIndex]);
//              MsgStr:=MsgStr+'.';
//            end
//            else
            if MapBuffer[I][LineIndex]<>255  then
            begin
              MsgStr:=MsgStr+IntToStr(MapBuffer[I][LineIndex]);
              MsgStr:=MsgStr+'.';
            end
            else
            begin
              delete(MsgStr, length(MsgStr), 1);
              break;
            end;
          end;

          giSentCom := StrToInt(LeftStr(VariablesUn.MNI_WriteMem,3));
          ComFm.SetGenericParam(giUID, VariablesUn.MNI_WriteMem+IntToStr(BlockCount)+'.'+IntToStr(PageCount)+'.'+IntToStr(Offset)+'.'+MsgStrlen+'.'+MsgStr, rhMNIStatusRecieved);
          if(WaitforGenericResponseConfirmation(giUID, VariablesUn.MNI_WriteMem+IntToStr(BlockCount)+'.'+IntToStr(PageCount)+'.'+IntToStr(Offset)+'.'+MsgStrlen+'.'+MsgStr, rhMNIStatusRecieved))then
          begin
            PageCount:=PageCount+1;
          end;
        end;
      end;
    end
    else
    begin
      SendMap:=False;
      Exit;
    end;
  end
  else
  begin
    SendMap:=False;
    Exit;
  end;
  SendMap:=true;
end;

Function  TMNIConfigFm.CalcCheckSum:word;
var
  ButtonLoopCount,StringIndex,ButtonIndex,SwitchIndex,Line,I,II,LookUpItemCount:Integer;
  TempCounter,Count1,Count2:Integer;
  CheckSumWord :Word;
  swAlpha:String;
  MapLineValue,SplitZGN,DeviceSide:TStringList;
  AlphabeticalTemp:TArrayOfBytes;
  CustomMSG,ButtonID,ButtonDownAction,ButtonUpAction,DeviceSideStr,DeviceID:String;
  MyChar:Char;
  Swapped:Boolean;
begin

  Line:=0;
  //LineIndex:=0;
  MapLineValue:=TStringList.Create;
  SplitZGN:=TStringList.Create;
  DeviceSide:=TStringList.Create;
  CheckSumWord:=Word(316071);
  LookUpItemCount:= GetLookupCount;
//  checksumWord:=CheckSumWord+word(GetLookupCount)+1;
  SetLength(MapBuffer, 0);
  SetLength(MapBuffer, LookUpItemCount);
  //write 255 for whole buffer
  for I := 0 to Length(MapBuffer)-1 do
  begin
    //SetLength(MapBuffer[I], 256);
    for II := 0 to 255 do
    begin
      MapBuffer[I][II]:=255;
    end;
  end;

  for SwitchIndex := 0 to AllSwitches.Count-1 do
  begin
    for ButtonIndex := 1 to AllSwitches[SwitchIndex].ButtonCount do
    begin
      ButtonLoopCount:=0;
//      if (AllSwitches[SwitchIndex].GetButtonActionByName('Button'+IntToStr(ButtonIndex))='(UP)')then
//      begin
//        While ButtonLoopCount < 2 do
//        begin
//          MapLineValue.Add ('1');
//          MapLineValue.Add ('62');
//          MapLineValue.Add('64');
//          MapLineValue.Add('0');
//          MapLineValue.Add('13');
//          Split('.',AllSwitches[SwitchIndex].GetButtonGroupByName('Button'+IntToStr(ButtonIndex)),SplitZGN);
//          MapLineValue.Add(SplitZGN[0]);
//          MapLineValue.Add(SplitZGN[1]);
//          MapLineValue.Add(SplitZGN[2]);
//          MapLineValue.Add('0');
//          MapLineValue.Add('0');
//          MapLineValue.Add('0');
//          MapLineValue.Add('0');
//          MapLineValue.Add('0');
//          MapLineValue.Add('0');
//          MapLineValue.Add('0');
//          MapLineValue.Add('0');
//          MapLineValue.Add('0');
//          MapLineValue.Add('0');
//          MapLineValue.Add('0');
//          MapLineValue.Add('0');
//          MapLineValue.Add('0');
//          MapLineValue.Add('0');
//          MapLineValue.Add('0');
//          MapLineValue.Add('0');//~DEVICE,
//          DeviceSide.Clear;
//          DeviceSide:=AllSwitches[SwitchIndex].GetswAlpha;
////          DeviceSide.Delimiter:=' ';
////          DeviceSide.DelimitedText:='D E V I C E ,';
//          DeviceID:=AllSwitches[SwitchIndex].DeviceID;
//          For StringIndex := 1 to Length(DeviceID) do
//          begin
//            DeviceSide.Add(DeviceID[StringIndex]);
//          end;
//          DeviceSide.Add(',');
//          ButtonID:=AllSwitches[SwitchIndex].GetButtonIDbyName('Button'+IntToStr(ButtonIndex));
//          for I := 1 to Length(ButtonID) do
//          begin
//            DeviceSide.Add(ButtonID[I]);
//            //DeviceSide.Add(',');
//          end;
//          DeviceSide.Add(',');
//          ButtonDownAction:= AllSwitches[SwitchIndex].GetButtonDownbyName('Button'+IntToStr(ButtonIndex));
//          ButtonUpAction:= AllSwitches[SwitchIndex].GetButtonUpbyName('Button'+IntToStr(ButtonIndex));
//          if(ButtonDownAction='') then
//          begin
//            ButtonLoopCount:=1;
//          end;
//          if ButtonLoopCount=0 then
//          begin
//             //DeviceSide.Add('3');
//            for I := 1 to Length(ButtonDownAction) do
//            begin
//              DeviceSide.Add(ButtonDownAction[I]);
//              //DeviceSide.Add(',');
//            end;
//             //DeviceSide.Add(AllSwitches[SwitchIndex].GetButtonDownbyName('Button'+IntToStr(ButtonIndex)));
//            if (ButtonUpAction='') then
//            begin
//              ButtonLoopCount:=ButtonLoopCount+2;
//            end
//            else
//            begin
//              ButtonLoopCount:=ButtonLoopCount+1;
//            end;
//          end
//          else if ButtonLoopCount=1 then
//          begin
//            //DeviceSide.Add('4');
//            for I := 1 to Length(ButtonUpAction) do
//            begin
//              DeviceSide.Add(ButtonUpAction[I]);
//              //DeviceSide.Add(',');
//            end;
//            //DeviceSide.Add(AllSwitches[SwitchIndex].GetButtonUpbyName('Button'+IntToStr(ButtonIndex)));
//            ButtonLoopCount:=ButtonLoopCount+1;
//          end;
//          MapLineValue.Add(IntTOStr(DeviceSide.Count));//LUtron Side Count
//          for I := 0 to DeviceSide.Count-1 do
//          begin
//            DeviceSideStr:=DeviceSide[I];
//            if isNUmber(DeviceSideStr) then
//            begin
//              MapLineValue.Add(intToStr(StrToInt(DeviceSideStr)+48));   //
//            end
//            else
//            begin
//              for II := 1 to length(DeviceSideStr) do
//              begin
//                MapLineValue.Add(IntToStr(ord(DeviceSideStr[II])));
//              end;
//            end;
//
//          end;
//          for I := 0 to MapLineValue.count-1 do
//          begin
//            MapBuffer[Line][I]:=StrToInt(MapLineValue[I]);
//          end;
//          MapLineValue.clear;
//          DeviceSide.Clear;
//          Line:=Line+1;
//        end;
//
//      end
      if (AllSwitches[SwitchIndex].GetButtonActionByName('Button'+IntToStr(ButtonIndex))='(UP)')then
      begin
//        While ButtonLoopCount < 2 do
//        begin
          ButtonDownAction:= AllSwitches[SwitchIndex].GetButtonDownbyName('Button'+IntToStr(ButtonIndex));
          ButtonUpAction:= AllSwitches[SwitchIndex].GetButtonUpbyName('Button'+IntToStr(ButtonIndex));
          if (ButtonDownAction<>'')and(ButtonUpAction<>'') then
          begin
            LookUpItemCount:=LookUpItemCount-1;
            SetLength(MapBuffer, LookUpItemCount);
          end;
          MapLineValue.Add ('1');
          MapLineValue.Add ('62');
          MapLineValue.Add('64');
          MapLineValue.Add('0');
          MapLineValue.Add('13');
          Split('.',AllSwitches[SwitchIndex].GetButtonGroupByName('Button'+IntToStr(ButtonIndex)),SplitZGN);
          MapLineValue.Add(SplitZGN[0]);
          MapLineValue.Add(SplitZGN[1]);
          MapLineValue.Add(SplitZGN[2]);
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');//~DEVICE,
          DeviceSide.Clear;
          DeviceSide:=AllSwitches[SwitchIndex].GetswAlpha;
//          DeviceSide.Delimiter:=' ';
//          DeviceSide.DelimitedText:='D E V I C E ,';
          DeviceID:=AllSwitches[SwitchIndex].DeviceID;
          For StringIndex := 1 to Length(DeviceID) do
          begin
            DeviceSide.Add(DeviceID[StringIndex]);
          end;
          DeviceSide.Add(',');
          ButtonID:=AllSwitches[SwitchIndex].GetButtonIDbyName('Button'+IntToStr(ButtonIndex));
          for I := 1 to Length(ButtonID) do
          begin
            DeviceSide.Add(ButtonID[I]);
            //DeviceSide.Add(',');
          end;
          DeviceSide.Add(',');
          ButtonDownAction:= AllSwitches[SwitchIndex].GetButtonDownbyName('Button'+IntToStr(ButtonIndex));
          ButtonUpAction:= AllSwitches[SwitchIndex].GetButtonUpbyName('Button'+IntToStr(ButtonIndex));
//          if(ButtonDownAction='') then
//          begin
//            ButtonLoopCount:=1;
//          end;
//          if ButtonLoopCount=0 then
//          begin
//             //DeviceSide.Add('3');
            for I := 1 to Length(ButtonDownAction) do
            begin
              DeviceSide.Add(ButtonDownAction[I]);
              //DeviceSide.Add(',');
            end;
//             //DeviceSide.Add(AllSwitches[SwitchIndex].GetButtonDownbyName('Button'+IntToStr(ButtonIndex)));
//            if (ButtonUpAction='') then
//            begin
//              ButtonLoopCount:=ButtonLoopCount+2;
//            end
//            else
//            begin
//              ButtonLoopCount:=ButtonLoopCount+1;
//            end;
//          end
//          else if ButtonLoopCount=1 then
//          begin
            //DeviceSide.Add('4');
//            for I := 1 to Length(ButtonUpAction) do
//            begin
//              DeviceSide.Add(ButtonUpAction[I]);
//              //DeviceSide.Add(',');
//            end;
            //DeviceSide.Add(AllSwitches[SwitchIndex].GetButtonUpbyName('Button'+IntToStr(ButtonIndex)));
//            ButtonLoopCount:=ButtonLoopCount+1;
//          end;
          MapLineValue.Add(IntTOStr(DeviceSide.Count));//LUtron Side Count
          for I := 0 to DeviceSide.Count-1 do
          begin
            DeviceSideStr:=DeviceSide[I];
            if isNUmber(DeviceSideStr) then
            begin
              MapLineValue.Add(intToStr(StrToInt(DeviceSideStr)+48));   //
            end
            else
            begin
              for II := 1 to length(DeviceSideStr) do
              begin
                MapLineValue.Add(IntToStr(ord(DeviceSideStr[II])));
              end;
            end;

          end;
          for I := 0 to MapLineValue.count-1 do
          begin
            MapBuffer[Line][I]:=StrToInt(MapLineValue[I]);
          end;
          MapLineValue.clear;
          DeviceSide.Clear;
          Line:=Line+1;
//        end;

      end
      else if (AllSwitches[SwitchIndex].GetButtonActionByName('Button'+IntToStr(ButtonIndex))='(Custom 7B)')then
      begin
        While ButtonLoopCount < 2 do
        begin
          MapLineValue.Add ('1');
          MapLineValue.Add ('62');
          Split('.',AllSwitches[SwitchIndex].GetButtonGroupByName('Button'+IntToStr(ButtonIndex)),SplitZGN);
          for count2 := 0 to splitZGN.Count-1 do
          begin
            MapLineValue.Add(splitZGN[count2]);
          end;
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');//~DEVICE,
          DeviceSide.Clear;
          DeviceSide:=AllSwitches[SwitchIndex].GetswAlpha;
//          DeviceSide.Delimiter:=' ';
//          DeviceSide.DelimitedText:='D E V I C E ,';
          DeviceID:=AllSwitches[SwitchIndex].DeviceID;
          For StringIndex := 1 to Length(DeviceID) do
          begin
            DeviceSide.Add(DeviceID[StringIndex]);
          end;
          DeviceSide.Add(',');
          ButtonID:=AllSwitches[SwitchIndex].GetButtonIDbyName('Button'+IntToStr(ButtonIndex));
          for I := 1 to Length(ButtonID) do
          begin
            DeviceSide.Add(ButtonID[I]);
            //DeviceSide.Add(',');
          end;
          DeviceSide.Add(',');

          ButtonDownAction:= AllSwitches[SwitchIndex].GetButtonDownbyName('Button'+IntToStr(ButtonIndex));
          ButtonUpAction:= AllSwitches[SwitchIndex].GetButtonUpbyName('Button'+IntToStr(ButtonIndex));
          if(ButtonDownAction='') then
          begin
            ButtonLoopCount:=1;
          end;
          if ButtonLoopCount=0 then
          begin
            for I := 1 to Length(ButtonDownAction) do
            begin
              DeviceSide.Add(ButtonDownAction[I]);
            end;
            if (ButtonUpAction='') then
            begin
              ButtonLoopCount:=ButtonLoopCount+2;
            end
            else
            begin
              ButtonLoopCount:=ButtonLoopCount+1;
            end;
          end
          else if ButtonLoopCount=1 then
          begin
            for I := 1 to Length(ButtonUpAction) do
            begin
              DeviceSide.Add(ButtonUpAction[I]);
            end;
            ButtonLoopCount:=ButtonLoopCount+1;
          end;
          MapLineValue.Add(IntTOStr(DeviceSide.Count));//LUtron Side Count
          for I := 0 to DeviceSide.Count-1 do
          begin
            DeviceSideStr:=DeviceSide[I];
            if isNUmber(DeviceSideStr) then
            begin
              MapLineValue.Add(intToStr(StrToInt(DeviceSideStr)+48));
            end
            else
            begin
              for II := 1 to length(DeviceSideStr) do
              begin
                MapLineValue.Add(IntToStr(ord(DeviceSideStr[II])));
              end;
            end;

          end;
          for I := 0 to MapLineValue.count-1 do
          begin
            MapBuffer[Line][I]:=StrToInt(MapLineValue[I]);
          end;
          MapLineValue.clear;
          DeviceSide.Clear;
          Line:=Line+1;
        end;

      end
      else if AllSwitches[SwitchIndex].GetButtonActionByName('Button'+IntToStr(ButtonIndex))='(UP Hold)' then
      begin
        MapLineValue.Add ('1');
        MapLineValue.Add ('62');
        MapLineValue.Add('64');
        MapLineValue.Add('0');
        MapLineValue.Add('13');
        Split('.',AllSwitches[SwitchIndex].GetButtonGroupByName('Button'+IntToStr(ButtonIndex)),SplitZGN);
        MapLineValue.Add(SplitZGN[0]);
        MapLineValue.Add(SplitZGN[1]);
        MapLineValue.Add(SplitZGN[2]);
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');//~DEVICE,
        DeviceSide.Clear;
        DeviceSide:=AllSwitches[SwitchIndex].GetswAlpha;
//          DeviceSide.Delimiter:=' ';
//          DeviceSide.DelimitedText:='D E V I C E ,';
        DeviceID:=AllSwitches[SwitchIndex].DeviceID;
        For StringIndex := 1 to Length(DeviceID) do
        begin
          DeviceSide.Add(DeviceID[StringIndex]);
        end;
        DeviceSide.Add(',');
        ButtonID:=AllSwitches[SwitchIndex].GetButtonIDbyName('Button'+IntToStr(ButtonIndex));
        for I := 1 to Length(ButtonID) do
        begin
          DeviceSide.Add(ButtonID[I]);

        end;
        DeviceSide.Add(',');
        ButtonDownAction:= AllSwitches[SwitchIndex].GetButtonDownbyName('Button'+IntToStr(ButtonIndex));
        for I := 1 to Length(ButtonDownAction) do
        begin
          DeviceSide.Add(ButtonDownAction[I]);
        end;
        //DeviceSide.Add('3');
        MapLineValue.Add(IntTOStr(DeviceSide.Count));//LUtron Side Count
        for I := 0 to DeviceSide.Count-1 do
        begin
          DeviceSideStr:=DeviceSide[I];
          if isNUmber(DeviceSideStr) then
          begin
            MapLineValue.Add(intToStr(StrToInt(DeviceSideStr)+48));
          end
          else
          begin
            for II := 1 to length(DeviceSideStr) do
            begin
              MapLineValue.Add(IntToStr(ord(DeviceSideStr[II])));
            end;
          end;

        end;
        for I := 0 to MapLineValue.count-1 do
        begin
          MapBuffer[Line][I]:=StrToInt(MapLineValue[I]);
        end;
        MapLineValue.clear;
        DeviceSide.Clear;
        Line:=Line+1;
        MapLineValue.Add ('1');
        MapLineValue.Add ('62');
        MapLineValue.Add('64');
        MapLineValue.Add('0');
        MapLineValue.Add('34');
        Split('.',AllSwitches[SwitchIndex].GetButtonGroupByName('Button'+IntToStr(ButtonIndex)),SplitZGN);
        MapLineValue.Add(SplitZGN[0]);
        MapLineValue.Add(SplitZGN[1]);
        MapLineValue.Add(SplitZGN[2]);
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');//~DEVICE,
        DeviceSide.Clear;
        DeviceSide:=AllSwitches[SwitchIndex].GetswAlpha;
//          DeviceSide.Delimiter:=' ';
//          DeviceSide.DelimitedText:='D E V I C E ,';
        DeviceID:=AllSwitches[SwitchIndex].DeviceID;
        For StringIndex := 1 to Length(DeviceID) do
        begin
          DeviceSide.Add(DeviceID[StringIndex]);
        end;
        DeviceSide.Add(',');
        ButtonID:=AllSwitches[SwitchIndex].GetButtonIDbyName('Button'+IntToStr(ButtonIndex));
        for I := 1 to Length(ButtonID) do
        begin
          DeviceSide.Add(ButtonID[I]);

        end;
        DeviceSide.Add(',');
        ButtonUpAction:= AllSwitches[SwitchIndex].GetButtonUpbyName('Button'+IntToStr(ButtonIndex));
        for I := 1 to Length(ButtonUpAction) do
        begin
          DeviceSide.Add(ButtonUpAction[I]);
        end;
        //DeviceSide.Add('4');
        MapLineValue.Add(IntTOStr(DeviceSide.Count));//LUtron Side Count
        for I := 0 to DeviceSide.Count-1 do
        begin
          DeviceSideStr:=DeviceSide[I];
          if isNUmber(DeviceSideStr) then
          begin
            MapLineValue.Add(intToStr(StrToInt(DeviceSideStr)+48));
          end
          else
          begin
            for II := 1 to length(DeviceSideStr) do
            begin
              MapLineValue.Add(IntToStr(ord(DeviceSideStr[II])));
            end;
          end;

        end;
        for I := 0 to MapLineValue.count-1 do
        begin
          MapBuffer[Line][I]:=StrToInt(MapLineValue[I]);
        end;
        MapLineValue.clear;
        DeviceSide.Clear;
        Line:=Line+1;
      end
      else if (AllSwitches[SwitchIndex].GetButtonActionByName('Button'+IntToStr(ButtonIndex))='(PS1)')then
      begin
//        While ButtonLoopCount < 2 do
//        begin
          ButtonDownAction:= AllSwitches[SwitchIndex].GetButtonDownbyName('Button'+IntToStr(ButtonIndex));
          ButtonUpAction:= AllSwitches[SwitchIndex].GetButtonUpbyName('Button'+IntToStr(ButtonIndex));
          if (ButtonDownAction<>'')and(ButtonUpAction<>'') then
          begin
            LookUpItemCount:=LookUpItemCount-1;
            SetLength(MapBuffer, LookUpItemCount);
          end;
          MapLineValue.Add ('1');
          MapLineValue.Add ('62');
          MapLineValue.Add('64');
          MapLineValue.Add('0');
          MapLineValue.Add('14');
          Split('.',AllSwitches[SwitchIndex].GetButtonGroupByName('Button'+IntToStr(ButtonIndex)),SplitZGN);
          MapLineValue.Add(SplitZGN[0]);
          MapLineValue.Add(SplitZGN[1]);
          MapLineValue.Add(SplitZGN[2]);
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');//~DEVICE,
          DeviceSide.Clear;
          DeviceSide:=AllSwitches[SwitchIndex].GetswAlpha;
//          DeviceSide.Delimiter:=' ';
//          DeviceSide.DelimitedText:='D E V I C E ,';
          DeviceID:=AllSwitches[SwitchIndex].DeviceID;
          For StringIndex := 1 to Length(DeviceID) do
          begin
            DeviceSide.Add(DeviceID[StringIndex]);
          end;
          DeviceSide.Add(',');
          ButtonID:=AllSwitches[SwitchIndex].GetButtonIDbyName('Button'+IntToStr(ButtonIndex));
          for I := 1 to Length(ButtonID) do
          begin
            DeviceSide.Add(ButtonID[I]);

          end;
          DeviceSide.Add(',');
          ButtonDownAction:= AllSwitches[SwitchIndex].GetButtonDownbyName('Button'+IntToStr(ButtonIndex));
          ButtonUpAction:= AllSwitches[SwitchIndex].GetButtonUpbyName('Button'+IntToStr(ButtonIndex));
//          if(ButtonDownAction='') then
//          begin
//            ButtonLoopCount:=1;
//          end;
//          if ButtonLoopCount=0 then
//          begin
            for I := 1 to Length(ButtonDownAction) do
            begin
              DeviceSide.Add(ButtonDownAction[I]);
            end;
//            if (ButtonUpAction='') then
//            begin
//              ButtonLoopCount:=ButtonLoopCount+2;
//            end
//            else
//            begin
//              ButtonLoopCount:=ButtonLoopCount+1;
//            end;
//          end
//          else if ButtonLoopCount=1 then
//          begin
//            for I := 1 to Length(ButtonUpAction) do
//            begin
//              DeviceSide.Add(ButtonUpAction[I]);
//            end;
//            ButtonLoopCount:=ButtonLoopCount+1;
//          end;
          MapLineValue.Add(IntTOStr(DeviceSide.Count));//LUtron Side Count
          for I := 0 to DeviceSide.Count-1 do
          begin
            DeviceSideStr:=DeviceSide[I];
            if isNUmber(DeviceSideStr) then
            begin
              MapLineValue.Add(intToStr(StrToInt(DeviceSideStr)+48));
            end
            else
            begin
              for II := 1 to length(DeviceSideStr) do
              begin
                MapLineValue.Add(IntToStr(ord(DeviceSideStr[II])));
              end;
            end;

          end;
          for I := 0 to MapLineValue.count-1 do
          begin
            MapBuffer[Line][I]:=StrToInt(MapLineValue[I]);
          end;
          MapLineValue.clear;
          DeviceSide.Clear;
          Line:=Line+1;
//        end;

      end
      else if (AllSwitches[SwitchIndex].GetButtonActionByName('Button'+IntToStr(ButtonIndex))='(PS2)')then
      begin
//        While ButtonLoopCount < 2 do
//        begin
          ButtonDownAction:= AllSwitches[SwitchIndex].GetButtonDownbyName('Button'+IntToStr(ButtonIndex));
          ButtonUpAction:= AllSwitches[SwitchIndex].GetButtonUpbyName('Button'+IntToStr(ButtonIndex));
          if (ButtonDownAction<>'')and(ButtonUpAction<>'') then
          begin
            LookUpItemCount:=LookUpItemCount-1;
            SetLength(MapBuffer, LookUpItemCount);
          end;
          MapLineValue.Add ('1');
          MapLineValue.Add ('62');
          MapLineValue.Add('64');
          MapLineValue.Add('0');
          MapLineValue.Add('15');
          Split('.',AllSwitches[SwitchIndex].GetButtonGroupByName('Button'+IntToStr(ButtonIndex)),SplitZGN);
          MapLineValue.Add(SplitZGN[0]);
          MapLineValue.Add(SplitZGN[1]);
          MapLineValue.Add(SplitZGN[2]);
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');//~DEVICE,
          DeviceSide.Clear;
          DeviceSide:=AllSwitches[SwitchIndex].GetswAlpha;
//          DeviceSide.Delimiter:=' ';
//          DeviceSide.DelimitedText:='D E V I C E ,';
          DeviceID:=AllSwitches[SwitchIndex].DeviceID;
          For StringIndex := 1 to Length(DeviceID) do
          begin
            DeviceSide.Add(DeviceID[StringIndex]);
          end;
          DeviceSide.Add(',');
          ButtonID:=AllSwitches[SwitchIndex].GetButtonIDbyName('Button'+IntToStr(ButtonIndex));
          for I := 1 to Length(ButtonID) do
          begin
            DeviceSide.Add(ButtonID[I]);

          end;
          DeviceSide.Add(',');
          ButtonDownAction:= AllSwitches[SwitchIndex].GetButtonDownbyName('Button'+IntToStr(ButtonIndex));
          ButtonUpAction:= AllSwitches[SwitchIndex].GetButtonUpbyName('Button'+IntToStr(ButtonIndex));
//          if(ButtonDownAction='') then
//          begin
//            ButtonLoopCount:=1;
//          end;
//          if ButtonLoopCount=0 then
//          begin
            for I := 1 to Length(ButtonDownAction) do
            begin
              DeviceSide.Add(ButtonDownAction[I]);
            end;
//            if (ButtonUpAction='') then
//            begin
//              ButtonLoopCount:=ButtonLoopCount+2;
//            end
//            else
//            begin
//              ButtonLoopCount:=ButtonLoopCount+1;
//            end;
//          end
//          else if ButtonLoopCount=1 then
//          begin
//            for I := 1 to Length(ButtonUpAction) do
//            begin
//              DeviceSide.Add(ButtonUpAction[I]);
//            end;
//            ButtonLoopCount:=ButtonLoopCount+1;
//          end;
          MapLineValue.Add(IntTOStr(DeviceSide.Count));//LUtron Side Count
          for I := 0 to DeviceSide.Count-1 do
          begin
            DeviceSideStr:=DeviceSide[I];
            if isNUmber(DeviceSideStr) then
            begin
              MapLineValue.Add(intToStr(StrToInt(DeviceSideStr)+48));
            end
            else
            begin
              for II := 1 to length(DeviceSideStr) do
              begin
                MapLineValue.Add(IntToStr(ord(DeviceSideStr[II])));
              end;
            end;

          end;
          for I := 0 to MapLineValue.count-1 do
          begin
            MapBuffer[Line][I]:=StrToInt(MapLineValue[I]);
          end;
          MapLineValue.clear;
          DeviceSide.Clear;
          Line:=Line+1;
//        end;

      end
      else if (AllSwitches[SwitchIndex].GetButtonActionByName('Button'+IntToStr(ButtonIndex))='(PS3)')then
      begin
//        While ButtonLoopCount < 2 do
//        begin
          ButtonDownAction:= AllSwitches[SwitchIndex].GetButtonDownbyName('Button'+IntToStr(ButtonIndex));
          ButtonUpAction:= AllSwitches[SwitchIndex].GetButtonUpbyName('Button'+IntToStr(ButtonIndex));
          if (ButtonDownAction<>'')and(ButtonUpAction<>'') then
          begin
            LookUpItemCount:=LookUpItemCount-1;
            SetLength(MapBuffer, LookUpItemCount);
          end;
          MapLineValue.Add ('1');
          MapLineValue.Add ('62');
          MapLineValue.Add('64');
          MapLineValue.Add('0');
          MapLineValue.Add('16');
          Split('.',AllSwitches[SwitchIndex].GetButtonGroupByName('Button'+IntToStr(ButtonIndex)),SplitZGN);
          MapLineValue.Add(SplitZGN[0]);
          MapLineValue.Add(SplitZGN[1]);
          MapLineValue.Add(SplitZGN[2]);
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');//~DEVICE,
          DeviceSide.Clear;
          DeviceSide:=AllSwitches[SwitchIndex].GetswAlpha;
//          DeviceSide.Delimiter:=' ';
//          DeviceSide.DelimitedText:='D E V I C E ,';
          DeviceID:=AllSwitches[SwitchIndex].DeviceID;
          For StringIndex := 1 to Length(DeviceID) do
          begin
            DeviceSide.Add(DeviceID[StringIndex]);
          end;
          DeviceSide.Add(',');
          ButtonID:=AllSwitches[SwitchIndex].GetButtonIDbyName('Button'+IntToStr(ButtonIndex));
          for I := 1 to Length(ButtonID) do
          begin
            DeviceSide.Add(ButtonID[I]);

          end;
          DeviceSide.Add(',');
          ButtonDownAction:= AllSwitches[SwitchIndex].GetButtonDownbyName('Button'+IntToStr(ButtonIndex));
          ButtonUpAction:= AllSwitches[SwitchIndex].GetButtonUpbyName('Button'+IntToStr(ButtonIndex));
//          if(ButtonDownAction='') then
//          begin
//            ButtonLoopCount:=1;
//          end;
//          if ButtonLoopCount=0 then
//          begin
            for I := 1 to Length(ButtonDownAction) do
            begin
              DeviceSide.Add(ButtonDownAction[I]);
            end;
//            if (ButtonUpAction='') then
//            begin
//              ButtonLoopCount:=ButtonLoopCount+2;
//            end
//            else
//            begin
//              ButtonLoopCount:=ButtonLoopCount+1;
//            end;
//          end
//          else if ButtonLoopCount=1 then
//          begin
//            for I := 1 to Length(ButtonUpAction) do
//            begin
//              DeviceSide.Add(ButtonUpAction[I]);
//            end;
//            ButtonLoopCount:=ButtonLoopCount+1;
//          end;
          MapLineValue.Add(IntTOStr(DeviceSide.Count));//LUtron Side Count
          for I := 0 to DeviceSide.Count-1 do
          begin
            DeviceSideStr:=DeviceSide[I];
            if isNUmber(DeviceSideStr) then
            begin
              MapLineValue.Add(intToStr(StrToInt(DeviceSideStr)+48));
            end
            else
            begin
              for II := 1 to length(DeviceSideStr) do
              begin
                MapLineValue.Add(IntToStr(ord(DeviceSideStr[II])));
              end;
            end;

          end;
          for I := 0 to MapLineValue.count-1 do
          begin
            MapBuffer[Line][I]:=StrToInt(MapLineValue[I]);
          end;
          MapLineValue.clear;
          DeviceSide.Clear;
          Line:=Line+1;
//        end;

      end
      else if (AllSwitches[SwitchIndex].GetButtonActionByName('Button'+IntToStr(ButtonIndex))='(Down)')then
      begin
//        While ButtonLoopCount < 2 do
//        begin
          ButtonDownAction:= AllSwitches[SwitchIndex].GetButtonDownbyName('Button'+IntToStr(ButtonIndex));
          ButtonUpAction:= AllSwitches[SwitchIndex].GetButtonUpbyName('Button'+IntToStr(ButtonIndex));
          if (ButtonDownAction<>'')and(ButtonUpAction<>'') then
          begin
            LookUpItemCount:=LookUpItemCount-1;
            SetLength(MapBuffer, LookUpItemCount);
          end;
          MapLineValue.Add ('1');
          MapLineValue.Add ('62');
          MapLineValue.Add('64');
          MapLineValue.Add('0');
          MapLineValue.Add('18');
          Split('.',AllSwitches[SwitchIndex].GetButtonGroupByName('Button'+IntToStr(ButtonIndex)),SplitZGN);
          MapLineValue.Add(SplitZGN[0]);
          MapLineValue.Add(SplitZGN[1]);
          MapLineValue.Add(SplitZGN[2]);
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');//~DEVICE,
          DeviceSide.Clear;
          DeviceSide:=AllSwitches[SwitchIndex].GetswAlpha;
//          DeviceSide.Delimiter:=' ';
//          DeviceSide.DelimitedText:='D E V I C E ,';
          DeviceID:=AllSwitches[SwitchIndex].DeviceID;
          For StringIndex := 1 to Length(DeviceID) do
          begin
            DeviceSide.Add(DeviceID[StringIndex]);
          end;
          DeviceSide.Add(',');
          ButtonID:=AllSwitches[SwitchIndex].GetButtonIDbyName('Button'+IntToStr(ButtonIndex));
          for I := 1 to Length(ButtonID) do
          begin
            DeviceSide.Add(ButtonID[I]);

          end;
          DeviceSide.Add(',');
          ButtonDownAction:= AllSwitches[SwitchIndex].GetButtonDownbyName('Button'+IntToStr(ButtonIndex));
          ButtonUpAction:= AllSwitches[SwitchIndex].GetButtonUpbyName('Button'+IntToStr(ButtonIndex));
//          if(ButtonDownAction='') then
//          begin
//            ButtonLoopCount:=1;
//          end;
//          if ButtonLoopCount=0 then
//          begin
            for I := 1 to Length(ButtonDownAction) do
            begin
              DeviceSide.Add(ButtonDownAction[I]);
            end;
//            if (ButtonUpAction='') then
//            begin
//              ButtonLoopCount:=ButtonLoopCount+2;
//            end
//            else
//            begin
//              ButtonLoopCount:=ButtonLoopCount+1;
//            end;
//          end
//          else if ButtonLoopCount=1 then
//          begin
//            for I := 1 to Length(ButtonUpAction) do
//            begin
//              DeviceSide.Add(ButtonUpAction[I]);
//            end;
//            ButtonLoopCount:=ButtonLoopCount+1;
//          end;
          MapLineValue.Add(IntTOStr(DeviceSide.Count));//LUtron Side Count
          for I := 0 to DeviceSide.Count-1 do
          begin
            DeviceSideStr:=DeviceSide[I];
            if isNUmber(DeviceSideStr) then
            begin
              MapLineValue.Add(intToStr(StrToInt(DeviceSideStr)+48));
            end
            else
            begin
              for II := 1 to length(DeviceSideStr) do
              begin
                MapLineValue.Add(IntToStr(ord(DeviceSideStr[II])));
              end;
            end;

          end;
          for I := 0 to MapLineValue.count-1 do
          begin
            MapBuffer[Line][I]:=StrToInt(MapLineValue[I]);
          end;
          MapLineValue.clear;
          DeviceSide.Clear;
          Line:=Line+1;
//        end;

      end
      else if AllSwitches[SwitchIndex].GetButtonActionByName('Button'+IntToStr(ButtonIndex))='(Down Hold)' then
      begin
        MapLineValue.Add ('1');
        MapLineValue.Add ('62');
        MapLineValue.Add('64');
        MapLineValue.Add('0');
        MapLineValue.Add('18');
        Split('.',AllSwitches[SwitchIndex].GetButtonGroupByName('Button'+IntToStr(ButtonIndex)),SplitZGN);
        MapLineValue.Add(SplitZGN[0]);
        MapLineValue.Add(SplitZGN[1]);
        MapLineValue.Add(SplitZGN[2]);
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');//~DEVICE,
        DeviceSide.Clear;
        DeviceSide:=AllSwitches[SwitchIndex].GetswAlpha;
//          DeviceSide.Delimiter:=' ';
//          DeviceSide.DelimitedText:='D E V I C E ,';
        DeviceID:=AllSwitches[SwitchIndex].DeviceID;
        For StringIndex := 1 to Length(DeviceID) do
        begin
          DeviceSide.Add(DeviceID[StringIndex]);
        end;
        DeviceSide.Add(',');
        ButtonID:=AllSwitches[SwitchIndex].GetButtonIDbyName('Button'+IntToStr(ButtonIndex));
        for I := 1 to Length(ButtonID) do
        begin
          DeviceSide.Add(ButtonID[I]);

        end;
        DeviceSide.Add(',');
        ButtonDownAction:= AllSwitches[SwitchIndex].GetButtonDownbyName('Button'+IntToStr(ButtonIndex));
        for I := 1 to Length(ButtonDownAction) do
        begin
          DeviceSide.Add(ButtonDownAction[I]);
        end;
        //DeviceSide.Add('3');
        MapLineValue.Add(IntTOStr(DeviceSide.Count));//LUtron Side Count
        for I := 0 to DeviceSide.Count-1 do
        begin
          DeviceSideStr:=DeviceSide[I];
          if isNUmber(DeviceSideStr) then
          begin
            MapLineValue.Add(intToStr(StrToInt(DeviceSideStr)+48));
          end
          else
          begin
            for II := 1 to length(DeviceSideStr) do
            begin
              MapLineValue.Add(IntToStr(ord(DeviceSideStr[II])));
            end;
          end;

        end;
        for I := 0 to MapLineValue.count-1 do
        begin
          MapBuffer[Line][I]:=StrToInt(MapLineValue[I]);
        end;
        MapLineValue.clear;
        DeviceSide.Clear;
        Line:=Line+1;

        MapLineValue.Add ('1');
        MapLineValue.Add ('62');
        MapLineValue.Add('64');
        MapLineValue.Add('0');
        MapLineValue.Add('34');
        Split('.',AllSwitches[SwitchIndex].GetButtonGroupByName('Button'+IntToStr(ButtonIndex)),SplitZGN);
        MapLineValue.Add(SplitZGN[0]);
        MapLineValue.Add(SplitZGN[1]);
        MapLineValue.Add(SplitZGN[2]);
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');//~DEVICE,
        DeviceSide.Clear;
        DeviceSide:=AllSwitches[SwitchIndex].GetswAlpha;
//          DeviceSide.Delimiter:=' ';
//          DeviceSide.DelimitedText:='D E V I C E ,';
        DeviceID:=AllSwitches[SwitchIndex].DeviceID;
        For StringIndex := 1 to Length(DeviceID) do
        begin
          DeviceSide.Add(DeviceID[StringIndex]);
        end;
        DeviceSide.Add(',');
        ButtonID:=AllSwitches[SwitchIndex].GetButtonIDbyName('Button'+IntToStr(ButtonIndex));
        for I := 1 to Length(ButtonID) do
        begin
          DeviceSide.Add(ButtonID[I]);

        end;
        DeviceSide.Add(',');
        ButtonUpAction:= AllSwitches[SwitchIndex].GetButtonUpbyName('Button'+IntToStr(ButtonIndex));
        for I := 1 to Length(ButtonUpAction) do
        begin
          DeviceSide.Add(ButtonUpAction[I]);
        end;
        //DeviceSide.Add('4');
        MapLineValue.Add(IntTOStr(DeviceSide.Count));//Lutron Side Count
        for I := 0 to DeviceSide.Count-1 do
        begin
          DeviceSideStr:=DeviceSide[I];
          if isNUmber(DeviceSideStr) then
          begin
            MapLineValue.Add(intToStr(StrToInt(DeviceSideStr)+48));
          end
          else
          begin
            for II := 1 to length(DeviceSideStr) do
            begin
              MapLineValue.Add(IntToStr(ord(DeviceSideStr[II])));
            end;
          end;

        end;
        for I := 0 to MapLineValue.count-1 do
        begin
          MapBuffer[Line][I]:=StrToInt(MapLineValue[I]);
        end;
        MapLineValue.clear;
        DeviceSide.Clear;
        Line:=Line+1;
      end
      else if AllSwitches[SwitchIndex].GetButtonActionByName('Button'+IntToStr(ButtonIndex))='(Stop)' then
      begin
//        While ButtonLoopCount < 2 do
//        begin
          ButtonDownAction:= AllSwitches[SwitchIndex].GetButtonDownbyName('Button'+IntToStr(ButtonIndex));
          ButtonUpAction:= AllSwitches[SwitchIndex].GetButtonUpbyName('Button'+IntToStr(ButtonIndex));
          if (ButtonDownAction<>'')and(ButtonUpAction<>'') then
          begin
            LookUpItemCount:=LookUpItemCount-1;
            SetLength(MapBuffer, LookUpItemCount);
          end;
          MapLineValue.Add ('1');
          MapLineValue.Add ('62');
          MapLineValue.Add('64');
          MapLineValue.Add('0');
          MapLineValue.Add('34');
          Split('.',AllSwitches[SwitchIndex].GetButtonGroupByName('Button'+IntToStr(ButtonIndex)),SplitZGN);
          MapLineValue.Add(SplitZGN[0]);
          MapLineValue.Add(SplitZGN[1]);
          MapLineValue.Add(SplitZGN[2]);
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');//~DEVICE,
          DeviceSide.Clear;
          DeviceSide:=AllSwitches[SwitchIndex].GetswAlpha;
//          DeviceSide.Delimiter:=' ';
//          DeviceSide.DelimitedText:='D E V I C E ,';
          DeviceID:=AllSwitches[SwitchIndex].DeviceID;
          For StringIndex := 1 to Length(DeviceID) do
          begin
            DeviceSide.Add(DeviceID[StringIndex]);
          end;
          DeviceSide.Add(',');
          ButtonID:=AllSwitches[SwitchIndex].GetButtonIDbyName('Button'+IntToStr(ButtonIndex));
          for I := 1 to Length(ButtonID) do
          begin
            DeviceSide.Add(ButtonID[I]);

          end;
          DeviceSide.Add(',');
          ButtonDownAction:= AllSwitches[SwitchIndex].GetButtonDownbyName('Button'+IntToStr(ButtonIndex));
          ButtonUpAction:= AllSwitches[SwitchIndex].GetButtonUpbyName('Button'+IntToStr(ButtonIndex));
//          if(ButtonDownAction='') then
//          begin
//            ButtonLoopCount:=1;
//          end;
//          if ButtonLoopCount=0 then
//          begin
            for I := 1 to Length(ButtonDownAction) do
            begin
              DeviceSide.Add(ButtonDownAction[I]);
            end;
//            if (ButtonUpAction='') then
//            begin
//              ButtonLoopCount:=ButtonLoopCount+2;
//            end
//            else
//            begin
//              ButtonLoopCount:=ButtonLoopCount+1;
//            end;
//          end
//          else if ButtonLoopCount=1 then
//          begin
//            for I := 1 to Length(ButtonUpAction) do
//            begin
//              DeviceSide.Add(ButtonUpAction[I]);
//            end;
//            ButtonLoopCount:=ButtonLoopCount+1;
//          end;
          MapLineValue.Add(IntTOStr(DeviceSide.Count));//LUtron Side Count
          for I := 0 to DeviceSide.Count-1 do
          begin
            DeviceSideStr:=DeviceSide[I];
            if isNUmber(DeviceSideStr) then
            begin
              MapLineValue.Add(intToStr(StrToInt(DeviceSideStr)+48));
            end
            else
            begin
              for II := 1 to length(DeviceSideStr) do
              begin
                MapLineValue.Add(IntToStr(ord(DeviceSideStr[II])));
              end;
            end;

          end;
          for I := 0 to MapLineValue.count-1 do
          begin
            MapBuffer[Line][I]:=StrToInt(MapLineValue[I]);
          end;
          MapLineValue.clear;
          DeviceSide.Clear;
          Line:=Line+1;
//        end;
      end
      else if AllSwitches[SwitchIndex].GetButtonActionByName('Button'+IntToStr(ButtonIndex))='(Ret Auto)' then
      begin
//        While ButtonLoopCount < 2 do
//        begin
          ButtonDownAction:= AllSwitches[SwitchIndex].GetButtonDownbyName('Button'+IntToStr(ButtonIndex));
          ButtonUpAction:= AllSwitches[SwitchIndex].GetButtonUpbyName('Button'+IntToStr(ButtonIndex));
          if (ButtonDownAction<>'')and(ButtonUpAction<>'') then
          begin
            LookUpItemCount:=LookUpItemCount-1;
            SetLength(MapBuffer, LookUpItemCount);
          end;
          MapLineValue.Add ('1');
          MapLineValue.Add ('62');
          MapLineValue.Add('64');
          MapLineValue.Add('0');
          MapLineValue.Add('25');
          Split('.',AllSwitches[SwitchIndex].GetButtonGroupByName('Button'+IntToStr(ButtonIndex)),SplitZGN);
          MapLineValue.Add(SplitZGN[0]);
          MapLineValue.Add(SplitZGN[1]);
          MapLineValue.Add(SplitZGN[2]);
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');
          MapLineValue.Add('0');//~DEVICE,
          DeviceSide.Clear;
          DeviceSide:=AllSwitches[SwitchIndex].GetswAlpha;
//          DeviceSide.Delimiter:=' ';
//          DeviceSide.DelimitedText:='D E V I C E ,';
          DeviceID:=AllSwitches[SwitchIndex].DeviceID;
          For StringIndex := 1 to Length(DeviceID) do
          begin
            DeviceSide.Add(DeviceID[StringIndex]);
          end;
          DeviceSide.Add(',');
          ButtonID:=AllSwitches[SwitchIndex].GetButtonIDbyName('Button'+IntToStr(ButtonIndex));
          for I := 1 to Length(ButtonID) do
          begin
            DeviceSide.Add(ButtonID[I]);

          end;
          DeviceSide.Add(',');
          ButtonDownAction:= AllSwitches[SwitchIndex].GetButtonDownbyName('Button'+IntToStr(ButtonIndex));
          ButtonUpAction:= AllSwitches[SwitchIndex].GetButtonUpbyName('Button'+IntToStr(ButtonIndex));
//          if(ButtonDownAction='') then
//          begin
//            ButtonLoopCount:=1;
//          end;
//          if ButtonLoopCount=0 then
//          begin
            for I := 1 to Length(ButtonDownAction) do
            begin
              DeviceSide.Add(ButtonDownAction[I]);
            end;
//            if (ButtonUpAction='') then
//            begin
//              ButtonLoopCount:=ButtonLoopCount+2;
//            end
//            else
//            begin
//              ButtonLoopCount:=ButtonLoopCount+1;
//            end;
//          end
//          else if ButtonLoopCount=1 then
//          begin
//            for I := 1 to Length(ButtonUpAction) do
//            begin
//              DeviceSide.Add(ButtonUpAction[I]);
//            end;
//            ButtonLoopCount:=ButtonLoopCount+1;
//          end;
          MapLineValue.Add(IntTOStr(DeviceSide.Count));//LUtron Side Count
          for I := 0 to DeviceSide.Count-1 do
          begin
            DeviceSideStr:=DeviceSide[I];
            if isNUmber(DeviceSideStr) then
            begin
              MapLineValue.Add(intToStr(StrToInt(DeviceSideStr)+48));
            end
            else
            begin
              for II := 1 to length(DeviceSideStr) do
              begin
                MapLineValue.Add(IntToStr(ord(DeviceSideStr[II])));
              end;
            end;

          end;
          for I := 0 to MapLineValue.count-1 do
          begin
            MapBuffer[Line][I]:=StrToInt(MapLineValue[I]);
          end;
          MapLineValue.clear;
          DeviceSide.Clear;
          Line:=Line+1;
//        end;
      end
      else if (AllSwitches[SwitchIndex].GetButtonActionByName('Button'+IntToStr(ButtonIndex))='(Pre-UP)')then
      begin
        for ButtonLoopCount := 1 to AllSwitches[SwitchIndex].ButtonCount do
        begin
          if(AllSwitches[SwitchIndex].GetButtonActionByName('Button'+IntToStr(ButtonLoopCount))='(Pre-Group)')then
          begin
//              if (ButtonDownAction<>'')and(ButtonUpAction<>'') then
//              begin
//                LookUpItemCount:=LookUpItemCount-1;
//                SetLength(MapBuffer, LookUpItemCount);
//              end;
//            PreLoopCount:=0;
//            While PreLoopCount < 2 do
//            begin
              MapLineValue.Add ('1');
              MapLineValue.Add ('62');
              MapLineValue.Add('64');
              MapLineValue.Add('0');
              MapLineValue.Add('13');
              Split('.',AllSwitches[SwitchIndex].GetButtonGroupByName('Button'+IntToStr(ButtonLoopCount)),SplitZGN);
              MapLineValue.Add(SplitZGN[0]);
              MapLineValue.Add(SplitZGN[1]);
              MapLineValue.Add(SplitZGN[2]);
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('32');
              MapLineValue.Add('17');
              MapLineValue.Add(IntToStr(ButtonLoopCount));
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');//~DEVICE,
              DeviceSide.Clear;
              DeviceSide:=AllSwitches[SwitchIndex].GetswAlpha;
//          DeviceSide.Delimiter:=' ';
//          DeviceSide.DelimitedText:='D E V I C E ,';
              DeviceID:=AllSwitches[SwitchIndex].DeviceID;
              For StringIndex := 1 to Length(DeviceID) do
              begin
                DeviceSide.Add(DeviceID[StringIndex]);
              end;
              DeviceSide.Add(',');
              ButtonID:=AllSwitches[SwitchIndex].GetButtonIDbyName('Button'+IntToStr(ButtonIndex));
              for I := 1 to Length(ButtonID) do
              begin
                DeviceSide.Add(ButtonID[I]);

              end;
              DeviceSide.Add(',');
              ButtonUpAction:= AllSwitches[SwitchIndex].GetButtonUpbyName('Button'+IntToStr(ButtonIndex));
              for I := 1 to Length(ButtonUpAction) do
              begin
                DeviceSide.Add(ButtonUpAction[I]);
              end;
              //DeviceSide.Add('4');
              MapLineValue.Add(IntTOStr(DeviceSide.Count));//LUtron Side Count
              for I := 0 to DeviceSide.Count-1 do
              begin
                DeviceSideStr:=DeviceSide[I];
                if isNUmber(DeviceSideStr) then
                begin
                  MapLineValue.Add(intToStr(StrToInt(DeviceSideStr)+48));
                end
                else
                begin
                  for II := 1 to length(DeviceSideStr) do
                  begin
                    MapLineValue.Add(IntToStr(ord(DeviceSideStr[II])));
                  end;
                end;

              end;
              for I := 0 to MapLineValue.count-1 do
              begin
                MapBuffer[Line][I]:=StrToInt(MapLineValue[I]);
              end;
              MapLineValue.clear;
              DeviceSide.Clear;
              Line:=Line+1;
//            end;
          end;
        end;
      end
      else if (AllSwitches[SwitchIndex].GetButtonActionByName('Button'+IntToStr(ButtonIndex))='(Pre-UP Hold)')then
      begin ///////////////////////////EDITING////////////////////////////////////////////
        for ButtonLoopCount := 1 to AllSwitches[SwitchIndex].ButtonCount do
        begin
          if(AllSwitches[SwitchIndex].GetButtonActionByName('Button'+IntToStr(ButtonLoopCount))='(Pre-Group)')then
          begin
            if(AllSwitches[SwitchIndex].GetButtonDownbyName('Button'+IntToStr(ButtonLoopCount))<>'')then
            begin
              MapLineValue.Add ('1');
              MapLineValue.Add ('62');
              MapLineValue.Add('64');
              MapLineValue.Add('0');
              MapLineValue.Add('13');
              Split('.',AllSwitches[SwitchIndex].GetButtonGroupByName('Button'+IntToStr(ButtonLoopCount)),SplitZGN);
              MapLineValue.Add(SplitZGN[0]);
              MapLineValue.Add(SplitZGN[1]);
              MapLineValue.Add(SplitZGN[2]);
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('32');
              MapLineValue.Add('17');
              MapLineValue.Add(IntToStr(ButtonLoopCount));
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');//~DEVICE,
              DeviceSide.Clear;
              DeviceSide:=AllSwitches[SwitchIndex].GetswAlpha;
//          DeviceSide.Delimiter:=' ';
//          DeviceSide.DelimitedText:='D E V I C E ,';
              DeviceID:=AllSwitches[SwitchIndex].DeviceID;
              For StringIndex := 1 to Length(DeviceID) do
              begin
                DeviceSide.Add(DeviceID[StringIndex]);
              end;
              DeviceSide.Add(',');
              ButtonID:=AllSwitches[SwitchIndex].GetButtonIDbyName('Button'+IntToStr(ButtonIndex));
              for I := 1 to Length(ButtonID) do
              begin
                DeviceSide.Add(ButtonID[I]);

              end;
              DeviceSide.Add(',');
              ButtonUpAction:= AllSwitches[SwitchIndex].GetButtonDownbyName('Button'+IntToStr(ButtonIndex));
              for I := 1 to Length(ButtonUpAction) do
              begin
                DeviceSide.Add(ButtonUpAction[I]);
              end;
              //DeviceSide.Add('4');
              MapLineValue.Add(IntTOStr(DeviceSide.Count));//LUtron Side Count
              for I := 0 to DeviceSide.Count-1 do
              begin
                DeviceSideStr:=DeviceSide[I];
                if isNUmber(DeviceSideStr) then
                begin
                  MapLineValue.Add(intToStr(StrToInt(DeviceSideStr)+48));
                end
                else
                begin
                  for II := 1 to length(DeviceSideStr) do
                  begin
                    MapLineValue.Add(IntToStr(ord(DeviceSideStr[II])));
                  end;
                end;

              end;
              for I := 0 to MapLineValue.count-1 do
              begin
                MapBuffer[Line][I]:=StrToInt(MapLineValue[I]);
              end;
              MapLineValue.clear;
              DeviceSide.Clear;
              Line:=Line+1;
            end;
            if(AllSwitches[SwitchIndex].GetButtonUpbyName('Button'+IntToStr(ButtonLoopCount))<>'')then
            begin
//           /////////////////////////////////////////////////////////////////////
              MapLineValue.Add ('1');
              MapLineValue.Add ('62');
              MapLineValue.Add('64');
              MapLineValue.Add('0');
              MapLineValue.Add('34');
              Split('.',AllSwitches[SwitchIndex].GetButtonGroupByName('Button'+IntToStr(ButtonLoopCount)),SplitZGN);
              MapLineValue.Add(SplitZGN[0]);
              MapLineValue.Add(SplitZGN[1]);
              MapLineValue.Add(SplitZGN[2]);
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('32');
              MapLineValue.Add('17');
              MapLineValue.Add(IntToStr(ButtonLoopCount));
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');//~DEVICE,
              DeviceSide.Clear;
              DeviceSide:=AllSwitches[SwitchIndex].GetswAlpha;
//          DeviceSide.Delimiter:=' ';
//          DeviceSide.DelimitedText:='D E V I C E ,';
              DeviceID:=AllSwitches[SwitchIndex].DeviceID;
              For StringIndex := 1 to Length(DeviceID) do
              begin
                DeviceSide.Add(DeviceID[StringIndex]);
              end;
              DeviceSide.Add(',');
              ButtonID:=AllSwitches[SwitchIndex].GetButtonIDbyName('Button'+IntToStr(ButtonIndex));
              for I := 1 to Length(ButtonID) do
              begin
                DeviceSide.Add(ButtonID[I]);

              end;
              DeviceSide.Add(',');
              ButtonUpAction:= AllSwitches[SwitchIndex].GetButtonUpbyName('Button'+IntToStr(ButtonIndex));
              for I := 1 to Length(ButtonUpAction) do
              begin
                DeviceSide.Add(ButtonUpAction[I]);
              end;
              //DeviceSide.Add('4');
              MapLineValue.Add(IntTOStr(DeviceSide.Count));//LUtron Side Count
              for I := 0 to DeviceSide.Count-1 do
              begin
                DeviceSideStr:=DeviceSide[I];
                if isNUmber(DeviceSideStr) then
                begin
                  MapLineValue.Add(intToStr(StrToInt(DeviceSideStr)+48));
                end
                else
                begin
                  for II := 1 to length(DeviceSideStr) do
                  begin
                    MapLineValue.Add(IntToStr(ord(DeviceSideStr[II])));
                  end;
                end;

              end;
              for I := 0 to MapLineValue.count-1 do
              begin
                MapBuffer[Line][I]:=StrToInt(MapLineValue[I]);
              end;
              MapLineValue.clear;
              DeviceSide.Clear;
              Line:=Line+1;
            end;
          end;
        end;
      end
      else if (AllSwitches[SwitchIndex].GetButtonActionByName('Button'+IntToStr(ButtonIndex))='(Pre-Down Hold)')then
      begin ///////////////////////////EDITING////////////////////////////////////////////
        for ButtonLoopCount := 1 to AllSwitches[SwitchIndex].ButtonCount do
        begin
          if(AllSwitches[SwitchIndex].GetButtonActionByName('Button'+IntToStr(ButtonLoopCount))='(Pre-Group)')then
          begin
            if(AllSwitches[SwitchIndex].GetButtonDownbyName('Button'+IntToStr(ButtonLoopCount))<>'')then
            begin
              MapLineValue.Add ('1');
              MapLineValue.Add ('62');
              MapLineValue.Add('64');
              MapLineValue.Add('0');
              MapLineValue.Add('18');
              Split('.',AllSwitches[SwitchIndex].GetButtonGroupByName('Button'+IntToStr(ButtonLoopCount)),SplitZGN);
              MapLineValue.Add(SplitZGN[0]);
              MapLineValue.Add(SplitZGN[1]);
              MapLineValue.Add(SplitZGN[2]);
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('32');
              MapLineValue.Add('17');
              MapLineValue.Add(IntToStr(ButtonLoopCount));
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');//~DEVICE,
              DeviceSide.Clear;
              DeviceSide:=AllSwitches[SwitchIndex].GetswAlpha;
//          DeviceSide.Delimiter:=' ';
//          DeviceSide.DelimitedText:='D E V I C E ,';
              DeviceID:=AllSwitches[SwitchIndex].DeviceID;
              For StringIndex := 1 to Length(DeviceID) do
              begin
                DeviceSide.Add(DeviceID[StringIndex]);
              end;
              DeviceSide.Add(',');
              ButtonID:=AllSwitches[SwitchIndex].GetButtonIDbyName('Button'+IntToStr(ButtonIndex));
              for I := 1 to Length(ButtonID) do
              begin
                DeviceSide.Add(ButtonID[I]);

              end;
              DeviceSide.Add(',');
              ButtonUpAction:= AllSwitches[SwitchIndex].GetButtonDownbyName('Button'+IntToStr(ButtonIndex));
              for I := 1 to Length(ButtonUpAction) do
              begin
                DeviceSide.Add(ButtonUpAction[I]);
              end;
              //DeviceSide.Add('4');
              MapLineValue.Add(IntTOStr(DeviceSide.Count));//LUtron Side Count
              for I := 0 to DeviceSide.Count-1 do
              begin
                DeviceSideStr:=DeviceSide[I];
                if isNUmber(DeviceSideStr) then
                begin
                  MapLineValue.Add(intToStr(StrToInt(DeviceSideStr)+48));
                end
                else
                begin
                  for II := 1 to length(DeviceSideStr) do
                  begin
                    MapLineValue.Add(IntToStr(ord(DeviceSideStr[II])));
                  end;
                end;

              end;
              for I := 0 to MapLineValue.count-1 do
              begin
                MapBuffer[Line][I]:=StrToInt(MapLineValue[I]);
              end;
              MapLineValue.clear;
              DeviceSide.Clear;
              Line:=Line+1;
            end;
            if(AllSwitches[SwitchIndex].GetButtonUpbyName('Button'+IntToStr(ButtonLoopCount))<>'')then
            begin
//           /////////////////////////////////////////////////////////////////////
              MapLineValue.Add ('1');
              MapLineValue.Add ('62');
              MapLineValue.Add('64');
              MapLineValue.Add('0');
              MapLineValue.Add('34');
              Split('.',AllSwitches[SwitchIndex].GetButtonGroupByName('Button'+IntToStr(ButtonLoopCount)),SplitZGN);
              MapLineValue.Add(SplitZGN[0]);
              MapLineValue.Add(SplitZGN[1]);
              MapLineValue.Add(SplitZGN[2]);
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('32');
              MapLineValue.Add('17');
              MapLineValue.Add(IntToStr(ButtonLoopCount));
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');//~DEVICE,
              DeviceSide.Clear;
              DeviceSide:=AllSwitches[SwitchIndex].GetswAlpha;
//          DeviceSide.Delimiter:=' ';
//          DeviceSide.DelimitedText:='D E V I C E ,';
              DeviceID:=AllSwitches[SwitchIndex].DeviceID;
              For StringIndex := 1 to Length(DeviceID) do
              begin
                DeviceSide.Add(DeviceID[StringIndex]);
              end;
              DeviceSide.Add(',');
              ButtonID:=AllSwitches[SwitchIndex].GetButtonIDbyName('Button'+IntToStr(ButtonIndex));
              for I := 1 to Length(ButtonID) do
              begin
                DeviceSide.Add(ButtonID[I]);

              end;
              DeviceSide.Add(',');
              ButtonUpAction:= AllSwitches[SwitchIndex].GetButtonUpbyName('Button'+IntToStr(ButtonIndex));
              for I := 1 to Length(ButtonUpAction) do
              begin
                DeviceSide.Add(ButtonUpAction[I]);
              end;
              //DeviceSide.Add('4');
              MapLineValue.Add(IntTOStr(DeviceSide.Count));//LUtron Side Count
              for I := 0 to DeviceSide.Count-1 do
              begin
                DeviceSideStr:=DeviceSide[I];
                if isNUmber(DeviceSideStr) then
                begin
                  MapLineValue.Add(intToStr(StrToInt(DeviceSideStr)+48));
                end
                else
                begin
                  for II := 1 to length(DeviceSideStr) do
                  begin
                    MapLineValue.Add(IntToStr(ord(DeviceSideStr[II])));
                  end;
                end;

              end;
              for I := 0 to MapLineValue.count-1 do
              begin
                MapBuffer[Line][I]:=StrToInt(MapLineValue[I]);
              end;
              MapLineValue.clear;
              DeviceSide.Clear;
              Line:=Line+1;
            end;
          end;
        end;
      end
      else if (AllSwitches[SwitchIndex].GetButtonActionByName('Button'+IntToStr(ButtonIndex))='(Pre-Down)')then
      begin
        for ButtonLoopCount := 1 to AllSwitches[SwitchIndex].ButtonCount do
        begin
          if(AllSwitches[SwitchIndex].GetButtonActionByName('Button'+IntToStr(ButtonLoopCount))='(Pre-Group)')then
          begin
//            PreLoopCount:=0;
//            While PreLoopCount < 2 do
//            begin
            MapLineValue.Add ('1');
            MapLineValue.Add ('62');
            MapLineValue.Add('64');
            MapLineValue.Add('0');
            MapLineValue.Add('18');
            Split('.',AllSwitches[SwitchIndex].GetButtonGroupByName('Button'+IntToStr(ButtonLoopCount)),SplitZGN);
            MapLineValue.Add(SplitZGN[0]);
            MapLineValue.Add(SplitZGN[1]);
            MapLineValue.Add(SplitZGN[2]);
            MapLineValue.Add('0');
            MapLineValue.Add('0');
            MapLineValue.Add('0');
            MapLineValue.Add('0');
            MapLineValue.Add('32');
            MapLineValue.Add('17');
            MapLineValue.Add(IntToStr(ButtonLoopCount));
            MapLineValue.Add('0');
            MapLineValue.Add('0');
            MapLineValue.Add('0');
            MapLineValue.Add('0');
            MapLineValue.Add('0');
            MapLineValue.Add('0');
            MapLineValue.Add('0');
            MapLineValue.Add('0');
            MapLineValue.Add('0');//~DEVICE,
            DeviceSide.Clear;
            DeviceSide:=AllSwitches[SwitchIndex].GetswAlpha;
//          DeviceSide.Delimiter:=' ';
//          DeviceSide.DelimitedText:='D E V I C E ,';
            DeviceID:=AllSwitches[SwitchIndex].DeviceID;
            For StringIndex := 1 to Length(DeviceID) do
            begin
              DeviceSide.Add(DeviceID[StringIndex]);
            end;
            DeviceSide.Add(',');
            ButtonID:=AllSwitches[SwitchIndex].GetButtonIDbyName('Button'+IntToStr(ButtonIndex));
            for I := 1 to Length(ButtonID) do
            begin
              DeviceSide.Add(ButtonID[I]);

            end;
            DeviceSide.Add(',');
            ButtonUpAction:= AllSwitches[SwitchIndex].GetButtonUpbyName('Button'+IntToStr(ButtonIndex));
            for I := 1 to Length(ButtonUpAction) do
            begin
              DeviceSide.Add(ButtonUpAction[I]);
            end;
            //DeviceSide.Add('4');
            MapLineValue.Add(IntTOStr(DeviceSide.Count));//LUtron Side Count
            for I := 0 to DeviceSide.Count-1 do
            begin
              DeviceSideStr:=DeviceSide[I];
              if isNUmber(DeviceSideStr) then
              begin
                MapLineValue.Add(intToStr(StrToInt(DeviceSideStr)+48));
              end
              else
              begin
                for II := 1 to length(DeviceSideStr) do
                begin
                  MapLineValue.Add(IntToStr(ord(DeviceSideStr[II])));
                end;
              end;

            end;
            for I := 0 to MapLineValue.count-1 do
            begin
              MapBuffer[Line][I]:=StrToInt(MapLineValue[I]);
            end;
            MapLineValue.clear;
            DeviceSide.Clear;
            Line:=Line+1;
//            end;
          end;
        end;
      end
      else if (AllSwitches[SwitchIndex].GetButtonActionByName('Button'+IntToStr(ButtonIndex))='(Pre-Group)')or(AllSwitches[SwitchIndex].GetButtonActionByName('Button'+IntToStr(ButtonIndex))='(Pre-None)')then
      begin
        MapLineValue.Add ('1');
        MapLineValue.Add ('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        //Split('.',AllSwitches[SwitchIndex].GetButtonGroupByName('Button'+IntToStr(ButtonLoopCount)),SplitZGN);
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('16'); //Set SysVar(16-set/32-compare)
        MapLineValue.Add('17'); //
        MapLineValue.Add(IntToStr(ButtonIndex));
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');
        MapLineValue.Add('0');//~DEVICE,
        DeviceSide.Clear;
        DeviceSide:=AllSwitches[SwitchIndex].GetswAlpha;
//          DeviceSide.Delimiter:=' ';
//          DeviceSide.DelimitedText:='D E V I C E ,';
        DeviceID:=AllSwitches[SwitchIndex].DeviceID;
        For StringIndex := 1 to Length(DeviceID) do
        begin
          DeviceSide.Add(DeviceID[StringIndex]);
        end;
        DeviceSide.Add(',');
        ButtonID:=AllSwitches[SwitchIndex].GetButtonIDbyName('Button'+IntToStr(ButtonIndex));
        for I := 1 to Length(ButtonID) do
        begin
          DeviceSide.Add(ButtonID[I]);

        end;
        DeviceSide.Add(',');
        ButtonUpAction:= AllSwitches[SwitchIndex].GetButtonUpbyName('Button'+IntToStr(ButtonIndex));
        for I := 1 to Length(ButtonUpAction) do
        begin
          DeviceSide.Add(ButtonUpAction[I]);
        end;
        //DeviceSide.Add('4');
        MapLineValue.Add(IntTOStr(DeviceSide.Count));//LUtron Side Count
        for I := 0 to DeviceSide.Count-1 do
        begin
          DeviceSideStr:=DeviceSide[I];
          if isNUmber(DeviceSideStr) then
          begin
            MapLineValue.Add(intToStr(StrToInt(DeviceSideStr)+48));
          end
          else
          begin
            for II := 1 to length(DeviceSideStr) do
            begin
              MapLineValue.Add(IntToStr(ord(DeviceSideStr[II])));
            end;
          end;

        end;
        for I := 0 to MapLineValue.count-1 do
        begin
          MapBuffer[Line][I]:=StrToInt(MapLineValue[I]);
        end;
        MapLineValue.clear;
        DeviceSide.Clear;
        Line:=Line+1;

      end
      else if (AllSwitches[SwitchIndex].GetButtonActionByName('Button'+IntToStr(ButtonIndex))='(Pre-PS1)')then
      begin
        for ButtonLoopCount := 1 to AllSwitches[SwitchIndex].ButtonCount do
        begin
          if(AllSwitches[SwitchIndex].GetButtonActionByName('Button'+IntToStr(ButtonLoopCount))='(Pre-Group)')then
          begin
//            PreLoopCount:=0;
//            While PreLoopCount < 2 do
//            begin
              MapLineValue.Add ('1');
              MapLineValue.Add ('62');
              MapLineValue.Add('64');
              MapLineValue.Add('0');
              MapLineValue.Add('14');
              Split('.',AllSwitches[SwitchIndex].GetButtonGroupByName('Button'+IntToStr(ButtonLoopCount)),SplitZGN);
              MapLineValue.Add(SplitZGN[0]);
              MapLineValue.Add(SplitZGN[1]);
              MapLineValue.Add(SplitZGN[2]);
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('32');
              MapLineValue.Add('17');
              MapLineValue.Add(IntToStr(ButtonLoopCount));
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');//~DEVICE,
              DeviceSide.Clear;
              DeviceSide:=AllSwitches[SwitchIndex].GetswAlpha;
//          DeviceSide.Delimiter:=' ';
//          DeviceSide.DelimitedText:='D E V I C E ,';
              DeviceID:=AllSwitches[SwitchIndex].DeviceID;
              For StringIndex := 1 to Length(DeviceID) do
              begin
                DeviceSide.Add(DeviceID[StringIndex]);
              end;
              DeviceSide.Add(',');
              ButtonID:=AllSwitches[SwitchIndex].GetButtonIDbyName('Button'+IntToStr(ButtonIndex));
              for I := 1 to Length(ButtonID) do
              begin
                DeviceSide.Add(ButtonID[I]);

              end;
              DeviceSide.Add(',');
              ButtonUpAction:= AllSwitches[SwitchIndex].GetButtonUpbyName('Button'+IntToStr(ButtonIndex));
              for I := 1 to Length(ButtonUpAction) do
              begin
                DeviceSide.Add(ButtonUpAction[I]);
              end;
              //DeviceSide.Add('4');
              MapLineValue.Add(IntTOStr(DeviceSide.Count));//LUtron Side Count
              for I := 0 to DeviceSide.Count-1 do
              begin
                DeviceSideStr:=DeviceSide[I];
                if isNUmber(DeviceSideStr) then
                begin
                  MapLineValue.Add(intToStr(StrToInt(DeviceSideStr)+48));
                end
                else
                begin
                  for II := 1 to length(DeviceSideStr) do
                  begin
                    MapLineValue.Add(IntToStr(ord(DeviceSideStr[II])));
                  end;
                end;

              end;
              for I := 0 to MapLineValue.count-1 do
              begin
                MapBuffer[Line][I]:=StrToInt(MapLineValue[I]);
              end;
              MapLineValue.clear;
              DeviceSide.Clear;
              Line:=Line+1;
//            end;
          end;
        end;
      end
      else if (AllSwitches[SwitchIndex].GetButtonActionByName('Button'+IntToStr(ButtonIndex))='(Pre-PS2)')then
      begin
        for ButtonLoopCount := 1 to AllSwitches[SwitchIndex].ButtonCount do
        begin
          if(AllSwitches[SwitchIndex].GetButtonActionByName('Button'+IntToStr(ButtonLoopCount))='(Pre-Group)')then
          begin
//            PreLoopCount:=0;
//            While PreLoopCount < 2 do
//            begin
            MapLineValue.Add ('1');
            MapLineValue.Add ('62');
            MapLineValue.Add('64');
            MapLineValue.Add('0');
            MapLineValue.Add('15');
            Split('.',AllSwitches[SwitchIndex].GetButtonGroupByName('Button'+IntToStr(ButtonLoopCount)),SplitZGN);
            MapLineValue.Add(SplitZGN[0]);
            MapLineValue.Add(SplitZGN[1]);
            MapLineValue.Add(SplitZGN[2]);
            MapLineValue.Add('0');
            MapLineValue.Add('0');
            MapLineValue.Add('0');
            MapLineValue.Add('0');
            MapLineValue.Add('32');
            MapLineValue.Add('17');
            MapLineValue.Add(IntToStr(ButtonLoopCount));
            MapLineValue.Add('0');
            MapLineValue.Add('0');
            MapLineValue.Add('0');
            MapLineValue.Add('0');
            MapLineValue.Add('0');
            MapLineValue.Add('0');
            MapLineValue.Add('0');
            MapLineValue.Add('0');
            MapLineValue.Add('0');//~DEVICE,
            DeviceSide.Clear;
            DeviceSide:=AllSwitches[SwitchIndex].GetswAlpha;
//          DeviceSide.Delimiter:=' ';
//          DeviceSide.DelimitedText:='D E V I C E ,';
            DeviceID:=AllSwitches[SwitchIndex].DeviceID;
            For StringIndex := 1 to Length(DeviceID) do
            begin
              DeviceSide.Add(DeviceID[StringIndex]);
            end;
            DeviceSide.Add(',');
            ButtonID:=AllSwitches[SwitchIndex].GetButtonIDbyName('Button'+IntToStr(ButtonIndex));
            for I := 1 to Length(ButtonID) do
            begin
              DeviceSide.Add(ButtonID[I]);

            end;
            DeviceSide.Add(',');
            ButtonUpAction:= AllSwitches[SwitchIndex].GetButtonUpbyName('Button'+IntToStr(ButtonIndex));
            for I := 1 to Length(ButtonUpAction) do
            begin
              DeviceSide.Add(ButtonUpAction[I]);
            end;
            //DeviceSide.Add('4');
            MapLineValue.Add(IntTOStr(DeviceSide.Count));//LUtron Side Count
            for I := 0 to DeviceSide.Count-1 do
            begin
              DeviceSideStr:=DeviceSide[I];
              if isNUmber(DeviceSideStr) then
              begin
                MapLineValue.Add(intToStr(StrToInt(DeviceSideStr)+48));
              end
              else
              begin
                for II := 1 to length(DeviceSideStr) do
                begin
                  MapLineValue.Add(IntToStr(ord(DeviceSideStr[II])));
                end;
              end;

            end;
            for I := 0 to MapLineValue.count-1 do
            begin
              MapBuffer[Line][I]:=StrToInt(MapLineValue[I]);
            end;
            MapLineValue.clear;
            DeviceSide.Clear;
            Line:=Line+1;
//            end;
          end;
        end;
      end
      else if (AllSwitches[SwitchIndex].GetButtonActionByName('Button'+IntToStr(ButtonIndex))='(Pre-PS3)')then
      begin
        for ButtonLoopCount := 1 to AllSwitches[SwitchIndex].ButtonCount do
        begin
          if(AllSwitches[SwitchIndex].GetButtonActionByName('Button'+IntToStr(ButtonLoopCount))='(Pre-Group)')then
          begin
//            PreLoopCount:=0;
//            While PreLoopCount < 2 do
//            begin
            MapLineValue.Add ('1');
            MapLineValue.Add ('62');
            MapLineValue.Add('64');
            MapLineValue.Add('0');
            MapLineValue.Add('16');
            Split('.',AllSwitches[SwitchIndex].GetButtonGroupByName('Button'+IntToStr(ButtonLoopCount)),SplitZGN);
            MapLineValue.Add(SplitZGN[0]);
            MapLineValue.Add(SplitZGN[1]);
            MapLineValue.Add(SplitZGN[2]);
            MapLineValue.Add('0');
            MapLineValue.Add('0');
            MapLineValue.Add('0');
            MapLineValue.Add('0');
            MapLineValue.Add('32');
            MapLineValue.Add('17');
            MapLineValue.Add(IntToStr(ButtonLoopCount));
            MapLineValue.Add('0');
            MapLineValue.Add('0');
            MapLineValue.Add('0');
            MapLineValue.Add('0');
            MapLineValue.Add('0');
            MapLineValue.Add('0');
            MapLineValue.Add('0');
            MapLineValue.Add('0');
            MapLineValue.Add('0');//~DEVICE,
            DeviceSide.Clear;
            DeviceSide:=AllSwitches[SwitchIndex].GetswAlpha;
//          DeviceSide.Delimiter:=' ';
//          DeviceSide.DelimitedText:='D E V I C E ,';
            DeviceID:=AllSwitches[SwitchIndex].DeviceID;
            For StringIndex := 1 to Length(DeviceID) do
            begin
              DeviceSide.Add(DeviceID[StringIndex]);
            end;
            DeviceSide.Add(',');
            ButtonID:=AllSwitches[SwitchIndex].GetButtonIDbyName('Button'+IntToStr(ButtonIndex));
            for I := 1 to Length(ButtonID) do
            begin
              DeviceSide.Add(ButtonID[I]);

            end;
            DeviceSide.Add(',');
            ButtonUpAction:= AllSwitches[SwitchIndex].GetButtonUpbyName('Button'+IntToStr(ButtonIndex));
            for I := 1 to Length(ButtonUpAction) do
            begin
              DeviceSide.Add(ButtonUpAction[I]);
            end;
            //DeviceSide.Add('4');
            MapLineValue.Add(IntTOStr(DeviceSide.Count));//LUtron Side Count
            for I := 0 to DeviceSide.Count-1 do
            begin
              DeviceSideStr:=DeviceSide[I];
              if isNUmber(DeviceSideStr) then
              begin
                MapLineValue.Add(intToStr(StrToInt(DeviceSideStr)+48));
              end
              else
              begin
                for II := 1 to length(DeviceSideStr) do
                begin
                  MapLineValue.Add(IntToStr(ord(DeviceSideStr[II])));
                end;
              end;

            end;
            for I := 0 to MapLineValue.count-1 do
            begin
              MapBuffer[Line][I]:=StrToInt(MapLineValue[I]);
            end;
            MapLineValue.clear;
            DeviceSide.Clear;
            Line:=Line+1;
//            end;
          end;
        end;
      end
      else if (AllSwitches[SwitchIndex].GetButtonActionByName('Button'+IntToStr(ButtonIndex))='(Pre-Stop)')then
      begin
        for ButtonLoopCount := 1 to AllSwitches[SwitchIndex].ButtonCount do
        begin
          if(AllSwitches[SwitchIndex].GetButtonActionByName('Button'+IntToStr(ButtonLoopCount))='(Pre-Group)')then
          begin
//            PreLoopCount:=0;
//            While PreLoopCount < 2 do
//            begin
              MapLineValue.Add ('1');
              MapLineValue.Add ('62');
              MapLineValue.Add('64');
              MapLineValue.Add('0');
              MapLineValue.Add('34');
              Split('.',AllSwitches[SwitchIndex].GetButtonGroupByName('Button'+IntToStr(ButtonLoopCount)),SplitZGN);
              MapLineValue.Add(SplitZGN[0]);
              MapLineValue.Add(SplitZGN[1]);
              MapLineValue.Add(SplitZGN[2]);
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('32');
              MapLineValue.Add('17');
              MapLineValue.Add(IntToStr(ButtonLoopCount));
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');//~DEVICE,
              DeviceSide.Clear;
              DeviceSide:=AllSwitches[SwitchIndex].GetswAlpha;
//          DeviceSide.Delimiter:=' ';
//          DeviceSide.DelimitedText:='D E V I C E ,';
              DeviceID:=AllSwitches[SwitchIndex].DeviceID;
              For StringIndex := 1 to Length(DeviceID) do
              begin
                DeviceSide.Add(DeviceID[StringIndex]);
              end;
              DeviceSide.Add(',');
              ButtonID:=AllSwitches[SwitchIndex].GetButtonIDbyName('Button'+IntToStr(ButtonIndex));
              for I := 1 to Length(ButtonID) do
              begin
                DeviceSide.Add(ButtonID[I]);

              end;
              DeviceSide.Add(',');
              ButtonUpAction:= AllSwitches[SwitchIndex].GetButtonUpbyName('Button'+IntToStr(ButtonIndex));
              for I := 1 to Length(ButtonUpAction) do
              begin
                DeviceSide.Add(ButtonUpAction[I]);
              end;
              //DeviceSide.Add('4');
              MapLineValue.Add(IntTOStr(DeviceSide.Count));//LUtron Side Count
              for I := 0 to DeviceSide.Count-1 do
              begin
                DeviceSideStr:=DeviceSide[I];
                if isNUmber(DeviceSideStr) then
                begin
                  MapLineValue.Add(intToStr(StrToInt(DeviceSideStr)+48));
                end
                else
                begin
                  for II := 1 to length(DeviceSideStr) do
                  begin
                    MapLineValue.Add(IntToStr(ord(DeviceSideStr[II])));
                  end;
                end;

              end;
              for I := 0 to MapLineValue.count-1 do
              begin
                MapBuffer[Line][I]:=StrToInt(MapLineValue[I]);
              end;
              MapLineValue.clear;
              DeviceSide.Clear;
              Line:=Line+1;
//            end;
          end;
        end;
      end
      else if (AllSwitches[SwitchIndex].GetButtonActionByName('Button'+IntToStr(ButtonIndex))='(Pre-Ret Auto)')then
      begin
        for ButtonLoopCount := 1 to AllSwitches[SwitchIndex].ButtonCount do
        begin
          if(AllSwitches[SwitchIndex].GetButtonActionByName('Button'+IntToStr(ButtonLoopCount))='(Pre-Group)')then
          begin
//            PreLoopCount:=0;
//            While PreLoopCount < 2 do
//            begin
              MapLineValue.Add ('1');
              MapLineValue.Add ('62');
              MapLineValue.Add('64');
              MapLineValue.Add('0');
              MapLineValue.Add('25');
              Split('.',AllSwitches[SwitchIndex].GetButtonGroupByName('Button'+IntToStr(ButtonLoopCount)),SplitZGN);
              MapLineValue.Add(SplitZGN[0]);
              MapLineValue.Add(SplitZGN[1]);
              MapLineValue.Add(SplitZGN[2]);
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('32');
              MapLineValue.Add('17');
              MapLineValue.Add(IntToStr(ButtonLoopCount));
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              MapLineValue.Add('0');
              DeviceSide.Clear;
              DeviceSide:=AllSwitches[SwitchIndex].GetswAlpha;
//          DeviceSide.Delimiter:=' ';
//          DeviceSide.DelimitedText:='D E V I C E ,';
              DeviceID:=AllSwitches[SwitchIndex].DeviceID;
              For StringIndex := 1 to Length(DeviceID) do
              begin
                DeviceSide.Add(DeviceID[StringIndex]);
              end;
              DeviceSide.Add(',');
              ButtonID:=AllSwitches[SwitchIndex].GetButtonIDbyName('Button'+IntToStr(ButtonIndex));
              for I := 1 to Length(ButtonID) do
              begin
                DeviceSide.Add(ButtonID[I]);

              end;
              DeviceSide.Add(',');
              ButtonUpAction:= AllSwitches[SwitchIndex].GetButtonUpbyName('Button'+IntToStr(ButtonIndex));
              for I := 1 to Length(ButtonUpAction) do
              begin
                DeviceSide.Add(ButtonUpAction[I]);
              end;
              //DeviceSide.Add('4');
              MapLineValue.Add(IntTOStr(DeviceSide.Count));//LUtron Side Count
              for I := 0 to DeviceSide.Count-1 do
              begin
                DeviceSideStr:=DeviceSide[I];
                if isNUmber(DeviceSideStr) then
                begin
                  MapLineValue.Add(intToStr(StrToInt(DeviceSideStr)+48));
                end
                else
                begin
                  for II := 1 to length(DeviceSideStr) do
                  begin
                    MapLineValue.Add(IntToStr(ord(DeviceSideStr[II])));
                  end;
                end;

              end;
              for I := 0 to MapLineValue.count-1 do
              begin
                MapBuffer[Line][I]:=StrToInt(MapLineValue[I]);
              end;
              MapLineValue.clear;
              DeviceSide.Clear;
              Line:=Line+1;
//            end;
          end;
        end;
      end;

    end;
  end;
  AdjustedLookupCount:= LookUpItemCount;
  checksumWord:=CheckSumWord+word(AdjustedLookupCount)+1;
  for I := 0 to Length(MapBuffer)-1 do
  begin
    tempCounter:=0;
    for II := 0 to Length(MapBuffer[I])-1 do
    begin
      tempCounter:= tempCounter+MapBuffer[I][II];
      CheckSumWord:=CheckSumWord+MapBuffer[I][II];
    end;
    //showmessage(IntToStr(tempCounter));
  end;

  repeat
    Swapped:=false;
    for I := 0 to Length(MapBuffer)-2 do
    begin
      if MapBuffer[I][24]>MapBuffer[I+1][24] then  //data length
      begin
        AlphabeticalTemp:=MapBuffer[I];
        MapBuffer[I]:=MapBuffer[I+1];
        MapBuffer[I+1]:=AlphabeticalTemp;
        Swapped:=true;
      end
      else
      begin
        for Count1 := 25 to Length(MapBuffer[I])-1 do
        begin
          if MapBuffer[I][Count1]>MapBuffer[I+1][Count1] then
          begin
            if(MapBuffer[I][24]>=MapBuffer[I+1][24])then
              begin
                AlphabeticalTemp:=MapBuffer[I];
                MapBuffer[I]:=MapBuffer[I+1];
                MapBuffer[I+1]:=AlphabeticalTemp;
                Swapped:=true;
                break;
              end;
          end
          else if MapBuffer[I][Count1]<MapBuffer[I+1][Count1] then
          begin
            break;
          end;
        end;
      end

    end;
  until not swapped;
//  repeat
//    Swapped:=false;
//    for I := 0 to Length(MapBuffer)-2 do
//    begin
//      if MapBuffer[I][24]>MapBuffer[I+1][24] then  //data length
//      begin
//        AlphabeticalTemp:=MapBuffer[I];
//        MapBuffer[I]:=MapBuffer[I+1];
//        MapBuffer[I+1]:=AlphabeticalTemp;
//        Swapped:=true;
//      end
//      else
//      begin
//        for Count1 := 25 to Length(MapBuffer[I])-1 do
//        begin
//          if MapBuffer[I][Count1]>MapBuffer[I+1][Count1] then
//          begin
//            if(MapBuffer[I][24]>=MapBuffer[I+1][24])then
//              begin
//                AlphabeticalTemp:=MapBuffer[I];
//                MapBuffer[I]:=MapBuffer[I+1];
//                MapBuffer[I+1]:=AlphabeticalTemp;
//                Swapped:=true;
//                break;
//              end;
//          end
//          else if MapBuffer[I][Count1]<MapBuffer[I+1][Count1] then
//          begin
//            break;
//          end;
//        end;
//      end
//
//    end;
//  until not swapped;
  CalcCheckSum:= CheckSumWord;
end;

procedure TMNIConfigFm.cbIRFWDEnabledClick(Sender: TObject);
begin
  shpcbIRFWDEnabled.Visible:=true;
  btUpdateParams.Color:=clLime;
end;

procedure TMNIConfigFm.cbIRPortEnabledClick(Sender: TObject);
begin
  shpcbIRPortEnabled.Visible:=true;
  btUpdateParams.Color:=clLime;
end;

procedure TMNIConfigFm.cbSerialEnabledClick(Sender: TObject);
begin
  shpcbSerialEnabled.Visible:=true;
  btUpdateParams.Color:=clLime;
end;

procedure TMNIConfigFm.cbSerialMSGChkSumClick(Sender: TObject);
begin
  shpcbSerialMSGChkSum.Visible:=true;
  btUpdateParams.Color:=clLime;
end;

procedure TMNIConfigFm.cbSerialReceiveClick(Sender: TObject);
begin
  shpcbSerialReceive.Visible:=true;
  btUpdateParams.Color:=clLime;
end;

procedure TMNIConfigFm.cbSerialRoutingFieldClick(Sender: TObject);
begin
  shpcbSerialRoutingField.Visible:=true;
  btUpdateParams.Color:=clLime;
end;

procedure TMNIConfigFm.cbSerialTransmitClick(Sender: TObject);
begin
  shpcbSerialTransmit.Visible:=true;
  btUpdateParams.Color:=clLime;
end;

procedure TMNIConfigFm.cbSerialUIDinZGNClick(Sender: TObject);
begin
  shpcbSerialUIDinZGN.Visible:=true;
  btUpdateParams.Color:=clLime;
end;

procedure TMNIConfigFm.cbSWEnabledClick(Sender: TObject);
begin
  shpcbSWEnabled.Visible:=true;
  btUpdateParams.Color:=clLime;
end;

procedure TMNIConfigFm.cbSWFWDEnabledClick(Sender: TObject);
begin
  shpcbSWFWDEnabled.Visible:=true;
  btUpdateParams.Color:=clLime;
end;

procedure TMNIConfigFm.btUpdateParamsClick(Sender: TObject);
var
  AliasText,SerialPerson,SerialBaud,SerialPL,MotorField,MotorCount:String;
  intDS,IntConfig,SwitchPerson:Integer;
  SwIndex,SerialOutgoingMSG,I: Integer;
  CheckSum:Word;
  OutPutList:TStringList;
begin
  if teSerialMapGroup.Focused then
  begin
    OutPutList:=TStringlist.Create;
    Split('.', Trim(teSerialMapGroup.Text), OutPutList);
    ProperZGNFlg:=false;
    if(teSerialMapGroup.Text<>'')then
    begin
      if VariablesUn.IsG('.', Trim(teSerialMapGroup.Text), OutPutList)then
      begin
        ProperZGNFlg:=true;
        teSerialMapGroup.Text:='255.'+teSerialMapGroup.Text+'.255';
      end
      else if VariablesUn.IsZGN(teSerialMapGroup.Text) then
      begin
        ProperZGNFlg:=true;
      end
      else if AllSwitches[SwIndex].GetButtonActionByName('Button'+IntToStr(sgMap.Row))='(Custom 7B)' then
      begin
        ProperZGNFlg:=true;
      end
      else
      begin
        showmessage('ZGN Fail: Improper Format xx.xx.xx or xx. Max 255.');
        exit;
      end;
    end;
  end;

  cmbSerialMapCol1Exit(nil);
  teSerialMapGroupExit(Nil);
  DataUn.dmDataModule.dsMNI.Filtered := False;
  DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(cmbProjectMNI.Text) +  ' and UID = ' + cmbUidMNI.Text;
  DataUn.dmDataModule.dsMNI.Filtered := True;
  giUID:=StrToInt(cmbUidMNI.Text);
  //Serial Params
  //Serial Personality
  if (cmbSerialDevices.Text <>LastSerialProfile)then
  begin
    LastSerialProfile:=cmbSerialDevices.Text;
    VariablesUn.ClearData;
    if cmbSerialProtocol.Text='RS-232-TIA' then
    begin
      SerialPL:=variablesUn.IntToBin(1);
    end
    else if cmbSerialProtocol.Text='RS-485' then
    begin
      SerialPL:=variablesUn.IntToBin(2);
    end
    else if cmbSerialProtocol.Text='RS-232-TTL' then
    begin
      SerialPL:='0';
    end
    else if cmbSerialProtocol.Text='USB' then
    begin
      SerialPL:=variablesUn.IntToBin(3);
    end
    else if cmbSerialProtocol.Text='WiFi' then
    begin
      SerialPL:=variablesUn.IntToBin(4);
    end;
    SerialPL:=RightStr(SerialPL,6);
    if cbSerialTransmit.Checked=true then
    begin
      SerialPL:=SerialPL+'1';
    end
    else
    begin
      SerialPL:=SerialPL+'0';
    end;
    if cbSerialReceive.Checked=true then
    begin
      SerialPL:=SerialPL+'1';
    end
    else
    begin
      SerialPL:=SerialPL+'0';
    end;
    SerialOutgoingMSG:=0;
    if cbSerialUIDinZGN.Checked=true then
    begin
      SerialOutgoingMSG:=Set_a_Bit(SerialOutgoingMSG,0);
    end
    else
    begin
      SerialOutgoingMSG:=Clear_a_Bit(SerialOutgoingMSG,0);
    end;
    if cbSerialRoutingField.Checked=true then
    begin
      SerialOutgoingMSG:=Set_a_Bit(SerialOutgoingMSG,1);
    end
    else
    begin
      SerialOutgoingMSG:=Clear_a_Bit(SerialOutgoingMSG,1);
    end;
    if cbSerialMSGChkSum.Checked=true then
    begin
      SerialOutgoingMSG:=Set_a_Bit(SerialOutgoingMSG,2);
    end
    else
    begin
      SerialOutgoingMSG:=Clear_a_Bit(SerialOutgoingMSG,2);
    end;

    SerialBaud:=IntToStr(cmbSerialBaud.ItemIndex+1);
    SerialPerson:=IntToStr(cmbSerialPerson.ItemIndex);

    giSentCom := StrToInt(LeftStr(VariablesUn.MNI_SerialPerson,3));
    ComFm.SetGenericParam(giUID, VariablesUn.MNI_SerialPerson+'.0.'+SerialPerson, rhMNIStatusRecieved);
    if(WaitforGenericResponseConfirmation(giUID, VariablesUn.MNI_SerialPerson+'.0.'+SerialPerson, rhMNIStatusRecieved))then
    begin
      shpcmbSerialPerson.Brush.Color :=clLime;
      shpcmbSerialPerson.Visible:=false;
    end
    else
    begin
      shpcmbSerialPerson.Brush.Color :=ClRed;
    end;

    giSentCom := StrToInt(LeftStr(VariablesUn.MNI_SerialBaud,3));
    ComFm.SetGenericParam(giUID, VariablesUn.MNI_SerialBaud+'.0.'+SerialBaud, rhMNIStatusRecieved);
    if WaitforGenericResponseConfirmation(giUID, VariablesUn.MNI_SerialBaud+'.0.'+SerialBaud, rhMNIStatusRecieved)then
    begin
      shpcmbSerialBaud.Brush.Color :=clLime;
      shpcmbSerialBaud.Visible:=false;
    end
    else
    begin
      shpcmbSerialBaud.Brush.Color :=ClRed;
    end;

    giSentCom := StrToInt(LeftStr(VariablesUn.MNI_SerialPL,3));
    ComFm.SetGenericParam(giUID, VariablesUn.MNI_SerialPL+'.0.'+IntToStr(BinToInt(SerialPL)), rhMNIStatusRecieved);
    if WaitforGenericResponseConfirmation(giUID, VariablesUn.MNI_SerialPL+'.0.'+IntToStr(BinToInt(SerialPL)), rhMNIStatusRecieved) then
    begin
      shpcbSerialReceive.Brush.Color :=clLime;
      shpcbSerialTransmit.Brush.Color :=clLime;
      shpcmbSerialProtocol.Brush.Color :=clLime;
      shpcbSerialReceive.Visible:=false;
      shpcbSerialTransmit.Visible:=false;
      shpcmbSerialProtocol.Visible:=false;
    end
    else
    begin
      shpcbSerialReceive.Brush.Color :=ClRed;
      shpcbSerialTransmit.Brush.Color :=ClRed;
      shpcmbSerialProtocol.Brush.Color :=ClRed;
    end;

    giSentCom := StrToInt(LeftStr(VariablesUn.MNI_SerialOutMSG,3));
    ComFm.SetGenericParam(giUID, VariablesUn.MNI_SerialOutMSG+'.0.'+IntToStr(SerialOutgoingMSG), rhMNIStatusRecieved);
    if WaitforGenericResponseConfirmation(giUID, VariablesUn.MNI_SerialOutMSG+'.0.'+IntToStr(SerialOutgoingMSG), rhMNIStatusRecieved)then
    begin
      cbSerialMSGChkSum.Brush.Color:=clLime;
      cbSerialRoutingField.Brush.Color:=clLime;
      cbSerialUIDinZGN.Brush.Color:=clLime;
      cbSerialMSGChkSum.Visible:=false;
      cbSerialRoutingField.Visible:=false;
      cbSerialUIDinZGN.Visible:=false;
    end
    else
    begin
      cbSerialMSGChkSum.Brush.Color:=ClRed;
      cbSerialRoutingField.Brush.Color:=ClRed;
      cbSerialUIDinZGN.Brush.Color:=ClRed;
    end;

  end;

  if (cbSerialEnabled.Checked=true) and (DataUn.dmDataModule.dsMNI.FieldByName('SerialDisable').AsInteger=1)then
  begin
    VariablesUn.ClearData;
    giSentCom := StrToInt(LeftStr(VariablesUn.MNI_SerialEnable,3));
    ComFm.SetGenericParam(giUID, VariablesUn.MNI_SerialEnable+'.0.0', rhMNIStatusRecieved);
    if WaitforGenericResponseConfirmation(giUID, VariablesUn.MNI_SerialEnable+'.0.0', rhMNIStatusRecieved)then
    begin
      shpcbSerialEnabled.Brush.Color:=clLime;
      shpcbSerialEnabled.Visible:=false;
    end
    else
    begin
      shpcbSerialEnabled.Brush.Color:=clRed;
    end;
  end
  else if (cbSerialEnabled.Checked=false) and (DataUn.dmDataModule.dsMNI.FieldByName('SerialDisable').AsInteger=0)then
  begin
    VariablesUn.ClearData;
    giSentCom := StrToInt(LeftStr(VariablesUn.MNI_SerialEnable,3));
    ComFm.SetGenericParam(giUID, VariablesUn.MNI_SerialEnable+'.0.1', rhMNIStatusRecieved);
    if WaitforGenericResponseConfirmation(giUID, VariablesUn.MNI_SerialEnable+'.0.1', rhMNIStatusRecieved) then
    begin
      shpcbSerialEnabled.Brush.Color:=clLime;
      shpcbSerialEnabled.Visible:=false;
    end
    else
    begin
      shpcbSerialEnabled.Brush.Color:=clRed;
    end;
  end;


  if SerialEditTest then
  begin
    if(SerialActionTest)then
    begin
      if(SerialGroupTest)then
      begin
        CheckSum:=CalcCheckSum;
        //ShowMessage(IntToStr(CheckSum));
        //exit;
        if SendMapPrep then
        begin
          if(SendMap(CheckSum))then
          begin
            for I := 0 to AllSwitches.count-1 do
            begin
              AllSwitches[I].EditFlg:=false;
            end;
          end;
        end;
      end
      else
      begin
        showMessage('Button Missing Group! All buttons require Groups');
        exit;
      end;
    end
    else
    begin
      showMessage('Button Missing Action! All buttons require Actions');
      exit;
    end;
  end;

  //SW PORT Button Personality
  if(cmbSWPerson.Text='3T') then
  begin
    SwitchPerson:=9;
  end
  else if(cmbSWPerson.Text='3L') then
  begin
    SwitchPerson:=17;
  end
  else if(cmbSWPerson.Text='3M') then
  begin
    SwitchPerson:=33;
  end
  else if(cmbSWPerson.Text='2L') then
  begin
    SwitchPerson:=18;
  end
  else if(cmbSWPerson.Text='2M') then
  begin
    SwitchPerson:=34;
  end
  else if(cmbSWPerson.Text='1A') then
  begin
    SwitchPerson:=4;
  end
  else if(cmbSWPerson.Text='1B') then
  begin
    SwitchPerson:=68;
  end
  else if(cmbSWPerson.Text='1C') then
  begin
    SwitchPerson:=132;
  end;
  case StrToInt(cmbSwitch.Text) of
    1:
    begin
      if DataUn.dmDataModule.dsMNI.FieldByName('SW1BtnPerson').AsInteger<>SwitchPerson then
      begin
        VariablesUn.ClearData;
        giSentCom := StrToInt(VariablesUn.UID_BP);
        ComFm.SetGenericParam(giUID, VariablesUn.UID_BP+'.'+cmbSwitch.Text+'.0.'+IntToStr(SwitchPerson), rhMNIStatusRecieved);
        if WaitforGenericResponseConfirmation(giUID, VariablesUn.UID_BP+'.'+cmbSwitch.Text+'.0.'+IntToStr(SwitchPerson), rhMNIStatusRecieved) then
        begin
          shpcmbSWPerson.Brush.Color:=clLime;
          shpcmbSWPerson.Visible:=false;
        end
        else
        begin
          shpcmbSWPerson.Brush.Color:=clRed;
        end;
      end;
    end;
    2:
    begin
      if DataUn.dmDataModule.dsMNI.FieldByName('SW2BtnPerson').AsInteger<>SwitchPerson then
      begin
        VariablesUn.ClearData;
        giSentCom := StrToInt(VariablesUn.UID_BP);
        ComFm.SetGenericParam(giUID, VariablesUn.UID_BP+'.'+cmbSwitch.Text+'.0.'+IntToStr(SwitchPerson), rhMNIStatusRecieved);
        if WaitforGenericResponseConfirmation(giUID, VariablesUn.UID_BP+'.'+cmbSwitch.Text+'.0.'+IntToStr(SwitchPerson), rhMNIStatusRecieved) then
        begin
          shpcmbSWPerson.Brush.Color:=clLime;
          shpcmbSWPerson.Visible:=false;
        end
        else
        begin
          shpcmbSWPerson.Brush.Color:=clRed;
        end;
      end;
    end;
    3:
    begin
      if DataUn.dmDataModule.dsMNI.FieldByName('SW3BtnPerson').AsInteger<>SwitchPerson then
      begin
        VariablesUn.ClearData;
        giSentCom := StrToInt(VariablesUn.UID_BP);
        ComFm.SetGenericParam(giUID, VariablesUn.UID_BP+'.'+cmbSwitch.Text+'.0.'+IntToStr(SwitchPerson), rhMNIStatusRecieved);
        if WaitforGenericResponseConfirmation(giUID, VariablesUn.UID_BP+'.'+cmbSwitch.Text+'.0.'+IntToStr(SwitchPerson), rhMNIStatusRecieved) then
        begin
          shpcmbSWPerson.Brush.Color:=clLime;
          shpcmbSWPerson.Visible:=false;
        end
        else
        begin
          shpcmbSWPerson.Brush.Color:=clRed;
        end;

      end;
    end;
    4:
    begin
      if DataUn.dmDataModule.dsMNI.FieldByName('SW4BtnPerson').AsInteger<>SwitchPerson then
      begin
        VariablesUn.ClearData;
        giSentCom := StrToInt(VariablesUn.UID_BP);
        ComFm.SetGenericParam(giUID, VariablesUn.UID_BP+'.'+cmbSwitch.Text+'.0.'+IntToStr(SwitchPerson), rhMNIStatusRecieved);
        if WaitforGenericResponseConfirmation(giUID, VariablesUn.UID_BP+'.'+cmbSwitch.Text+'.0.'+IntToStr(SwitchPerson), rhMNIStatusRecieved)then
        begin
          shpcmbSWPerson.Brush.Color:=clLime;
          shpcmbSWPerson.Visible:=false;
        end
        else
        begin
          shpcmbSWPerson.Brush.Color:=clRed;
        end;
      end;
    end;
  end;
  //IR ZGN
  if DataUn.dmDataModule.dsMNI.FieldByName('IRPortFwdAddr').AsString<> teIRFWDAddr.Text then
  begin
    VariablesUn.ClearData;
    giSentCom := StrToInt(VariablesUn.MNI_IRZGN);
    ComFm.SetGenericParam(giUID, VariablesUn.MNI_IRZGN+'.'+ teIRFWDAddr.Text, rhMNIStatusRecieved);
    if WaitforGenericResponseConfirmation(giUID, VariablesUn.MNI_IRZGN+'.'+ teIRFWDAddr.Text, rhMNIStatusRecieved) then
    begin
      teIRFWDAddr.Color:=clWhite;

    end
    else
    begin
       teIRFWDAddr.Color:=clRed;
    end;
  end;
  // Switch ZGN
  case StrToInt(cmbSwitch.Text) of
    1:
    begin
      if DataUn.dmDataModule.dsMNI.FieldByName('SwPort1FWDAddr').AsString<> teSWFWDAddr.Text then
      begin
        VariablesUn.ClearData;
        giSentCom := StrToInt(VariablesUn.UID_LS1_ADDR);
        ComFm.SetGenericParam(giUID, VariablesUn.UID_LS1_ADDR+'.'+ teSWFWDAddr.Text, rhMNIStatusRecieved);
        if WaitforGenericResponseConfirmation(giUID, VariablesUn.UID_LS1_ADDR+'.'+ teSWFWDAddr.Text, rhMNIStatusRecieved)then
        begin
          teSWFWDAddr.Color:=clWhite;

        end
        else
        begin
           teSWFWDAddr.Color:=clRed;
        end;
      end;
    end;
    2:
    begin
      if DataUn.dmDataModule.dsMNI.FieldByName('SwPort2FWDAddr').AsString<> teSWFWDAddr.Text then
      begin
        VariablesUn.ClearData;
        giSentCom := StrToInt(VariablesUn.UID_LS2_ADDR);
        ComFm.SetGenericParam(giUID, VariablesUn.UID_LS2_ADDR+'.'+ teSWFWDAddr.Text, rhMNIStatusRecieved);
        if WaitforGenericResponseConfirmation(giUID, VariablesUn.UID_LS2_ADDR+'.'+ teSWFWDAddr.Text, rhMNIStatusRecieved) then
        begin
          teSWFWDAddr.Color:=clWhite;

        end
        else
        begin
           teSWFWDAddr.Color:=clRed;
        end;
      end;
    end;
    3:
    begin
      if DataUn.dmDataModule.dsMNI.FieldByName('SwPort3FWDAddr').AsString<> teSWFWDAddr.Text then
      begin
        VariablesUn.ClearData;
        giSentCom := StrToInt(VariablesUn.UID_LS3_ADDR);
        ComFm.SetGenericParam(giUID, VariablesUn.UID_LS3_ADDR+'.'+ teSWFWDAddr.Text, rhMNIStatusRecieved);
        if WaitforGenericResponseConfirmation(giUID, VariablesUn.UID_LS3_ADDR+'.'+ teSWFWDAddr.Text, rhMNIStatusRecieved) then
        begin
          teSWFWDAddr.Color:=clWhite;

        end
        else
        begin
           teSWFWDAddr.Color:=clRed;
        end;
      end;
    end;
    4:
    begin
      if DataUn.dmDataModule.dsMNI.FieldByName('SwPort4FWDAddr').AsString<> teSWFWDAddr.Text then
      begin
        VariablesUn.ClearData;
        giSentCom := StrToInt(VariablesUn.UID_LS4_ADDR);
        ComFm.SetGenericParam(giUID, VariablesUn.UID_LS4_ADDR+'.'+ teSWFWDAddr.Text, rhMNIStatusRecieved);
        if WaitforGenericResponseConfirmation(giUID, VariablesUn.UID_LS4_ADDR+'.'+ teSWFWDAddr.Text, rhMNIStatusRecieved) then
        begin
          teSWFWDAddr.Color:=clWhite;

        end
        else
        begin
           teSWFWDAddr.Color:=clRed;
        end;
      end;
    end;
  end;

  //IR PORT Personality
  if DataUn.dmDataModule.dsMNI.FieldByName('IRPortPerson').AsInteger<>cmbIRPerson.ItemIndex then
  begin
    VariablesUn.ClearData;
    giSentCom := StrToInt(LeftStr(VariablesUn.MNI_IRPerson,3));
    ComFm.SetGenericParam(giUID, VariablesUn.MNI_IRPerson+'.0.'+IntToStr(cmbIRPerson.ItemIndex), rhMNIStatusRecieved);
    if WaitforGenericResponseConfirmation(giUID, VariablesUn.MNI_IRPerson+'.0.'+IntToStr(cmbIRPerson.ItemIndex), rhMNIStatusRecieved)then
    begin
      shpcmbIRPerson.Brush.Color:=clLime;
      shpcmbIRPerson.Visible:=false;
    end
    else
    begin
      shpcmbIRPerson.Brush.Color:=clRed;
    end;
  end;

  //All Ports Enabled
  intDS:= GetDisabledMotorsIntDS;
  intConfig:= GetDisabledMotorsIntConfig;
  if(intDS<>intConfig) then
  begin
    VariablesUn.ClearData;
    giSentCom := StrToInt(VariablesUn.UID_LSE);
    ComFm.SetGenericParam(giUID, VariablesUn.UID_LSE+'.0.0.'+IntToStr(intConfig), rhMNIStatusRecieved);
    if WaitforGenericResponseConfirmation(giUID, VariablesUn.UID_LSE+'.0.0.'+IntToStr(intConfig), rhMNIStatusRecieved)then
    begin
      shpcbSWEnabled.Brush.color:=clLime;
      shpcbSWEnabled.Visible:=false;
      shpcbIRPortEnabled.Brush.Color:=clLime;
      shpcbIRPortEnabled.Visible:=false;
    end
    else
    begin
      shpcbSWEnabled.Brush.Color:=clRed;
      cbIRPortEnabled.Brush.Color:=clRed;
    end;
  end;

  //All Ports Fwd Enabled
  intDS:= GetEnabledFWDMOIntDS;
  intConfig:= GetEnabledFWDMOIntConfig;
  if(intDS<>intConfig) then
  begin
    VariablesUn.ClearData;
    giSentCom := StrToInt(VariablesUn.UID_LSF);
    ComFm.SetGenericParam(giUID, VariablesUn.UID_LSF+'.0.0.'+IntToStr(intConfig), rhMNIStatusRecieved);
    if WaitforGenericResponseConfirmation(giUID, VariablesUn.UID_LSF+'.0.0.'+IntToStr(intConfig), rhMNIStatusRecieved) then
    begin
      shpcbSWFWDEnabled.Brush.Color:=clLime;
      shpcbSWFWDEnabled.Visible:=false;
      shpcbIRFWDEnabled.Brush.Color:=clLime;
      shpcbIRFWDEnabled.Visible:=false;
    end
    else
    begin
      shpcbIRFWDEnabled.Brush.Color:=clRed;
      shpcbSWFWDEnabled.Brush.Color:=clRed;
    end;
  end;

  //All Ports Mode
  intDS:= GetPortModeIntDS;
  intConfig:= GetPortModeIntConfig;
  if(intDS<>intConfig) then
  begin
    VariablesUn.ClearData;
    giSentCom := StrToInt(VariablesUn.UID_PortMode);
    ComFm.SetGenericParam(giUID, VariablesUn.UID_PortMode+'.0.0.'+IntToStr(intConfig), rhMNIStatusRecieved);
    if WaitforGenericResponseConfirmation(giUID, VariablesUn.UID_PortMode+'.0.0.'+IntToStr(intConfig), rhMNIStatusRecieved)then
    begin
      shpcmbSWMode.Brush.Color:=clLime;
      shpcmbSWMode.Visible:=false;
    end
    else
    begin
      shpcmbSWMode.Brush.Color:=clRed;
    end;
  end;

  //Alias
  if(txtAlias.Text<>DataUn.dmDataModule.dsMNI.FieldByName('Alias').AsString) then
  begin
    AliasText:= txtAlias.text;
    AliasText:=AliasText+ StringOfChar ( Char(#32), 12 - Length(AliasText) );
    setLength(AliasText,12);
    VariablesUn.ClearData;
    giSentCom := StrToInt(VariablesUn.UID_NAME);
    ComFm.SetGenericParam(giUID, VariablesUn.UID_NAME+'.'+AliasText, rhMNIStatusRecieved);
    if WaitforGenericResponseConfirmation(giUID, VariablesUn.UID_NAME+'.'+AliasText, rhMNIStatusRecieved)then
    begin
      txtAlias.Color:=clWhite;
    end
    else
    begin
      txtAlias.Color:=clRed;
    end;
  end
  else
  begin
    txtAlias.Brush.Color:=clWhite;
  end;
  if SingleEditTest then
  begin
    btUpdateParams.Color:=clBtnFace;
  end;
  MNIUpdateParam;
end;

procedure TMNIConfigFm.btUpdateParamsMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
if Sender is TPanel
  then with TPanel(Sender)
       do BevelOuter := bvLowered;
end;

procedure TMNIConfigFm.btUpdateParamsMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
if Sender is TPanel
  then with TPanel(Sender)
       do BevelOuter := bvRaised;
end;
procedure TMNIConfigFm.disableForm(Disable:Boolean);
begin
  if Disable then
  begin
    //self.Enabled:=False;
    sgMap.enabled:=false;
    btupdateParams.enabled:=false;
    btUpdateUID.enabled:=false;
    teUIDMNI.enabled:=false;
    cmbUidMNI.enabled:=false;
    image2.enabled:=false;
    cmbDeviceSelector.enabled:=false;
    txtAlias.enabled:=false;
    cmbProjectMNI.Enabled:=false;
  end
  else
  begin
    //self.Enabled:=True;
    sgMap.enabled:=True;
    btupdateParams.enabled:=True;
    btUpdateUID.enabled:=True;
    teUIDMNI.enabled:=True;
    cmbUidMNI.enabled:=True;
    image2.enabled:=True;
    cmbDeviceSelector.enabled:=True;
    txtAlias.enabled:=True;
    cmbProjectMNI.Enabled:=true;
  end;

end;
procedure TMNIConfigFm.btUpdateUIDClick(Sender: TObject);
var
  I,I2,CMBIndex,DuplicateFlg:Integer;
begin
  if dmDataModule.DuplicateUIDCheck(teUIDmni.Text) then
  begin
    ShowMEssage('Error: UID already exist. Choose Different ID.');
    exit;
  end;
  teUIDmni.OnChange:=nil;
  Screen.Cursor := crHourGlass;
  disableForm(True);
  if teUIDmni.Text<>'' then
  begin
    if(VariablesUn.isnumber(teUIDMNI.text))and(teUIDMNI.text<>IntToStr(LastUID))then
    begin
      giUID := StrToInt(teUIDMNI.Text);
      giSentCom := StrToInt(VariablesUn.UID_ChangeUID);
      ComFm.SetGenericParam(lastUID, VariablesUn.UID_ChangeUID+'.0.'+IntToStr(Hi(StrToInt(teUIDMNI.text)))+'.'+IntToStr(Lo(StrToInt(teUIDMNI.text))), rhMNIStatusRecieved);  //Sets UID
      if WaitforGenericResponseConfirmation(lastUID, VariablesUn.UID_ChangeUID+'.0.'+IntToStr(Hi(StrToInt(teUIDMNI.text)))+'.'+IntToStr(Lo(StrToInt(teUIDMNI.text))), rhMNIStatusRecieved) then
      begin
        teUIDMNI.Color:=clwhite;
      end
      else
      begin
        teUIDMNI.Color:=clRed;
      end;
      btUpdateParams.Color:=clbtnface;
    end;
    cmbUIDMNI.Clear;
    LastUID:=StrToInt(teUIDMNI.text);
    DataUn.dmDataModule.dsMNI.Filtered := False;
    DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsMNI.Filtered := True;
    for I := 0 to DataUn.dmDataModule.dsMNI.RecordCount-1 do
    begin
      cmbUIDMNI.AddItem(IntToStr(DataUn.dmDataModule.dsMNI.FieldByName('UID').AsInteger),Nil);
      DataUn.dmDataModule.dsMNI.Next;
    end;
    cmbUIDMNI.ItemIndex:=cmbUIDMNI.Items.IndexOf(teUIDMNI.text);
  end
  else
  begin
    showMessage('UID Update Fail: No UID Entered!');
    btUpdateParams.Color:=clbtnface;
    teUIDMNI.Color:=clwhite;
  end;
//  cmbUIDMNI.Clear;
//  LastUID:=StrToInt(teUIDMNI.text);
//  DataUn.dmDataModule.dsMNI.Filtered := False;
//  DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
//  DataUn.dmDataModule.dsMNI.Filtered := True;
//  for I := 0 to DataUn.dmDataModule.dsMNI.RecordCount-1 do
//  begin
//    cmbUIDMNI.AddItem(IntToStr(DataUn.dmDataModule.dsMNI.FieldByName('UID').AsInteger),Nil);
//    DataUn.dmDataModule.dsMNI.Next;
//  end;
//  cmbUIDMNI.ItemIndex:=cmbUIDMNI.Items.IndexOf(teUIDMNI.text);

  DisableForm(false);
  teUIDMNI.Clear;
  teUIDmni.OnChange:=teUIDMNIChange;
  self.Enabled:=true;
  Screen.Cursor := crDefault;
end;

procedure TMNIConfigFm.SaveSwMapData(UID:Integer);
var
  UIDCount,I,II:Integer;
  ButtonName,ButtonGroup,ButtonAction,SwButtonInfo:String;
  //UIDList:TComboBox;
begin
  //SwIndex:= SelectSwByID(cmbDeviceID.text);
  //UIDList:=Tstringlist.Create;
  //UIDList.items:=cmbUIDMNI.Items;

  if(Allswitches.Count>0)then
  begin
    if fileexists(VariablesUn.gsDataPath + 'dsSwitch.txt') then
    begin
      DataUn.dmDataModule.dsSwitches.Filtered := False;
      DataUn.dmDataModule.dsSwitches.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName)+' and UID = '+IntToStr(UID);
      DataUn.dmDataModule.dsSwitches.Filtered := True;
      while not dmDataModule.dsSwitches.Eof do
      begin
        dmDataModule.dsSwitches.Edit;
        DataUn.dmDataModule.dsSwitches.Delete;
      end;
    end
    else
    begin
      DataUn.dmDataModule.dsSwitches.CreateDataSet;
      DataUn.dmDataModule.dsSwitches.Active := True;
    end;
//  for UidCount := 0 to cmbUIDMNI.Items.Count-1 do
//  begin

    for I := 0 to Allswitches.Count-1 do
    begin

      SwButtonInfo:='';
      ButtonName:='';
      ButtonAction:='';
      ButtonGroup:='';
      SwButtonInfo:= Allswitches[I].name+','+Allswitches[I].Alpha+',';

      DataUn.dmDataModule.dsSwitches.Insert;
      //DataUn.dmDataModule.dsSwitches.FieldByName('ButtonCount').AsInteger:=  Allswitches[I].ButtonCount;
      //DataUn.dmDataModule.dsSwitches.FieldByName('SwName').AsString:=  Allswitches[I].name;
      DataUn.dmDataModule.dsSwitches.FieldByName('ProjectName').AsString:= VariablesUn.gsProjectName;
      DataUn.dmDataModule.dsSwitches.FieldByName('UID').AsInteger:=UID;
      DataUn.dmDataModule.dsSwitches.FieldByName('DeviceID').AsString:=  Allswitches[I].DeviceID;
      DataUn.dmDataModule.dsSwitches.FieldByName('PreSelect').AsBoolean:=  Allswitches[I].Preselect;

      for II := 1 to Allswitches[I].ButtonCount do
      begin
        if(Allswitches[I].GetButtonIDbyName('Button'+IntToStr(II))<>'') then
        begin
          SwButtonInfo:= SwButtonInfo+Allswitches[I].GetButtonIDbyName('Button'+IntToStr(II))+',';
        end;

        if(Allswitches[I].GetButtonDownbyName('Button'+IntToStr(II))<>'') then
        begin
          SwButtonInfo:=SwButtonInfo+Allswitches[I].GetButtonDownbyName('Button'+IntToStr(II))+',';
        end
        else
        begin
          SwButtonInfo:=SwButtonInfo+',';
        end;

        if(Allswitches[I].GetButtonUpbyName ('Button'+IntToStr(II))<>'') then
        begin
          if II=Allswitches[I].ButtonCount then
          begin
            SwButtonInfo:=  SwButtonInfo+Allswitches[I].GetButtonUpbyName('Button'+IntToStr(II));
          end
          else
          begin
            SwButtonInfo:=  SwButtonInfo+Allswitches[I].GetButtonUpbyName('Button'+IntToStr(II))+',';
          end;
        end
        else
        begin
          if II<>Allswitches[I].ButtonCount then
          begin
            SwButtonInfo:=SwButtonInfo+',';
          end;
        end;

        if(Allswitches[I].GetButtonNamebyName('Button'+IntToStr(II))<>'') then
        begin
          if II=Allswitches[I].ButtonCount then
          begin
            ButtonName:= ButtonName+ Allswitches[I].GetButtonNamebyName('Button'+IntToStr(II));
          end
          else
          begin
            ButtonName:= ButtonName+ Allswitches[I].GetButtonNamebyName('Button'+IntToStr(II))+',';
          end;
        end;

        if(Allswitches[I].GetButtonActionbyName('Button'+IntToStr(II))<>'') then
        begin
          if II=Allswitches[I].ButtonCount then
          begin
            ButtonAction:= ButtonAction+ Allswitches[I].GetButtonActionbyName('Button'+IntToStr(II));
          end
          else
          begin
            ButtonAction:= ButtonAction+ Allswitches[I].GetButtonActionbyName('Button'+IntToStr(II))+',';
          end;
        end
        else
        begin
          ButtonAction:=ButtonAction+',';
        end;

        if(Allswitches[I].GetButtonGroupbyName('Button'+IntToStr(II))<>'') then
        begin
          if II=Allswitches[I].ButtonCount then
          begin
            ButtonGroup:= ButtonGroup+ Allswitches[I].GetButtonGroupbyName('Button'+IntToStr(II));
          end
          else
          begin
            ButtonGroup:= ButtonGroup+ Allswitches[I].GetButtonGroupbyName('Button'+IntToStr(II))+',';
          end;
        end
        else
        begin
          ButtonGroup:=ButtonGroup+',';
        end;

      end;
      DataUn.dmDataModule.dsSwitches.FieldByName('SwButtonData').AsString:=SwButtonInfo;
      DataUn.dmDataModule.dsSwitches.FieldByName('ButtonNames').AsString:=ButtonName;
      DataUn.dmDataModule.dsSwitches.FieldByName('ButtonGroups').AsString:=ButtonGroup;
      DataUn.dmDataModule.dsSwitches.FieldByName('ButtonActions').AsString:=ButtonAction;
      DataUn.dmDataModule.dsSwitches.mergeChangeLog;
    end;


    DataUn.dmDataModule.dsSwitches.SaveToFile(VariablesUn.gsDataPath +'dsSwitch.txt', dfXML);
  end
  else
  begin
     if fileexists(VariablesUn.gsDataPath + 'dsSwitch.txt') then
    begin
      DataUn.dmDataModule.dsSwitches.Filtered := False;
      DataUn.dmDataModule.dsSwitches.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
      DataUn.dmDataModule.dsSwitches.Filtered := True;
      while not dmDataModule.dsSwitches.Eof do
      begin
        dmDataModule.dsSwitches.Edit;
        DataUn.dmDataModule.dsSwitches.Delete;
      end;
    end;
  end;

end;

Procedure TMNIConfigFm.LoadsgMap;
var
  I:Integer;
begin
  cmbDeviceID.Clear;
  cmbDeviceID.Visible:=true;
  for I := 0 to allSwitches.Count-1 do
  begin
    cmbDeviceID.AddItem(allSwitches[I].DeviceID,Nil)
  end;
  cmbDeviceID.ItemIndex:=cmbDeviceID.Items.IndexOf(allSwitches.First.DeviceID);
  sgmap.rowCount:= allSwitches.First.ButtonCount+1;
  for I:=1 to allSwitches.First.ButtonCount do
  begin
    sgmap.Cells[0,I]:=AllSwitches.First.GetButtonNamebyName('Button'+IntToStr(I));
    sgmap.Cells[1,I]:=AllSwitches.First.GetButtonActionbyName('Button'+IntToStr(I));
    sgmap.Cells[2,I]:=AllSwitches.First.GetButtonGroupbyName('Button'+IntToStr(I));
  end;
end;

Function TMNIConfigFm.LoadAllSwitchData:Boolean;
var
  I,I2:Integer;
  SwData,ButtonNames,ButtonGroups,ButtonActions:Tstringlist;
begin

  AllSwitches.Clear;
  SwData:=TStringList.Create();
  ButtonNames:=TStringList.Create();
  ButtonGroups:=TStringList.Create();
  ButtonActions:=TStringList.Create();
  if fileexists(VariablesUn.gsDataPath + 'dsSwitch.txt') then
  begin
    if not DataUn.dmDataModule.dsSwitches.active then
    begin
      DataUn.dmDataModule.dsSwitches.CreateDataSet;
      DataUn.dmDataModule.dsSwitches.active:=true;
    end;
    DataUn.dmDataModule.dsSwitches.LoadFromFile(VariablesUn.gsDataPath +'dsSwitch.txt');
    DataUn.dmDataModule.dsSwitches.Filtered := False;
    DataUn.dmDataModule.dsSwitches.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName)+' and UID = '+cmbUIDMNI.text;
    DataUn.dmDataModule.dsSwitches.Filtered := True;
    //DataUn.dmDataModule.dsSwitches.Active := True;
    //DataUn.dmDataModule.dsSwitches.edit;
    if DataUn.dmDataModule.dsSwitches.RecordCount>0 then
    begin
      for I := 0 to DataUn.dmDataModule.dsSwitches.RecordCount-1 do
      begin
        Split(',',DataUn.dmDataModule.dsSwitches.FieldByName('SwButtonData').AsString,SwData);
        AllSwitches.Add(TSwitchClassUn.Create(SwData,DataUn.dmDataModule.dsSwitches.FieldByName('DeviceID').AsString));
        AllSwitches.Last.DeviceID:=DataUn.dmDataModule.dsSwitches.FieldByName('DeviceID').AsString;
        Split(',',DataUn.dmDataModule.dsSwitches.FieldByName('ButtonNames').AsString,ButtonNames);
        Split(',',DataUn.dmDataModule.dsSwitches.FieldByName('ButtonGroups').AsString,ButtonGroups);
        Split(',',DataUn.dmDataModule.dsSwitches.FieldByName('ButtonActions').AsString,ButtonActions);
        for I2 := 1 to AllSwitches.Last.ButtonCount do
        begin
          AllSwitches.Last.AddButtonGroup('Button'+IntToStr(I2),ButtonGroups[I2-1]);
          AllSwitches.Last.EditButtonName('Button'+IntToStr(I2),ButtonNames[I2-1]);
          AllSwitches.Last.AddButtonAction('Button'+IntToStr(I2),ButtonActions[I2-1]);
        end;
        allSwitches.Last.Preselect:=DataUn.dmDataModule.dsSwitches.FieldByName('PreSelect').AsBoolean;
        DataUn.dmDataModule.dsSwitches.Next;
      end;
      LoadAllSwitchData:=True;
    end
    else
    begin
      LoadAllSwitchData:=False;
    end;


  end;

end;

procedure TMNIConfigFm.btnSerialRemoveSwClick(Sender: TObject);
var
  I,SwIndex:integer;
begin
  if allSwitches.Count>0 then
  begin
    DataUn.dmDataModule.dsSwitches.Filtered := False;
    DataUn.dmDataModule.dsSwitches.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName)+' and UID = '+cmbUIDMNI.text + ' and DeviceID = ' + QuotedStr(cmbDeviceID.text);
    DataUn.dmDataModule.dsSwitches.Filtered := True;
    while not DataUn.dmDataModule.dsSwitches.Eof do
    begin
      DataUn.dmDataModule.dsSwitches.Delete;
      DataUn.dmDataModule.dsSwitches.Next;
    end;
    DataUn.dmDataModule.dsSwitches.mergeChangeLog;
    DataUn.dmDataModule.dsSwitches.SaveToFile(VariablesUn.gsDataPath +'dsSwitch.txt', dfXML);
    SwIndex:= SelectSwByID(cmbDeviceID.text);
    allSwitches.Delete(SwIndex);
    cmbDeviceID.Items.Delete(cmbDeviceID.ItemIndex);
    cmbDeviceID.ItemIndex:=0;
    if cmbDeviceID.Items.Count=0 then
    begin
      cmbDeviceID.Visible:=false;
      for I := 1 to sgMap.RowCount-1 do
      begin
        sgMap.cells[0,I]:='';
        sgMap.cells[1,I]:='';
        sgMap.cells[2,I]:='';
      end;

      sgMap.RowCount:=1;
    end
    else
    begin
      cmbDeviceIDChange(nil);
    end;
  end;
end;

Function TMNIConfigFm.GetDisabledMotorsIntDS:Integer;
var Binary:String;
begin
   if DataUn.dmDataModule.dsMNI.FieldByName('SwPort1Enabled').AsBoolean then
  begin
    Binary:='0';
  end
  else
  begin
    Binary:='1';
  end;
  if DataUn.dmDataModule.dsMNI.FieldByName('SwPort2Enabled').AsBoolean then
  begin
    Binary:=Binary+'0';
  end
  else
  begin
    Binary:=Binary+'1';
  end;
  if DataUn.dmDataModule.dsMNI.FieldByName('SwPort3Enabled').AsBoolean then
  begin
    Binary:=Binary+'0';
  end
  else
  begin
    Binary:=Binary+'1';
  end;
  if DataUn.dmDataModule.dsMNI.FieldByName('SwPort4Enabled').AsBoolean then
  begin
    Binary:=Binary+'0';
  end
  else
  begin
    Binary:=Binary+'1';
  end;

  Binary:=Binary+'11';

  if DataUn.dmDataModule.dsMNI.FieldByName('IRPortEnabled').AsBoolean then
  begin
    Binary:=Binary+'0';
  end
  else
  begin
    Binary:=Binary+'1';
  end;
  if DataUn.dmDataModule.dsMNI.FieldByName('AuxPortEnabled').AsBoolean then
  begin
    Binary:=Binary+'0';
  end
  else
  begin
    Binary:=Binary+'1';
  end;
  GetDisabledMotorsIntDS:=VariablesUn.BinToInt(ansireversestring(Binary));
end;

Function TMNIConfigFm.GetDisabledMotorsIntConfig:Integer;
var Binary:String;
begin
  if cmbSwitch.Text='1' then
  begin
    if(cbSWEnabled.checked)then
    begin
      Binary:='0';
    end
    else
    begin
      Binary:='1';
    end;
  end
  else
  begin
    if DataUn.dmDataModule.dsMNI.FieldByName('SwPort1Enabled').AsBoolean then
    begin
      Binary:='0';
    end
    else
    begin
      Binary:='1';
    end;
  end;
  if cmbSwitch.Text='2' then
  begin
    if(cbSWEnabled.checked)then
    begin
      Binary:=Binary+'0';
    end
    else
    begin
      Binary:=Binary+'1';
    end;
  end
  else
  begin
    if DataUn.dmDataModule.dsMNI.FieldByName('SwPort2Enabled').AsBoolean then
    begin
      Binary:=Binary+'0';
    end
    else
    begin
      Binary:=Binary+'1';
    end;
  end;
  if cmbSwitch.Text='3' then
  begin
    if(cbSWEnabled.checked)then
    begin
      Binary:=Binary+'0';
    end
    else
    begin
      Binary:=Binary+'1';
    end;
  end
  else
  begin
    if DataUn.dmDataModule.dsMNI.FieldByName('SwPort3Enabled').AsBoolean then
    begin
      Binary:=Binary+'0';
    end
    else
    begin
      Binary:=Binary+'1';
    end;
  end;
  if cmbSwitch.Text='4' then
  begin
    if(cbSWEnabled.checked)then
    begin
      Binary:=Binary+'0';
    end
    else
    begin
      Binary:=Binary+'1';
    end;
  end
  else
  begin
    if DataUn.dmDataModule.dsMNI.FieldByName('SwPort4Enabled').AsBoolean then
    begin
      Binary:=Binary+'0';
    end
    else
    begin
      Binary:=Binary+'1';
    end;
  end;
  Binary:=Binary+'11';

  if cbIRPortEnabled.checked then
  begin
    Binary:=Binary+'0';
  end
  else
  begin
    Binary:=Binary+'1';
  end;
  if DataUn.dmDataModule.dsMNI.FieldByName('AuxPortEnabled').AsBoolean then
  begin
    Binary:=Binary+'0';
  end
  else
  begin
    Binary:=Binary+'1';
  end;
  GetDisabledMotorsIntConfig:=VariablesUn.BinToInt(ansireversestring(Binary))
end;

Function TMNIConfigFm.GetEnabledFWDMOIntConfig:Integer;
var Binary:String;
begin
  if cmbSwitch.Text='1' then
  begin
    if(cbSWFWDEnabled.checked)then
    begin
      Binary:='1';
    end
    else
    begin
      Binary:='0';
    end;
  end
  else
  begin
    if DataUn.dmDataModule.dsMNI.FieldByName('SwPort1FWDEnabled').AsBoolean then
    begin
      Binary:='1';
    end
    else
    begin
      Binary:='0';
    end;
  end;
  if cmbSwitch.Text='2' then
  begin
    if(cbSWFWDEnabled.checked)then
    begin
      Binary:=Binary+'1';
    end
    else
    begin
      Binary:=Binary+'0';
    end;
  end
  else
  begin
    if DataUn.dmDataModule.dsMNI.FieldByName('SwPort2FWDEnabled').AsBoolean then
    begin
      Binary:=Binary+'1';
    end
    else
    begin
      Binary:=Binary+'0';
    end;
  end;
  if cmbSwitch.Text='3' then
  begin
    if(cbSWFWDEnabled.checked)then
    begin
      Binary:=Binary+'1';
    end
    else
    begin
      Binary:=Binary+'0';
    end;
  end
  else
  begin
    if DataUn.dmDataModule.dsMNI.FieldByName('SwPort3FWDEnabled').AsBoolean then
    begin
      Binary:=Binary+'1';
    end
    else
    begin
      Binary:=Binary+'0';
    end;
  end;
  if cmbSwitch.Text='4' then
  begin
    if(cbSWFWDEnabled.checked)then
    begin
      Binary:=Binary+'1';
    end
    else
    begin
      Binary:=Binary+'0';
    end;
  end
  else
  begin
    if DataUn.dmDataModule.dsMNI.FieldByName('SwPort4FWDEnabled').AsBoolean then
    begin
      Binary:=Binary+'1';
    end
    else
    begin
      Binary:=Binary+'0';
    end;
  end;
  Binary:=Binary+'00';

  if cbIRFWDEnabled.checked then
  begin
    Binary:=Binary+'1';
  end
  else
  begin
    Binary:=Binary+'0';
  end;
  if DataUn.dmDataModule.dsMNI.FieldByName('AuxPortFWDEnabled').AsBoolean then
  begin
    Binary:=Binary+'1';
  end
  else
  begin
    Binary:=Binary+'0';
  end;
  GetEnabledFWDMOIntConfig:=VariablesUn.BinToInt(ansireversestring(Binary));
end;

Function TMNIConfigFm.GetEnabledFWDMOIntDS:Integer;
var Binary:String;
begin
  if DataUn.dmDataModule.dsMNI.FieldByName('SwPort1FWDEnabled').AsBoolean then
  begin
    Binary:='1';
  end
  else
  begin
    Binary:='0';
  end;
  if DataUn.dmDataModule.dsMNI.FieldByName('SwPort2FWDEnabled').AsBoolean then
  begin
    Binary:=Binary+'1';
  end
  else
  begin
    Binary:=Binary+'0';
  end;
  if DataUn.dmDataModule.dsMNI.FieldByName('SwPort3FWDEnabled').AsBoolean then
  begin
    Binary:=Binary+'1';
  end
  else
  begin
    Binary:=Binary+'0';
  end;
  if DataUn.dmDataModule.dsMNI.FieldByName('SwPort4FWDEnabled').AsBoolean then
  begin
    Binary:=Binary+'1';
  end
  else
  begin
    Binary:=Binary+'0';
  end;
  Binary:=Binary+'00';
  if DataUn.dmDataModule.dsMNI.FieldByName('IRPortFWDEnabled').AsBoolean then
  begin
    Binary:=Binary+'1';
  end
  else
  begin
    Binary:=Binary+'0';
  end;
  if DataUn.dmDataModule.dsMNI.FieldByName('AuxPortFWDEnabled').AsBoolean then
  begin
    Binary:=Binary+'1';
  end
  else
  begin
    Binary:=Binary+'0';
  end;
  GetEnabledFWDMOIntDS:=VariablesUn.BinToInt(ansireversestring(Binary));
end;

Function TMNIConfigFm.GetPortModeIntConfig:Integer;
var Binary:String;
begin
  if cmbSwitch.Text='1' then
  begin
    Binary:= IntToStr(cmbSWMode.ItemIndex);
  end
  else
  begin
    Binary:=IntToStr(DataUn.dmDataModule.dsMNI.FieldByName('SwPort1Mode').AsInteger);
  end;
  if cmbSwitch.Text='2' then
  begin
    Binary:= Binary+IntToStr(cmbSWMode.ItemIndex);
  end
  else
  begin
    Binary:=Binary+IntToStr(DataUn.dmDataModule.dsMNI.FieldByName('SwPort2Mode').AsInteger);
  end;
  if cmbSwitch.Text='3' then
  begin
    Binary:= Binary+IntToStr(cmbSWMode.ItemIndex);
  end
  else
  begin
    Binary:=Binary+IntToStr(DataUn.dmDataModule.dsMNI.FieldByName('SwPort3Mode').AsInteger);
  end;
  if cmbSwitch.Text='4' then
  begin
    Binary:= Binary+IntToStr(cmbSWMode.ItemIndex);
  end
  else
  begin
    Binary:=Binary+IntToStr(DataUn.dmDataModule.dsMNI.FieldByName('SwPort4Mode').AsInteger);
  end;
  Binary:=Binary+'00';
  Binary:= Binary+IntToStr(cmbIRMode.ItemIndex);
  Binary:=Binary+IntToStr(DataUn.dmDataModule.dsMNI.FieldByName('AuxPortMode').AsInteger);
  GetPortModeIntConfig:=VariablesUn.BinToInt(ansireversestring(Binary));
end;

Function TMNIConfigFm.GetPortModeIntDS:Integer;
var Binary:String;
begin
  Binary:=IntToStr( DataUn.dmDataModule.dsMNI.FieldByName('SwPort1Mode').AsInteger);
  Binary:=Binary+IntToStr( DataUn.dmDataModule.dsMNI.FieldByName('SwPort2Mode').AsInteger);
  Binary:=Binary+IntToStr( DataUn.dmDataModule.dsMNI.FieldByName('SwPort3Mode').AsInteger);
  Binary:=Binary+IntToStr( DataUn.dmDataModule.dsMNI.FieldByName('SwPort4Mode').AsInteger);
  Binary:=Binary+'00';
  Binary:=Binary+IntToStr( DataUn.dmDataModule.dsMNI.FieldByName('IRPortMode').AsInteger);
  Binary:=Binary+IntToStr( DataUn.dmDataModule.dsMNI.FieldByName('AuxPortMode').AsInteger);
  GetPortModeIntDS:=VariablesUn.BinToInt(ansireversestring(Binary));
end;

procedure TMNIConfigFm.WaitforGenericResponse(UID:Integer;Command:String;rh:TNotifyEvent);
var
  PauseCount:Integer;
begin
  passFlag:=false;
  PauseCount:=0;
  while not PassFlag do
  begin
      PauseCount:=PauseCount+1;
      delay(10);
      if (PauseCount=100) or (PauseCount=200) then
        ComFm.GetGenericStatus(UID,Command,rh);
      if PauseCount>300 then
      begin

        ShowMessage('Failed Command');
        Break;
      end;
  end;
end;

Function TMNIConfigFm.WaitforGenericResponseConfirmation(UID:Integer;Command:String;rh:TNotifyEvent):Boolean;
var
  PauseCount:Integer;
begin
  passFlag:=false;
  PauseCount:=0;
  while not PassFlag do
  begin
      PauseCount:=PauseCount+1;
      delay(10);
      if (PauseCount=100) or (PauseCount=200) then
        ComFm.GetGenericStatus(UID,Command,rh);
      if PauseCount>300 then
      begin
        Result:=false;
        ShowMessage('Failed Command');
        Exit;
      end;
  end;
  Result:=True;
end;

procedure TMNIConfigFm.cmbDeviceIDChange(Sender: TObject);
var
  I,SwIndex:Integer;
begin
  //sgMap.RowCount:=1;
  SwIndex:= SelectSwByID(cmbDeviceID.text);
  sgMap.col:=0;
  sgMap.Row:=1;
  sgMap.RowCount:= AllSwitches[SwIndex].ButtonCount+1;
  for I := 1 to AllSwitches[SwIndex].ButtonCount do
  Begin
    sgMap.Cells[0,I]:='Button'+IntToStr(I);
  End;
  if AllSwitches[SwIndex].ButtonActionCount>0 then
  begin
    for I := 1 to sgMap.RowCount-1 do
    begin
//    for I := 1 to AllSwitches[SwIndex].ButtonActionCount do
//    begin
      if(AllSwitches[SwIndex].GetButtonActionbyName('Button'+IntToStr(I))<>'') then
      begin
        sgMap.Cells[1,I]:=AllSwitches[SwIndex].GetButtonActionbyName('Button'+IntToStr(I));
      end
      else
      begin
        sgMap.Cells[1,I]:='';
      end;
    end;
//    for I := 1 to AllSwitches[SwIndex].ButtonActionCount do
//    Begin
//      sgMap.Cells[1,I]:='Button'+IntToStr(I);
//    End;
  end
  else
  begin
    for I := 1 to AllSwitches[SwIndex].ButtonCount do
      Begin
        sgMap.Cells[1,I]:='';
      End;
  end;
  if AllSwitches[SwIndex].ButtonGroupCount>0 then
  begin
    for I := 1 to sgMap.RowCount-1 do
    begin
      if(AllSwitches[SwIndex].GetButtonGroupByName('Button'+IntToStr(I))<>'') then
      begin
        sgMap.Cells[2,I]:=AllSwitches[SwIndex].GetButtonGroupByName('Button'+IntToStr(I));
      end
      else
      begin
        sgMap.Cells[2,I]:='';
      end;
    end;
  end
  else
  begin
    for I := 1 to AllSwitches[SwIndex].ButtonCount do
      Begin
        sgMap.Cells[2,I]:='';
      End;
  end;
  for I := 1 to sgMap.RowCount-1 do
  begin
    sgMap.Cells[0,I]:=AllSwitches[SwIndex].GetButtonNamebyName('Button'+IntToStr(I));
  end;

end;

Function TMNIConfigFm.SelectSwByID(ID:String):Integer;
var
  I:Integer;
begin
  for I := 0 to AllSwitches.Count-1 do
  begin
    if(AllSwitches[I].DeviceID=ID)then
    begin
      SelectSwByID:=I;
      break;
    end;
  end;
end;

procedure TMNIConfigFm.cmbDeviceSelectorChange(Sender: TObject);
var
  I:Integer;
begin
  VariablesUn.giTop := Top;
  VariablesUn.giLeft := Left;
  if(cmbDeviceSelector.Text='MWC')then
  begin

    DataUn.dmDataModule.dsMWCChannels.Filtered := False;
    DataUn.dmDataModule.dsMWCChannels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsMWCChannels.Filtered := True;

    if not dmDataModule.dsMWCChannels.Eof then
    begin

      VariablesUn.giTop:=top;
      variablesUn.giLeft:=Left;
      Hide;
      MWCDisplayfm.show;
    end
    else
    begin
      ShowMessage('No MWC Devices Found');
      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('MNI');
    end;
  end
  else if (cmbDeviceSelector.Text='iQ3-DC-RF') then
    begin
      DataUn.dmDataModule.dsIQ3s.Filtered := False;
      DataUn.dmDataModule.dsIQ3s.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
      DataUn.dmDataModule.dsIQ3s.Filtered := True;

      if not dmDataModule.dsIQ3s.Eof then
      begin

        VariablesUn.giTop:=top;
        variablesUn.giLeft:=Left;
        IQ3RFDisplayfm.Show;
        Hide;
        //MWCConfigfm.Show;
      end
      else
      begin
        ShowMessage('No iQ3 Devices Found');
        cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('MTR');
      end;
    end
  else if(cmbDeviceSelector.Text='MTR')then
  begin
    VariablesUn.giTop:=top;
    variablesUn.giLeft:=Left;
    if Assigned (rh_ShowMain) then
      rh_ShowMain(Self);
    Hide;
    VariablesUn.PassLoopFlag:=True;
  end
  else if(cmbDeviceSelector.Text='MDC')then
  begin
    DataUn.dmDataModule.dsMNI2.Filtered := False;
    DataUn.dmDataModule.dsMNI2.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsMNI2.Filtered := True;

    if not dmDataModule.dsMNI2.Eof then
    begin
      MNIConfigFm.Hide;
      MNI2configFm.Show;
      VariablesUn.giTop := Top;
      VariablesUn.giLeft := Left;
      MNI2ConfigFm.GetVirtualMotorsStatus(self);
    end
    else
    begin
      ShowMessage('No MDC Devices Found');
      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('MNI');
    end;
  end
  else if(cmbDeviceSelector.Text='IQMLC2')then
  begin
    DataUn.dmDataModule.dsIQMLC2.Filtered := False;
    DataUn.dmDataModule.dsIQMLC2.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsIQMLC2.Filtered := True;

    if not dmDataModule.dsIQMLC2.Eof then
    begin
      Hide;
      VariablesUn.giTop := Top;
      VariablesUn.giLeft := Left;
      if Assigned (rh_showIQMLC2Config) then
        rh_showIQMLC2Config(self);
      //IQMLC2configFm.Show;
    end
    else
    begin
      ShowMessage('No IQMLC2 Devices Found');
      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('MNI');
    end;
  end
  else if(cmbDeviceSelector.Text='PowerPanel')then
  begin
    DataUn.dmDataModule.dsPowerPanel.Filtered := False;
    DataUn.dmDataModule.dsPowerPanel.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsPowerPanel.Filtered := True;

    if not dmDataModule.dsPowerPanel.Eof then
    begin
      Hide;
      VariablesUn.giTop := Top;
      VariablesUn.giLeft := Left;
      PowerPanelConfigFm.Show;
    end
    else
    begin
      ShowMessage('No IQ2-DC Power Panel Devices Found');
      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('MNI');
    end;

  end
  else if(cmbDeviceSelector.Text='Motor Maint')then
  begin
    DataUn.dmDataModule.dsMotors.Filtered := False;
    DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) + ' and MotorType = 69 or MotorType = 66';
    DataUn.dmDataModule.dsMotors.Filtered := True;

    if not dmDataModule.dsMotors.Eof then
    begin
      VariablesUn.giTop := Top;
      VariablesUn.giLeft := Left;
      Hide;
      MotorMaintenanceDisplayFm.Show;
      MotorMaintenanceDisplayFm.cmbUidMM.ItemIndex:= 0;
      MotorMaintenanceDisplayFm.cmbUidMM.OnChange(self);
    end
    else
    begin
      ShowMessage('No Motors Found');
      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('MNI');
    end;
  end;
end;

procedure TMNIConfigFm.cmbIRModeChange(Sender: TObject);
begin
  shpcmbIRMode.Visible:=true;
  btUpdateParams.Color:=clLime;
end;

procedure TMNIConfigFm.cmbIRPersonChange(Sender: TObject);
begin
  shpcmbIRPerson.Visible:=true;
  btUpdateParams.Color:=clLime;
end;

Function TMNIConfigFm.SingleEditTest:boolean;
begin
  if (shpcbSerialEnabled.visible)then
  begin
    SingleEditTest:=false;
  end
  else if (shpcmbSerialDevices.visible) then
  begin
    SingleEditTest:=false;
  end
  else if (shpcmbSerialPerson.visible) then
  begin
    SingleEditTest:=false;
  end
  else if (shpcmbSerialProtocol.visible) then
  begin
    SingleEditTest:=false;
  end
  else if (shpcmbSerialBaud.visible) then
  begin
    SingleEditTest:=false;
  end
  else if (shpcbSerialReceive.visible) then
  begin
    SingleEditTest:=false;
  end
  else if (shpcbSerialTransmit.visible) then
  begin
    SingleEditTest:=false;
  end
  else if (shpcbSerialUIDinZGN.visible)then
  begin
    SingleEditTest:=false;
  end
  else if (shpcbSerialRoutingField.visible) then
  begin
    SingleEditTest:=false;
  end
  else if (shpcbSerialMSGChkSum.visible) then
  begin
    SingleEditTest:=false;
  end
  else if (shpcbIRPortEnabled.visible) then
  begin
    SingleEditTest:=false;
  end
  else if (shpcmbIRPerson.visible) then
  begin
    SingleEditTest:=false;
  end
  else if (shpcbIRFWDEnabled.visible)then
  begin
    SingleEditTest:=false;
  end
  else if (shpcmbIRMode.visible) then
  begin
    SingleEditTest:=false;
  end
  else if (shpcbSWEnabled.visible)then
  begin
    SingleEditTest:=false;
  end
  else if (shpcmbSWPerson.visible) then
  begin
    SingleEditTest:=false;
  end
  else if (shpcbSWFWDEnabled.visible) then
  begin
    SingleEditTest:=false;
  end
  else if (shpcmbSWMode.visible) then
  begin
    SingleEditTest:=false;
  end
  else if (teSWFWDAddr.Brush.Color=cllime) then
  begin
    SingleEditTest:=false;
  end
  else if (teIRFWDAddr.Brush.Color=cllime) then
  begin
    SingleEditTest:=false;
  end
  else
  begin
    SingleEditTest:=true;
  end;
end;

Procedure TMNIConfigFm.ClearEdits;
begin

  shpcbSerialEnabled.visible:=False;
  shpcmbSerialDevices.visible:=False;
  shpcmbSerialPerson.visible:=False;
  shpcmbSerialProtocol.visible:=False;
  shpcmbSerialBaud.visible:=False;
  shpcbSerialReceive.visible:=False;
  shpcbSerialTransmit.visible:=False;
  shpcbSerialUIDinZGN.visible:=False;
  shpcbSerialRoutingField.visible:=False;
  shpcbSerialMSGChkSum.visible:=False;
  shpcbIRPortEnabled.visible:=False;
  shpcmbIRPerson.visible:=False;
  shpcbIRFWDEnabled.visible:=False;
  shpcmbIRMode.visible:=False;
  shpcbSWEnabled.visible:=False;
  shpcmbSWPerson.visible:=False;
  shpcbSWFWDEnabled.visible:=False;
  shpcmbSWMode.visible:=False;


  shpcbSerialEnabled.Brush.Color:=clLime;
  shpcmbSerialDevices.Brush.Color:=clLime;
  shpcmbSerialPerson.Brush.Color:=clLime;
  shpcmbSerialProtocol.Brush.Color:=clLime;
  shpcmbSerialBaud.Brush.Color:=clLime;
  shpcbSerialReceive.Brush.Color:=clLime;
  shpcbSerialTransmit.Brush.Color:=clLime;
  shpcbSerialUIDinZGN.Brush.Color:=clLime;
  shpcbSerialRoutingField.Brush.Color:=clLime;
  shpcbSerialMSGChkSum.Brush.Color:=clLime;
  shpcbIRPortEnabled.Brush.Color:=clLime;
  shpcmbIRPerson.Brush.Color:=clLime;
  shpcbIRFWDEnabled.Brush.Color:=clLime;
  shpcmbIRMode.Brush.Color:=clLime;
  shpcbSWEnabled.Brush.Color:=clLime;
  shpcmbSWPerson.Brush.Color:=clLime;
  shpcbSWFWDEnabled.Brush.Color:=clLime;
  shpcmbSWMode.Brush.Color:=clLime;
  teSWFWDAddr.Brush.Color:=clWhite;
  teIRFWDAddr.Brush.Color:=clWhite;
  btUpdateParams.Color:=clBtnFace;
end;

procedure TMNIConfigFm.cmbProjectMNIChange(Sender: TObject);
begin
  DataUn.dmDataModule.dsMNI.Filtered := False;
  DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUIDMNI.Text;
  DataUn.dmDataModule.dsMNI.Filtered := True;

  if not (DataUn.dmDataModule.dsMNI.Eof) then
  begin
    DataUn.dmDataModule.dsMNI.Edit;
    DataUn.dmDataModule.dsMNI.FieldByName('Alias').AsString := txtAlias.Text;
    VariablesUn.gsAlias := txtAlias.Text;
    DataUn.dmDataModule.dsMNI.Post;
    DataUn.dmDataModule.dsMNI.MergeChangeLog;
  end;
  DataUn.dmDataModule.dsMNI.Filtered := False;
  DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
  DataUn.dmDataModule.dsMNI.Filtered := True;
  loadUID;

end;

procedure TMNIConfigFm.cmbSerialBaudChange(Sender: TObject);
begin
  shpcmbSerialBaud.Visible:=true;
  btUpdateParams.Color:=clLime;
end;

procedure TMNIConfigFm.cmbSerialDevicesChange(Sender: TObject);
begin
  if (cmbSerialDevices.Text='Lutron QS') then
  begin
    cmbSerialPerson.ItemIndex:=cmbSerialPerson.Items.IndexOf('MapIn');
    cmbSerialProtocol.ItemIndex:=cmbSerialProtocol.Items.IndexOf('RS-232-TIA');
    cmbSerialBaud.ItemIndex:=cmbSerialBaud.Items.IndexOf('9600');
    cbSerialReceive.Checked:=true;
    cbSerialTransmit.Checked:=False;
    cbSerialUIDinZGN.Checked:=False;
    cbSerialRoutingField.Checked:=False;
    cbSerialMSGChkSum.Checked:=False;
    pnlSerialSettings.Enabled:=False;
    pnlSerialSettings.Visible:=True;

  end
  else if (cmbSerialDevices.Text='Somfy RTS') then
  begin
    cmbSerialPerson.ItemIndex:=cmbSerialPerson.Items.IndexOf('SomfyRTS');
    cmbSerialProtocol.ItemIndex:=cmbSerialProtocol.Items.IndexOf('RS-232-TIA');
    cmbSerialBaud.ItemIndex:=cmbSerialBaud.Items.IndexOf('9600');
    cbSerialReceive.Checked:=true;
    cbSerialTransmit.Checked:=False;
    cbSerialUIDinZGN.Checked:=False;
    cbSerialRoutingField.Checked:=False;
    cbSerialMSGChkSum.Checked:=False;
    pnlSerialSettings.Enabled:=False;
    pnlSerialSettings.Visible:=True;

  end
  else if (cmbSerialDevices.Text='Watt Stopper') then
  begin
    cmbSerialPerson.ItemIndex:=cmbSerialPerson.Items.IndexOf('MapInOut');
    cmbSerialProtocol.ItemIndex:=cmbSerialProtocol.Items.IndexOf('RS-232-TIA');
    cmbSerialBaud.ItemIndex:=cmbSerialBaud.Items.IndexOf('19200');
    cbSerialReceive.Checked:=true;
    cbSerialTransmit.Checked:=true;
    cbSerialUIDinZGN.Checked:=False;
    cbSerialRoutingField.Checked:=False;
    cbSerialMSGChkSum.Checked:=False;
    pnlSerialSettings.Enabled:=False;
    pnlSerialSettings.Visible:=True;

  end
  else if (cmbSerialDevices.Text='Default') then
  begin
    cmbSerialPerson.ItemIndex:=cmbSerialPerson.Items.IndexOf('MN-Floor');
    cmbSerialProtocol.ItemIndex:=cmbSerialProtocol.Items.IndexOf('RS-232-TIA');
    cmbSerialBaud.ItemIndex:=cmbSerialBaud.Items.IndexOf('19200');
    cbSerialReceive.Checked:=true;
    cbSerialTransmit.Checked:=true;
    cbSerialUIDinZGN.Checked:=False;
    cbSerialRoutingField.Checked:=False;
    cbSerialMSGChkSum.Checked:=False;
    pnlSerialSettings.Enabled:=False;
    pnlSerialSettings.Visible:=True;

  end
  else if (cmbSerialDevices.Text='Mechonet Repeater') then
  begin
    cmbSerialPerson.ItemIndex:=cmbSerialPerson.Items.IndexOf('MN-Floor');
    cmbSerialProtocol.ItemIndex:=cmbSerialProtocol.Items.IndexOf('RS-485');
    cmbSerialBaud.ItemIndex:=cmbSerialBaud.Items.IndexOf('9600');
    cbSerialReceive.Checked:=true;
    cbSerialTransmit.Checked:=true;
    cbSerialUIDinZGN.Checked:=True;
    cbSerialRoutingField.Checked:=False;
    cbSerialMSGChkSum.Checked:=True;
    pnlSerialSettings.Enabled:=False;
    pnlSerialSettings.Visible:=True;

  end
  else if (cmbSerialDevices.Text='Somfy SDN1') then
  begin
    cmbSerialPerson.ItemIndex:=cmbSerialPerson.Items.IndexOf('SDN1');
    cmbSerialProtocol.ItemIndex:=cmbSerialProtocol.Items.IndexOf('RS-485');
    cmbSerialBaud.ItemIndex:=cmbSerialBaud.Items.IndexOf('4800');
    cbSerialReceive.Checked:=true;
    cbSerialTransmit.Checked:=true;
    cbSerialUIDinZGN.Checked:=False;
    cbSerialRoutingField.Checked:=False;
    cbSerialMSGChkSum.Checked:=true;
    pnlSerialSettings.Enabled:=false;
    pnlSerialSettings.Visible:=True;

  end
  else if (cmbSerialDevices.Text='Somfy SDN2') then
  begin
    cmbSerialPerson.ItemIndex:=cmbSerialPerson.Items.IndexOf('SDN2');
    cmbSerialProtocol.ItemIndex:=cmbSerialProtocol.Items.IndexOf('RS-485');
    cmbSerialBaud.ItemIndex:=cmbSerialBaud.Items.IndexOf('4800');
    cbSerialReceive.Checked:=true;
    cbSerialTransmit.Checked:=true;
    cbSerialUIDinZGN.Checked:=False;
    cbSerialRoutingField.Checked:=False;
    cbSerialMSGChkSum.Checked:=true;
    pnlSerialSettings.Enabled:=false;
    pnlSerialSettings.Visible:=True;

  end
  else if (cmbSerialDevices.Text='Custom') then
  begin
    //cmbSerialPerson.ItemIndex:=cmbSerialPerson.Items.IndexOf('MechoNet');
    pnlSerialSettings.Enabled:=true;
    pnlSerialSettings.Visible:=True;
  end
  else if (cmbSerialDevices.Text='None') then
  begin
    pnlSerialSettings.Visible:=False;
  end;
  shpcmbSerialDevices.Visible:=true;
  btUpdateParams.Color:=clLime;
end;

procedure TMNIConfigFm.cmbSerialPersonChange(Sender: TObject);
begin
//  if(cmbSerialPerson.text='MapIn')or(cmbSerialPerson.text='MapOut')or(cmbSerialPerson.text='MapInOut')then
//  begin
//    pcProtocolParams.ActivePage :=tsMap;
//  end
//  else
//  begin
//    pcProtocolParams.ActivePage :=tsVirtualMotors;
//  end;
  shpcmbSerialPerson.Visible:=true;
  btUpdateParams.Color:=clLime;
end;

procedure TMNIConfigFm.cmbSerialProtocolChange(Sender: TObject);
begin
  shpcmbSerialProtocol.Visible:=true;
  btUpdateParams.Color:=clLime;
end;

procedure TMNIConfigFm.FindSerialProfile;
begin
  DataUn.dmDataModule.dsMNI.Filtered := False;
  DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMNI.Text;
  DataUn.dmDataModule.dsMNI.Filtered := True;

  if (cmbSerialProtocol.text='RS-232-TIA')and(cmbSerialBaud.text='9600')and
  (cbSerialReceive.Checked=true)and(cbSerialTransmit.Checked=False)and(cbSerialUIDinZGN.Checked=False)and
  (cbSerialRoutingField.Checked=False)and(cbSerialMSGChkSum.Checked=False)and
  (DataUn.dmDataModule.dsMNI.FieldByName('SerialPerson').AsInteger=3) then
  begin
    cmbSerialDevices.itemindex:=cmbSerialDevices.items.Indexof('Lutron QS');
  end
  else if (cmbSerialProtocol.text='RS-232-TIA')and(cmbSerialBaud.text='9600')and
  (cbSerialReceive.Checked=true)and(cbSerialTransmit.Checked=False)and(cbSerialUIDinZGN.Checked=False)and
  (cbSerialRoutingField.Checked=False)and(cbSerialMSGChkSum.Checked=False)and
  (DataUn.dmDataModule.dsMNI.FieldByName('SerialPerson').AsInteger=7)then
  begin
    cmbSerialDevices.itemindex:=cmbSerialDevices.items.Indexof('Somfy RTS');
  end
  else if(cmbSerialProtocol.text='RS-232-TIA')and(cmbSerialBaud.text='19200')and
  (cbSerialReceive.Checked=true)and(cbSerialTransmit.Checked=true)and(cbSerialUIDinZGN.Checked=False)and
  (cbSerialRoutingField.Checked=False)and(cbSerialMSGChkSum.Checked=False)and
  (DataUn.dmDataModule.dsMNI.FieldByName('SerialPerson').AsInteger=5)then
  begin
    cmbSerialDevices.itemindex:=cmbSerialDevices.items.Indexof('Watt Stopper');
  end
  else if(cmbSerialProtocol.text='RS-232-TIA')and(cmbSerialBaud.text='19200')and
  (cbSerialReceive.Checked=true)and(cbSerialTransmit.Checked=true)and(cbSerialUIDinZGN.Checked=False)and
  (cbSerialRoutingField.Checked=False)and(cbSerialMSGChkSum.Checked=False)and
  (DataUn.dmDataModule.dsMNI.FieldByName('SerialPerson').AsInteger=1) then
  begin
    cmbSerialDevices.itemindex:=cmbSerialDevices.items.Indexof('Default');
  end
  else if (cmbSerialProtocol.text='RS-485')and(cmbSerialBaud.text='9600')and
  (cbSerialReceive.Checked=true)and(cbSerialTransmit.Checked=true)and(cbSerialUIDinZGN.Checked=False)and
  (cbSerialRoutingField.Checked=False)and(cbSerialMSGChkSum.Checked=False)and
  (DataUn.dmDataModule.dsMNI.FieldByName('SerialPerson').AsInteger=1) then
  begin
    cmbSerialDevices.itemindex:=cmbSerialDevices.items.Indexof('Mechonet Repeater');
  end
  else if (cmbSerialProtocol.text='RS-485')and(cmbSerialBaud.text='4800')and
  (cbSerialReceive.Checked=true)and(cbSerialTransmit.Checked=true)and(cbSerialUIDinZGN.Checked=False)and
  (cbSerialRoutingField.Checked=False)and(cbSerialMSGChkSum.Checked=true)and
  (DataUn.dmDataModule.dsMNI.FieldByName('SerialPerson').AsInteger=8)then
  begin
    cmbSerialDevices.itemindex:=cmbSerialDevices.items.Indexof('Somfy SDN1');
  end
  else if (cmbSerialProtocol.text='RS-485')and(cmbSerialBaud.text='4800')and
  (cbSerialReceive.Checked=true)and(cbSerialTransmit.Checked=true)and(cbSerialUIDinZGN.Checked=False)and
  (cbSerialRoutingField.Checked=False)and(cbSerialMSGChkSum.Checked=true)and
  (DataUn.dmDataModule.dsMNI.FieldByName('SerialPerson').AsInteger=9) then
  begin
    cmbSerialDevices.itemindex:=cmbSerialDevices.items.Indexof('Somfy SDN2');
  end
  else if(DataUn.dmDataModule.dsMNI.FieldByName('SerialPerson').AsInteger=0)then
  begin
    cmbSerialDevices.itemindex:=cmbSerialDevices.items.Indexof('None');
  end
  else
  begin
    cmbSerialDevices.itemindex:=cmbSerialDevices.items.Indexof('Custom');
  end;

end;

procedure TMNIConfigFm.cmbSwitchChange(Sender: TObject);
begin
  DataUn.dmDataModule.dsMNI.Filtered := False;
  DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMNI.Text;
  DataUn.dmDataModule.dsMNI.Filtered := True;
  case StrToInt(cmbSwitch.Text) of
    1:
    begin
      cbSWEnabled.Checked:= DataUn.dmDataModule.dsMNI.FieldByName('SwPort1Enabled').AsBoolean;
      cbSWFWDEnabled.Checked:= DataUn.dmDataModule.dsMNI.FieldByName('SwPort1FWDEnabled').AsBoolean;
      if DataUn.dmDataModule.dsMNI.FieldByName('SW1BtnPerson').AsInteger<>-1 then
      begin
      case DataUn.dmDataModule.dsMNI.FieldByName('SW1BtnPerson').AsInteger of
        9:
        Begin
          cmbSWPerson.ItemIndex:= 0;
        End;
        17:
        Begin
          cmbSWPerson.ItemIndex:=1;
        End;
        33:
        Begin
          cmbSWPerson.ItemIndex:=2;
        End;
        18:
        Begin
          cmbSWPerson.ItemIndex:=3;
        End;
        34:
        Begin
          cmbSWPerson.ItemIndex:=4;
        End;
        4:
        Begin
          cmbSWPerson.ItemIndex:=5;
        End;
        68:
        Begin
          cmbSWPerson.ItemIndex:=6;
        End;
        132:
        Begin
          cmbSWPerson.ItemIndex:=7;
        End;
      end;

      end;
      if DataUn.dmDataModule.dsMNI.FieldByName('SwPort1FWDAddr').AsString<>'-1' then
      begin
        teSWFWDAddr.Text:=DataUn.dmDataModule.dsMNI.FieldByName('SwPort1FWDAddr').AsString;
      end;
      if DataUn.dmDataModule.dsMNI.FieldByName('SwPort1Mode').AsInteger<>-1 then
      begin
        cmbSWMode.ItemIndex:=DataUn.dmDataModule.dsMNI.FieldByName('SwPort1Mode').AsInteger;
      end;
    end;
    2:
    begin
      cbSWEnabled.Checked:= DataUn.dmDataModule.dsMNI.FieldByName('SwPort2Enabled').AsBoolean;
      cbSWFWDEnabled.Checked:= DataUn.dmDataModule.dsMNI.FieldByName('SwPort2FWDEnabled').AsBoolean;
      if DataUn.dmDataModule.dsMNI.FieldByName('SW2BtnPerson').AsInteger<>-1 then
      begin
        case DataUn.dmDataModule.dsMNI.FieldByName('SW2BtnPerson').AsInteger of
          9:
          Begin
            cmbSWPerson.ItemIndex:= 0;
          End;
          17:
          Begin
            cmbSWPerson.ItemIndex:=1;
          End;
          33:
          Begin
            cmbSWPerson.ItemIndex:=2;
          End;
          18:
          Begin
            cmbSWPerson.ItemIndex:=3;
          End;
          34:
          Begin
            cmbSWPerson.ItemIndex:=4;
          End;
          4:
          Begin
            cmbSWPerson.ItemIndex:=5;
          End;
          68:
          Begin
            cmbSWPerson.ItemIndex:=6;
          End;
          132:
          Begin
            cmbSWPerson.ItemIndex:=7;
          End;
        end;
      end;
      if DataUn.dmDataModule.dsMNI.FieldByName('SwPort2FWDAddr').AsString<>'-1' then
      begin
        teSWFWDAddr.Text:=DataUn.dmDataModule.dsMNI.FieldByName('SwPort2FWDAddr').AsString;
      end;
      if DataUn.dmDataModule.dsMNI.FieldByName('SwPort2Mode').AsInteger<>-1 then
      begin
        cmbSWMode.ItemIndex:=DataUn.dmDataModule.dsMNI.FieldByName('SwPort2Mode').AsInteger;
      end;
    end;
    3:
    begin
      cbSWEnabled.Checked:= DataUn.dmDataModule.dsMNI.FieldByName('SwPort3Enabled').AsBoolean;
      cbSWFWDEnabled.Checked:= DataUn.dmDataModule.dsMNI.FieldByName('SwPort3FWDEnabled').AsBoolean;
      if DataUn.dmDataModule.dsMNI.FieldByName('SW3BtnPerson').AsInteger<>-1 then
      begin
        case DataUn.dmDataModule.dsMNI.FieldByName('SW3BtnPerson').AsInteger of
          9:
          Begin
            cmbSWPerson.ItemIndex:= 0;
          End;
          17:
          Begin
            cmbSWPerson.ItemIndex:=1;
          End;
          33:
          Begin
            cmbSWPerson.ItemIndex:=2;
          End;
          18:
          Begin
            cmbSWPerson.ItemIndex:=3;
          End;
          34:
          Begin
            cmbSWPerson.ItemIndex:=4;
          End;
          4:
          Begin
            cmbSWPerson.ItemIndex:=5;
          End;
          68:
          Begin
            cmbSWPerson.ItemIndex:=6;
          End;
          132:
          Begin
            cmbSWPerson.ItemIndex:=7;
          End;
        end;
      end;
      if DataUn.dmDataModule.dsMNI.FieldByName('SwPort3FWDAddr').AsString<>'-1' then
      begin
        teSWFWDAddr.Text:=DataUn.dmDataModule.dsMNI.FieldByName('SwPort3FWDAddr').AsString;
      end;
      if DataUn.dmDataModule.dsMNI.FieldByName('SwPort3Mode').AsInteger<>-1 then
      begin
        cmbSWMode.ItemIndex:=DataUn.dmDataModule.dsMNI.FieldByName('SwPort3Mode').AsInteger;
      end;
    end;
    4:
    begin
      cbSWEnabled.Checked:= DataUn.dmDataModule.dsMNI.FieldByName('SwPort4Enabled').AsBoolean;
      cbSWFWDEnabled.Checked:= DataUn.dmDataModule.dsMNI.FieldByName('SwPort4FWDEnabled').AsBoolean;
      if DataUn.dmDataModule.dsMNI.FieldByName('SW4BtnPerson').AsInteger<>-1 then
      begin
        case DataUn.dmDataModule.dsMNI.FieldByName('SW4BtnPerson').AsInteger of
          9:
          Begin
            cmbSWPerson.ItemIndex:= 0;
          End;
          17:
          Begin
            cmbSWPerson.ItemIndex:=1;
          End;
          33:
          Begin
            cmbSWPerson.ItemIndex:=2;
          End;
          18:
          Begin
            cmbSWPerson.ItemIndex:=3;
          End;
          34:
          Begin
            cmbSWPerson.ItemIndex:=4;
          End;
          4:
          Begin
            cmbSWPerson.ItemIndex:=5;
          End;
          68:
          Begin
            cmbSWPerson.ItemIndex:=6;
          End;
          132:
          Begin
            cmbSWPerson.ItemIndex:=7;
          End;
        end;
      end;
      if DataUn.dmDataModule.dsMNI.FieldByName('SwPort4FWDAddr').AsString<>'-1' then
      begin
        teSWFWDAddr.Text:=DataUn.dmDataModule.dsMNI.FieldByName('SwPort4FWDAddr').AsString;
      end;
      if DataUn.dmDataModule.dsMNI.FieldByName('SwPort4Mode').AsInteger<>-1 then
      begin
        cmbSWMode.ItemIndex:=DataUn.dmDataModule.dsMNI.FieldByName('SwPort4Mode').AsInteger;
      end;
    end;

  end;
  shpcbSWEnabled.Visible:=false;
  shpcmbSWPerson.Visible:=false;
  shpcbSWFWDEnabled.Visible:=false;
  teSWFWDAddr.Brush.Color:=clWhite;
  btUpdateParams.Color:=clWhite;
  shpcmbSWMode.Visible:=false;
  if SingleEditTest then
  begin

  end;

end;

procedure TMNIConfigFm.cmbSWModeChange(Sender: TObject);
begin
  shpcmbSWMode.Visible:=true;
  btUpdateParams.Color:=clLime;
end;

procedure TMNIConfigFm.cmbSWPersonChange(Sender: TObject);
begin
  shpcmbSWPerson.Visible:=true;
  btUpdateParams.Color:=clLime;
end;

Function TMNIConfigFm.ConvertBaudRate:String;
begin
  case DataUn.dmDataModule.dsMNI.FieldByName('SerialPerson').AsInteger of
    1:
    begin
      ConvertBaudRate:='1200';
    end;
    2:
    begin
      ConvertBaudRate:='2400';
    end;
    3:
    begin
      ConvertBaudRate:='4800';
    end;
    4:
    begin
      ConvertBaudRate:='9600';
    end;
    5:
    begin
      ConvertBaudRate:='19200';
    end;
    6:
    begin
      ConvertBaudRate:='38400';
    end;
    7:
    begin
      ConvertBaudRate:='57600';
    end;
    8:
    begin
      ConvertBaudRate:='115200';
    end;
  end;
end;

procedure TMNIConfigFm.cmbUidMNIChange(Sender: TObject);
var
  I: Integer;
begin
//  if LastUID<>StrToInt(cmbUidMNI.text) then
//  Begin
    if(sgMap.Cells[0,1]<>'')then
    begin
      SaveSwMapData(LastUID);

      AllSwitches.Clear;
      for I := 1 to sgMap.RowCount-1 do
      begin
        sgmap.Cells[0,I]:='';
        sgmap.Cells[1,I]:='';
        sgmap.Cells[2,I]:='';
      end;
      sgmap.RowCount:=1;
    end;
    cmbDeviceID.clear;
    if loadAllSwitchData then
    begin
      LoadsgMap;
    end
    else
    begin
      sgmap.Enabled:=false;
      cmbDeviceID.Visible:=false;
    end;

    DataUn.dmDataModule.dsMNI.Filtered := False;
    DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMNI.Text;
    DataUn.dmDataModule.dsMNI.Filtered := True;

    DataUn.dmDataModule.dsMNI.MergeChangeLog;

    cmbSwitch.ItemIndex:=0;
    //Alias
    txtAlias.OnChange:=nil;
    txtAlias.Text:=DataUn.dmDataModule.dsMNI.FieldByName('Alias').AsString;
    txtAlias.OnChange:=txtAliasChange;
    //Serial Enabled
    if DataUn.dmDataModule.dsMNI.FieldByName('SerialDisable').AsInteger=1 then
    begin
      cbSerialEnabled.checked:=False;
    end
    else
    begin
      cbSerialEnabled.checked:=True;
    end;

    //Serial Baud
    if DataUn.dmDataModule.dsMNI.FieldByName('SerialBaud').AsInteger<>-1 then
    begin
      cmbSerialBaud.ItemIndex:=DataUn.dmDataModule.dsMNI.FieldByName('SerialBaud').AsInteger-1;
    end;
    //UID in ZGN
    if DataUn.dmDataModule.dsMNI.FieldByName('SerialUIDInZGN').AsInteger<>-1 then
    begin
      if DataUn.dmDataModule.dsMNI.FieldByName('SerialUIDInZGN').AsInteger=1 then
      begin
        cbSerialUIDinZGN.Checked:= True;
      end
      else
      begin
        cbSerialUIDinZGN.Checked:= False;
      end;
    end;
    //Serial Routing Field
    if DataUn.dmDataModule.dsMNI.FieldByName('SerialRoutingField').AsInteger<>-1 then
    begin
      if DataUn.dmDataModule.dsMNI.FieldByName('SerialRoutingField').AsInteger=1 then
      begin
        cbSerialRoutingField.Checked:= True;
      end
      else
      begin
        cbSerialRoutingField.Checked:= False;
      end;
    end;
    //Serial Checksum
    if DataUn.dmDataModule.dsMNI.FieldByName('SerialCheckSum').AsInteger<>-1 then
    begin
      if DataUn.dmDataModule.dsMNI.FieldByName('SerialCheckSum').AsInteger=1 then
      begin
        cbSerialMSGChkSum.Checked:= True;
      end
      else
      begin
        cbSerialMSGChkSum.Checked:= False;
      end;
    end;
    //Serial Receiver
    if DataUn.dmDataModule.dsMNI.FieldByName('SerialReceiver').AsBoolean then
    begin
      cbSerialReceive.Checked:= True;
    end
    else
    begin
      cbSerialReceive.Checked:= False;
    end;
    //Serial Transmiiter
    if DataUn.dmDataModule.dsMNI.FieldByName('SerialTransmitter').AsBoolean then
    begin
      cbSerialTransmit.Checked:= True;
    end
    else
    begin
      cbSerialTransmit.Checked:= False;
    end;
    //Serial Physical Layer
    if DataUn.dmDataModule.dsMNI.FieldByName('PLPerson').AsInteger<>-1 then
    begin
      if DataUn.dmDataModule.dsMNI.FieldByName('PLPerson').AsInteger=0 then
      begin
        cmbSerialProtocol.ItemIndex:=cmbSerialProtocol.Items.IndexOf('RS-232-TTL')
      end
      else if DataUn.dmDataModule.dsMNI.FieldByName('PLPerson').AsInteger=1 then
      begin
        cmbSerialProtocol.ItemIndex:=cmbSerialProtocol.Items.IndexOf('RS-232-TIA')
      end
      else if DataUn.dmDataModule.dsMNI.FieldByName('PLPerson').AsInteger=2 then
      begin
        cmbSerialProtocol.ItemIndex:=cmbSerialProtocol.Items.IndexOf('RS-485')
      end
      else if DataUn.dmDataModule.dsMNI.FieldByName('PLPerson').AsInteger=3 then
      begin
        cmbSerialProtocol.ItemIndex:=cmbSerialProtocol.Items.IndexOf('USB')
      end
      else if DataUn.dmDataModule.dsMNI.FieldByName('PLPerson').AsInteger=4 then
      begin
        cmbSerialProtocol.ItemIndex:=cmbSerialProtocol.Items.IndexOf('WiFi')
      end;

    end;
  //  //Serial Person
  //  if DataUn.dmDataModule.dsMNI.FieldByName('PLPerson').AsInteger<>-1 then
  //  begin
  //    if DataUn.dmDataModule.dsMNI.FieldByName('PLPerson').AsInteger=0 then
  //    begin
  //      cmbSerialProtocol.ItemIndex:=cmbSerialProtocol.Items.IndexOf('RS-232-TTL')
  //    end
  //    else if DataUn.dmDataModule.dsMNI.FieldByName('PLPerson').AsInteger=1 then
  //    begin
  //      cmbSerialProtocol.ItemIndex:=cmbSerialProtocol.Items.IndexOf('RS-232-TIA')
  //    end
  //    else if DataUn.dmDataModule.dsMNI.FieldByName('PLPerson').AsInteger=2 then
  //    begin
  //      cmbSerialProtocol.ItemIndex:=cmbSerialProtocol.Items.IndexOf('RS-485')
  //    end
  //    else if DataUn.dmDataModule.dsMNI.FieldByName('PLPerson').AsInteger=3 then
  //    begin
  //      cmbSerialProtocol.ItemIndex:=cmbSerialProtocol.Items.IndexOf('USB')
  //    end
  //    else if DataUn.dmDataModule.dsMNI.FieldByName('PLPerson').AsInteger=4 then
  //    begin
  //      cmbSerialProtocol.ItemIndex:=cmbSerialProtocol.Items.IndexOf('WiFi')
  //    end;
  //
  //  end;
    //Serial Person
    if DataUn.dmDataModule.dsMNI.FieldByName('SerialPerson').AsInteger<>-1 then
    begin
      if DataUn.dmDataModule.dsMNI.FieldByName('SerialPerson').AsInteger=0 then
      begin
        cmbSerialPerson.ItemIndex:=cmbSerialPerson.Items.IndexOf('None');
      end
      else if DataUn.dmDataModule.dsMNI.FieldByName('SerialPerson').AsInteger=1 then
      begin
        cmbSerialPerson.ItemIndex:=cmbSerialPerson.Items.IndexOf('MN-Floor');
      end
      else if DataUn.dmDataModule.dsMNI.FieldByName('SerialPerson').AsInteger=2 then
      begin
        cmbSerialPerson.ItemIndex:=cmbSerialPerson.Items.IndexOf('MN-Riser');
      end
      else if DataUn.dmDataModule.dsMNI.FieldByName('SerialPerson').AsInteger=3 then
      begin
        cmbSerialPerson.ItemIndex:=cmbSerialPerson.Items.IndexOf('MapIn');
      end
      else if DataUn.dmDataModule.dsMNI.FieldByName('SerialPerson').AsInteger=4 then
      begin
        cmbSerialPerson.ItemIndex:=cmbSerialPerson.Items.IndexOf('MapOut');
      end
      else if DataUn.dmDataModule.dsMNI.FieldByName('SerialPerson').AsInteger=5 then
      begin
        cmbSerialPerson.ItemIndex:=cmbSerialPerson.Items.IndexOf('MapInOut');
      end
      else if DataUn.dmDataModule.dsMNI.FieldByName('SerialPerson').AsInteger=6 then
      begin
        cmbSerialPerson.ItemIndex:=cmbSerialPerson.Items.IndexOf('Binary');
      end
      else if DataUn.dmDataModule.dsMNI.FieldByName('SerialPerson').AsInteger=7 then
      begin
        cmbSerialPerson.ItemIndex:=cmbSerialPerson.Items.IndexOf('SomfyRTS');
      end
      else if DataUn.dmDataModule.dsMNI.FieldByName('SerialPerson').AsInteger=8 then
      begin
        cmbSerialPerson.ItemIndex:=cmbSerialPerson.Items.IndexOf('SDN1');
      end
      else if DataUn.dmDataModule.dsMNI.FieldByName('SerialPerson').AsInteger=9 then
      begin
        cmbSerialPerson.ItemIndex:=cmbSerialPerson.Items.IndexOf('SDN2');
      end;
    end;

    FindSerialProfile;
    LastSerialProfile:=cmbSerialDevices.Text;

    //IR PORT PARAMS
    if DataUn.dmDataModule.dsMNI.FieldByName('IRPortPerson').AsInteger<>-1 then
    begin
      cmbIRPerson.ItemIndex:= DataUn.dmDataModule.dsMNI.FieldByName('IRPortPerson').AsInteger;
    end;
    if DataUn.dmDataModule.dsMNI.FieldByName('IRPortFwdAddr').AsString<>'-1' then
    begin
      teIRFWDAddr.Text:= DataUn.dmDataModule.dsMNI.FieldByName('IRPortFwdAddr').AsString;
    end;

    cbIRPortEnabled.Checked:= DataUn.dmDataModule.dsMNI.FieldByName('IRPortEnabled').AsBoolean;
    cbIRFWDEnabled.Checked:= DataUn.dmDataModule.dsMNI.FieldByName('IRPortFWDEnabled').AsBoolean;

    if DataUn.dmDataModule.dsMNI.FieldByName('IRPortMode').AsInteger<>-1 then
    begin
      cmbIRMode.ItemIndex:=DataUn.dmDataModule.dsMNI.FieldByName('IRPortMode').AsInteger;
    end;

    //SWITCH PARAMS
    cbSWEnabled.Checked:= DataUn.dmDataModule.dsMNI.FieldByName('SwPort1Enabled').AsBoolean;
    cbSWFWDEnabled.Checked:= DataUn.dmDataModule.dsMNI.FieldByName('SwPort1FWDEnabled').AsBoolean;
    if DataUn.dmDataModule.dsMNI.FieldByName('SW1BtnPerson').AsInteger<>-1 then
    begin
      case DataUn.dmDataModule.dsMNI.FieldByName('SW1BtnPerson').AsInteger of
        9:
        Begin
          cmbSWPerson.ItemIndex:= 0;
        End;
        17:
        Begin
          cmbSWPerson.ItemIndex:=1;
        End;
        33:
        Begin
          cmbSWPerson.ItemIndex:=2;
        End;
        18:
        Begin
          cmbSWPerson.ItemIndex:=3;
        End;
        34:
        Begin
          cmbSWPerson.ItemIndex:=4;
        End;
        4:
        Begin
          cmbSWPerson.ItemIndex:=5;
        End;
        68:
        Begin
          cmbSWPerson.ItemIndex:=6;
        End;
        132:
        Begin
          cmbSWPerson.ItemIndex:=7;
        End;

      end;

        cmbSWPerson.ItemIndex:=DataUn.dmDataModule.dsMNI.FieldByName('SW1BtnPerson').AsInteger;
    end;
    if DataUn.dmDataModule.dsMNI.FieldByName('SwPort1FWDAddr').AsString<>'-1' then
    begin
      teSWFWDAddr.Text:=DataUn.dmDataModule.dsMNI.FieldByName('SwPort1FWDAddr').AsString;
    end;
    if DataUn.dmDataModule.dsMNI.FieldByName('SwPort1Mode').AsInteger<>-1 then
    begin
      cmbSWMode.ItemIndex:=DataUn.dmDataModule.dsMNI.FieldByName('SwPort1Mode').AsInteger;
    end;
    //Firmware
    teFirmwareRev.Text:=  DataUn.dmDataModule.dsMNI.FieldByName('FirmwareRev').AsString;
    LastUID:=StrToInt(cmbUidMNI.text);
    ClearEdits;
  //End;

end;

procedure TMNIConfigFm.MNIUpdateParam;
var
  SwPortEnabled,SwPortFWDEnabled,SwPortPerson,SwPortMode,SwPortFWDAddr:String;

begin
  DataUn.dmDataModule.dsMNI.Filtered := False;
  DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMNI.Text;
  DataUn.dmDataModule.dsMNI.Filtered := True;
  //Serial Params

  //MOTOR PERSONALITY
//  Case StrToInt(cmbMotorPort.text) of
//    1:
//    begin
//      if DataUn.dmDataModule.dsMNI.FieldByName('Motor1PortPerson').AsInteger<>-1 then
//      begin
//        cmbDCMotors.ItemIndex:= DataUn.dmDataModule.dsMNI.FieldByName('Motor1PortPerson').AsInteger;
//      end;
//    end;
//    2:
//    begin
//      if DataUn.dmDataModule.dsMNI.FieldByName('Motor2PortPerson').AsInteger<>-1 then
//      begin
//        cmbDCMotors.ItemIndex:= DataUn.dmDataModule.dsMNI.FieldByName('Motor2PortPerson').AsInteger;
//      end;
//    end;
//    3:
//    begin
//      if DataUn.dmDataModule.dsMNI.FieldByName('Motor3PortPerson').AsInteger<>-1 then
//      begin
//        cmbDCMotors.ItemIndex:= DataUn.dmDataModule.dsMNI.FieldByName('Motor3PortPerson').AsInteger;
//      end;
//    end;
//    4:
//    begin
//      if DataUn.dmDataModule.dsMNI.FieldByName('Motor4PortPerson').AsInteger<>-1 then
//      begin
//        cmbDCMotors.ItemIndex:= DataUn.dmDataModule.dsMNI.FieldByName('Motor4PortPerson').AsInteger;
//      end;
//    end;
//  End;


  //IR PORT PARAMS
  if DataUn.dmDataModule.dsMNI.FieldByName('IRPortPerson').AsInteger<>-1 then
  begin
    cmbIRPerson.ItemIndex:= DataUn.dmDataModule.dsMNI.FieldByName('IRPortPerson').AsInteger;
  end;
  if DataUn.dmDataModule.dsMNI.FieldByName('IRPortFwdAddr').AsString<>'-1' then
  begin
    teIRFWDAddr.Text:= DataUn.dmDataModule.dsMNI.FieldByName('IRPortFwdAddr').AsString;
  end;

  cbIRPortEnabled.Checked:= DataUn.dmDataModule.dsMNI.FieldByName('IRPortEnabled').AsBoolean;
  cbIRFWDEnabled.Checked:= DataUn.dmDataModule.dsMNI.FieldByName('IRPortFWDEnabled').AsBoolean;

  if DataUn.dmDataModule.dsMNI.FieldByName('IRPortMode').AsInteger<>-1 then
  begin
    cmbIRMode.ItemIndex:=DataUn.dmDataModule.dsMNI.FieldByName('IRPortMode').AsInteger;
  end;

  //SWITCH PARAMS
  case StrToInt(cmbSwitch.Text) of
    1:
    begin
      SwPortEnabled:='SwPort1Enabled';
      SwPortFWDEnabled:='SwPort1FWDEnabled';
      SwPortPerson:='SW1BtnPerson';
      SwPortFWDAddr:='SwPort1FWDAddr';
      SwPortMode:='SwPort1Mode';
    end;
    2:
    begin
      SwPortEnabled:='SwPort2Enabled';
      SwPortFWDEnabled:='SwPort2FWDEnabled';
      SwPortPerson:='SW2BtnPerson';
      SwPortFWDAddr:='SwPort2FWDAddr';
      SwPortMode:='SwPort2Mode';
    end;
    3:
    begin
      SwPortEnabled:='SwPort3Enabled';
      SwPortFWDEnabled:='SwPort3FWDEnabled';
      SwPortPerson:='SW3BtnPerson';
      SwPortFWDAddr:='SwPort3FWDAddr';
      SwPortMode:='SwPort3Mode';
    end;
    4:
    begin
      SwPortEnabled:='SwPort4Enabled';
      SwPortFWDEnabled:='SwPort4FWDEnabled';
      SwPortPerson:='SW4BtnPerson';
      SwPortFWDAddr:='SwPort4FWDAddr';
      SwPortMode:='SwPort4Mode';
    end;
  end;
  cbSWEnabled.Checked:= DataUn.dmDataModule.dsMNI.FieldByName(SwPortEnabled).AsBoolean;
  cbSWFWDEnabled.Checked:= DataUn.dmDataModule.dsMNI.FieldByName(SwPortFWDEnabled).AsBoolean;
  if DataUn.dmDataModule.dsMNI.FieldByName(SwPortPerson).AsInteger<>-1 then
  begin
    case DataUn.dmDataModule.dsMNI.FieldByName(SwPortPerson).AsInteger of
      9:
      Begin
        cmbSWPerson.ItemIndex:= 0;
      End;
      17:
      Begin
        cmbSWPerson.ItemIndex:=1;
      End;
      33:
      Begin
        cmbSWPerson.ItemIndex:=2;
      End;
      18:
      Begin
        cmbSWPerson.ItemIndex:=3;
      End;
      34:
      Begin
        cmbSWPerson.ItemIndex:=4;
      End;
      4:
      Begin
        cmbSWPerson.ItemIndex:=5;
      End;
      68:
      Begin
        cmbSWPerson.ItemIndex:=6;
      End;
      132:
      Begin
        cmbSWPerson.ItemIndex:=7;
      End;

    end;

  end;
  if DataUn.dmDataModule.dsMNI.FieldByName(SwPortFWDAddr).AsString<>'-1' then
  begin
    teSWFWDAddr.Text:=DataUn.dmDataModule.dsMNI.FieldByName(SwPortFWDAddr).AsString;
  end;
  if DataUn.dmDataModule.dsMNI.FieldByName(SwPortMode).AsInteger<>-1 then
  begin
    cmbSWMode.ItemIndex:=DataUn.dmDataModule.dsMNI.FieldByName(SwPortMode).AsInteger;
  end;
//  //Firmware
//  teFirmwareRev.Text:=  DataUn.dmDataModule.dsMNI.FieldByName('FirmwareRev').AsString;
  ClearEdits;
end;

procedure TMNIConfigFm.cmbSerialMapCol1Change(Sender: TObject);
var
  SwIndex:Integer;
begin
  if cmbSerialMapCol1.ItemIndex<>-1 then
  begin
    SwIndex:= SelectSwByID(cmbDeviceID.text);
    sgMap.Cells[sgMap.Col, sgMap.Row] :=cmbSerialMapCol1.Items[cmbSerialMapCol1.ItemIndex];
    cmbSerialMapCol1.Visible := False;
    sgMap.SetFocus;

  end;
end;

procedure TMNIConfigFm.cmbSerialMapCol1Exit(Sender: TObject);
var
  SwIndex:Integer;
begin

  if(cmbSerialMapCol1.text<>'')then
  begin
    SwIndex:= SelectSwByID(cmbDeviceID.text);
    if(AllSwitches[SwIndex].GetButtonActionByName('Button'+IntToStr(sgMap.Row))<>cmbSerialMapCol1.text)then
    begin
      AllSwitches[SwIndex].EditFlg:=true;
    end;
    if cmbSerialMapCol1.ItemIndex=-1 then
    begin
      sgMap.Cells[sgMap.Col, sgMap.Row] := cmbSerialMapCol1.text;

    end
    else
    begin
      sgMap.Cells[lastCol, lastRow]:= cmbSerialMapCol1.Items[cmbSerialMapCol1.ItemIndex];
    end;
    AllSwitches[SwIndex].AddButtonAction('Button'+IntToStr(sgMap.Row),cmbSerialMapCol1.text);
    if AllSwitches[SwIndex].Preselect then
    begin
      if cmbSerialMapCol1.text<>'(Pre-Group)' then
      begin
        sgMap.Cells[2,sgMap.Row]:='NA';
        AllSwitches[SwIndex].AddButtonGroup('Button'+IntToStr(sgMap.Row),'NA');
      end
      else if sgMap.Cells[2,sgMap.Row]='NA' then
      begin
        sgMap.Cells[2,sgMap.Row]:='';
        AllSwitches[SwIndex].AddButtonGroup('Button'+IntToStr(sgMap.Row),'');
      end;
    end;


  end;
    cmbSerialMapCol1.ItemIndex:=-1;
    cmbSerialMapCol1.Text := '';
    cmbSerialMapCol1.Visible  := False;
//    if (Screen.ActiveControl.Name='sgMap')and(sgMap.Col=0)and(sgMap.Row=0)then
//    begin
//      sgMap.col:=0;
//      sgMap.Row:=1;
//    end;
    //sgMap.SetFocus;

end;

procedure TMNIConfigFm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  btnExitClick(self);
end;

procedure TMNIConfigFm.FormCreate(Sender: TObject);
begin
  MapInBuffer:=TStringList.Create;
  MapInTemp:=TStringList.Create;
  AllSwitches:=TObjectList<TSwitchClassUn>.Create();
with cmbSerialMapCol1 do
  begin
    sgMap.DefaultRowHeight := Height;
    Visible := False;
  end;
  ComboBox_AutoWidth(cmbDeviceSelector);
end;

//Allows combox  dropdown to size to oversized string items
procedure TMNIConfigFm.ComboBox_AutoWidth(const theComboBox: TCombobox);
const
  HORIZONTAL_PADDING = 4;
var
  itemsFullWidth: integer;
  idx: integer;
  itemWidth: integer;
begin
  itemsFullWidth := 0;
  // get the max needed with of the items in dropdown state
  for idx := 0 to -1 + theComboBox.Items.Count do
  begin
    itemWidth := theComboBox.Canvas.TextWidth(theComboBox.Items[idx]);
    Inc(itemWidth, 2 * HORIZONTAL_PADDING);
    if (itemWidth > itemsFullWidth) then itemsFullWidth := itemWidth;
  end;
  itemsFullWidth:=itemsFullWidth+20;
    // set the width of drop down if needed
  if (itemsFullWidth > theComboBox.Width) then
  begin
    //check if there would be a scroll bar
    if theComboBox.DropDownCount < theComboBox.Items.Count then
      itemsFullWidth := itemsFullWidth + GetSystemMetrics(SM_CXVSCROLL);
    SendMessage(theComboBox.Handle, CB_SETDROPPEDWIDTH, itemsFullWidth, 0);
  end;
end;

procedure TMNIConfigFm.FormHide(Sender: TObject);
var
  I:Integer;
begin
  VariablesUn.giTop := Top;
  VariablesUn.giLeft := Left;
  SaveSwMapData(StrToInt(cmbUIDMNI.text));
  for I := 1 to sgMap.RowCount-1 do
  begin
    sgmap.Cells[0,I].Empty;
    sgmap.Cells[1,I].Empty;
    sgmap.Cells[2,I].Empty;
  end;
  sgmap.RowCount:=1;
end;

procedure TMNIConfigFm.FormMouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
var
  I: Integer;
begin
  cmbSerialMapCol1Exit(Nil);
  teSerialMapGroupExit(Nil);
  scrollbox1.SetFocus;
  Handled := PtInRect(ScrollBox1.ClientRect, ScrollBox1.ScreenToClient(MousePos));
  if Handled then
    for I := 1 to Mouse.WheelScrollLines do
    try
      if WheelDelta > 0 then
        ScrollBox1.Perform(WM_VSCROLL, SB_LINEUP, 0)
      else
        ScrollBox1.Perform(WM_VSCROLL, SB_LINEDOWN, 0);
    finally
      ScrollBox1.Perform(WM_VSCROLL, SB_ENDSCROLL, 0);
    end;
end;

Procedure TMNIConfigFm.LoadProjects;
var
  I:Integer;
begin


  cmbProjectMNI.Items.clear;
  //Polpulate project if it exists
  if DataUn.dmDataModule.dsProject.State <> dsInactive then
  begin
    DataUn.dmDataModule.dsProject.Filtered := False;
    DataUn.dmDataModule.dsProject.First;
    while not DataUn.dmDataModule.dsProject.eof do
    begin
      if (DataUn.dmDataModule.dsProject.FieldByName('Last').AsInteger = 1) then
         cmbProjectMNI.Items.Insert(0, DataUn.dmDataModule.dsProject.FieldByName('ProjectName').AsString)
      else
         cmbProjectMNI.Items.Add(DataUn.dmDataModule.dsProject.FieldByName('ProjectName').AsString);

      DataUn.dmDataModule.dsProject.Next;
    end;
    DataUn.dmDataModule.dsProject.Filtered := False;
    DataUn.dmDataModule.dsProject.First;
    for I := 0 to cmbProjectMNI.Items.Count-1 do
    begin
      if cmbprojectMNI.text = VariablesUn.gsProjectName then
      begin
        break;
      end
      else
      begin
        cmbprojectMNI.ItemIndex:=I;
      end;
    end;
  End;
end;

procedure TMNIConfigFm.FormShow(Sender: TObject);
var
  I,page:Integer;
  SwitchInfo:TStringList;
  SWDeviceID:String;
begin
  SwitchInfo:=TStringList.Create;
  Top:=VariablesUn.giTop;
  Left:=VariablesUn.giLeft;
  cmbDeviceSelector.ItemIndex:=cmbDeviceSelector.Items.IndexOf('MNI');;

  ComFm.rh_MNIConfigPortOpend := rhPortOpend;
  ComFm.rh_MNIConfigPortClosed := rhPortClosed;
  sgMap.Cells[0,0]:='Button';
  sgMap.Cells[1,0]:='Action';
  sgMap.Cells[2,0]:='Group';
  LoadProjects;
  LoadUID;
  LastUID:=StrToInt(cmbUIDMNI.Text);
  if AllSwitches.count=0 then
  begin
    sgMap.enabled:=false;
  end;
  ClearEdits;
  cmbUidMNIChange(nil);
  cmbSwitchChange(Nil);
  vsOldAlias:=txtAlias.Text;
end;

procedure TMNIConfigFm.btnSettingsClick(Sender: TObject);
begin
  VariablesUn.gitop:=top;
  VariablesUn.giLeft:=left;
  ScreenSetupFm.rh_showmain := rhShowMNI;
  pnlMenuBar.Visible := False;
  Hide;
  ScreenSetupFm.Show;
end;

procedure TMNIConfigFm.btnSettingsMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;



procedure TMNIConfigFm.Image2MouseEnter(Sender: TObject);
begin
  if image2.enabled then
  Begin
    pnlMenuBar.Visible := True;
  End;
end;

//procedure TMNIConfigFm.Image5Click(Sender: TObject);
//begin
//  VariablesUn.gitop:=top;
//  VariablesUn.giLeft:=left;
//  SaveSwMapData(StrToInt(cmbUIDMNI.Text));
//  hide;
//  rh_ShowMNIDisplay(self);
//
//end;

procedure TMNIConfigFm.JvCaptionButton1Click(Sender: TObject);
begin
  Application.HelpContext(8);
end;

procedure TMNIConfigFm.pnlMenuBarMouseLeave(Sender: TObject);
begin
  pnlMenuBar.Visible := false;
end;

//Changes Color of Com Indicator lights
procedure TMNIConfigFm.rhPortOpend(Sender: TObject);
begin
  giComPortOn := True;
  Shape14.Pen.Color := clLime;
end;
//Changes Color of Com Indicator lights
procedure TMNIConfigFm.rhPortClosed(Sender: TObject);
begin
  giComPortOn := False;
  Shape14.Pen.Color := clRed;
end;



procedure TMNIConfigFm.sgMapClick(Sender: TObject);
var
  swIndex:Integer;
begin
 if (LastCol<>sgMap.col)or(LastRow<>sgMap.Row)then
  begin
    if(LastCol=0)then
    begin
      if sgMap.Cells[LastCol,LastRow]='' then
      begin
        sgMap.Cells[LastCol,LastRow]:='Button'+intToStr(LastRow);
        SwIndex:= SelectSwByID(cmbDeviceID.text);
        AllSwitches[SwIndex].EditButtonName('Button'+IntToStr(LastRow),'Button'+intToStr(LastRow));
      end;
    end;
    LastCol:=sgMap.Col;
    LastRow:=sgMap.Row;
  end;
end;

procedure TMNIConfigFm.sgMapSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
var
  R,R2:  TRect;
  SwIndex:Integer;
  TextHolder:String;
begin

  if (ACol = 1) and (ARow <> 0) and (sgMap.Cells[0,ARow]<>'')then
  begin
    SwIndex:= SelectSwByID(cmbDeviceID.text);
    sgMap.Options:=sgMap.Options-[goEditing];
    //CanSelect := false;
    if sgMap.Cells[ACol,ARow]<>'' then
    begin
      cmbSerialMapCol1.Text:=sgMap.Cells[ACol,ARow]
    end;
    R := sgMap.CellRect(ACol, ARow);
    R.Left := R.Left + sgMap.Left;
    R.Right := R.Right + sgMap.Left+20;
    R.Top := R.Top + sgMap.Top;
    R.Bottom := R.Bottom + sgMap.Top;
    with cmbSerialMapCol1 do
    begin
      Left := R.Left + 1;
      Top := R.Top + 1;
      Width := (R.Right + 1) - R.Left;
      Height := (R.Bottom + 1) - R.Top;

      TextHolder:= sgMap.cells[ACol,Arow];
      if (AllSwitches[SwIndex].GetButtonDownbyName('Button'+IntToStr(ARow))='') xor (AllSwitches[SwIndex].GetButtonUpbyName('Button'+IntToStr(ARow))='')then
      begin

        clear;
        if AllSwitches[SwIndex].Preselect then
        begin
          Items.Add('(Pre-Group)');
          Items.Add('(Pre-None)');
          Items.Add('(Pre-UP)');
          Items.Add('(Pre-PS1)');
          Items.Add('(Pre-PS2)');
          Items.Add('(Pre-PS3)');
          Items.Add('(Pre-Down)');
          Items.Add('(Pre-Stop)');
          Items.Add('(Pre-Ret Auto)');
        end
        else if (AllSwitches[SwIndex].GetButtonDownbyName('Button'+IntToStr(ARow))='') and (AllSwitches[SwIndex].GetButtonUpbyName('Button'+IntToStr(ARow))='')then
        begin
          showMessage('Switch DB Error. Please Contact Tech Support!');
          exit;
        end
        else
        begin
          Items.Add('(UP)');
          Items.Add('(PS1)');
          Items.Add('(PS2)');
          Items.Add('(PS3)');
          Items.Add('(Down)');
          Items.Add('(Stop)');
          Items.Add('(Ret Auto)');
          Items.Add('(Custom 7B)');
        end;
      end
      else
      begin
        clear;
        if AllSwitches[SwIndex].Preselect then
        begin
          Items.Add('(Pre-Group)');
          Items.Add('(Pre-None)');
          Items.Add('(Pre-UP)');
          Items.Add('(Pre-UP Hold)');
          Items.Add('(Pre-PS1)');
          Items.Add('(Pre-PS2)');
          Items.Add('(Pre-PS3)');
          Items.Add('(Pre-Down)');
          Items.Add('(Pre-Down Hold)');
          Items.Add('(Pre-Stop)');
          Items.Add('(Pre-Ret Auto)');
        end
        else
        begin
          Items.Add('(UP)');
          Items.Add('(UP Hold)');
          Items.Add('(PS1)');
          Items.Add('(PS2)');
          Items.Add('(PS3)');
          Items.Add('(Down)');
          Items.Add('(Down Hold)');
          Items.Add('(Stop)');
          Items.Add('(Ret Auto)');
          Items.Add('(Custom 7B)');
        end;
      end;

      //ItemIndex:=0;
      ItemIndex:=Items.IndexOf(TextHolder);
      if ItemIndex=-1 then
      begin
        ItemIndex:=0;
      end;
      Visible := True;
      SetFocus;
    end;
  end;
  if(Acol=2)and(sgMap.Cells[0,ARow]<>'')and (ARow <> 0)and(sgMap.Cells[2,ARow]<>'NA')then
  begin
    //SwIndex:= SelectSwByID(cmbDeviceID.text);
    //CanSelect := false;
    sgMap.Options:=sgMap.Options-[goEditing];
    if sgMap.Cells[ACol,ARow]<>'' then
    begin
      teSerialMapGroup.Text:=sgMap.Cells[ACol,ARow]
    end;
    R2 := sgMap.CellRect(ACol, ARow);
    R2.Left := R2.Left + sgMap.Left;
    R2.Right := R2.Right + sgMap.Left;
    R2.Top := R2.Top + sgMap.Top;
    R2.Bottom := R2.Bottom + sgMap.Top;
    with teSerialMapGroup do
    begin
//      Left := R2.Left+5;
//      Top := R2.Top;
//      Width := (R2.Right)- R2.Left;
      //Width := (R2.Right + 11)- R2.Left;
      Left := R2.Left + 1;
      Top := R2.Top + 1;
      Width := (R2.Right + 1) - R2.Left;
      Height := (R2.Bottom + 1) - R2.Top;
      Visible := True;
      SetFocus;

    end;
  end;
  if(Acol=0)then
  begin
    //SwIndex:= SelectSwByID(cmbDeviceID.text);
    if(sgMap.Cells[ACol,ARow]='')then
    begin
      CanSelect := false;
      sgMap.Options:=sgMap.Options-[goEditing];
    end
    else
    begin
      CanSelect := true;
      sgMap.Options:=sgMap.Options+[goEditing];
    end;

  end;


end;
procedure TMNIConfigFm.sgMapSetEditText(Sender: TObject; ACol, ARow: Integer;
  const Value: string);
var
  SwIndex:Integer;
begin
  if(ACol=0)then
  begin
    if Value<>'' then
    begin
      SwIndex:= SelectSwByID(cmbDeviceID.text);
      AllSwitches[SwIndex].EditButtonName('Button'+IntToStr(ARow),Value);
    end;
  end;
end;

Procedure TMNIConfigFm.ClearGrid();
var
  I:Integer;
begin
    for I := 1 to sgMap.RowCount-1 do
    BEGIN
      sgMap.cells[0,I]:='';
      sgMap.cells[1,I]:='';
      sgMap.cells[2,I]:='';
    END;

end;
end.
