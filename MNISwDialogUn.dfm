object MNISwDialogFM: TMNISwDialogFM
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Choose Switch'
  ClientHeight = 412
  ClientWidth = 308
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 1
    Width = 88
    Height = 18
    Caption = 'Switch Brand:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 8
    Top = 48
    Width = 69
    Height = 18
    Caption = 'Device ID:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lblSwBrand: TLabel
    Left = 102
    Top = 1
    Width = 29
    Height = 18
    Caption = 'TBD'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 20
    Top = 23
    Width = 57
    Height = 18
    Caption = 'CMD ID:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lblSwAlpha: TLabel
    Left = 83
    Top = 23
    Width = 11
    Height = 18
    Caption = '#'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object btnAddSwitch: TButton
    Left = 225
    Top = 379
    Width = 75
    Height = 25
    Caption = 'Add'
    TabOrder = 0
    OnClick = btnAddSwitchClick
  end
  object pcAvailableSw: TPageControl
    Left = 8
    Top = 72
    Width = 292
    Height = 273
    TabOrder = 1
    OnChange = pcAvailableSwChange
  end
  object teSwDeviceID: TEdit
    Left = 79
    Top = 45
    Width = 118
    Height = 26
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
  end
  object cbInstantSw: TCheckBox
    Left = 8
    Top = 381
    Width = 123
    Height = 17
    Caption = 'Instant Mode'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnClick = cbInstantSwClick
  end
  object cbPreselectSw: TCheckBox
    Left = 8
    Top = 358
    Width = 153
    Height = 17
    Caption = 'PreSelect Mode'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    OnClick = cbPreselectSwClick
  end
end
