unit SettingsUn;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, ComUn, VariablesUn, DiscoverUn,
  Vcl.Buttons, DataUn, Datasnap.DBClient, Data.DB, AdSelCom, NewProjectUn,MWCConfigUn,
  Vcl.Imaging.pngimage;

type
  TSettingsFm = class(TForm)
    Panel1: TPanel;
    Shape4: TShape;
    Shape3: TShape;
    Shape2: TShape;
    Shape5: TShape;
    Shape1: TShape;
    lblAlias: TLabel;
    lblProjectName: TLabel;
    btnSave: TButton;
    Label2: TLabel;
    cbComPort: TComboBox;
    pnlMenuBar: TPanel;
    Label1: TLabel;
    txtNewProject: TEdit;
    Image1: TImage;
    btnSetup: TImage;
    btnCancel: TImage;
    Image3: TImage;
    btnMain: TImage;
    btnConfig: TImage;
    btnFind: TImage;
    btnHelp: TImage;
    btnInfo: TImage;
    btnExit: TImage;
    Image2: TImage;
    procedure FormShow(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure Image1MouseEnter(Sender: TObject);
    procedure pnlMenuBarMouseEnter(Sender: TObject);
    procedure pnlMenuBarMouseLeave(Sender: TObject);
    procedure btnMainMouseEnter(Sender: TObject);
    procedure btnConfigMouseEnter(Sender: TObject);
    procedure btnFindMouseEnter(Sender: TObject);
    procedure btnHelpClick(Sender: TObject);
    procedure btnInfoClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure btnMainClick(Sender: TObject);
    procedure btnConfigClick(Sender: TObject);
    procedure btnFindClick(Sender: TObject);
    procedure btnExitMouseEnter(Sender: TObject);
    procedure FormClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure cbComPortChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure txtNewProjectChange(Sender: TObject);
    procedure btnInfoMouseEnter(Sender: TObject);
    procedure btnSetupClick(Sender: TObject);
    procedure Image2Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
    gbSavePort: Boolean;
    procedure ComPortSelect;
    procedure SaveProject;
    procedure rhShowSettings(Sender: TObject);
  public
    { Public declarations }
    rh_ShowMain: TNotifyEvent;
    procedure rhPortClosed(Sender: TObject);
    procedure rhPortOpend(Sender: TObject);
    procedure FindComPort;
  end;

var
  SettingsFm: TSettingsFm;

implementation
uses AboutUn;
{$R *.dfm}

procedure TSettingsFm.btnCancelClick(Sender: TObject);
begin
  btnMainClick(nil);
end;

procedure TSettingsFm.btnConfigClick(Sender: TObject);
begin
{
  pnlMenuBar.Visible := False;
  if not gbSavePort or not (MessageDlg('Do you want to save the changes?', mtWarning, [mbYes, mbNo], 0) = mrYes) then
  begin
    gbSavePort := False;
    txtNewProject.Color := clWhite;
    txtNewProject.Text := VariablesUn.gsProjectName;
    SettingsFm.Hide;
    ConfigScreenFm.Show;
    ConfigScreenFm.btnGetStatusClick(nil);
  end
  else
    btnSave.Click;
  }
end;

procedure TSettingsFm.btnConfigMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TSettingsFm.btnHelpClick(Sender: TObject);
begin
  DataUn.dmDataModule.dsMWCChannels.Filtered := False;
  DataUn.dmDataModule.dsMWCChannels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
  DataUn.dmDataModule.dsMWCChannels.Filtered := True;

  if not dmDataModule.dsMWCChannels.Eof then
  begin
    Hide;
    //MWCfm.rh_ShowMain := rhShowMain;
    MWCConfigfm.Show;
  end
  else ShowMessage('No MWC Devices Found');

end;

procedure TSettingsFm.btnInfoClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  //Hide;
  AboutFm.rh_ShowForm := rhShowSettings;
  AboutFm.Show;
end;

procedure TSettingsFm.btnInfoMouseEnter(Sender: TObject);
begin
    pnlMenuBar.Visible := True;
end;

procedure TSettingsFm.FindComPort;
var
  I : Integer;
  S: string;
  begin
  try
    cbComPort.Items.Clear;
    for I := 1 to 50 do
    begin
     if IsPortAvailable(I) then
     begin
       S := Format('COM%d', [I]);
       //if ComportInUse= True then S:=S+'  [In use]';
       cbComPort.Items.Add(S);
     end;
    end;
  except
  on E: Exception do
  begin
    MessageDlg('TProjectFm.FindComPort: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;

procedure TSettingsFm.FormClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TSettingsFm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := False;
    btnExitClick(Nil);
end;

procedure TSettingsFm.FormCreate(Sender: TObject);
begin
  VariablesUn.giPortIndexOld := -1;
  FindComPort;
end;

procedure TSettingsFm.FormDestroy(Sender: TObject);
begin
    rh_ShowMain := nil;
end;

procedure TSettingsFm.FormHide(Sender: TObject);
begin
  AboutFm.Hide;
  VariablesUn.giTop := Top;
  VariablesUn.giLeft := Left;
end;

procedure TSettingsFm.FormShow(Sender: TObject);
begin
  Top := VariablesUn.giTop;
  Left := VariablesUn.giLeft;
  Screen.Cursor := crDefault;
  lblProjectName.Caption := VariablesUn.gsProjectName;
  txtNewProject.Text := VariablesUn.gsProjectName;
  gbSavePort := False;
  txtNewProject.Color := clWhite;
end;

procedure TSettingsFm.Image1MouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TSettingsFm.Image2Click(Sender: TObject);
var vsPortNumber: String;
    i, viPort, viPos: Integer;
begin
try
  Screen.Cursor := crHourGlass;
  VariablesUn.gsComPortUSB := ComFm.FindUSB;
  if (VariablesUn.gsComPortUSB <> '-1') and (VariablesUn.gsComPortUSB <> '') then
  begin
    viPos := pos('COM', VariablesUn.gsComPortUSB);
    vsPortNumber := Copy(VariablesUn.gsComPortUSB, viPos + 3, 50);
    if VariablesUn.IsNumber(vsPortNumber) then
    begin
      viPort := StrToInt(vsPortNumber);
      ComFm.ComPort.Open := false;
      VariablesUn.Delay(200);
      if IsPortAvailable(viPort) then
      begin
        ComFm.ComPort.ComNumber := viPort;
        ComFm.ComPort.Baud := 9600;

        VariablesUn.gbUSBfound := True;

        ComFm.ComPort.Open := True;
        VariablesUn.Delay(100);

        for i := 0 to cbComPort.Items.Count - 1 do
        begin
          if cbComPort.Items[i] = VariablesUn.gsComPortUSB then
          begin
            cbComPort.ItemIndex := i;
          end;
        end;

        if cbComPort.Text <> VariablesUn.gsComPortUSB then
        begin
          cbComPort.Items.Add(VariablesUn.gsComPortUSB);
          cbComPort.ItemIndex :=  cbComPort.Items.Count - 1;
        end;

        gbSavePort := True;
        txtNewProject.Color := $00A4FFFF;
        //ComUn.dmComModule.ComPort.ProcessCommunications;
        VariablesUn.giPortIndexOld := cbComPort.ItemIndex;
        MessageDlg('Found USB adapter (' + VariablesUn.gsComPortUSB + ').', mtInformation, [mbOk], 0);
      end
      else
        MessageDlg('Can not open port ' + VariablesUn.gsComPortUSB  + '. Check if USB adapter is connected and not in use by another application.', mtWarning, [mbOk], 0);
    end
    else
     MessageDlg('Can not find USB adapter.', mtWarning, [mbOk], 0);
  end
  else
    MessageDlg('Can not find USB adapter.', mtWarning, [mbOk], 0);

  Screen.Cursor := crDefault;
  except
  on E: Exception do
  begin
    Screen.Cursor := crDefault;
    MessageDlg('TSettingsFm.Image2Click: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;

procedure TSettingsFm.pnlMenuBarMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TSettingsFm.pnlMenuBarMouseLeave(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TSettingsFm.rhPortClosed(Sender: TObject);
begin
 Shape1.Brush.Color := clRed;;
 Shape2.Brush.Color := clRed;;
 Shape3.Brush.Color := clRed;;
 Shape4.Brush.Color := clRed;;
 Shape5.Brush.Color := clRed;;
end;

procedure TSettingsFm.rhPortOpend(Sender: TObject);
begin
 Shape1.Brush.Color := clLime;
 Shape2.Brush.Color := clLime;
 Shape3.Brush.Color := clLime;
 Shape4.Brush.Color := clLime;
 Shape5.Brush.Color := clLime;

 if not VariablesUn.gbUSBfound then
 begin
  Shape1.Brush.Color := clRed;;
  Shape2.Brush.Color := clRed;;
  Shape3.Brush.Color := clRed;;
  Shape4.Brush.Color := clRed;;
  Shape5.Brush.Color := clRed;;
 end;
end;

procedure TSettingsFm.rhShowSettings(Sender: TObject);
begin
  Show;
end;

procedure TSettingsFm.SaveProject;
begin
  try
  if not (DataUn.dmDataModule.dsProject.State = dsInactive) then
  begin
    Screen.Cursor := crHourGlass;
    DataUn.dmDataModule.dsProject.Filtered := False;
    DataUn.dmDataModule.dsProject.Filter :=  'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsProject.Filtered := True;


    if not (length(txtNewProject.Text) = 0) then
    begin
       if not (cbComPort.ItemIndex = -1) then
       begin
        if not DataUn.dmDataModule.dsProject.Eof then
        begin
           DataUn.dmDataModule.dsProject.Edit;
           DataUn.dmDataModule.dsProject.FieldByName('ProjectName').AsString := txtNewProject.Text;
           DataUn.dmDataModule.dsProject.FieldByName('ComPort').AsString := cbComPort.Text;
           DataUn.dmDataModule.dsProject.Post;
           dmDataModule.dsProject.MergeChangeLog;
           if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
           begin
           {
              DataUn.dmDataModule.dsMotors.Filtered := False;
              DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
              DataUn.dmDataModule.dsMotors.Filtered := True;
           }
              DataUn.dmDataModule.dsMotors.Filtered := False;
              DataUn.dmDataModule.dsMotors.First;
              while not dmDataModule.dsMotors.Eof do
              begin
                if (DataUn.dmDataModule.dsMotors.FieldByName('ProjectName').AsString = VariablesUn.gsProjectName) then
                begin
                  dmDataModule.dsMotors.Edit;
                  DataUn.dmDataModule.dsMotors.FieldByName('ProjectName').AsString := txtNewProject.Text;
                  DataUn.dmDataModule.dsMotors.Post;
                end;
                DataUn.dmDataModule.dsMotors.Next;
              end;
              dmDataModule.dsMotors.MergeChangeLog;
              //DataUn.dmDataModule.dsMotors.Filtered := False;
           end;
           DataUn.dmDataModule.dsProject.Filtered := False;
           DataUn.dmDataModule.dsProject.SaveToFile(VariablesUn.gsDataPath + 'dsProject.txt', dfXML);
           DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
           VariablesUn.gsProjectName := txtNewProject.Text;
           lblProjectName.Caption := VariablesUn.gsProjectName;
           gbSavePort := False;
           txtNewProject.Color := clWhite;
           pnlMenuBar.Visible := False;
           Screen.Cursor := crDefault;
           Hide;

           if Assigned (rh_ShowMain) then
              rh_ShowMain(Self);
        end
        else
        begin
          MessageDlg('Cannot find project!', mtWarning, [mbOk], 0);
        end;
       end
       else
       begin
         MessageDlg('Please select a COM Port.', mtWarning, [mbOk], 0);
       end;
    end
    else
    begin
      MessageDlg('Please enter the project name.', mtWarning, [mbOk], 0);
    end;
    Screen.Cursor := crDefault;
  end;
  except
  on E: Exception do
  begin
    Screen.Cursor := crDefault;
    MessageDlg('TSettingsFm.SaveProject: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;

procedure TSettingsFm.txtNewProjectChange(Sender: TObject);
begin
  gbSavePort := True;
  txtNewProject.Color := $00A4FFFF;
end;

procedure TSettingsFm.btnExitClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  if not gbSavePort or not (MessageDlg('Do you want to save the changes?', mtWarning, [mbYes, mbNo], 0) = mrYes) then
    Application.Terminate
  else
  begin
     btnSave.Click;
     Application.Terminate;
  end;
end;

procedure TSettingsFm.btnExitMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TSettingsFm.btnFindClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  if not gbSavePort or not (MessageDlg('Do you want to save the changes?', mtWarning, [mbYes, mbNo], 0) = mrYes) then
  begin
    gbSavePort := False;
    txtNewProject.Color := clWhite;
    txtNewProject.Text := VariablesUn.gsProjectName;
    pnlMenuBar.Visible := False;
    Hide;
    DiscoverFm.Show;
    end
  else
    btnSave.Click;
end;

procedure TSettingsFm.btnFindMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TSettingsFm.btnMainClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  if not gbSavePort or not (MessageDlg('Do you want to save the changes?', mtWarning, [mbYes, mbNo], 0) = mrYes) then
  begin
    gbSavePort := False;
    txtNewProject.Color := clWhite;
    txtNewProject.Text := VariablesUn.gsProjectName;
    Hide;
    if Assigned (rh_ShowMain) then
     rh_ShowMain(Self);
  end
  else
      btnSave.Click;
end;

procedure TSettingsFm.btnMainMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TSettingsFm.btnSaveClick(Sender: TObject);
begin
  if gbSavePort then
  begin
    SaveProject;
  end;
end;

procedure TSettingsFm.btnSetupClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  //Hide;
  AboutFm.rh_ShowForm := rhShowSettings;
  AboutFm.Show;
end;

procedure TSettingsFm.cbComPortChange(Sender: TObject);
begin
  ComPortSelect;
end;

procedure TSettingsFm.ComPortSelect;
var vsPortNumber: String;
    viPos, viPort: Integer;
begin
  try
    ActiveControl := nil;
    viPos := pos('COM', cbComPort.Text);
    if viPos > 0 then
    begin
      vsPortNumber := Copy(cbComPort.Text, viPos + 3, 50);
      viPort := StrToInt(vsPortNumber);
      if IsPortAvailable(viPort) then
      begin
        ComFm.ComPort.Open := false;
        VariablesUn.Delay(200);
        ComFm.ComPort.ComNumber := viPort;
        ComFm.ComPort.Baud := 9600;

        if  not ComFm.ComPort.Open then
        begin
          if VariablesUn.gsComPortUSB = cbComPort.Text then
            VariablesUn.gbUSBfound := True
          else
            VariablesUn.gbUSBfound := False;

           ComFm.ComPort.Open := True;
           VariablesUn.Delay(100);
        end;
        gbSavePort := True;
        txtNewProject.Color := $00A4FFFF;
        //ComUn.dmComModule.ComPort.ProcessCommunications;
        VariablesUn.giPortIndexOld := cbComPort.ItemIndex;
      end
      else
      begin
        cbComPort.ItemIndex := VariablesUn.giPortIndexOld;
        gbSavePort := True;
        txtNewProject.Color := $00A4FFFF;
        //MessageDlg('The selected port is in use. Please select another port.', mtWarning, [mbOk], 0);
      end;
    end;
    NewProjectFm.cbComPort.ItemIndex := cbComPort.ItemIndex;
  except
  on E: Exception do
  begin
    MessageDlg('TProjectFm.ComPortSelect: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;
end.
