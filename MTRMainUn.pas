unit MTRMainUn;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, VariablesUn, DataUn, Datasnap.DBClient,
  Data.DB, NewProjectUn, ProjectUn, ComUn, System.UITypes, Vcl.ImgList, ScreenSetupUn,
  Vcl.ToolWin, Vcl.ComCtrls, Vcl.Buttons, DiscoverUn, PowerPanelConfigUn,Vcl.Grids, Vcl.DBGrids, MTRConfigScreen, MWCConfigUn, Dwmapi,
  Vcl.Imaging.pngimage,MWCDisplayUn,ASCIIUn,System.Generics.Collections,IQMLC2ConfigUn,MNIConfigUn,StrUtils,MNI2ConfigUn,
  Vcl.Imaging.jpeg, JvComponentBase, JvCaptionButton,HtmlHelpViewer,HelpIntfs,MotorMaintenanceDisplay,ODDisplayUn;
type
  TIQMainFm = class(TForm)
    Image1: TImage;
    cbProject: TComboBox;
    pnlProject: TPanel;
    cmbMotors: TComboBox;
    lblUID: TLabel;
    Label2: TLabel;
    cmbPosition: TComboBox;
    cmbZGN: TComboBox;
    lblZGN: TLabel;
    cmbCmdType: TComboBox;
    pnlMenuBar: TPanel;
    btnMenu: TImage;
    cmbG: TComboBox;
    lblG: TLabel;
    txtAlias: TEdit;
    img0: TImage;
    btnAuto: TImage;
    btnStop: TImage;
    Image2: TImage;
    imgWinkUp: TImage;
    imgUp: TImage;
    imgIS1: TImage;
    imgIS2: TImage;
    imgIS3: TImage;
    imgDn: TImage;
    btnFind: TImage;
    btnInfo: TImage;
    btnExit: TImage;
    img10: TImage;
    img20: TImage;
    img30: TImage;
    img40: TImage;
    img50: TImage;
    img60: TImage;
    img70: TImage;
    img80: TImage;
    img90: TImage;
    img100: TImage;
    btnUID: TSpeedButton;
    imgSetpointSetOff: TImage;
    imgSetpointSetOn: TImage;
    imgSetpoint1: TImage;
    imgSetpoint2: TImage;
    imgSetpoint3: TImage;
    cmbDeviceSelector: TComboBox;
    Shape14: TShape;
    lblCmdType: TLabel;
    btnSettings: TImage;
    cmbMotorSelector: TComboBox;
    lblMotorNumber: TLabel;
    lblMotorTypeVal: TLabel;
    btnConfig: TImage;
    btnControl: TImage;
    cmbClearFlag: TComboBox;
    Label1: TLabel;
    JvCaptionButton1: TJvCaptionButton;
    Image4: TImage;
    btnLimitSetting: TImage;

    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure plNewProjectDblClick(Sender: TObject);
    procedure txtProjectNameDblClick(Sender: TObject);
    procedure Image1MouseEnter(Sender: TObject);
    procedure pnlMenuBarMouseEnter(Sender: TObject);
    procedure pnlMenuBarMouseLeave(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure btnExitMouseEnter(Sender: TObject);
    procedure btnFindMouseEnter(Sender: TObject);
    procedure btnFindClick(Sender: TObject);
    procedure imgDnClick(Sender: TObject);
    procedure imgUpClick(Sender: TObject);
    procedure imgUpMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure imgUpMouseLeave(Sender: TObject);
    procedure imgUpMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure imgDnMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure imgDnMouseLeave(Sender: TObject);
    procedure imgDnMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure imgWinkUpMouseLeave(Sender: TObject);
    procedure imgWinkUpMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure imgWinkDnClick(Sender: TObject);
    procedure imgWinkDnMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure imgWinkDnMouseLeave(Sender: TObject);
    procedure imgWinkDnMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure imgIS1Click(Sender: TObject);
    procedure imgIS1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure imgIS1MouseLeave(Sender: TObject);
    procedure imgIS1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure imgIS2MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure imgIS2MouseLeave(Sender: TObject);
    procedure imgIS2MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure imgIS3MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure imgIS3MouseLeave(Sender: TObject);
    procedure imgIS3MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure imgIS2Click(Sender: TObject);
    procedure imgIS3Click(Sender: TObject);
    procedure btnAutoClick(Sender: TObject);
    procedure btnStopClick(Sender: TObject);
    procedure imgWinkUpClick(Sender: TObject);
    procedure Wink(viWinkCount: Integer);
    procedure cmbMotorsChange(Sender: TObject);
    procedure ShadePosChange(Sender: TObject);
    procedure cmbPositionChange(Sender: TObject);
    procedure cmbPositionKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnMainClick(Sender: TObject);
    procedure btnMainMouseEnter(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton3MouseEnter(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure FormClick(Sender: TObject);
    procedure cmbCmdTypeChange(Sender: TObject);
    procedure cmbZGNChange(Sender: TObject);
    procedure cmbGChange(Sender: TObject);
    procedure btnConfigClick(Sender: TObject);
    procedure cbProjectChange(Sender: TObject);

    procedure lblAliasClick(Sender: TObject);
    procedure pnlProjectClick(Sender: TObject);
    procedure txtAliasDblClick(Sender: TObject);
    procedure txtAliasChange(Sender: TObject);
    procedure btnInfoClick(Sender: TObject);
    procedure imgSetupClick(Sender: TObject);
    procedure WinkUpClick(Sender: TObject);
    procedure btnUIDClick(Sender: TObject);
    procedure cmbMotorsExit(Sender: TObject);
    procedure imgSetpointSetOffClick(Sender: TObject);
    procedure imgSetpointSetOnClick(Sender: TObject);
    procedure btnHelpClick(Sender: TObject);
    procedure cmbDeviceSelectorChange(Sender: TObject);
    procedure cmbMotorSelectorChange(Sender: TObject);
    procedure btnConfigMouseEnter(Sender: TObject);
    procedure btnControlMouseEnter(Sender: TObject);
    procedure FormMouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure txtAliasEnterKey(Sender: TObject);
    procedure txtAliasKeyPress(Sender: TObject; var Key: Char);
    procedure Shape14MouseEnter(Sender: TObject);
    procedure cbProjectMouseEnter(Sender: TObject);
    procedure txtAliasMouseEnter(Sender: TObject);
    procedure cmbMotorsMouseEnter(Sender: TObject);
    procedure FormMouseLeave(Sender: TObject);
    procedure JvCaptionButton1Click(Sender: TObject);
    procedure cmbGKeyPress(Sender: TObject; var Key: Char);
    procedure cmbMotorsKeyPress(Sender: TObject; var Key: Char);
    procedure cmbZGNKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnLimitSettingMouseEnter(Sender: TObject);
    procedure btnLimitSettingClick(Sender: TObject);
    procedure Image3Click(Sender: TObject);
  private
    { Private declarations }
    gsUID: String;
    giLastPos: Integer;
    giComPortOn: Boolean;
    gsSetPoint: String;
    vsOldAlias:String;
    DoubleFoundFlag:boolean;
    PassFlag:Boolean;
    HoldTimer:TTimer;
    MoveTimer:TTimer;
    moveFailFlag:boolean;  //used to tell if a move command has suceeded in sending
    giUID:Integer;
    procedure ComboBox_AutoWidth(const theComboBox: TCombobox);
    procedure clearInhibit;
    procedure LongHoldEventUp(Sender: TObject);
    procedure LongHoldEventDN(Sender: TObject);
    procedure CreepUp(Sender: TObject);
    procedure CreepDN(Sender: TObject);
    Procedure MNIMotorDisable(MotorPerson:String);
    procedure ShowProject;
    procedure rhShowMain(Sender: TObject);
    procedure rhShowMainDiscover(Sender: TObject);
    procedure rhPortClosed(Sender: TObject);
    procedure rhPortOpend(Sender: TObject);
    procedure ButtonsEnable;
    procedure ButtonsDisable;
    procedure rhMoveRecieved(sender: TObject);
    procedure GetPosition(vsUID: String);
    procedure rhPositionRecieved(Sender: TObject);
    procedure rhSetpointRecieved(Sender: TObject);
    procedure ShowUID;
    procedure ShowZGN;
    procedure MovePrcntg(viPos: Integer);
    procedure SelectProject;
    procedure SetCom(Index: Integer);
    procedure DisplayPosition(viPos: Integer);
    procedure EditCancel;
    procedure SetSetpointsOn;
    procedure SetSetpointsOff;
    procedure DrawCaption;
    Procedure SortStringListUIDS(UIDList:TStringList);
    procedure WaitforGenericResponse(UID:Integer;Command:String;rh:TNotifyEvent;ResendTime:Integer);
    Function  IsMNI:Boolean;
    Function  IsIQMLC2:Boolean;
    function  IsIQMLC2Old:Boolean;
    function  IsIQMLC2EVB:Boolean;
    Function  IsDC:Integer;
    Function  GetUID(MixedUID:String):String;
    Function  GetCurrentUID:String;
    function  GetAddress(vsAddress: String): String;
    function  GetZGNAddress(vsAddress: String): String;
    function  MotorNumberSetBits(MotorNum:String):String;
  public
    { Public declarations }
  end;

var
  IQMainFm: TIQMainFm;

implementation
uses AboutUn, MidasLib, LimitSettingUn, IQ3ConfigUn, IQ3RFDisplayUn;
{$R *.dfm}

procedure TIQMainFm.btnAutoClick(Sender: TObject);
var vsHi, vsLo, vsCommand, vsType, vsZGN, vsG: String;
begin
  try
    if cmbClearFlag.Text='Selected' then
    begin
     clearInhibit;
    end
    else
    begin
      //clear all inhibits
      vsCommand := '>64.0.25.0.0.0';
      ComFm.SendCommand(vsCommand, NIL);
      cmbClearFlag.ItemIndex:=0;
    end;
  except
  on E: Exception do
  begin
    MessageDlg('TIQMainFm.btnAutoClick: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;

procedure TIQMainFm.clearInhibit;
var
  vsType,vsZGN,vsCommand,vsHi,vsLo:String;
  I,DataByte:Integer;
begin
//  if not (VariablesUn.giAdrType=2) or (VariablesUn.giAdrType=1)then
//  begin
    DataByte:=0;

    vsHi := IntToStr(Hi(StrToInt(GetCurrentUID)));
    vsLo := IntToStr(Lo(StrToInt(GetCurrentUID)));
    if (cmbCmdType.ItemIndex = 0) then
      vsType := '64'
    else
      vsType := '128';

    if VariablesUn.giAdrType=2 then
    begin
      if(IsMNI) or (isIQMLC2)then
      begin
        for I := 1 to 4 do
        begin
          if(IntToStr(I)<>cmbMotorSelector.Text )then
          begin
            DataUn.dmDataModule.dsMotors.Filtered := False;
            DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(cbProject.text) +  ' and UID = ' + GetCurrentUID + ' and MotorNumber = '+cmbMotorSelector.text;
            DataUn.dmDataModule.dsMotors.Filtered := True;
            if(DataUn.dmDataModule.dsMotors.FieldByName('Inhibit').AsString='On')and (IntToStr(I)<>cmbMotorSelector.Text)then
            begin
              DataByte:=VariablesUn.Set_a_Bit(DataByte,I-1);
            end;
          end;
        end;

        vsCommand:= '>' + vsHi + '.'+vsLo+'.' + VariablesUn.UID_MTA_Flag + '.0.0.'+IntToStr(DataByte);
      end
      else
      begin
        vsCommand:= '>' + vsHi + '.'+vsLo+'.' + VariablesUn.UID_MTA_Flag + '.0.0.0'
      end;
      ComFm.SendCommand(vsCommand,rhMoveRecieved);
      delay(500);
      GetPosition(GetCurrentUID);
    end
    else if VariablesUn.giAdrType=1 then
    begin
      vsZGN:= GetZGNAddress(cmbZGN.Text);
      vsCommand := '>' + vsType + '.0.' + VariablesUn.UID_MTA_Flag + '.'+vsZGN;
      ComFm.SendCommand(vsCommand,rhMoveRecieved);
      delay(500);
    end
    else if VariablesUn.giAdrType=0 then
    begin
      vsZGN:= GetZGNAddress(cmbG.Text);
//      if(IsGTest(cmbG.text))then
//      begin
//        vsCommand := '>' + vsType + '.0.' + VariablesUn.UID_MTA_Flag + '.255.'+vsZGN+'.255';
//      end
//      else
//      begin
        vsCommand := '>' + vsType + '.0.' + VariablesUn.UID_MTA_Flag + '.'+vsZGN;
//      end;
      ComFm.SendCommand(vsCommand,rhMoveRecieved);
      delay(500);
    end;

//    if isMNI or isIQMLC2 then
//    begin
//      if(variablesUn.giAdrType=0)then
//      begin
//        vsZGN := GetMNIAddress(cmbG.Text);
//      end
//      else if(variablesUn.giAdrType=1)then
//      begin
//        vsZGN := GetMNIAddress(cmbZGN.Text);
//      end
//      else if(variablesUn.giAdrType=2)then
//      begin
//        vsZGN := GetMNIAddress(cmbMotors.Text);
//      end
//    end
//    else
//    begin
//      if(variablesUn.giAdrType=0)then
//      begin
//        vsZGN := GetAddress(cmbG.Text);
//      end
//      else if(variablesUn.giAdrType=1)then
//      begin
//        vsZGN := GetAddress(cmbZGN.Text);
//      end
//      else if(variablesUn.giAdrType=2)then
//      begin
//        vsZGN := GetAddress(cmbMotors.Text);
//      end
//    end;
//
//    if (VariablesUn.giAdrType = 0) then
//    begin
//      if (VariablesUn.IsGTest(vsZGN))then
//      begin
//        vsCommand := '>' + vsType + '.0.' + VariablesUn.UID_MTA_Flag + '.255.'+vsZGN+'.255';
//      end else if(VariablesUn.isZGN(vsZGN))then
//      begin
//        vsCommand := '>' + vsType + '.0.' + VariablesUn.UID_MTA_Flag +'.'+vsZGN;
//      end
//      else
//      begin
//        MessageDlg('Please enter a valid Group# (0-255) or valid ZGN Address: n.n.n (n=0-255)', mtWarning, [mbOK], 0);
//        moveFailFlag:=true;
////          if cmbG.Items.Count > 0 then
////            cmbG.ItemIndex := 0;
//
//        exit;
//      end;
//    end
//    else if (VariablesUn.giAdrType = 1) then
//    begin
//      if VariablesUn.IsZGN(vsZGN) then
//      begin
//
//        vsCommand := '>' + vsType + '.0.' + VariablesUn.UID_MTA_Flag +'.'+vsZGN;
//      end
//      else
//      begin
//        MessageDlg('Please enter a valid ZGN Address: n.n.n (n=0-255)', mtWarning, [mbOK], 0);
//        moveFailFlag:=true;
////          if cmbZGN.Items.Count > 0 then
////            cmbZGN.ItemIndex := 0;
//
//        exit;
//      end;
//    end;
//    cmbPosition.Text := '---';
//    ComFm.SendCommand(vsCommand, rhMoveRecieved);
//
//    Delay(500);
//
//    VariablesUn.giShadePosition := -1;
//    giLastPos := -1;
//    GetPosition(GetCurrentUID);
//  end
//  else if VariablesUn.IsNumber(GetCurrentUID) then
//  begin
//    vsHi := IntToStr(Hi(StrToInt(GetCurrentUID)));
//    vsLo := IntToStr(Lo(StrToInt(GetCurrentUID)));
//    if not cmbMotorSelector.Visible then     //means its an MNI or IQMLC2
//    begin
//      vsCommand := '>' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_MTA_Flag + '.0.0.0';
//    end
//    else
//    begin
//      if isIQMLC2 then
//      begin
//        for I:=1 to 4 do
//        begin
//          if I<>StrToInt(cmbMotorSelector.Text) then
//          begin
//            DataUn.dmDataModule.dsMotors.Filtered := False;
//            DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(cbProject.text) +  ' and UID = ' + GetCurrentUID + ' and MotorNumber = '+cmbMotorSelector.text;
//            DataUn.dmDataModule.dsMotors.Filtered := True;
//            if(DataUn.dmDataModule.dsMotors.FieldByName('Inhibit').AsString = 'ON')then
//            begin
//
//            end;
//          end;
//        end;
//      end
//      else
//      begin
//        vsCommand := '>' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_MTA_Flag + '.0.0.'+MotorNumberSetBits(cmbMotorSelector.text);
//      end;
//    end;
//    cmbPosition.Text := '---';
//    ComFm.SendCommand(vsCommand, rhMoveRecieved);
//
//    Delay(500);
//
//    VariablesUn.giShadePosition := -1;
//    giLastPos := -1;
//    GetPosition(GetCurrentUID);
//  end
//  else
//  begin
//    MessageDlg('Please enter a valid UID (0 - 9999).', mtWarning, [mbOK], 0);
//    moveFailFlag:=true;
//  end;
end;

procedure TIQMainFm.btnMainClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TIQMainFm.btnMainMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TIQMainFm.btnStopClick(Sender: TObject);
var vsHi, vsLo, vsCommand, vsType, vsZGN, vsG: String;
begin
  try
    SetSetpointsOff;
    moveFailFlag:=false;
    if (VariablesUn.giAdrType=0) or (VariablesUn.giAdrType=1)then
    begin
      if (cmbCmdType.ItemIndex = 0) then
        vsType := '64'
      else
        vsType := '128';

      if isMNI or isIQMLC2 then
      begin
        if(variablesUn.giAdrType=1)then
        begin
          vsZGN := GetZGNAddress(cmbZGN.Text);
        end
        else if(variablesUn.giAdrType=2)then
        begin
          vsZGN := GetZGNAddress(cmbG.Text);
        end
      end
      else
      begin
        if(variablesUn.giAdrType=1)then
        begin
          vsZGN := GetAddress(cmbZGN.Text);
        end
        else if(variablesUn.giAdrType=0)then
        begin
          vsZGN := GetAddress(cmbG.Text);
        end
      end;

      if (VariablesUn.giAdrType = 0) then
      begin

        if not (pos('.',cmbG.text)>0)then
        begin
         if(StrToInt64(vsZGN)>2147483647)then
          begin
            MessageDlg('Please enter a valid Group# (0-255) or valid ZGN Address: n.n.n (n=0-255)', mtWarning, [mbOK], 0);
            moveFailFlag:=true;
            exit;
          end
          else
          begin
            vsZGN:='255.'+vsZGN+'.255'
          end;
        end;

      end;
      if VariablesUn.IsZGN(vsZGN) then
      begin

        vsCommand := '>' + vsType + '.0.' + VariablesUn.UID_STOP +'.'+vsZGN;
      end
      else
      begin
        MessageDlg('Please enter a valid ZGN Address: n.n.n (n=0-255)', mtWarning, [mbOK], 0);
        moveFailFlag:=true;
//          if cmbZGN.Items.Count > 0 then
//            cmbZGN.ItemIndex := 0;

        exit;
      end;
//      vsCommand := '>' + vsType + '.0.' + VariablesUn.UID_TOP +'.'+vsZGN;
      cmbPosition.Text := '---';

      if(IsIQMLC2EVB)then
      begin
        clearInhibit;
      end;
      ComFm.SendCommand(vsCommand, rhMoveRecieved);

      Delay(500);

      VariablesUn.giShadePosition := -1;
      giLastPos := -1;
      GetPosition(GetCurrentUID);
    end
    else if VariablesUn.IsNumber(GetCurrentUID) then
    begin
      vsHi := IntToStr(Hi(StrToInt(GetCurrentUID)));
      vsLo := IntToStr(Lo(StrToInt(GetCurrentUID)));
      if not cmbMotorSelector.Visible then     //means its an MNI or IQMLC2
      begin
        vsCommand := '>' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_STOP + '.0.0.0';
      end
      else
      begin
        vsCommand := '>' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_STOP + '.0.0.'+MotorNumberSetBits(cmbMotorSelector.text);
      end;
    end
    else
    begin
      MessageDlg('Please enter a valid UID (0 - 9999).', mtWarning, [mbOK], 0);
      moveFailFlag:=true;
    end;

    cmbPosition.Text := '---';
    ComFm.SendCommand(vsCommand, rhMoveRecieved);

    Delay(500);
    if isMNI then
    begin
      img0.Visible := True;
      img10.Visible := False;
      img20.Visible := False;
      img30.Visible := False;
      img40.Visible := False;
      img50.Visible := False;
      img60.Visible := False;
      img70.Visible := False;
      img80.Visible := False;
      img90.Visible := False;
      img100.Visible := False;
      cmbPosition.itemindex:=0;
    end
    else
    begin
      VariablesUn.giShadePosition := -1;
      giLastPos := -1;
      GetPosition(GetCurrentUID);
    end;

  except
  on E: Exception do
  begin
    MessageDlg('TIQMainFm.btnAutoClick: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;

function TIQMainFm.MotorNumberSetBits(MotorNum:String):String;
var MotorBits,MotorNumber:Integer;
begin
  MotorBits:=0;
  MotorNumber:= StrToInt(MotorNum);
  MotorBits:=VariablesUn.Set_a_Bit(MotorBits,MotorNumber-1);
  MotorNumberSetBits:=IntToStr(MotorBits);
end;

procedure TIQMainFm.btnUIDClick(Sender: TObject);
var I,viIndex: Integer;
begin
try
  SetSetpointsOff;
  if  btnUID.Caption = '>' then
  begin
    btnUID.Caption := '<';
    cmbMotors.Style := csDropDown;
    cmbMotors.SetFocus;
  end
  else
  begin
    //EditCancel;
    if cmbMotors.Items.Count > 0 then
    begin
      cmbMotors.OnChange := nil;

      cmbMotors.Enabled := True;

      for I := 0 to cmbMotors.Items.Count do
      begin
        if pos(VariablesUn.gsUIDCrnt,cmbMotors.Items[I])>=0 then
        begin
          cmbMotors.ItemIndex := I;
          break;
        end
        else
        begin
          cmbMotors.ItemIndex := 0;
        end;
      end;

//      viIndex := cmbMotors.Items.IndexOf(VariablesUn.gsUIDCrnt);
//      if viIndex >= 0 then
//        cmbMotors.ItemIndex := viIndex
//      else
//        cmbMotors.ItemIndex := 0;
    end;
  end;
finally
   cmbMotors.OnChange := cmbMotorsChange;
end;
end;

procedure TIQMainFm.ButtonsDisable;
begin
  imgUp.Enabled := False;
  imgDn.Enabled := False;
  imgWinkUp.Enabled := False;
  imgIS1.Enabled := False;
  imgIS2.Enabled := False;
  imgIS3.Enabled := False;
  btnAuto.Enabled := False;
  btnStop.Enabled := False;
  cmbPosition.Enabled := False;
  cmbZGN.Enabled := False;
  cmbG.Enabled := False;
  cmbMotors.Enabled := False;
  SetSetpointsOff;
  imgSetpointSetOn.Enabled := False;
end;

procedure TIQMainFm.ButtonsEnable;
begin
  imgUp.Enabled := True;
  imgDn.Enabled := True;
  imgWinkUp.Enabled := True;
  imgIS1.Enabled := True;
  imgIS2.Enabled := True;
  imgIS3.Enabled := True;
  imgIS1.visible := True;
  imgIS2.visible := True;
  imgIS3.visible := True;
  btnAuto.Enabled := True;
  btnStop.Enabled := True;
  //ShadePos.Enabled := True;
  cmbPosition.Enabled := True;
  cmbZGN.Enabled := True;
  cmbG.Enabled := True;
  cmbMotors.Enabled := True;
  SetSetpointsOff;
  if (VariablesUn.giAdrType <> 0) and (VariablesUn.giAdrType <> 1) then
    imgSetpointSetOn.Enabled := True;
end;

procedure TIQMainFm.cbProjectChange(Sender: TObject);
begin
  SetSetpointsOff;
  ActiveControl := nil;
  if cbProject.ItemIndex > -1 then
  begin
    VariablesUn.gsProjectName := cbProject.Text;
    SelectProject;
    ShowProject;
  end;
end;



procedure TIQMainFm.cbProjectMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TIQMainFm.cmbCmdTypeChange(Sender: TObject);
begin
  SetSetpointsOff;
  ActiveControl := nil;
  //cmbCmdType.ItemIndex := cmbCmdType.ItemIndex;
end;



procedure TIQMainFm.cmbDeviceSelectorChange(Sender: TObject);
begin
  if(cmbDeviceSelector.Text='MWC')then
  begin

    DataUn.dmDataModule.dsMWCChannels.Filtered := False;
    DataUn.dmDataModule.dsMWCChannels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsMWCChannels.Filtered := True;

    if not dmDataModule.dsMWCChannels.Eof then
    begin

      VariablesUn.giTop:=top;
      variablesUn.giLeft:=Left;
      MWCConfigfm.rh_ShowMain := rhShowMain;
      MWCDisplayfm.rh_ShowMain := rhShowMain;
      Hide;
      MWCDisplayfm.show;


      //MWCConfigfm.Show;
    end
    else
    begin
      ShowMessage('No MWC Devices Found');
      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('MTR');
    end;

  end
  else if (cmbDeviceSelector.Text='iQ3-DC-RF') then
  begin
    DataUn.dmDataModule.dsIQ3s.Filtered := False;
    DataUn.dmDataModule.dsIQ3s.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsIQ3s.Filtered := True;

    if not dmDataModule.dsIQ3s.Eof then
    begin

      VariablesUn.giTop:=top;
      variablesUn.giLeft:=Left;
      IQ3RFDisplayfm.Show;
      Hide;
      IQ3RFDisplayfm.rh_ShowMain:=rhShowMain;
      IQ3Configfm.rh_ShowMain:=rhShowMain;
      //MWCConfigfm.Show;
    end
    else
    begin
      ShowMessage('No iQ3 Devices Found');
      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('MTR');
    end;
  end
  else if (ContainsText(cmbDeviceSelector.Text,'iQ3-DC')) then
  begin
    DataUn.dmDataModule.dsIQ3s.Filtered := False;
    DataUn.dmDataModule.dsIQ3s.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsIQ3s.Filtered := True;

    if not dmDataModule.dsIQ3s.Eof then
    begin

      VariablesUn.giTop:=top;
      variablesUn.giLeft:=Left;
      IQ3RFDisplayfm.Show;
      Hide;
      IQ3RFDisplayfm.rh_ShowMain:=rhShowMain;
      IQ3Configfm.rh_ShowMain:=rhShowMain;
      //MWCConfigfm.Show;
    end
    else
    begin
      ShowMessage('No iQ3 Devices Found');
      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('MTR');
    end;
  end
  else if(cmbDeviceSelector.Text='MNI')then
  begin
    DataUn.dmDataModule.dsMNI.Filtered := False;
    DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsMNI.Filtered := True;

    if not dmDataModule.dsMNI.Eof then
    begin
      IQMainFm.Hide;
      MNIconfigFm.Show;
      VariablesUn.giTop := Top;
      VariablesUn.giLeft := Left;
    end
    else
    begin
      ShowMessage('No MNI Devices Found');
      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('MTR');
    end;
  end
  else if(cmbDeviceSelector.Text='MDC')then
  begin
    DataUn.dmDataModule.dsMNI2.Filtered := False;
    DataUn.dmDataModule.dsMNI2.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsMNI2.Filtered := True;

    if not dmDataModule.dsMNI2.Eof then
    begin
      IQMainFm.Hide;
      VariablesUn.giTop := Top;
      VariablesUn.giLeft := Left;
      MNI2configFm.Show;
      MNI2ConfigFm.GetVirtualMotorsStatus(self);
    end
    else
    begin
      ShowMessage('No MDC Devices Found');
      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('MTR');
    end;
  end
  else if(cmbDeviceSelector.Text='IQMLC2')then
  begin
    DataUn.dmDataModule.dsIQMLC2.Filtered := False;
    DataUn.dmDataModule.dsIQMLC2.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsIQMLC2.Filtered := True;

    if not dmDataModule.dsIQMLC2.Eof then
    begin
      IQMainFm.Hide;
      VariablesUn.giTop := Top;
      VariablesUn.giLeft := Left;
      IQMLC2configFm.Show;
    end
    else
    begin
      ShowMessage('No IQMLC2 Devices Found');
      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('MTR');
    end;
  end
  else if(cmbDeviceSelector.Text='PowerPanel')then
  begin
    DataUn.dmDataModule.dsPowerPanel.Filtered := False;
    DataUn.dmDataModule.dsPowerPanel.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsPowerPanel.Filtered := True;

    if not dmDataModule.dsPowerPanel.Eof then
    begin
      IQMainFm.Hide;
      VariablesUn.giTop := Top;
      VariablesUn.giLeft := Left;
      PowerPanelConfigFm.Show;
    end
    else
    begin
      ShowMessage('No IQ2-DC Power Panel Devices Found');
      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('MTR');
    end;

  end
  else if(cmbDeviceSelector.Text='Motor Maint')then
  begin
    DataUn.dmDataModule.dsMotors.Filtered := False;
    DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) + ' and MotorType = 69 or MotorType = 66 or MotorType = 70';
    DataUn.dmDataModule.dsMotors.Filtered := True;

    if not dmDataModule.dsMotors.Eof then
    begin
      VariablesUn.giTop := Top;
      VariablesUn.giLeft := Left;
      while not dmDataModule.dsMotors.Eof do
      begin
        if GetUID(cmbMotors.Text)=IntToStr(DataUn.dmDataModule.dsMotors.FieldByName('UID').AsInteger) then
        begin
          IQMainFm.Hide;
          MotorMaintenanceDisplayFm.Show;
          MotorMaintenanceDisplayFm.cmbUidMM.ItemIndex:= MotorMaintenanceDisplayFm.cmbUidMM.Items.IndexOf(GetUID(cmbMotors.Text));
          MotorMaintenanceDisplayFm.cmbUidMM.OnChange(self);
          exit;
        end;
        dmDataModule.dsMotors.Next;
      end;
      IQMainFm.Hide;
      MotorMaintenanceDisplayFm.Show;
      MotorMaintenanceDisplayFm.cmbUidMM.ItemIndex:= 0;
      MotorMaintenanceDisplayFm.cmbUidMM.OnChange(self);
    end
    else
    end
  else if(cmbDeviceSelector.Text='Obstacle Detection')then
  begin
    DataUn.dmDataModule.dsMotors.Filtered := False;
    DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) + ' and MotorType = 70';
    DataUn.dmDataModule.dsMotors.Filtered := True;

    if not dmDataModule.dsMotors.Eof then
    begin
      VariablesUn.giTop := Top;
      VariablesUn.giLeft := Left;
      while not dmDataModule.dsMotors.Eof do
      begin
        if GetUID(cmbMotors.Text)=IntToStr(DataUn.dmDataModule.dsMotors.FieldByName('UID').AsInteger) then
        begin
          IQMainFm.Hide;
          ODFm.Show;
          ODFm.cmbUidMM.ItemIndex:= ODFm.cmbUidMM.Items.IndexOf(GetUID(cmbMotors.Text));
          ODFm.cmbUidMM.OnChange(self);
          exit;
        end;
        dmDataModule.dsMotors.Next;
      end;
      IQMainFm.Hide;
      ODFm.Show;
      ODFm.cmbUidMM.ItemIndex:= 0;
      ODFm.cmbUidMM.OnChange(self);
    end
    else
    begin
      ShowMessage('No iQ3 motors Found');
      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('MTR');
    end;
  end;
end;

procedure TIQMainFm.cmbGChange(Sender: TObject);
var I,viPos, viIndex: Integer;
    vsUID: String;
begin
  //ActiveControl := nil;
  SetSetpointsOff;

  if cmbG.ItemIndex <> -1 then
    cmbZGN.ItemIndex := cmbG.ItemIndex;

  viPos := pos(':', cmbG.Text);
  if viPos > 0 then
  begin
    vsUID := Copy(cmbG.Text, 0, viPos - 1);
    for I := 0 to cmbMotors.Items.Count do
    begin
      if pos(vsUID,cmbMotors.Items[I])>0 then
      begin
        cmbMotors.ItemIndex := viIndex;
        cmbMotorsChange(cmbMotors);
        break;
      end;
    end;

    //viIndex := cmbMotors.Items.IndexOf(vsUID);

//    if viIndex >= 0 then
//    begin
//      cmbMotors.ItemIndex := viIndex;
//      cmbMotorsChange(cmbMotors);
//    end;
  end;
end;

procedure TIQMainFm.cmbGKeyPress(Sender: TObject; var Key: Char);
var
  i,periodCount:Integer;
begin

  if not (Key in [#8, '0'..'9', '.']) then begin
    Key := #0;
  end
  else if (Key = '.')then// and (Pos(Key, cmbG.Text) > 0) then begin
  begin
    periodCount:=0;
    for i := 1 to Length(cmbG.Text) do
    begin
      if cmbG.Text[i] = '.' then
      begin
        inc(periodCount);
      end;
    end;
    if(periodCount=2)then
    begin
      Key := #0;
    end;
  end;
end;

Procedure TIQMainFm.MNIMotorDisable(MotorPerson:String);
begin
  if MotorPerson='0' then
  begin
    ButtonsDisable;
  end
  else if MotorPerson='1' then
  begin
    ButtonsEnable;
    imgIS1.visible:=false;
    imgIS3.visible:=false;
    cmbPosition.Enabled:=false;

  end
  else if MotorPerson='2' then
  begin
    ButtonsEnable;
  end
  else if MotorPerson='3' then
  begin
    ButtonsEnable;
    imgIS1.visible:=false;
    imgIS3.visible:=false;
    cmbPosition.Enabled:=false;

  end
  else if MotorPerson='4' then
  begin
    ButtonsEnable;
    imgIS1.visible:=false;
    imgIS2.visible:=false;
    imgIS3.visible:=false;
    cmbPosition.Enabled:=false;

  end
  else if MotorPerson='5' then
  begin
    ButtonsEnable;
    imgIS1.visible:=false;
    imgIS2.visible:=false;
    imgIS3.visible:=false;
    cmbPosition.Enabled:=false;

  end
  else if MotorPerson='6' then
  begin
    ButtonsEnable;
    imgIS1.visible:=false;
    imgIS3.visible:=false;
    cmbPosition.Enabled:=false;

  end
  else if MotorPerson='7' then
  begin
    ButtonsEnable;
    imgIS1.visible:=false;
    imgIS3.visible:=false;
    cmbPosition.Enabled:=false;

  end
  else if MotorPerson='8' then
  begin
    ButtonsEnable;
    imgIS1.visible:=false;
    imgIS3.visible:=false;
    cmbPosition.Enabled:=false;

  end
  else if MotorPerson='9' then
  begin
    ButtonsEnable;
  end;
end;

procedure TIQMainFm.cmbMotorsChange(Sender: TObject);
var UID:String;
begin
try
  SetSetpointsOff;
  //if (cmbMotors.ItemIndex <> -1) then
    //EditCancel;

//  if VariablesUn.giAdrType = 2 then
//  begin
//    UID:=GetCurrentUID;
//  end;

  If VariablesUn.IsNumber(GetCurrentUID) and not (StrToInt(GetCurrentUID) < 0) then
  begin
    gsUID := GetCurrentUID;

    VariablesUn.gsUIDCrnt := GetCurrentUID;
    txtAlias.OnChange := nil;
    cmbMotorSelector.ItemIndex:=0;
    if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
    begin
      DataUn.dmDataModule.dsMotors.Filtered := False;
      DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt;
      DataUn.dmDataModule.dsMotors.Filtered := True;

      if not DataUn.dmDataModule.dsMotors.Eof then
      begin
        txtAlias.Text := DataUn.dmDataModule.dsMotors.FieldByName('Alias').AsString;
        VariablesUn.gsAlias := txtAlias.Text;


        if (DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='74') or(DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='65') then
        begin
          if IsIQMLC2EVB or IsIQMLC2Old then
          begin
            cmbPosition.Visible:=False;
            Label2.Visible:=false;
          end
          else
          begin
            cmbPosition.Visible:=True;
            Label2.Visible:=true;
          end;
          if (VariablesUn.giAdrType = 2) then
          begin
            cmbMotorSelector.Visible:=true;
            lblMotorNumber.Visible:=true;
          end
          else
          begin
            cmbMotorSelector.Visible:=False;
            lblMotorNumber.Visible:=False;
          end;


          if (DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='74')then
          begin
            lblMotorTypeVal.caption:='MNI';
          end
          else
          begin
            lblMotorTypeVal.caption:='IQMLC2';
          end;
          DataUn.dmDataModule.dsMotors.Filtered := False;
          DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt + ' and MotorNumber = 1' ;
          DataUn.dmDataModule.dsMotors.Filtered := True;
          MNIMotorDisable(DataUn.dmDataModule.dsMotors.FieldByName('MotorPerson').AsString);
          img0.Visible := True;
          img10.Visible := False;
          img20.Visible := False;
          img30.Visible := False;
          img40.Visible := False;
          img50.Visible := False;
          img60.Visible := False;
          img70.Visible := False;
          img80.Visible := False;
          img90.Visible := False;
          img100.Visible := False;
          cmbPosition.itemindex:=0;
        end
        else if DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='66' then
        begin
          cmbPosition.Visible:=True;
          Label2.Visible:=true;
          cmbMotorSelector.Visible:=False;
          lblMotorNumber.Visible:=False;
          lblMotorTypeVal.caption:='IQ2';
          ButtonsEnable;
        end
         else if DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='70' then
        begin
          cmbPosition.Visible:=True;
          Label2.Visible:=true;
          cmbMotorSelector.Visible:=False;
          lblMotorNumber.Visible:=False;
          lblMotorTypeVal.caption:='iQ3-DC';
          ButtonsEnable;
        end
        else if (DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='77') or (DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='205') then
        begin
          cmbPosition.Visible:=True;
          Label2.Visible:=true;
          cmbMotorSelector.Visible:=False;
          lblMotorNumber.Visible:=False;
          lblMotorTypeVal.caption:='DC50';
          ButtonsEnable;
        end
        else if (DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='78') or (DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='206') then
        begin
          cmbPosition.Visible:=True;
          Label2.Visible:=true;
          cmbMotorSelector.Visible:=False;
          lblMotorNumber.Visible:=False;
          lblMotorTypeVal.caption:='AC50';
          ButtonsEnable;
        end
        else if (DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='79')  or (DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='207') then
        begin
          cmbPosition.Visible:=True;
          Label2.Visible:=true;
          cmbMotorSelector.Visible:=False;
          lblMotorNumber.Visible:=False;
          lblMotorTypeVal.caption:='st30';
          ButtonsEnable;
        end
        else if (DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='69') then
        begin
          cmbPosition.Visible:=True;
          Label2.Visible:=true;
          cmbMotorSelector.Visible:=False;
          lblMotorNumber.Visible:=False;
          lblMotorTypeVal.caption:='IQ2-DC';
          ButtonsEnable;
        end
        else
        begin
          ButtonsEnable;
        end;
//        if( DataUn.dmDataModule.dsMotors.FieldByName('HostUID').AsInteger<>0)then
//        begin
//          lblHostUIDVal.caption:= DataUn.dmDataModule.dsMotors.FieldByName('HostUID').AsString;
//        end
//        else
//        begin
//          lblHostUIDVal.caption:='N/A';
//        end;
      end;

      DataUn.dmDataModule.dsMotors.Filtered := False;
    end;

    if giComPortOn then
    begin
      VariablesUn.giShadePosition := -1;
      cmbPosition.Text := '---';
      GetPosition(GetCurrentUID);
    end;
    txtAlias.OnChange := txtAliasChange;
  end
  else
  begin
    if not (cmbMotors.Text = '') then
    begin      MessageDlg('Please enter a valid UID (0 - 9999).', mtWarning, [mbOK], 0);
      if cmbMotors.Items.Count > 0 then
      begin
        cmbMotors.ItemIndex := 0;
        cmbMotorsChange(cmbMotors);
        //EditCancel;
      end;
    end
    else
    begin
      cmbPosition.Visible := True;
      Label2.Visible:=true;
      VariablesUn.giShadePosition := -1;
      cmbPosition.Text := '---';
      //ShowZGN;
    end;
  end;
  except
  on E: Exception do
  begin
    txtAlias.OnChange := txtAliasChange;
    MessageDlg('TIQMainFm.imgDn_upClick: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;
//
//procedure TIQMainFm.cmbMotorsDrawItem(Control: TWinControl; Index: Integer;
//  Rect: TRect; State: TOwnerDrawState);
//begin
//  .Canvas.TextRect(Rect, Rect.Left, Rect.Top, ComboBox1.Items.Names[Index]);
//end;

procedure TIQMainFm.cmbMotorSelectorChange(Sender: TObject);
begin
  DataUn.dmDataModule.dsMotors.Filtered := False;
  DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt + ' and MotorNumber = '+cmbMotorSelector.text;
  DataUn.dmDataModule.dsMotors.Filtered := True;
  MNIMotorDisable(DataUn.dmDataModule.dsMotors.FieldByName('MotorPerson').asString);
end;

procedure TIQMainFm.cmbMotorsExit(Sender: TObject);
begin
  if cmbMotors.Text = '' then
  begin
    if cmbMotors.Items.Count > 0 then
    begin
      cmbMotors.ItemIndex := 0;
    end;
    //EditCancel;
  end;
end;

procedure TIQMainFm.cmbMotorsKeyPress(Sender: TObject; var Key: Char);
begin
  if not (Key in [#8, '0'..'9']) then begin
    Key := #0;
  end;
end;

procedure TIQMainFm.cmbMotorsMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TIQMainFm.cmbPositionChange(Sender: TObject);
var viPos: Integer;
begin
  SetSetpointsOff;
  If VariablesUn.IsNumber(cmbPosition.Text) and (StrToInt(cmbPosition.Text) >= 0) and (StrToInt(cmbPosition.Text) <= 100) then
  begin
    if giComPortOn then
    begin
      if VariablesUn.giAdrType=2 then
      begin
        if VariablesUn.IsNumber(GetCurrentUID) and not (cmbPosition.ItemIndex = -1) then
        begin
          if(StrToInt(GetCurrentUID)<65535)and(StrToInt(GetCurrentUID)>=0)then
          begin
            viPos := StrToInt(cmbPosition.Text);
            VariablesUn.giShadePosition := -1;
            cmbPosition.Text := '---';
            MovePrcntg(viPos);
          end
          else
          begin
            MessageDlg('Please enter a valid UID (0 - 9999).', mtWarning, [mbOK], 0);
          end;
        end;
      end
      else
      begin
        viPos := StrToInt(cmbPosition.Text);
        VariablesUn.giShadePosition := -1;
        cmbPosition.Text := '---';
        MovePrcntg(viPos);
      end;
    end;
  end
  else
  begin
    MessageDlg('Please enter a valid position (0 - 100).', mtWarning, [mbOK], 0);
    if cmbPosition.Items.Count > 0 then
      cmbPosition.ItemIndex := 50;
  end;
end;

procedure TIQMainFm.cmbPositionKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  var viPos: Integer;
begin
  SetSetpointsOff;
  if Key = 13 then
  begin
    If VariablesUn.IsNumber(cmbPosition.Text) and (StrToInt(cmbPosition.Text) >= 0) and (StrToInt(cmbPosition.Text) <= 100) then
    begin
      if giComPortOn and VariablesUn.IsNumber(GetCurrentUID)then
      begin
        viPos := StrToInt(cmbPosition.Text);
        VariablesUn.giShadePosition := -1;
        cmbPosition.Text := '---';
        MovePrcntg(viPos);
      end;
    end
    else
    begin
      MessageDlg('Please enter a valid position (0 - 100).', mtWarning, [mbOK], 0);
      if cmbPosition.Items.Count > 0 then
        cmbPosition.ItemIndex := 50;
    end;
  end;
end;

procedure TIQMainFm.cmbZGNChange(Sender: TObject);
var I,viPos, viIndex: Integer;
    vsUID: String;
begin
  //ActiveControl := nil;
  SetSetpointsOff;

  viPos := pos(':', cmbZGN.Text);
  if viPos > 0 then
  begin
   vsUID := Copy(cmbZGN.Text, 0, viPos - 1);
   for I := 0 to cmbMotors.Items.Count do
    begin
      if pos(vsUID,cmbMotors.Items[I])>0 then
      begin
        cmbMotors.ItemIndex := viIndex;
        cmbMotorsChange(cmbMotors);
        break;
      end;
    end;

//   viIndex := cmbMotors.Items.IndexOf(vsUID);
//
//   if viIndex >= 0 then
//   begin
//    cmbMotors.ItemIndex := viIndex;
//    cmbMotorsChange(cmbMotors);
//   end;
  end;
end;



procedure TIQMainFm.cmbZGNKeyPress(Sender: TObject; var Key: Char);
var
  i,periodCount:Integer;
begin
 if not (Key in [#8, '0'..'9', '.']) then
  begin
    Key := #0;
  end
  else if (Key = '.')then// and (Pos(Key, cmbG.Text) > 0) then begin
  begin
    periodCount:=0;
    for i := 1 to Length(cmbG.Text) do
    begin
      if cmbG.Text[i] = '.' then
      begin
        inc(periodCount);
      end;
    end;
    if(periodCount=2)then
    begin
      Key := #0;
    end;
  end;
end;

procedure TIQMainFm.DisplayPosition(viPos: Integer);
begin
   case viPos of
    -1,0,1,2,3:
    begin
      img0.Visible := True;
      img10.Visible := False;
      img20.Visible := False;
      img30.Visible := False;
      img40.Visible := False;
      img50.Visible := False;
      img60.Visible := False;
      img70.Visible := False;
      img80.Visible := False;
      img90.Visible := False;
      img100.Visible := False;
    end;
    4,5,6,7,8,9,10,11,12,13:
    begin
      img0.Visible := False;
      img10.Visible := True;
      img20.Visible := False;
      img30.Visible := False;
      img40.Visible := False;
      img50.Visible := False;
      img60.Visible := False;
      img70.Visible := False;
      img80.Visible := False;
      img90.Visible := False;
      img100.Visible := False;
    end;
    14,15,16,17,18,19,20,21,22,23:
    begin
      img0.Visible := False;
      img10.Visible := False;
      img20.Visible := True;
      img30.Visible := False;
      img40.Visible := False;
      img50.Visible := False;
      img60.Visible := False;
      img70.Visible := False;
      img80.Visible := False;
      img90.Visible := False;
      img100.Visible := False;
    end;
    24,25,26,27,28,29,30,31,32,33:
    begin
      img0.Visible := False;
      img10.Visible := False;
      img20.Visible := False;
      img30.Visible := True;
      img40.Visible := False;
      img50.Visible := False;
      img60.Visible := False;
      img70.Visible := False;
      img80.Visible := False;
      img90.Visible := False;
      img100.Visible := False;
    end;
    34,35,36,37,38,39,40,41,42,43:
    begin
      img0.Visible := False;
      img10.Visible := False;
      img20.Visible := False;
      img30.Visible := False;
      img40.Visible := True;
      img50.Visible := False;
      img60.Visible := False;
      img70.Visible := False;
      img80.Visible := False;
      img90.Visible := False;
      img100.Visible := False;
    end;
    44,45,46,47,48,49,50,51,52,53:
    begin
      img0.Visible := False;
      img10.Visible := False;
      img20.Visible := False;
      img30.Visible := False;
      img40.Visible := False;
      img50.Visible := True;
      img60.Visible := False;
      img70.Visible := False;
      img80.Visible := False;
      img90.Visible := False;
      img100.Visible := False;
    end;
    54,55,56,57,58,59,60,61,62,63:
    begin
      img0.Visible := False;
      img10.Visible := False;
      img20.Visible := False;
      img30.Visible := False;
      img40.Visible := False;
      img50.Visible := False;
      img60.Visible := True;
      img70.Visible := False;
      img80.Visible := False;
      img90.Visible := False;
      img100.Visible := False;
    end;
    64,65,66,67,68,69,70,71,72,73:
    begin
      img0.Visible := False;
      img10.Visible := False;
      img20.Visible := False;
      img30.Visible := False;
      img40.Visible := False;
      img50.Visible := False;
      img60.Visible := False;
      img70.Visible := True;
      img80.Visible := False;
      img90.Visible := False;
      img100.Visible := False;
    end;
    74,75,76,77,78,79,80,81,82,83:
    begin
      img0.Visible := False;
      img10.Visible := False;
      img20.Visible := False;
      img30.Visible := False;
      img40.Visible := False;
      img50.Visible := False;
      img60.Visible := False;
      img70.Visible := False;
      img80.Visible := True;
      img90.Visible := False;
      img100.Visible := False;
    end;
    84,85,86,87,88,89,90,91,92,93:
    begin
      img0.Visible := False;
      img10.Visible := False;
      img20.Visible := False;
      img30.Visible := False;
      img40.Visible := False;
      img50.Visible := False;
      img60.Visible := False;
      img70.Visible := False;
      img80.Visible := False;
      img90.Visible := True;
      img100.Visible := False;
    end;
    else
    begin
      img0.Visible := False;
      img10.Visible := False;
      img20.Visible := False;
      img30.Visible := False;
      img40.Visible := False;
      img50.Visible := False;
      img60.Visible := False;
      img70.Visible := False;
      img80.Visible := False;
      img90.Visible := False;
      img100.Visible := True;
    end;
   end;
end;

procedure TIQMainFm.DrawCaption;
var
  ACanvas: TCanvas;
  AllowNCPaintValue: Bool;
  RenderingPolicy: UInt32;
begin
  inherited;
  ACanvas := TCanvas.Create;
  try
    if DwmCompositionEnabled then
    begin
     RenderingPolicy := DWMNCRP_DISABLED;
     if DwmSetWindowAttribute(Handle, DWMWA_NCRENDERING_POLICY, @RenderingPolicy, sizeof(RenderingPolicy)) = S_Ok then
     begin
       //AllowNCPaintValue := True;
       //DwmSetWindowAttribute(Handle, DWMWA_ALLOW_NCPAINT, @AllowNCPaintValue, sizeof(AllowNCPaintValue));
     end;
    end;

    ACanvas.Handle := GetWindowDC(Self.Handle);
    with ACanvas do
    begin
      Brush.Style := bsClear;
      Font.Name := 'Tahoma';
      Font.Size := 11;
      Font.Color := clBlack;
      Font.Style := [fsBold];
      TextOut(GetSystemMetrics(SM_CYMENU) + GetSystemMetrics(SM_CXBORDER), Round((GetSystemMetrics(SM_CYCAPTION) - Abs(Font.Height)) / 2), 'Mechonet Programming Software');
    end;
  finally
    ReleaseDC(Self.Handle, ACanvas.Handle);
    ACanvas.Free;
  end;
end;

procedure TIQMainFm.EditCancel;
begin
    btnUID.Caption := '>';
    cmbMotors.Style := csDropDownList;
    ActiveControl := nil;
end;

procedure TIQMainFm.FormClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TIQMainFm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  btnExitClick(nil);
end;

procedure TIQMainFm.FormCreate(Sender: TObject);
var i: Integer;
begin
  //DC1List:=TObjectList<TDC1ClassUn>.Create();
  VariablesUn.giTop := -1;
  VariablesUn.giLeft := -1;
  //GetDir(0, VariablesUn.gsAppPath);
  VariablesUn.gsDataPath:='C:\MPSdata\';
  //VariablesUn.gsDataPath := VariablesUn.gsAppPath + '\data\';
  VariablesUn.gsProjectName := 'Test';
  giComPortOn := False;
  VariablesUn.giShadePosition := -1;
  giLastPos := -1;
  gsSetPoint := '';
  VariablesUn.giAdrType := 2;
  VariablesUn.giCmdType := 0;
  VariablesUn.gsUIDCrnt := '';
  VariablesUn.gbUSBfound := False;
  VariablesUn.gbShowSTA := False;
  VariablesUn.gsComPortUSB := '-1';
  VariablesUn.gbPowerOn := True;
  VariablesUn.gbGoToConfig := False;
  //VariablesUn.giLastForm:='Load';
  for i := 0 to 100 do
  begin
   cmbPosition.Items.Add(IntToStr(i));
  end;
  ComboBox_AutoWidth(cmbDeviceSelector);
 //EnableMenuItem(GetSystemMenu(handle, False), SC_CLOSE, MF_BYCOMMAND or MF_GRAYED); //disable border icon (exit)
end;

//Allows combox  dropdown to size to oversized string items
procedure TIQMainFm.ComboBox_AutoWidth(const theComboBox: TCombobox);
const
  HORIZONTAL_PADDING = 4;
var
  itemsFullWidth: integer;
  idx: integer;
  itemWidth: integer;
begin
  itemsFullWidth := 0;
  // get the max needed with of the items in dropdown state
  for idx := 0 to -1 + theComboBox.Items.Count do
  begin
    itemWidth := theComboBox.Canvas.TextWidth(theComboBox.Items[idx]);
    Inc(itemWidth, 2 * HORIZONTAL_PADDING);
    if (itemWidth > itemsFullWidth) then itemsFullWidth := itemWidth;
  end;
  itemsFullWidth:=itemsFullWidth+20;
    // set the width of drop down if needed
  if (itemsFullWidth > theComboBox.Width) then
  begin
    //check if there would be a scroll bar
    if theComboBox.DropDownCount < theComboBox.Items.Count then
      itemsFullWidth := itemsFullWidth + GetSystemMetrics(SM_CXVSCROLL);
    SendMessage(theComboBox.Handle, CB_SETDROPPEDWIDTH, itemsFullWidth, 0);
  end;
end;
procedure TIQMainFm.FormHide(Sender: TObject);
begin
  VariablesUn.giTop := Top;
  VariablesUn.giLeft := Left;
  SetSetpointsOff;
end;

procedure TIQMainFm.FormMouseLeave(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TIQMainFm.FormMouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
begin
  if Screen.ActiveControl = cmbClearFlag then Handled := True;
end;

procedure TIQMainFm.FormShow(Sender: TObject);
begin
  Application.HelpFile := VariablesUn.gsDataPath + 'MechoNet Programming Software User Guide.chm';
  HoldTimer:=TTimer.Create(self);
  MoveTimer:=TTimer.Create(self);
  btnFind.Enabled:=false;
  cmbClearFlag.ItemIndex:=0;
  cmbDeviceSelector.ItemIndex:=0;
  if(VariablesUn.Debug)then
  begin
    Left:=1925;
    Top:=300
  end
  else
  begin
    if VariablesUn.giTop=-1 then
    begin
      Top := (Screen.Height-Height) div 2;
      Left := (Screen.Width-Width)  div 2;
    end
    else
    begin
      Top := VariablesUn.giTop;
      Left := VariablesUn.giLeft;
    end;
  end;

  Screen.Cursor := crDefault;
  cmbZGN.Style := csDropDownList;
  cmbG.Style := csDropDownList;
  cmbMotors.Color := clWindow;
  cmbZGN.Color := clWindow;
  cmbG.Color := clWindow;
  pnlMenuBar.Visible := False;
  cmbZGN.Clear;
  cmbG.Clear;
  cmbMotors.Clear;

  MWCDisplayFm.rh_GetMNI2VirtualMotorStatus:=MNI2ConfigFm.GetVirtualMotorsStatus;
  MWCConfigFm.rh_GetMNI2VirtualMotorStatus:=MNI2ConfigFm.GetVirtualMotorsStatus;
  //NewProjectFm.rh_ShowLast := rhShowMain;
  DiscoverFm.rh_ShowMain := rhShowMain;
  //ScreenSetupFm.rh_ShowMain := rhShowMain;
  ConfigScreenFm.rh_ShowMain := rhShowMain;
  ComFm.rh_PortOpend1 := rhPortOpend;
  ComFm.rh_PortClosed1 := rhPortClosed;
  ComFm.rh_PortOpend2 := rhPortOpend;
  ComFm.rh_PortClosed2 := rhPortClosed;
  MWCConfigfm.rh_ShowMain  := rhShowMain;
  MWCDisplayfm.rh_ShowMain  := rhShowMain;
  ConfigScreenfm.rh_ShowMain := rhShowMain;
  MNIConfigFm.rh_ShowMain:=rhShowMain;
  PowerPanelConfigFm.rh_ShowMain:=rhShowMain;
  MotorMaintenanceDisplayFm.rh_ShowMain:=rhShowMain;
  MNI2ConfigFM.rh_ShowMain:=rhshowmain;
  IQMLC2ConfigFM.rh_ShowMain:= rhshowmain;
  ComFm.rh_PortOpend := NewProjectFm.rhPortOpend;
  ComFm.rh_PortClosed := NewProjectFm.rhPortClosed;
  ComFm.rh_PortOpendDisc := DiscoverFm.rhPortOpend;
  ComFm.rh_PortClosedDisc := DiscoverFm.rhPortClosed;
  ComFm.rh_PortOpendSetup := ScreenSetupFm.rhPortOpend;
  ComFm.rh_PortClosedSetup := ScreenSetupFm.rhPortClosed;
  ComFm.rh_PortOpendConfig := ConfigScreenFm.rhPortOpend;
  ComFm.rh_PortClosedConfig := ConfigScreenFm.rhPortClosed;
  ComFm.rh_PortOpenLimitSetting := LimitSettingFm.rhPortOpenLimitSetting;
  ComFm.rh_PortClosedLimitSetting := LimitSettingFm.rhPortClosedLimitSetting;
  ComFm.rh_PortOpenLimitSetting := LimitSettingFm.rhPortOpenLimitSetting;
  ComFm.rh_PortClosedLimitSetting := LimitSettingFm.rhPortClosedLimitSetting;
  //ComFm.rh_AboutPortOpend := AboutFm.rhPortOpend;
  // ComFm.rh_AboutPortClosed := AboutFm.rhPortClosed;
  ComFm.rh_MWCPortOpend := MWCConfigfm.rhPortOpend;
  ComFm.rh_MWCPortClosed := MWCConfigfm.rhPortClosed;
  ComFm.rh_IQ3RFConfigPortOpend := IQ3Configfm.rhPortOpend;
  ComFm.rh_IQ3RFConfigPortClosed := IQ3Configfm.rhPortClosed;
  ComFm.rh_IQ3RFDisplayPortOpend := IQ3RFDisplayfm.rhPortOpend;
  ComFm.rh_IQ3RFDisplayPortClosed := IQ3RFDisplayfm.rhPortClosed;
  ComFm.rh_MWCDisplayPortOpend := MWCDisplayfm.rhPortOpend;
  ComFm.rh_MWCDisplayPortClosed := MWCDisplayfm.rhPortClosed;
  ComFm.rh_MNIConfigPortOpend := MNIConfigFm.rhPortOpend;
  ComFm.rh_MNIConfigPortClosed := MNIConfigFm.rhPortClosed;
  ComFm.rh_PowerPanelConfigPortOpend := PowerPanelConfigFm.rhPortOpend;
  ComFm.rh_PowerPanelConfigPortClosed := PowerPanelConfigFm.rhPortClosed;
  ComFm.rh_MMConfigPortOpend := MotorMaintenanceDisplayFm.rhPortOpend;
  ComFm.rh_MMConfigPortClosed := MotorMaintenanceDisplayFm.rhPortClosed;
  ComFm.rh_MNI2ConfigPortOpend := MNI2ConfigFm.rhPortOpend;
  ComFm.rh_MNI2ConfigPortClosed := MNI2ConfigFm.rhPortClosed;
  ComFm.rh_IQMLC2ConfigPortOpend := IQMLC2ConfigFm.rhPortOpend;
  ComFm.rh_IQMLC2ConfigPortClosed := IQMLC2ConfigFm.rhPortClosed;
  ComFm.SetRH_handlerforASCII(ASCIIFm.rhMessageRecieved);

  MWCDisplayFm.rh_showMNI2Config:=MNI2ConfigFm.rhShowMNI2Config;
  MWCConfigFm.rh_showMNI2Config:=MNI2ConfigFm.rhShowMNI2Config;
  SetSetpointsOff;
  imgSetpointSetOn.Enabled := False;
  imgSetpointSetOn.Hint := 'Unavailable';

  ConfigScreenFm.rh_ShowFind := DiscoverFm.rhShowFind;

  ShowProject();
  vsOldAlias:=Trim(txtAlias.Text);
  btnFind.Enabled:=True;
end;

function TIQMainFm.GetAddress(vsAddress: String): String;
var viPos: Integer;
begin
  //viPos := LastDelimiter(':', vsAddress);
  viPos := LastDelimiter(' ', vsAddress);
  if viPos > 0 then
  begin
    Result := Copy(vsAddress, viPos + 1, 50)
  end
  else
  begin
    Result := vsAddress;
  end;
end;
function TIQMainFm.GetZGNAddress(vsAddress: String): String;
var
  viPos: Integer;
  vsFullZGN:String;
begin
  viPos := LastDelimiter(':', vsAddress);
  if viPos > 0 then
  begin
    vsFullZGN :=Copy(vsAddress, viPos + 1, 50);
    if not (pos('.',vsFullZGN)>0)then
    begin
      Result:= '255.'+vsFullZGN+'.255';
    end
    else
    begin
      Result:= vsFullZGN;
    end;
  end
  else
  begin
    vsFullZGN:=vsAddress;
    if not (pos('.',vsFullZGN)>0)then
    begin
      Result:= '255.'+vsFullZGN+'.255';
    end
    else
    begin
      Result:= vsFullZGN;
    end;
  end;
end;


procedure TIQMainFm.GetPosition(vsUID: String);
var vsHi, vsLo, vsCommand: String;
begin
  try
    if (isMNI)or(IsIQMLC2) then
    begin
      if not(IsIQMLC2Old or IsIQMLC2EVB)then
      begin
//        if VariablesUn.IsNumber(GetUID(cmbMotors.Text)) then
//        begin
          vsHi := IntToStr(Hi(StrToInt(vsUID)));
          vsLo := IntToStr(Lo(StrToInt(vsUID)));
          vsCommand := '?' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_IQPos + '.'+cmbMotorSelector.Text+'.0.0';
          ComFm.SendCommand(vsCommand, rhPositionRecieved);
//        end;
      end;
    end
    else
    begin
      if VariablesUn.IsNumber(GetCurrentUID) then
      begin
        vsUID:=GetCurrentUID;
        vsCommand:= VariablesUn.UID_IQPos + '.0.0.0';
        ComFm.GetGenericStatus(StrToInt(vsUID),vsCommand,rhPositionRecieved);
      end;
    end;
  except
  on E: Exception do
  begin
    MessageDlg('TIQMainFm.imgDn_upClick: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;



procedure TIQMainFm.Image1MouseEnter(Sender: TObject);
begin
  SetSetpointsOff;
  pnlMenuBar.Visible := True;
end;

procedure TIQMainFm.Image3Click(Sender: TObject);
begin
//  pnlMenuBar.Visible := False;
//  LimitSettingFm.rh_showmain := rhShowMain;
//  IQMainFm.Hide;
//  LimitSettingFm.Show;

//  pnlMenuBar.Visible := False;
//  MotorMaintenanceDisplay.rh := rhShowMain;
//  IQMainFm.Hide;
//  LimitSettingFm.Show;
end;

procedure TIQMainFm.imgDnClick(Sender: TObject);
var vsHi, vsLo, vsCommand, vsType, vsZGN, vsG: String;
begin
  try
    SetSetpointsOff;
    moveFailFlag:=false;
    if (VariablesUn.giAdrType=0) or (VariablesUn.giAdrType=1)then
    begin
      if (cmbCmdType.ItemIndex = 0) then
        vsType := '64'
      else
        vsType := '128';

      if isMNI or isIQMLC2 then
      begin
        if(variablesUn.giAdrType=1)then
        begin
          vsZGN := GetZGNAddress(cmbZGN.Text);
        end
        else if(variablesUn.giAdrType=2)then
        begin
          vsZGN := GetZGNAddress(cmbG.Text);
        end
      end
      else
      begin
        if(variablesUn.giAdrType=1)then
        begin
          vsZGN := GetAddress(cmbZGN.Text);
        end
        else if(variablesUn.giAdrType=0)then
        begin
          vsZGN := GetAddress(cmbG.Text);
        end
      end;

      if (VariablesUn.giAdrType = 0) then
      begin

        if not (pos('.',cmbG.text)>0)then
        begin
         if(StrToInt64(vsZGN)>2147483647)then
          begin
            MessageDlg('Please enter a valid Group# (0-255) or valid ZGN Address: n.n.n (n=0-255)', mtWarning, [mbOK], 0);
            moveFailFlag:=true;
            exit;
          end
          else
          begin
            vsZGN:='255.'+vsZGN+'.255'
          end;
        end;

      end;
      if VariablesUn.IsZGN(vsZGN) then
      begin

        vsCommand := '>' + vsType + '.0.' + VariablesUn.UID_DN +'.'+vsZGN;
      end
      else
      begin
        MessageDlg('Please enter a valid ZGN Address: n.n.n (n=0-255)', mtWarning, [mbOK], 0);
        moveFailFlag:=true;
//          if cmbZGN.Items.Count > 0 then
//            cmbZGN.ItemIndex := 0;

        exit;
      end;
//      vsCommand := '>' + vsType + '.0.' + VariablesUn.UID_TOP +'.'+vsZGN;
      cmbPosition.Text := '---';

      if(IsIQMLC2EVB)then
      begin
        clearInhibit;
      end;
      ComFm.SendCommand(vsCommand, rhMoveRecieved);

      Delay(500);

      VariablesUn.giShadePosition := -1;
      giLastPos := -1;
      GetPosition(GetCurrentUID);
    end
    else if VariablesUn.IsNumber(GetCurrentUID) then
    begin
      if not (pos('.',GetCurrentUID)>0)then
      begin
        if(StrToInt64(GetCurrentUID)>2147483647)then
        begin
          MessageDlg('Please enter a valid UID (0 - 9999).', mtWarning, [mbOK], 0);
          moveFailFlag:=true;
          exit;
        end;
      end
      else
      begin
        MessageDlg('Please enter a valid UID (0 - 9999).', mtWarning, [mbOK], 0);
        moveFailFlag:=true;
        exit;
      end;
      vsHi := IntToStr(Hi(StrToInt(GetCurrentUID)));
      vsLo := IntToStr(Lo(StrToInt(GetCurrentUID)));
      if not cmbMotorSelector.Visible then     //means its an MNI or IQMLC2
      begin
        vsCommand := '>' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_DN+ '.0.0.0';
      end
      else
      begin
        vsCommand := '>' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_DN + '.0.0.'+MotorNumberSetBits(cmbMotorSelector.text);
      end;
    end;

    cmbPosition.Text := '---';
    ComFm.SendCommand(vsCommand, rhMoveRecieved);

    Delay(500);
    if isMNI then
    begin
      img0.Visible := True;
      img10.Visible := False;
      img20.Visible := False;
      img30.Visible := False;
      img40.Visible := False;
      img50.Visible := False;
      img60.Visible := False;
      img70.Visible := False;
      img80.Visible := False;
      img90.Visible := False;
      img100.Visible := False;
      cmbPosition.itemindex:=0;
    end
    else
    begin
      VariablesUn.giShadePosition := -1;
      giLastPos := -1;
      GetPosition(GetCurrentUID);
    end;
  except
  on E: Exception do
  begin
    MessageDlg('TIQMainFm.imgDn_upClick: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;

procedure TIQMainFm.imgIS1Click(Sender: TObject);
var vsHi, vsLo, vsCommand, vsType, vsZGN, vsG: String;
begin
  try
    if imgSetpointSetOn.Visible = False then
    begin
      vsHi := IntToStr(Hi(StrToInt(GetCurrentUID)));
      vsLo := IntToStr(Lo(StrToInt(GetCurrentUID)));
      gsSetPoint := '1';
      vsCommand := '>' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_Set1 + '.0.0.0';
      ComFm.SendCommand(vsCommand, rhSetpointRecieved);
      exit;
    end;
    if (VariablesUn.giAdrType=0) or (VariablesUn.giAdrType=1)then
    begin
      if (cmbCmdType.ItemIndex = 0) then
        vsType := '64'
      else
        vsType := '128';

      if isMNI or isIQMLC2 then
      begin
        if(variablesUn.giAdrType=1)then
        begin
          vsZGN := GetZGNAddress(cmbZGN.Text);
        end
        else if(variablesUn.giAdrType=2)then
        begin
          vsZGN := GetZGNAddress(cmbG.Text);
        end
      end
      else
      begin
        if(variablesUn.giAdrType=1)then
        begin
          vsZGN := GetAddress(cmbZGN.Text);
        end
        else if(variablesUn.giAdrType=0)then
        begin
          vsZGN := GetAddress(cmbG.Text);
        end
      end;

      if (VariablesUn.giAdrType = 0) then
      begin

        if not (pos('.',cmbG.text)>0)then
        begin
         if(StrToInt64(vsZGN)>2147483647)then
          begin
            MessageDlg('Please enter a valid Group# (0-255) or valid ZGN Address: n.n.n (n=0-255)', mtWarning, [mbOK], 0);
            moveFailFlag:=true;
            exit;
          end
          else
          begin
            vsZGN:='255.'+vsZGN+'.255'
          end;
        end;

      end;
      if VariablesUn.IsZGN(vsZGN) then
      begin

        vsCommand := '>' + vsType + '.0.' + VariablesUn.UID_G1 +'.'+vsZGN;
      end
      else
      begin
        MessageDlg('Please enter a valid ZGN Address: n.n.n (n=0-255)', mtWarning, [mbOK], 0);
        moveFailFlag:=true;
//          if cmbZGN.Items.Count > 0 then
//            cmbZGN.ItemIndex := 0;

        exit;
      end;
//      vsCommand := '>' + vsType + '.0.' + VariablesUn.UID_TOP +'.'+vsZGN;
      cmbPosition.Text := '---';

      if(IsIQMLC2EVB)then
      begin
        clearInhibit;
      end;
      ComFm.SendCommand(vsCommand, rhMoveRecieved);

      Delay(500);

      VariablesUn.giShadePosition := -1;
      giLastPos := -1;
      GetPosition(GetCurrentUID);
    end
    else if VariablesUn.IsNumber(GetCurrentUID) then
    begin
      if not (pos('.',GetCurrentUID)>0)then
      begin
        if(StrToInt64(GetCurrentUID)>2147483647)then
        begin
          MessageDlg('Please enter a valid UID (0 - 9999).', mtWarning, [mbOK], 0);
          moveFailFlag:=true;
          exit;
        end;
      end
      else
      begin
        MessageDlg('Please enter a valid UID (0 - 9999).', mtWarning, [mbOK], 0);
        moveFailFlag:=true;
        exit;
      end;
      vsHi := IntToStr(Hi(StrToInt(GetCurrentUID)));
      vsLo := IntToStr(Lo(StrToInt(GetCurrentUID)));
      if not cmbMotorSelector.Visible then     //means its an MNI or IQMLC2
      begin
        vsCommand := '>' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_G1 + '.0.0.0';
      end
      else
      begin
        if isIQMLC2EVB then
        begin
          vsCommand := '>' + vsHi + '.' + vsLo + '.' + VariablesUn.EVBAnglePoint + '.0.101.'+MotorNumberSetBits(cmbMotorSelector.text);
        end
        else
        begin
          vsCommand := '>' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_G1 + '.0.0.'+MotorNumberSetBits(cmbMotorSelector.text);
        end;
      end;
      cmbPosition.Text := '---';
      ComFm.SendCommand(vsCommand, rhMoveRecieved);

    end
    else
    begin
      MessageDlg('Please enter a valid UID (0 - 9999).', mtWarning, [mbOK], 0);
    end;



    Delay(500);
    if isMNI then
    begin
      img0.Visible := True;
      img10.Visible := False;
      img20.Visible := False;
      img30.Visible := False;
      img40.Visible := False;
      img50.Visible := False;
      img60.Visible := False;
      img70.Visible := False;
      img80.Visible := False;
      img90.Visible := False;
      img100.Visible := False;
      cmbPosition.itemindex:=0;
    end
    else
    begin
      VariablesUn.giShadePosition := -1;
      giLastPos := -1;
      GetPosition(GetCurrentUID);
    end;
    //
  except
  on E: Exception do
  begin
    MessageDlg('TIQMainFm.imgIS1Click: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;

procedure TIQMainFm.imgIS1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
 if TImage(Sender).Enabled then
    TImage(Sender).Visible := False;
end;

procedure TIQMainFm.imgIS1MouseLeave(Sender: TObject);
begin
    TImage(Sender).Visible := True;
end;

procedure TIQMainFm.imgIS1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
    TImage(Sender).Visible := True;
end;

procedure TIQMainFm.imgIS2Click(Sender: TObject);
var vsHi, vsLo, vsCommand, vsType, vsZGN, vsG: String;
begin
  try
    if imgSetpointSetOn.Visible = False then
    begin
      vsHi := IntToStr(Hi(StrToInt(GetCurrentUID)));
      vsLo := IntToStr(Lo(StrToInt(GetCurrentUID)));
      gsSetPoint := '1';
      vsCommand := '>' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_Set2 + '.0.0.0';
      ComFm.SendCommand(vsCommand, rhSetpointRecieved);
      exit;
    end;

   if (VariablesUn.giAdrType=0) or (VariablesUn.giAdrType=1)then
    begin
      if (cmbCmdType.ItemIndex = 0) then
        vsType := '64'
      else
        vsType := '128';

      if isMNI or isIQMLC2 then
      begin
        if(variablesUn.giAdrType=1)then
        begin
          vsZGN := GetZGNAddress(cmbZGN.Text);
        end
        else if(variablesUn.giAdrType=2)then
        begin
          vsZGN := GetZGNAddress(cmbG.Text);
        end
      end
      else
      begin
        if(variablesUn.giAdrType=1)then
        begin
          vsZGN := GetAddress(cmbZGN.Text);
        end
        else if(variablesUn.giAdrType=0)then
        begin
          vsZGN := GetAddress(cmbG.Text);
        end
      end;

      if (VariablesUn.giAdrType = 0) then
      begin

        if not (pos('.',cmbG.text)>0)then
        begin
         if(StrToInt64(vsZGN)>2147483647)then
          begin
            MessageDlg('Please enter a valid Group# (0-255) or valid ZGN Address: n.n.n (n=0-255)', mtWarning, [mbOK], 0);
            moveFailFlag:=true;
            exit;
          end
          else
          begin
            vsZGN:='255.'+vsZGN+'.255'
          end;
        end;

      end;
      if VariablesUn.IsZGN(vsZGN) then
      begin

        vsCommand := '>' + vsType + '.0.' + VariablesUn.UID_G2 +'.'+vsZGN;
      end
      else
      begin
        MessageDlg('Please enter a valid ZGN Address: n.n.n (n=0-255)', mtWarning, [mbOK], 0);
        moveFailFlag:=true;
//          if cmbZGN.Items.Count > 0 then
//            cmbZGN.ItemIndex := 0;

        exit;
      end;
//      vsCommand := '>' + vsType + '.0.' + VariablesUn.UID_TOP +'.'+vsZGN;
      cmbPosition.Text := '---';

      if(IsIQMLC2EVB)then
      begin
        clearInhibit;
      end;
      ComFm.SendCommand(vsCommand, rhMoveRecieved);

      Delay(500);

      VariablesUn.giShadePosition := -1;
      giLastPos := -1;
      GetPosition(GetCurrentUID);
    end
    else if VariablesUn.IsNumber(GetCurrentUID) then
    begin
      if not (pos('.',GetCurrentUID)>0)then
      begin
        if(StrToInt64(GetCurrentUID)>2147483647)then
        begin
          MessageDlg('Please enter a valid UID (0 - 9999).', mtWarning, [mbOK], 0);
          moveFailFlag:=true;
          exit;
        end;
      end
      else
      begin
        MessageDlg('Please enter a valid UID (0 - 9999).', mtWarning, [mbOK], 0);
        moveFailFlag:=true;
        exit;
      end;
      vsHi := IntToStr(Hi(StrToInt(GetCurrentUID)));
      vsLo := IntToStr(Lo(StrToInt(GetCurrentUID)));
      if not cmbMotorSelector.Visible then     //means its an MNI or IQMLC2
      begin
        vsCommand := '>' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_G2 + '.0.0.0';
      end
      else
      begin
        vsCommand := '>' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_G2 + '.0.0.'+MotorNumberSetBits(cmbMotorSelector.text);
      end;
      cmbPosition.Text := '---';
      ComFm.SendCommand(vsCommand, rhMoveRecieved);

    end
    else
    begin
      MessageDlg('Please enter a valid UID (0 - 9999).', mtWarning, [mbOK], 0);
    end;



    Delay(500);
    if isMNI then
    begin
      img0.Visible := True;
      img10.Visible := False;
      img20.Visible := False;
      img30.Visible := False;
      img40.Visible := False;
      img50.Visible := False;
      img60.Visible := False;
      img70.Visible := False;
      img80.Visible := False;
      img90.Visible := False;
      img100.Visible := False;
      cmbPosition.itemindex:=0;
    end
    else
    begin
      VariablesUn.giShadePosition := -1;
      giLastPos := -1;
      GetPosition(GetCurrentUID);
    end;
  except
  on E: Exception do
  begin
    MessageDlg('TIQMainFm.imgIS2Click: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;

procedure TIQMainFm.imgIS2MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
 if TImage(Sender).Enabled then
    TImage(Sender).Visible := False;
end;

procedure TIQMainFm.imgIS2MouseLeave(Sender: TObject);
begin
    TImage(Sender).Visible := True;
end;

procedure TIQMainFm.imgIS2MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
    TImage(Sender).Visible := True;
end;

procedure TIQMainFm.imgIS3Click(Sender: TObject);
var vsHi, vsLo, vsCommand, vsType, vsZGN, vsG: String;
begin
  try
    if imgSetpointSetOn.Visible = False then
    begin
      vsHi := IntToStr(Hi(StrToInt(GetCurrentUID)));
      vsLo := IntToStr(Lo(StrToInt(GetCurrentUID)));
      gsSetPoint := '1';
      vsCommand := '>' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_Set3 + '.0.0.0';
      ComFm.SendCommand(vsCommand, rhSetpointRecieved);
      exit;
    end;
    if (VariablesUn.giAdrType=0) or (VariablesUn.giAdrType=1)then
    begin
      if (cmbCmdType.ItemIndex = 0) then
        vsType := '64'
      else
        vsType := '128';

      if isMNI or isIQMLC2 then
      begin
        if(variablesUn.giAdrType=1)then
        begin
          vsZGN := GetZGNAddress(cmbZGN.Text);
        end
        else if(variablesUn.giAdrType=2)then
        begin
          vsZGN := GetZGNAddress(cmbG.Text);
        end
      end
      else
      begin
        if(variablesUn.giAdrType=1)then
        begin
          vsZGN := GetAddress(cmbZGN.Text);
        end
        else if(variablesUn.giAdrType=0)then
        begin
          vsZGN := GetAddress(cmbG.Text);
        end
      end;

      if (VariablesUn.giAdrType = 0) then
      begin

        if not (pos('.',cmbG.text)>0)then
        begin
         if(StrToInt64(vsZGN)>2147483647)then
          begin
            MessageDlg('Please enter a valid Group# (0-255) or valid ZGN Address: n.n.n (n=0-255)', mtWarning, [mbOK], 0);
            moveFailFlag:=true;
            exit;
          end
          else
          begin
            vsZGN:='255.'+vsZGN+'.255'
          end;
        end;

      end;
      if VariablesUn.IsZGN(vsZGN) then
      begin

        vsCommand := '>' + vsType + '.0.' + VariablesUn.UID_G3 +'.'+vsZGN;
      end
      else
      begin
        MessageDlg('Please enter a valid ZGN Address: n.n.n (n=0-255)', mtWarning, [mbOK], 0);
        moveFailFlag:=true;
//          if cmbZGN.Items.Count > 0 then
//            cmbZGN.ItemIndex := 0;

        exit;
      end;
//      vsCommand := '>' + vsType + '.0.' + VariablesUn.UID_TOP +'.'+vsZGN;
      cmbPosition.Text := '---';

      if(IsIQMLC2EVB)then
      begin
        clearInhibit;
      end;
      ComFm.SendCommand(vsCommand, rhMoveRecieved);

      Delay(500);

      VariablesUn.giShadePosition := -1;
      giLastPos := -1;
      GetPosition(GetCurrentUID);
    end
    else if VariablesUn.IsNumber(GetCurrentUID) then
    begin
      if not (pos('.',GetCurrentUID)>0)then
      begin
        if(StrToInt64(GetCurrentUID)>2147483647)then
        begin
          MessageDlg('Please enter a valid UID (0 - 9999).', mtWarning, [mbOK], 0);
          moveFailFlag:=true;
          exit;
        end;
      end
      else
      begin
        MessageDlg('Please enter a valid UID (0 - 9999).', mtWarning, [mbOK], 0);
        moveFailFlag:=true;
        exit;
      end;
      vsHi := IntToStr(Hi(StrToInt(GetCurrentUID)));
      vsLo := IntToStr(Lo(StrToInt(GetCurrentUID)));
      if not cmbMotorSelector.Visible then     //means its an MNI or IQMLC2
      begin
        vsCommand := '>' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_G3 + '.0.0.0';
      end
      else
      begin
        vsCommand := '>' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_G3 + '.0.0.'+MotorNumberSetBits(cmbMotorSelector.text);
      end;
    end;

    cmbPosition.Text := '---';
    ComFm.SendCommand(vsCommand, rhMoveRecieved);

    Delay(500);
    if isMNI then
    begin
      img0.Visible := True;
      img10.Visible := False;
      img20.Visible := False;
      img30.Visible := False;
      img40.Visible := False;
      img50.Visible := False;
      img60.Visible := False;
      img70.Visible := False;
      img80.Visible := False;
      img90.Visible := False;
      img100.Visible := False;
      cmbPosition.itemindex:=0;
    end
    else
    begin
      VariablesUn.giShadePosition := -1;
      giLastPos := -1;
      GetPosition(GetCurrentUID);
    end;
  except
  on E: Exception do
  begin
    MessageDlg('TIQMainFm.imgIS3Click: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;

procedure TIQMainFm.imgIS3MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
 if TImage(Sender).Enabled then
    TImage(Sender).Visible := False;
end;

procedure TIQMainFm.imgIS3MouseLeave(Sender: TObject);
begin
    TImage(Sender).Visible := True;
end;

procedure TIQMainFm.imgIS3MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
    TImage(Sender).Visible := True;
end;

procedure TIQMainFm.imgSetpointSetOffClick(Sender: TObject);
begin
  SetSetpointsOff;
end;

procedure TIQMainFm.imgSetpointSetOnClick(Sender: TObject);
begin
  if VariablesUn.IsNumber(GetCurrentUID) then
    SetSetpointsOn;
end;

procedure TIQMainFm.imgSetupClick(Sender: TObject);
begin
  ScreenSetupFm.rh_ShowMain := rhShowMain;
  SetSetpointsOff;
  pnlMenuBar.Visible := False;
  IQMainFm.Hide;
  ScreenSetupFm.Show;
end;

procedure TIQMainFm.imgUpClick(Sender: TObject);
var vsHi, vsLo, vsCommand, vsType, vsZGN: String;
begin
  try
    SetSetpointsOff;
    moveFailFlag:=false;
    if (VariablesUn.giAdrType=0) or (VariablesUn.giAdrType=1)then
    begin
      if (cmbCmdType.ItemIndex = 0) then
        vsType := '64'
      else
        vsType := '128';

      if isMNI or isIQMLC2 then
      begin
        if(variablesUn.giAdrType=1)then
        begin
          vsZGN := GetZGNAddress(cmbZGN.Text);
        end
        else if(variablesUn.giAdrType=2)then
        begin
          vsZGN := GetZGNAddress(cmbG.Text);
        end
      end
      else
      begin
        if(variablesUn.giAdrType=1)then
        begin
          vsZGN := GetAddress(cmbZGN.Text);
        end
        else if(variablesUn.giAdrType=0)then
        begin
          vsZGN := GetAddress(cmbG.Text);
        end
      end;

      if (VariablesUn.giAdrType = 0) then
      begin

        if not (pos('.',cmbG.text)>0)then
        begin
         if(StrToInt64(vsZGN)>2147483647)then
          begin
            MessageDlg('Please enter a valid Group# (0-255) or valid ZGN Address: n.n.n (n=0-255)', mtWarning, [mbOK], 0);
            moveFailFlag:=true;
            exit;
          end
          else
          begin
            vsZGN:='255.'+vsZGN+'.255'
          end;
        end;

      end;
      if VariablesUn.IsZGN(vsZGN) then
      begin

        vsCommand := '>' + vsType + '.0.' + VariablesUn.UID_TOP +'.'+vsZGN;
      end
      else
      begin
        MessageDlg('Please enter a valid ZGN Address: n.n.n (n=0-255)', mtWarning, [mbOK], 0);
        moveFailFlag:=true;
//          if cmbZGN.Items.Count > 0 then
//            cmbZGN.ItemIndex := 0;

        exit;
      end;
//      vsCommand := '>' + vsType + '.0.' + VariablesUn.UID_TOP +'.'+vsZGN;
      cmbPosition.Text := '---';

      if(IsIQMLC2EVB)then
      begin
        clearInhibit;
      end;
      ComFm.SendCommand(vsCommand, rhMoveRecieved);

      Delay(500);

      VariablesUn.giShadePosition := -1;
      giLastPos := -1;
      GetPosition(GetCurrentUID);
    end
    else if VariablesUn.IsNumber(GetCurrentUID) then
    begin
      if not (pos('.',GetCurrentUID)>0)then
      begin
        if(StrToInt64(GetCurrentUID)>2147483647)then
        begin
          MessageDlg('Please enter a valid UID (0 - 9999).', mtWarning, [mbOK], 0);
          moveFailFlag:=true;
          exit;
        end;
      end
      else
      begin
        MessageDlg('Please enter a valid UID (0 - 9999).', mtWarning, [mbOK], 0);
        moveFailFlag:=true;
        exit;
      end;
      vsHi := IntToStr(Hi(StrToInt(GetCurrentUID)));
      vsLo := IntToStr(Lo(StrToInt(GetCurrentUID)));
      if not cmbMotorSelector.Visible then     //means its an MNI or IQMLC2
      begin
        vsCommand := '>' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_TOP + '.0.0.0';
      end
      else
      begin
        vsCommand := '>' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_TOP + '.0.0.'+MotorNumberSetBits(cmbMotorSelector.text);
      end;
      if(IsIQMLC2EVB)then
      begin
        clearInhibit;
      end;
      cmbPosition.Text := '---';
      ComFm.SendCommand(vsCommand, rhMoveRecieved);
    end
    else
    begin
      MessageDlg('Please enter a valid UID (0 - 9999).', mtWarning, [mbOK], 0);
      moveFailFlag:=true;
    end;



    Delay(500);
    if isMNI then
    begin
      img0.Visible := True;
      img10.Visible := False;
      img20.Visible := False;
      img30.Visible := False;
      img40.Visible := False;
      img50.Visible := False;
      img60.Visible := False;
      img70.Visible := False;
      img80.Visible := False;
      img90.Visible := False;
      img100.Visible := False;
      cmbPosition.itemindex:=0;
    end
    else
    begin
      VariablesUn.giShadePosition := -1;
      giLastPos := -1;
      GetPosition(GetCurrentUID);
    end;
  except
  on E: Exception do
  begin
    MessageDlg('TIQMainFm.imgUpClick: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;


function TIQMainFm.IsMNI:Boolean;
var
  I,DelimiterCount:Integer;
begin
  if (VariablesUn.giAdrType = 2) then
  begin
    if pos('MNI',cmbMotors.text)=0 then
    begin
      if cmbMotorSelector.Visible=false then
      begin
        IsMNI:=false;
      end
      else
      begin
        IsMNI:=True;
      end;

    end
    else
    begin
      IsMNI:=True;
    end;
  end
  else if (VariablesUn.giAdrType = 1) then
  begin
    DelimiterCount:=0;
//    DataUn.dmDataModule.dsMotors.Filtered := False;
//    DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(cbProject.Text) +  ' and UID = ' + VariablesUn.gsUIDCrnt + ' and MotorNumber = 4';
//    DataUn.dmDataModule.dsMotors.Filtered := True;
//    if not(DataUn.dmDataModule.dsMotors.Eof)then
//    begin
//      if(DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsInteger=65)then
//      begin
//        IsMNI:=true;
//        exit;
//      end
//      else
//      begin
//        IsMNI:=False;
//        exit;
//      end;
//    end;
    for I := 1 to Length(cmbZGN.text) do
    begin
      if cmbZGN.text[I] = ':' then
      begin
        inc(DelimiterCount);
      end;
    end;
    if DelimiterCount>1 then
    begin
      IsMNI:=true;
    end
    else
    begin
      IsMNI:=False;
    end;
  end
  else if (VariablesUn.giAdrType = 0) then
  begin
    DelimiterCount:=0;
//    DataUn.dmDataModule.dsMotors.Filtered := False;
//    DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(cbProject.Text) +  ' and UID = ' + VariablesUn.gsUIDCrnt + ' and MotorNumber = 4';
//    DataUn.dmDataModule.dsMotors.Filtered := True;
//    if not(DataUn.dmDataModule.dsMotors.Eof)then
//    begin
//      if(DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsInteger=65)then
//      begin
//        IsMNI:=true;
//        exit;
//      end
//      else
//      begin
//        IsMNI:=False;
//        exit;
//      end;
//    end;
    for I := 1 to Length(cmbG.text) do
    begin
      if cmbG.text[I] = ':' then
      begin
        inc(DelimiterCount);
      end;
    end;
    if DelimiterCount>1 then
    begin
      IsMNI:=true;
    end
    else
    begin
      IsMNI:=False;
    end;
  end;

end;

function TIQMainFm.IsIQMLC2Old:Boolean;
begin
  DataUn.dmDataModule.dsIQMLC2.Filtered := False;
  DataUn.dmDataModule.dsIQMLC2.Filter := 'ProjectName = ' + QuotedStr(cbProject.text) +  ' and UID = ' + GetCurrentUID;
  DataUn.dmDataModule.dsIQMLC2.Filtered := True;
  if(DataUn.dmDataModule.dsIQMLC2.FieldByName('FirmwareRev').AsString = '8.8')then
  begin
    result:=True;
  end
  else
  begin
    if(DataUn.dmDataModule.dsIQMLC2.FieldByName('FirmwareRev').AsString = '0.3')then
    begin
      result:=True;
    end
    else
    begin
      result:=False;
    end;
  end
end;
function TIQMainFm.IsIQMLC2EVB:Boolean;
begin
  DataUn.dmDataModule.dsIQMLC2.Filtered := False;
  DataUn.dmDataModule.dsIQMLC2.Filter := 'ProjectName = ' + QuotedStr(cbProject.text) +  ' and UID = ' + GetCurrentUID;
  DataUn.dmDataModule.dsIQMLC2.Filtered := True;
  if(LeftStr(DataUn.dmDataModule.dsIQMLC2.FieldByName('FirmwareRev').AsString,1) = 'A') then
  begin
    result:=True;
  end
  else
  begin
    result:=False;
  end
end;

function TIQMainFm.IsIQMLC2:Boolean;
var
  I,DelimiterCount:Integer;
begin
  if (VariablesUn.giAdrType = 2) then
  begin
    if pos('IQMLC2',cmbMotors.text)=0 then
    begin
      if cmbMotorSelector.Visible=false then
      begin
        IsIQMLC2:=false;
      end
      else
      begin
        IsIQMLC2:=True;
      end;

    end
    else
    begin
      IsIQMLC2:=True;
    end;
  end
  else if (VariablesUn.giAdrType = 1) then
  begin
    DelimiterCount:=0;
//    DataUn.dmDataModule.dsMotors.Filtered := False;
//    DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(cbProject.Text) +  ' and UID = ' + VariablesUn.gsUIDCrnt + ' and MotorNumber = 4';
//    DataUn.dmDataModule.dsMotors.Filtered := True;
//    if not(DataUn.dmDataModule.dsMotors.Eof)then
//    begin
//      if(DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsInteger=65)then
//      begin
//        IsIQMLC2:=true;
//        exit;
//      end
//      else
//      begin
//        IsIQMLC2:=False;
//        exit;
//      end;
//    end;
    for I := 1 to Length(cmbZGN.text) do
    begin
      if cmbZGN.text[I] = ':' then
      begin
        inc(DelimiterCount);
      end;
    end;
    if DelimiterCount>1 then
    begin
      IsIQMLC2:=true;
    end
    else
    begin
      IsIQMLC2:=False;
    end;
  end
  else if (VariablesUn.giAdrType = 0) then
  begin
    DelimiterCount:=0;
//    DataUn.dmDataModule.dsMotors.Filtered := False;
//    DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(cbProject.Text) +  ' and UID = ' + VariablesUn.gsUIDCrnt + ' and MotorNumber = 4';
//    DataUn.dmDataModule.dsMotors.Filtered := True;
//    if not(DataUn.dmDataModule.dsMotors.Eof)then
//    begin
//      if(DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsInteger=65)then
//      begin
//        IsIQMLC2:=true;
//        exit;
//      end
//      else
//      begin
//        IsIQMLC2:=False;
//        exit;
//      end;
//    end;
    for I := 1 to Length(cmbG.text) do
    begin
      if cmbG.text[I] = ':' then
      begin
        inc(DelimiterCount);
      end;
    end;
    if DelimiterCount>1 then
    begin
      IsIQMLC2:=true;
    end
    else
    begin
      IsIQMLC2:=False;
    end;
  end;

end;

procedure TIQMainFm.JvCaptionButton1Click(Sender: TObject);
begin

  Application.HelpContext(4);
end;

function TIQMainFm.IsDC:Integer;
var
  UID:String;
begin
  if (VariablesUn.giAdrType = 2) then
  begin
    if (pos('DC50',cmbMotors.text)<>0) or (pos('ST30',cmbMotors.text)<>0)then
    begin
      IsDC:=1;
      exit;
    end
    else if (pos('AC',cmbMotors.text)<>0) then
    begin
      IsDC:=2;
      exit;
    end;
    IsDC:=0;
  end
  else if (VariablesUn.giAdrType = 1) then
  begin
    UID:=Copy(cmbzgn.text,0,ansiPos(' ',cmbzgn.text));
    DataUn.dmDataModule.dsMotors.Filtered := False;
    DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + UID;
    DataUn.dmDataModule.dsMotors.Filtered := True;

    if (dmDataModule.dsMotors.FieldByName('MotorType').asInteger = 77) or (dmDataModule.dsMotors.FieldByName('MotorType').asInteger = 205) then
    begin
      IsDC:=1;
      exit;
    end
    else if (dmDataModule.dsMotors.FieldByName('MotorType').asInteger = 78) or (dmDataModule.dsMotors.FieldByName('MotorType').asInteger = 206) then
    begin
      IsDC:=2;
      exit;
    end
    else if (dmDataModule.dsMotors.FieldByName('MotorType').asInteger = 79) or (dmDataModule.dsMotors.FieldByName('MotorType').asInteger = 207) then
    begin
      IsDC:=1;
      exit;
    end;
    IsDC:=0;
  end
  else if (VariablesUn.giAdrType = 0) then
  begin
    UID:=Copy(cmbG.text,0,ansiPos(' ',cmbG.text));
    DataUn.dmDataModule.dsMotors.Filtered := False;
    DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + UID;
    DataUn.dmDataModule.dsMotors.Filtered := True;

    if (dmDataModule.dsMotors.FieldByName('MotorType').asInteger = 77) or (dmDataModule.dsMotors.FieldByName('MotorType').asInteger = 205) then
    begin
      IsDC:=1;
      exit;
    end
    else if (dmDataModule.dsMotors.FieldByName('MotorType').asInteger = 78) or (dmDataModule.dsMotors.FieldByName('MotorType').asInteger = 206) then
    begin
      IsDC:=2;
      exit;
    end
    else if (dmDataModule.dsMotors.FieldByName('MotorType').asInteger = 79) or (dmDataModule.dsMotors.FieldByName('MotorType').asInteger = 207) then
    begin
      IsDC:=1;
      exit;
    end;
    IsDC:=0;
  end;

end;

procedure TIQMainFm.imgDnMouseLeave(Sender: TObject);
begin
    //TImage(Sender).Visible := True;
end;

procedure TIQMainFm.imgDnMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  imgDN.OnClick:=ImgDnClick;
  outputdebugstring(pchar('***MouseUp***'));
  if HoldTimer<>nil then
  begin
    HoldTimer.Enabled:=False;
  end;
  if MoveTimer<>nil then
  begin
    MoveTimer.Enabled:=False;
  end;
  if (VariablesUn.giAdrType = 2) then
  begin
    GetPosition(GetCurrentUID);
  end;
end;

procedure TIQMainFm.imgUpMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  imgUp.OnClick:=ImgUpClick;
  if HoldTimer<>nil then
  begin
    HoldTimer.Enabled:=False;
  end;
  if MoveTimer<>nil then
  begin
    MoveTimer.Enabled:=False;
  end;
  if (VariablesUn.giAdrType = 2) then
  begin
    GetPosition(GetCurrentUID);
  end;
end;

procedure TIQMainFm.imgDnMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if (not isMNI) and (VariablesUn.giAdrType = 2) or (VariablesUn.giAdrType = 0) or (VariablesUn.giAdrType = 1)then
  begin
    outputdebugstring(pchar('***MouseDown***'));
    Holdtimer.Interval:=4000;
    Holdtimer.OnTimer:=LongHoldEventDN;
    HoldTimer.Enabled:=True;
  end;
end;

procedure TIQMainFm.imgUpMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if (not isMNI) and (VariablesUn.giAdrType = 2) or (VariablesUn.giAdrType = 0) or (VariablesUn.giAdrType = 1)then
  begin

    Holdtimer.Interval:=4000;
    Holdtimer.OnTimer:=LongHoldEventUp;
    HoldTimer.Enabled:=True;
  end;
end;
procedure TIQMainFm.LongHoldEventUp(Sender: TObject);
begin
  imgUp.OnClick:=nil;
  HoldTimer.Enabled:=False;
  //MoveTimer:=TTimer.Create(self);
  MoveTimer.Interval:=1000;
  MoveTimer.OnTimer:=CreepUp;
  MoveTimer.Enabled:=true;
end;

procedure TIQMainFm.LongHoldEventDN(Sender: TObject);
begin
  outputdebugstring(pchar('***LongHoldActivated***'));
  imgDN.OnClick:=nil;
  HoldTimer.Enabled:=False;
  //MoveTimer:=TTimer.Create(self);
  MoveTimer.Interval:=1000;
  MoveTimer.OnTimer:=CreepDN;
  MoveTimer.Enabled:=true;
end;

procedure TIQMainFm.CreepUp(Sender: TObject);
var
  vscommand,vsType,vsZGN,vsG:String;
begin
  if (cmbCmdType.ItemIndex = 0) then
  begin
    vsType := '64'
  end
  else
  begin
    vsType := '128';
  end;

  if isMNI then
  begin
    vsZGN := GetZGNAddress(cmbZGN.Text);
    vsG := GetZGNAddress(cmbG.Text);
  end
  else
  begin
    vsZGN :=GetAddress(cmbZGN.Text);
    vsG := GetAddress(cmbG.Text);
  end;

  if (VariablesUn.giAdrType = 2) then
  begin
    if isdc = 1 then  //dc50  //st30
    begin
      vscommand:=VariablesUn.UID_CreepUp+ '.0.0.10';
    end
    else if isdc = 2 then //AC50
    begin
      vscommand:=VariablesUn.UID_CreepUp+ '.0.0.55';
    end
    else
    begin
      vscommand:=VariablesUn.UID_CreepUp+ '.0.0.0';
    end;
    ComFm.SetGenericParam(StrToInt(GetCurrentUID),vsCommand, nil);
  end
  else if VariablesUn.giAdrType = 1 then
  begin
    if VariablesUn.IsZGN(vsZGN) then
    begin
      if isdc = 1 then  //dc50  //st30
      begin
        vsCommand := '>' + vsType + '.10.' + VariablesUn.UID_CreepUp + '.' + IntToStr(VariablesUn.giZ) + '.' + IntToStr(VariablesUn.giG) + '.' + IntToStr(VariablesUn.giN);
      end
      else if isdc = 2 then //AC50
      if isdc = 1 then  //dc50  //st30
      begin
        vsCommand := '>' + vsType + '.55.' + VariablesUn.UID_CreepUp + '.' + IntToStr(VariablesUn.giZ) + '.' + IntToStr(VariablesUn.giG) + '.' + IntToStr(VariablesUn.giN);
      end
      else
      begin
        vsCommand := '>' + vsType + '.0.' + VariablesUn.UID_CreepUp + '.' + IntToStr(VariablesUn.giZ) + '.' + IntToStr(VariablesUn.giG) + '.' + IntToStr(VariablesUn.giN);
      end;
      ComFm.SendCommand(vsCommand, nil);
    end
    else
    begin
      MessageDlg('Please enter a valid ZGN Address: n.n.n (n=0-255)', mtWarning, [mbOK], 0);
      if cmbZGN.Items.Count > 0 then
        cmbZGN.ItemIndex := 0;

      exit;
    end;
  end
  else if VariablesUn.giAdrType = 0 then
  begin
    if VariablesUn.IsZGN(vsZGN) and VariablesUn.IsNumber(vsG) and (StrToInt(vsG) >= 0) and (StrToInt(vsG) < 256) then
    begin
      if isdc = 1 then  //dc50  //st30
      begin
        vsCommand := '>' + vsType + '.10.' + VariablesUn.UID_CreepUp + '.' + IntToStr(VariablesUn.giZ) + '.' + vsG + '.' + IntToStr(VariablesUn.giN);
      end
      else if isdc = 2 then //AC50
      begin
        vsCommand := '>' + vsType + '.55.' + VariablesUn.UID_CreepUp + '.' + IntToStr(VariablesUn.giZ) + '.' + vsG + '.' + IntToStr(VariablesUn.giN);
      end
      else  //IQ
      begin
        vsCommand := '>' + vsType + '.0.' + VariablesUn.UID_CreepUp + '.' + IntToStr(VariablesUn.giZ) + '.' + vsG + '.' + IntToStr(VariablesUn.giN);
      end;
      ComFm.SendCommand(vsCommand, nil);
    end
    else
    begin
      MessageDlg('Please enter a valid Group# (0-255)', mtWarning, [mbOK], 0);
      if cmbG.Items.Count > 0 then
        cmbG.ItemIndex := 0;

      exit;
    end;
  end;

end;

procedure TIQMainFm.CreepDN(Sender: TObject);
var
  vscommand,vsType,vsZGN,vsG:String;
begin
  outputdebugstring(pchar('***Creep***'));
  if (cmbCmdType.ItemIndex = 0) then
  begin
    vsType := '64';
  end
  else
  begin
    vsType := '128';
  end;

  if isMNI then
  begin
    vsZGN := GetZGNAddress(cmbZGN.Text);
    vsG := GetZGNAddress(cmbG.Text);
  end
  else
  begin
    vsZGN :=GetAddress(cmbZGN.Text);
    vsG := GetAddress(cmbG.Text);
  end;

  if (VariablesUn.giAdrType = 2) then
  begin
    if isdc = 1 then  //dc50  //st30
    begin
      vscommand:=VariablesUn.UID_CreepDn+ '.0.0.10';
    end
    else if isdc = 2 then //AC50
    begin
      vscommand:=VariablesUn.UID_CreepDn+ '.0.0.50';
    end
    else
    begin
      vscommand:=VariablesUn.UID_CreepDn+ '.0.0.0';
    end;
    ComFm.SetGenericParam(StrToInt(GetCurrentUID),vsCommand, nil);
  end
  else if VariablesUn.giAdrType = 1 then
  begin
    if VariablesUn.IsZGN(vsZGN) then
    begin
      if isdc = 1 then  //dc50  //st30
      begin
        vsCommand := '>' + vsType + '.10.' + VariablesUn.UID_CreepDN + '.' + IntToStr(VariablesUn.giZ) + '.' + IntToStr(VariablesUn.giG) + '.' + IntToStr(VariablesUn.giN);
      end
      else if isdc = 2 then //AC50
      if isdc = 1 then  //dc50  //st30
      begin
        vsCommand := '>' + vsType + '.50.' + VariablesUn.UID_CreepDN + '.' + IntToStr(VariablesUn.giZ) + '.' + IntToStr(VariablesUn.giG) + '.' + IntToStr(VariablesUn.giN);
      end
      else
      begin
        vsCommand := '>' + vsType + '.0.' + VariablesUn.UID_CreepDN + '.' + IntToStr(VariablesUn.giZ) + '.' + IntToStr(VariablesUn.giG) + '.' + IntToStr(VariablesUn.giN);
      end;
      ComFm.SendCommand(vsCommand, Nil);
    end
    else
    begin
      MessageDlg('Please enter a valid ZGN Address: n.n.n (n=0-255)', mtWarning, [mbOK], 0);
      if cmbZGN.Items.Count > 0 then
        cmbZGN.ItemIndex := 0;

      exit;
    end;
  end
  else if VariablesUn.giAdrType = 0 then
  begin
    if VariablesUn.IsZGN(vsZGN) and VariablesUn.IsNumber(vsG) and (StrToInt(vsG) >= 0) and (StrToInt(vsG) < 256) then
    begin
      if isdc = 1 then  //dc50  //st30
      begin
        vsCommand := '>' + vsType + '.10.' + VariablesUn.UID_CreepDN + '.' + IntToStr(VariablesUn.giZ) + '.' + vsG + '.' + IntToStr(VariablesUn.giN);
      end
      else if isdc = 2 then //AC50
      begin
        vsCommand := '>' + vsType + '.50.' + VariablesUn.UID_CreepDN + '.' + IntToStr(VariablesUn.giZ) + '.' + vsG + '.' + IntToStr(VariablesUn.giN);
      end
      else  //IQ
      begin
        vsCommand := '>' + vsType + '.0.' + VariablesUn.UID_CreepDN + '.' + IntToStr(VariablesUn.giZ) + '.' + vsG + '.' + IntToStr(VariablesUn.giN);
      end;
      ComFm.SendCommand(vsCommand, Nil);
    end
    else
    begin
      MessageDlg('Please enter a valid Group# (0-255)', mtWarning, [mbOK], 0);
      if cmbG.Items.Count > 0 then
        cmbG.ItemIndex := 0;

      exit;
    end;
  end;
end;

procedure TIQMainFm.imgUpMouseLeave(Sender: TObject);
begin
  //TImage(Sender).Visible := True;
end;



procedure TIQMainFm.imgWinkDnClick(Sender: TObject);
begin
 Wink(2);
end;

procedure TIQMainFm.imgWinkDnMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
    if TImage(Sender).Enabled then
    begin
      TImage(Sender).Visible := False;
      imgWinkUp.Visible := False;
    end;
end;

procedure TIQMainFm.imgWinkDnMouseLeave(Sender: TObject);
begin
  TImage(Sender).Visible := True;
  imgWinkUp.Visible := True;
end;

procedure TIQMainFm.imgWinkDnMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  TImage(Sender).Visible := True;
  imgWinkUp.Visible := True;
end;

procedure TIQMainFm.imgWinkUpClick(Sender: TObject);
begin
try
  Wink(2);
finally
  imgWinkUp.Visible := True;
end;
end;

procedure TIQMainFm.imgWinkUpMouseLeave(Sender: TObject);
begin
  //TImage(Sender).Visible := True;
end;

procedure TIQMainFm.imgWinkUpMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  //TImage(Sender).Visible := True;
end;

procedure TIQMainFm.lblAliasClick(Sender: TObject);
begin
   IQMainFm.Hide;
   NewProjectFm.Show
end;

procedure TIQMainFm.MovePrcntg(viPos: Integer);
var vsHi, vsLo, vsCommand, vsType, vsZGN: String;
begin
  try
//    if VariablesUn.IsNumber(GetCurrentUID) then
//    begin
//      vsHi := IntToStr(Hi(StrToInt(GetCurrentUID)));
//      vsLo := IntToStr(Lo(StrToInt(GetCurrentUID)));
    if not (VariablesUn.giAdrType=2) or (VariablesUn.giAdrType=1)then
    begin
      if (cmbCmdType.ItemIndex = 0) then
        vsType := '64'
      else
        vsType := '128';
      if isMNI or isIQMLC2 then
      begin
        if(variablesUn.giAdrType=0)then
        begin
          vsZGN := GetZGNAddress(cmbG.Text);
        end
        else if(variablesUn.giAdrType=1)then
        begin
          vsZGN := GetZGNAddress(cmbZGN.Text);
        end
        else if(variablesUn.giAdrType=0)then
        begin
          vsZGN := GetZGNAddress(cmbMotors.Text);
        end
      end
      else
      begin
        if(variablesUn.giAdrType=0)then
        begin
          vsZGN := GetAddress(cmbG.Text);
        end
        else if(variablesUn.giAdrType=1)then
        begin
          vsZGN := GetAddress(cmbZGN.Text);
        end
        else if(variablesUn.giAdrType=0)then
        begin
          vsZGN := GetAddress(cmbMotors.Text);
        end
      end;

      if (VariablesUn.giAdrType = 0) then
      begin
        if (VariablesUn.IsGTest(vsZGN))then
        begin
          vsCommand := '>' + vsType + '.' + IntToStr(viPos) + '.21.' + '255.'+vsZGN+'.255';
        end else if(VariablesUn.isZGN(vsZGN))then
        begin
          vsCommand := '>' + vsType + '.' + IntToStr(viPos) + '.21.' +vsZGN;
        end
        else
        begin
          MessageDlg('Please enter a valid Group# (0-255) or valid ZGN Address: n.n.n (n=0-255)', mtWarning, [mbOK], 0);
          if cmbG.Items.Count > 0 then
            cmbG.ItemIndex := 0;

          exit;
        end;
      end
      else if (VariablesUn.giAdrType = 1) then
      begin
        if VariablesUn.IsZGN(vsZGN) then
        begin
          vsCommand := '>' + vsType + '.' + IntToStr(viPos) + '.21.' + vsZGN;
        end
        else
        begin
          MessageDlg('Please enter a valid ZGN Address: n.n.n (n=0-255) ', mtWarning, [mbOK], 0);
          if cmbZGN.Items.Count > 0 then
            cmbZGN.ItemIndex := 0;

          exit;
        end;
      end;
//      else
//      begin
//        vsCommand := '>' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_IQPos + '.0.0.' + IntToStr(viPos);
//      end;
      cmbPosition.Text := '---';
      ComFm.SendCommand(vsCommand, rhMoveRecieved);

      Delay(500);

      VariablesUn.giShadePosition := -1;
      giLastPos := -1;
      GetPosition(GetCurrentUID);
    end
    else if VariablesUn.IsNumber(GetCurrentUID) then
    begin
      vsHi := IntToStr(Hi(StrToInt(GetCurrentUID)));
      vsLo := IntToStr(Lo(StrToInt(GetCurrentUID)));

      vsCommand := '>' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_IQPos + '.0.0.' + IntToStr(viPos);

      cmbPosition.Text := '---';
      ComFm.SendCommand(vsCommand, rhMoveRecieved);

      Delay(500);

      VariablesUn.giShadePosition := -1;
      giLastPos := -1;
      GetPosition(GetCurrentUID);
    end;
  except
  on E: Exception do
  begin
    MessageDlg('TIQMainFm.MovePrcntg: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;

procedure TIQMainFm.plNewProjectDblClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  IQMainFm.Hide;
  NewProjectFm.Show;
end;

procedure TIQMainFm.pnlMenuBarMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TIQMainFm.pnlMenuBarMouseLeave(Sender: TObject);
begin
  //pnlMenuBar.Visible := False;
end;

procedure TIQMainFm.pnlProjectClick(Sender: TObject);
begin
  SetSetpointsOff;
  pnlMenuBar.Visible := False;
  IQMainFm.Hide;
  NewProjectFm.Show;
end;

procedure TIQMainFm.rhMoveRecieved;
begin
  ComFm.rh_ReturnHandler := nil;
end;

procedure TIQMainFm.rhPortClosed(Sender: TObject);
begin
  ButtonsDisable;
  giComPortOn := False;
  shape14.Pen.Color:=clRed;
  VariablesUn.gbPowerOn:=True;
  variablesUn.gbUSBFound:=False;
end;

procedure TIQMainFm.rhPortOpend(Sender: TObject);
begin
 if cmbMotors.Items.Count > 0 then
 begin
    ButtonsEnable;
 end;

 giComPortOn := True;
 shape14.Pen.Color:=clLime;
 VariablesUn.gbPowerOn:=False;
 variablesUn.gbUSBFound:=True;
end;

procedure TIQMainFm.WaitforGenericResponse(UID:Integer;Command:String;rh:TNotifyEvent;ResendTime:Integer);
var
  PauseCount:Integer;
begin
  passFlag:=false;
  PauseCount:=0;
  while not PassFlag do
  begin
      PauseCount:=PauseCount+1;
      delay(1);
      if (PauseCount=ResendTime) or (PauseCount=ResendTime*2) then
        ComFm.GetGenericStatus(UID,Command,rh);
      if PauseCount>ResendTime*3 then
      begin
        ShowMessage('Failed Command');
        Break;
      end;
  end;
  outputdebugstring(pchar('*Wait LOOP '+IntToStr(PauseCount)));
end;

procedure TIQMainFm.rhPositionRecieved;
begin
  if not (VariablesUn.giShadePosition = -1) then
  begin
    cmbPosition.Visible := True;
    Label2.Visible := True;
    PassFlag:=true;
    //ShadePos.Position := VariablesUn.giShadePosition;
    if not isMNI then
    begin
      DisplayPosition(VariablesUn.giShadePosition);
    end;
    cmbPosition.Text := IntToStr(VariablesUn.giShadePosition);
    ComFm.rh_ReturnHandler := nil;
    Delay(2000);
    if not ((giLastPos - 1) = VariablesUn.giShadePosition) and not ((giLastPos + 1) = VariablesUn.giShadePosition) and not (giLastPos = VariablesUn.giShadePosition) then
    begin
      giLastPos := VariablesUn.giShadePosition;
      VariablesUn.giShadePosition := -1;
      if self.Visible=true then
      begin
        GetPosition(GetCurrentUID);
      end;
      DoubleFoundFlag:=False;
      OutputDebugString('*Flag Cleared*');
    end
    else
    begin
      if DoubleFoundFlag then
      begin
        giLastPos:=-1;
        DoubleFoundFlag:=False;
        OutputDebugString('*Flag 2*');
      end
      else
      begin
        DoubleFoundFlag:=True;
        giLastPos := VariablesUn.giShadePosition;
        VariablesUn.giShadePosition := -1;
        if self.Visible=true then
        begin
          GetPosition(GetCurrentUID);
        end;
        OutputDebugString('*Flag True 1*');
      end;
    end;
  end
  else
  begin
    cmbPosition.Text := '---';
    ComFm.rh_ReturnHandler := nil;
  end;
end;

procedure TIQMainFm.rhSetpointRecieved(Sender: TObject);
begin
  SetSetpointsOff;
  ComFm.rh_ReturnHandler := nil;
  MessageDlg('Set Point ' + gsSetPoint + ' has been set to the current position.', mtInformation, [mbOk], 0);
  gsSetPoint := '';
end;

procedure TIQMainFm.rhShowMain(Sender: TObject);
begin
  IQMainFm.Show;
end;
procedure TIQMainFm.rhShowMainDiscover(Sender: TObject);
begin
  IQMainFm.Show;
  DiscoverFm.Hide;
end;

procedure TIQMainFm.SelectProject;
begin
  try
    if not (DataUn.dmDataModule.dsProject.State = dsInactive) then
    begin
      DataUn.dmDataModule.dsProject.Filtered := False;
      DataUn.dmDataModule.dsProject.First;

      while not (dmDataModule.dsProject.Eof) do
      begin
        dmDataModule.dsProject.Edit;
        DataUn.dmDataModule.dsProject.FieldByName('Last').AsInteger := 0;
        DataUn.dmDataModule.dsProject.Post;
        DataUn.dmDataModule.dsProject.Next;
      end;
      //DataUn.dmDataModule.dsProject.Filtered := False;
      DataUn.dmDataModule.dsProject.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
      DataUn.dmDataModule.dsProject.Filtered := True;
      if not dmDataModule.dsProject.Eof then
      begin
        dmDataModule.dsProject.Edit;
        DataUn.dmDataModule.dsProject.FieldByName('Last').AsInteger := 1;
        DataUn.dmDataModule.dsProject.Post;
      end;
      DataUn.dmDataModule.dsProject.Filtered := False;
      dmDataModule.dsProject.MergeChangeLog;
      DataUn.dmDataModule.dsProject.SaveToFile(VariablesUn.gsDataPath + 'dsProject.txt', dfXML);
    end;
  except
  on E: Exception do
  begin
    MessageDlg('TIQMainFm.SelectProject: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;

procedure TIQMainFm.SetCom(Index: Integer);
begin
 ScreenSetupFm.cbComPort.ItemIndex := Index;
 NewProjectFm.cbComPort.ItemIndex := Index;
 ScreenSetupFm.cbComPortChange( ScreenSetupFm.cbComPort);
end;

procedure TIQMainFm.SetSetpointsOff;
begin
  imgSetpointSetOn.Visible := True;
  imgSetpoint1.Visible := False;
  imgSetpoint2.Visible := False;
  imgSetpoint3.Visible := False;
  imgIS1.Hint := 'Move Motor to Position1';
  imgIS2.Hint := 'Move Motor to Position2';
  imgIS3.Hint := 'Move Motor to Position3';
end;

procedure TIQMainFm.SetSetpointsOn;
begin
  DataUn.dmDataModule.dsMotors.Filtered := False;
  DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt;
  DataUn.dmDataModule.dsMotors.Filtered := True;
  if not DataUn.dmDataModule.dsMotors.eof then
  begin
    if DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='74' then
    begin
      DataUn.dmDataModule.dsMotors.Filtered := False;
      DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt + ' and MotorNumber = '+cmbMotorSelector.text;
      DataUn.dmDataModule.dsMotors.Filtered := True;
      if DataUn.dmDataModule.dsMotors.FieldByName('MotorPerson').AsString='1' then
      begin
        imgSetpointSetOn.Visible := False;
        imgSetpoint2.Visible := True;
        imgIS2.Hint := 'Fix Set Point 2 to the current position';
      end
      else if DataUn.dmDataModule.dsMotors.FieldByName('MotorPerson').AsString='2' then
      begin
        imgSetpointSetOn.Visible := False;
        imgSetpoint1.Visible := True;
        imgSetpoint2.Visible := True;
        imgSetpoint3.Visible := True;
        imgIS1.Hint := 'Fix Set Point 1 to the current position';
        imgIS2.Hint := 'Fix Set Point 2 to the current position';
        imgIS3.Hint := 'Fix Set Point 3 to the current position';
      end
      else if DataUn.dmDataModule.dsMotors.FieldByName('MotorPerson').AsString='3' then
      begin
        imgSetpointSetOn.Visible := False;
        imgSetpoint2.Visible := True;
        imgIS2.Hint := 'Fix Set Point 2 to the current position';
      end
      else if DataUn.dmDataModule.dsMotors.FieldByName('MotorPerson').AsString='4' then
      begin
        ShowMessage('Setting SetPoints not allowd for WT3000!')
      end
      else if DataUn.dmDataModule.dsMotors.FieldByName('MotorPerson').AsString='5' then
      begin
        ShowMessage('Setting SetPoints not allowd for WT1000!')
      end
      else if DataUn.dmDataModule.dsMotors.FieldByName('MotorPerson').AsString='6' then
      begin
        imgSetpointSetOn.Visible := False;
        imgSetpoint2.Visible := True;
        imgIS2.Hint := 'Fix Set Point 2 to the current position';
      end
      else if DataUn.dmDataModule.dsMotors.FieldByName('MotorPerson').AsString='7' then
      begin
        imgSetpointSetOn.Visible := False;
        imgSetpoint2.Visible := True;
        imgIS2.Hint := 'Fix Set Point 2 to the current position';
      end
      else if DataUn.dmDataModule.dsMotors.FieldByName('MotorPerson').AsString='8' then
      begin
        imgSetpointSetOn.Visible := False;
        imgSetpoint2.Visible := True;
        imgIS2.Hint := 'Fix Set Point 2 to the current position';
      end
      else if DataUn.dmDataModule.dsMotors.FieldByName('MotorPerson').AsString='9' then
      begin
        imgSetpointSetOn.Visible := False;
        imgSetpoint1.Visible := True;
        imgSetpoint2.Visible := True;
        imgSetpoint3.Visible := True;
        imgIS1.Hint := 'Fix Set Point 1 to the current position';
        imgIS2.Hint := 'Fix Set Point 2 to the current position';
        imgIS3.Hint := 'Fix Set Point 3 to the current position';
      end;
    end
    else
    begin
      imgSetpointSetOn.Visible := False;
      imgSetpoint1.Visible := True;
      imgSetpoint2.Visible := True;
      imgSetpoint3.Visible := True;
      imgIS1.Hint := 'Fix Set Point 1 to the current position';
      imgIS2.Hint := 'Fix Set Point 2 to the current position';
      imgIS3.Hint := 'Fix Set Point 3 to the current position';
    end;
  end;


end;

procedure TIQMainFm.ShadePosChange(Sender: TObject);
begin
 //MovePrcntg(ShadePos.Position);
end;

procedure TIQMainFm.Shape14MouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible:=false;
end;

procedure TIQMainFm.ShowUID;
var DeviceType,ComboBoxString:String;
  I: Integer;
  ComboList:TStringList;
  InsertFlag:Boolean;
  FirstFlag:Boolean;
  LastUID:Integer;
begin
  FirstFlag:=true;
  ComboList:=Tstringlist.Create;
  try
    if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
    begin
      DataUn.dmDataModule.dsMotors.Filtered := False;
      DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
      DataUn.dmDataModule.dsMotors.Filtered := True;
    end;
    ComboList.Sorted:=true;
    Combolist.Duplicates:=dupIgnore;
    while not dmDataModule.dsMotors.Eof do
    begin
      if DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='66' then
      begin
       DeviceType:='IQ2';
      end
      else if DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='69' then
      begin
       DeviceType:='IQ2-DC';
      end
      else if DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='70' then
      begin
       DeviceType:='iQ3-DC';
      end
      else if DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='77' then
      begin
       DeviceType:='DC50';
      end
      else if DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='78' then
      begin
       DeviceType:='AC50';
      end
      else if DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='79' then
      begin
       DeviceType:='ST30';
      end
      else if DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='205' then
      begin
       DeviceType:='DC50 SW';
      end
      else if DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='206' then
      begin
       DeviceType:='AC50 SW';
      end
      else if DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='207' then
      begin
       DeviceType:='ST30 SW';
      end
      else if DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='74' then
      begin
       DeviceType:='MNI';
      end
      else if DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='65' then
      begin
       DeviceType:='IQMLC2';
      end;
      ComboBoxString:=IntToStr(DataUn.dmDataModule.dsMotors.FieldByName('UID').AsInteger)+' : '+DeviceType;

      ComboList.Add(ComboBoxString);
      dmDataModule.dsMotors.Next;
    end;
    ComboList.Sorted:=False;
    SortStringListUIDS(ComboList);
    cmbMotors.items.Assign(ComboList);
    if cmbMotors.Items.Count > 0 then
    begin
      cmbMotors.Enabled := True;
      if VariablesUn.gsUIDCrnt<>'' then
      begin
        for I := 0 to cmbMotors.Items.Count-1 do
        begin
          if Pos(VariablesUn.gsUIDCrnt,cmbMotors.Items.Create[I])>0 then
          begin
            cmbMotors.ItemIndex:=I;
            break;
          end;
          if I = cmbMotors.Items.Count-1 then
          begin
            cmbMotors.ItemIndex:=0;
            VariablesUn.gsUIDCrnt:=cmbMotors.Items.Create[0];
          end;
        end;
      end
      else
      begin
        cmbMotors.ItemIndex := 0;
      end;
      cmbMotorsChange(cmbMotors);
      if (VariablesUn.giAdrType <> 0) and (VariablesUn.giAdrType <> 1) then
      begin
        lblUID.Visible := True;
        cmbMotors.Visible := True;
        cmbposition.Visible:=true;
        label2.Visible:=true;
        VariablesUn.giAdrType := 2;
        txtAlias.ReadOnly := False;
        imgSetpointSetOn.Hint := 'Set Intermediate Set Points';
        if giComPortOn then
        begin
          imgSetpointSetOn.Enabled := True;
        end;
      end;

      if giComPortOn then
      begin
        ButtonsEnable;
      end;
    end
    else
    begin
      cmbMotors.Enabled := False;
      ButtonsDisable;
    end;
    DataUn.dmDataModule.dsMotors.Filtered := False;
  except
  on E: Exception do
  begin
    MessageDlg('TIQMainFm.ShowProject: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;

Procedure TIQMainFm.SortStringListUIDS(UIDList:TStringList);
var
  I:Integer;
  First,Second:Integer;
  Swap:Boolean;
  TempStr:String;
begin
  Swap:=true;
  while Swap do
  begin
    Swap:=False;
    for I := 0 to UIDList.Count-2 do
    begin
      First:=StrToInt(GetUID(UIDList[I]));
      Second:=StrToInt(GetUID(UIDList[I+1]));
      if First > Second then
      begin
        TempStr:=UIDList[I];
        UIDList[I]:=UIDList[I+1];
        UIDList[I+1]:=TempStr;
        Swap:=true;
      end;
    end;
  end;
end;

Function TIQMainFm.GetUID(MixedUID:String):String;
var SpacePosition:Integer;
begin
    SpacePosition:=pos(' ', MixedUID)-1;
    GetUID:=LeftStr(MixedUID,SpacePosition);

end;

Function TIQMainFm.GetCurrentUID():String;
var SpacePosition:Integer;
begin
  //if(cmbMotors.Visible)then

  //if(VariablesUn.giAdrType=2)then
  if(cmbMotors.text<>'')and(VariablesUn.giAdrType=2)then
  begin
    SpacePosition:=pos(' ', cmbMotors.text)-1;
    if(SpacePosition=(-1))then
    begin
        GetCurrentUID:=trim(cmbMotors.text);
    end
    else
    begin
      GetCurrentUID:=trim(LeftStr(cmbMotors.text,SpacePosition));
    end;
  end
  else if(cmbZGN.text<>'')and(VariablesUn.giAdrType=1)then//else if(VariablesUn.giAdrType=1)then//  else if cmbZGN.Visible then
  begin
    SpacePosition:= pos(':', cmbZGN.text)-1;
    GetCurrentUID:=trim(LeftStr(cmbZGN.text,SpacePosition));
  end
  else if(cmbG.text<>'')and(VariablesUn.giAdrType=0) then//else if(VariablesUn.giAdrType=0)then//else if cmbG.Visible then
  begin
    SpacePosition:= pos(':', cmbG.Text)-1;
    GetCurrentUID:=trim(LeftStr(cmbG.text,SpacePosition));
  end
  else
  begin
    SpacePosition:=pos(' ', cmbMotors.text)-1;
    GetCurrentUID:=trim(LeftStr(cmbMotors.text,SpacePosition));
  end;

end;


procedure TIQMainFm.ShowZGN;
var
  vsAddress, vsUID,vsLoopUID: String;
  ZGNUIDList,GUIDList:TStringList;
  i:integer;
begin
  ZGNUIDList:=TStringList.Create;
  GUIDList:=TStringList.Create;
  try
    cmbZGN.Clear;
    cmbG.Clear;
//    if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then //and (VariablesUn.IsNumber(GetCurrentUID)) then
    begin

      DataUn.dmDataModule.dsMotors.Filtered := False;
      DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName); //+  ' and UID = ' + cmbMotors.Text;
      DataUn.dmDataModule.dsMotors.Filtered := True;

      DataUn.dmDataModule.dsMotors.First;
      if DataUn.dmDataModule.dsMotors.FieldByName('MotorNumber').AsInteger=0 then
      begin
        vsUID := IntToStr(DataUn.dmDataModule.dsMotors.FieldByName('UID').AsInteger);
        vsUID := vsUID + ' : ';

      end
      else
      begin
        vsUID := IntToStr(DataUn.dmDataModule.dsMotors.FieldByName('UID').AsInteger);
        vsUID := vsUID + ' : ';
        vsUID :=vsUID+DataUn.dmDataModule.dsMotors.FieldByName('MotorNumber').AsString+':';
      end;
      while not (DataUn.dmDataModule.dsMotors.Eof) do
      begin
        if DataUn.dmDataModule.dsMotors.FieldByName('MotorNumber').AsInteger=0 then
        begin
          vsLoopUID := IntToStr(DataUn.dmDataModule.dsMotors.FieldByName('UID').AsInteger);
          vsLoopUID := vsLoopUID + ' : ';

        end
        else
        begin
          vsLoopUID := IntToStr(DataUn.dmDataModule.dsMotors.FieldByName('UID').AsInteger);
          vsLoopUID := vsLoopUID + ' : ';
          vsLoopUID :=vsLoopUID+DataUn.dmDataModule.dsMotors.FieldByName('MotorNumber').AsString+':';
        end;
        vsAddress :=  DataUn.dmDataModule.dsMotors.FieldByName('Address1').AsString;

        if VariablesUn.IsZGN(vsAddress) then
        begin
          if (VariablesUn.giZ=255)and(VariablesUn.giN=255) then
          begin
            GUIDList.add(vsLoopUID + IntToStr(VariablesUn.giG));
          end
          else
          begin
            GUIDList.add(vsLoopUID + vsAddress);
          end;
          ZGNUIDList.Add(vsLoopUID + vsAddress);
        end;
        vsAddress := DataUn.dmDataModule.dsMotors.FieldByName('Address2').AsString;
        if VariablesUn.IsZGN(vsAddress) then
        begin
          if (VariablesUn.giZ=255)and(VariablesUn.giN=255) then
          begin
            GUIDList.add(vsLoopUID + IntToStr(VariablesUn.giG));
          end
          else
          begin
            GUIDList.add(vsLoopUID + vsAddress);
          end;
          ZGNUIDList.Add(vsLoopUID + vsAddress);
        end;
        vsAddress := DataUn.dmDataModule.dsMotors.FieldByName('Address3').AsString;
        if VariablesUn.IsZGN(vsAddress) then
        begin
          if (VariablesUn.giZ=255)and(VariablesUn.giN=255) then
          begin
            GUIDList.add(vsLoopUID + IntToStr(VariablesUn.giG));
          end
          else
          begin
            GUIDList.add(vsLoopUID + vsAddress);
          end;
          ZGNUIDList.Add(vsLoopUID + vsAddress);
        end;
        vsAddress := DataUn.dmDataModule.dsMotors.FieldByName('Address4').AsString;
        if VariablesUn.IsZGN(vsAddress) then
        begin
          if (VariablesUn.giZ=255)and(VariablesUn.giN=255) then
          begin
            GUIDList.add(vsLoopUID + IntToStr(VariablesUn.giG));
          end
          else
          begin
            GUIDList.add(vsLoopUID + vsAddress);
          end;
          ZGNUIDList.Add(vsLoopUID + vsAddress);
        end;
           //cmbG.Items.Add(vsUID + IntToStr(VariablesUn.giG));
        vsAddress := DataUn.dmDataModule.dsMotors.FieldByName('Address5').AsString;
        //cmbZGN.Items.Add(vsUID + vsAddress);
        if VariablesUn.IsZGN(vsAddress) then
        begin
          if (VariablesUn.giZ=255)and(VariablesUn.giN=255) then
          begin
            GUIDList.add(vsLoopUID + IntToStr(VariablesUn.giG));
          end
          else
          begin
            GUIDList.add(vsLoopUID + vsAddress);
          end;
          ZGNUIDList.Add(vsLoopUID + vsAddress);
        end;
           //cmbG.Items.Add(vsUID + IntToStr(VariablesUn.giG));
        vsAddress := DataUn.dmDataModule.dsMotors.FieldByName('Address6').AsString;
        //cmbZGN.Items.Add(vsUID + vsAddress);
        if VariablesUn.IsZGN(vsAddress) then
        begin
          if (VariablesUn.giZ=255)and(VariablesUn.giN=255) then
          begin
            GUIDList.add(vsLoopUID + IntToStr(VariablesUn.giG));
          end
          else
          begin
            GUIDList.add(vsLoopUID + vsAddress);
          end;
          ZGNUIDList.Add(vsLoopUID + vsAddress);
        end;
           //cmbG.Items.Add(vsUID + IntToStr(VariablesUn.giG));
        vsAddress := DataUn.dmDataModule.dsMotors.FieldByName('Address7').AsString;
        //cmbZGN.Items.Add(vsUID + vsAddress);
        if VariablesUn.IsZGN(vsAddress) then
        begin
          if (VariablesUn.giZ=255)and(VariablesUn.giN=255) then
          begin
            GUIDList.add(vsLoopUID + IntToStr(VariablesUn.giG));
          end
          else
          begin
            GUIDList.add(vsLoopUID + vsAddress);
          end;
          ZGNUIDList.Add(vsLoopUID + vsAddress);
        end;
           //cmbG.Items.Add(vsUID + IntToStr(VariablesUn.giG));
        vsAddress := DataUn.dmDataModule.dsMotors.FieldByName('Address8').AsString;
        //cmbZGN.Items.Add(vsUID + vsAddress);
        if VariablesUn.IsZGN(vsAddress) then
        begin
          if (VariablesUn.giZ=255)and(VariablesUn.giN=255) then
          begin
            GUIDList.add(vsLoopUID + IntToStr(VariablesUn.giG));
          end
          else
          begin
            GUIDList.add(vsLoopUID + vsAddress);
          end;
          ZGNUIDList.Add(vsLoopUID + vsAddress);
        end;
           //cmbG.Items.Add(vsUID + IntToStr(VariablesUn.giG));
        vsAddress := DataUn.dmDataModule.dsMotors.FieldByName('LvPort').AsString;
        //cmbZGN.Items.Add(vsUID + vsAddress);
        if VariablesUn.IsZGN(vsAddress) then
        begin
          if (VariablesUn.giZ=255)and(VariablesUn.giN=255) then
          begin
            GUIDList.add(vsLoopUID + IntToStr(VariablesUn.giG));
          end
          else
          begin
            GUIDList.add(vsLoopUID + vsAddress);
          end;
          ZGNUIDList.Add(vsLoopUID + vsAddress);
        end;
           //cmbG.Items.Add(vsUID + IntToStr(VariablesUn.giG));
        DataUn.dmDataModule.dsMotors.Next
      end;
      SortStringListUIDS(ZGNUIDList);
      cmbZGN.items.Assign(ZGNUIDList);

      SortStringListUIDS(GUIDList);
      cmbG.items.Assign(GUIDList);

      if cmbZGN.Items.Count > 0 then
      begin
        cmbZGN.Enabled := True;
        for i:=0 to ZGNUIDList.Count-1 do
        begin
           if(pos(vsUID,ZGNUIDList[i])>0)then
           begin
             cmbZGN.ItemIndex:=i;
             break;
           end;
        end;
        //cmbZGN.ItemIndex := 0;
        cmbG.Enabled := True;
        for i:=0 to GUIDList.Count-1 do
        begin
           if(pos(vsUID,GUIDList[i])>0)then
           begin
             cmbG.ItemIndex:=i;
             break;
           end;
        end;
        //cmbG.ItemIndex := 0;
        if VariablesUn.giAdrType = 0 then
        begin
          //cmbZGN.Visible := False;
          //lblZGN.Visible := False;
          //cmbMotors.Style := csDropDownList;
          cmbG.Style := csDropDown;
          //cmbMotors.Color := clWindow;
          //cmbG.Color := $00BBEECC;
          cmbG.Visible := True;
          lblG.Visible := True;
          //cmbCmdType.Visible := True;
          cmbCmdType.ItemIndex := VariablesUn.giCmdType;
          lblCmdType.Caption := cmbCmdType.Text;
          lblCmdType.Visible := True;
          cmbGChange(cmbG);
        end
        else if VariablesUn.giAdrType = 1 then
        begin
          //cmbG.Visible := False;
          //lblG.Visible := False;
          //cmbMotors.Style := csDropDownList;
          cmbZGN.Style := csDropDown;
          //cmbMotors.Color := clWindow;
          //cmbZGN.Color := $00BBEECC;
          cmbZGN.Visible := True;
          lblZGN.Visible := True;
          //cmbCmdType.Visible := True;
          cmbCmdType.ItemIndex := VariablesUn.giCmdType;
          lblCmdType.Caption := cmbCmdType.Text;
          lblCmdType.Visible := True;
          cmbGChange(cmbZGN);
        end;
      end
      else
      begin
        cmbZGN.Enabled := False;
        cmbG.Enabled := False;
      end;
      DataUn.dmDataModule.dsMotors.Filtered := False;
    end;
  except
  on E: Exception do
  begin
    MessageDlg('TIQMainFm.ShowZGN: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;

procedure TIQMainFm.ShowProject;
var viIndex: Integer;
begin
//  try

  //cmbMotors.Style := csDropDownList;
  cmbMotors.Color := clWhite;
  cmbMotors.Items.Clear;
  cmbMotors.Visible := False;
  lblUID.Visible := False;
  cmbG.Style := csDropDownList;
  cmbG.Color := clWhite;
  cmbG.Items.Clear;
  cmbG.Visible := False;
  lblG.Visible := False;
  cmbZGN.Style := csDropDownList;
  cmbZGN.Color := clWhite;
  cmbZGN.Items.Clear;
  cmbZGN.Visible := False;
  lblZGN.Visible := False;
  cmbCmdType.Visible := False;
  lblCmdType.Visible := False;
  txtAlias.ReadOnly := True;
  txtAlias.OnChange := nil;
  txtAlias.Text := '';
  txtAlias.OnChange := txtAliasChange;
  btnUID.Visible := False;


    //settingsfm.findComPort;
  ScreenSetupFm.FindComPort;
//    if not(VariablesUn.giLastForm='Cancel')then
//    begin
  cbProject.OnChange := nil;
  cbProject.Items.Clear;
  if not (DataUn.dmDataModule.dsProject.State = dsInactive) then
  begin
    VariablesUn.giLastForm:='';
    DataUn.dmDataModule.dsProject.Filtered := False;
    DataUn.dmDataModule.dsProject.First;
    while not DataUn.dmDataModule.dsProject.eof do
    begin
      if (DataUn.dmDataModule.dsProject.FieldByName('Last').AsInteger = 1) then
        cbProject.Items.Insert(0, DataUn.dmDataModule.dsProject.FieldByName('ProjectName').AsString)
      else
        cbProject.Items.Add(DataUn.dmDataModule.dsProject.FieldByName('ProjectName').AsString);

      DataUn.dmDataModule.dsProject.Next;
    end;
    if cbProject.Items.Count > 0 then
    begin
      pnlProject.Visible := False;
      cbProject.Visible := True;
      cbProject.ItemIndex := 0;
      VariablesUn.gsProjectName := cbProject.Text;

      DataUn.dmDataModule.dsProject.Filtered := False;
      DataUn.dmDataModule.dsProject.Filter := 'ProjectName = ' + QuotedStr(cbProject.Text);
      DataUn.dmDataModule.dsProject.Filtered := True;
      if not dmDataModule.dsProject.Eof then
      begin
        if not (DataUn.dmDataModule.dsProject.FieldValues['AdrType'] = Null) then
          VariablesUn.giAdrType := DataUn.dmDataModule.dsProject.FieldValues['AdrType'];

        if not (DataUn.dmDataModule.dsProject.FieldValues['CmdType'] = Null) then
          VariablesUn.giCmdType := DataUn.dmDataModule.dsProject.FieldValues['CmdType'];


        if not VariablesUn.gbUSBfound then
        begin
          //viIndex := SettingsFm.cbComPort.Items.IndexOf(DataUn.dmDataModule.dsProject.FieldByName('ComPort').AsString);
          viIndex := ScreenSetupfm.cbComPort.Items.IndexOf(DataUn.dmDataModule.dsProject.FieldByName('ComPort').AsString);

          if viIndex > -1 then
          begin
            SetCom(viIndex);
            VariablesUn.gsComPortUSB:=DataUn.dmDataModule.dsProject.FieldByName('ComPort').AsString;
          end
          else
          begin
            if VariablesUn.gbPowerOn then
            begin
              VariablesUn.gsComPortUSB := ComFm.FindUSB;
              if (VariablesUn.gsComPortUSB <> '-1') and (VariablesUn.gsComPortUSB <> '') then
              begin
                viIndex := ScreenSetupfm.cbComPort.Items.IndexOf(VariablesUn.gsComPortUSB);
                if viIndex > -1 then
                begin
                   VariablesUn.gbUSBfound := True;
                   SetCom(viIndex);
                   //VariablesUn.gsComPortUSB:='COM'+intToStr(viIndex);
                   //ScreenSetupFm.FindComPort;
                end
                else
                  VariablesUn.gbUSBfound := False;
              end;
              VariablesUn.gbPowerOn := False;
              //VariablesUn.gsComPortUSB:=DataUn.dmDataModule.dsProject.FieldByName('ComPort').AsString;
            end;
          end;
        end;
      end;
      DataUn.dmDataModule.dsProject.Filtered := False;
      ShowZGN;
      ShowUID;
      delay(1000);

      if IsNumber(GetCurrentUID) then
        VariablesUn.gsUIDCrnt := GetCurrentUID
      else
        VariablesUn.gsUIDCrnt := '';

      //btnSetup.SetFocus;
    end;
  end
  else
  begin
    if VariablesUn.gbPowerOn then
    begin
      VariablesUn.gsComPortUSB := ComFm.FindUSB;
      if (VariablesUn.gsComPortUSB <> '-1') and (VariablesUn.gsComPortUSB <> '') then
      begin
        viIndex := ScreenSetupfm.cbComPort.Items.IndexOf(VariablesUn.gsComPortUSB);
        if viIndex > -1 then
        begin
          VariablesUn.gbUSBfound := True;
          SetCom(viIndex);
        end
        else
        begin
          VariablesUn.gbUSBfound := False;
        end;
      end;
      VariablesUn.gbPowerOn := False;
    end;
    DataUn.dmDataModule.dsProject.CreateDataSet;
    DataUn.dmDataModule.dsProject.Active := True;
    DataUn.dmDataModule.dsProject.Insert;
    DataUn.dmDataModule.dsProject.FieldByName('ProjectName').AsString := 'DefaultProject';
    DataUn.dmDataModule.dsProject.FieldByName('Last').AsInteger := 1;
    DataUn.dmDataModule.dsProject.FieldByName('ComPort').AsString := 'COM'+intToStr(ComFm.ComPort.ComNumber);
    DataUn.dmDataModule.dsProject.FieldByName('AdrType').AsInteger := 2;
    DataUn.dmDataModule.dsProject.FieldByName('CmdType').AsInteger := 0;
    DataUn.dmDataModule.dsProject.FieldByName('ZGNG').AsInteger := 1;
    DataUn.dmDataModule.dsProject.Post;
    cbProject.Items.Add(DataUn.dmDataModule.dsProject.FieldByName('ProjectName').AsString);
    cbProject.itemIndex:=0;
    DataUn.dmDataModule.dsProject.SaveToFile(VariablesUn.gsDataPath + 'dsProject.txt', dfXML);
    VariablesUn.gsProjectName := 'DefaultProject';
  end;
  //cbProject.Items.Add('Add New');
  cbProject.OnChange := cbProjectChange;

//        except
//        on E: Exception do
//        begin
//     cbProject.OnChange := cbProjectChange;
//      MessageDlg('TIQMainFm.ShowProject: ' + E.Message, mtWarning, [mbOk], 0);
//    end;
  //End;


end;

procedure TIQMainFm.btnConfigClick(Sender: TObject);
begin
  cmbDeviceSelector.Enabled:=False;
  pnlMenuBar.Visible := False;
  //IQMainFm.Hide;
  ConfigScreenFm.Show;
  Hide;
  ConfigScreenFm.sbMain.VertScrollBar.Position:=0;
  ConfigScreenFm.btnGetStatusClick(nil);
  cmbDeviceSelector.Enabled:=True;
end;

procedure TIQMainFm.btnConfigMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TIQMainFm.btnControlMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TIQMainFm.btnExitClick(Sender: TObject);
begin
  DataUn.dmDataModule.dsMotors.Filtered := False;
  DataUn.dmDataModule.dsMWCChannels.Filtered := False;
  DataUn.dmDataModule.dsMWCs.Filtered := False;
  DataUn.dmDataModule.dsMNI.Filtered := False;
  DataUn.dmDataModule.dsMNI2.Filtered := False;
  DataUn.dmDataModule.dsIQMLC2.Filtered := False;
  DataUn.dmDataModule.dsPowerPanel.Filtered := False;

  DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
  DataUn.dmDataModule.dsMWCs.SaveToFile(VariablesUn.gsDataPath + 'dsMWCs.txt', dfXML);
  DataUn.dmDataModule.dsMWCChannels.SaveToFile(VariablesUn.gsDataPath + 'dsMWCChannels.txt', dfXML);
  DataUn.dmDataModule.dsMNI.SaveToFile(VariablesUn.gsDataPath + 'dsMNI.txt', dfXML);
  DataUn.dmDataModule.dsMNI2.SaveToFile(VariablesUn.gsDataPath + 'dsMNI2.txt', dfXML);
  DataUn.dmDataModule.dsIQMLC2.SaveToFile(VariablesUn.gsDataPath + 'dsIQMLC2.txt', dfXML);
  DataUn.dmDataModule.dsPowerPanel.SaveToFile(VariablesUn.gsDataPath + 'dsPowerPanel.txt', dfXML);
  pnlMenuBar.Visible := False;
  Application.Terminate;
end;

procedure TIQMainFm.btnExitMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TIQMainFm.btnFindClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  IQMainFm.Hide;
  DiscoverFm.Show;
end;

procedure TIQMainFm.btnFindMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TIQMainFm.btnHelpClick(Sender: TObject);
begin
  DataUn.dmDataModule.dsMWCChannels.Filtered := False;
  DataUn.dmDataModule.dsMWCChannels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
  DataUn.dmDataModule.dsMWCChannels.Filtered := True;

  if not dmDataModule.dsMWCChannels.Eof then
  begin
    Hide;
    MWCConfigfm.rh_ShowMain := rhShowMain;
    MWCConfigfm.Show;
  end
  else ShowMessage('No MWC Devices Found');

end;

procedure TIQMainFm.btnInfoClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  Hide;
  AboutFm.rh_ShowForm := rhShowMain;
  AboutFm.Show;
end;

procedure TIQMainFm.btnLimitSettingClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  LimitSettingFm.rh_showmain := rhShowMain;
  IQMainFm.Hide;
  LimitSettingFm.Show;
end;

procedure TIQMainFm.btnLimitSettingMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := true;
end;

procedure TIQMainFm.SpeedButton3Click(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TIQMainFm.txtAliasChange(Sender: TObject);
var
  vsAlias,vsCommand:String;
  I:Integer;
begin
  try
//    if VariablesUn.IsNumber(GetUID(cmbMotors.text)) then
//    begin
//      if vsOldAlias<>trim(txtAlias.text) then
//      begin
//        vsOldAlias:=trim(txtAlias.text);
//        giUID :=  StrToInt(GetUID(cmbMotors.text));
//        vsAlias:=txtAlias.Text;
//        vsAlias := vsAlias + StringOfChar ( Char(#0), 12 - Length(vsAlias) );
//        vsCommand:=VariablesUn.UID_NAME+'.'+vsAlias;
//        ComFm.SetGenericParam(giUID,vsCommand, nil);
//        Delay(200);
//
//        if isMNI then
//        begin
//          for I := 1 to 4 do
//          begin
//            DataUn.dmDataModule.dsMotors.Filtered := False;
//            DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt+ ' and MotorNumber = '+IntToStr(I);
//            DataUn.dmDataModule.dsMotors.Filtered := True;
//            if not (DataUn.dmDataModule.dsMotors.Eof) then
//            begin
//              DataUn.dmDataModule.dsMotors.Edit;
//              DataUn.dmDataModule.dsMotors.FieldByName('Alias').AsString := txtAlias.Text;
//              VariablesUn.gsAlias := txtAlias.Text;
//              DataUn.dmDataModule.dsMotors.Post;
//              DataUn.dmDataModule.dsMotors.MergeChangeLog;
//              DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
//
//            end;
//          end;
//          DataUn.dmDataModule.dsMNI.Filtered := False;
//          DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt;
//          DataUn.dmDataModule.dsMNI.Filtered := True;
//          if not (DataUn.dmDataModule.dsMNI.Eof) then
//          begin
//            DataUn.dmDataModule.dsMNI.Edit;
//            DataUn.dmDataModule.dsMNI.FieldByName('Alias').AsString := txtAlias.Text;
//            DataUn.dmDataModule.dsMNI.Post;
//            DataUn.dmDataModule.dsMNI.MergeChangeLog;
//            DataUn.dmDataModule.dsMNI.SaveToFile(VariablesUn.gsDataPath + 'dsMNI.txt', dfXML);
//
//          end;
//        end
//        else
//        begin
//          DataUn.dmDataModule.dsMotors.Filtered := False;
//          DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt;
//          DataUn.dmDataModule.dsMotors.Filtered := True;
//
//          if not (DataUn.dmDataModule.dsMotors.Eof) then
//          begin
//            DataUn.dmDataModule.dsMotors.Edit;
//            DataUn.dmDataModule.dsMotors.FieldByName('Alias').AsString := txtAlias.Text;
//            VariablesUn.gsAlias := txtAlias.Text;
//            DataUn.dmDataModule.dsMotors.Post;
//            DataUn.dmDataModule.dsMotors.MergeChangeLog;
//            DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
//          end;
//
//          DataUn.dmDataModule.dsMotors.Filtered := False;
//        end;
//      end;
//    end
//    else
//    begin
//      txtAlias.Text := '';
//    end;
 except
 on E: Exception do
 begin
  MessageDlg('TConfigScreenFm.lblAliasChange: ' + E.Message, mtWarning, [mbOk], 0);
 end;
 End;
end;

procedure TIQMainFm.txtAliasDblClick(Sender: TObject);
begin
  NewProjectFm.rh_ShowLast := rhShowMain;
  SetSetpointsOff;
  IQMainFm.Hide;
  NewProjectFm.Show
end;

procedure TIQMainFm.txtAliasEnterKey(Sender: TObject);
var
  vsAlias,vsCommand:String;
  I:Integer;
begin
  if(VariablesUn.giAdrType=2)then
  begin
    if VariablesUn.IsNumber(GetCurrentUID) then
    begin
      if vsOldAlias<>trim(txtAlias.text) then
      begin
        vsOldAlias:=trim(txtAlias.text);
        giUID :=  StrToInt(GetCurrentUID);
        vsAlias:=txtAlias.Text;
        vsAlias := vsAlias + StringOfChar ( Char(#0), 12 - Length(vsAlias) );
        vsCommand:=VariablesUn.UID_NAME+'.'+vsAlias;
        ComFm.SetGenericParam(giUID,vsCommand, nil);
        Delay(200);

        if isMNI or isIQMLC2 then
        begin
          for I := 1 to 4 do
          begin
            DataUn.dmDataModule.dsMotors.Filtered := False;
            DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt+ ' and MotorNumber = '+IntToStr(I);
            DataUn.dmDataModule.dsMotors.Filtered := True;
            if not (DataUn.dmDataModule.dsMotors.Eof) then
            begin
              DataUn.dmDataModule.dsMotors.Edit;
              DataUn.dmDataModule.dsMotors.FieldByName('Alias').AsString := txtAlias.Text;
              VariablesUn.gsAlias := txtAlias.Text;
              DataUn.dmDataModule.dsMotors.Post;
              DataUn.dmDataModule.dsMotors.MergeChangeLog;
              DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);

            end;
          end;
          DataUn.dmDataModule.dsMNI.Filtered := False;
          DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt;
          DataUn.dmDataModule.dsMNI.Filtered := True;
          if not (DataUn.dmDataModule.dsMNI.Eof) then
          begin
            DataUn.dmDataModule.dsMNI.Edit;
            DataUn.dmDataModule.dsMNI.FieldByName('Alias').AsString := txtAlias.Text;
            DataUn.dmDataModule.dsMNI.Post;
            DataUn.dmDataModule.dsMNI.MergeChangeLog;
            DataUn.dmDataModule.dsMNI.SaveToFile(VariablesUn.gsDataPath + 'dsMNI.txt', dfXML);

          end;
        end
        else
        begin
          DataUn.dmDataModule.dsMotors.Filtered := False;
          DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt;
          DataUn.dmDataModule.dsMotors.Filtered := True;

          if not (DataUn.dmDataModule.dsMotors.Eof) then
          begin
            DataUn.dmDataModule.dsMotors.Edit;
            DataUn.dmDataModule.dsMotors.FieldByName('Alias').AsString := txtAlias.Text;
            VariablesUn.gsAlias := txtAlias.Text;
            DataUn.dmDataModule.dsMotors.Post;
            DataUn.dmDataModule.dsMotors.MergeChangeLog;
            DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
          end;

          DataUn.dmDataModule.dsMotors.Filtered := False;
        end;
      end;
    end
    else
    begin
      txtAlias.Text := vsOldAlias;
    end;
  end
  else
  begin
    MessageDlg('Alias can only be changed in UID mode.', mtWarning, [mbOk], 0);
    txtAlias.Text := vsOldAlias;
  end;
end;

procedure TIQMainFm.txtAliasKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #$D then
  begin
    txtAliasEnterKey(self);
  end;
end;

procedure TIQMainFm.txtAliasMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TIQMainFm.txtProjectNameDblClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  IQMainFm.Hide;

  //if (cbProject.ItemIndex = -1) then
    NewProjectFm.Show
  //else
    //ProjectFm.Show;
end;

procedure TIQMainFm.Wink(viWinkCount: Integer);
var i: Integer;
begin
try
  Screen.Cursor := crHourGlass;
//  if ismni then
//  begin
    for i := 1 to viWinkCount do
     begin
       imgUpClick(Nil);
       if moveFailFlag then
       begin
         Exit;
       end;
       //Delay(1000);
       Delay(1500);
       btnStopClick(nil); //TODO
       //Delay(500);
       //Delay(1500);
       imgDnClick(nil);
       //Delay(1150);
       Delay(1500);
       btnStopClick(nil);
       //Delay(500);
       //Delay(1500);
     end;
//  end
//  else
//  begin
//
//  end;
finally
  Screen.Cursor := crDefault;
end;
end;

procedure TIQMainFm.WinkUpClick(Sender: TObject);
begin
try
  Wink(2);
finally
  imgWinkUp.Visible := True;
end;
end;
{
procedure TIQMainFm.WMNCActivate(var Msg: TWMNCActivate);
begin
  inherited;
  //DrawCaption;
end;

procedure TIQMainFm.WMPaint(var Msg: TWMNCPaint);
begin
  inherited;
  //DrawCaption;
end;
}
procedure TIQMainFm.SpeedButton3MouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

end.
