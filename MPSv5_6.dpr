﻿program MPSv5_6;

uses
  Windows,
  Messages,
  Dialogs,
  Vcl.Forms,
  Vcl.Themes,
  Vcl.Styles,
  MTRMainUn in 'MTRMainUn.pas' {IQMainFm},
  MTRConfigScreen in 'MTRConfigScreen.pas' {ConfigScreenFm},
  VariablesUn in 'VariablesUn.pas',
  DataUn in 'DataUn.pas' {dmDataModule: TDataModule},
  NewProjectUn in 'NewProjectUn.pas' {NewProjectFm},
  DiscoverUn in 'DiscoverUn.pas' {DiscoverFm},
  ComUn in 'ComUn.pas' {ComFm},
  ScreenSetupUn in 'ScreenSetupUn.pas' {ScreenSetupFm},
  AboutUn in 'AboutUn.pas' {AboutFm},
  SplashScreenUn in 'SplashScreenUn.pas' {SplashScreen},
  MWCConfigUn in 'MWCConfigUn.pas' {MWCConfigfm},
  EnOceanDialog in 'EnOceanDialog.pas' {EnOceanDialog1fm},
  MWCDisplayUn in 'MWCDisplayUn.pas' {MWCDisplayfm},
  MWCDefaultChooserUn in 'MWCDefaultChooserUn.pas' {MWCDefaultChooserfm},
  ASCIIUn in 'ASCIIUn.pas' {ASCIIFm},
  MNISwDialogUn in 'MNISwDialogUn.pas' {MNISwDialogFM},
  SwitchClassUn in 'SwitchClassUn.pas',
  MNI2ConfigUn in 'MNI2ConfigUn.pas' {MNI2ConfigFm},
  IQMLC2ConfigUn in 'IQMLC2ConfigUn.pas' {IQMLC2ConfigFm},
  MNIConfigUn in 'MNIConfigUn.pas' {MNIConfigFm},
  PowerPanelConfigUn in 'hold\PowerPanelConfigUn.pas' {PowerpanelConfigFm},
  LimitSettingUn in 'LimitSettingUn.pas' {LimitSettingFm},
  IQ3ConfigUn in 'IQ3ConfigUn.pas' {IQ3Configfm},
  IQ3RFDisplayUn in 'IQ3RFDisplayUn.pas' {IQ3RFDisplayfm},
  MotorMaintenanceDisplay in 'MotorMaintenanceDisplay.pas' {MotorMaintenanceDisplayFm},
  ODDisplayUn in 'ODDisplayUn.pas' {ODFm};

var
    Mutex :THandle;

{$R *.res}

begin
  Mutex := CreateMutex(nil, True, 'My_Unique_App_IQMainFm');
  if not (Mutex = 0) and not (GetLastError = ERROR_ALREADY_EXISTS) then
  begin
    Application.Initialize;
    Application.MainFormOnTaskbar := False;
    SplashScreen := TSplashScreen.Create(Nil);
    SplashScreen.Show;
    SplashScreen.Update;
    Application.CreateForm(TIQMainFm, IQMainFm);
  Application.CreateForm(TdmDataModule, dmDataModule);
  Application.CreateForm(TNewProjectFm, NewProjectFm);
  Application.CreateForm(TDiscoverFm, DiscoverFm);
  Application.CreateForm(TAboutFm, AboutFm);
  Application.CreateForm(TScreenSetupFm, ScreenSetupFm);
  Application.CreateForm(TConfigScreenFm, ConfigScreenFm);
  Application.CreateForm(TComFm, ComFm);
  Application.CreateForm(TMWCConfigfm, MWCConfigfm);
  Application.CreateForm(TMNISwDialogFM, MNISwDialogFM);
  Application.CreateForm(TMNI2ConfigFm, MNI2ConfigFm);
  Application.CreateForm(TEnOceanDialog1fm, EnOceanDialog1fm);
  Application.CreateForm(TMWCDisplayfm, MWCDisplayfm);
  Application.CreateForm(TMWCDefaultChooserfm, MWCDefaultChooserfm);
  Application.CreateForm(TASCIIFm, ASCIIFm);
  Application.CreateForm(TMNIConfigFm, MNIConfigFm);
  Application.CreateForm(TIQMLC2ConfigFm, IQMLC2ConfigFm);
  Application.CreateForm(TPowerpanelConfigFm, PowerpanelConfigFm);
  Application.CreateForm(TLimitSettingfm, LimitSettingfm);
  Application.CreateForm(TLimitSettingFm, LimitSettingFm);
  Application.CreateForm(TIQ3Configfm, IQ3Configfm);
  Application.CreateForm(TIQ3Configfm, IQ3Configfm);
  Application.CreateForm(TIQ3RFDisplayfm, IQ3RFDisplayfm);
  Application.CreateForm(TMotorMaintenanceDisplayFm, MotorMaintenanceDisplayFm);
  Application.CreateForm(TODFm, ODFm);
  SplashScreen.Hide;
    SplashScreen.Free;
    Application.Run;
    if Mutex <> 0 then CloseHandle(Mutex);
  end
  else
  begin
    MessageDlg('Program is already running!', mtWarning, [mbOk], 0);
    if Mutex <> 0 then CloseHandle(Mutex);
  end;
end.
