unit ComUn1;

interface

uses
  System.SysUtils, System.Classes, OoMisc, AdPort, AdExcept, AdSelCom, VariablesUn;

type
  T7ByteData = array[1..200] of String;
  TdmComModule = class(TDataModule)
    ComPort: TApdComPort;
    procedure ComPortPortOpen(Sender: TObject);
    procedure ComPortPortClose(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure ComPortTrigger(CP: TObject; Msg, TriggerHandle, Data: Word);
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    procedure RXDataProcess(RxData:String);
    function SevenByteConverter(StrRx:String): T7ByteData ;
  public
    { Public declarations }
    rh_PortOpend: TNotifyEvent;
    rh_PortClosed: TNotifyEvent;
    rh_PortOpend1: TNotifyEvent;
    rh_PortClosed1: TNotifyEvent;
    rh_PortOpend2: TNotifyEvent;
    rh_PortClosed2: TNotifyEvent;
    rh_PortOpendDisc: TNotifyEvent;
    rh_PortClosedDisc: TNotifyEvent;
    rh_ReturnHandler: TNotifyEvent;
    procedure SendCommand(vsCommand: String; vRH: TNotifyEvent);
  end;

var
  gsMessage: String;
  dmComModule: TdmComModule;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TdmComModule }

procedure TdmComModule.ComPortPortClose(Sender: TObject);
begin
  if Assigned (rh_PortClosed) then
     rh_PortClosed(Self);

  if Assigned (rh_PortClosed1) then
     rh_PortClosed1(Self);

  if Assigned (rh_PortClosed2) then
     rh_PortClosed2(Self);

  if Assigned (rh_PortClosedDisc) then
     rh_PortClosedDisc(Self);
end;

procedure TdmComModule.ComPortPortOpen(Sender: TObject);
begin
  if Assigned (rh_PortOpend) then
     rh_PortOpend(Self);

  if Assigned (rh_PortOpend1) then
     rh_PortOpend1(Self);

  if Assigned (rh_PortOpend2) then
     rh_PortOpend2(Self);

  if Assigned (rh_PortOpendDisc) then
     rh_PortOpendDisc(Self);
end;

procedure TdmComModule.ComPortTrigger(CP: TObject; Msg, TriggerHandle,
  Data: Word);
  var vsChr: AnsiChar;
begin
  try
    While ( ComPort.InBuffUsed > 0) do
    begin
      vsChr := ComPort.GetChar;

      gsMessage := gsMessage + vsChr;

      if (vsChr in [#32..#127,#13]) then
      begin
        if ((vsChr='>')or(vsChr='?')or(vsChr='+')or(vsChr='*')or (vsChr='#') or(vsChr='!')) then          // check for start character
             gsMessage := vsChr;
      end;

      if vsChr =#13 then
      begin
        RXDataProcess(gsMessage);
        gsMessage := '';
      end;

    end;
  except
    gsMessage := '';
  End;
end;

procedure TdmComModule.DataModuleCreate(Sender: TObject);
begin
  gsMessage := '';
end;

procedure TdmComModule.DataModuleDestroy(Sender: TObject);
begin
  rh_PortOpend := nil;
  rh_PortClosed := nil;
  rh_PortOpend1 := nil;
  rh_PortClosed1 := nil;
  rh_PortOpend2 := nil;
  rh_PortClosed2 := nil;
  rh_PortOpendDisc := nil;
  rh_PortClosedDisc := nil;
  rh_ReturnHandler := nil;
end;

procedure TdmComModule.RXDataProcess(RxData: String);
var
 SevenByteProtocol : T7ByteData;
 //ZGN_Data: TMixedData;
 CMD,i,DataH,DataM,DataL,ps : Integer;
 sUID,sUIDH,sUIDL : String;
 UID_NoRx : String;

begin
  Try
   if ((RxData[1] = '>')or(RxData[1] = '?')or(RxData[1] = '+')or(RxData[1] = '*')) then
    begin
      {Check for ruppt data}
      for ps := 2 to length(RxData) do
       if not((RxData[ps]in['A'..'Z'])or(RxData[ps]in['0'..'9'])
        or (RxData[ps]='.')or(RxData[length(RxData)]=#13)) then
          exit;

      SevenByteProtocol:= SevenByteConverter(RxData);

      if SevenByteProtocol[2]='' then exit;
      if ((VariablesUn.IsNumber(SevenByteProtocol[2])= True)and(VariablesUn.IsNumber(SevenByteProtocol[4])= True)) then
       VariablesUn.giUID_rx := (StrToInt(SevenByteProtocol[2])*256)+ StrToInt(SevenByteProtocol[4]);     // received UID #
      if IsNumber(SevenByteProtocol[6])= True then
            CMD := StrToInt(SevenByteProtocol[6]);                                             // received command
      if CMD <> 85 then
       begin
        if ((IsNumber(SevenByteProtocol[8])= True)and(IsNumber(SevenByteProtocol[10])= True)
            and(IsNumber(SevenByteProtocol[12])= True)) then
         begin
          DataH := StrToInt(SevenByteProtocol[8]);                                             // High byte
          DataM := StrToInt(SevenByteProtocol[10]);                                             // Middle byte
          DataL := StrToInt(SevenByteProtocol[12]);

          VariablesUn.giShadePosition := DataL;                                             // Low byte
         end;
       end;

      if CMD = 85 then
      begin
        //tempAlias := Copy(RxData,AnsiPos('.85.',RxData)+4,12);
      end;

      if ((SevenByteProtocol[1]='*') or (SevenByteProtocol[1]='+')) then
        begin

         //UID_Sorting(IntToStr(UID_rx));
         //IQFind := True;                                                         // Correct Message received
          case CMD of
           //*******************************************************************
           //           Motor position
           //*******************************************************************
           {
            19  :    case DataH of
                       0,1 : begin
                            M1Progress.Progress := 255-DataL;
                            M1Gauge.Progress :=  DataL;
                            frmVitualSW01.M1Progress.Progress := 255-DataL;
                            frmVitualSW01.M1Gauge.Progress :=  DataL;
                           end;
                       2 : begin
                            M2Progress.Progress := 255-DataL;
                            M2Gauge.Progress :=  DataL;
                            frmVitualSW01.M2Progress.Progress := 255-DataL;
                            frmVitualSW01.M2Gauge.Progress :=  DataL;
                           end;
                       3 : begin
                            M3Progress.Progress := 255-DataL;
                            M3Gauge.Progress :=  DataL;
                            frmVitualSW01.M3Progress.Progress := 255-DataL;
                            frmVitualSW01.M3Gauge.Progress :=  DataL;

                           end;
                       4 : begin
                            M4Progress.Progress := 255-DataL;
                            M4Gauge.Progress :=  DataL;
                            frmVitualSW01.M4Progress.Progress := 255-DataL;
                            frmVitualSW01.M4Gauge.Progress :=  DataL;

                           end;
                         end;
            }
            //*******************************************************************
           //           Network UID search/ add UID into the list
           //*******************************************************************
            21:
                       begin
                        if Assigned (rh_ReturnHandler) then
                          rh_ReturnHandler(Self);
                       end;
            1,200 :    begin
                        if Assigned (rh_ReturnHandler) then
                          rh_ReturnHandler(Self);

                       {Add all the devices found}
                       //if ListOfDevices.Items.IndexOf(IntToStr(UID_rx)) = -1 then
                        //begin
                          //ListOfDevices.Items.Add(IntToStr(UID_rx));
                          //listofTypeOfDevices.Items.Add(IntToStr(DataL));
                        //end;

                      end;

            //==================================================================
              end;
        end;
    end;

    if (RxData[1] = '!') then
    begin
          {Check for corruppt data}
      for ps := 2 to length(RxData) do
       if not((RxData[ps]in['A'..'Z'])or(RxData[ps]in['0'..'9'])
        or (RxData[ps]='.')or(RxData[length(RxData)]=#13)) then
          exit;
      {
      ZGN_Data  := StringSeparator(RxData);

      //Check Received position
      if ((ZGN_Data[2]='!') and (ZGN_Data[10]='=')) then
      begin
          UID_NoRx := RxData;
          delete(UID_NoRx,1,AnsiPos('(',UID_NoRx));
          sUIDH := copy(UID_NoRx,1,AnsiPos('.',UID_NoRx)-1);
          delete(UID_NoRx,1,AnsiPos('.',UID_NoRx));
          sUIDL := copy(UID_NoRx,1,AnsiPos(')',UID_NoRx)-1);
          if ((IsNumber(sUIDH)= True) and (IsNumber(sUIDL)= True)) then
          begin
              UID_rx := (StrToInt(sUIDH)*256)+ StrToInt(sUIDL);     // received UID #
              UID_Sorting(IntToStr(UID_rx));
          end;
      end;
      }
    end;
  except
    rh_ReturnHandler := Nil;
  end;
end;

procedure TdmComModule.SendCommand(vsCommand: String; vRH: TNotifyEvent);
begin
  try
    if ComPort.Open then
    begin
      ComPort.FlushInBuffer;
      ComPort.FlushOutBuffer;
      rh_ReturnHandler := Nil;
      rh_ReturnHandler := vRH;
      ComPort.PutString(vsCommand + #13);
      VariablesUn.Delay(10);
    end;
  except
     rh_ReturnHandler := Nil;
  End;
end;

function TdmComModule.SevenByteConverter(StrRx:String):T7ByteData;              {7 byte protocol convertor}
var
 ps,ArrayPos : integer;
 cs: String;
begin
  Try
    if StrRx ='' then exit;
    if not ((StrRx[1] = '>')or(StrRx[1] = '?')or(StrRx[1] = '+')or(StrRx[1] = '*'))
      then Exit;

    for ps := 2 to length(StrRx) do
     if not((StrRx[ps]in['A'..'Z']) or (StrRx[ps]in['0'..'9'])
         or (StrRx[ps]='.')or (StrRx[length(StrRx)]=#13)) then   exit;

    ArrayPos := 1;
    SevenByteConverter[ArrayPos]:= StrRx[1];
    cs := '';
    for ps:=2 to Length(StrRx) do
     begin
      if not((StrRx[ps]in['A'..'Z']) or (StrRx[ps]in['0'..'9'])) then
       begin
        inc(ArrayPos);
        SevenByteConverter[ArrayPos]:= cs;
        inc(ArrayPos);
        SevenByteConverter[ArrayPos]:= StrRx[ps];
        cs := '';
       end
      else
       cs := cs + StrRx[ps];
     end;
   except
      SevenByteConverter[2]:= '';
   end;
end;

end.
