unit IQ3RFDisplayUn;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Imaging.pngimage, Vcl.ExtCtrls,VariablesUn,ComUn,NewProjectUn,
  Vcl.StdCtrls,DataUn,Data.DB,Datasnap.DBClient,OtlParallel,math,MWCConfigun,AboutUn,MNIConfigUn,ScreenSetupUn,
  Vcl.Imaging.jpeg, JvComponentBase, JvCaptionButton, PowerPanelConfigUn,MotorMaintenanceDisplay;

type
  TIQ3RFDisplayfm = class(TForm)
    btnMenuIQ31: TImage;
    pnlMenuIQ3: TPanel;
    cmbUidIQ3: TComboBox;
    cmbChannel: TComboBox;
    imgShadeDown: TImage;
    lblLuxTh1Down: TLabel;
    lblLuxTh2Down: TLabel;
    lblLuxTh3Down: TLabel;
    lblLuxTh4Down: TLabel;
    cmbProjectIQ3: TComboBox;
    Label12: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    cmbMode: TComboBox;
    teLuxTh1: TEdit;
    teLuxTh2: TEdit;
    teLuxTh3: TEdit;
    teLuxTh4: TEdit;
    cbUp: TCheckBox;
    cb1: TCheckBox;
    cb2: TCheckBox;
    cb3: TCheckBox;
    cbDown: TCheckBox;
    Label10: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    teHysteresis: TEdit;
    Label18: TLabel;
    Label19: TLabel;
    teChZGN: TEdit;
    Label20: TLabel;
    Label21: TLabel;
    Image1: TImage;
    Image2: TImage;
    lbTelegramCounter: TLabel;
    Label1: TLabel;
    Shape6: TShape;
    Shape7: TShape;
    Shape8: TShape;
    Shape9: TShape;
    Label8: TLabel;
    Shape10: TShape;
    Shape11: TShape;
    Shape12: TShape;
    Shape13: TShape;
    shpIQ3DisplayBackground: TShape;
    imgShadePs3: TImage;
    imgShadeps2: TImage;
    imgShadePs1: TImage;
    imgShadeUP: TImage;
    lblShadePos: TLabel;
    lblSwitch: TLabel;
    shpUp: TShape;
    shpPs1: TShape;
    shpPS2: TShape;
    shpDn: TShape;
    shpPs3: TShape;
    btUpdate: TPanel;
    pnlLiveData: TPanel;
    Label2: TLabel;
    lbIntensity: TLabel;
    Label6: TLabel;
    Label3: TLabel;
    lbTemperature: TLabel;
    Label7: TLabel;
    imgMoon: TImage;
    imgSun: TImage;
    lblOccStatus: TLabel;
    lblOccStatusData: TLabel;
    pnlRefresh: TPanel;
    pnlPair: TPanel;
    cbPaired: TCheckBox;
    teAlias: TEdit;
    btnMenu: TImage;
    btnFind: TImage;
    btnInfo: TImage;
    btnSettings: TImage;
    btnExit: TImage;
    btnConfig: TImage;
    btnControl: TImage;
    JvCaptionButton1: TJvCaptionButton;
    btnLimitSetting: TImage;
    cmbDeviceSelector: TComboBox;
    procedure btnMainMCWClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure rhPortOpend(Sender: TObject);
    procedure rhPortClosed(Sender: TObject);
    procedure btnMCWMouseEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure pnlMenuIQ3MouseLeave(Sender: TObject);
    procedure messageLoop();
    Procedure ShowUID();
    Procedure rhIQ3DisplayStatusRecieved(sender: TObject);
    procedure imgSetupClick(Sender: TObject);
    procedure rhShowIQ3RFDisplay(Sender: TObject);
    procedure cmbDeviceSelectorChange(Sender: TObject);
    procedure btnFindClick(Sender: TObject);
    procedure rhShowDisplayIQ3(Sender: TObject);
    procedure btnInfoClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure cmbChannelChange(Sender: TObject);
    procedure cmbUidIQ3Change(Sender: TObject);
    procedure cmbUidIQ3KeyPress(Sender: TObject; var Key: Char);
    procedure cmbProjectIQ3Change(Sender: TObject);
    procedure SelectProject;
    procedure ConvertShadePosToCB(LoopString:String);
    procedure btUpdateClick(Sender: TObject);
    procedure rhIQ3DisplayMSGReceived(Sender: TObject);
    Function ConvertShadePosToBin():String;
    procedure cb1Click(Sender: TObject);
    procedure cb2Click(Sender: TObject);
    procedure cb3Click(Sender: TObject);
    procedure cbDownClick(Sender: TObject);
    procedure cbUpClick(Sender: TObject);
    procedure teLuxTh1Exit(Sender: TObject);
    procedure teLuxTh2Exit(Sender: TObject);
    procedure teLuxTh3Exit(Sender: TObject);
    procedure teLuxTh4Exit(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure cbUpMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure cb1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure cb2MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure cb3MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure cbDownMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btUpdateMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btUpdateMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btnSettingsClick(Sender: TObject);
    procedure pnlRefreshClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure teAliasChange(Sender: TObject);
    procedure btnFindMouseEnter(Sender: TObject);
    procedure btnInfoMouseEnter(Sender: TObject);
    procedure btnSettingsMouseEnter(Sender: TObject);
    procedure btnExitMouseEnter(Sender: TObject);
    procedure txtAliasEnterKey(Sender: TObject);
    procedure btnConfigClick(Sender: TObject);
    procedure btnControlMouseEnter(Sender: TObject);
    procedure btnConfigMouseEnter(Sender: TObject);
    procedure teAliasDblClick(Sender: TObject);
    procedure teAliasKeyPress(Sender: TObject; var Key: Char);
    procedure JvCaptionButton1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnLimitSettingClick(Sender: TObject);
    procedure btnLimitSettingMouseEnter(Sender: TObject);
  private
    { Private declarations }
    viUIDs : Array of Integer;
    stProgrammingFlg:Boolean;
    PassFlag:Boolean;
    giUID:Integer;
    procedure ComboBox_AutoWidth(const theComboBox: TCombobox);
    procedure disableEnableButtons(Enableflg:boolean);
    Procedure SortStringListUIDS(UIDList:TStringList);
  public
    rh_ShowMain: TNotifyEvent;
    rh_ShowIQ3: TNotifyEvent;
    rh_ShowMWC: TNotifyEvent;
    rh_showIQMLC2Config: TNotifyEvent;
    rh_ShowMNI2Config: TnotifyEvent;
    rh_GetMNI2VirtualMotorStatus:TNotifyevent

  end;

var

  IQ3RFDisplayfm: TIQ3RFDisplayfm;
  viSentCom:Extended;
  viUID,PauseCount,PauseCountLoop:Integer;

  ChToUpdate:string;
  PassLoopTimeOutFlag:Boolean;
  //PassLoopFlag:boolean;
  EnableShadePos:String;
Const
  LoopTimer:Integer=9500;

implementation

{$R *.dfm}

uses DiscoverUn, LimitSettingUn, MWCDisplayUn, IQ3ConfigUn;

Procedure TIQ3RFDisplayfm.SortStringListUIDS(UIDList:TStringList);
var
  I:Integer;
  First,Second:Integer;
  Swap:Boolean;
  TempStr:String;
begin
  Swap:=true;
  while Swap do
  begin
    Swap:=False;
    for I := 0 to UIDList.Count-2 do
    begin
      First:=StrToInt(UIDList[I]);
      Second:=StrToInt(UIDList[I+1]);
      if First > Second then
      begin
        TempStr:=UIDList[I];
        UIDList[I]:=UIDList[I+1];
        UIDList[I+1]:=TempStr;
        Swap:=true;
      end;
    end;
  end;
end;

procedure TIQ3RFDisplayfm.ShowUID();
var
  I:Integer;
  comboUIDList:TStringList;
begin
  comboUIDList:=TStringList.Create;
  cmbUidIQ3.Clear;
  try
  begin
    VariablesUn.gsChannelNum:=IntToStr(cmbChannel.ItemIndex);
    if not (DataUn.dmDataModule.dsIQ3s.State = dsInactive) then
    begin
      DataUn.dmDataModule.dsIQ3s.Filtered := False;
      DataUn.dmDataModule.dsIQ3s.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
      DataUn.dmDataModule.dsIQ3s.Filtered := True;
    end;
//    if(DataUn.dmDataModule.dsIQ3s.RecordCount=0)then
//    begin
//      ShowMessage('No MWCs Found After Scan. Returning to MTR page.');
//      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('MTR');
//      cmbDeviceSelectorChange(nil);
//      result:=false;
//      exit;
//    end;
//    result:=true;
    Setlength(viUIDs,0);
    while not dmDataModule.dsIQ3s.Eof do
    begin
     //cmbUidIQ3.Items.Add(IntToStr(DataUn.dmDataModule.dsIQ3s.FieldByName('UID').AsInteger));
     comboUIDList.Add(IntToStr(DataUn.dmDataModule.dsIQ3s.FieldByName('UID').AsInteger));
     SetLength(viUIDs,Length(viUIDs)+1);
     viUIDs[Length(viUIDs)-1] := DataUn.dmDataModule.dsIQ3s.FieldByName('UID').AsInteger;
     dmDataModule.dsIQ3s.Next;
    end;
    SortStringListUIDS(comboUIDList);
    cmbUidIQ3.Items.Assign(comboUIDList);

    cmbUidIQ3.ItemIndex:=0;
    DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
    DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + IQ3RFDisplayfm.cmbUidIQ3.text + ' and ChannelID = ' + cmbChannel.Text;
    DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
    if not(DataUn.dmDataModule.dsIQ3Channels.EOF)then
    begin
      teLuxTh1.text := DataUn.dmDataModule.dsIQ3Channels.FieldByName('Threshold1').AsString;
      teLuxTh2.text := DataUn.dmDataModule.dsIQ3Channels.FieldByName('Threshold2').AsString;
      teLuxTh3.text := DataUn.dmDataModule.dsIQ3Channels.FieldByName('Threshold3').AsString;
      teLuxTh4.text := DataUn.dmDataModule.dsIQ3Channels.FieldByName('Threshold4').AsString;
      teHysteresis.Text:= Dataun.dmDataModule.dsIQ3Channels.FieldByName('HysteresisPercent').asstring;
      lblLuxTh1down.Caption := FloatToStr(((100-StrToFloat(teHysteresis.Text))/100)*StrToFloat(DataUn.dmDataModule.dsIQ3Channels.FieldByName('Threshold1').AsString));
      lblLuxTh2down.Caption := FloatToStr(((100-StrToFloat(teHysteresis.Text))/100)*StrToFloat(DataUn.dmDataModule.dsIQ3Channels.FieldByName('Threshold2').AsString));
      lblLuxTh3down.Caption := FloatToStr(((100-StrToFloat(teHysteresis.Text))/100)*StrToFloat(DataUn.dmDataModule.dsIQ3Channels.FieldByName('Threshold3').AsString));
      lblLuxTh4down.Caption := FloatToStr(((100-StrToFloat(teHysteresis.Text))/100)*StrToFloat(DataUn.dmDataModule.dsIQ3Channels.FieldByName('Threshold4').AsString));
      teChZGN.Text := IQ3Configfm.ConvertZGN(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ZGN').asstring);

      cbUP.Checked:=true;

    end;
    ConvertShadePosToCB(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ShadePosition').asstring);
    //Changes screen for ST/SAC modes
    DataUn.dmDataModule.dsIQ3s.Filtered := False;
    DataUn.dmDataModule.dsIQ3s.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + IQ3RFDisplayfm.cmbUidIQ3.text;
    DataUn.dmDataModule.dsIQ3s.Filtered := True;
    end;
    if(DataUn.dmDataModule.dsIQ3s.FieldByName('EnableMessages').AsString='0')or(DataUn.dmDataModule.dsIQ3Channels.FieldByName('DeviceID').AsString='2')then
    begin
      imgShadedown.Visible:=false;
      imgShadeps1.Visible:=false;
      imgShadeps2.Visible:=false;
      imgShadeps3.Visible:=false;
      imgShadeup.Visible:=false;
      image1.Visible:=false;
      image2.Visible:=false;
      cbup.Visible:=false;
      cb1.Visible:=False;
      cb2.Visible:=false;
      cb3.Visible:=false;
      cbdown.Visible:=false;
      label10.Visible:=false;
      label14.Visible:=false;
      label15.Visible:=false;
      label16.Visible:=false;
      label17.Visible:=false;
      label18.Visible:=false;
      label19.Visible:=false;
      label21.Visible:=false;
      tehysteresis.Visible:=false;
      teluxTH1.Visible:=false;
      teluxTh2.Visible:=false;
      teluxTh3.Visible:=false;
      teluxTh4.Visible:=false;
      lblluxTH1down.Visible:=false;
      lblluxTh2down.Visible:=false;
      lblluxTh3down.Visible:=false;
      lblluxTh4down.Visible:=false;
      btUpdate.Visible:=false;

      if(DataUn.dmDataModule.dsIQ3Channels.FieldByName('DeviceID').AsString='2')then
      begin
        lblSwitch.visible:=true;
      end
      else
      begin
        cmbMode.ItemIndex:=1;
      end;
    end
    else
    begin
      imgShadedown.Visible:=true;
      imgShadeps1.Visible:=true;
      imgShadeps2.Visible:=true;
      imgShadeps3.Visible:=true;
      imgShadeup.Visible:=true;
      image1.Visible:=true;
      image2.Visible:=true;
      cbup.Visible:=true;
      cb1.Visible:=true;
      cb2.Visible:=true;
      cb3.Visible:=true;
      cbdown.Visible:=true;
      label10.Visible:=true;
      label14.Visible:=true;
      label15.Visible:=true;
      label16.Visible:=true;
      label17.Visible:=true;
      label18.Visible:=true;
      label19.Visible:=true;
      label21.Visible:=true;
      tehysteresis.Visible:=true;
      teluxTH1.Visible:=true;
      teluxTh2.Visible:=true;
      teluxTh3.Visible:=true;
      teluxTh4.Visible:=true;
      lblluxTH1down.Visible:=true;
      lblluxTh2down.Visible:=true;
      lblluxTh3down.Visible:=true;
      lblluxTh4down.Visible:=true;
      btUpdate.Visible:=true;
      cmbMode.ItemIndex:=0;
      lblSwitch.visible:=false;

  end;
  finally

  end;
end;

procedure TIQ3RFDisplayfm.teAliasChange(Sender: TObject);
//var
//  vsAlias,vscommand:String;
begin
//  try
//    giUID :=  StrToInt(cmbUIDMWC.text);
//    vsAlias:=teAlias.Text;
//    vsAlias := vsAlias + StringOfChar ( Char(#0), 12 - Length(vsAlias) );
//    vsCommand:=VariablesUn.UID_NAME+'.'+vsAlias;
//    ComFm.SetGenericParam(giUID,vsCommand, nil);
//    Delay(200);
//
//    DataUn.dmDataModule.dsIQ3s.Filtered := False;
//    DataUn.dmDataModule.dsIQ3s.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidIQ3.text;
//    DataUn.dmDataModule.dsIQ3s.Filtered := True;
//    DataUn.dmDataModule.dsIQ3s.edit;
//    DataUn.dmDataModule.dsIQ3s.FieldByName('Alias').AsString:=teAlias.Text;
//    DataUn.dmDataModule.dsIQ3s.MergeChangeLog;
//
//
//  except
//    on E: Exception do
//    begin
//      MessageDlg('TConfigScreenFm.lblAliasChange: ' + E.Message, mtWarning, [mbOk], 0);
//    end;
//  End;
end;

procedure TIQ3RFDisplayfm.teAliasDblClick(Sender: TObject);
begin
  NewProjectFm.rh_ShowLast:=rhShowDisplayIQ3;
  NewProjectFm.Show;
  Hide;
end;

procedure TIQ3RFDisplayfm.teAliasKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #$D then
  begin
    txtAliasEnterKey(self);
  end;
end;

procedure TIQ3RFDisplayfm.txtAliasEnterKey(Sender: TObject);
var
  vsAlias,vscommand:String;
begin
  try
    giUID :=  StrToInt(cmbUidIQ3.text);
    vsAlias:=teAlias.Text;
    vsAlias := vsAlias + StringOfChar ( Char(#0), 12 - Length(vsAlias) );
    vsCommand:=VariablesUn.UID_NAME+'.'+vsAlias;
    ComFm.SetGenericParam(giUID,vsCommand, nil);
    Delay(200);

    DataUn.dmDataModule.dsIQ3s.Filtered := False;
    DataUn.dmDataModule.dsIQ3s.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidIQ3.text;
    DataUn.dmDataModule.dsIQ3s.Filtered := True;
    DataUn.dmDataModule.dsIQ3s.edit;
    DataUn.dmDataModule.dsIQ3s.FieldByName('Alias').AsString:=teAlias.Text;
    DataUn.dmDataModule.dsIQ3s.MergeChangeLog;


  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.teAliasExit: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  End;

end;

procedure TIQ3RFDisplayfm.teLuxTh1Exit(Sender: TObject);
begin
  if (StrToInt(teLuxTh1.text)>=StrToInt(teLuxTh2.text)) or (StrToInt(teLuxTh1.text)>=StrToInt(teLuxTh3.text)) or (StrToInt(teLuxTh1.text)>=StrToInt(teLuxTh4.text))then
  begin
    showMessage('Threshold1 must be the lowest value!');
    teLuxTh1.SetFocus;

  end;
end;

procedure TIQ3RFDisplayfm.teLuxTh2Exit(Sender: TObject);
begin
  if (StrToInt(teLuxTh2.text)>=StrToInt(teLuxTh3.text)) or (StrToInt(teLuxTh2.text)>=StrToInt(teLuxTh4.text))then
  begin
    showMessage('Threshold2 must be lower than Th3 and Th4!');
    teLuxTh2.SetFocus;
  end
  else if (StrToInt(teLuxTh2.text)<=StrToInt(teLuxTh1.text))then
  begin
    showMessage('Threshold2 must be higher than Th1!');
    teLuxTh2.SetFocus;
  end;
end;

procedure TIQ3RFDisplayfm.teLuxTh3Exit(Sender: TObject);
begin
  if(StrToInt(teLuxTh3.text)>=StrToInt(teLuxTh4.text))then
  begin
    showMessage('Threshold3 must be lower than Th4!');
    teLuxTh3.SetFocus;
  end
  else if (StrToInt(teLuxTh3.text)<=StrToInt(teLuxTh1.text))or(StrToInt(teLuxTh3.text)<=StrToInt(teLuxTh2.text)) then
  begin
    showMessage('Threshold3 must be higher than Th1 and Th2!');
    teLuxTh2.SetFocus;
  end;
end;

procedure TIQ3RFDisplayfm.teLuxTh4Exit(Sender: TObject);
begin
  if (StrToInt(teLuxTh1.text)>=StrToInt(teLuxTh4.text)) or (StrToInt(teLuxTh4.text)<=StrToInt(teLuxTh3.text)) or (StrToInt(teLuxTh4.text)<=StrToInt(teLuxTh2.text))then
  begin
    showMessage('Threshold4 must be the Highest value!');
    teLuxTh4.SetFocus;
  end;
end;

procedure TIQ3RFDisplayfm.MessageLoop();
var
  loopcounter:Integer;
  ChannelMath:string;
begin
  try
    gipauseLoopExit:=false;
    giloopExit:=false;
    while not variablesUn.PassLoopFlag do
    begin
        if cmbUidIQ3.Text<>'' then
        begin
        DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
        DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidIQ3.text + ' and ChannelID = ' + IntToStr(StrToInt(gsChannelNum)+1) ;
        DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
        loopcounter:=0;
        if(cmbUidIQ3.text<>'')then
        begin
          PassLoopTimeOutFlag:=false;
          PauseCountLoop:=0;
          viUID := StrToInt(cmbUidIQ3.text);
          viSentCom:=StrToFloat(VariablesUn.MWC_LiveData)*1000;
          ComFm.GetMWCDisplayStatus(StrToInt(cmbUidIQ3.text),VariablesUn.MWC_LiveData,VariablesUn.gsChannelNum,rhIQ3DisplayStatusRecieved);
          while not PassLoopTimeOutFlag do
          begin
            PauseCountLoop:=PauseCountLoop+1;
            VariablesUn.delay(10);
            if PauseCountLoop=100 then
              ComFm.GetMWCDisplayStatus(StrToInt(cmbUidIQ3.text),(VariablesUn.MWC_LiveData),IntToStr(StrToInt(cmbChannel.Text)-1),rhIQ3DisplayStatusRecieved);
            if PauseCountLoop>300 then
            begin
              Break;
            end;
          end;
          PassLoopTimeOutFlag:=False;
          PauseCountLoop:=0;
          viUID := StrToInt(cmbUidIQ3.text);
          viSentCom:=StrToFloat(VariablesUn.MWC_DayOrNight)*1000;
          ComFm.GetMWCDisplayStatus(StrToInt(cmbUidIQ3.text),MWC_DayOrNight,VariablesUn.gsChannelNum,rhIQ3DisplayStatusRecieved);
          while not PassLoopTimeOutFlag do
          begin
            PauseCountLoop:=PauseCountLoop+1;
            VariablesUn.delay(10);
            if PauseCountLoop=100 then
              ComFm.GetMWCDisplayStatus(StrToInt(cmbUidIQ3.text),MWC_DayOrNight,IntToStr(StrToInt(cmbChannel.Text)-1),rhIQ3DisplayStatusRecieved);
            if PauseCountLoop>300 then
            begin
              Break;
            end;
          end;
          PassLoopTimeOutFlag:=False;
          PauseCountLoop:=0;

        end;
        while LoopTimer>loopcounter do
        begin
          VariablesUn.Delay(10);
          loopCounter:=loopcounter+10;
          if gipauseLoopExit then
          begin
            exit;
          end;
        end;
      end;
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TIQ3RFDisplayfm.MessageLoop: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

Procedure TIQ3RFDisplayfm.rhIQ3DisplayStatusRecieved(sender: TObject);
var
  ShadePosBin:String;
begin
  DataUn.dmDataModule.dsIQ3s.Filtered := False;
  DataUn.dmDataModule.dsIQ3s.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + IQ3RFDisplayfm.cmbUidIQ3.text;
  DataUn.dmDataModule.dsIQ3s.Filtered := True;

  DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
  DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + IQ3RFDisplayfm.cmbUidIQ3.text + ' and ChannelID = ' + IntToStr(StrToInt(gsChannelNum)+1) ;
  DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
  if(viSentCom = (StrToFloat(IntToStr(VariablesUn.giCommand)+'.'+IntToStr(VariablesUn.giCommandHi)))*1000) and (viUID = VariablesUn.giUID_rx) then
  begin
    PassLoopTimeOutFlag:=True;
    if(VariablesUn.giCommandHi=30)then
    begin
      ShadePosBin:=IntToBin(VariablesUn.giDataM);
      if(ShadePosBin[1]='1')then
      begin
        imgsun.Visible:=false;
        imgMoon.Visible:=true;
      end
      else
      begin
        imgMoon.Visible:=false;
        imgsun.Visible:=true;
      end;
      if(VariablesUn.giDataL=1)and (DataUn.dmDataModule.dsIQ3s.FieldByName('EnableMessages').AsString='1')then
      begin
        imgShadedown.Visible:=false;
        imgShadeps1.Visible:=false;
        imgShadeps2.Visible:=false;
        imgShadeps3.Visible:=false;
        imgShadeup.Visible:=true;
        lblShadePos.Caption:='UP';
      end
      else if(VariablesUn.giDataL=3)and (DataUn.dmDataModule.dsIQ3s.FieldByName('EnableMessages').AsString='1')then
      begin
        imgShadedown.Visible:=false;
        imgShadeps1.Visible:=true;
        imgShadeps2.Visible:=false;
        imgShadeps3.Visible:=false;
        imgShadeup.Visible:=false;
        lblShadePos.Caption:='P1';
      end
      else if(VariablesUn.giDataL=2)and (DataUn.dmDataModule.dsIQ3s.FieldByName('EnableMessages').AsString='1')then
      begin
        imgShadedown.Visible:=false;
        imgShadeps1.Visible:=false;
        imgShadeps2.Visible:=true;
        imgShadeps3.Visible:=false;
        imgShadeup.Visible:=false;
        lblShadePos.Caption:='P2';
      end
      else if(VariablesUn.giDataL=6)and (DataUn.dmDataModule.dsIQ3s.FieldByName('EnableMessages').AsString='1')then
      begin
        imgShadedown.Visible:=false;
        imgShadeps1.Visible:=false;
        imgShadeps2.Visible:=false;
        imgShadeps3.Visible:=true;
        imgShadeup.Visible:=false;
        lblShadePos.Caption:='P3';
      end
      else if(VariablesUn.giDataL=4)and (DataUn.dmDataModule.dsIQ3s.FieldByName('EnableMessages').AsString='1')then
      begin
        imgShadedown.Visible:=true;
        imgShadeps1.Visible:=false;
        imgShadeps2.Visible:=false;
        imgShadeps3.Visible:=false;
        imgShadeup.Visible:=false;
        lblShadePos.Caption:='DN';
      end;
    end
    else if(VariablesUn.giCommandHi=25)then
    begin

      if(VariablesUn.giTelegramCounter<>0)then
      begin
        IQ3RFDisplayfm.lbTelegramCounter.Caption:=IntToStr(VariablesUn.giTelegramCounter);
      end
      else
      begin
        IQ3RFDisplayfm.lbTelegramCounter.Caption:='-';
      end;
      if(VariablesUn.giIntensity<>0)then
      begin
        IQ3RFDisplayfm.lbIntensity.Caption:=IntToStr(VariablesUn.giIntensity);
      end
      else
      begin
        IQ3RFDisplayfm.lbIntensity.Caption:='-';
      end;
      if(VariablesUn.giTemperature<>-4)then
      begin
        IQ3RFDisplayfm.lbTemperature.Caption:=FloatToStr(VariablesUn.giTemperature);
      end
      else
      begin
        IQ3RFDisplayfm.lbTemperature.Caption:='-'
      end;
      if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring='1')then
      begin
        if(VariablesUn.giCapVoltage>2.5)and(VariablesUn.giCapVoltage<3)then
        begin
          shape6.Brush.Color:=cllime;
          shape7.Brush.Color:=clWhite;
          shape8.Brush.Color:=clWhite;
          shape9.Brush.Color:=clWhite;
        end
        else if(VariablesUn.giCapVoltage>=3)and(VariablesUn.giCapVoltage<3.5)then
        begin
          shape6.Brush.Color:=cllime;
          shape7.Brush.Color:=cllime;
          shape8.Brush.Color:=clWhite;
          shape9.Brush.Color:=clWhite;
        end
        else if(VariablesUn.giCapVoltage>=3.5)and(VariablesUn.giCapVoltage<4)then
        begin
          shape6.Brush.Color:=cllime;
          shape7.Brush.Color:=cllime;
          shape8.Brush.Color:=cllime;
          shape9.Brush.Color:=clWhite;
        end
        else if(VariablesUn.giCapVoltage>=4)then
        begin
          shape6.Brush.Color:=cllime;
          shape7.Brush.Color:=cllime;
          shape8.Brush.Color:=cllime;
          shape9.Brush.Color:=cllime;
        end
        else if(VariablesUn.giCapVoltage=2.5)then
        begin
          shape6.Brush.Color:=clred;
          shape7.Brush.Color:=clred;
          shape8.Brush.Color:=clred;
          shape9.Brush.Color:=clred;
        end;
      end
      else if Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring='3' then
      begin
        if(VariablesUn.gsOccStatus='1')then
        begin
          lblOccStatusData.Caption:='Occupied';
        end
        else
        begin
          lblOccStatusData.Caption:='Unoccupied';
        end;

        if(VariablesUn.giOccCapVoltage>5)and(VariablesUn.giOccCapVoltage<25)then
        begin
          shape6.Brush.Color:=cllime;
          shape7.Brush.Color:=clWhite;
          shape8.Brush.Color:=clWhite;
          shape9.Brush.Color:=clWhite;
        end
        else if(VariablesUn.giOccCapVoltage>=25)and(VariablesUn.giOccCapVoltage<50)then
        begin
          shape6.Brush.Color:=cllime;
          shape7.Brush.Color:=cllime;
          shape8.Brush.Color:=clWhite;
          shape9.Brush.Color:=clWhite;
        end
        else if(VariablesUn.giOccCapVoltage>=50)and(VariablesUn.giOccCapVoltage<75)then
        begin
          shape6.Brush.Color:=cllime;
          shape7.Brush.Color:=cllime;
          shape8.Brush.Color:=cllime;
          shape9.Brush.Color:=clWhite;
        end
        else if(VariablesUn.giOccCapVoltage>=75)then
        begin
          shape6.Brush.Color:=cllime;
          shape7.Brush.Color:=cllime;
          shape8.Brush.Color:=cllime;
          shape9.Brush.Color:=cllime;
        end
        else if(VariablesUn.giOccCapVoltage<5)then
        begin
          shape6.Brush.Color:=clred;
          shape7.Brush.Color:=clred;
          shape8.Brush.Color:=clred;
          shape9.Brush.Color:=clred;
        end;
      end;


      if(VariablesUn.giSignalStrength>-95)and(VariablesUn.giSignalStrength<-80)then
      begin
        shape13.Brush.Color:=clred;
        shape12.Brush.Color:=clWhite;
        shape11.Brush.Color:=clWhite;
        shape10.Brush.Color:=clWhite;
      end
      else if(VariablesUn.giSignalStrength>=-80)and(VariablesUn.giSignalStrength<-65)then
      begin
        shape13.Brush.Color:=clyellow;
        shape12.Brush.Color:=clyellow;
        shape11.Brush.Color:=clWhite;
        shape10.Brush.Color:=clWhite;
      end
      else if(VariablesUn.giSignalStrength>=-65)and(VariablesUn.giSignalStrength<-50)then
      begin
        shape13.Brush.Color:=cllime;
        shape12.Brush.Color:=cllime;
        shape11.Brush.Color:=cllime;
        shape10.Brush.Color:=clWhite;
      end
      else if(VariablesUn.giSignalStrength>=-50)and(VariablesUn.giSignalStrength<0)then
      begin
        shape13.Brush.Color:=cllime;
        shape12.Brush.Color:=cllime;
        shape11.Brush.Color:=cllime;
        shape10.Brush.Color:=cllime;
      end
      else
      begin
        shape13.Brush.Color:=clred;
        shape12.Brush.Color:=clred;
        shape11.Brush.Color:=clred;
        shape10.Brush.Color:=clred;
      end;
    end;
  End;
end;


procedure TIQ3RFDisplayfm.rhIQ3DisplayMSGReceived(Sender: TObject);
begin

  if(giSentCom = (StrToFloat(IntToStr(VariablesUn.giCommand)+'.'+IntToStr(VariablesUn.giCommandHi)))*1000) and (giUID = VariablesUn.giUID_rx) then
  begin

    if InRange(VariablesUn.giCommandHi,0,15)then
    Begin
      PassFlag:=True;
      DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
      DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidIQ3.text+' and ChannelID = ' + ChToUpdate;
      DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
      DataUn.dmDataModule.dsIQ3Channels.Edit;
      DataUn.dmDataModule.dsIQ3Channels.FieldByName('Threshold1').AsString := IntToStr(VariablesUn.DataMidLowConvert);
    End
    else if InRange(VariablesUn.giCommandHi,16,31)then
    Begin
      PassFlag:=True;
      DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
      DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidIQ3.text+' and ChannelID = ' + ChToUpdate;
      DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
      DataUn.dmDataModule.dsIQ3Channels.Edit;
      DataUn.dmDataModule.dsIQ3Channels.FieldByName('Threshold2').AsString := IntToStr(VariablesUn.DataMidLowConvert);
    End
    else if InRange(VariablesUn.giCommandHi,32,47)then
    Begin
      PassFlag:=True;
      DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
      DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidIQ3.text+' and ChannelID = ' + ChToUpdate;
      DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
      DataUn.dmDataModule.dsIQ3Channels.Edit;
      DataUn.dmDataModule.dsIQ3Channels.FieldByName('Threshold3').AsString := IntToStr(VariablesUn.DataMidLowConvert);
    End
    else if InRange(VariablesUn.giCommandHi,48,63) then
    Begin
      PassFlag:=True;
      DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
      DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidIQ3.text+' and ChannelID = ' + ChToUpdate;
      DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
      DataUn.dmDataModule.dsIQ3Channels.Edit;
      DataUn.dmDataModule.dsIQ3Channels.FieldByName('Threshold4').AsString := IntToStr(VariablesUn.DataMidLowConvert);
    End
    else if InRange(VariablesUn.giCommandHi,80,95) then
    Begin
      PassFlag:=True;
      DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
      DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidIQ3.text+' and ChannelID = ' + ChToUpdate;
      DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
      DataUn.dmDataModule.dsIQ3Channels.Edit;
      DataUn.dmDataModule.dsIQ3Channels.FieldByName('ShadePosition').AsString := VariablesUn.IntToBin(VariablesUn.giDataL);
      ConvertShadePosToCB(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ShadePosition').asstring);
    End
    else if InRange(VariablesUn.giCommandHi,208,223) then
    begin
      PassFlag:=True;
      DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
      DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidIQ3.text+' and ChannelID = ' + ChToUpdate;
      DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
      dmDataModule.dsIQ3Channels.Edit;
      DataUn.dmDataModule.dsIQ3Channels.FieldByName('HysteresisPercent').AsInteger := VariablesUn.giDataL;
    end;
  end;
end;

procedure TIQ3RFDisplayfm.disableEnableButtons(Enableflg:boolean);
begin
  if Enableflg then
  begin
    cmbProjectIQ3.enabled:=true;
    cmbDeviceSelector.enabled:=true;
    cmbUidIQ3.enabled:=true;
    cmbChannel.enabled:=true;
    cbUp.enabled:=true;
    cb1.enabled:=true;
    cb2.enabled:=true;
    cb3.enabled:=true;
    cbdown.enabled:=true;
    teLuxTh1.enabled:=true;
    teLuxTh2.enabled:=true;
    teLuxTh3.enabled:=true;
    teLuxTh4.enabled:=true;
    teHysteresis.enabled:=true;
    btUpdate.Enabled:=true;
  end
  else
  begin
    cmbProjectIQ3.enabled:=false;
    cmbDeviceSelector.enabled:=false;
    cmbUidIQ3.enabled:=false;
    cmbChannel.enabled:=false;
    cbUp.enabled:=false;
    cb1.enabled:=false;
    cb2.enabled:=false;
    cb3.enabled:=false;
    cbdown.enabled:=false;
    teLuxTh1.enabled:=false;
    teLuxTh2.enabled:=false;
    teLuxTh3.enabled:=false;
    teLuxTh4.enabled:=false;
    teHysteresis.enabled:=false;
    btUpdate.Enabled:=false;
  end;

end;


procedure TIQ3RFDisplayfm.btUpdateClick(Sender: TObject);
var
  viHi,viLo:Integer;
  ChannelMath:string;

begin
//Set Threshhold 1
  Screen.Cursor := crHourGlass;
  disableEnableButtons(false);
  giloopExit:=false;
  gipauseLoopExit:=true;
  PassLoopFlag:=true;
  while not giloopExit do
  begin
    VariablesUn.Delay(10);
  end;
  ChToUpdate:=cmbChannel.Text;
  DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
  DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidIQ3.text + ' and ChannelID = ' + cmbChannel.Text ;
  DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
  PassFlag:=false;
  if(teLuxTh1.Text<>'-')then
  begin
    if(teLuxTh1.Text<>Dataun.dmDataModule.dsIQ3Channels.FieldByName('Threshold1').AsString)and(StrToInt(teLuxTh1.Text)>0)then
    Begin
      giUID := StrToInt(cmbUidIQ3.text);
      viHi := Hi(StrToInt(teLuxTh1.Text));
      viLo := Lo(StrToInt(teLuxTh1.Text));
      ChannelMath:=IntToStr(StrToInt(cmbChannel.Text)-1);
      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
      ComFm.SetMWCParams(StrToInt(cmbUidIQ3.Text),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3DisplayMSGReceived);
      while not PassFlag do
      begin
        PauseCount:=PauseCount+1;
        VariablesUn.delay(10);
        if PauseCount=100 then
          ComFm.SetMWCParams(StrToInt(cmbUidIQ3.Text),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3DisplayMSGReceived);
        if PauseCount>300 then
        begin
          teluxTh1.Color:=clred;
          btupdate.Color:=clred;
          //ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
          Break;
        end;
      end;
      if(PassFlag)then
      begin
        teluxTh1.Color:=clWhite;
        btupdate.Color:=clbtnface;
      end
      else
      begin
        teluxTh1.Color:=clred;
        btupdate.Color:=clred;
      end;
      PassFlag:=False;
      PauseCount:=0;
    End
    else
    begin
      teluxTh1.Color:=clwhite;
      btupdate.Color:=clbtnface;
    end;
  end;
  //Set Threshhold 2
  DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
  DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidIQ3.text + ' and ChannelID = ' + cmbChannel.Text ;
  DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
  if(teLuxTh2.Text<>'-')then
  begin
    if(teLuxTh2.Text<>Dataun.dmDataModule.dsIQ3Channels.FieldByName('Threshold2').AsString)and(StrToInt(teLuxTh2.Text)>0)then
    Begin
      giUID := StrToInt(cmbUidIQ3.text);
      viHi := Hi(StrToInt(teLuxTh2.Text));
      viLo := Lo(StrToInt(teLuxTh2.Text));
      ChannelMath:=IntToStr((StrToInt(cmbChannel.Text)-1)+16);
      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
      ComFm.SetMWCParams(StrToInt(cmbUidIQ3.Text),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3DisplayMSGReceived);
      while not PassFlag do
      begin
        PauseCount:=PauseCount+1;
        VariablesUn.delay(10);
        if PauseCount=100 then
          ComFm.SetMWCParams(StrToInt(cmbUidIQ3.Text),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3DisplayMSGReceived);
        if PauseCount>300 then
        begin
          teluxTh2.Color:=clred;
          btupdate.Color:=clred;
          //ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
          Break;
        end;
      end;
      if(PassFlag)then
      begin
        teluxTh2.Color:=clWhite;
        btupdate.Color:=clbtnface;
      end
      else
      begin
        teluxTh2.Color:=clred;
        btupdate.Color:=clred;
      end;
      PassFlag:=False;
      PauseCount:=0;
    End
    else
    begin
      teluxTh2.Color:=clwhite;
      btupdate.Color:=clbtnface;
    end;
  end;
  //Set Threshhold 3
  DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
  DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidIQ3.text + ' and ChannelID = ' + cmbChannel.Text ;
  DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
  if(teLuxTh3.Text<>'-')then
  begin
    if(teLuxTh3.Text<>Dataun.dmDataModule.dsIQ3Channels.FieldByName('Threshold3').AsString)and(StrToInt(teLuxTh3.Text)>0)then
    Begin
      giUID := StrToInt(cmbUidIQ3.text);
      viHi := Hi(StrToInt(teLuxTh3.Text));
      viLo := Lo(StrToInt(teLuxTh3.Text));
      ChannelMath:=IntToStr((StrToInt(cmbChannel.Text)-1)+32);
      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
      ComFm.SetMWCParams(StrToInt(cmbUidIQ3.Text),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3DisplayMSGReceived);
      while not PassFlag do
      begin
        PauseCount:=PauseCount+1;
        VariablesUn.delay(10);
        if PauseCount=100 then
          ComFm.SetMWCParams(StrToInt(cmbUidIQ3.Text),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3DisplayMSGReceived);
        if PauseCount>300 then
        begin
          teluxTh3.Color:=clred;
          btupdate.Color:=clred;
          //ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
          Break;
        end;
      end;
      if(PassFlag)then
      begin
        teluxTh3.Color:=clWhite;
        btupdate.Color:=clbtnface;
      end
      else
      begin
        teluxTh3.Color:=clred;
        btupdate.Color:=clred;
      end;
      PassFlag:=False;
      PauseCount:=0;
    End
    else
    begin
      teluxTh3.Color:=clwhite;
      btupdate.Color:=clbtnface;
    end;
  end;
  //Set Threshhold 4
  DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
  DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidIQ3.text + ' and ChannelID = ' + cmbChannel.Text ;
  DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
  if(teLuxTh4.Text<>'-')then
  begin
    if(teLuxTh4.Text<>Dataun.dmDataModule.dsIQ3Channels.FieldByName('Threshold4').AsString)and(StrToInt(teLuxTh4.Text)>0)then
    Begin
      giUID := StrToInt(cmbUidIQ3.text);
      viHi := Hi(StrToInt(teLuxTh4.Text));
      viLo := Lo(StrToInt(teLuxTh4.Text));
      ChannelMath:=IntToStr((StrToInt(cmbChannel.Text)-1)+48);
      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
      ComFm.SetMWCParams(StrToInt(cmbUidIQ3.Text),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3DisplayMSGReceived);
      while not PassFlag do
      begin
        PauseCount:=PauseCount+1;
        VariablesUn.delay(10);
        if PauseCount=100 then
          ComFm.SetMWCParams(StrToInt(cmbUidIQ3.Text),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3DisplayMSGReceived);
        if PauseCount>300 then
        begin
          //ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
          Break;
        end;
      end;
      if(PassFlag)then
      begin
        teluxTh4.Color:=clWhite;
        btupdate.Color:=clbtnface;
      end
      else
      begin
        teluxTh4.Color:=clred;
        btupdate.Color:=clred;
      end;
      PassFlag:=False;
      PauseCount:=0;
    End
    else
    begin
      teluxTh4.Color:=clwhite;
      btupdate.Color:=clbtnface;
    end;
  end;
  //Set enabled POS, Night Mode, Retract Mode
  DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
  DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidIQ3.text + ' and ChannelID = ' + cmbChannel.Text ;
  DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
  if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ShadePosition').AsString<>'') then
  begin
    EnableShadePos:=ConvertShadePosToBin();
    if(EnableShadePos<>Dataun.dmDataModule.dsIQ3Channels.FieldByName('ShadePosition').AsString) then
    Begin
      giUID := StrToInt(cmbUidIQ3.text);
      viHi := 0;
      viLo := VariablesUn.BinToInt(EnableShadePos);
      ChannelMath:=IntToStr((StrToInt(cmbChannel.Text)-1)+80);
      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
      ComFm.SetMWCParams(StrToInt(cmbUidIQ3.Text),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3DisplayMSGReceived);
      while not PassFlag do
      begin
        PauseCount:=PauseCount+1;
        VariablesUn.delay(10);
        if PauseCount=100 then
          ComFm.SetMWCParams(StrToInt(cmbUidIQ3.Text),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3DisplayMSGReceived);
        if PauseCount>300 then
        begin
          //ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
          Break;
        end;
      end;
      if(PassFlag)then
        begin
          if shpUp.Visible then
          begin
            shpUp.Visible:=false;
            shpUp.pen.Color:=clLime;
            btupdate.Color:=clbtnface;
          end;
          if shpPs1.Visible then
          begin
            shpPs1.Visible:=false;
            shpPs1.pen.Color:=clLime;
            btupdate.Color:=clbtnface;
          end;
          if shpPs2.Visible then
          begin
            shpPs2.Visible:=false;
            shpPs2.pen.Color:=clLime;
            btupdate.Color:=clbtnface;
          end;
          if shpPs3.Visible then
          begin
            shpPs3.Visible:=false;
            shpPs3.pen.Color:=clLime;
            btupdate.Color:=clbtnface;
          end;
          if shpDN.Visible then
          begin
            shpDN.Visible:=false;
            shpDN.pen.Color:=clLime;
            btupdate.Color:=clbtnface;
          end;
        end
        else
        begin
          if shpUp.Visible then
          begin
            shpUp.pen.Color:=clred;
            btupdate.Color:=clred;
          end;
          if shpPs1.Visible then
          begin
            shpPs1.pen.Color:=clred;
            btupdate.Color:=clred;
          end;
          if shpPs2.Visible then
          begin
            shpPs2.pen.Color:=clred;
            btupdate.Color:=clred;
          end;
          if shpPs3.Visible then
          begin
            shpPs3.pen.Color:=clred;
            btupdate.Color:=clred;
          end;
          if shpDN.Visible then
          begin
            shpDN.pen.Color:=clred;
            btupdate.Color:=clred;
          end;
        end;
      PassFlag:=False;
      PauseCount:=0;
    end
    else
    begin
      if shpUp.Visible then
      begin
        shpUp.Visible:=false;
        shpUp.pen.Color:=clLime;
        btupdate.Color:=clbtnface;
      end;
      if shpPs1.Visible then
      begin
        shpPs1.Visible:=false;
        shpPs1.pen.Color:=clLime;
        btupdate.Color:=clbtnface;
      end;
      if shpPs2.Visible then
      begin
        shpPs2.Visible:=false;
        shpPs2.pen.Color:=clLime;
        btupdate.Color:=clbtnface;
      end;
      if shpPs3.Visible then
      begin
        shpPs3.Visible:=false;
        shpPs3.pen.Color:=clLime;
        btupdate.Color:=clbtnface;
      end;
      if shpDN.Visible then
      begin
        shpDN.Visible:=false;
        shpDN.pen.Color:=clLime;
        btupdate.Color:=clbtnface;
      end;
    end;
  end;
    //Set Hysteresis
  DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
  DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidIQ3.text + ' and ChannelID = ' + cmbChannel.Text ;
  DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
  if(teHysteresis.Text<>'-')then
  begin
    if(StrToInt(teHysteresis.text)<>Dataun.dmDataModule.dsIQ3Channels.FieldByName('HysteresisPercent').AsInteger) then
    Begin
      giUID := StrToInt(cmbUidIQ3.text);
      viHi := 0;
      ChannelMath:=IntToStr((StrToInt(cmbChannel.Text)-1)+208);
      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
      ComFm.SetMWCParams(StrToInt(cmbUidIQ3.text),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), teHysteresis.Text,rhIQ3DisplayMSGReceived);
      while not PassFlag do
      begin
          PauseCount:=PauseCount+1;
          VariablesUn.delay(10);
          if PauseCount=100 then
            ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), teHysteresis.Text,rhIQ3DisplayMSGReceived);
          if PauseCount>300 then
          begin
            //ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
            Break;
          end;
      end;
      if(PassFlag)then
      begin
        teHysteresis.Color:=clWhite;
        btupdate.Color:=clbtnface;
      end
      else
      begin
        teHysteresis.Color:=clred;
        btupdate.Color:=clred;
      end;
      PassFlag:=False;
      PauseCount:=0;
    End
    else
    begin
      btupdate.Color:=clbtnface;
      teHysteresis.Color:=clwhite;
    end;
  end;
  stProgrammingFlg:=true;
  cmbChannelChange(NIL);
  stProgrammingFlg:=false;
  gipauseLoopExit:=false;
  PassLoopFlag:=false;
  disableEnableButtons(true);
  Screen.Cursor := crDefault;
  Async(MessageLoop).Await(
  Procedure
  begin
    giloopExit:=true;
  end);
end;

procedure TIQ3RFDisplayfm.btUpdateMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Sender is TPanel
  then with TPanel(Sender)
       do BevelOuter := bvLowered; { bvLowered for MouseDown }
end;

procedure TIQ3RFDisplayfm.btUpdateMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Sender is TPanel
  then with TPanel(Sender)
       do BevelOuter := bvRaised; { bvLowered for MouseDown }
end;

procedure TIQ3RFDisplayfm.cb1Click(Sender: TObject);
begin
  if cb1.Checked=false then
  begin
    DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
    DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidIQ3.text + ' and ChannelID = ' + cmbChannel.Text ;
    DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
    if (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring='1') then
    begin
      if not(cb2.Checked)and not(cb3.Checked)and not(cbdown.Checked) and (cbup.checked)or not(cb2.Checked)and not(cb3.Checked)and(cbdown.Checked) and not(cbup.checked)or not(cb2.Checked)and (cb3.Checked)and not(cbdown.Checked) and not(cbup.checked)or (cb2.Checked)and not(cb3.Checked)and not(cbdown.Checked) and not(cbup.checked)then
      begin
        showmessage('There must be at lest two positions checked');
        cb1.Checked:=true;
      end;
    end;
  end;
end;

procedure TIQ3RFDisplayfm.cb1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  shpPs1.visible:=true;
  shpPs1.pen.color:=CLlime;
  btupdate.Color:=CLLime;
end;

procedure TIQ3RFDisplayfm.cb2Click(Sender: TObject);
begin
  if cb2.Checked=false then
  begin
    DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
    DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidIQ3.text + ' and ChannelID = ' + cmbChannel.Text ;
    DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
    if (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring='1') then
    begin
      if not(cb1.Checked)and not(cb3.Checked)and not(cbdown.Checked) and (cbup.checked)or not(cb1.Checked)and not(cb3.Checked)and(cbdown.Checked) and not(cbup.checked)or not(cb1.Checked)and (cb3.Checked)and not(cbdown.Checked) and not(cbup.checked)or (cb1.Checked)and not(cb3.Checked)and not(cbdown.Checked) and not(cbup.checked)then
      begin
        showmessage('There must be at lest two positions checked');
        cb2.Checked:=true;
      end;
    end;
  end;
end;

procedure TIQ3RFDisplayfm.cb2MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  shpPs2.visible:=true;
  shpPs2.pen.color:=CLlime;
  btupdate.Color:=CLLime;
end;

procedure TIQ3RFDisplayfm.cb3Click(Sender: TObject);
begin

  if cb3.Checked=false then
  begin
    DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
    DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidIQ3.text + ' and ChannelID = ' + cmbChannel.Text ;
    DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
    if (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring='1') then
    begin
      if not(cb1.Checked)and not(cb2.Checked)and not(cbdown.Checked) and (cbup.checked)or not(cb1.Checked)and not(cb2.Checked)and(cbdown.Checked) and not(cbup.checked)or not(cb1.Checked)and (cb2.Checked)and not(cbdown.Checked) and not(cbup.checked)or (cb1.Checked)and not(cb2.Checked)and not(cbdown.Checked) and not(cbup.checked)then
      begin
        showmessage('There must be at lest two positions checked');
        cb3.Checked:=true;
      end;
    end;
  end;
end;

procedure TIQ3RFDisplayfm.cb3MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  shpPs3.visible:=true;
  shpPs3.Pen.Color:=Cllime;
  btupdate.Color:=CLLime;
end;

procedure TIQ3RFDisplayfm.cbDownClick(Sender: TObject);
begin
  if cbdown.Checked=false then
  begin
    DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
    DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidIQ3.text + ' and ChannelID = ' + cmbChannel.Text ;
    DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
    if (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring='1') then
    begin
      if not(cb1.Checked)and not(cb2.Checked)and not(cb3.Checked) and (cbup.checked)or not(cb1.Checked)and not(cb2.Checked)and(cb3.Checked) and not(cbup.checked)or not(cb1.Checked)and (cb2.Checked)and not(cb3.Checked) and not(cbup.checked)or (cb1.Checked)and not(cb2.Checked)and not(cb3.Checked) and not(cbup.checked)then
      begin
        showmessage('There must be at lest two positions checked');
        cbDown.Checked:=true;
      end;
    end;
  end;
end;

procedure TIQ3RFDisplayfm.cbDownMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  shpDn.visible:=true;
  shpDn.Pen.Color:=CLlime;
  btupdate.Color:=CLLime;
end;

procedure TIQ3RFDisplayfm.cbUpClick(Sender: TObject);
begin
  if cbUp.Checked=false then
  begin
    DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
    DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidIQ3.text + ' and ChannelID = ' + cmbChannel.Text ;
    DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
    if (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring='1') then
    begin
      if not(cb2.Checked)and not(cb3.Checked)and not(cbdown.Checked) and (cb1.checked)or not(cb2.Checked)and not(cb3.Checked)and(cbdown.Checked) and not(cb1.checked)or not(cb2.Checked)and (cb3.Checked)and not(cbdown.Checked) and not(cb1.checked)or (cb2.Checked)and not(cb3.Checked)and not(cbdown.Checked) and not(cb1.checked)then
      begin
        showmessage('There must be at lest two positions checked');
        cbUp.Checked:=true;
      end;
    end;
  end;
end;

procedure TIQ3RFDisplayfm.cbUpMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  shpUp.visible:=true;
  shpUp.pen.color:=CLlime;
  btupdate.Color:=CLLime;
end;

procedure TIQ3RFDisplayfm.cmbChannelChange(Sender: TObject);
begin
  if not stProgrammingFlg then
  begin
    btupdate.Color:=clbtnface;
    teLuxTh1.Color:=clwhite;
    teLuxTh2.Color:=clwhite;
    teLuxTh3.Color:=clwhite;
    teLuxTh4.Color:=clwhite;
    teHysteresis.Color:=clwhite;

    shpUp.Visible:=false;
    shpPs1.Visible:=false;
    shpps2.Visible:=false;
    shpPs3.Visible:=false;
    shpDn.Visible:=false;

    shpUp.pen.Color:=clLime;
    shpPs1.pen.Color:=clLime;
    shpps2.pen.Color:=clLime;
    shpPs3.pen.Color:=clLime;
    shpDn.pen.Color:=clLime;
  end;

  VariablesUn.gsChannelNum:=IntToStr(cmbChannel.ItemIndex);
  DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
  DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + IQ3RFDisplayfm.cmbUidIQ3.text + ' and ChannelID = ' + cmbChannel.Text;
  DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
  if(DataUn.dmDataModule.dsIQ3Channels.FieldByName('bound').asBoolean)then
  begin
    cbPaired.Checked:=True;
  end
  else
  begin
    cbPaired.Checked:=false;
  end;
  if not(DataUn.dmDataModule.dsIQ3Channels.EOF)then
  begin
    teLuxTh1.text := DataUn.dmDataModule.dsIQ3Channels.FieldByName('Threshold1').AsString;
    teLuxTh2.text := DataUn.dmDataModule.dsIQ3Channels.FieldByName('Threshold2').AsString;
    teLuxTh3.text := DataUn.dmDataModule.dsIQ3Channels.FieldByName('Threshold3').AsString;
    teLuxTh4.text := DataUn.dmDataModule.dsIQ3Channels.FieldByName('Threshold4').AsString;
    teHysteresis.Text:= Dataun.dmDataModule.dsIQ3Channels.FieldByName('HysteresisPercent').asstring;
    lblLuxTh1down.caption := FloatToStr(((100-StrToFloat(teHysteresis.Text))/100)*StrToFloat(DataUn.dmDataModule.dsIQ3Channels.FieldByName('Threshold1').AsString));
    lblLuxTh2down.caption := FloatToStr(((100-StrToFloat(teHysteresis.Text))/100)*StrToFloat(DataUn.dmDataModule.dsIQ3Channels.FieldByName('Threshold2').AsString));
    lblLuxTh3down.caption := FloatToStr(((100-StrToFloat(teHysteresis.Text))/100)*StrToFloat(DataUn.dmDataModule.dsIQ3Channels.FieldByName('Threshold3').AsString));
    lblLuxTh4down.caption := FloatToStr(((100-StrToFloat(teHysteresis.Text))/100)*StrToFloat(DataUn.dmDataModule.dsIQ3Channels.FieldByName('Threshold4').AsString));
    teChZGN.Text := Dataun.dmDataModule.dsIQ3Channels.FieldByName('ZGN').asstring;
    lbIntensity.caption := '-';
    lbtemperature.caption:='-';
  end
  else
  begin
    lbIntensity.caption := '-';
    lbtemperature.caption:='-';
    teLuxTh1.text := '-';
    teLuxTh2.text := '-';
    teLuxTh3.text := '-';
    teLuxTh4.text := '-';
    lbTelegramCounter.Caption := '-';
    lbIntensity.Caption :='-';
    lbTemperature.Caption :='-';
  end;
  DataUn.dmDataModule.dsIQ3s.Filtered := False;
  DataUn.dmDataModule.dsIQ3s.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + IQ3RFDisplayfm.cmbUidIQ3.text;
  DataUn.dmDataModule.dsIQ3s.Filtered := True;
  teAlias.Text:=DataUn.dmDataModule.dsIQ3s.FieldByName('Alias').AsString;
  if(DataUn.dmDataModule.dsIQ3s.FieldByName('EnableMessages').AsString='1')then
    ConvertShadePosToCB(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ShadePosition').asstring);
  if(DataUn.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='2') or (DataUn.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='3')or (DataUn.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='4')or (DataUn.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='5')then
  begin
    imgShadedown.Visible:=false;
    imgShadeps1.Visible:=false;
    imgShadeps2.Visible:=false;
    imgShadeps3.Visible:=false;
    imgShadeup.Visible:=false;
    image1.Visible:=false;
    image2.Visible:=false;
    cbup.Visible:=false;
    cb1.Visible:=False;
    cb2.Visible:=false;
    cb3.Visible:=false;
    cbdown.Visible:=false;
    label10.Visible:=false;
    label14.Visible:=false;
    label15.Visible:=false;
    label16.Visible:=false;
    label17.Visible:=false;
    label18.Visible:=false;
    label19.Visible:=false;
    label21.Visible:=false;
    tehysteresis.Visible:=false;
    teluxTH1.Visible:=false;
    teluxTh2.Visible:=false;
    teluxTh3.Visible:=false;
    teluxTh4.Visible:=false;
    lblluxTH1down.Visible:=false;
    lblluxTh2down.Visible:=false;
    lblluxTh3down.Visible:=false;
    lblluxTh4down.Visible:=false;
    btUpdate.Visible:=false;
    lblshadepos.Visible:=false;
    pnlLiveData.Visible:=false;

    if((DataUn.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='2')) then
    begin
      lblSwitch.Caption:='SWITCH';
      lblOccStatus.Visible:=false;
      lblOccStatusData.Visible:=false;
      pnlRefresh.Visible:=false;
    end
    else if((DataUn.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='3')) then
    begin
      lblSwitch.Caption:='OCCUPANCY SENSOR';
      lblOccStatus.Visible:=true;
      lblOccStatusData.Visible:=true;
      pnlRefresh.Visible:=true;
    end
    else if((DataUn.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='4')) then
    begin
      lblSwitch.Caption:='MBK 4B Auto';
      lblOccStatus.Visible:=false;
      lblOccStatusData.Visible:=false;
      pnlRefresh.Visible:=false;
    end
     else if((DataUn.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='5')) then
    begin
      lblSwitch.Caption:='MBK 4B Normal';
      lblOccStatus.Visible:=false;
      lblOccStatusData.Visible:=false;
      pnlRefresh.Visible:=false;
    end
    else if((DataUn.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='6')) then
    begin
      lblSwitch.Caption:='MBK 7B Auto';
      lblOccStatus.Visible:=false;
      lblOccStatusData.Visible:=false;
      pnlRefresh.Visible:=false;
    end
     else if((DataUn.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='7')) then
    begin
      lblSwitch.Caption:='MBK 7B Normal';
      lblOccStatus.Visible:=false;
      lblOccStatusData.Visible:=false;
      pnlRefresh.Visible:=false;
    end;
    lblSwitch.visible:=true;

  end
  else
  begin
    lblSwitch.visible:=False;
    if(DataUn.dmDataModule.dsIQ3s.FieldByName('EnableMessages').AsString='1')then
    begin
      pnlRefresh.Visible:=true;
      pnlLiveData.Visible:=true;
      imgShadedown.Visible:=true;
      imgShadeps1.Visible:=true;
      imgShadeps2.Visible:=true;
      imgShadeps3.Visible:=true;
      imgShadeup.Visible:=true;
      image1.Visible:=true;
      image2.Visible:=true;
      cbup.Visible:=true;
      cb1.Visible:=true;
      cb2.Visible:=true;
      cb3.Visible:=true;
      cbdown.Visible:=true;
      label10.Visible:=true;
      label14.Visible:=true;
      label15.Visible:=true;
      label16.Visible:=true;
      label17.Visible:=true;
      label18.Visible:=true;
      label19.Visible:=true;
      label21.Visible:=true;
      tehysteresis.Visible:=true;
      teluxTH1.Visible:=true;
      teluxTh2.Visible:=true;
      teluxTh3.Visible:=true;
      teluxTh4.Visible:=true;
      lblluxTH1down.Visible:=true;
      lblluxTh2down.Visible:=true;
      lblluxTh3down.Visible:=true;
      lblluxTh4down.Visible:=true;
      btUpdate.Visible:=true;
      lblshadepos.Visible:=true;
      lblSwitch.visible:=false;
      lblOccStatus.Visible:=false;
      lblOccStatusData.Visible:=false;
    end
    else
    begin
      lblSwitch.visible:=false;
      lblOccStatus.Visible:=false;
      lblOccStatusData.Visible:=false;
      pnlLiveData.Visible:=true;
    end;
  end;
end;

procedure TIQ3RFDisplayfm.cmbDeviceSelectorChange(Sender: TObject);
begin
  VariablesUn.giTop:=Top;
  VariablesUn.giLeft:=Left;
  if(cmbDeviceSelector.Text='MTR')then
  begin
    DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
    DataUn.dmDataModule.dsIQ3s.SaveToFile(VariablesUn.gsDataPath + 'dsIQ3s.txt', dfXML);
    DataUn.dmDataModule.dsIQ3Channels.SaveToFile(VariablesUn.gsDataPath + 'dsIQ3Channels.txt', dfXML);
    if Assigned (rh_ShowMain) then
    begin
      rh_ShowMain(Self);
    end;
    Hide;
    variablesUn.PassLoopFlag:=True;
  end
  else if(cmbDeviceSelector.Text='MNI')then
  begin
    DataUn.dmDataModule.dsMNI.Filtered := False;
    DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsMNI.Filtered := True;
    if not DataUn.dmDataModule.dsMNI.EOF then
    begin
      IQ3RFDisplayFm.Hide;
      MNIConfigFm.Show;
      VariablesUn.giTop := Top;
      VariablesUn.giLeft := Left;
      variablesUn.PassLoopFlag:=True;
    end
    else
    begin
      ShowMessage('No MNI Devices Found');
      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('iQ3-DC-RF');
    end;


  end
  else if(cmbDeviceSelector.Text='MDC')then
  begin
    DataUn.dmDataModule.dsMNI2.Filtered := False;
    DataUn.dmDataModule.dsMNI2.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsMNI2.Filtered := True;

    if not dmDataModule.dsMNI2.Eof then
    begin
      IQ3RFDisplayFm.Hide;
      rh_showMNI2Config(self);
      VariablesUn.giTop := Top;
      VariablesUn.giLeft := Left;
      rh_GetMNI2VirtualMotorStatus(Self);
    end
    else
    begin
      ShowMessage('No MDC Devices Found');
      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('iQ3-DC RF');
    end;

  end
  else if(cmbDeviceSelector.Text='IQMLC2')then
  begin
    DataUn.dmDataModule.dsIQMLC2.Filtered := False;
    DataUn.dmDataModule.dsIQMLC2.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsIQMLC2.Filtered := True;

    if not dmDataModule.dsIQMLC2.Eof then
    begin
      Hide;
      VariablesUn.giTop := Top;
      VariablesUn.giLeft := Left;
      if Assigned (rh_showIQMLC2Config) then
        rh_showIQMLC2Config(self);
      //IQMLC2configFm.Show;
    end
    else
    begin
      ShowMessage('No IQMLC2 Devices Found');
      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('iQ3-DC RF');
    end;
  end
  else if(cmbDeviceSelector.Text='PowerPanel')then
  begin
    DataUn.dmDataModule.dsPowerPanel.Filtered := False;
    DataUn.dmDataModule.dsPowerPanel.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsPowerPanel.Filtered := True;

    if not dmDataModule.dsPowerPanel.Eof then
    begin
      Hide;
      VariablesUn.giTop := Top;
      VariablesUn.giLeft := Left;
      PowerPanelConfigFm.Show;
    end
    else
    begin
      ShowMessage('No IQ2-DC Power Panel Devices Found');
      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('iQ3-DC RF');
    end;
  end
  else if(cmbDeviceSelector.Text='Motor Maint')then
  begin
    DataUn.dmDataModule.dsMotors.Filtered := False;
    DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) + ' and MotorType = 69 or MotorType = 66';
    DataUn.dmDataModule.dsMotors.Filtered := True;

    if not dmDataModule.dsMotors.Eof then
    begin
      VariablesUn.giTop := Top;
      VariablesUn.giLeft := Left;
      Hide;
      MotorMaintenanceDisplayFm.Show;
      MotorMaintenanceDisplayFm.cmbUidMM.ItemIndex:= 0;
      MotorMaintenanceDisplayFm.cmbUidMM.OnChange(self);
    end
    else
    begin
      ShowMessage('No Motors Found');
      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('iQ3-DC RF');
    end;
  end;

end;

procedure TIQ3RFDisplayfm.cmbProjectIQ3Change(Sender: TObject);
begin
  if cmbProjectIQ3.ItemIndex > -1 then
  begin
    VariablesUn.gsProjectName := cmbProjectIQ3.Text;
    SelectProject;
    ShowUID;
  end;
end;

procedure TIQ3RFDisplayfm.SelectProject;
begin
  try
    if not (DataUn.dmDataModule.dsProject.State = dsInactive) then
    begin
      DataUn.dmDataModule.dsProject.Filtered := False;
      DataUn.dmDataModule.dsProject.First;
      while not (dmDataModule.dsProject.Eof) do
      begin
        dmDataModule.dsProject.Edit;
        DataUn.dmDataModule.dsProject.FieldByName('Last').AsInteger := 0;
        DataUn.dmDataModule.dsProject.Post;
        DataUn.dmDataModule.dsProject.Next;
      end;
      //DataUn.dmDataModule.dsProject.Filtered := False;
      DataUn.dmDataModule.dsProject.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
      DataUn.dmDataModule.dsProject.Filtered := True;
      if not dmDataModule.dsProject.Eof then
      begin
        dmDataModule.dsProject.Edit;
        DataUn.dmDataModule.dsProject.FieldByName('Last').AsInteger := 1;
        DataUn.dmDataModule.dsProject.Post;
      end;
      DataUn.dmDataModule.dsProject.Filtered := False;
      dmDataModule.dsProject.MergeChangeLog;
      DataUn.dmDataModule.dsProject.SaveToFile(VariablesUn.gsDataPath + 'dsProject.txt', dfXML);
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TIQ3RFDisplayfm.SelectProject: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  End;
end;

procedure TIQ3RFDisplayfm.cmbUidIQ3Change(Sender: TObject);
begin
  try
    lbTelegramCounter.Caption := '-';
    lbIntensity.Caption :='-';
    lbTemperature.Caption :='-';
    if cmbUidIQ3.text<>'' then
    begin
      DataUn.dmDataModule.dsIQ3s.Filtered := False;
      DataUn.dmDataModule.dsIQ3s.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + IQ3RFDisplayfm.cmbUidIQ3.text;
      DataUn.dmDataModule.dsIQ3s.Filtered := True;
      if(DataUn.dmDataModule.dsIQ3s.FieldByName('EnableMessages').AsString='0')then
      begin
        image1.Visible:=false;
        image2.Visible:=false;
        cbup.Visible:=false;
        cb1.Visible:=False;
        cb2.Visible:=false;
        cb3.Visible:=false;
        cbdown.Visible:=false;
        label10.Visible:=false;
        label14.Visible:=false;
        label15.Visible:=false;
        label16.Visible:=false;
        label17.Visible:=false;
        label18.Visible:=false;
        label19.Visible:=false;
        label21.Visible:=false;
        tehysteresis.Visible:=false;
        teluxTH1.Visible:=false;
        teluxTh2.Visible:=false;
        teluxTh3.Visible:=false;
        teluxTh4.Visible:=false;
        lblluxTH1down.Visible:=false;
        lblluxTh2down.Visible:=false;
        lblluxTh3down.Visible:=false;
        lblluxTh4down.Visible:=false;
        lblShadePOS.Visible:=false;
        imgShadeup.Visible:=false;
        imgShadeps1.Visible:=false;
        imgShadeps2.Visible:=false;
        imgShadeps3.Visible:=false;
        imgShadedown.Visible:=false;

        btUpdate.Visible:=false;
        cmbMode.ItemIndex:=1;
        if(cmbUidIQ3.text<>'')then
          cmbChannelChange(nil);
      end
      else
      begin
        image1.Visible:=True;
        image2.Visible:=True;
        cbup.Visible:=True;
        cb1.Visible:=True;
        cb2.Visible:=True;
        cb3.Visible:=True;
        cbdown.Visible:=True;
        label10.Visible:=True;
        label14.Visible:=True;
        label15.Visible:=True;
        label16.Visible:=True;
        label17.Visible:=True;
        label18.Visible:=True;
        label19.Visible:=True;
        label21.Visible:=True;
        tehysteresis.Visible:=True;
        teluxTH1.Visible:=True;
        teluxTh2.Visible:=True;
        teluxTh3.Visible:=True;
        teluxTh4.Visible:=True;
        lblluxTH1down.Visible:=True;
        lblluxTh2down.Visible:=True;
        lblluxTh3down.Visible:=True;
        lblluxTh4down.Visible:=True;
        btUpdate.Visible:=True;
        imgShadeup.Visible:=True;
        imgShadeps1.Visible:=True;
        imgShadeps2.Visible:=True;
        imgShadeps3.Visible:=True;
        imgShadedown.Visible:=True;
        cmbMode.ItemIndex:=0;
        if(cmbUidIQ3.text<>'')then
        begin
          cmbChannelChange(nil);
        end;
      end;
    end;
    lblShadePos.Caption:='Up';
  except
    on E: Exception do
    begin
      MessageDlg('TIQ3RFDisplayfm.SelectProject: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  End;
end;

procedure TIQ3RFDisplayfm.cmbUidIQ3KeyPress(Sender: TObject; var Key: Char);
begin
  //(Key in [#8, '0'..'9'])then
  if not (Key in [#8, '0'..'9']) then
  begin
    // Discard the key
    Key := #0;
  end;
  if (sender = teluxTh1) and (Key <> #0) then
  begin
    teluxTh1.Color:=clLime;
    btUpdate.Color:=cllime;
  end;
  if (sender = teluxTh2)  and (Key <> #0) then
  begin
    teluxTh2.Color:=clLime;
    btUpdate.Color:=cllime;
  end;
  if (sender = teluxTh3)  and (Key <> #0) then
  begin
    teluxTh3.Color:=clLime;
    btUpdate.Color:=cllime;
  end;
  if (sender = teluxTh4)  and (Key <> #0) then
  begin
    teluxTh4.Color:=clLime;
    btUpdate.Color:=cllime;
  end;
  if (sender = teHysteresis)  and (Key <> #0) then
  begin
    teHysteresis.Color:=clLime;
    btUpdate.Color:=cllime;
  end;

end;

procedure TIQ3RFDisplayfm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  btnExitClick(self);
  ComboBox_AutoWidth(cmbDeviceSelector);
end;

//Allows combox  dropdown to size to oversized string items
procedure TIQ3RFDisplayfm.ComboBox_AutoWidth(const theComboBox: TCombobox);
const
  HORIZONTAL_PADDING = 4;
var
  itemsFullWidth: integer;
  idx: integer;
  itemWidth: integer;
begin
  itemsFullWidth := 0;
  // get the max needed with of the items in dropdown state
  for idx := 0 to -1 + theComboBox.Items.Count do
  begin
    itemWidth := theComboBox.Canvas.TextWidth(theComboBox.Items[idx]);
    Inc(itemWidth, 2 * HORIZONTAL_PADDING);
    if (itemWidth > itemsFullWidth) then itemsFullWidth := itemWidth;
  end;
  itemsFullWidth:=itemsFullWidth+20;
    // set the width of drop down if needed
  if (itemsFullWidth > theComboBox.Width) then
  begin
    //check if there would be a scroll bar
    if theComboBox.DropDownCount < theComboBox.Items.Count then
      itemsFullWidth := itemsFullWidth + GetSystemMetrics(SM_CXVSCROLL);
    SendMessage(theComboBox.Handle, CB_SETDROPPEDWIDTH, itemsFullWidth, 0);
  end;
end;

procedure TIQ3RFDisplayfm.FormDestroy(Sender: TObject);
begin
  passLoopFlag:=true;
end;

procedure TIQ3RFDisplayfm.FormHide(Sender: TObject);
var
  I:Integer;
begin
  VariablesUn.giTop := Top;
  VariablesUn.giLeft := Left;
//  for I := 0 to Length(viUIDs)-1 do
//  begin
//    ComFm.SetMWCParams(viUIDs[I],MWC_HaltTimer,'0','0',rhMWCDisplayMSGReceived);
//    VariablesUn.delay(200);
//  end;
end;

procedure TIQ3RFDisplayfm.FormShow(Sender: TObject);
var
  I:Integer;
begin
  cmbDeviceSelector.itemIndex:=2;
  ComFm.rh_IQ3RFDisplayPortOpend := rhPortOpend;
  ComFm.rh_IQ3RFDisplayPortClosed := rhPortClosed;
  Top := VariablesUn.giTop;
  Left := VariablesUn.giLeft;
  cmbUidIQ3.Clear;
  ShowUID;

  //Populate Project
  if DataUn.dmDataModule.dsProject.State <> dsInactive then
  begin
    cmbProjectIQ3.Items.clear;
    DataUn.dmDataModule.dsProject.edit;
    DataUn.dmDataModule.dsProject.Filtered := False;
    DataUn.dmDataModule.dsProject.First;
    //PassLoopFlag:=false;
    VariablesUn.PassLoopFlag:=false;
    while not DataUn.dmDataModule.dsProject.eof do
    begin
      if (DataUn.dmDataModule.dsProject.FieldByName('Last').AsInteger = 1) then
         cmbProjectIQ3.Items.Insert(0, DataUn.dmDataModule.dsProject.FieldByName('ProjectName').AsString)
      else
         cmbProjectIQ3.Items.Add(DataUn.dmDataModule.dsProject.FieldByName('ProjectName').AsString);

      DataUn.dmDataModule.dsProject.Next;
    end;
    DataUn.dmDataModule.dsProject.Filtered := False;
    DataUn.dmDataModule.dsProject.First;
    for I := 0 to cmbProjectIQ3.Items.Count-1 do
    begin
      if cmbProjectIQ3.text = VariablesUn.gsProjectName then
      begin
        break;
      end
      else
      begin
        cmbProjectIQ3.ItemIndex:=I;
      end;

    end;
  end;
  cbUpClick(nil);
  cb1Click(nil);
  cb2Click(nil);
  cb3Click(nil);
  cbDownClick(nil);
  cmbChannelChange(nil);
  Async(MessageLoop).Await(
  Procedure
  begin
    giloopExit:=true;
  end);
  cmbUidIQ3.ItemIndex:=giUIDItemIndex;
  cmbChannel.ItemIndex:=giLastChannel;
  cmbUidIQ3Change(nil);
end;

procedure TIQ3RFDisplayfm.btnSettingsClick(Sender: TObject);
begin
  //SetSetpointsOff;
  ScreenSetupFm.rh_showmain := rhShowIQ3RFDisplay;
  pnlMenuIQ3.Visible := False;
  IQ3RFdisplayfm.Hide;
  ScreenSetupFm.Show;
end;

procedure TIQ3RFDisplayfm.btnSettingsMouseEnter(Sender: TObject);
begin
  pnlMenuIQ3.Visible := True;
end;

procedure TIQ3RFDisplayfm.imgSetupClick(Sender: TObject);
begin
    IQ3Configfm.rh_ShowIQ3RFDisplay := rhShowIQ3RFDisplay;
    Hide;
    IQ3Configfm.Show;
end;

procedure TIQ3RFDisplayfm.JvCaptionButton1Click(Sender: TObject);
begin
  Application.HelpContext(5);
end;

procedure TIQ3RFDisplayfm.rhShowIQ3RFDisplay(Sender: TObject);
begin
  cmbChannel.Itemindex:=giLastChannel;
  IQ3RFDisplayfm.Show;


end;



procedure TIQ3RFDisplayfm.pnlMenuIQ3MouseLeave(Sender: TObject);
begin
  pnlMenuIQ3.Visible := False;
end;

procedure TIQ3RFDisplayfm.pnlRefreshClick(Sender: TObject);
begin
  if(cmbUidIQ3.text<>'')then
  begin
    PassLoopTimeOutFlag:=false;
    PauseCountLoop:=0;
    viUID := StrToInt(cmbUidIQ3.text);
    viSentCom:=StrToFloat(VariablesUn.MWC_LiveData)*1000;
    //ComFm.GetMWCDisplayStatus(StrToInt(cmbUidIQ3.text),VariablesUn.MWC_LiveData,IntToStr(StrToInt(cmbChannel.Text)-1),rhMWCDisplayStatusRecieved);
    ComFm.GetMWCDisplayStatus(StrToInt(cmbUidIQ3.text),VariablesUn.MWC_LiveData,VariablesUn.gsChannelNum,rhIQ3DisplayStatusRecieved);
    while not PassLoopTimeOutFlag do
    begin
      PauseCountLoop:=PauseCountLoop+1;
      VariablesUn.delay(10);
      if PauseCountLoop=100 then
        ComFm.GetMWCDisplayStatus(StrToInt(cmbUidIQ3.text),(VariablesUn.MWC_LiveData),IntToStr(StrToInt(cmbChannel.Text)-1),rhIQ3DisplayStatusRecieved);
      if PauseCountLoop>300 then
      begin
        //ShowMessage('Failed Command: '+ VariablesUn.MWC_LiveData);
        Break;
      end;
    end;
    PassLoopTimeOutFlag:=False;
    PauseCountLoop:=0;
    viUID := StrToInt(cmbUidIQ3.text);
    viSentCom:=StrToFloat(VariablesUn.MWC_DayOrNight)*1000;
    //ComFm.GetMWCDisplayStatus(StrToInt(cmbUidIQ3.text),MWC_DayOrNight,IntToStr(StrToInt(cmbChannel.Text)-1),rhMWCDisplayStatusRecieved);
    ComFm.GetMWCDisplayStatus(StrToInt(cmbUidIQ3.text),MWC_DayOrNight,VariablesUn.gsChannelNum,rhIQ3DisplayStatusRecieved);
    while not PassLoopTimeOutFlag do
    begin
      PauseCountLoop:=PauseCountLoop+1;
      VariablesUn.delay(10);
      if PauseCountLoop=100 then
        ComFm.GetMWCDisplayStatus(StrToInt(cmbUidIQ3.text),MWC_DayOrNight,IntToStr(StrToInt(cmbChannel.Text)-1),rhIQ3DisplayStatusRecieved);
      if PauseCountLoop>300 then
      begin
        //ShowMessage('Failed Command: '+ MWC_DayOrNight);
        Break;
      end;
    end;
    PassLoopTimeOutFlag:=False;
    PauseCountLoop:=0;

  end;
end;

//Changes Color of Com Indicator lights
procedure TIQ3RFDisplayfm.rhPortOpend(Sender: TObject);
begin
  giComPortOn := True;
  if shpIQ3DisplayBackground <> nil then
  begin

    shpIQ3DisplayBackground.Pen.Color := clLime
  end;
end;
//Changes Color of Com Indicator lights
procedure TIQ3RFDisplayfm.rhPortClosed(Sender: TObject);
begin
  giComPortOn := False;
  if shpIQ3DisplayBackground<>nil then
  begin
    shpIQ3DisplayBackground.Pen.Color := clRed
  end;
end;

//Convert the shade postion Binary into the appropriate checkboxes
procedure TIQ3RFDisplayfm.ConvertShadePosToCB(LoopString:String);
var
  I:integer;
begin
  if(loopstring<>'-1') then
  begin
    for I := 4 to loopString.Length do
    Begin
      if (I=4) and (loopString[4]='1') then
      begin
        cbDown.Checked:=True;
      end
      else if (I=4) and (loopString[4]='0') then
      begin
        cbDown.Checked:=False;
      end;

      if (I=5) and (loopString[5]='1') then
      begin
        cb3.Checked:=True;
      end
      else if (I=5) and (loopString[5]='0') then
      begin
        cb3.Checked:=False;
      end;

      if (I=6) and (loopString[6]='1') then
      begin
        cb2.Checked:=True;
      end
      else if (I=6) and (loopString[6]='0') then
      begin
        cb2.Checked:=False;
      end;

      if (I=7) and (loopString[7]='1') then
      begin
        cb1.Checked:=True;
      end
      else if (I=7) and (loopString[7]='0') then
      begin
        cb1.Checked:=False;
      end;

      if (I=8) and (loopString[8]='1') then
      begin
        cbup.Checked:=True;
      end
      else if (I=8) and (loopString[8]='0') then
      begin
        cbup.Checked:=False;
      end;
    end;
  end;
end;
//Convert Check boxes to binary   ShadePosition
Function TIQ3RFDisplayfm.ConvertShadePosToBin():String;
var
  value,CurrentBin:string;
begin
  CurrentBin:= Dataun.dmDataModule.dsIQ3Channels.FieldByName('ShadePosition').AsString;
  Value:='';
  if(CurrentBin[1]='1')then
  begin
    Value:='1';
  end
  else
  begin
    Value:='0';
  End;
  if(CurrentBin[2]='1')then
  begin
    Value:=Value+'1';
  end
  else
  begin
    Value:=Value+'0';
  end;
  value:=value+'0';
  If(cbdown.Checked)then
  begin
    Value:=Value+'1';
  end
  else
  begin
    Value:=Value+'0';
  end;
  If(cb3.Checked)then
  begin
    Value:=Value+'1';
  end
  else
  begin
    Value:=Value+'0';
  end;
  If(cb2.Checked)then
  begin
    Value:=Value+'1';
  end
  else
  begin
    Value:=Value+'0';
  end;
  If(cb1.Checked)then
  begin
    Value:=Value+'1';
  end
  else
  begin
    Value:=Value+'0';
  end;

  If(cbup.Checked)then
  begin
    Value:=Value+'1';
  end
  else
  begin
    Value:=Value+'0';
  end;
  Result := Value;
end;
procedure TIQ3RFDisplayfm.btnCancelClick(Sender: TObject);
begin
  Hide;
  PassFlag:=True;
  if Assigned (rh_ShowIQ3) then
    rh_ShowIQ3(Self);
end;
procedure TIQ3RFDisplayfm.btnConfigClick(Sender: TObject);
begin
  VariablesUn.giTop := Top;
  VariablesUn.giLeft := Left;
  btupdate.Color:=ClBtnFace;
  IQ3Configfm.rh_ShowIQ3RFDisplay := rhShowIQ3RFDisplay;

  giLastChannel:=cmbChannel.ItemIndex;
  if(cmbUidIQ3.ItemIndex=-1) then
  begin
    giUIDItemIndex:= 0;
  end
  else
  begin
    giUIDItemIndex:= cmbUidIQ3.ItemIndex;
  end;

  Hide;
  IQ3Configfm.stIQ3unLoading:=true;
  IQ3Configfm.Show;
end;

procedure TIQ3RFDisplayfm.btnConfigMouseEnter(Sender: TObject);
begin
  pnlMenuIQ3.Visible := True;
end;

procedure TIQ3RFDisplayfm.btnControlMouseEnter(Sender: TObject);
begin
  pnlMenuIQ3.Visible := True;
end;

procedure TIQ3RFDisplayfm.btnExitClick(Sender: TObject);
begin

  gipauseLoopExit:=true;
  variablesUn.PassLoopFlag:=true;
  while not giloopExit do
  begin
    variablesUn.Delay(10);
  end;
  DataUn.dmDataModule.dsMotors.Filtered := False;
  DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
  DataUn.dmDataModule.dsIQ3s.Filtered := False;
  DataUn.dmDataModule.dsMNI.Filtered := False;
  DataUn.dmDataModule.dsMNI2.Filtered := False;
  DataUn.dmDataModule.dsIQMLC2.Filtered := False;
  DataUn.dmDataModule.dsPowerPanel.Filtered := False;

  DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
  DataUn.dmDataModule.dsIQ3s.SaveToFile(VariablesUn.gsDataPath + 'dsIQ3s.txt', dfXML);
  DataUn.dmDataModule.dsIQ3Channels.SaveToFile(VariablesUn.gsDataPath + 'dsIQ3Channels.txt', dfXML);
  DataUn.dmDataModule.dsMNI.SaveToFile(VariablesUn.gsDataPath + 'dsMNI.txt', dfXML);
  DataUn.dmDataModule.dsMNI2.SaveToFile(VariablesUn.gsDataPath + 'dsMNI2.txt', dfXML);
  DataUn.dmDataModule.dsIQMLC2.SaveToFile(VariablesUn.gsDataPath + 'dsIQMLC2.txt', dfXML);
  DataUn.dmDataModule.dsPowerPanel.SaveToFile(VariablesUn.gsDataPath + 'dsPowerPanel.txt', dfXML);
  pnlMenuIQ3.Visible := False;
  Application.Terminate;
end;

procedure TIQ3RFDisplayfm.btnExitMouseEnter(Sender: TObject);
begin
  pnlMenuIQ3.Visible := True;
end;

procedure TIQ3RFDisplayfm.btnFindClick(Sender: TObject);
begin
  VariablesUn.giTop := Top;
  VariablesUn.giLeft := Left;
  variablesUn.PassLoopFlag:=true;
  pnlMenuIQ3.Visible := False;
  IQ3RFDisplayfm.Hide;
  discoverfm.rh_showIQ3RFDisplay:= rhShowDisplayIQ3;
  DiscoverFm.Show;
end;

procedure TIQ3RFDisplayfm.btnFindMouseEnter(Sender: TObject);
begin
  pnlMenuIQ3.Visible := True;
end;

procedure TIQ3RFDisplayfm.rhShowDisplayIQ3(Sender: TObject);
begin
  IQ3RFDisplayfm.Show;
end;

procedure TIQ3RFDisplayfm.btnInfoClick(Sender: TObject);
begin
  pnlMenuIQ3.Visible := False;
  Hide;
  AboutFm.rh_ShowForm := rhShowDisplayIQ3;
  AboutFm.Show;
end;

procedure TIQ3RFDisplayfm.btnInfoMouseEnter(Sender: TObject);
begin
  pnlMenuIQ3.Visible := True;
end;

procedure TIQ3RFDisplayfm.btnLimitSettingClick(Sender: TObject);
begin
  pnlMenuIQ3.Visible := False;
  LimitSettingFm.rh_showmain := rhShowDisplayIQ3;
  Hide;
  LimitSettingFm.Show;
end;

procedure TIQ3RFDisplayfm.btnLimitSettingMouseEnter(Sender: TObject);
begin
  pnlMenuIQ3.Visible := True;
end;

procedure TIQ3RFDisplayfm.btnMainMCWClick(Sender: TObject);
begin
  Hide;
  if Assigned (rh_ShowMain) then
    rh_ShowMain(Self);
end;
procedure TIQ3RFDisplayfm.btnMCWMouseEnter(Sender: TObject);
begin
  pnlMenuIQ3.Visible := True;
end;

end.
