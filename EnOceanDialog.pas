unit EnOceanDialog;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons;

type
  TEnOceanDialog1fm = class(TForm)
    teEnOceanID: TEdit;
    btnOK: TBitBtn;
    procedure teEnOceanIDKeyPress(Sender: TObject; var Key: Char);

  private
    { Private declarations }
  public
    EnOceanID: string;          // added by you
    function Execute(): Boolean;
  end;

var
  EnOceanDialog1fm: TEnOceanDialog1fm;

implementation

{$R *.dfm}



procedure TEnOceanDialog1fm.teEnOceanIDKeyPress(Sender: TObject; var Key: Char);
begin
//(Key in [#8, '0'..'9'])then
  if not (Key in [#8, '0'..'9',#46]) then
  begin
    // Discard the key
    Key := #0;
  end;
end;

function TEnOceanDialog1fm.Execute: Boolean;
begin
  Result := (ShowModal = mrOK);
  EnOceanID := teEnOceanID.Text;
  teEnOceanID.Clear;
end;

end.
