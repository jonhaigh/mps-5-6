unit BluePrint;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, ComUn, VariablesUn, DiscoverUn,
  Vcl.Buttons;

type
  TPrintFm = class(TForm)
    Image1: TImage;
    Panel1: TPanel;
    Shape4: TShape;
    Shape3: TShape;
    Shape2: TShape;
    Shape5: TShape;
    Shape1: TShape;
    lblAlias: TLabel;
    lblProjectName: TLabel;
    pnlMenuBar: TPanel;
    Image3: TImage;
    btnExit: TSpeedButton;
    btnFind: TSpeedButton;
    btnMain: TSpeedButton;
    btnConfig: TSpeedButton;
    btnHelp: TSpeedButton;
    btnInfo: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure Image1MouseEnter(Sender: TObject);
    procedure pnlMenuBarMouseEnter(Sender: TObject);
    procedure pnlMenuBarMouseLeave(Sender: TObject);
    procedure btnMainMouseEnter(Sender: TObject);
    procedure btnConfigMouseEnter(Sender: TObject);
    procedure btnFindMouseEnter(Sender: TObject);
    procedure btnHelpClick(Sender: TObject);
    procedure btnInfoClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure btnMainClick(Sender: TObject);
    procedure btnConfigClick(Sender: TObject);
    procedure btnFindClick(Sender: TObject);
    procedure btnExitMouseEnter(Sender: TObject);
    procedure FormClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    rh_ShowMain: TNotifyEvent;
    procedure rhPortClosed(Sender: TObject);
    procedure rhPortOpend(Sender: TObject);
  end;

var
  PrintFm: TPrintFm;

implementation

{$R *.dfm}

procedure TPrintFm.btnConfigClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TPrintFm.btnConfigMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TPrintFm.btnHelpClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TPrintFm.btnInfoClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TPrintFm.FormClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TPrintFm.FormDestroy(Sender: TObject);
begin
  rh_ShowMain := nil;
end;

procedure TPrintFm.FormHide(Sender: TObject);
begin
  VariablesUn.giTop := Top;
  VariablesUn.giLeft := Left;
end;

procedure TPrintFm.FormShow(Sender: TObject);
begin
  Top := VariablesUn.giTop;
  Left := VariablesUn.giLeft;

  lblProjectName.Caption := VariablesUn.gsProjectName;
end;

procedure TPrintFm.Image1MouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TPrintFm.pnlMenuBarMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TPrintFm.pnlMenuBarMouseLeave(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TPrintFm.rhPortClosed(Sender: TObject);
begin
 Shape1.Brush.Color := clGray;
 Shape2.Brush.Color := clGray;
 Shape3.Brush.Color := clGray;
 Shape4.Brush.Color := clGray;
 Shape5.Brush.Color := clGray;
end;

procedure TPrintFm.rhPortOpend(Sender: TObject);
begin
 Shape1.Brush.Color := clLime;
 Shape2.Brush.Color := clLime;
 Shape3.Brush.Color := clLime;
 Shape4.Brush.Color := clLime;
 Shape5.Brush.Color := clLime;
end;

procedure TPrintFm.btnExitClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  Application.Terminate;
end;

procedure TPrintFm.btnExitMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TPrintFm.btnFindClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  Hide;
  DiscoverFm.Show;
end;

procedure TPrintFm.btnFindMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TPrintFm.btnMainClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  ProjectFm.Hide;
  if Assigned (rh_ShowMain) then
     rh_ShowMain(Self);
end;

procedure TPrintFm.btnMainMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

end.
