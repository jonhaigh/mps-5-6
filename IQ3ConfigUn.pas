unit IQ3ConfigUn;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.StrUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Buttons, Vcl.ExtCtrls, VariablesUn, ComUn,NewProjectUn, DataUn,
  Vcl.StdCtrls, Data.DB, Datasnap.DBClient, Math, Vcl.Imaging.pngimage,EnOceanDialog,
  Vcl.ComCtrls,DiscoverUn,AboutUn,OtlParallel,ScreenSetupUn, Vcl.Imaging.jpeg,
  JvComponentBase, JvCaptionButton,PowerPanelConfigUn,MotorMaintenanceDisplay;

type
    TIQ3Configfm = class(TForm)
    pnlMenuMwc: TPanel;
    btnMenuMcw1: TImage;
    cmbUidMWC: TComboBox;
    Label10: TLabel;
    teUIDMWC: TEdit;
    cmbChannelIQ3: TComboBox;
    Label62: TLabel;
    ScrollBox1: TScrollBox;
    pnLightParameters: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label12: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label52: TLabel;
    lblZGN: TLabel;
    teTH4MWC: TEdit;
    teTH3MWC: TEdit;
    teTH2MWC: TEdit;
    teTH1MWC: TEdit;
    teHysteresisMWC: TEdit;
    cbUpMWC: TCheckBox;
    CBPos1MWC: TCheckBox;
    cbPos2MWC: TCheckBox;
    cbPos3MWC: TCheckBox;
    cbDownMWC: TCheckBox;
    teZGNMWC: TEdit;
    pnlThreshNight: TPanel;
    Label6: TLabel;
    Label17: TLabel;
    Label57: TLabel;
    teNightTHMWC: TEdit;
    pnlConfig: TPanel;
    Label58: TLabel;
    Label59: TLabel;
    cbClosedLoopMWC: TCheckBox;
    Label24: TLabel;
    lbDeviceUIDMWC: TLabel;
    teDeviceUIDMWC: TEdit;
    cmbNightShadePos: TComboBox;
    Label9: TLabel;
    pnlNightTimers: TPanel;
    Label64: TLabel;
    Label14: TLabel;
    Label13: TLabel;
    Label18: TLabel;
    Label50: TLabel;
    teNightInMWC: TEdit;
    teNightOutMWC: TEdit;
    Label56: TLabel;
    cbRetractOption: TCheckBox;
    lbDeviceIDMWC: TLabel;
    teTH1MWCOut: TEdit;
    Label2: TLabel;
    Label53: TLabel;
    teTH2MWCOut: TEdit;
    teTH3MWCOut: TEdit;
    Label54: TLabel;
    teTH4MWCOut: TEdit;
    Label55: TLabel;
    Label11: TLabel;
    Label65: TLabel;
    Label66: TLabel;
    Label68: TLabel;
    cmbDeviceType: TComboBox;
    cmbSTSAC: TComboBox;
    cmbBindMethod: TComboBox;
    btBind: TButton;
    btUnBind: TButton;
    Shape14: TShape;
    cmbProjectMWC: TComboBox;
    Image5: TImage;
    Shape6: TShape;
    Shape7: TShape;
    Shape8: TShape;
    Shape9: TShape;
    Label1: TLabel;
    Label21: TLabel;
    lbTelegramCounter: TLabel;
    Shape13: TShape;
    Shape12: TShape;
    Shape11: TShape;
    Shape10: TShape;
    lblGroupZGN: TLabel;
    pnlSwitch: TPanel;
    Label20: TLabel;
    lblZGn1: TLabel;
    Label44: TLabel;
    lblSwPersonality: TLabel;
    lblTimeoutZGN1: TLabel;
    teZGN2Switch: TEdit;
    cmbSwitchPerson: TComboBox;
    teZGN2Timeout: TEdit;
    cmbISILSwitch: TComboBox;
    shpISLS: TShape;
    shpSwitchPersonality: TShape;
    shpUp: TShape;
    shpPS1: TShape;
    shpPS2: TShape;
    shpPS3: TShape;
    shpDN: TShape;
    shpRetractMode: TShape;
    shpClosedLoop: TShape;
    shpNightPos: TShape;
    lblRta: TLabel;
    teSwitchAutoReturn: TEdit;
    lblRtaUnit: TLabel;
    teNetSwitchUID2: TEdit;
    lblMasterUID1: TLabel;
    btUpdateParams: TPanel;
    shpSTSAC: TShape;
    Panel1: TPanel;
    cbBoundMWC: TCheckBox;
    pnlOccSensor: TPanel;
    Label60: TLabel;
    cmbOccPos: TComboBox;
    shpCmbOcc: TShape;
    teOccUnTime: TEdit;
    Label69: TLabel;
    Label72: TLabel;
    teOccTime: TEdit;
    Label73: TLabel;
    Label74: TLabel;
    Label75: TLabel;
    lblNoEpp: TLabel;
    btUpdateUIDIQ3: TPanel;
    Label76: TLabel;
    teZGNOcc: TEdit;
    pnlCmdType: TPanel;
    Label61: TLabel;
    shpFWDAddrConfig: TShape;
    cmbFwdAddrFormat: TComboBox;
    teAlias: TEdit;
    btnExit: TImage;
    btnSettings: TImage;
    btnInfo: TImage;
    btnFind: TImage;
    btnMenu: TImage;
    btnConfig: TImage;
    btnControl: TImage;
    JvCaptionButton1: TJvCaptionButton;
    btnLimitSetting: TImage;
    teZGN1Switch: TEdit;
    lblForwardEnabled: TLabel;
    cbForwardEnabled: TCheckBox;
    shpForwarding: TShape;
    lblZgn2: TLabel;
    pnlFirmware: TPanel;
    lbBaseCodeRevMWC: TLabel;
    lbFirmwareRevMWC: TLabel;
    Label43: TLabel;
    btDefaultA: TButton;
    btDefaultB: TButton;
    pnlFirmwareRevMWC: TPanel;
    pnlAdapterRevMWC: TPanel;
    pnBaseRevMWC: TPanel;
    pnlTimersDay: TPanel;
    Label71: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label49: TLabel;
    Label48: TLabel;
    Label19: TLabel;
    Label67: TLabel;
    teShadeDownMWC: TEdit;
    teShadeUpMWC: TEdit;
    teMTAInhibitMWC: TEdit;
    cmbDeviceSelector: TComboBox;
    procedure UpdateChannel();
    procedure disableEnableButtons(Enableflg: boolean);
    procedure rhPortOpend(Sender: TObject);
    procedure rhPortClosed(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormHide(Sender: TObject);
    procedure btnMenuMouseEnter(Sender: TObject);
    procedure btnMenuMcw1MouseEnter(Sender: TObject);
    procedure pnlMenuMwcMouseLeave(Sender: TObject);
    procedure btnMainMCWMouseEnter(Sender: TObject);
    procedure btnConfigMCWMouseEnter(Sender: TObject);
    procedure btnMCWMCWMouseEnter(Sender: TObject);
    procedure btnInfoMouseEnter(Sender: TObject);
    procedure btnExitMouseEnter(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure btnMainIQ3Click(Sender: TObject);
    Procedure showuid;
    procedure cmbChannelIQ3Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure rhIQ3StatusRecieved(sender: TObject);
    procedure cmbUidMWCChange(Sender: TObject);
    procedure btUpdateParamsClick(Sender: TObject);
    Function ConvertShadePosToBin():String;
    procedure ConvertShadePosToCB(LoopString,OCloop:String);
    procedure btUpdateUIDIQ31Click(Sender: TObject);
    procedure teUIDMWCKeyPress(Sender: TObject; var Key: Char);
    procedure teNightTHMWCKeyPress(Sender: TObject; var Key: Char);
    procedure teNightInMWCKeyPress(Sender: TObject; var Key: Char);
    procedure teNightOutMWCKeyPress(Sender: TObject; var Key: Char);
    procedure teMTAInhibitMWCKeyPress(Sender: TObject; var Key: Char);
    procedure teShadeDwnMWCKeyPress(Sender: TObject; var Key: Char);
    procedure teShadeUpMWCKeyPress(Sender: TObject; var Key: Char);
    procedure teTH1MWCKeyPress(Sender: TObject; var Key: Char);
    procedure teTH2MWCKeyPress(Sender: TObject; var Key: Char);
    procedure teTH3MWCKeyPress(Sender: TObject; var Key: Char);
    procedure teTH4MWCKeyPress(Sender: TObject; var Key: Char);
    procedure teHysteresisMWCKeyPress(Sender: TObject; var Key: Char);
    //procedure teNightPosMWCKeyPress(Sender: TObject; var Key: Char);
    procedure teDeviceUIDMWCKeyPress(Sender: TObject; var Key: Char);
    procedure teTH1MWCEnter(Sender: TObject);
    procedure teTH1MWCExit(Sender: TObject);
    procedure teTH2MWCEnter(Sender: TObject);
    procedure teTH2MWCExit(Sender: TObject);
    procedure teTH3MWCEnter(Sender: TObject);
    procedure teTH3MWCExit(Sender: TObject);
    procedure teTH4MWCEnter(Sender: TObject);
    procedure teTH4MWCExit(Sender: TObject);
    procedure teNightTHMWCEnter(Sender: TObject);
    procedure teNightTHMWCExit(Sender: TObject);
    procedure teNightInMWCEnter(Sender: TObject);
    procedure teNightInMWCExit(Sender: TObject);
    procedure teNightOutMWCEnter(Sender: TObject);
    procedure teNightOutMWCExit(Sender: TObject);
    procedure teMTAInhibitMWCEnter(Sender: TObject);
    procedure teMTAInhibitMWCExit(Sender: TObject);
    procedure teShadeDownMWCEnter(Sender: TObject);
    procedure teShadeDownMWCExit(Sender: TObject);
    procedure teShadeUpMWCEnter(Sender: TObject);
    procedure teShadeUpMWCExit(Sender: TObject);
    procedure teHysteresisMWCEnter(Sender: TObject);
    procedure teHysteresisMWCExit(Sender: TObject);
    procedure teDeviceUIDMWCEnter(Sender: TObject);
    procedure teDeviceUIDMWCExit(Sender: TObject);
    procedure ZGNMWCKeyPress(Sender: TObject; var Key: Char);
    procedure btDefaultAClick(Sender: TObject);
    procedure btDefaultBClick(Sender: TObject);
//    procedure btSaveChTemplateClick(Sender: TObject);
//    procedure btUpdateGroupClick(Sender: TObject);
//    procedure btCarryOverChToGroupClick(Sender: TObject);
    procedure cmbGroupChSelectorChange(Sender: TObject);
    //procedure cmbAvailChChange(Sender: TObject);
    procedure FormMouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure btBindClick(Sender: TObject);
    procedure btUnBindClick(Sender: TObject);
    procedure cmbSTSACChange(Sender: TObject);
    //procedure imgSetupClick(Sender: TObject);
    procedure rhShowMWC(Sender: TObject);
    procedure cmbDeviceSelectorChange(Sender: TObject);
    procedure btnFindClick(Sender: TObject);
    procedure btnFindMouseEnter(Sender: TObject);
    procedure btnBackClick(Sender: TObject);
    procedure btnInfoClick(Sender: TObject);
    procedure cmbProjectMWCChange(Sender: TObject);
    procedure SelectProject;
    Procedure ThreadLoop();
    procedure cbUpMWCClick(Sender: TObject);
    procedure CBPos1MWCClick(Sender: TObject);
    procedure cbPos2MWCClick(Sender: TObject);
    procedure cbPos3MWCClick(Sender: TObject);
    procedure cbDownMWCClick(Sender: TObject);
    procedure pnlSwitchClick(Sender: TObject);
    procedure cmbDeviceTypeChange(Sender: TObject);
    procedure cmbISILSwitchChange(Sender: TObject);
    procedure cmbSwitchPersonChange(Sender: TObject);
    procedure cbUpMWCMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure CBPos1MWCMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure cbPos2MWCMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure cbPos3MWCMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure cbDownMWCMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure cbRetractOptionMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure cbClosedLoopMWCMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure cmbNightShadePosChange(Sender: TObject);
    procedure cmbFwdAddrFormatChange(Sender: TObject);

    procedure Label20Click(Sender: TObject);
    procedure SaveAsCSV(myFileName: string; myChannelDataSet,myMWCDataSet: TDataSet);
    //procedure teNetSwitchUID1KeyPress(Sender: TObject; var Key: Char);
    procedure SwitchOccKeyPress(Sender: TObject; var Key: Char);
    procedure teNetSwitchUID2KeyPress(Sender: TObject; var Key: Char);
    procedure btUpdateParamsMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btUpdateParamsMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Button1Click(Sender: TObject);
    Procedure SortStringListUIDS(UIDList:TStringList);
    procedure btnSettingsClick(Sender: TObject);
    procedure txtAliasEnterKey(Sender: TObject);
    procedure teAliasDblClick(Sender: TObject);
    procedure teAliasKeyPress(Sender: TObject; var Key: Char);
    procedure btnMenuMouseLeave(Sender: TObject);
    procedure btnControlMouseEnter(Sender: TObject);
    procedure btnConfigMouseEnter(Sender: TObject);
    procedure JvCaptionButton1Click(Sender: TObject);
    procedure teAliasChange(Sender: TObject);
    procedure btnLimitSettingClick(Sender: TObject);
    procedure btnLimitSettingMouseEnter(Sender: TObject);
    procedure cbForwardEnabledMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private

    stIQ3LoadingParams:boolean;
//    stGroupChannels:Tstringlist;
    PassFlag:Boolean;
    giUID:Integer;
    procedure ComboBox_AutoWidth(const theComboBox: TCombobox);
    procedure WaitforResponseSet(UID:Integer;Command,MByte,LByte:String;rh:TNotifyEvent);
    procedure WaitforResponseStatus(UID:Integer;Command:String;rh:TNotifyEvent);
  public
    stIQ3unLoading:boolean;
    rh_ShowMain: TNotifyEvent;
    rh_ShowIQ3RFDisplay: TNotifyEvent;
    rh_showMNI2Config: TNotifyEvent;
    rh_showIQMLC2Config: TNotifyEvent;
    rh_GetMNI2VirtualMotorStatus: TNotifyEvent;
  procedure Show;

  function  ConvertZGN(ZGN:String):String;

    { Public declarations }
  end;

var
  IQ3Configfm: TIQ3Configfm;
  giSentCom: Extended;
  PauseCount, timerDelayUid:integer;
  EnableShadePos,vsSwitchBin:String;
  ChToUpdate:string;
  viUIDs : Array of Integer;
  viUIDOnShow : Array of Integer;
  IQ3PassLoopFlag:Boolean;
  vbDeviceTypeChange:Boolean;


implementation

{$R *.dfm}

uses MWCDefaultChooserUn, MWCDisplayUn, MNIConfigUn, LimitSettingUn,
  MWCConfigUn;

Procedure TIQ3Configfm.SortStringListUIDS(UIDList:TStringList);
var
  I:Integer;
  First,Second:Integer;
  Swap:Boolean;
  TempStr:String;
begin
  Swap:=true;
  while Swap do
  begin
    Swap:=False;
    for I := 0 to UIDList.Count-2 do
    begin
      First:=StrToInt(UIDList[I]);
      Second:=StrToInt(UIDList[I+1]);
      if First > Second then
      begin
        TempStr:=UIDList[I];
        UIDList[I]:=UIDList[I+1];
        UIDList[I+1]:=TempStr;
        Swap:=true;
      end;
    end;
  end;
end;

//Changes Color of Com Indicator lights
procedure TIQ3Configfm.rhPortOpend(Sender: TObject);
begin
 giComPortOn := True;
 if Shape14<>nil then
  begin
    Shape14.Pen.Color := clLime;
  end;
end;

//Changes Color of Com Indicator lights
procedure TIQ3Configfm.rhPortClosed(Sender: TObject);
begin
  giComPortOn := False;
  if Shape14<>nil then
  begin
    Shape14.Pen.Color := clLime;
  end;

end;

//return handler from other forms
procedure TIQ3Configfm.rhShowMWC(Sender: TObject);
begin
  IQ3Configfm.Show;
end;

procedure TIQ3Configfm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  rh_ShowMain := nil;
  btnExitClick(Self);
end;

//populates channel combobox
procedure TIQ3Configfm.FormCreate(Sender: TObject);
var i:integer;
begin
  for i := 1 to 8 do
    cmbChannelIQ3.Items.Add(IntToStr(i));
  cmbChannelIQ3.ItemIndex:=0;
//  stGroupChannels:=TStringList.Create;
  ComboBox_AutoWidth(cmbDeviceSelector);
end;

//Allows combox  dropdown to size to oversized string items
procedure TIQ3Configfm.ComboBox_AutoWidth(const theComboBox: TCombobox);
const
  HORIZONTAL_PADDING = 4;
var
  itemsFullWidth: integer;
  idx: integer;
  itemWidth: integer;
begin
  itemsFullWidth := 0;
  // get the max needed with of the items in dropdown state
  for idx := 0 to -1 + theComboBox.Items.Count do
  begin
    itemWidth := theComboBox.Canvas.TextWidth(theComboBox.Items[idx]);
    Inc(itemWidth, 2 * HORIZONTAL_PADDING);
    if (itemWidth > itemsFullWidth) then itemsFullWidth := itemWidth;
  end;
  itemsFullWidth:=itemsFullWidth+20;
    // set the width of drop down if needed
  if (itemsFullWidth > theComboBox.Width) then
  begin
    //check if there would be a scroll bar
    if theComboBox.DropDownCount < theComboBox.Items.Count then
      itemsFullWidth := itemsFullWidth + GetSystemMetrics(SM_CXVSCROLL);
    SendMessage(theComboBox.Handle, CB_SETDROPPEDWIDTH, itemsFullWidth, 0);
  end;
end;

procedure TIQ3Configfm.FormDestroy(Sender: TObject);
begin
  rh_ShowMain := nil;
end;

//hides form and stops the halt timer for MWC
procedure TIQ3Configfm.FormHide(Sender: TObject);
var
  I:Integer;
begin

  VariablesUn.delay(200);
  VariablesUn.giTop := Top;
  VariablesUn.giLeft := Left;
  Screen.Cursor := crDefault;
  IQ3PassLoopFlag:=true;
end;

//Allows form to scroll with mouse wheel
procedure TIQ3Configfm.FormMouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
var
  I: Integer;
begin
  Handled := PtInRect(ScrollBox1.ClientRect, ScrollBox1.ScreenToClient(MousePos));
  if Handled then
    for I := 1 to Mouse.WheelScrollLines do
    try
      if WheelDelta > 0 then
        ScrollBox1.Perform(WM_VSCROLL, SB_LINEUP, 0)
      else
        ScrollBox1.Perform(WM_VSCROLL, SB_LINEDOWN, 0);
    finally
      ScrollBox1.Perform(WM_VSCROLL, SB_ENDSCROLL, 0);
    end;
end;

procedure TIQ3Configfm.FormShow(Sender: TObject);
begin
  ComFm.rh_PortOpend1 := rhPortOpend;
  ComFm.rh_PortClosed1 := rhPortClosed;
  Top:=VariablesUn.giTop;
  Left := VariablesUn.giLeft;
  cmbDeviceSelector.ItemIndex:=2;

end;

procedure TIQ3Configfm.JvCaptionButton1Click(Sender: TObject);
begin
  Application.HelpContext(7);
end;

procedure TIQ3Configfm.btnSettingsClick(Sender: TObject);
begin
  ScreenSetupFm.rh_showmain := rhShowMWC;
  pnlMenumwc.Visible := False;
  IQ3Configfm.Hide;
  ScreenSetupFm.Show;
end;

procedure TIQ3Configfm.Label20Click(Sender: TObject);
begin

end;

//prep all data and record sets on initial load
procedure TIQ3Configfm.Show;
var
  i,II:integer;
begin

  IQ3PassLoopFlag:=false;
  cmbChannelIQ3.ItemIndex:=giLastChannel;
  Async(ThreadLoop).Await(Procedure
  begin
    PassFlag:=false;
  end) ;
  Visible := true;
  BringToFront;
  Setlength(viUIDs,0);
  cmbUidMWC.Clear;
  ShowUID;
  cmbChannelIQ3Change(nil);
//  UpdateGroupCB;
//  cmbGroupChSelectorChange(nil);
  cmbSTSACChange(nil);

  cmbProjectMWC.Items.clear;
  //Polpulate project if it exists
  if DataUn.dmDataModule.dsProject.State <> dsInactive then
  begin
    DataUn.dmDataModule.dsProject.Filtered := False;
    DataUn.dmDataModule.dsProject.First;
    while not DataUn.dmDataModule.dsProject.eof do
    begin
      if (DataUn.dmDataModule.dsProject.FieldByName('Last').AsInteger = 1) then
         cmbProjectMWC.Items.Insert(0, DataUn.dmDataModule.dsProject.FieldByName('ProjectName').AsString)
      else
         cmbProjectMWC.Items.Add(DataUn.dmDataModule.dsProject.FieldByName('ProjectName').AsString);

      DataUn.dmDataModule.dsProject.Next;
    end;
    DataUn.dmDataModule.dsProject.Filtered := False;
    DataUn.dmDataModule.dsProject.First;
    for II := 0 to cmbprojectmwc.Items.Count-1 do
    begin
      if cmbprojectmwc.text = VariablesUn.gsProjectName then
      begin
        break;
      end
      else
      begin
        cmbprojectmwc.ItemIndex:=II;
      end;
    end;
  End;
  cmbUIDMWC.ItemIndex:=giUIDItemIndex;
  cmbUidMWCChange(nil);
//  ComFm.SetMWCParams(StrToInt(cmbUIDmwc.text),MWC_HaltTimer,'0','60',rhMWCStatusRecieved);
//  VariablesUn.delay(200);
  stIQ3unLoading:=false;
end;

procedure TIQ3Configfm.btnMenuMouseEnter(Sender: TObject);
begin
  pnlMenuMwc.Visible := True;

end;

procedure TIQ3Configfm.btnMenuMouseLeave(Sender: TObject);
begin

end;

//Copies loaded channel template into another channel

//splits any string with delimiter='.'
procedure Split(Delimiter: Char; Str: string; ListOfStrings: TStrings) ;
begin
   ListOfStrings.Clear;
   ListOfStrings.Delimiter       := Delimiter;
   ListOfStrings.StrictDelimiter := True; // Requires D2006 or newer.
   ListOfStrings.DelimitedText   := Str;
end;

procedure TIQ3Configfm.disableEnableButtons(Enableflg: boolean);
begin
  if not(enableflg) then
  begin
    Scrollbox1.enabled:=false;
    cmbUidMWC.enabled:=false;
    teUIDMWC.enabled:=false;
    btUpdateUIDIQ3.enabled:=false;
    cmbSTSAC.enabled:=false;
    cmbChannelIQ3.enabled:=false;
    //cmbAvailCh.enabled:=false;
    cmbDeviceType.enabled:=false;
    cmbBindMethod.enabled:=false;
    btBind.enabled:=false;
    btUnBind.enabled:=false;
    btUpdateParams.enabled:=false;
  end
  else
  begin
    Scrollbox1.enabled:=true;
    cmbUidMWC.enabled:=true;
    teUIDMWC.enabled:=true;
    btUpdateUIDIQ3.enabled:=true;
    cmbSTSAC.enabled:=true;
    cmbChannelIQ3.Enabled:=true;
    //cmbAvailCh.enabled:=true;
    cmbDeviceType.enabled:=true;
    cmbBindMethod.enabled:=true;
    btBind.enabled:=true;
    btUnBind.enabled:=true;
    btUpdateParams.enabled:=true;
  end;
end;

//UPdates the current mwc's UID. First checks for duplicates
procedure TIQ3Configfm.btUpdateUIDIQ31Click(Sender: TObject);
var
  viHi, viLo, viNewID, viOldId:Integer;
  I,DuplicateFlg: Integer;
begin
  //Check UIDS
  if dmDataModule.DuplicateUIDCheck(teUIDMWC.Text) then
  begin
    ShowMEssage('Error: UID already exist. Choose Different ID.');
    exit;
  end;
  VariablesUn.PassLoopFlag:=True;
  IQ3PassLoopFlag:=true;
  Screen.Cursor := crHourGlass;
  disableEnableButtons(false);
  //IQ3Configfm.enabled:=false;
  if teUIDmwc.Text<>'' then
  begin
//    SetLength(viUIDs,0);
//    giSentCom := StrToFloat(VariablesUn.MWC_GetUIDs)*1000;
//
//    ComFm.GetMWCStatus(65280, VariablesUn.MWC_GetUIDs, rhMWCStatusRecieved);
//
//    try
//      //VariablesUn.delay(255);
//    except
//    on E: Exception do
//    begin
//    end;
//    end;
//    timerDelayUid:=0;
//    //VariablesUn.delay(250);
//    VariablesUn.delay(255);
//    While(timerDelayUid>0) do
//    begin
//      timerDelayUid:=0;
//      VariablesUn.delay(6000);
//    end;
//    //Set UID
//    DuplicateFlg:=0;
//
//    for I := 0 to Length(viUIDs)-1 do
//    begin
//      if(viUIDs[i]=StrToInt(teUIDMWC.text)) then
//      begin
//        DuplicateFlg:=1;
//        break;
//      end;
//    end;

    if(VariablesUn.isnumber(teUIDMWC.text))and(teUIDMWC.text<>VariablesUn.gsUIDCrntMWC)then
    begin
      viNewID := StrToInt(teUIDMWC.text);
      viHi := Hi(viNewID);
      viLo := Lo(viNewID);
      giUID := StrToInt(teUIDMWC.text);
      viOldId := StrToInt(VariablesUn.gsUIDCrntMWC);
      giSentCom := StrToInt(VariablesUn.UID_ChangeUID)*1000;
      ComFm.SetMotorStatus(viOldId, VariablesUn.UID_ChangeUID, 0, viHi, viLo, rhIQ3StatusRecieved);  //Sets UID
      while not PassFlag do
      begin
        PauseCount:=PauseCount+1;
        VariablesUn.delay(10);
        if PauseCount=100 then
          ComFm.SetMotorStatus(viOldId, VariablesUn.UID_ChangeUID, 0, viHi, viLo, rhIQ3StatusRecieved);  //Sets UID
        if PauseCount>300 then
        begin
          ShowMessage('Failed Command: '+VariablesUn.UID_ChangeUID);
          Break;
        end;
      end;
      if(PassFlag) then
      begin
        btUpdateUIDIQ3.Color:=clbtnface;
        teUIDMWC.Color:=clwhite;
      end
      else
      begin
        btUpdateUIDIQ3.Color:=clbtnface;
        teUIDMWC.Color:=clred;
      end;
      PassFlag:=False;
      PauseCount:=0;

    end;
//    else
//    begin
//      showMessage('UID Update Fail: Duplicate ID!');
//      btUpdateUIDMWC.Color:=clbtnface;
//      teUIDMWC.Color:=clwhite;
//    end;
  end
  else
  begin
    showMessage('UID Update Fail: No UID Entered!');
    btUpdateUIDIQ3.Color:=clbtnface;
    teUIDMWC.Color:=clwhite;
  end;
  //IQ3Configfm.enabled:=true;
  disableEnableButtons(true);
  Screen.Cursor := crDefault;
end;

procedure TIQ3Configfm.Button1Click(Sender: TObject);
begin
  btUpdateUIDIQ3.enabled:=true;
end;

procedure TIQ3Configfm.cbClosedLoopMWCMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  shpClosedLoop.visible:=true;
  shpClosedLoop.Pen.Color:=clgreen;
  btUpdateParams.Color:=clLime;
end;

procedure TIQ3Configfm.cbDownMWCClick(Sender: TObject);
begin
  if cbDownMWC.Checked=false then
  begin
    DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
    DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC+' and ChannelID = ' + IQ3Configfm.cmbChannelIQ3.Text;
    DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
    if (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring='1') then
    begin
      if not(cbUpMWC.Checked)and not(cbpos1MWC.Checked)and not(cbpos2MWC.Checked) and (cbpos3MWC.checked)or not(cbUpMWC.Checked)and not(cbpos1MWC.Checked)and(cbpos2MWC.Checked) and not(cbpos3MWC.checked)or not(cbUpMWC.Checked)and (cbpos1MWC.Checked)and not(cbpos2MWC.Checked) and not(cbpos3MWC.checked)or (cbUpMWC.Checked)and not(cbpos1MWC.Checked)and not(cbpos2MWC.Checked) and not(cbpos3MWC.checked)then
      begin
        showmessage('There must be at lest two positions checked');
        cbDownMWC.Checked:=true;
      end;
    end;
  end;
end;

procedure TIQ3Configfm.cbDownMWCMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  shpDN.Visible:=true;
  shpDN.Pen.Color:=clGreen;
  btUpdateParams.Color:=clLime;
end;


procedure TIQ3Configfm.cbForwardEnabledMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  shpForwarding.visible:=true;
  shpForwarding.Pen.Color:=clgreen;
  btUpdateParams.Color:=clLime;
end;

//procedure TIQ3Configfm.cbFwdAddrEnableMouseDown(Sender: TObject; Button: TMouseButton;
//  Shift: TShiftState; X, Y: Integer);
//begin
//  shpFWDAddr.visible:=true;
//  shpFWDAddr.Pen.Color:=clgreen;
//  btUpdateParams.Color:=clLime;
//end;

//Makes sure that at least 2 positions are enabled
procedure TIQ3Configfm.CBPos1MWCClick(Sender: TObject);
begin
  if CBPos1MWC.Checked=false then
  begin
    DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
    DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC+' and ChannelID = ' + IQ3Configfm.cmbChannelIQ3.Text;
    DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
    if (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring='1') then
    begin
      if not(cbUpMWC.Checked)and not(cbpos2MWC.Checked)and not(cbpos3MWC.Checked) and (cbDownMWC.checked)or not(cbUpMWC.Checked)and not(cbpos2MWC.Checked)and(cbpos3MWC.Checked) and not(cbDownMWC.checked)or not(cbUpMWC.Checked)and (cbpos2MWC.Checked)and not(cbpos3MWC.Checked) and not(cbDownMWC.checked)or (cbUpMWC.Checked)and not(cbpos2MWC.Checked)and not(cbpos3MWC.Checked) and not(cbDownMWC.checked)then
      begin
        showmessage('There must be at lest two positions checked');
        CBPos1MWC.Checked:=true;
      end;
    end;
  end;
end;

procedure TIQ3Configfm.CBPos1MWCMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  shpPS1.Visible:=true;
  shpPS1.Pen.Color:=clGreen;
  btUpdateParams.Color:=clLime;
end;

//UNbinds a device from a channel
procedure TIQ3Configfm.btUnBindClick(Sender: TObject);
var
  I: Integer;
  CurrentEnID:String;
begin
  //unbind channel
  DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
  DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMWC.text + ' and ChannelID = ' + cmbChannelIQ3.Text ;
  DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
  if(cbBoundMWC.Checked=True)and(Dataun.dmDataModule.dsIQ3Channels.FieldByName('Bound').AsBoolean)then
  Begin

    CurrentEnID:= Dataun.dmDataModule.dsIQ3Channels.FieldByName('DeviceUID').AsString;
    giUID := StrToInt(cmbUidMWC.text);
    VariablesUn.gbBoundSet:= False;
    giSentCom:=StrToFloat(VariablesUn.MWC_SetMWCBound)*1000;
    ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),MWC_SetMWCBound,'0',IntToStr(StrToInt(cmbChannelIQ3.Text)-1),rhIQ3StatusRecieved);
    while not PassFlag do
    begin
      PauseCount:=PauseCount+1;
      VariablesUn.delay(10);
      if PauseCount=100 then
        ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),MWC_SetMWCBound,'0',IntToStr(StrToInt(cmbChannelIQ3.Text)-1),rhIQ3StatusRecieved);
      if PauseCount>300 then
      begin
        ShowMessage('Failed Command: '+ VariablesUn.MWC_SetMWCBound);
        Break;
      end;
    end;
    PassFlag:=False;
    PauseCount:=0;
    for I := 1 to 8 do
    begin
      DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
      DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMWC.text + ' and ChannelID = ' + IntToStr(I) ;
      DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
      if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('Bound').AsBoolean)and(Dataun.dmDataModule.dsIQ3Channels.FieldByName('DeviceUID').AsString=CurrentEnID)then
      begin
        DataUn.dmDataModule.dsIQ3Channels.edit;
        Dataun.dmDataModule.dsIQ3Channels.FieldByName('Bound').AsBoolean:=false;
//        if cmbChannelIQ3.Text = Dataun.dmDataModule.dsIQ3Channels.FieldByName('ChanelID').as then
        DataUn.dmDataModule.dsIQ3Channels.MergeChangeLog;
      end;


    end;
    cmbChannelIQ3Change(nil);
  end
  else
  showMessage('Fail Update: Already unBound.');
end;


//refreshes current channel params from the MWC after a default has been sent
procedure TIQ3Configfm.UpdateChannel();
Var
  command:String;
  iii:Integer;
begin
  for iii := 0 to 7 do
  begin
//    if iii<>6 then
//    begin
      VariablesUn.ClearData;
      giSentCom := StrToFloat(VariablesUn.MWC_GetAllCHParams)*1000;
      ComFm.GetMWCDisplayStatus(giUID, VariablesUn.MWC_GetAllCHParams, intToStr(iii), rhIQ3StatusRecieved);
      //VariablesUn.delay(250);
      While(PassFlag=True)do
      begin
        VariablesUn.delay(10);
        PassFlag:=False;
      end;
      PassFlag:=False;
      PauseCount:=0;
      while not PassFlag do
      begin
        PauseCount:=PauseCount+1;
        VariablesUn.delay(10);
        if PauseCount=100 then
          ComFm.GetMWCDisplayStatus(giUID, VariablesUn.MWC_GetAllCHParams, intToStr(iii), rhIQ3StatusRecieved);
        if PauseCount>300 then
        begin
          ShowMessage('Failed Command: '+FloatToStr(giSentCom/1000));
          Break;
        end;
      end;
      PassFlag:=False;
      PauseCount:=0;
//    end;
  end;
  //Get Channel Params
//  for iii := 0 to 14 do
//  begin
//    if iii<>6 then
//    begin
//      VariablesUn.ClearData;
//      giSentCom := StrToFloat(VariablesUn.MWC_ChannelStart+IntToStr((iii*16)+(StrToInt(IQ3Configfm.cmbChannelIQ3.text)-1)))*1000;
//      command := VariablesUn.MWC_ChannelStart+IntToStr((iii*16)+(StrToInt(IQ3Configfm.cmbChannelIQ3.text)-1));
//      ComFm.GetMWCStatus(giUID, command, rhIQ3StatusRecieved);
//      //VariablesUn.delay(250);
//      While(PassFlag=True)do
//      begin
//        VariablesUn.delay(10);
//        PassFlag:=False;
//      end;
//      PassFlag:=False;
//      PauseCount:=0;
//      while not PassFlag do
//      begin
//        PauseCount:=PauseCount+1;
//        VariablesUn.delay(10);
//        if PauseCount=100 then
//          ComFm.GetMWCStatus(giUID, command, rhIQ3StatusRecieved);
//        if PauseCount>300 then
//        begin
//          ShowMessage('Failed Command: '+FloatToStr(giSentCom/1000));
//          Break;
//        end;
//      end;
//      PassFlag:=False;
//      PauseCount:=0;
//    end;
//  end;
end;



//Update Channel Parameters
procedure TIQ3Configfm.btUpdateParamsClick(Sender: TObject);
var
  viHi, viLo, viNewID, viOldId,giHByte,giLByte:Integer;
  OutPutList: TStringList;
  EnableMessageflg,EnableShadePos,ChannelMath,ZGNCommand,GroupChBin,HByte,LByte, GroupNum, GroupZGNCom, GroupPos, GroupCh, GroupChInfoCom:string;
  vsZGN,vsF12SwitchBin,AliasText:String;
  SetZGNFlg:boolean;
  viZGNSeg1,viZGNSeg2,viZGNSeg3,i,ii,FwdAddrFormat,liFwdAddrFormatMask,NightPos,viF12SwitchInt:Integer;
begin
  SetZGNFlg:=false;
  Screen.Cursor := crHourGlass;
  disableEnableButtons(false);
  VariablesUn.PassLoopFlag:=True;
  PassFlag:=False;
  PauseCount:=0;
  OutPutList := TStringList.Create;
  ChToUpdate:=IQ3Configfm.cmbChannelIQ3.Text;
  DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
  DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMWC.text + ' and ChannelID = ' + cmbChannelIQ3.Text ;
  DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
  OutPutList.Clear;
  //Set ZGNs
  //If Daylight Sensor
  if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('paramID').AsString='1')then
  begin
    Split('.', Trim(teZGNMWC.Text), OutPutList);
    if VariablesUn.IsG('.', Trim(teZGNMWC.Text), OutPutList)then
    begin
      viZGNSeg1:=255;
      viZGNSeg3:=255;
      viZGNSeg2:=StrToInt(OutputList[0]);
    end
    else if VariablesUn.IsZGN(teZGNMWC.Text) then
    begin
      viZGNSeg1:=StrToInt(OutputList[0]);
      viZGNSeg2:=StrToInt(OutputList[1]);
      viZGNSeg3:=StrToInt(OutputList[2]);

    end
    else
      showmessage('ZGN Fail: Improper Format xx.xx.xx or xx');
    vsZGN:=IntToStr(viZGNSeg1)+'.'+IntToStr(viZGNSeg2)+'.'+IntToStr(viZGNSeg3);
  end   //If Switch
  else if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='2')then
  begin
    Split('.', Trim(teZGN2Switch.Text), OutPutList);
    if VariablesUn.IsG('.', Trim(teZGN2Switch.Text), OutPutList)then
    begin
      viZGNSeg1:=255;
      viZGNSeg3:=255;
      viZGNSeg2:=StrToInt(OutputList[0]);
    end
    else if VariablesUn.IsZGN(teZGN2Switch.Text) then
    begin
      viZGNSeg1:=StrToInt(OutputList[0]);
      viZGNSeg2:=StrToInt(OutputList[1]);
      viZGNSeg3:=StrToInt(OutputList[2]);
    end
    else
      showmessage('ZGN Fail: Improper Format xx.xx.xx or xx');

    vsZGN:=IntToStr(viZGNSeg1)+'.'+IntToStr(viZGNSeg2)+'.'+IntToStr(viZGNSeg3);
    if(vsZGN<>'-') and not(OutPutList.Count=2) and not(OutPutList.Count=0) and(LastDelimiter('.',vsZGN)<>Length(vsZGN))and(ansipos('.',vsZGN)<>1)and(vsZGN<>Dataun.dmDataModule.dsIQ3Channels.FieldByName('ZGN2').AsString)then
    begin
      case StrToInt(cmbChannelIQ3.text) of
        1:
        begin
          ZGNCommand:= UID_LS2_BUS1;
        end;
        2:
        begin
          ZGNCommand:= UID_LS2_BUS2;
        end;
        3:
        begin
          ZGNCommand:= UID_LS2_BUS3;
        end;
        4:
        begin
          ZGNCommand:= UID_LS2_BUS4;
        end;
        5:
        begin
          ZGNCommand:= UID_LS2_BUS5;
        end;
        6:
        begin
          ZGNCommand:= UID_LS2_BUS6;
        end;
        7:
        begin
          ZGNCommand:= UID_LS2_BUS7;
        end;
        8:
        begin
          ZGNCommand:= UID_LS2_BUS8;
        end;

      end;
      giUID := StrToInt(cmbUidMWC.text);
      giSentCom:=StrToFloat(ZGNCommand)*1000;
      //giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ZGNCommand)*1000;
      ComFm.SetMotorStatus(StrToInt(VariablesUn.gsUIDCrntMWC),ZGNCommand,viZGNSeg1,viZGNSeg2,viZGNSeg3,IQ3Configfm.rhIQ3StatusRecieved);
      while not PassFlag do
      begin
        PauseCount:=PauseCount+1;
        VariablesUn.delay(10);
        if PauseCount=100 then
          ComFm.SetMotorStatus(StrToInt(VariablesUn.gsUIDCrntMWC),ZGNCommand,viZGNSeg1,viZGNSeg2,viZGNSeg3,IQ3Configfm.rhIQ3StatusRecieved);
        if PauseCount>300 then
        begin
          teZGN2Switch.Color:=clred;
          btUpdateParams.Color:=clRed;
          //ShowMessage('Failed Command: '+ZGNCommand);
          Break;
        end;
      end;
      if(PassFlag)then
      begin
        teZGN2Switch.Color:=clwhite;
        btUpdateParams.Color:=clbtnface;
      end
      else
      begin
        teZGN2Switch.Color:=clred;
        btUpdateParams.Color:=clRed;
      end;
      PassFlag:=False;
      PauseCount:=0;
      OutPutList.Clear;
    end;
//    Split('.', Trim(teZGN1Switch.Text), OutPutList);
//    if VariablesUn.IsG('.', Trim(teZGN1Switch.Text), OutPutList)then
//    begin
//      viZGNSeg1:=255;
//      viZGNSeg3:=255;
//      viZGNSeg2:=StrToInt(OutputList[0]);
//    end
//    else if VariablesUn.IsZGN(teZGN1Switch.Text) then
//    begin
//      viZGNSeg1:=StrToInt(OutputList[0]);
//      viZGNSeg2:=StrToInt(OutputList[1]);
//      viZGNSeg3:=StrToInt(OutputList[2]);
//    end
//    else
//      showmessage('ZGN Fail: Improper Format xx.xx.xx or xx');
//
//    vsZGN:=IntToStr(viZGNSeg1)+'.'+IntToStr(viZGNSeg2)+'.'+IntToStr(viZGNSeg3);
  end
  else if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='4') or (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='5') or (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='6') or (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='7')then
  begin
    Split('.', Trim(teZGN2Switch.Text), OutPutList);
    if VariablesUn.IsG('.', Trim(teZGN2Switch.Text), OutPutList)then
    begin
      viZGNSeg1:=255;
      viZGNSeg3:=255;
      viZGNSeg2:=StrToInt(OutputList[0]);
    end
    else if VariablesUn.IsZGN(teZGN2Switch.Text) then
    begin
      viZGNSeg1:=StrToInt(OutputList[0]);
      viZGNSeg2:=StrToInt(OutputList[1]);
      viZGNSeg3:=StrToInt(OutputList[2]);
    end
    else
      showmessage('ZGN Fail: Improper Format xx.xx.xx or xx');

    vsZGN:=IntToStr(viZGNSeg1)+'.'+IntToStr(viZGNSeg2)+'.'+IntToStr(viZGNSeg3);
    if(vsZGN<>'-') and not(OutPutList.Count=2) and not(OutPutList.Count=0) and(LastDelimiter('.',vsZGN)<>Length(vsZGN))and(ansipos('.',vsZGN)<>1)and(vsZGN<>Dataun.dmDataModule.dsIQ3Channels.FieldByName('ZGN2').AsString)then
    begin
      case StrToInt(cmbChannelIQ3.text) of
        1:
        begin
          ZGNCommand:= UID_LS2_BUS1;
        end;
        2:
        begin
          ZGNCommand:= UID_LS2_BUS2;
        end;
        3:
        begin
          ZGNCommand:= UID_LS2_BUS3;
        end;
        4:
        begin
          ZGNCommand:= UID_LS2_BUS4;
        end;
        5:
        begin
          ZGNCommand:= UID_LS2_BUS5;
        end;
        6:
        begin
          ZGNCommand:= UID_LS2_BUS6;
        end;
        7:
        begin
          ZGNCommand:= UID_LS2_BUS7;
        end;
        8:
        begin
          ZGNCommand:= UID_LS2_BUS8;
        end;

      end;
      giUID := StrToInt(cmbUidMWC.text);
      giSentCom:=StrToFloat(ZGNCommand)*1000;
      //giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ZGNCommand)*1000;
      ComFm.SetMotorStatus(StrToInt(VariablesUn.gsUIDCrntMWC),ZGNCommand,viZGNSeg1,viZGNSeg2,viZGNSeg3,IQ3Configfm.rhIQ3StatusRecieved);
      while not PassFlag do
      begin
        PauseCount:=PauseCount+1;
        VariablesUn.delay(10);
        if PauseCount=100 then
          ComFm.SetMotorStatus(StrToInt(VariablesUn.gsUIDCrntMWC),ZGNCommand,viZGNSeg1,viZGNSeg2,viZGNSeg3,IQ3Configfm.rhIQ3StatusRecieved);
        if PauseCount>300 then
        begin
          teZGN2Switch.Color:=clred;
          btUpdateParams.Color:=clRed;
          //ShowMessage('Failed Command: '+ZGNCommand);
          Break;
        end;
      end;
      if(PassFlag)then
      begin
        teZGN2Switch.Color:=clwhite;
        btUpdateParams.Color:=clbtnface;
      end
      else
      begin
        teZGN2Switch.Color:=clred;
        btUpdateParams.Color:=clRed;
      end;
      PassFlag:=False;
      PauseCount:=0;
      OutPutList.Clear;
    end;

  end  //If Occupancy Sensor
  else if Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='3' then
  begin
    Split('.', Trim(teZGNOCC.Text), OutPutList);
    if VariablesUn.IsG('.', Trim(teZGNOCC.Text), OutPutList)then
    begin
      viZGNSeg1:=255;
      viZGNSeg3:=255;
      viZGNSeg2:=StrToInt(OutputList[0]);
    end
    else if VariablesUn.IsZGN(teZGNOCC.Text) then
    begin
      viZGNSeg1:=StrToInt(OutputList[0]);
      viZGNSeg2:=StrToInt(OutputList[1]);
      viZGNSeg3:=StrToInt(OutputList[2]);
    end
    else
      showmessage('ZGN Fail: Improper Format xx.xx.xx or xx');
    vsZGN:=IntToStr(viZGNSeg1)+'.'+IntToStr(viZGNSeg2)+'.'+IntToStr(viZGNSeg3);
  end
  else
  begin
    teZGN2Switch.Color:=clwhite;
    btupdateParams.Color:=clbtnface;
  end;
  //Send ZGN1
//  if(vsZGN<>'-')and not(OutPutList.Count=2) and not(OutPutList.Count=0) and(LastDelimiter('.',vsZGN)<>Length(vsZGN))and(ansipos('.',vsZGN)<>1)and(vsZGN<>Dataun.dmDataModule.dsIQ3Channels.FieldByName('ZGN').AsString)then
//  begin
//    case StrToInt(cmbChannelIQ3.text) of
//        1:
//        begin
//          ZGNCommand:= UID_LS2_BUS1;
//        end;
//        2:
//        begin
//          ZGNCommand:= UID_LS2_BUS2;
//        end;
//        3:
//        begin
//          ZGNCommand:= UID_LS2_BUS3;
//        end;
//        4:
//        begin
//          ZGNCommand:= UID_LS2_BUS4;
//        end;
//        5:
//        begin
//          ZGNCommand:= UID_LS2_BUS5;
//        end;
//        6:
//        begin
//          ZGNCommand:= UID_LS2_BUS6;
//        end;
//        7:
//        begin
//          ZGNCommand:= UID_LS2_BUS7;
//        end;
//        8:
//        begin
//          ZGNCommand:= UID_LS2_BUS8;
//        end;
//    end;
//
//
//    giUID := StrToInt(cmbUidMWC.text);
//    giSentCom:=StrToFloat(ZGNCommand)*1000;
//    ComFm.SetMotorStatus(StrToInt(VariablesUn.gsUIDCrntMWC),ZGNCommand,viZGNSeg1,viZGNSeg2,viZGNSeg3,IQ3Configfm.rhIQ3StatusRecieved);
//    while not PassFlag do
//    begin
//      PauseCount:=PauseCount+1;
//      VariablesUn.delay(10);
//      if PauseCount=100 then
//        ComFm.SetMotorStatus(StrToInt(VariablesUn.gsUIDCrntMWC),ZGNCommand,viZGNSeg1,viZGNSeg2,viZGNSeg3,IQ3Configfm.rhIQ3StatusRecieved);
//      if PauseCount>300 then
//      begin
//        Break;
//      end;
//    end;
//    if(PassFlag)then
//    begin
//      teZGNOcc.Color:=clwhite;
//      teZGN1Switch.Color:=clwhite;
//      teZGNMWC.Color:=clwhite;
//      btUpdateParams.Color:=clBtnface;
//      if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='1')then
//      begin
//        SetZGNFlg:=true;
//      end;
//    end
//    else
//    begin
//      teZGNOcc.Color:=clred;
//      teZGN1Switch.Color:=clred;
//      teZGNMWC.Color:=clred;
//      btUpdateParams.Color:=clRed;
//      if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='1')then
//      begin
//        SetZGNFlg:=false;
//      end;
//    end;
//    PassFlag:=False;
////    PauseCount:=0;
//  end
//  else
//  begin
//    teZGNOcc.Color:=clwhite;
//    teZGN1Switch.Color:=clwhite;
//    teZGNMWC.Color:=clwhite;
//    btupdateParams.Color:=clbtnface;
//  end;

  //Alias
  DataUn.dmDataModule.dsIQ3s.Filtered := False;
  DataUn.dmDataModule.dsIQ3s.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMWC.text;
  DataUn.dmDataModule.dsIQ3s.Filtered := True;

  if(teAlias.Text<>DataUn.dmDataModule.dsIQ3s.FieldByName('Alias').AsString) then
  begin
    giUID := StrToInt(cmbUidMWC.text);
    AliasText:= teAlias.text;
    AliasText := AliasText + StringOfChar ( Char(#32), 12 - Length(AliasText) );
    VariablesUn.ClearData;
    giSentCom := StrToFloat(VariablesUn.UID_NAME)*1000;
    ComFm.SetGenericParam(StrToInt(VariablesUn.gsUIDCrntMWC), VariablesUn.UID_NAME+'.'+AliasText, rhIQ3StatusRecieved);
    while not PassFlag do
    begin
      PauseCount:=PauseCount+1;
      VariablesUn.delay(10);
      if PauseCount=100 then
        ComFm.SetGenericParam(StrToInt(VariablesUn.gsUIDCrntMWC), VariablesUn.UID_NAME+'.'+AliasText, rhIQ3StatusRecieved);
      if PauseCount>300 then
      begin
        Break;
      end;
    end;
    if(PassFlag)then
    begin
      teAlias.Color:=clWhite;
    end
    else
    begin
      teAlias.Color:=clRed;
    end;

  end
  else
  begin
    teAlias.Brush.Color:=clWhite;
  end;
  //Set Forward address Format and/or enable messages
  DataUn.dmDataModule.dsIQ3s.Filtered := False;
  DataUn.dmDataModule.dsIQ3s.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMWC.text;
  DataUn.dmDataModule.dsIQ3s.Filtered := True;
  //if(cbEnableMessage.Checked=True)then
  if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='1')or (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='3')then
  begin
    if(cmbSTSAC.text='SAC Mode')then
    begin
      EnableMessageflg:='1'
    end
    else
    begin
      EnableMessageflg:='0'
    end;
    if(cmbFwdAddrFormat.text<>Dataun.dmDataModule.dsIQ3s.FieldByName('FwdAddrFormat').AsString)or(EnableMessageflg<>Dataun.dmDataModule.dsIQ3s.FieldByName('EnableMessages').AsString)then
    Begin
      giUID := StrToInt(cmbUidMWC.text);
      giSentCom:=StrToFloat(VariablesUn.MWC_FwdAddrFormat)*1000;
      if(cmbFwdAddrFormat.Text='LS')then
      begin
        FwdAddrFormat:=VariablesUn.BinToInt('0000000'+EnableMessageflg);
      end
      else if(cmbFwdAddrFormat.Text='IS')then
      begin
        FwdAddrFormat:=VariablesUn.BinToInt('1000000'+EnableMessageflg);
      end
      else if(cmbFwdAddrFormat.Text='SS')then
      begin
        FwdAddrFormat:=VariablesUn.BinToInt('1100000'+EnableMessageflg);
      end;
      liFwdAddrFormatMask:=Dataun.dmDataModule.dsIQ3s.FieldByName('dataH3').AsInteger and $3E;
      FwdAddrFormat:=liFwdAddrFormatMask or FwdAddrFormat;
      ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),MWC_FwdAddrFormat,'0', IntToStr(FwdAddrFormat),rhIQ3StatusRecieved);
      while not PassFlag do
      begin
        PauseCount:=PauseCount+1;
        VariablesUn.delay(10);
        if PauseCount=100 then
          ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),MWC_FwdAddrFormat,'0', IntToStr(FwdAddrFormat),rhIQ3StatusRecieved);
        if PauseCount>300 then
        begin
          Break;
        end;
      end;
      if(PassFlag)then
      begin
        shpSTSAC.Visible:=False;
        shpSTSAC.Pen.color:=clgreen;
        shpFWDAddrConfig.Visible:=False;
        shpISLS.Visible:=False;
        shpISLS.pen.Color:=clGreen;
        shpFWDAddrConfig.pen.Color:=clgreen;
        btUpdateParams.Color:=clBtnface;
      end
      else
      begin
        shpISLS.Pen.Color:=clred;
        shpSTSAC.Pen.Color:=clred;
        shpSTSAC.Update;
        shpSTSAC.refresh;
        shpFWDAddrConfig.Pen.Color:=clred;
        btUpdateParams.Color:=clRed;
      end;
      PassFlag:=False;
      PauseCount:=0;
    End
    else
    begin
      shpISLS.Visible:=False;
      shpFWDAddrConfig.Visible:=False;
      shpISLS.Pen.Color:=clGreen;
      shpFWDAddrConfig.Pen.Color:=clgreen;
    end;
  end;
  if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='2') then
  begin
    vsSwitchBin:=IntToBin(StrToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('OCLoop').asstring));
    if(cmbISILSwitch.Text='IS')then
    begin
      vsF12SwitchBin:='1'
    end
    else if(cmbISILSwitch.Text='LS')then
    begin
      vsF12SwitchBin:='0'
    end;
    vsF12SwitchBin:=vsF12SwitchBin+'00000'+vsSwitchBin[7];
    vsF12SwitchBin:=vsF12SwitchBin+'1';

    viF12SwitchInt:=VariablesUn.BinToInt(vsF12SwitchBin);
    if(IntToStr(viF12SwitchInt)<>Dataun.dmDataModule.dsIQ3Channels.FieldByName('OCLoop').AsString)then
    Begin
      giUID := StrToInt(cmbUidMWC.text);
      ChannelMath:=IntToStr((StrToInt(IQ3Configfm.cmbChannelIQ3.Text)-1)+192);
      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
      ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),VariablesUn.MWC_ChannelStart+ChannelMath,'0', IntToStr(viF12SwitchInt),rhIQ3StatusRecieved);
      while not PassFlag do
      begin
        PauseCount:=PauseCount+1;
        VariablesUn.delay(10);
        if PauseCount=100 then
          ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),MWC_FwdAddrFormat,'0', IntToStr(FwdAddrFormat),rhIQ3StatusRecieved);
        if PauseCount>300 then
        begin
          shpISLS.Pen.Color:=CLRed;
          //shpFWDAddr.Pen.Color:=clred;
          btUpdateParams.Color:=clRed;
          //ShowMessage('Failed Command: '+ VariablesUn.MWC_FwdAddrFormat);
          Break;
        end;
      end;
      if(PassFlag)then
      begin
        shpSTSAC.Visible:=false;
        shpISLS.Visible:=False;
        //shpFWDAddr.Visible:=False;
        shpSTSAC.Pen.Color:=clGreen;
        shpISLS.Pen.Color:=clGreen;
        //shpFWDAddr.Pen.Color:=clGreen;
        btUpdateParams.Color:=clBtnface;
      end
      else
      begin
        shpSTSAC.Pen.Color:=clred;
        shpISLS.pen.Color:=clred;
        //shpFWDAddr.pen.Color:=clred;
        btUpdateParams.Color:=clRed;
      end;
      PassFlag:=False;
      PauseCount:=0;
    End
    else
    begin
      shpSTSAC.Visible:=false;
      shpISLS.Visible:=False;
      //shpFWDAddr.Visible:=False;
      shpSTSAC.Pen.Color:=clGreen;
      shpISLS.pen.Color:=clGreen;
      //shpFWDAddr.pen.Color:=clGreen;
    end;
  end
  else if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='4') or (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='5') or (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='6') or (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='7') then
  begin
    vsSwitchBin:=IntToBin(StrToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('OCLoop').asstring));
    if(cmbISILSwitch.Text='IS')then
    begin
      vsF12SwitchBin:='1'
    end
    else if(cmbISILSwitch.Text='LS')then
    begin
      vsF12SwitchBin:='0'
    end;
    vsF12SwitchBin:=vsF12SwitchBin+'00000'+vsSwitchBin[7];
    if cbForwardEnabled.Checked then
    begin
      vsF12SwitchBin:=vsF12SwitchBin+'1';
    end
    else
    begin
      vsF12SwitchBin:=vsF12SwitchBin+'0';
    end;

    viF12SwitchInt:=VariablesUn.BinToInt(vsF12SwitchBin);
    if(IntToStr(viF12SwitchInt)<>Dataun.dmDataModule.dsIQ3Channels.FieldByName('OCLoop').AsString)then
    Begin
      giUID := StrToInt(cmbUidMWC.text);
      ChannelMath:=IntToStr((StrToInt(IQ3Configfm.cmbChannelIQ3.Text)-1)+192);
      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
      ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),VariablesUn.MWC_ChannelStart+ChannelMath,'0', IntToStr(viF12SwitchInt),rhIQ3StatusRecieved);
      while not PassFlag do
      begin
        PauseCount:=PauseCount+1;
        VariablesUn.delay(10);
        if PauseCount=100 then
          ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),MWC_FwdAddrFormat,'0', IntToStr(FwdAddrFormat),rhIQ3StatusRecieved);
        if PauseCount>300 then
        begin
          shpISLS.Pen.Color:=CLRed;
          shpForwarding.Pen.Color:=CLRed;
          //shpFWDAddr.Pen.Color:=clred;
          btUpdateParams.Color:=clRed;
          //ShowMessage('Failed Command: '+ VariablesUn.MWC_FwdAddrFormat);
          Break;
        end;
      end;
      if(PassFlag)then
      begin
        shpSTSAC.Visible:=false;
        shpISLS.Visible:=false;
        shpForwarding.Visible:=false;
        //shpFWDAddr.Visible:=False;
        shpSTSAC.Pen.Color:=clGreen;
        shpISLS.Pen.Color:=clGreen;
        shpForwarding.Pen.Color:=clGreen;
        //shpFWDAddr.Pen.Color:=clGreen;
        btUpdateParams.Color:=clBtnface;
      end
      else
      begin
        shpSTSAC.Pen.Color:=clred;
        shpISLS.pen.Color:=clred;
        shpForwarding.Pen.Color:=clRed;
        //shpFWDAddr.pen.Color:=clred;
        btUpdateParams.Color:=clRed;
      end;
      PassFlag:=False;
      PauseCount:=0;
    End
    else
    begin
      shpSTSAC.Visible:=false;
      shpISLS.Visible:=False;
      shpForwarding.Visible:=False;
      shpForwarding.Pen.Color:=clGreen;
      //shpFWDAddr.Visible:=False;
      shpSTSAC.Pen.Color:=clGreen;
      shpISLS.pen.Color:=clGreen;
      //shpFWDAddr.pen.Color:=clGreen;
    end;
  end;
  //Set Threshhold 1
  DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
  DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMWC.text + ' and ChannelID = ' + cmbChannelIQ3.Text ;
  DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
  if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString<>'2') and (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString<>'4') and (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString<>'5') and (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString<>'6') and (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString<>'7')then
  begin
    if(teTH1MWC.Text<>'-')then
    begin
      if(teTH1MWC.Text<>Dataun.dmDataModule.dsIQ3Channels.FieldByName('Threshold1').AsString)and(StrToInt(teTH1MWC.Text)>0)then
      Begin
        giUID := StrToInt(cmbUidMWC.text);
        viHi := Hi(StrToInt(teTH1MWC.Text));
        viLo := Lo(StrToInt(teTH1MWC.Text));
        ChannelMath:=IntToStr(StrToInt(IQ3Configfm.cmbChannelIQ3.Text)-1);
        giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
        ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3StatusRecieved);
        while not PassFlag do
        begin
          PauseCount:=PauseCount+1;
          VariablesUn.delay(10);
          if PauseCount=100 then
            ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3StatusRecieved);
          if PauseCount>300 then
          begin
            teTH1MWC.Color:=clred;
            btUpdateParams.Color:=clRed;
            //ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
            Break;
          end;
        end;
        if(PassFlag)then
        begin
          teTH1MWC.Color:=clWhite;
          btUpdateParams.Color:=clBtnface;
        end
        else
        begin
          teTH1MWC.Color:=clred;
          btUpdateParams.Color:=clRed;
        end;
        PassFlag:=False;
        PauseCount:=0;
      End
      else
      begin
        teTH1MWC.Color:=clwhite;
        btupdateParams.Color:=clbtnface;
      end;
    end;
  end;
  //Set Threshhold 2
  DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
  DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMWC.text + ' and ChannelID = ' + cmbChannelIQ3.Text ;
  DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
  if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString<>'2') and (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString<>'4') and (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString<>'5') and (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString<>'6') and (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString<>'7')then
  begin
    if(teTH2MWC.Text<>'-')then
    begin
      if(teTH2MWC.Text<>Dataun.dmDataModule.dsIQ3Channels.FieldByName('Threshold2').AsString)and(StrToInt(teTH2MWC.Text)>0)then
      Begin
        giUID := StrToInt(cmbUidMWC.text);
        viHi := Hi(StrToInt(teTH2MWC.Text));
        viLo := Lo(StrToInt(teTH2MWC.Text));
        ChannelMath:=IntToStr((StrToInt(IQ3Configfm.cmbChannelIQ3.Text)-1)+16);
        giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
        ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3StatusRecieved);
        while not PassFlag do
        begin
          PauseCount:=PauseCount+1;
          VariablesUn.delay(10);
          if PauseCount=100 then
            ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3StatusRecieved);
          if PauseCount>300 then
          begin
            teTH2MWC.Color:=clred;
            btUpdateParams.Color:=clRed;
            //ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
            Break;
          end;
        end;
        if(PassFlag)then
        begin
          teTH2MWC.Color:=clWhite;
          btUpdateParams.Color:=clBtnface;
        end
        else
        begin
          teTH2MWC.Color:=clred;
          btUpdateParams.Color:=clRed;
        end;
        PassFlag:=False;
        PauseCount:=0;
      End
      else
      begin
        teTH2MWC.Color:=clwhite;
        btupdateParams.Color:=clbtnface;
      end;
    end;
  end
  else if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString<>'2') and (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString<>'4') and (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString<>'5') and (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString<>'6') and (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString<>'7')then
  begin
    if(teNetSwitchUID2.Text<>'') and (teNetSwitchUID2.Text<>'-')and (teNetSwitchUID2.Text<>Dataun.dmDataModule.dsIQ3Channels.FieldByName('NetworkSwitchUID2').AsString)then
    begin
      giUID := StrToInt(cmbUidMWC.text);
      viHi := Hi(StrToInt(teNetSwitchUID2.Text));
      viLo := Lo(StrToInt(teNetSwitchUID2.Text));
      ChannelMath:=IntToStr((StrToInt(IQ3Configfm.cmbChannelIQ3.Text)-1)+16);
      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
      ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath), IntToStr(vihi),IntToStr(vilo),rhIQ3StatusRecieved);
      while not PassFlag do
      begin
        PauseCount:=PauseCount+1;
        VariablesUn.delay(10);
        if PauseCount=100 then
          ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath), IntToStr(vihi),IntToStr(vilo),rhIQ3StatusRecieved);
        if PauseCount>300 then
        begin
          teNetSwitchUID2.Color:=clred;
          btUpdateParams.Color:=clRed;
          //ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
          Break;
        end;
      end;
      if(PassFlag)then
      begin
        teNetSwitchUID2.Color:=clWhite;
        btUpdateParams.Color:=clBtnface;
      end
      else
      begin
        teNetSwitchUID2.Color:=clred;
        btUpdateParams.Color:=clRed;
      end;
      PassFlag:=False;
      PauseCount:=0;
    End
    else
    begin
      teNetSwitchUID2.Color:=clwhite;
      btupdateParams.Color:=clbtnface;
    end;
  end;
  //Set Threshhold 3
  DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
  DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMWC.text + ' and ChannelID = ' + cmbChannelIQ3.Text ;
  DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
  if(teTH3MWC.Text<>'-')then
  begin
    if(teTH3MWC.Text<>Dataun.dmDataModule.dsIQ3Channels.FieldByName('Threshold3').AsString)and(StrToInt(teTH3MWC.Text)>0)then
    Begin
      giUID := StrToInt(cmbUidMWC.text);
      viHi := Hi(StrToInt(teTH3MWC.Text));
      viLo := Lo(StrToInt(teTH3MWC.Text));
      ChannelMath:=IntToStr((StrToInt(IQ3Configfm.cmbChannelIQ3.Text)-1)+32);
      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
      ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3StatusRecieved);
      while not PassFlag do
      begin
        PauseCount:=PauseCount+1;
        VariablesUn.delay(10);
        if PauseCount=100 then
          ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3StatusRecieved);
        if PauseCount>300 then
        begin
          teTH3MWC.Color:=clred;
          btUpdateParams.Color:=clRed;
          //ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
          Break;
        end;
      end;
      if(PassFlag)then
      begin
        teTH3MWC.Color:=clWhite;
        btUpdateParams.Color:=clBtnface;
      end
      else
      begin
        teTH3MWC.Color:=clred;
        btUpdateParams.Color:=clRed;
      end;
      PassFlag:=False;
      PauseCount:=0;
    End
    else
    begin
      teTH3MWC.Color:=clwhite;
      btupdateParams.Color:=clbtnface;
    end;
  end;
  //Set Threshhold 4
  DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
  DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMWC.text + ' and ChannelID = ' + cmbChannelIQ3.Text ;
  DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
  if(teTH4MWC.Text<>'-')then
  begin
    if(teTH4MWC.Text<>Dataun.dmDataModule.dsIQ3Channels.FieldByName('Threshold4').AsString)and(StrToInt(teTH4MWC.Text)>0)then
    Begin
      giUID := StrToInt(cmbUidMWC.text);
      viHi := Hi(StrToInt(teTH4MWC.Text));
      viLo := Lo(StrToInt(teTH4MWC.Text));
      ChannelMath:=IntToStr((StrToInt(IQ3Configfm.cmbChannelIQ3.Text)-1)+48);
      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
      ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3StatusRecieved);
      while not PassFlag do
      begin
        PauseCount:=PauseCount+1;
        VariablesUn.delay(10);
        if PauseCount=100 then
          ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3StatusRecieved);
        if PauseCount>300 then
        begin
          teTH4MWC.Color:=clred;
          btUpdateParams.Color:=clRed;
          //ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
          Break;
        end;
      end;
      if(PassFlag)then
      begin
        teTH4MWC.Color:=clWhite;
        btUpdateParams.Color:=clBtnface;
      end
      else
      begin
        teTH4MWC.Color:=clred;
        btUpdateParams.Color:=clRed;
      end;
      PassFlag:=False;
      PauseCount:=0;
    End
    else
    begin
      teTH4MWC.Color:=clwhite;
      btupdateParams.Color:=clbtnface;
    end;
  end;
  //Set Night Threshhold
  DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
  DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMWC.text + ' and ChannelID = ' + cmbChannelIQ3.Text ;
  DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
  if(teNightTHMWC.Text<>'-')then
  begin
    if(teNightTHMWC.Text<>Dataun.dmDataModule.dsIQ3Channels.FieldByName('NMThreshold').AsString)and(StrToInt(teNightTHMWC.Text)>0)then
    Begin
      giUID := StrToInt(cmbUidMWC.text);
      viHi := Hi(StrToInt(teNightTHMWC.Text));
      viLo := Lo(StrToInt(teNightTHMWC.Text));
      ChannelMath:=IntToStr((StrToInt(IQ3Configfm.cmbChannelIQ3.Text)-1)+64);
      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
      ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3StatusRecieved);
      while not PassFlag do
      begin
        PauseCount:=PauseCount+1;
        VariablesUn.delay(10);
        if PauseCount=100 then
          ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3StatusRecieved);
        if PauseCount>300 then
        begin
          teNightTHMWC.Color:=clred;
          btUpdateParams.Color:=clRed;
          //ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
          Break;
        end;
      end;
      if(PassFlag)then
      begin
        teNightTHMWC.color:=clWhite;
        btUpdateParams.Color:=clBtnface;
      end
      else
      begin
        teNightTHMWC.Color:=clred;
        btUpdateParams.Color:=clRed;
      end;
      PassFlag:=False;
      PauseCount:=0;
    end
    else
    begin
      teNightTHMWC.Color:=clwhite;
      btupdateParams.Color:=clbtnface;
    end;
  end;
  //Set enabled PS, Night Mode, Retract Mode
  DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
  DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMWC.text + ' and ChannelID = ' + cmbChannelIQ3.Text ;
  DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
  if(teDeviceUIDMWC.text<>'-')then//and(teDeviceUIDMWC.text<>'0.0.0.0')then
  begin
    if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ShadePosition').AsString<>'') and (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ShadePosition').AsString<>'-1')then
    begin
      EnableShadePos:=ConvertShadePosToBin();
      if(EnableShadePos<>Dataun.dmDataModule.dsIQ3Channels.FieldByName('ShadePosition').AsString) then
      Begin
        giUID := StrToInt(cmbUidMWC.text);
        viHi := 0;
        viLo := VariablesUn.BinToInt(EnableShadePos);
        ChannelMath:=IntToStr((StrToInt(IQ3Configfm.cmbChannelIQ3.Text)-1)+80);
        giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
        ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3StatusRecieved);
        while not PassFlag do
        begin
          PauseCount:=PauseCount+1;
          VariablesUn.delay(10);
          if PauseCount=100 then
            ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3StatusRecieved);
          if PauseCount>300 then
          begin
            if shpRetractmode.Visible then
            begin
              shpRetractmode.pen.Color:=clred;
              btUpdateParams.Color:=clRed;
            end;
            if shpUp.Visible then
            begin
              shpUp.pen.Color:=clred;
              btUpdateParams.Color:=clRed;
            end;
            if shpPs1.Visible then
            begin
              shpPs1.pen.Color:=clred;
              btUpdateParams.Color:=clRed;
            end;
            if shpPs2.Visible then
            begin
              shpPs2.pen.Color:=clred;
              btUpdateParams.Color:=clRed;
            end;
            if shpPs3.Visible then
            begin
              shpPs3.pen.Color:=clred;
              btUpdateParams.Color:=clRed;
            end;
            if shpDN.Visible then
            begin
              shpDN.pen.Color:=clred;
              btUpdateParams.Color:=clRed;
            end;

            //ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
            Break;
          end;
        end;
        if(PassFlag)then
        begin
          if shpRetractmode.Visible then
          begin
            shpRetractmode.Visible:=false;
            shpRetractmode.pen.Color:=clgreen;
            btUpdateParams.Color:=clBtnface;
          end;
          if shpUp.Visible then
          begin
            shpUp.Visible:=false;
            shpUp.pen.Color:=clgreen;
            btUpdateParams.Color:=clBtnface;
          end;
          if shpPs1.Visible then
          begin
            shpPs1.Visible:=false;
            shpPs1.pen.Color:=clgreen;
            btUpdateParams.Color:=clBtnface;
          end;
          if shpPs2.Visible then
          begin
            shpPs2.Visible:=false;
            shpPs2.pen.Color:=clgreen;
            btUpdateParams.Color:=clBtnface;
          end;
          if shpPs3.Visible then
          begin
            shpPs3.Visible:=false;
            shpPs3.pen.Color:=clgreen;
            btUpdateParams.Color:=clBtnface;
          end;
          if shpDN.Visible then
          begin
            shpDN.Visible:=false;
            shpDN.pen.Color:=clgreen;
            btUpdateParams.Color:=clBtnface;
          end;
        end
        else
        begin
          if shpRetractmode.Visible then
          begin
            shpRetractmode.pen.Color:=clred;
            btUpdateParams.Color:=clRed;
          end;
          if shpUp.Visible then
          begin
            shpUp.pen.Color:=clred;
            btUpdateParams.Color:=clRed;
          end;
          if shpPs1.Visible then
          begin
            shpPs1.pen.Color:=clred;
            btUpdateParams.Color:=clRed;
          end;
          if shpPs2.Visible then
          begin
            shpPs2.pen.Color:=clred;
            btUpdateParams.Color:=clRed;
          end;
          if shpPs3.Visible then
          begin
            shpPs3.pen.Color:=clred;
            btUpdateParams.Color:=clRed;
          end;
          if shpDN.Visible then
          begin
            shpDN.pen.Color:=clred;
            btUpdateParams.Color:=clRed;
          end;
        end;
        PassFlag:=False;
        PauseCount:=0;
      end
      else
      begin
        if shpRetractmode.Visible then
        begin
          shpRetractmode.Visible:=false;
          shpRetractmode.pen.Color:=clgreen;
        end;
        if shpUp.Visible then
        begin
          shpUp.Visible:=false;
          shpUp.pen.Color:=clgreen;
        end;
        if shpPs1.Visible then
        begin
          shpPs1.Visible:=false;
          shpPs1.pen.Color:=clgreen;
        end;
        if shpPs2.Visible then
        begin
          shpPs2.Visible:=false;
          shpPs2.pen.Color:=clgreen;
        end;
        if shpPs3.Visible then
        begin
          shpPs3.Visible:=false;
          shpPs3.pen.Color:=clgreen;
        end;
        if shpDN.Visible then
        begin
          shpDN.Visible:=false;
          shpDN.pen.Color:=clgreen;
        end;
      end;
    end;
  end;
  //Set Night Time Threshold IN
  DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
  DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMWC.text + ' and ChannelID = ' + cmbChannelIQ3.Text ;
  DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
  if(teNightInMWC.Text<>'-')then
  begin
    if(teNightInMWC.Text<>Dataun.dmDataModule.dsIQ3Channels.FieldByName('NtThresholdIn').AsString) then
    Begin
      giUID := StrToInt(cmbUidMWC.text);
      viHi := 0;
      viLo := StrToInt(teNightInMwc.Text);
      ChannelMath:=IntToStr((StrToInt(IQ3Configfm.cmbChannelIQ3.Text)-1)+112);
      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
      ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3StatusRecieved);
      while not PassFlag do
      begin
        PauseCount:=PauseCount+1;
        VariablesUn.delay(10);
        if PauseCount=100 then
          ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3StatusRecieved);
        if PauseCount>300 then
        begin
          teNightInMWC.Color:=clred;
          btUpdateParams.Color:=clRed;
          //ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
          Break;
        end;
      end;
      if(PassFlag)then
      begin
        teNightInMWC.color:=clWhite;
        btUpdateParams.Color:=clBtnface;
      end
      else
      begin
        teNightInMWC.Color:=clred;
        btUpdateParams.Color:=clRed;
      end;
      PassFlag:=False;
      PauseCount:=0;
    end
    else
    begin
      teNightInMWC.Color:=clwhite;
      btupdateParams.Color:=clbtnface;
    end;
  end;
  //Set Night Time Threshold Out
  DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
  DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMWC.text + ' and ChannelID = ' + cmbChannelIQ3.Text ;
  DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
  if(teNightOutMWC.Text<>'-')then
  begin
    if(teNightOutMWC.Text<>Dataun.dmDataModule.dsIQ3Channels.FieldByName('NtThresholdOut').AsString) then
    Begin
      giUID := StrToInt(cmbUidMWC.text);
      viHi := 0;
      viLo := StrToInt(teNightOutMwc.Text);
      ChannelMath:=IntToStr((StrToInt(IQ3Configfm.cmbChannelIQ3.Text)-1)+128);
      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
      ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3StatusRecieved);
      while not PassFlag do
      begin
        PauseCount:=PauseCount+1;
        VariablesUn.delay(10);
        if PauseCount=100 then
          ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3StatusRecieved);
        if PauseCount>300 then
        begin
          teNightOutMWC.Color:=clred;
          btUpdateParams.Color:=clRed;
          //ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
          Break;
        end;
      end;
      if(PassFlag)then
      begin
        teNightOutMWC.color:=clWhite;
        btUpdateParams.Color:=clBtnface;
      end
      else
      begin
        teNightOutMWC.Color:=clred;
        btUpdateParams.Color:=clRed;
      end;
      PassFlag:=False;
      PauseCount:=0;
    end
    else
    begin
      teNightOutMWC.Color:=clwhite;
      btupdateParams.Color:=clbtnface;
    end;
  end;
  //Set Night Position
  DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
  DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMWC.text + ' and ChannelID = ' + cmbChannelIQ3.Text ;
  DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
  if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring = '1')then
  begin
    if(cmbNightShadePos.ItemIndex=0)then
    begin
      NightPos:=1;
    end
    else if (cmbNightShadePos.ItemIndex=1)then
    begin
      NightPos:=3;
    end
    else if (cmbNightShadePos.ItemIndex=2)then
    begin
      NightPos:=2;
    end
    else if (cmbNightShadePos.ItemIndex=3)then
    begin
      NightPos:=6;
    end
    else if (cmbNightShadePos.ItemIndex=4)then
    begin
      NightPos:=4;
    end;
    if(cmbNightShadePos.ItemIndex<>5)and(NightPos<>Dataun.dmDataModule.dsIQ3Channels.FieldByName('nmPosition').AsInteger) then
    Begin

      giUID := StrToInt(cmbUidMWC.text);
      viHi := 0;
      viLo :=NightPos;
      ChannelMath:=IntToStr((StrToInt(IQ3Configfm.cmbChannelIQ3.Text)-1)+144);
      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
      ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3StatusRecieved);
      WaitforResponseSet(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3StatusRecieved);
      if(PassFlag)then
      begin
        shpNightPos.Visible:=False;
        shpCmbOcc.Visible:=false;
        shpNightPos.pen.Color:=clGreen;
        shpCmbOcc.pen.Color:=clGreen;
        btUpdateParams.Color:=clBtnface;
      end
      else
      begin
        shpNightPos.pen.Color:=clred;
        shpNightPos.pen.Color:=clred;
        btUpdateParams.Color:=clRed;
      end;
      PassFlag:=False;
      PauseCount:=0;
    end
    else
    begin
      shpNightPos.Visible:=false;
      shpNightPos.pen.Color:=clgreen;
      shpCmbOcc.Visible:=false;
      shpCmbOcc.pen.Color:=clGreen;
      btupdateParams.Color:=clbtnface;
    end;
  end
  else if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring = '3')then
  begin
    if(cmbOccPos.ItemIndex=0)then
    begin
      NightPos:=1;
    end
    else if (cmbOccPos.ItemIndex=1)then
    begin
      NightPos:=3;
    end
    else if (cmbOccPos.ItemIndex=2)then
    begin
      NightPos:=2;
    end
    else if (cmbOccPos.ItemIndex=3)then
    begin
      NightPos:=6;
    end
    else if (cmbOccPos.ItemIndex=4)then
    begin
      NightPos:=4;
    end;
    if(NightPos<>Dataun.dmDataModule.dsIQ3Channels.FieldByName('nmPosition').AsInteger) then
    Begin
      giUID := StrToInt(cmbUidMWC.text);
      viHi := 0;
      viLo :=NightPos;
      ChannelMath:=IntToStr((StrToInt(IQ3Configfm.cmbChannelIQ3.Text)-1)+144);
      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
      ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3StatusRecieved);
      WaitforResponseSet(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3StatusRecieved);

      if(PassFlag)then
      begin
        shpNightPos.Visible:=False;
        shpCmbOcc.Visible:=false;
        shpNightPos.pen.Color:=clGreen;
        shpCmbOcc.pen.Color:=clGreen;
        btUpdateParams.Color:=clBtnface;
      end
      else
      begin
        shpNightPos.pen.Color:=clred;
        shpNightPos.pen.Color:=clred;
        btUpdateParams.Color:=clRed;
      end;
      PassFlag:=False;
      PauseCount:=0;
    end
    else
    begin
      shpNightPos.Visible:=false;
      shpNightPos.pen.Color:=clgreen;
      shpCmbOcc.Visible:=false;
      shpCmbOcc.pen.Color:=clGreen;
      btupdateParams.Color:=clbtnface;
    end;

  end;

  //Set Shade Down Time Threshold
  DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
  DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMWC.text + ' and ChannelID = ' + cmbChannelIQ3.Text ;
  DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
  if (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring = '1')then
  begin
    if(StrToInt(teShadeDownMWC.Text)<>Dataun.dmDataModule.dsIQ3Channels.FieldByName('DownTimeThreshold').AsInteger) then
    Begin
      giUID := StrToInt(cmbUidMWC.text);
      viHi := Hi(StrToInt(teShadeDownMWC.Text));
      viLo := Lo(StrToInt(teShadeDownMWC.Text));
      ChannelMath:=IntToStr((StrToInt(IQ3Configfm.cmbChannelIQ3.Text)-1)+160);
      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
      ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3StatusRecieved);
      WaitforResponseSet(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3StatusRecieved);
      if(PassFlag)then
      begin
        teShadeDownMWC.Color:=clWhite;

        teOccTime.color:=clWhite;
        btUpdateParams.Color:=clBtnface;
      end
      else
      begin
        teShadeDownMWC.Color:=clred;

        teOccTime.color:=clred;
        btUpdateParams.Color:=clRed;
      end;
      PassFlag:=False;
      PauseCount:=0;
    end
    else
    begin
      teShadeDownMWC.Color:=clwhite;
      teOccTime.color:=clWhite;
      btupdateParams.Color:=clbtnface;
    end;
  end
  else
  begin
    if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring = '3')then
    begin
      if(StrToInt(teOccTime.Text)<>Dataun.dmDataModule.dsIQ3Channels.FieldByName('DownTimeThreshold').AsInteger) then
      Begin
        giUID := StrToInt(cmbUidMWC.text);
        viHi := Hi(StrToInt(teOccTime.Text));
        viLo := Lo(StrToInt(teOccTime.Text));
        ChannelMath:=IntToStr((StrToInt(IQ3Configfm.cmbChannelIQ3.Text)-1)+160);
        giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
        ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3StatusRecieved);
        WaitforResponseSet(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3StatusRecieved);
        if(PassFlag)then
        begin
          teShadeDownMWC.Color:=clWhite;
          teOccTime.color:=clWhite;
          btUpdateParams.Color:=clBtnface;
        end
        else
        begin
          teShadeDownMWC.Color:=clred;
          teOccTime.color:=clred;
          btUpdateParams.Color:=clRed;
        end;
        PassFlag:=False;
        PauseCount:=0;
      end
      else
      begin
        teShadeDownMWC.Color:=clwhite;
        teOccTime.color:=clWhite;
        btupdateParams.Color:=clbtnface;
      end;
    end;
  end;

  //Set Shade Up Time Threshold
  DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
  DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMWC.text + ' and ChannelID = ' + cmbChannelIQ3.Text ;
  DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
  if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring = '1')then
  begin
    if(StrToInt(teShadeUpMWC.Text)<>Dataun.dmDataModule.dsIQ3Channels.FieldByName('UpTimeThreshold').AsInteger) then
    Begin
      giUID := StrToInt(cmbUidMWC.text);
      viHi := Hi(StrToInt(teShadeUpMWC.Text));
      viLo := Lo(StrToInt(teShadeUpMWC.Text));
      ChannelMath:=IntToStr((StrToInt(IQ3Configfm.cmbChannelIQ3.Text)-1)+176);
      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
      ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3StatusRecieved);
      WaitforResponseSet(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3StatusRecieved);
       if(PassFlag)then
      begin
        teShadeUpMWC.color:=clWhite;
        teZGN2Timeout.color:=clWhite;
        teOccUnTime.color:=clWhite;
        btUpdateParams.Color:=clBtnface;
      end
      else
      begin
        teShadeUpMWC.Color:=clred;
        teZGN2Timeout.Color:=clred;
        teOccUnTime.Color:=clred;
        btUpdateParams.Color:=clRed;

      end;
      PassFlag:=False;
      PauseCount:=0;
    end
    else
    begin
      teShadeUpMWC.Color:=clwhite;
      teZGN2Timeout.Color:=clwhite;
      teOccUnTime.color:=clWhite;
      btupdateParams.Color:=clbtnface;
    end;
  end
  else if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring = '2') or (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring = '4') or (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring = '5') or (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring = '6') or (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring = '7')then
  begin
    if(StrToInt(teZGN2Timeout.Text)<>Dataun.dmDataModule.dsIQ3Channels.FieldByName('UpTimeThreshold').AsInteger) then
    Begin
      giUID := StrToInt(cmbUidMWC.text);
      viHi := Hi(StrToInt(teZGN2Timeout.Text));
      viLo := Lo(StrToInt(teZGN2Timeout.Text));
      ChannelMath:=IntToStr((StrToInt(IQ3Configfm.cmbChannelIQ3.Text)-1)+176);
      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
      ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3StatusRecieved);
      WaitforResponseSet(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3StatusRecieved);
       if(PassFlag)then
      begin
        teShadeUpMWC.color:=clWhite;
        teZGN2Timeout.color:=clWhite;
        teOccUnTime.color:=clWhite;
        btUpdateParams.Color:=clBtnface;
      end
      else
      begin
        teShadeUpMWC.Color:=clred;
        teZGN2Timeout.Color:=clred;
        teOccUnTime.Color:=clred;
        btUpdateParams.Color:=clRed;

      end;
      PassFlag:=False;
      PauseCount:=0;
    end
    else
    begin
      teShadeUpMWC.Color:=clwhite;
      teZGN2Timeout.Color:=clwhite;
      teOccUnTime.color:=clWhite;
      btupdateParams.Color:=clbtnface;
    end;
  end
  else if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring = '3')then
  begin
    if(StrToInt(teOccUnTime.Text)<>Dataun.dmDataModule.dsIQ3Channels.FieldByName('UpTimeThreshold').AsInteger) then
    Begin
      giUID := StrToInt(cmbUidMWC.text);
      viHi := Hi(StrToInt(teOccUnTime.Text));
      viLo := Lo(StrToInt(teOccUnTime.Text));
      ChannelMath:=IntToStr((StrToInt(IQ3Configfm.cmbChannelIQ3.Text)-1)+176);
      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
      ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3StatusRecieved);
      WaitforResponseSet(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3StatusRecieved);
      if(PassFlag)then
      begin
        teShadeUpMWC.color:=clWhite;
        teZGN2Timeout.color:=clWhite;
        teOccUnTime.color:=clWhite;
        btUpdateParams.Color:=clBtnface;
      end
      else
      begin
        teShadeUpMWC.Color:=clred;
        teZGN2Timeout.Color:=clred;
        teOccUnTime.Color:=clred;
        btUpdateParams.Color:=clRed;

      end;
      PassFlag:=False;
      PauseCount:=0;
    end
    else
    begin
      teShadeUpMWC.Color:=clwhite;
      teZGN2Timeout.Color:=clwhite;
      teOccUnTime.color:=clWhite;
      btupdateParams.Color:=clbtnface;
    end;
  end;

  //Set Open/closed loop
  if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='1') then
  begin
    DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
    DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMWC.text + ' and ChannelID = ' + cmbChannelIQ3.Text ;
    DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
    viLo := IfThen(cbClosedLoopMWC.Checked,1,0);
    if(viLo<>Dataun.dmDataModule.dsIQ3Channels.FieldByName('OCLoop').AsInteger)and(Dataun.dmDataModule.dsIQ3Channels.FieldByName('OCLoop').AsInteger<>-1) then
    Begin

      giUID := StrToInt(cmbUidMWC.text);
      viHi := 0;
      ChannelMath:=IntToStr((StrToInt(IQ3Configfm.cmbChannelIQ3.Text)-1)+192);
      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
      ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3StatusRecieved);
      while not PassFlag do
      begin
        PauseCount:=PauseCount+1;
        VariablesUn.delay(10);
        if PauseCount=100 then
          ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3StatusRecieved);
        if PauseCount>300 then
        begin
          shpClosedLoop.pen.Color:=clred;
          btUpdateParams.Color:=clRed;
          //ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
          Break;
        end;
      end;
      if(PassFlag)then
      begin
        shpClosedLoop.Visible:=False;
        shpClosedLoop.pen.Color:=clGreen;
        btUpdateParams.Color:=clBtnface;
      end
      else
      begin
        shpClosedLoop.pen.Color:=clred;
        btUpdateParams.Color:=clRed;
      end;
      PassFlag:=False;
      PauseCount:=0;
    end
    else
    begin
      shpClosedLoop.Visible:=false;
      shpClosedLoop.pen.Color:=clgreen;
      btupdateParams.Color:=clbtnface;
    end;
  end;
  //Set Hysteresis
  DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
  DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMWC.text + ' and ChannelID = ' + cmbChannelIQ3.Text ;
  DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
  if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='1')then
  begin
    if(teHysteresisMWC.Text<>'-')then
    begin
      if(StrToInt(teHysteresisMWC.text)<>Dataun.dmDataModule.dsIQ3Channels.FieldByName('HysteresisPercent').AsInteger) then
      Begin
        giUID := StrToInt(cmbUidMWC.text);
        viHi := 0;
        ChannelMath:=IntToStr((StrToInt(IQ3Configfm.cmbChannelIQ3.Text)-1)+208);
        giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
        ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), teHysteresisMWC.Text,rhIQ3StatusRecieved);
        while not PassFlag do
        begin
            PauseCount:=PauseCount+1;
            VariablesUn.delay(10);
            if PauseCount=100 then
              ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), teHysteresisMWC.Text,rhIQ3StatusRecieved);
            if PauseCount>300 then
            begin
              teHysteresisMWC.Color:=clred;
              btUpdateParams.Color:=clRed;
              //ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
              Break;
            end;
        end;
        if(PassFlag)then
        begin
          teHysteresisMWC.Color:=clWhite;
          btUpdateParams.Color:=clBtnface;
        end
        else
        begin
          teHysteresisMWC.Color:=clred;
          btUpdateParams.Color:=clRed;
        end;
        PassFlag:=False;
        PauseCount:=0;
      end
      else
      begin
        teHysteresisMWC.Color:=clwhite;
        btupdateParams.Color:=clbtnface;
      end;
    end;
  end
  else if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='2')then
  begin
    if(cmbSwitchPerson.itemindex<>Dataun.dmDataModule.dsIQ3Channels.FieldByName('HysteresisPercent').AsInteger) then
    Begin
      giUID := StrToInt(cmbUidMWC.text);
      viHi := 0;
      ChannelMath:=IntToStr((StrToInt(IQ3Configfm.cmbChannelIQ3.Text)-1)+208);
      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
      ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(cmbSwitchPerson.itemindex),rhIQ3StatusRecieved);
      while not PassFlag do
      begin
          PauseCount:=PauseCount+1;
          VariablesUn.delay(10);
          if PauseCount=100 then
            ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(cmbSwitchPerson.itemindex),rhIQ3StatusRecieved);
          if PauseCount>300 then
          begin
            shpSwitchPersonality.pen.Color:=CLRed;
            btUpdateParams.Color:=clRed;
            //ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
            Break;
          end;
      end;
      if(PassFlag)then
      begin
        shpSwitchPersonality.Visible:=False;
        shpSwitchPersonality.pen.Color:=clGreen;
        btUpdateParams.Color:=clBtnface;
      end
      else
      begin
        shpSwitchPersonality.pen.Color:=clred;
        btUpdateParams.Color:=clRed;
      end;
      PassFlag:=False;
      PauseCount:=0;
    End
    else
    begin
      shpSwitchPersonality.Visible:=False;
      shpSwitchPersonality.pen.Color:=clGreen;
      btupdateParams.Color:=clbtnface;
    end;
  end;
  //Set MTA Inhibit Timer
  DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
  DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMWC.text + ' and ChannelID = ' + cmbChannelIQ3.Text ;
  DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
  if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring='1') then
  begin
    if(teMTAInhibitMWC.Text<>'-')then
    begin
      if(StrToInt(teMTAInhibitMWC.text)<>Dataun.dmDataModule.dsIQ3Channels.FieldByName('MTAInhibit').AsInteger) then
      Begin

        giUID := StrToInt(cmbUidMWC.text);
        viHi := 0;
        ChannelMath:=IntToStr((StrToInt(IQ3Configfm.cmbChannelIQ3.Text)-1)+224);
        giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
        ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), teMTAInhibitMWC.Text,rhIQ3StatusRecieved);
        while not PassFlag do
        begin
            PauseCount:=PauseCount+1;
            VariablesUn.delay(10);
            if PauseCount=100 then
              ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), teMTAInhibitMWC.Text,rhIQ3StatusRecieved);
            if PauseCount>300 then
            begin
              teMTAInhibitMWC.Color:=clRed;
              btUpdateParams.Color:=clRed;
              //ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
              Break;
            end;
        end;
        if(PassFlag)then
        begin
          teMTAInhibitMWC.color:=clWhite;
          btUpdateParams.Color:=clBtnface;
        end
        else
        begin
          teMTAInhibitMWC.Color:=clred;
          btUpdateParams.Color:=clRed;
        end;
        PassFlag:=False;
        PauseCount:=0;
      end
      else
      begin
        teMTAInhibitMWC.Color:=clwhite;
        btupdateParams.Color:=clbtnface;
      end;
    end;
  end
  else if (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring='2') then
  begin
    if(teSwitchAutoReturn.Text<>'-')then
    begin
      if(StrToInt(teSwitchAutoReturn.text)<>Dataun.dmDataModule.dsIQ3Channels.FieldByName('MTAInhibit').AsInteger) then
      Begin

        giUID := StrToInt(cmbUidMWC.text);
        viHi := 0;
        ChannelMath:=IntToStr((StrToInt(IQ3Configfm.cmbChannelIQ3.Text)-1)+224);
        giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
        ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), teSwitchAutoReturn.Text,rhIQ3StatusRecieved);
        while not PassFlag do
        begin
            PauseCount:=PauseCount+1;
            VariablesUn.delay(10);
            if PauseCount=100 then
              ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), teSwitchAutoReturn.Text,rhIQ3StatusRecieved);
            if PauseCount>300 then
            begin
              teSwitchAutoReturn.Color:=clRed;
              btUpdateParams.Color:=clRed;
              //ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
              Break;
            end;
        end;
        if(PassFlag)then
        begin
          teSwitchAutoReturn.color:=clWhite;
          btUpdateParams.Color:=clBtnface;
        end
        else
        begin
          teSwitchAutoReturn.Color:=clred;
          btUpdateParams.Color:=clRed;
        end;
        PassFlag:=False;
        PauseCount:=0;
      end
      else
      begin
        teSwitchAutoReturn.Color:=clwhite;
        btupdateParams.Color:=clbtnface;
      end;
    end;
  end;

  ChToUpdate:='';
  stIQ3LoadingParams:=true;
  cmbChannelIQ3Change(nil);

  stIQ3LoadingParams:=false;
  disableEnableButtons(true);
  Screen.Cursor := crDefault;
end;

procedure TIQ3Configfm.WaitforResponseSet(UID:Integer;Command,MByte,LByte:String;rh:TNotifyEvent);
var
  PauseCount:Integer;
begin
  passFlag:=false;
  PauseCount:=0;
  while not PassFlag do
  begin
      PauseCount:=PauseCount+1;
      delay(10);
      if (PauseCount=100) or (PauseCount=200) then
        ComFm.SetMWCParams(UID,MByte,LByte,Command,rh);
      if PauseCount>300 then
      begin

        ShowMessage('Failed Command');
        Break;
      end;
  end;
end;
procedure TIQ3Configfm.WaitforResponseStatus(UID:Integer;Command:String;rh:TNotifyEvent);
var
  PauseCount:Integer;
begin
  passFlag:=false;
  PauseCount:=0;
  while not PassFlag do
  begin
      PauseCount:=PauseCount+1;
      delay(10);
      if (PauseCount=100) or (PauseCount=200) then
        ComFm.GetGenericStatus(UID,Command,rh);
      if PauseCount>300 then
      begin

        ShowMessage('Failed Command');
        Break;
      end;
  end;
end;

procedure TIQ3Configfm.btUpdateParamsMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Sender is TPanel
  then with TPanel(Sender)
       do BevelOuter := bvLowered; { bvLowered for MouseDown }
end;

procedure TIQ3Configfm.btUpdateParamsMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Sender is TPanel
  then with TPanel(Sender)
       do BevelOuter := bvRaised; { bvLowered for MouseDown }
end;

procedure TIQ3Configfm.cmbISILSwitchChange(Sender: TObject);
begin
   shpISLS.Visible:=true;
   shpISLS.Pen.Color:=clgreen;
   btUpdateParams.Color:=clLime;
end;

procedure TIQ3Configfm.cmbNightShadePosChange(Sender: TObject);
begin
  if sender=cmbOccPos then
  begin
    shpCmbOcc.visible:=true;
    shpCmbOcc.Pen.Color:=clGreen;
  end
  else if sender=cmbNightShadePos then
  begin
    shpNightPos.visible:=true;
    shpNightPos.Pen.Color:=clGreen;
  end;
  btUpdateParams.Color:=clLime;
end;

//Switched between projects
procedure TIQ3Configfm.cmbProjectMWCChange(Sender: TObject);
begin
  if cmbProjectMWC.ItemIndex > -1 then
  begin
    VariablesUn.gsProjectName := cmbProjectMWC.Text;
    SelectProject;
    Show;
  end;
end;

///Changes which project was opened last. Checked on startup
procedure TIQ3Configfm.SelectProject;
begin
  try
    if not (DataUn.dmDataModule.dsProject.State = dsInactive) then
    begin
      DataUn.dmDataModule.dsProject.Filtered := False;
      DataUn.dmDataModule.dsProject.First;

      while not (dmDataModule.dsProject.Eof) do
      begin
        dmDataModule.dsProject.Edit;
        DataUn.dmDataModule.dsProject.FieldByName('Last').AsInteger := 0;
        DataUn.dmDataModule.dsProject.Post;
        DataUn.dmDataModule.dsProject.Next;
      end;
      //DataUn.dmDataModule.dsProject.Filtered := False;
      DataUn.dmDataModule.dsProject.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
      DataUn.dmDataModule.dsProject.Filtered := True;
      if not dmDataModule.dsProject.Eof then
      begin
        dmDataModule.dsProject.Edit;
        DataUn.dmDataModule.dsProject.FieldByName('Last').AsInteger := 1;
        DataUn.dmDataModule.dsProject.Post;
      end;
      DataUn.dmDataModule.dsProject.Filtered := False;
      dmDataModule.dsProject.MergeChangeLog;
      DataUn.dmDataModule.dsProject.SaveToFile(VariablesUn.gsDataPath + 'dsProject.txt', dfXML);
    end;
  except
  on E: Exception do
  begin
    MessageDlg('TIQMainFm.SelectProject: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;

//Updates Gui for st and sac modes
procedure TIQ3Configfm.cmbSTSACChange(Sender: TObject);
begin

  if not stIQ3unLoading then
  begin
    shpSTSAC.Visible:=true;
    shpSTSAC.Pen.Color:=clgreen;
    btUpdateParams.color:=clLime;
  end;
  if(cmbSTSAC.Text='SAC Mode')then
  begin
    ScrollBox1.Visible:=True;
//    if(cmbGroupChSelector.Text<>'Channel')then
//    begin
//      //btCarryOverChToGroup.Visible:=true;
//      btUpdateGroup.Visible:=true;
//    end;

    btDefaultA.Visible:=True;
    btDefaultB.Visible:=True;
  end
  else
  begin
    ScrollBox1.Visible:=false;
    //btUpdateParams.Visible:=False;
    //btCarryOverChToGroup.Visible:=false;
    btDefaultA.Visible:=False;
    btDefaultB.Visible:=False;
  end;
end;

procedure TIQ3Configfm.cmbSwitchPersonChange(Sender: TObject);
begin
  shpSwitchPersonality.visible :=true;
  shpSwitchPersonality.Pen.Color:=clgreen;
  btUpdateParams.Color:=clLime;
  DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
  DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMWC.text + ' and ChannelID = ' + cmbChannelIQ3.Text;
  DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
  if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring ='2') and (cmbSwitchPerson.ItemIndex=1)then
  begin
    lblTimeoutZGN1.Visible:=true;
    lblMasterUID1.Visible:=false;
    teNetSwitchUID2.Visible:=false;
    teZGN2Timeout.visible:=true;
    teZGN2Timeout.Enabled:=false;
  end
  else if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring ='2') and (cmbSwitchPerson.ItemIndex=0)then
  begin
    lblTimeoutZGN1.Visible:=true;
    lblMasterUID1.Visible:=false;
    teZGN2Timeout.visible:=true;
    teNetSwitchUID2.Visible:=false;
    teZGN2Timeout.Enabled:=true;
  end
  else if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring ='2') then
  begin
    lblMasterUID1.Visible:=true;
    teNetSwitchUID2.Visible:=true;
    lblTimeoutZGN1.Visible:=false;
    teZGN2Timeout.visible:=false;

  end;

end;

procedure TIQ3Configfm.cbPos2MWCClick(Sender: TObject);
begin
  if cbPos2MWC.Checked=false then
  begin
    DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
    DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC+' and ChannelID = ' + IQ3Configfm.cmbChannelIQ3.Text;
    DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
    if (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring='1') then
    begin
      if not(cbUpMWC.Checked)and not(cbpos1MWC.Checked)and not(cbpos3MWC.Checked) and (cbDownMWC.checked)or not(cbUpMWC.Checked)and not(cbpos1MWC.Checked)and(cbpos3MWC.Checked) and not(cbDownMWC.checked)or not(cbUpMWC.Checked)and (cbpos1MWC.Checked)and not(cbpos3MWC.Checked) and not(cbDownMWC.checked)or (cbUpMWC.Checked)and not(cbpos1MWC.Checked)and not(cbpos3MWC.Checked) and not(cbDownMWC.checked)then
      begin
        showmessage('There must be at lest two positions checked');
        cbPos2MWC.Checked:=true;
      end;
    end;
  end;
end;

procedure TIQ3Configfm.cbPos2MWCMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  shpPS2.Visible:=true;
  shpPS2.Pen.Color:=clGreen;
  btUpdateParams.Color:=clLime;
end;

//makes sure at least two positions are enabled
procedure TIQ3Configfm.cbPos3MWCClick(Sender: TObject);
begin
  if cbPos3MWC.Checked=false then
  begin
    DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
    DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC+' and ChannelID = ' + IQ3Configfm.cmbChannelIQ3.Text;
    DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
    if (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring='1') then
    begin
      if not(cbUpMWC.Checked)and not(cbpos1MWC.Checked)and not(cbpos2MWC.Checked) and (cbDownMWC.checked)or not(cbUpMWC.Checked)and not(cbpos1MWC.Checked)and(cbpos2MWC.Checked) and not(cbDownMWC.checked)or not(cbUpMWC.Checked)and (cbpos1MWC.Checked)and not(cbpos2MWC.Checked) and not(cbDownMWC.checked)or (cbUpMWC.Checked)and not(cbpos1MWC.Checked)and not(cbpos2MWC.Checked) and not(cbDownMWC.checked)then
      begin
        showmessage('There must be at lest two positions checked');
        cbPos3MWC.Checked:=true;
      end;
    end;
  end;
end;

procedure TIQ3Configfm.cbPos3MWCMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  shpPS3.Visible:=true;
  shpPS3.Pen.Color:=clGreen;
  btUpdateParams.Color:=clLime;
end;

procedure TIQ3Configfm.cbRetractOptionMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  shpRetractMode.Visible:=true;
  shpRetractMode.Pen.Color:=clgreen;
  btUpdateParams.Color:=clLime;
end;

//makes sure at least two positions are enabled
procedure TIQ3Configfm.cbUpMWCClick(Sender: TObject);
begin
  if cbUpMWC.Checked=false then
  begin
    DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
    DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC+' and ChannelID = ' + IQ3Configfm.cmbChannelIQ3.Text;
    DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
    if (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring='1') then
    begin
      if not(cbpos1MWC.Checked)and not(cbpos2MWC.Checked)and not(cbpos3MWC.Checked) and (cbDownMWC.checked)or not(cbpos1MWC.Checked)and not(cbpos2MWC.Checked)and(cbpos3MWC.Checked) and not(cbDownMWC.checked)or not(cbpos1MWC.Checked)and (cbpos2MWC.Checked)and not(cbpos3MWC.Checked) and not(cbDownMWC.checked)or (cbpos1MWC.Checked)and not(cbpos2MWC.Checked)and not(cbpos3MWC.Checked) and not(cbDownMWC.checked)then
      begin
        showmessage('There must be at lest two positions checked');
        cbUpMWC.Checked:=true;
      end;
    end;
  end;
end;

procedure TIQ3Configfm.cbUpMWCMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  shpUp.Visible:=true;
  shpUp.Pen.Color:=clGreen;
  btUpdateParams.Color:=clLime;
end;

//updates gui with current selected channel's info
//procedure TIQ3Configfm.cmbAvailChChange(Sender: TObject);
//begin
//  stMWCunLoading:=true;
//  btUpdateParams.Color:=clbtnface;
//  cmbChannelMWC.ItemIndex:=StrToInt(cmbAvailCh.Text)-1;
//  cmbChannelMWCChange(nil);
//  stMWCunLoading:=false;
//end;

////updates gui with current selected channel's info
procedure TIQ3Configfm.cmbChannelIQ3Change(Sender: TObject);
Var
    loopString,Grouped1Channels,Grouped2Channels,Grouped3Channels,Grouped4Channels: String;
    AssignedGroup:String;
    I:Integer;

begin
  //Reset Param field colors
  stIQ3unLoading:=true;
  if not(stIQ3LoadingParams)then
  begin
    btUpdateParams.Color:=clbtnface;
    shpSTSAC.Visible:=false;
    shpSTSAC.pen.color:=clgreen;

    teZGN1Switch.Color:=clwhite;
    teZGN2Switch.Color:=clwhite;
    teZGN2Timeout.Color:=clwhite;
    teZGNMWC.Color:=clwhite;
    teTH1MWC.Color:=clwhite;
    teTH2MWC.Color:=clwhite;
    teTH3MWC.Color:=clwhite;
    teTH4MWC.Color:=clwhite;
    teHysteresisMWC.Color:=clwhite;
    teShadeUpMWC.Color:=clwhite;
    teShadeDownMWC.Color:=clwhite;
    teMTAInhibitMWC.Color:=clwhite;
    teSwitchAutoReturn.Color:=clwhite;
    teNightTHMWC.Color:=clwhite;
    teNightInMWC.Color:=clwhite;
    teNightOutMWC.Color:=clwhite;
    teZGNOcc.Color:=clwhite;

    shpISLS.Visible:=false;
    //shpFWDAddr.Visible:=false;
    shpSwitchPersonality.Visible:=false;
    shpUp.Visible:=false;
    shpPS1.Visible:=false;
    shpPS2.Visible:=false;
    shpPS3.Visible:=false;
    shpDN.Visible:=false;
    shpRetractMode.Visible:=false;
    shpClosedLoop.Visible:=false;
    shpNightPos.Visible:=false;
    shpFWDAddrConfig.Visible:=false;


    shpISLS.pen.Color:=clgreen;
    //shpFWDAddr.pen.Color:=clgreen;
    shpSwitchPersonality.pen.Color:=clgreen;
    shpUp.pen.Color:=clgreen;
    shpPS1.pen.Color:=clgreen;
    shpPS2.pen.Color:=clgreen;
    shpPS3.pen.Color:=clgreen;
    shpDN.pen.Color:=clgreen;
    shpRetractMode.pen.Color:=clgreen;
    shpClosedLoop.pen.Color:=clgreen;
    shpNightPos.pen.Color:=clgreen;
    shpFWDAddrConfig.pen.Color:=clgreen;

  end;
  //update depending on SAC Mode vs ST Mode
  DataUn.dmDataModule.dsIQ3s.Filtered := False;
  DataUn.dmDataModule.dsIQ3s.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMWC.text;
  DataUn.dmDataModule.dsIQ3s.Filtered := True;
  teAlias.OnChange:=nil;
  teAlias.Text:=DataUn.dmDataModule.dsIQ3s.FieldByName('Alias').AsString;
  teAlias.OnChange:=teAliasChange;
  if(Dataun.dmDataModule.dsIQ3s.FieldByName('EnableMessages').AsString='1')then
  begin
    cmbSTSAC.ItemIndex:= 0;

  end
  else
  begin
    cmbSTSAC.ItemIndex:= 1;
  end;
  cmbSTSACChange(nil);
  cmbFwdAddrFormat.ItemIndex:= cmbFwdAddrFormat.Items.IndexOf(Dataun.dmDataModule.dsIQ3s.FieldByName('FwdAddrFormat').AsString);
  //check if channel is bound to switch, if so update Switch params from dataset
  DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
  DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMWC.text + ' and ChannelID = ' + cmbChannelIQ3.Text;
  DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
  if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring ='2') then
  begin


    if (Dataun.dmDataModule.dsIQ3Channels.FieldByName('NetworkSwitchUID2').asstring <> '-1')then
    begin
      teNetSwitchUID2.Text := Dataun.dmDataModule.dsIQ3Channels.FieldByName('NetworkSwitchUID2').asstring;
    end
    else
    IQ3Configfm.teNetSwitchUID2.Text := '-';

    if(DataUn.dmDataModule.dsIQ3Channels.FieldByName('HysteresisPercent').AsInteger=1)then
    begin
      lblTimeoutZGN1.Visible:=true;
      lblMasterUID1.Visible:=false;
      teNetSwitchUID2.Visible:=false;
      teZGN2Timeout.visible:=true;
      teZGN2Timeout.Enabled:=false;
    end
    else if(DataUn.dmDataModule.dsIQ3Channels.FieldByName('HysteresisPercent').AsInteger=0)then
    begin
      lblTimeoutZGN1.Visible:=true;
      lblMasterUID1.Visible:=false;
      teZGN2Timeout.visible:=true;
      teNetSwitchUID2.Visible:=false;
      teZGN2Timeout.Enabled:=true;
    end
    else
    begin
      lblMasterUID1.Visible:=true;
      teNetSwitchUID2.Visible:=true;
      lblTimeoutZGN1.Visible:=false;
      teZGN2Timeout.visible:=false;

    end;
    if not (Dataun.dmDataModule.dsIQ3Channels.FieldByName('HysteresisPercent').asstring ='0') then
    begin
      teZGN2Timeout.Enabled:=false;
    end
    else
    begin
      teZGN2Timeout.Enabled:=true;
    end;
  end;

//  DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
//  DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMWC.text + ' and ChannelID = ' + cmbChannelMWC.Text;
//  DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
//  if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring ='2') and not (Dataun.dmDataModule.dsIQ3Channels.FieldByName('HysteresisPercent').asstring ='0')then
//  begin
//    teZGN1Timeout.Enabled:=false;
//    teZGN2Timeout.Enabled:=false;
//  end
//  else if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring ='2') and (Dataun.dmDataModule.dsIQ3Channels.FieldByName('HysteresisPercent').asstring='0')then
//  begin
//    teZGN1Timeout.Enabled:=true;
//    teZGN2Timeout.Enabled:=true;
//  end;

  //Update all Param fields
  if(cmbUidMWC.ItemIndex<>-1)then
  begin
    variablesUn.gsChannelNum:=IntToStr(cmbChannelIQ3.ItemIndex);
    DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
    DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMWC.text + ' and ChannelID = ' + cmbChannelIQ3.Text;
    DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
    cbRetractOption.Checked:=false;
    cbBoundMWC.Checked := Dataun.dmDataModule.dsIQ3Channels.FieldByName('Bound').AsBoolean;
    if(cbBoundMWC.Checked)then
    begin
      btUnBind.Visible:=True;
    end
    else
    begin
      btUnBind.Visible:=False;
    end;
    if (Dataun.dmDataModule.dsIQ3Channels.FieldByName('Threshold2').asstring <> '-1')then
    begin
      teTH2MWC.Text := Dataun.dmDataModule.dsIQ3Channels.FieldByName('Threshold2').asstring;
      if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('HysteresisPercent').asstring<>'-1')then
      begin
        IQ3Configfm.teTH2MWCOut.text:=FloatToStr(StrToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('Threshold2').asstring)*((100-StrToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('HysteresisPercent').asstring))/100));
      end
      else
      begin
        IQ3Configfm.teTH2MWCOut.text:='-';
      end;
    end
    else
      IQ3Configfm.teTH2MWC.Text := '-';

    if (Dataun.dmDataModule.dsIQ3Channels.FieldByName('Threshold1').asstring <> '-1') then
    begin
      teTH1MWC.Text := Dataun.dmDataModule.dsIQ3Channels.FieldByName('Threshold1').asstring;
      if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('HysteresisPercent').asstring<>'-1')then
      begin
        IQ3Configfm.teTH1MWCOut.text:=FloatToStr(StrToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('Threshold1').asstring)*((100-StrToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('HysteresisPercent').asstring))/100));
      end
      else
      begin
        IQ3Configfm.teTH1MWCOut.text:='-';
      end;
    end
    else
      IQ3Configfm.teTH1MWC.Text := '-';

    if (Dataun.dmDataModule.dsIQ3Channels.FieldByName('Threshold3').asstring <> '-1') then
    begin
      teTH3MWC.Text := Dataun.dmDataModule.dsIQ3Channels.FieldByName('Threshold3').asstring;
      if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('HysteresisPercent').asstring<>'-1')then
      begin
        IQ3Configfm.teTH3MWCOut.text:=FloatToStr(StrToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('Threshold3').asstring)*((100-StrToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('HysteresisPercent').asstring))/100));
      end
      else
      begin
        IQ3Configfm.teTH3MWCOut.text:='-';
      end;
    end
    else
      IQ3Configfm.teTH3MWC.Text := '-';

    if (Dataun.dmDataModule.dsIQ3Channels.FieldByName('Threshold4').asstring <> '-1') then
    begin
      teTH4MWC.Text := Dataun.dmDataModule.dsIQ3Channels.FieldByName('Threshold4').asstring;
      if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('HysteresisPercent').asstring<>'-1')then
      begin
        IQ3Configfm.teTH4MWCOut.text:=FloatToStr(StrToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('Threshold4').asstring)*((100-StrToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('HysteresisPercent').asstring))/100));
      end
      else
      begin
        IQ3Configfm.teTH4MWCOut.text:='-';
      end;
    end
    else
      IQ3Configfm.teTH4MWC.Text := '-';

    if (Dataun.dmDataModule.dsIQ3Channels.FieldByName('NmThreshold').asstring <> '-1') then
    begin
      teNightTHMWC.Text := Dataun.dmDataModule.dsIQ3Channels.FieldByName('NmThreshold').asstring;
    end
    else
      IQ3Configfm.teNightTHMWC.Text := '-';

    if (Dataun.dmDataModule.dsIQ3Channels.FieldByName('NtThresholdIn').asstring <> '-1') then
    begin
      teNightInMWC.Text := Dataun.dmDataModule.dsIQ3Channels.FieldByName('NtThresholdIn').asstring;
    end
    else
      IQ3Configfm.teNightInMWC.Text := '-';

    if (Dataun.dmDataModule.dsIQ3Channels.FieldByName('NtThresholdOut').asstring <> '-1') then
    begin
      teNightOutMWC.Text := Dataun.dmDataModule.dsIQ3Channels.FieldByName('NtThresholdOut').asstring;
    end
    else
      IQ3Configfm.teNightOutMWC.Text := '-';

    if (Dataun.dmDataModule.dsIQ3Channels.FieldByName('DownTimeThreshold').asstring <> '-1') then
    begin
      if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('paramID').asstring = '1')then
      begin
        teShadeDownMWC.Text := Dataun.dmDataModule.dsIQ3Channels.FieldByName('DownTimeThreshold').asstring;
      end

      else if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('paramID').asstring = '3')then
      begin
        teOccTime.Text:= Dataun.dmDataModule.dsIQ3Channels.FieldByName('DownTimeThreshold').asstring;
      end;
    end
    else
    begin
      IQ3Configfm.teShadeDownMWC.Text := '-';
    end;

    if (Dataun.dmDataModule.dsIQ3Channels.FieldByName('UpTimeThreshold').asstring <> '-1') then
    begin
      if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring = '1')then
      begin
        teShadeUpMWC.Text := Dataun.dmDataModule.dsIQ3Channels.FieldByName('UpTimeThreshold').asstring;
      end
      else if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('paramID').asstring = '2')then
      begin
        teZGN2Timeout.Text:= Dataun.dmDataModule.dsIQ3Channels.FieldByName('UpTimeThreshold').asstring;
      end
      else if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('paramID').asstring = '3')then
      begin
        teOccUnTime.Text:= Dataun.dmDataModule.dsIQ3Channels.FieldByName('UpTimeThreshold').asstring;
      end;
    end
    else
    begin
      IQ3Configfm.teShadeUpMWC.Text := '-';
      teZGN2Timeout.Text := '-';
    end;
    if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring ='1') then
    begin
      if (Dataun.dmDataModule.dsIQ3Channels.FieldByName('MTAInhibit').asstring <> '-1') then
      begin
        teMTAInhibitMWC.Text := Dataun.dmDataModule.dsIQ3Channels.FieldByName('MTAInhibit').asstring;
      end
      else
      begin
        IQ3Configfm.teMTAInhibitMWC.Text := '-';
      end;
    end
    else if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring ='2') then
    begin
      if (Dataun.dmDataModule.dsIQ3Channels.FieldByName('MTAInhibit').asstring <> '-1') then
      begin
        teSwitchAutoReturn.Text := Dataun.dmDataModule.dsIQ3Channels.FieldByName('MTAInhibit').asstring;
      end
      else
      begin
        IQ3Configfm.teSwitchAutoReturn.Text := '-';
      end;
    end;
    if (Dataun.dmDataModule.dsIQ3Channels.FieldByName('HysteresisPercent').asstring <> '-1') then
    begin
      if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring='1')then
      begin
        teHysteresisMWC.Text := Dataun.dmDataModule.dsIQ3Channels.FieldByName('HysteresisPercent').asstring;
      end
      else if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring='2')then
      begin
        if(StrToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('HysteresisPercent').asstring)<4)then
        begin
          cmbSwitchPerson.itemindex:=StrToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('HysteresisPercent').asstring)
        end
        else
        begin
          cmbSwitchPerson.itemindex:=0;
        end;
      end;
    end
    else
      IQ3Configfm.teHysteresisMWC.Text := '-';

    if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('nmPosition').asstring <> '-1')then
    begin
      if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('nmPosition').asstring='1')then
      begin
        cmbOccPos.ItemIndex:=0;
        cmbNightShadePos.ItemIndex:= 0;
      end
      else if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('nmPosition').asstring='3')then
      begin
        cmbOccPos.ItemIndex:=1;
        cmbNightShadePos.ItemIndex:= 1;
      end
      else if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('nmPosition').asstring='2')then
      begin
        cmbOccPos.ItemIndex:=2;
        cmbNightShadePos.ItemIndex:= 2;
      end
      else if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('nmPosition').asstring='6')then
      begin
        cmbOccPos.ItemIndex:=3;
        cmbNightShadePos.ItemIndex:= 3;
      end
      else if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('nmPosition').asstring='4')then
      begin
        cmbOccPos.ItemIndex:=4;
        cmbNightShadePos.ItemIndex:= 4;
      end;
    end;

    //Update Device Type
    cmbDeviceType.ItemIndex:= StrToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring)-1;

    if (Dataun.dmDataModule.dsIQ3Channels.FieldByName('DeviceUID').asstring <> '-1') then
    begin
      teDeviceUIDMWC.Text := Dataun.dmDataModule.dsIQ3Channels.FieldByName('DeviceUID').asstring;
    end
    else
    begin
      IQ3Configfm.teDeviceUIDMWC.Text := '-';
    end;


    cmbNightShadePos.Visible:=true;
    lblGroupZGN.Visible:=false;

    if (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ZGN').asstring <> '-1') then
    begin
      teZGNMWC.Text := ConvertZGN(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ZGN').asstring);
      //teZGNMWC.Text := Dataun.dmDataModule.dsIQ3Channels.FieldByName('ZGN').asstring;
    end
    else
    begin
      IQ3Configfm.teZGNMWC.Text := '-';
    end;
    //Change visible panels based on device type
    if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asInteger=2)then
    begin
      if (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ZGN').asstring <> '-1') then
      begin
        teZGN2Switch.Text := ConvertZGN(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ZGN').asstring);
      end
      else
      begin
          IQ3Configfm.teZGN1Switch.Text := '-';
      end;
      if (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ZGN2').asstring <> '-1') then
      begin
        teZGN1Switch.Text := ConvertZGN(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ZGN2').asstring);
      end
      else
      begin
        IQ3Configfm.teZGN2Switch.Text := '-';
      end;
      pnlcmdType.Visible:=false;
      cmbBindMethod.Visible:=false;
      pnlOccSensor.Visible:=false;
      pnLightParameters.Visible:=false;
      pnlTimersDay.Visible:=false;
      pnlThreshNight.Visible:=false;
      pnlNightTimers.Visible:=false;

      cmbSwitchPerson.Visible:=true;
      lblSwPersonality.Visible:=true;
      teSwitchAutoReturn.Visible:=true;
      lblrta.Visible:=true;
      lblRtaUnit.Visible:=true;
      teZGN2Switch.Visible:=true;
      lblZGn1.Visible:=true;
      lblForwardEnabled.Visible:=false;
      cbForwardEnabled.Visible:=false;
      shpForwarding.Visible:=false;

      pnlConfig.Visible:=False;
      pnlSwitch.Visible:=true;
      ScrollBox1.Visible:=true;
      lblNoEpp.Visible:=true;
      ScrollBox1.VertScrollBar.Position :=0;
      pnlFirmware.Top:=286;
    end
    else if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asInteger=4) or (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asInteger=5) or (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asInteger=6) or (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asInteger=7)then
    begin
      if (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ZGN').asstring <> '-1') then
      begin
        teZGN2Switch.Text := ConvertZGN(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ZGN').asstring);
      end
      else
      begin
          IQ3Configfm.teZGN1Switch.Text := '-';
      end;
      pnlcmdType.Visible:=false;
      cmbBindMethod.Visible:=false;
      pnlOccSensor.Visible:=false;
      pnLightParameters.Visible:=false;
      pnlTimersDay.Visible:=false;
      pnlThreshNight.Visible:=false;
      pnlNightTimers.Visible:=false;

      cmbSwitchPerson.Visible:=false;
      lblSwPersonality.Visible:=false;
      lblTimeoutZGN1.Visible:=false;

      lblMasterUID1.Visible:=false;
      teZGN2Switch.Visible:=true;
      teZGN1Switch.Visible:=false;
      lblZGn1.Visible:=true;
      lblZgn2.Visible:=false;
      teSwitchAutoReturn.Visible:=false;
      lblrta.Visible:=false;
      lblRtaUnit.Visible:=false;
      teNetSwitchUID2.Visible:=false;
      teZGN2Timeout.visible:=false;
      lblForwardEnabled.Visible:=true;
      cbForwardEnabled.Visible:=true;
      shpForwarding.Visible:=false;
//      teZGN2Timeout.Enabled:=false;

      pnlConfig.Visible:=False;
      pnlSwitch.Visible:=true;
      ScrollBox1.Visible:=true;
      lblNoEpp.Visible:=true;
      ScrollBox1.VertScrollBar.Position :=0;
      pnlFirmware.Top:=286;
    end
    else if (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asInteger=3)then
    begin
      if (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ZGN').asstring <> '-1') then
      begin
        teZGNOcc.Text := ConvertZGN(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ZGN').asstring);
      end;
      if cmbSTSAC.ItemIndex= 0 then
      begin
        pnlcmdType.Visible:=false;
        cmbBindMethod.Visible:=true;
        pnlOccSensor.Visible:=true;
        pnLightParameters.Visible:=false;
        pnlTimersDay.Visible:=false;
        pnlThreshNight.Visible:=false;
        pnlNightTimers.Visible:=false;

        pnlConfig.Visible:=False;
        pnlSwitch.Visible:=true;
        ScrollBox1.Visible:=true;
        lblNoEpp.Visible:=false;
        ScrollBox1.VertScrollBar.Position :=0;
        pnlSwitch.Visible:=false;
        pnlFirmware.Top:=177;
      end;
    end
    else
    begin
      pnlcmdType.Visible:=true;
      cmbBindMethod.Visible:=true;
      pnLightParameters.Visible:=true;
      pnlTimersDay.Visible:=true;
      pnlThreshNight.Visible:=true;
      pnlNightTimers.Visible:=true;
      pnlConfig.Visible:=true;
      lblNoEpp.Visible:=False;
      pnlSwitch.Visible:=false;
      pnlOccSensor.Visible:=false;
      ScrollBox1.VertScrollBar.Position :=0;
      pnlFirmware.Top:=646;
      pnlcmdType.Top:=612;
      shpFWDAddrConfig.Left:=165;
      cmbFwdAddrFormat.Left:=168;
    end;

    vsSwitchBin:=IntToBin(StrToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('OCLoop').asstring));
    if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring='1')then
    begin
      ConvertShadePosToCB(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ShadePosition').asstring,Dataun.dmDataModule.dsIQ3Channels.FieldByName('OCLoop').asstring);
    end
    else if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring='2')or(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring='4')or(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring='5')or(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring='6')or(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring='7')then
    begin
      if(vsSwitchBin[1]='1')then
      begin
        cmbISILSwitch.ItemIndex:=0;
      end
      else if(vsSwitchBin[1]='0')then
      begin
        cmbISILSwitch.ItemIndex:=1;
      end;
      if(vsSwitchBin[8]='1')then
      begin
        cbForwardEnabled.Checked := true;
      end
      else if(vsSwitchBin[8]='0')then
      begin
        cbForwardEnabled.Checked := False;
      end;
    end;
  end;

  stIQ3unLoading:=False;
end;

Function TIQ3Configfm.ConvertZGN(ZGN:String):String;
var
  OutPutList: TStringList;
begin
  DataUn.dmDataModule.dsProject.Filtered := False;
  DataUn.dmDataModule.dsProject.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
  DataUn.dmDataModule.dsProject.Filtered := True;
  if not(DataUn.dmDataModule.dsProject.EOF)then
  begin
    OutPutList := TStringList.Create;
    if(DataUn.dmDataModule.dsProject.FieldByName('ZGNG').asInteger=1)then
    begin
      Split('.', Trim(ZGN), OutPutList);
      if(OutPutList[0]='255')and(OutPutList[2]='255')then
      begin
        result:=OutPutList[1];
      end
      else
      begin
        result:=zgn;
      end;
    end
    else
    begin
      result:=zgn;
    end;
  end
  else
  begin
    showmessage('Please Create a Project Before Continuing!');
  end;
end;

//returns the gui to the IQ2 screen
procedure TIQ3Configfm.cmbDeviceSelectorChange(Sender: TObject);
VAR
  I:Integer;
begin
  VariablesUn.giTop := Top;
  VariablesUn.giLeft := Left;
  if(cmbDeviceSelector.Text='MTR')then
  begin
//    for I := 0 to Length(viUIDOnShow)-1 do
//    begin
//      ComFm.SetMWCParams(viUIDOnShow[I],MWC_HaltTimer,'0','0',rhIQ3StatusRecieved);
//      VariablesUn.delay(200);
//    end;
    DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
    DataUn.dmDataModule.dsIQ3s.SaveToFile(VariablesUn.gsDataPath + 'dsIQ3s.txt', dfXML);
    DataUn.dmDataModule.dsIQ3Channels.SaveToFile(VariablesUn.gsDataPath + 'dsIQ3Channels.txt', dfXML);
    if Assigned (rh_ShowMain) then
      rh_ShowMain(Self);
    Hide;
    VariablesUn.PassLoopFlag:=True;
  end
  else if(cmbDeviceSelector.Text='MWC')then
  begin

    DataUn.dmDataModule.dsMWCChannels.Filtered := False;
    DataUn.dmDataModule.dsMWCChannels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsMWCChannels.Filtered := True;

    if not dmDataModule.dsMWCChannels.Eof then
    begin


//      MWCConfigfm.rh_ShowMain := rhShowMain;
//      MWCDisplayfm.rh_ShowMain := rhShowMain;
      Hide;
      MWCConfigfm.show;


      //MWCConfigfm.Show;
    end
    else
    begin
      ShowMessage('No MWC Devices Found');
      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('MTR');
    end;

  end
  else if(cmbDeviceSelector.Text='MNI')then
  begin
    DataUn.dmDataModule.dsMNI.Filtered := False;
    DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsMNI.Filtered := True;
    if not DataUn.dmDataModule.dsMNI.Eof then
    begin
      IQ3Configfm.Hide;
      MNIConfigFm.Show;
      VariablesUn.PassLoopFlag:=True;
    end
    else
    begin
      ShowMessage('No MNI Devices Found');
      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('iQ3-DC-RF');
    end;

  end
  else if(cmbDeviceSelector.Text='MDC')then
  begin
    DataUn.dmDataModule.dsMNI2.Filtered := False;
    DataUn.dmDataModule.dsMNI2.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsMNI2.Filtered := True;

    if not dmDataModule.dsMNI2.Eof then
    begin
      IQ3Configfm.Hide;
      rh_showMNI2Config(self);
      VariablesUn.giTop := Top;
      VariablesUn.giLeft := Left;
      rh_GetMNI2VirtualMotorStatus(Self);
    end
    else
    begin
      ShowMessage('No MDC Devices Found');
      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('iQ3-DC RF');
    end;
  end
  else if(cmbDeviceSelector.Text='IQMLC2')then
  begin
    DataUn.dmDataModule.dsIQMLC2.Filtered := False;
    DataUn.dmDataModule.dsIQMLC2.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsIQMLC2.Filtered := True;

    if not dmDataModule.dsIQMLC2.Eof then
    begin
      Hide;
      VariablesUn.giTop := Top;
      VariablesUn.giLeft := Left;
      if Assigned (rh_showIQMLC2Config) then
        rh_showIQMLC2Config(self);
      //IQMLC2configFm.Show;
    end
    else
    begin
      ShowMessage('No IQMLC2 Devices Found');
      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('iQ3-DC RF');
    end;
  end
  else if(cmbDeviceSelector.Text='PowerPanel')then
  begin
    DataUn.dmDataModule.dsPowerPanel.Filtered := False;
    DataUn.dmDataModule.dsPowerPanel.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsPowerPanel.Filtered := True;

    if not dmDataModule.dsPowerPanel.Eof then
    begin
      Hide;
      VariablesUn.giTop := Top;
      VariablesUn.giLeft := Left;
      PowerPanelConfigFm.Show;
    end
    else
    begin
      ShowMessage('No IQ2-DC Power Panel Devices Found');
      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('iQ3-DC RF');
    end;
  end
  else if(cmbDeviceSelector.Text='Motor Maint')then
  begin
    DataUn.dmDataModule.dsMotors.Filtered := False;
    DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) + ' and MotorType = 69 or MotorType = 66';
    DataUn.dmDataModule.dsMotors.Filtered := True;

    if not dmDataModule.dsMotors.Eof then
    begin
      VariablesUn.giTop := Top;
      VariablesUn.giLeft := Left;

      Hide;
      MotorMaintenanceDisplayFm.Show;
      MotorMaintenanceDisplayFm.cmbUidMM.ItemIndex:= 0;
      MotorMaintenanceDisplayFm.cmbUidMM.OnChange(self);
    end
    else
    begin
      ShowMessage('No Motors Found');
      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('iQ3-DC RF');
    end;
  end;
end;

procedure TIQ3Configfm.cmbDeviceTypeChange(Sender: TObject);
var
  viHi,viLo:Integer;
  ChannelMath:string;
begin
  DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
  DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMWC.text + ' and ChannelID = ' + cmbChannelIQ3.Text ;
  DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
  if not (Dataun.dmDataModule.dsIQ3Channels.FieldByName('Bound').AsBoolean)then
  begin
    if(cmbDeviceType.ItemIndex+1<>StrToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString))then
    begin
      IQ3PassLoopFlag:=True;
      variablesUn.PassLoopFlag:=true;
      ChToUpdate:=cmbChannelIQ3.Text;
      if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='2') or (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='4') or (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='5') or (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='6') or (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='7')then
      begin
        btDefaultAClick(nil);
//        btUpdateParamsClick(nil);
      end
      else
      begin
         MWCDefaultChooserfm.ShowModal;
        if(MWCDefaultChooserfm.DeFaultToLoad=1)then
        begin
          btDefaultAClick(nil);
        end
        else if(MWCDefaultChooserfm.DeFaultToLoad=2) then
        begin
          btDefaultBClick(nil);
        end
        else if(MWCDefaultChooserfm.DeFaultToLoad=3) then
        begin
          DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
          DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMWC.text + ' and ChannelID = ' + cmbChannelIQ3.Text ;
          DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
          PassFlag:=False;
          PauseCount:=0;
          giUID := StrToInt(cmbUidMWC.text);
          viHi := 0;
          viLo := cmbDeviceType.ItemIndex+1;
          ChannelMath:=IntToStr((StrToInt(IQ3Configfm.cmbChannelIQ3.Text)-1)+240);
          giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
          ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3StatusRecieved);
          while not PassFlag do
          begin
            PauseCount:=PauseCount+1;
            VariablesUn.delay(10);
            if PauseCount=100 then
              ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhIQ3StatusRecieved);
            if PauseCount>300 then
            begin
              ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
              Break;
            end;
          end;
          btUpdateParamsClick(nil);

        end;

      end;
//      else
//      begin
//        cmbDeviceType.ItemIndex:=StrToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString)-1;
//      end;
    end;
  end
  else
  begin
    ShowMessage('Please Unpair Channel Before Changing device type.');
    cmbDeviceType.ItemIndex:=StrToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring)-1;
    //if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('DeviceID')
  end;

end;



procedure TIQ3Configfm.cmbFwdAddrFormatChange(Sender: TObject);
begin
  shpFWDAddrConfig.Visible:=true;
  shpFWDAddrConfig.Pen.Color:=clLime;
  btUpdateParams.Color:=clLime;
end;

//updates gui to specific group's data
procedure TIQ3Configfm.cmbGroupChSelectorChange(Sender: TObject);
begin
  DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
  DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMWC.text + ' and ChannelID = ' + cmbChannelIQ3.Text ;
  DataUn.dmDataModule.dsIQ3Channels.Filtered := True;

    cmbDeviceType.Enabled:=true;
    //btCarryOverChToGroup.Visible:=false;
    //cmbAvailCh.Visible:=False;
    IQ3Configfm.cmbChannelIQ3.Visible:=true;
    //btUpdateGroup.Visible:=false;
    btUpdateParams.Visible:=true;
    if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='1')then
    begin
      pnlConfig.Visible:=true;
      pnlFirmware.Visible:=true;
      pnlCmdType.Visible:=true;
      pnlNightTimers.Visible:=true;
      pnLightParameters.Visible:=true;
      pnlThreshNight.Visible:=true;
      pnlTimersDay.Visible:=true;
      pnlSwitch.Visible:=false;
      pnlOccSensor.Visible:=false;
    end
    else if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='2')then
    begin
      pnlConfig.Visible:=false;
      pnlFirmware.Visible:=true;
      pnlCmdType.Visible:=false;
      pnlNightTimers.Visible:=false;
      pnLightParameters.Visible:=false;
      pnlThreshNight.Visible:=false;
      pnlTimersDay.Visible:=false;
      pnlSwitch.Visible:=true;
      pnlOccSensor.Visible:=false;
    end
    else if(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString='3')then
    begin
      pnlConfig.Visible:=false;
      pnlFirmware.Visible:=true;
      pnlFirmware.Top:=177;
      pnlCmdType.Visible:=false;
      pnlNightTimers.Visible:=false;
      pnLightParameters.Visible:=false;
      pnlThreshNight.Visible:=false;
      pnlTimersDay.Visible:=false;
      pnlSwitch.Visible:=false;
      pnlOccSensor.Visible:=true;
    end;


end;

//Convert the shade postion Binary into the appropriate checkboxes
procedure TIQ3Configfm.ConvertShadePosToCB(LoopString,OCloop:String);
var

  I:integer;
begin
  If(OCloop='1')then
   begin
    cbClosedLoopMWC.Checked := true;
  end
  else if (OCloop='0')then
    cbClosedLoopMWC.Checked := False;
  if(loopstring<>'-1') then
  begin
    {JH TODO}
    for I := 1 to loopString.Length do
    Begin
      if (I=1) and (loopString[I]='1') then
      begin
        IQ3Configfm.cbRetractOption.Checked:=True;
      end
      else if (I=1) and (loopString[I]='0') then
      begin
        IQ3Configfm.cbRetractOption.Checked:=False;
      end;

      if (I=2) and (loopString[2]='0') then
      begin
        cmbNightShadePos.ItemIndex:=5;
      end;

      if (I=4) and (loopString[4]='1') then
      begin
        IQ3Configfm.cbDownMWC.Checked:=True;
      end
      else if (I=4) and (loopString[4]='0') then
      begin
        IQ3Configfm.cbDownMWC.Checked:=False;
      end;

      if (I=5) and (loopString[5]='1') then
      begin
        IQ3Configfm.cbPos3MWC.Checked:=True;
      end
      else if (I=5) and (loopString[5]='0') then
      begin
        IQ3Configfm.cbPos3MWC.Checked:=False;
      end;

      if (I=6) and (loopString[6]='1') then
      begin
        IQ3Configfm.cbPos2MWC.Checked:=True;
      end
      else if (I=6) and (loopString[6]='0') then
      begin
        IQ3Configfm.cbPos2MWC.Checked:=False;
      end;

      if (I=7) and (loopString[7]='1') then
      begin
        IQ3Configfm.cbPos1MWC.Checked:=True;
      end
      else if (I=7) and (loopString[7]='0') then
      begin
        IQ3Configfm.cbPos1MWC.Checked:=False;
      end;

      if (I=8) and (loopString[8]='1') then
      begin
        IQ3Configfm.cbupMWC.Checked:=True;
      end
      else if (I=8) and (loopString[8]='0') then
      begin
        IQ3Configfm.cbupMWC.Checked:=False;
      end;
    end;
  end;
end;

//Update channels on form when new UID is selected
procedure TIQ3Configfm.cmbUidMWCChange(Sender: TObject);
begin
//  ComFm.SetMWCParams(StrToInt(cmbUIDmwc.text),MWC_HaltTimer,'0','60',rhMWCStatusRecieved);
//  VariablesUn.delay(200);
//  ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),MWC_HaltTimer,'0','0',rhMWCStatusRecieved);
//  VariablesUn.delay(200);
  if (cmbUidMWC.ItemIndex <> -1) then
    VariablesUn.gsUIDCrntMWC := cmbUidMWC.Text;



  DataUn.dmDataModule.dsIQ3s.Filtered := False;
  DataUn.dmDataModule.dsIQ3s.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMWC.text;
  DataUn.dmDataModule.dsIQ3s.Filtered := True;
  if(Dataun.dmDataModule.dsIQ3s.FieldByName('EnableMessages').AsString='1')then
  begin
    cmbSTSAC.ItemIndex:= 0;

  end
  else
  begin
    cmbSTSAC.ItemIndex:= 1;
  end;
  cmbSTSACChange(nil);
  IQ3Configfm.cmbChannelIQ3Change(nil);
end;

//return handler for comport messages
procedure TIQ3Configfm.rhIQ3StatusRecieved;
var
  Whilecounter,i,ii,iii:integer;
  ChannelIndex,CurrentCH,stUID,stBinaryChannelsL,stBinaryChannelsM,GroupBin:string;
  ChannelData:Tstringlist;

begin
  ChannelData:=Tstringlist.Create();
  CurrentCH:=  cmbChannelIQ3.Text;
  try
    if(giSentCom = (StrToFloat(IntToStr(VariablesUn.giCommand)+'.'+IntToStr(VariablesUn.giCommandHi)))*1000) and (giUID = VariablesUn.giUID_rx) then
    begin
      case VariablesUn.giCommand of
        11:
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsIQ3s.Filtered := False;
          DataUn.dmDataModule.dsIQ3s.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC;
          DataUn.dmDataModule.dsIQ3s.Filtered := True;
          DataUn.dmDataModule.dsIQ3s.Edit;
          DataUn.dmDataModule.dsIQ3s.FieldByName('UID').AsString:=teUIDMWC.text;
          DataUn.dmDataModule.dsIQ3s.MergeChangeLog;
          DataUn.dmDataModule.dsMotors.Filtered := False;
          DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC;
          DataUn.dmDataModule.dsMotors.Filtered := True;
          DataUn.dmDataModule.dsMotors.Edit;
          DataUn.dmDataModule.dsMotors.FieldByName('UID').AsString:=teUIDMWC.text;
          DataUn.dmDataModule.dsMotors.MergeChangeLog;
          stUID:=DataUn.dmDataModule.dsIQ3s.FieldByName('UID').AsString;
          for i := 1 to 8 do
          begin
            DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
            DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC + ' and ChannelID = ' + IntToStr(i);
            DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
            DataUn.dmDataModule.dsIQ3Channels.Edit;
            DataUn.dmDataModule.dsIQ3Channels.FieldByName('UID').AsString := teUIDMWC.text;
            DataUn.dmDataModule.dsIQ3Channels.MergeChangeLog;
            stUID:= DataUn.dmDataModule.dsIQ3Channels.FieldByName('UID').AsString;
          end;
          for ii := 0 to IQ3Configfm.cmbUidMWC.Items.Count-1 do
          begin
            IQ3Configfm.cmbUidMWC.ItemIndex:=ii;
            if(IQ3Configfm.cmbUidMWC.text=VariablesUn.gsUIDCrntMWC) then
            begin
              IQ3Configfm.cmbUidMWC.Items.Delete(ii);
              IQ3Configfm.cmbUidMWC.Items.Add(teUIDMWC.Text);
              for iii := 0 to IQ3Configfm.cmbUidMWC.Items.Count-1 do
              begin
                IQ3Configfm.cmbUidMWC.ItemIndex:=iii;
                if(IQ3Configfm.cmbUidMWC.text=teUIDMWC.Text)then
                begin
                  VariablesUn.gsUIDCrntMWC:= teUIDMWC.Text;
                  break;
                end;
              end;
              break;
            end;
          end;
        end;

        51:                   // Save ZGN Channel 1
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
          DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC+' and ChannelID = 1';
          DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
          dmDataModule.dsIQ3Channels.Edit;
          DataUn.dmDataModule.dsIQ3Channels.FieldByName('ZGN').AsString := IntToStr(variablesUn.giDataH)+'.'+IntToStr(variablesUn.giDataM)+'.'+IntToStr(variablesUn.giDataL);
        end;
        55:                   // Save ZGN Channel 2
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
          DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC+' and ChannelID = 2';
          DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
          dmDataModule.dsIQ3Channels.Edit;
          DataUn.dmDataModule.dsIQ3Channels.FieldByName('ZGN').AsString := IntToStr(variablesUn.giDataH)+'.'+IntToStr(variablesUn.giDataM)+'.'+IntToStr(variablesUn.giDataL);
        end;
        59:                   // Save ZGN Channel 3
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
          DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC+' and ChannelID = 3';
          DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
          dmDataModule.dsIQ3Channels.Edit;
          DataUn.dmDataModule.dsIQ3Channels.FieldByName('ZGN').AsString := IntToStr(variablesUn.giDataH)+'.'+IntToStr(variablesUn.giDataM)+'.'+IntToStr(variablesUn.giDataL);
        end;
        63:                   // Save ZGN Channel 4
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
          DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC+' and ChannelID = 4';
          DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
          dmDataModule.dsIQ3Channels.Edit;
          DataUn.dmDataModule.dsIQ3Channels.FieldByName('ZGN').AsString := IntToStr(variablesUn.giDataH)+'.'+IntToStr(variablesUn.giDataM)+'.'+IntToStr(variablesUn.giDataL);
        end;
        67:                   // Save ZGN2 Channel 5
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
          DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC+' and ChannelID = 5';
          DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
          dmDataModule.dsIQ3Channels.Edit;
          DataUn.dmDataModule.dsIQ3Channels.FieldByName('ZGN2').AsString := IntToStr(variablesUn.giDataH)+'.'+IntToStr(variablesUn.giDataM)+'.'+IntToStr(variablesUn.giDataL);
        end;
        71:                   // Save ZGN2 Channel 6
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
          DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC+' and ChannelID = 6';
          DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
          dmDataModule.dsIQ3Channels.Edit;
          DataUn.dmDataModule.dsIQ3Channels.FieldByName('ZGN2').AsString := IntToStr(variablesUn.giDataH)+'.'+IntToStr(variablesUn.giDataM)+'.'+IntToStr(variablesUn.giDataL);
        end;
        75:                   // Save ZGN2 Channel 7
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
          DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC+' and ChannelID = 7';
          DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
          dmDataModule.dsIQ3Channels.Edit;
          DataUn.dmDataModule.dsIQ3Channels.FieldByName('ZGN2').AsString := IntToStr(variablesUn.giDataH)+'.'+IntToStr(variablesUn.giDataM)+'.'+IntToStr(variablesUn.giDataL);
        end;
        79:                   // Save ZGN2 Channel 8
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
          DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC+' and ChannelID = 8';
          DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
          dmDataModule.dsIQ3Channels.Edit;
          DataUn.dmDataModule.dsIQ3Channels.FieldByName('ZGN2').AsString := IntToStr(variablesUn.giDataH)+'.'+IntToStr(variablesUn.giDataM)+'.'+IntToStr(variablesUn.giDataL);
        end;
        85:                   // Alias
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsIQ3s.Filtered := False;
          DataUn.dmDataModule.dsIQ3s.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC;
          DataUn.dmDataModule.dsIQ3s.Filtered := True;
          dmDataModule.dsIQ3s.Edit;
          DataUn.dmDataModule.dsIQ3s.FieldByName('Alias').AsString := Trim(VariablesUn.gsMotorType);
          DataUn.dmDataModule.dsIQ3s.MergeChangeLog;
//          DataUn.dmDataModule.dsIQ3s.FieldByName('Alias').AsString := variablesUn.giDataH);
        end;
        199:
          case VariablesUn.giCommandHi of
            3:
            begin
              PassFlag:=True;
              stBinaryChannelsL:='';
              stBinaryChannelsL := VariablesUn.IntToBin(giDataL);
              DataUn.dmDataModule.dsIQ3s.Filtered := False;
              DataUn.dmDataModule.dsIQ3s.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + IntToStr(giUID);
              DataUn.dmDataModule.dsIQ3s.Filtered := True;
              dmDataModule.dsIQ3s.Edit;
              if(stBinaryChannelsL[1]='0') then
              begin
                DataUn.dmDataModule.dsIQ3s.FieldByName('FwdAddrFormat').AsString:='LS';
              end
              else if(stBinaryChannelsL[1]='1')and(stBinaryChannelsL[2]='0') then
              begin
                DataUn.dmDataModule.dsIQ3s.FieldByName('FwdAddrFormat').AsString:='IS';
              end
              else if(stBinaryChannelsL[1]='1')and(stBinaryChannelsL[2]='1') then
              begin
                DataUn.dmDataModule.dsIQ3s.FieldByName('FwdAddrFormat').AsString:='SS';
              end;
              Dataun.dmDataModule.dsIQ3s.FieldByName('dataH3').AsInteger:=giDataL;
              DataUn.dmDataModule.dsIQ3s.FieldByName('EnableMessages').AsString:=stBinaryChannelsL[8];

              DataUn.dmDataModule.dsIQ3s.MergeChangeLog;

            end;
            10:
            begin
              PassFlag:=True;
              stBinaryChannelsL:='';
              stBinaryChannelsM:='';
              stBinaryChannelsL := VariablesUn.IntToBin(giDataL);
              stBinaryChannelsM := VariablesUn.IntToBin(giDataM);
              DataUn.dmDataModule.dsIQ3s.Filtered := False;
              DataUn.dmDataModule.dsIQ3s.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC;
              DataUn.dmDataModule.dsIQ3s.Filtered := True;
              dmDataModule.dsIQ3s.Edit;
              DataUn.dmDataModule.dsIQ3s.FieldByName('Group1Channel').AsString := stBinaryChannelsM+stBinaryChannelsL;
            end;
            11:
            begin
              PassFlag:=True;
              stBinaryChannelsL:='';
              stBinaryChannelsM:='';
              stBinaryChannelsL := VariablesUn.IntToBin(giDataL);
              stBinaryChannelsM := VariablesUn.IntToBin(giDataM);
              DataUn.dmDataModule.dsIQ3s.Filtered := False;
              DataUn.dmDataModule.dsIQ3s.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC;
              DataUn.dmDataModule.dsIQ3s.Filtered := True;
              dmDataModule.dsIQ3s.Edit;
              DataUn.dmDataModule.dsIQ3s.FieldByName('Group2Channel').AsString := stBinaryChannelsM+stBinaryChannelsL;
            end;
            12:
            begin
              PassFlag:=True;
              stBinaryChannelsL:='';
              stBinaryChannelsM:='';
              stBinaryChannelsL := VariablesUn.IntToBin(giDataL);
              stBinaryChannelsM := VariablesUn.IntToBin(giDataM);
              DataUn.dmDataModule.dsIQ3s.Filtered := False;
              DataUn.dmDataModule.dsIQ3s.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC;
              DataUn.dmDataModule.dsIQ3s.Filtered := True;
              dmDataModule.dsIQ3s.Edit;
              DataUn.dmDataModule.dsIQ3s.FieldByName('Group3Channel').AsString := stBinaryChannelsM+stBinaryChannelsL;
            end;
            13:
            begin
              PassFlag:=True;
              stBinaryChannelsL:='';
              stBinaryChannelsM:='';
              stBinaryChannelsL := VariablesUn.IntToBin(giDataL);
              stBinaryChannelsM := VariablesUn.IntToBin(giDataM);
              DataUn.dmDataModule.dsIQ3s.Filtered := False;
              DataUn.dmDataModule.dsIQ3s.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC;
              DataUn.dmDataModule.dsIQ3s.Filtered := True;
              dmDataModule.dsIQ3s.Edit;
              DataUn.dmDataModule.dsIQ3s.FieldByName('Group4Channel').AsString := stBinaryChannelsM+stBinaryChannelsL;
            end;
            14:
            begin
              PassFlag:=True;
              DataUn.dmDataModule.dsIQ3s.Filtered := False;
              DataUn.dmDataModule.dsIQ3s.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC;
              DataUn.dmDataModule.dsIQ3s.Filtered := True;
              dmDataModule.dsIQ3s.Edit;
              if(giDataM=0)then
              begin
                DataUn.dmDataModule.dsIQ3s.FieldByName('GroupNightPos1').AsString := IntToStr(giDataL);
              end
              else if(giDataM=1)then
              begin
                DataUn.dmDataModule.dsIQ3s.FieldByName('GroupNightPos2').AsString := IntToStr(giDataL);
              end
              else if(giDataM=2)then
              begin
                DataUn.dmDataModule.dsIQ3s.FieldByName('GroupNightPos3').AsString := IntToStr(giDataL);
              end
              else if(giDataM=3)then
              begin
                DataUn.dmDataModule.dsIQ3s.FieldByName('GroupNightPos4').AsString := IntToStr(giDataL);
              end;
              DataUn.dmDataModule.dsIQ3Channels.MergeChangeLog;
            end;
            16:
            begin

              DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
              DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + intToStr(VariablesUn.giUID_rx)+' and ChannelID = ' +  intToStr(VariablesUn.giDataL+1);
              DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
              DataUn.dmDataModule.dsIQ3Channels.Edit;

              if(VariablesUn.gbBoundSet=True)then
              begin
                DataUn.dmDataModule.dsIQ3Channels.FieldByName('DeviceUID').AsString := EnOceanDialog1fm.EnOceanID;
                DataUn.dmDataModule.dsIQ3Channels.FieldByName('DeviceID').AsString := IntToStr(cmbDevicetype.ItemIndex+1);
                DataUn.dmDataModule.dsIQ3Channels.FieldByName('Bound').AsBoolean := True;
              end
              else
              begin
                DataUn.dmDataModule.dsIQ3Channels.FieldByName('Bound').AsBoolean := False;
                DataUn.dmDataModule.dsIQ3Channels.FieldByName('DeviceUID').AsString := '-1';
              end;
              PassFlag:=True;
              DataUn.dmDataModule.dsIQ3Channels.MergeChangeLog;
            end;
            17:
            begin
              PassFlag:=True;
              DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
              DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC+' and ChannelID = ' + IQ3Configfm.cmbChannelIQ3.Text;
              DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
              DataUn.dmDataModule.dsIQ3Channels.Edit;
              DataUn.dmDataModule.dsIQ3Channels.FieldByName('DeviceUID').AsString :=gsDataMWCAdd;
              DataUn.dmDataModule.dsIQ3Channels.FieldByName('Bound').AsBoolean := True;
              DataUn.dmDataModule.dsIQ3Channels.FieldByName('DeviceID').AsString:=IntToStr(VariablesUn.giDataH);
              if(vbDeviceTypeChange)then
              begin
                btDefaultAClick(nil);
              end
              else
              begin
                cmbChannelIQ3Change(nil);
              end;
            end;
            23:
            begin
              PassFlag:=True;
              Split('.',VariablesUn.gs64ByteData,ChannelData);
              ChannelIndex:=IntToStr(StrToInt(ChannelData[5])+1);
              for I := 0 to 5 do
              begin
                ChannelData.delete(0);
              end;

              DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
              DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + IntToStr(giUID) +  ' and ChannelID = ' + ChannelIndex;
              DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
              dmDataModule.dsIQ3Channels.Edit;
              DataUn.dmDataModule.dsIQ3Channels.FieldByName('paramID').AsString := ChannelData[37];
              DataUn.dmDataModule.dsIQ3Channels.FieldByName('ZGN2').AsString :=  ChannelData[28]+'.'+ ChannelData[29]+'.'+ ChannelData[30];
              DataUn.dmDataModule.dsIQ3Channels.FieldByName('ZGN').AsString := ChannelData[5]+'.'+ChannelData[6]+'.'+ ChannelData[7];
              if (ChannelData[0]<>'') or (ChannelData[1]<>'') or (ChannelData[2]<>'') or (ChannelData[3]<>'') then
              begin
                DataUn.dmDataModule.dsIQ3Channels.FieldByName('DeviceUID').AsString := ChannelData[0]+'.'+ ChannelData[1]+'.'+ ChannelData[2]+'.'+ ChannelData[3];
              end;
              DataUn.dmDataModule.dsIQ3Channels.FieldByName('DeviceID').AsString := ChannelData[4];

              //DataUn.dmDataModule.dsIQ3Channels.MergeChangeLog;
              //DataUn.dmDataModule.dsIQ3Channels.edit;
              if(DataUn.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asString = '2') or (DataUn.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asString = '4') or (DataUn.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asString = '5') or (DataUn.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asString = '6') or (DataUn.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asString = '7')then
              begin
                DataUn.dmDataModule.dsIQ3Channels.FieldByName('NetworkSwitchUID1').AsInteger := (StrToInt(ChannelData[9])*256)+StrToInt(ChannelData[8]);
                DataUn.dmDataModule.dsIQ3Channels.FieldByName('NetworkSwitchUID2').AsInteger := (StrToInt(ChannelData[11])*256)+StrToInt(ChannelData[10]);
                DataUn.dmDataModule.dsIQ3Channels.FieldByName('HysteresisPercent').AsInteger :=  StrToInt(ChannelData[31]);
                DataUn.dmDataModule.dsIQ3Channels.FieldByName('OCLoop').AsInteger := StrToInt(ChannelData[27]);
                DataUn.dmDataModule.dsIQ3Channels.FieldByName('DownTimeThreshold').AsInteger := (StrToInt(ChannelData[24])*256)+StrToInt(ChannelData[23]);
                DataUn.dmDataModule.dsIQ3Channels.FieldByName('UpTimeThreshold').AsInteger := (StrToInt(ChannelData[26])*256)+StrToInt(ChannelData[25]);
                DataUn.dmDataModule.dsIQ3Channels.FieldByName('MTAInhibit').AsInteger :=  StrToInt(ChannelData[32]);
              end
              else if(DataUn.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asString = '3') then
              begin
                DataUn.dmDataModule.dsIQ3Channels.FieldByName('nmPosition').AsInteger := StrToInt(ChannelData[22]);
                DataUn.dmDataModule.dsIQ3Channels.FieldByName('DownTimeThreshold').AsInteger := (StrToInt(ChannelData[24])*256)+StrToInt(ChannelData[23]);
                DataUn.dmDataModule.dsIQ3Channels.FieldByName('UpTimeThreshold').AsInteger := (StrToInt(ChannelData[26])*256)+StrToInt(ChannelData[25]);
              end
              else
              begin
                DataUn.dmDataModule.dsIQ3Channels.FieldByName('Threshold1').AsInteger := (StrToInt(ChannelData[9])*256)+StrToInt(ChannelData[8]);
                DataUn.dmDataModule.dsIQ3Channels.FieldByName('Threshold2').AsInteger := (StrToInt(ChannelData[11])*256)+StrToInt(ChannelData[10]);
                DataUn.dmDataModule.dsIQ3Channels.FieldByName('Threshold3').AsInteger := (StrToInt(ChannelData[13])*256)+StrToInt(ChannelData[12]);
                DataUn.dmDataModule.dsIQ3Channels.FieldByName('Threshold4').AsInteger := (StrToInt(ChannelData[15])*256)+StrToInt(ChannelData[14]);
                DataUn.dmDataModule.dsIQ3Channels.FieldByName('NmThreshold').AsInteger := (StrToInt(ChannelData[17])*256)+StrToInt(ChannelData[16]);
                DataUn.dmDataModule.dsIQ3Channels.FieldByName('ShadePosition').AsString := VariablesUn.IntToBin(StrToInt(ChannelData[18]));
                DataUn.dmDataModule.dsIQ3Channels.FieldByName('NtThresholdIn').AsInteger := StrToInt(ChannelData[20]);
                DataUn.dmDataModule.dsIQ3Channels.FieldByName('NtThresholdOut').AsInteger := StrToInt(ChannelData[21]);
                DataUn.dmDataModule.dsIQ3Channels.FieldByName('nmPosition').AsInteger := StrToInt(ChannelData[22]);
                DataUn.dmDataModule.dsIQ3Channels.FieldByName('DownTimeThreshold').AsInteger := (StrToInt(ChannelData[24])*256)+StrToInt(ChannelData[23]);
                DataUn.dmDataModule.dsIQ3Channels.FieldByName('UpTimeThreshold').AsInteger := (StrToInt(ChannelData[26])*256)+StrToInt(ChannelData[25]);
                DataUn.dmDataModule.dsIQ3Channels.FieldByName('OCLoop').AsInteger := StrToInt(ChannelData[27]);
                DataUn.dmDataModule.dsIQ3Channels.FieldByName('HysteresisPercent').AsInteger :=  StrToInt(ChannelData[31]);
                DataUn.dmDataModule.dsIQ3Channels.FieldByName('MTAInhibit').AsInteger :=  StrToInt(ChannelData[32]);
              end;

              DataUn.dmDataModule.dsIQ3Channels.MergeChangeLog;
//              PassFlag:=True;
//              DataUn.dmDataModule.dsIQ3s.Filtered := False;
//              DataUn.dmDataModule.dsIQ3s.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC;
//              DataUn.dmDataModule.dsIQ3s.Filtered := True;
//              dmDataModule.dsIQ3s.Edit;
//              Split('.',VariablesUn.gs64ByteData,stGroupChannels);
//              Whilecounter:=6;
//              while  Whilecounter <>14 do
//              begin
//                GroupBin:=VariablesUn.IntToBin(StrToInt(stGroupChannels[Whilecounter]));
//                GroupBin:=GroupBin+VariablesUn.IntToBin(StrToInt(stGroupChannels[Whilecounter+1]));
//
//                if(Whilecounter=6)then
//                begin
//                  DataUn.dmDataModule.dsIQ3s.FieldByName('Group1Channel').AsString:=GroupBin;
//                end
//                else if(Whilecounter=8)then
//                begin
//                  DataUn.dmDataModule.dsIQ3s.FieldByName('Group2Channel').AsString:=GroupBin;
//                end
//                else if(Whilecounter=10)then
//                begin
//                  DataUn.dmDataModule.dsIQ3s.FieldByName('Group3Channel').AsString:=GroupBin;
//                end
//                else if(Whilecounter=12)then
//                begin
//                  DataUn.dmDataModule.dsIQ3s.FieldByName('Group4Channel').AsString:=GroupBin;
//                end;
//                Whilecounter:=Whilecounter+2;
//              end;
            end;
            26,27:
            begin
             PassFlag:=True;

            end;
          end;
        196:
        begin
          if InRange(VariablesUn.giCommandHi,0,15)then
          Begin
            PassFlag:=True;
            DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
            DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC+' and ChannelID = ' + ChToUpdate;
            DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
            DataUn.dmDataModule.dsIQ3Channels.Edit;
            if(DataUn.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asString = '2') then
            begin
              DataUn.dmDataModule.dsIQ3Channels.FieldByName('NetworkSwitchUID1').AsInteger := VariablesUn.DataMidLowConvert;
              //DataUn.dmDataModule.dsIQ3Channels.MergeChangeLog;
            end
            else
            begin
              DataUn.dmDataModule.dsIQ3Channels.FieldByName('Threshold1').AsString := IntToStr(VariablesUn.DataMidLowConvert);
            end;
          End
          else if InRange(VariablesUn.giCommandHi,16,31)then
          Begin
            PassFlag:=True;
            DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
            DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC+' and ChannelID = ' + ChToUpdate;
            DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
            DataUn.dmDataModule.dsIQ3Channels.Edit;
            if(DataUn.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asString = '2') then
            begin
              DataUn.dmDataModule.dsIQ3Channels.FieldByName('NetworkSwitchUID2').AsInteger := VariablesUn.DataMidLowConvert;
              //DataUn.dmDataModule.dsIQ3Channels.MergeChangeLog;
            end
            else
            begin
              DataUn.dmDataModule.dsIQ3Channels.FieldByName('Threshold2').AsString := IntToStr(VariablesUn.DataMidLowConvert);
            end;
          End
          else if InRange(VariablesUn.giCommandHi,32,47)then
          Begin
            PassFlag:=True;
            DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
            DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC+' and ChannelID = ' + ChToUpdate;
            DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
            DataUn.dmDataModule.dsIQ3Channels.Edit;
            DataUn.dmDataModule.dsIQ3Channels.FieldByName('Threshold3').AsString := IntToStr(VariablesUn.DataMidLowConvert);
          End
          else if InRange(VariablesUn.giCommandHi,48,63) then
          Begin
            PassFlag:=True;
            DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
            DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC+' and ChannelID = ' + ChToUpdate;
            DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
            DataUn.dmDataModule.dsIQ3Channels.Edit;
            DataUn.dmDataModule.dsIQ3Channels.FieldByName('Threshold4').AsString := IntToStr(VariablesUn.DataMidLowConvert);
          End
          else if InRange(VariablesUn.giCommandHi,64,79) then
          Begin
            PassFlag:=True;
            DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
            DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC+' and ChannelID = ' + ChToUpdate;
            DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
            DataUn.dmDataModule.dsIQ3Channels.Edit;
            DataUn.dmDataModule.dsIQ3Channels.FieldByName('NMThreshold').AsString := IntToStr(VariablesUn.DataMidLowConvert);
          End
          else if InRange(VariablesUn.giCommandHi,80,95) then
          Begin
            PassFlag:=True;
            DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
            DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC+' and ChannelID = ' + ChToUpdate;
            DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
            DataUn.dmDataModule.dsIQ3Channels.Edit;
            DataUn.dmDataModule.dsIQ3Channels.FieldByName('ShadePosition').AsString := VariablesUn.IntToBin(VariablesUn.giDataL);
            ConvertShadePosToCB(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ShadePosition').asstring,Dataun.dmDataModule.dsIQ3Channels.FieldByName('OCLoop').asstring);
          End
          else if InRange(VariablesUn.giCommandHi,112,127) then
          Begin
            PassFlag:=True;
            DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
            DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC+' and ChannelID = ' + ChToUpdate;
            DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
            DataUn.dmDataModule.dsIQ3Channels.Edit;
            DataUn.dmDataModule.dsIQ3Channels.FieldByName('NtThresholdIn').AsString := IntToStr(VariablesUn.DataMidLowConvert);
          End
          else if InRange(VariablesUn.giCommandHi,128,143) then
          begin
            PassFlag:=True;
            DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
            DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC +  ' and ChannelID = ' + ChToUpdate;
            DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
            dmDataModule.dsIQ3Channels.Edit;
            DataUn.dmDataModule.dsIQ3Channels.FieldByName('NtThresholdOut').AsInteger := VariablesUn.giDataL;
          end
          else if InRange(VariablesUn.giCommandHi,144,159) then
          begin
            PassFlag:=True;
            DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
            DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC +  ' and ChannelID = ' + ChToUpdate;
            DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
            dmDataModule.dsIQ3Channels.Edit;
            DataUn.dmDataModule.dsIQ3Channels.FieldByName('nmPosition').AsInteger := VariablesUn.giDataL;
          end
          else if InRange(VariablesUn.giCommandHi,160,175) then
          begin
            PassFlag:=True;
            DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
            DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC +  ' and ChannelID = ' + ChToUpdate;
            DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
            dmDataModule.dsIQ3Channels.Edit;
            DataUn.dmDataModule.dsIQ3Channels.FieldByName('DownTimeThreshold').AsInteger := VariablesUn.DataMidLowConvert;
          end
          else if InRange(VariablesUn.giCommandHi,176,191) then
          begin
            PassFlag:=True;
            DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
            DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC +  ' and ChannelID = ' + ChToUpdate;
            DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
            dmDataModule.dsIQ3Channels.Edit;
            DataUn.dmDataModule.dsIQ3Channels.FieldByName('UpTimeThreshold').AsInteger := VariablesUn.DataMidLowConvert;
          end
          else if InRange(VariablesUn.giCommandHi,192,207) then
          begin
            PassFlag:=True;
            DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
            DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC +  ' and ChannelID = ' + ChToUpdate;
            DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
            dmDataModule.dsIQ3Channels.Edit;
            DataUn.dmDataModule.dsIQ3Channels.FieldByName('OCLoop').AsInteger := VariablesUn.giDataL;
          end
          else if InRange(VariablesUn.giCommandHi,208,223) then
          begin
            PassFlag:=True;
            DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
            DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC +  ' and ChannelID = ' + ChToUpdate;
            DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
            dmDataModule.dsIQ3Channels.Edit;
            DataUn.dmDataModule.dsIQ3Channels.FieldByName('HysteresisPercent').AsInteger := VariablesUn.giDataL;
          end
          else if InRange(VariablesUn.giCommandHi,224,239) then
          begin
            PassFlag:=True;
            DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
            DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC +  ' and ChannelID = ' + ChToUpdate;
            DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
            dmDataModule.dsIQ3Channels.Edit;
            DataUn.dmDataModule.dsIQ3Channels.FieldByName('MTAInhibit').AsInteger := VariablesUn.giDataL;
          end
          else if InRange(VariablesUn.giCommandHi,240,355) then
          begin
            PassFlag:=True;
            DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
            DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC +  ' and ChannelID = ' + ChToUpdate;
            DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
            dmDataModule.dsIQ3Channels.Edit;
            DataUn.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString := IntToStr(VariablesUn.giDataL);
            //DataUn.dmDataModule.dsIQ3Channels.MergeChangeLog;
          end;
          DataUn.dmDataModule.dsIQ3Channels.MergeChangeLog;
        end;
      end;
    end
    else if(giSentCom = 1000)then
    begin

      SetLength(viUIDs,Length(viUIDs)+1);
      viUIDs[Length(viUIDs)-1] := VariablesUn.giUID_rx;
      timerDelayUid:=timerDelayUid+1;
    end;
  finally
  end;
end;
//return to main IQ2 screen IQMainfm/IQMainUn
procedure TIQ3Configfm.btnMainIQ3Click(Sender: TObject);
begin
  Hide;
  DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
  DataUn.dmDataModule.dsIQ3s.SaveToFile(VariablesUn.gsDataPath + 'dsIQ3s.txt', dfXML);
  DataUn.dmDataModule.dsIQ3Channels.SaveToFile(VariablesUn.gsDataPath + 'dsIQ3Channels.txt', dfXML);
  if Assigned (rh_ShowMain) then
    rh_ShowMain(Self);
end;

procedure TIQ3Configfm.btnMainMCWMouseEnter(Sender: TObject);
begin
  pnlMenuMwc.Visible := True;
end;

//Binds device to mwc based on devide type
procedure TIQ3Configfm.btBindClick(Sender: TObject);
var
OutPutList: TStringList;
begin
  OutPutList := TStringList.Create;
  PassFlag:=False;
  PauseCount:=0;
  IQ3PassLoopFlag:=True;
  variablesUn.PassLoopFlag:=true;

  DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
  DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC +  ' and ChannelID = ' + cmbChannelIQ3.text;
  DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
  if(intToStr(cmbDeviceType.ItemIndex+1)<>Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsString)then
  begin
    vbDeviceTypeChange:=true;
  end
  else
  begin
    vbDeviceTypeChange:=false;
  end;

  //Bind
  if(cmbBindMethod.Text = 'Teach')or (Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asstring='2')then
  begin
    if(cmbDeviceType.ItemIndex+1<>0)then
    begin
      VariablesUn.gbBoundSet:= True;
      giUID := StrToInt(cmbUidMWC.text);
      giSentCom:=StrToFloat(VariablesUn.MWC_BindDeviceType)*1000;
      ComFm.SetMWCBoundDeviceType(StrToInt(VariablesUn.gsUIDCrntMWC), IntToStr(cmbDeviceType.ItemIndex+1),IntToStr(StrToInt(cmbChannelIQ3.Text)-1),rhIQ3StatusRecieved);//Set CHannel to deviceID and DeviceUID
      while not PassFlag do
      begin
        PauseCount:=PauseCount+1;
        VariablesUn.delay(10);
        if PauseCount>30000 then
        begin
          ShowMessage('Failed Command: '+VariablesUn.MWC_BindDeviceType);
          Break;
        end;
      end;
      PassFlag:=False;
      PauseCount:=0;
    end;
  end
  else if(cmbBindMethod.Text = 'Pair w/ Enocean')then
  begin
    //Change to bound. If changing to true must have DeviceID and Device UID
    if EnOceanDialog1fm.Execute then
    Begin
      Split('.', Trim(EnOceanDialog1fm.EnOceanID), OutPutList);
    End;
    OutPutList.Clear;
    Split('.', Trim(EnOceanDialog1fm.EnOceanID), OutPutList);
    if(EnOceanDialog1fm.EnOceanID<>'0.0.0.0')and (cmbDeviceType.ItemIndex+1<>0)and (OutPutList.Count=4)and(LastDelimiter('.',teDeviceUIDMWC.Text)<>Length(EnOceanDialog1fm.EnOceanID))and(ansipos('.',EnOceanDialog1fm.EnOceanID)<>1) then
    begin
      VariablesUn.gbBoundSet:= True;
      giUID := StrToInt(cmbUidMWC.text);
      giSentCom:=StrToFloat(VariablesUn.MWC_SetMWCBound)*1000;
      ComFm.SetMWCBound(StrToInt(VariablesUn.gsUIDCrntMWC), Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asInteger,IntToStr(cmbDeviceType.ItemIndex+1),IntToStr(StrToInt(cmbChannelIQ3.Text)-1),EnOceanDialog1fm.EnOceanID,rhIQ3StatusRecieved);//Set CHannel to deviceID and DeviceUID
      while not PassFlag do
      begin
        PauseCount:=PauseCount+1;
        VariablesUn.delay(10);
        if PauseCount=100 then
          ComFm.SetMWCBound(StrToInt(VariablesUn.gsUIDCrntMWC), Dataun.dmDataModule.dsIQ3Channels.FieldByName('ParamID').asInteger,IntToStr(cmbDeviceType.ItemIndex+1),IntToStr(StrToInt(cmbChannelIQ3.Text)-1),EnOceanDialog1fm.EnOceanID,rhIQ3StatusRecieved);//Set CHannel to deviceID and DeviceUID
        if PauseCount>300 then
        begin
          ShowMessage('Failed Command: '+VariablesUn.MWC_SetMWCBound);
          Break;
        end;
      end;
      PassFlag:=False;
      PauseCount:=0;
      cmbChannelIQ3Change(nil);
    end
    else if(LastDelimiter('.',EnOceanDialog1fm.EnOceanID)=Length(EnOceanDialog1fm.EnOceanID))or(ansipos('.',EnOceanDialog1fm.EnOceanID)=1)or(OutPutList.Count<>4) then
    begin
      ShowMessage('Bind Fail: Invalid EnOcean ID');
    end
    else if not(inRange(cmbDeviceType.ItemIndex+1,1,3)) then  //else if not(inRange(StrToInt(IQ3Configfm.teDeviceIDMWC.Text),1,3)) then
    begin
      ShowMessage('Bind Fail: Invalid Device Type');
    end;
  end;
end;

//share channel values with all the others assigned to it's group
//procedure TIQ3Configfm.btCarryOverChToGroupClick(Sender: TObject);
// var
//  viHi, viLo:Integer;
//  ChannelMath,cmbHoldPlace:string;
//  i,ii:Integer;
//  //test:Tstrings;
//  lsGroupChannels:String;
//  ItemList : TCombobox;
//begin
//  VariablesUn.PassLoopFlag:=True;
//  DataUn.dmDataModule.dsIQ3s.Filtered := False;
//  DataUn.dmDataModule.dsIQ3s.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC;
//  DataUn.dmDataModule.dsIQ3s.Filtered := True;
//  lsGroupChannels:=ReverseString(Dataun.dmDataModule.dsIQ3s.FieldByName('Group'+cmbGroupNumMWC.text+'Channel').AsString);
//  //cmbHoldPlace:= IQ3Configfm.cmbAvailCh.Text;
//  for I := 1 to 16 do
//  begin
//    if (lsGroupChannels[I]='1') and (IntToStr(i)<>cmbHoldPlace) then
//    begin
////    end;
////  end;
////  test:= cmbAvailCh.Items;
////  for i := 0 to test.Count-1 do
////  begin
////    if(test[i]<>cmbHoldPlace)then
////    begin
//      ChToUpdate:= IntToStr(i);
//      PassFlag:=False;
//      PauseCount:=0;
//
//      DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
//      DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC +  ' and ChannelID = ' + cmbHoldPlace;
//      DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
//      //set param ID
//      PassFlag:=False;
//      PauseCount:=0;
//      giUID := StrToInt(cmbUidMWC.text);
//      viHi := 0;
//      viLo := DataUn.dmDataModule.dsIQ3Channels.FieldByName('ParamID').AsInteger;
//      ChannelMath:=IntToStr((i-1)+240);
//      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
//      ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhMWCStatusRecieved);
//      while not PassFlag do
//      begin
//        PauseCount:=PauseCount+1;
//        VariablesUn.delay(10);
//        if PauseCount=100 then
//          ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhMWCStatusRecieved);
//        if PauseCount>300 then
//        begin
//          ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
//          Break;
//        end;
//      end;
//      PassFlag:=False;
//      PauseCount:=0;
//  //Set Threshhold 1
//      giUID := StrToInt(cmbUidMWC.text);
//      viHi := Hi(StrToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('Threshold1').AsString));
//      viLo := Lo(StrToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('Threshold1').AsString));
//      ChannelMath:=IntToStr(i-1);
//      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
//      ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhMWCStatusRecieved);
//      while not PassFlag do
//      begin
//        PauseCount:=PauseCount+1;
//        VariablesUn.delay(10);
//        if PauseCount=100 then
//          ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhMWCStatusRecieved);
//        if PauseCount>300 then
//        begin
//          ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
//          Break;
//        end;
//      end;
//      PassFlag:=False;
//      PauseCount:=0;
//  //Set Threshhold 2
//      DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
//      DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC +  ' and ChannelID = ' + cmbHoldPlace;
//      DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
//      giUID := StrToInt(cmbUidMWC.text);
//      viHi := Hi(StrToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('Threshold2').AsString));
//      viLo := Lo(StrToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('Threshold2').AsString));
//      ChannelMath:=IntToStr((i-1)+16);
//      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
//      ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhMWCStatusRecieved);
//      while not PassFlag do
//      begin
//        PauseCount:=PauseCount+1;
//        VariablesUn.delay(10);
//        if PauseCount=100 then
//          ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhMWCStatusRecieved);
//        if PauseCount>300 then
//        begin
//          ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
//          Break;
//        end;
//      end;
//      PassFlag:=False;
//      PauseCount:=0;
//  //Set Threshhold 3
//      DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
//      DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC +  ' and ChannelID = ' + cmbHoldPlace;
//      DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
//      giUID := StrToInt(cmbUidMWC.text);
//      viHi := Hi(StrToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('Threshold3').AsString));
//      viLo := Lo(StrToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('Threshold3').AsString));
//      ChannelMath:=IntToStr((i-1)+32);
//      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
//      ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhMWCStatusRecieved);
//      while not PassFlag do
//      begin
//        PauseCount:=PauseCount+1;
//        VariablesUn.delay(10);
//        if PauseCount=100 then
//          ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhMWCStatusRecieved);
//        if PauseCount>300 then
//        begin
//          ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
//          Break;
//        end;
//      end;
//      PassFlag:=False;
//      PauseCount:=0;
//  //Set Threshhold 4
//      DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
//      DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC +  ' and ChannelID = ' + cmbHoldPlace;
//      DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
//      giUID := StrToInt(cmbUidMWC.text);
//      viHi := Hi(StrToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('Threshold4').AsString));
//      viLo := Lo(StrToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('Threshold4').AsString));
//      ChannelMath:=IntToStr((i-1)+48);
//      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
//      ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhMWCStatusRecieved);
//      while not PassFlag do
//      begin
//        PauseCount:=PauseCount+1;
//        VariablesUn.delay(10);
//        if PauseCount=100 then
//          ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhMWCStatusRecieved);
//        if PauseCount>300 then
//        begin
//          ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
//          Break;
//        end;
//      end;
//      PassFlag:=False;
//      PauseCount:=0;
//  //Set Night Threshhold
//      DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
//      DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC +  ' and ChannelID = ' + cmbHoldPlace;
//      DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
//      giUID := StrToInt(cmbUidMWC.Text);
//      viHi := Hi(StrToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('NmThreshold').AsString));
//      viLo := Lo(StrToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('NmThreshold').AsString));
//      ChannelMath:=IntToStr((i-1)+64);
//      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
//      ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhMWCStatusRecieved);
//      while not PassFlag do
//      begin
//        PauseCount:=PauseCount+1;
//        VariablesUn.delay(10);
//        if PauseCount=100 then
//          ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhMWCStatusRecieved);
//        if PauseCount>300 then
//        begin
//          ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
//          Break;
//        end;
//      end;
//      PassFlag:=False;
//      PauseCount:=0;
//  //Set enabled POS, Night Mode, Retract Mode
//      DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
//      DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC +  ' and ChannelID = ' + cmbHoldPlace;
//      DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
//      giUID := StrToInt(cmbUidMWC.Text);
//      viHi := 0;
//      viLo := VariablesUn.BinToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('ShadePosition').AsString);
//      ChannelMath:=IntToStr((i-1)+80);
//      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
//      ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhMWCStatusRecieved);
//      while not PassFlag do
//      begin
//        PauseCount:=PauseCount+1;
//        VariablesUn.delay(10);
//        if PauseCount=100 then
//          ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhMWCStatusRecieved);
//        if PauseCount>300 then
//        begin
//          ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
//          Break;
//        end;
//      end;
//      PassFlag:=False;
//      PauseCount:=0;
//  //Set Night Time Threshold IN
//      DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
//      DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC +  ' and ChannelID = ' + cmbHoldPlace;
//      DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
//      giUID := StrToInt(cmbUidMWC.text);
//      viHi := 0;
//      viLo := StrToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('NtThresholdIn').AsString);
//      ChannelMath:=IntToStr((i-1)+112);
//      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
//      ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhMWCStatusRecieved);
//      while not PassFlag do
//      begin
//        PauseCount:=PauseCount+1;
//        VariablesUn.delay(10);
//        if PauseCount=100 then
//          ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhMWCStatusRecieved);
//        if PauseCount>300 then
//        begin
//          ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
//          Break;
//        end;
//      end;
//      PassFlag:=False;
//      PauseCount:=0;
//
//  //Set Night Time Threshold Out
//      DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
//      DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC +  ' and ChannelID = ' + cmbHoldPlace;
//      DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
//      giUID := StrToInt(cmbUidMWC.text);
//      viHi := 0;
//      viLo := StrToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('NtThresholdOut').AsString);
//      ChannelMath:=IntToStr((i-1)+128);
//      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
//      ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhMWCStatusRecieved);
//      while not PassFlag do
//      begin
//        PauseCount:=PauseCount+1;
//        VariablesUn.delay(10);
//        if PauseCount=100 then
//          ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhMWCStatusRecieved);
//        if PauseCount>300 then
//        begin
//          ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
//          Break;
//        end;
//      end;
//      PassFlag:=False;
//      PauseCount:=0;
//  //Set Night Position
//      DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
//      DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC +  ' and ChannelID = ' + cmbHoldPlace;
//      DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
//      giUID := StrToInt(cmbUidMWC.Text);
//      viHi := 0;
//      viLo := StrToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('nmPosition').AsString);
//      ChannelMath:=IntToStr((i-1)+144);
//      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
//      ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhMWCStatusRecieved);
//      while not PassFlag do
//      begin
//        PauseCount:=PauseCount+1;
//        VariablesUn.delay(10);
//        if PauseCount=100 then
//          ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhMWCStatusRecieved);
//        if PauseCount>300 then
//        begin
//          ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
//          Break;
//        end;
//      end;
//      PassFlag:=False;
//      PauseCount:=0;
//  //Set Shade Down Time Threshold
//      DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
//      DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC +  ' and ChannelID = ' + cmbHoldPlace;
//      DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
//      giUID := StrToInt(cmbUidMWC.text);
//      viHi := Hi(StrToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('DownTimeThreshold').AsString));
//      viLo := Lo(StrToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('DownTimeThreshold').AsString));
//      ChannelMath:=IntToStr((i-1)+160);
//      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
//      ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhMWCStatusRecieved);
//      while not PassFlag do
//      begin
//        PauseCount:=PauseCount+1;
//        VariablesUn.delay(10);
//        if PauseCount=100 then
//          ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhMWCStatusRecieved);
//        if PauseCount>300 then
//        begin
//          ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
//          Break;
//        end;
//      end;
//      PassFlag:=False;
//      PauseCount:=0;
//  //Set Shade Up Time Threshold
//      DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
//      DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC +  ' and ChannelID = ' + cmbHoldPlace;
//      DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
//      giUID := StrToInt(cmbUidMWC.text);
//      viHi := Hi(StrToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('UpTimeThreshold').AsString));
//      viLo := Lo(StrToInt(Dataun.dmDataModule.dsIQ3Channels.FieldByName('UpTimeThreshold').AsString));
//      ChannelMath:=IntToStr((i-1)+176);
//      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
//      ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhMWCStatusRecieved);
//      while not PassFlag do
//      begin
//        PauseCount:=PauseCount+1;
//        VariablesUn.delay(10);
//        if PauseCount=100 then
//          ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhMWCStatusRecieved);
//        if PauseCount>300 then
//        begin
//          ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
//          Break;
//        end;
//      end;
//      PassFlag:=False;
//      PauseCount:=0;
//  //Set Open/closed loop
//      DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
//      DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC +  ' and ChannelID = ' + cmbHoldPlace;
//      DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
//      viLo := Dataun.dmDataModule.dsIQ3Channels.FieldByName('OCLoop').AsInteger;
//      giUID := StrToInt(cmbUidMWC.text);
//      viHi := 0;
//      //viLo := IfThen(cbClosedLoopMWC.Checked,1,0);
//      ChannelMath:=IntToStr((i-1)+192);
//      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
//      ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhMWCStatusRecieved);
//      while not PassFlag do
//      begin
//        PauseCount:=PauseCount+1;
//        VariablesUn.delay(10);
//        if PauseCount=100 then
//          ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), IntToStr(viLo),rhMWCStatusRecieved);
//        if PauseCount>300 then
//        begin
//          ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
//          Break;
//        end;
//      end;
//      PassFlag:=False;
//      PauseCount:=0;
//  //Set Hysteresis
//      DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
//      DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC +  ' and ChannelID = ' + cmbHoldPlace;
//      DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
//      giUID := StrToInt(cmbUidMWC.text);
//      viHi := 0;
//      //viLo := Lo(StrToInt(teShadeUpMWC.));
//      ChannelMath:=IntToStr((i-1)+208);
//      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
//      ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), Dataun.dmDataModule.dsIQ3Channels.FieldByName('HysteresisPercent').AsString,rhMWCStatusRecieved);
//      while not PassFlag do
//      begin
//          PauseCount:=PauseCount+1;
//          VariablesUn.delay(10);
//          if PauseCount=100 then
//            ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), Dataun.dmDataModule.dsIQ3Channels.FieldByName('HysteresisPercent').AsString,rhMWCStatusRecieved);
//          if PauseCount>300 then
//          begin
//            ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
//            Break;
//          end;
//      end;
//      PassFlag:=False;
//      PauseCount:=0;
//  //Set MTA Inhibit Timer
//      DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
//      DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrntMWC +  ' and ChannelID = ' + cmbHoldPlace;
//      DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
//      giUID := StrToInt(cmbUidMWC.text);
//      viHi := 0;
//      ChannelMath:=IntToStr((i-1)+224);
//      giSentCom:=StrToFloat(VariablesUn.MWC_ChannelStart+ChannelMath)*1000;
//      ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), Dataun.dmDataModule.dsIQ3Channels.FieldByName('MTAInhibit').AsString,rhMWCStatusRecieved);
//      while not PassFlag do
//      begin
//        PauseCount:=PauseCount+1;
//        VariablesUn.delay(10);
//        if PauseCount=100 then
//          ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC),(VariablesUn.MWC_ChannelStart+ChannelMath),IntToStr(viHi), Dataun.dmDataModule.dsIQ3Channels.FieldByName('MTAInhibit').AsString,rhMWCStatusRecieved);
//        if PauseCount>300 then
//        begin
//          ShowMessage('Failed Command: '+ VariablesUn.MWC_ChannelStart+ChannelMath);
//          Break;
//        end;
//      end;
//      PassFlag:=False;
//      PauseCount:=0;
//    end;
//  end;
//  ChToUpdate:='';
//  cmbChannelMWCChange(nil);
//  UpdateGroupCB;
//end;

//Reset channel to default parameters
procedure TIQ3Configfm.btDefaultAClick(Sender: TObject);
var
  command:string;
begin
  ChToUpdate:= IQ3Configfm.cmbChannelIQ3.text;
  PassFlag:=False;
  PauseCount:=0;
  giUID := StrToInt(cmbUidMWC.text);
  giSentCom := StrToFloat(VariablesUn.MWC_DefaultA)*1000;
  ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC), VariablesUn.MWC_DefaultA,IntToStr(cmbDeviceType.ItemIndex+1),IntToStr(StrToInt(IQ3Configfm.cmbChannelIQ3.text)-1),rhIQ3StatusRecieved);
  while not PassFlag do
  begin
      PauseCount:=PauseCount+1;
      VariablesUn.delay(10);
      if PauseCount=100 then
      ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC), VariablesUn.MWC_DefaultA,IntToStr(cmbDeviceType.ItemIndex+1),IntToStr(StrToInt(IQ3Configfm.cmbChannelIQ3.text)-1),rhIQ3StatusRecieved);
      if PauseCount>300 then
      begin
        //ShowMessage('Failed Command: '+ VariablesUn.MWC_DefaultA);
        Break;
      end;
  end;

  PassFlag:=False;
  PauseCount:=0;
  giUID := StrToInt(cmbUidMWC.text);
  giSentCom := StrToFloat(VariablesUn.MWC_ChannelStart+IntToStr(240+(cmbChannelIQ3.itemIndex)))*1000;
  command := VariablesUn.MWC_ChannelStart+IntToStr(240+(cmbChannelIQ3.itemIndex));
  ComFm.GetMWCStatus(giUID, command, rhIQ3StatusRecieved);
  while not PassFlag do
  begin
      PauseCount:=PauseCount+1;
      VariablesUn.delay(10);
      if PauseCount=100 then
      ComFm.GetMWCStatus(giUID, command, rhIQ3StatusRecieved);
      if PauseCount>300 then
      begin
        //ShowMessage('Failed Command: '+ command);
        Break;
      end;
  end;
  PassFlag:=False;
  PauseCount:=0;
  UpdateChannel();
  cmbChannelIQ3Change(nil);
  ChToUpdate:= '';
end;
//Return channel to alternate default parameters
procedure TIQ3Configfm.btDefaultBClick(Sender: TObject);
var
  command:String;
begin
  ChToUpdate:= IQ3Configfm.cmbChannelIQ3.text;
  PassFlag:=False;
  giUID := StrToInt(cmbUidMWC.text);
  giSentCom := StrToFloat(VariablesUn.MWC_DefaultB)*1000;
  ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC), VariablesUn.MWC_DefaultB,IntToStr(cmbDeviceType.ItemIndex+1),IntToStr(StrToInt(IQ3Configfm.cmbChannelIQ3.text)-1),rhIQ3StatusRecieved);
  PauseCount:=0;
  while not PassFlag do
  begin
      PauseCount:=PauseCount+1;
      VariablesUn.delay(10);
      if PauseCount=100 then
        ComFm.SetMWCParams(StrToInt(VariablesUn.gsUIDCrntMWC), VariablesUn.MWC_DefaultB,IntToStr(cmbDeviceType.ItemIndex+1),IntToStr(StrToInt(IQ3Configfm.cmbChannelIQ3.text)-1),rhIQ3StatusRecieved);
      if PauseCount>300 then
      begin
        //ShowMessage('Failed Command: '+ command);
        Break;
      end;
  end;
  PassFlag:=False;
  PauseCount:=0;
   giUID := StrToInt(cmbUidMWC.text);
  giSentCom := StrToFloat(VariablesUn.MWC_ChannelStart+IntToStr(240+(cmbChannelIQ3.itemIndex)))*1000;
  command := VariablesUn.MWC_ChannelStart+IntToStr(240+(cmbChannelIQ3.itemIndex));
  ComFm.GetMWCStatus(giUID, command, rhIQ3StatusRecieved);
  while not PassFlag do
  begin
      PauseCount:=PauseCount+1;
      VariablesUn.delay(10);
      if PauseCount=100 then
      ComFm.GetMWCStatus(giUID, command, rhIQ3StatusRecieved);
      if PauseCount>300 then
      begin
        //ShowMessage('Failed Command: '+ command);
        Break;
      end;
  end;
  PassFlag:=False;
  PauseCount:=0;
  UpdateChannel();
  cmbChannelIQ3Change(nil);
  ChToUpdate:= '';
end;

procedure TIQ3Configfm.btnBackClick(Sender: TObject);
var
  I:Integer;
begin
//  for I := 0 to Length(viUIDOnShow)-1 do
//  begin
//    ComFm.SetMWCParams(viUIDOnShow[I],MWC_HaltTimer,'0','0',rhIQ3StatusRecieved);
//    VariablesUn.delay(200);
//  end;
  giUIDItemIndex:=cmbUIDMWC.ItemIndex;
  giLastChannel:=cmbChannelIQ3.ItemIndex;
  if Assigned (rh_ShowIQ3RFDisplay) then
    rh_ShowIQ3RFDisplay(Self);
  Hide;
end;

procedure TIQ3Configfm.btnConfigMCWMouseEnter(Sender: TObject);
begin
  pnlMenuMwc.Visible := True;
end;

procedure TIQ3Configfm.btnConfigMouseEnter(Sender: TObject);
begin
  pnlMenuMwc.Visible := True;
end;

procedure TIQ3Configfm.btnControlMouseEnter(Sender: TObject);
begin
  pnlMenuMwc.Visible := True;
end;

procedure TIQ3Configfm.btnFindMouseEnter(Sender: TObject);
begin
  pnlMenuMwc.Visible := True;
end;

procedure TIQ3Configfm.btnMCWMCWMouseEnter(Sender: TObject);
begin
  pnlMenuMwc.Visible := True;
end;

procedure TIQ3Configfm.btnInfoClick(Sender: TObject);
begin
  pnlMenuMWC.Visible := False;
  Hide;
  AboutFm.rh_ShowForm := rhShowMwc;
  AboutFm.Show;
end;

procedure TIQ3Configfm.btnInfoMouseEnter(Sender: TObject);
begin
  pnlMenuMwc.Visible := True;
end;
procedure TIQ3Configfm.btnLimitSettingClick(Sender: TObject);
begin
  pnlMenuMwc.Visible := False;
  LimitSettingFm.rh_showmain := rhShowMWC;
  Hide;
  LimitSettingFm.Show;
end;

procedure TIQ3Configfm.btnLimitSettingMouseEnter(Sender: TObject);
begin
  pnlMenuMwc.Visible := True;
end;

//close program and save data
procedure TIQ3Configfm.btnExitClick(Sender: TObject);
var
  I:Integer;
begin
//  for I := 0 to Length(viUIDOnShow)-1 do
//  begin
//    ComFm.SetMWCParams(viUIDOnShow[I],MWC_HaltTimer,'0','0',rhMWCStatusRecieved);
//    VariablesUn.delay(200);
//  end;
//  gipauseLoopExit:=true;
//  variablesUn.PassLoopFlag:=true;
//  while not giloopExit do
//  begin
//    variablesUn.Delay(10);
//  end;
  DataUn.dmDataModule.dsMotors.Filtered := False;
  DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
  DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
  DataUn.dmDataModule.dsIQ3s.Filtered := False;
  DataUn.dmDataModule.dsMWCs.Filtered := False;
  DataUn.dmDataModule.dsMNI.Filtered := False;
  DataUn.dmDataModule.dsMNI2.Filtered := False;
  DataUn.dmDataModule.dsIQMLC2.Filtered := False;
  DataUn.dmDataModule.dsPowerPanel.Filtered := False;

  DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
  DataUn.dmDataModule.dsMWCs.SaveToFile(VariablesUn.gsDataPath + 'dsIQ3s.txt', dfXML);
  DataUn.dmDataModule.dsIQ3s.SaveToFile(VariablesUn.gsDataPath + 'dsIQ3s.txt', dfXML);
  DataUn.dmDataModule.dsIQ3Channels.SaveToFile(VariablesUn.gsDataPath + 'dsIQ3Channels.txt', dfXML);
  DataUn.dmDataModule.dsIQ3Channels.SaveToFile(VariablesUn.gsDataPath + 'dsIQ3Channels.txt', dfXML);
  DataUn.dmDataModule.dsMNI.SaveToFile(VariablesUn.gsDataPath + 'dsMNI.txt', dfXML);
  DataUn.dmDataModule.dsMNI2.SaveToFile(VariablesUn.gsDataPath + 'dsMNI2.txt', dfXML);
  DataUn.dmDataModule.dsIQMLC2.SaveToFile(VariablesUn.gsDataPath + 'dsIQMLC2.txt', dfXML);
  DataUn.dmDataModule.dsPowerPanel.SaveToFile(VariablesUn.gsDataPath + 'dsPowerPanel.txt', dfXML);
  pnlMenuMwc.Visible := False;
  Application.Terminate;
end;

//saves file to csv. Needs full path as 'myfilename;. Not implimented currently
procedure TIQ3Configfm.SaveAsCSV(myFileName: string; myChannelDataSet,myMWCDataSet: TDataSet);
var
  myTextFile: TextFile;
  i,ii,LoopCounter: integer;
  s: string;
  HeaderData:TStringList;
begin
  HeaderData:=TStringList.Create;
  HeaderData.Add('ADDRESS TABLE (X.X),MWCSoftware,Revision 1.0,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,');
  HeaderData.Add('PROJECT:,'+myMWCDataSet.Fields[1].AsString+',,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,');
  HeaderData.Add(',,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,');
  HeaderData.Add('DATE:,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,');
  HeaderData.Add('Project Manager:,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,');
  HeaderData.Add('Created By:,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,');
  HeaderData.Add('Serial No:,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,');
  HeaderData.Add('Dealer:,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,');
  HeaderData.Add('WO:,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,');
  HeaderData.Add('DWG No:,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,');
  HeaderData.Add(',,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,');
  HeaderData.Add(',,,,,,Group Params,,,,,,,Daylight Sensor Params,,,,,,,,,,,,,,,,,,,Switch Parameters,,,,,');
  HeaderData.Add('Select Export,Room Name/Number,ZONE,DEVICE UID#,ST/SAC,Device Type,Channel,Group1,Group2,Group3,Group4,ZGN,Group Night POS,Up,Preset1,Preset2,Preset 3,Down,Threshold 1,Threshold 2,Threshold 3,Threshold 4,Hysteresis,');
  HeaderData.Add('Time Up,Time Down,Return To Automation,Threshold Night,Enter Night Mode Timer,Exit Night Mode Timer,Retract Mode,Closed Loop,Night Position,Switch ZGN1,Switch ZGN2,Switch Mode,Forward Address,Switch Personality,Fwd Addr Format');
  //create a new file
  AssignFile(myTextFile, myFileName);
  Rewrite(myTextFile);

  s := ''; //initialize empty string
  try
    //write Address table header (as column headers)
    for i := 0 to HeaderData.Count - 1 do
    begin
      if(i<12) then
      begin
        Writeln(myTextFile, HeaderData[i]);
      end
      else
      begin
        s:=s+HeaderData[i];
        if(i=13)then
          Writeln(myTextFile, s);
      end;

    end;
    myMWCDataSet.First;
    myMWCDataSet.Next;
    LoopCounter:=0;
    while not myMWCDataSet.Eof do
    begin
      s := '';
      //if(loopcounter=0)then
      //begin
        s := s + Format(',,,%s,', [myMWCDataSet.Fields[0].AsString]);
        if(myMWCDataSet.Fields[19].AsString='1')then
        begin
          s := s + 'SAC,';
        end
        else
        begin
          s := s + 'ST,';
        end;
        if(myChannelDataSet.Fields[21].AsString='1') then
        begin
          s := s + 'Daylight Sensor,';
        end
        else if(myChannelDataSet.Fields[21].AsString='2') then
        begin
          s := s + 'Switch,';
        end
        else if(myChannelDataSet.Fields[21].AsString='2') then
        begin
          s := s + 'Occupancy Sensor,';
        end;
        s := s + Format('%s,', [myChannelDataSet.Fields[2].AsString]);
        if myMWCDataSet.Fields[0].AsString[16]='1' then
        begin

      //else if i=2 then
      //begin

       end;

      Writeln(myTextfile, s);
      myMWCDataSet.Next;
      //LoopCounter:=LoopCounter+1;
    end;

  finally
    CloseFile(myTextFile);
  end;
end;

procedure TIQ3Configfm.btnExitMouseEnter(Sender: TObject);
begin
  pnlMenuMwc.Visible := True;
end;

// go to discover page and find devices
procedure TIQ3Configfm.btnFindClick(Sender: TObject);
begin
  IQ3PassLoopFlag:=true;
  variablesUn.PassLoopFlag:=true;
  pnlMenuMWC.Visible := False;
  IQ3Configfm.Hide;
  Discoverfm.rh_showIQ3:=rhShowMWC;
  DiscoverFm.Show;
end;

procedure TIQ3Configfm.btnMenuMcw1MouseEnter(Sender: TObject);
begin
  pnlMenuMwc.Visible := True;
end;

procedure TIQ3Configfm.pnlMenuMwcMouseLeave(Sender: TObject);
begin
 pnlMenuMwc.Visible := False;
end;
procedure TIQ3Configfm.pnlSwitchClick(Sender: TObject);
begin

end;

//Update all form visual indicators

Procedure TIQ3Configfm.ThreadLoop();
var
  loopcount:Integer;
begin
  while not(IQ3PassLoopFlag)do
  begin
    if(VariablesUn.giTelegramCounter<>0)then
    begin
      lbTelegramCounter.Caption:=IntToStr(VariablesUn.giTelegramCounter);
    end
    else
    begin
      lbTelegramCounter.Caption:='-';
    end;
    if(VariablesUn.giCapVoltage>2.5)and(VariablesUn.giCapVoltage<3)then
    begin
      shape6.brush.Color:=cllime;
      shape7.brush.Color:=clWhite;
      shape8.brush.Color:=clWhite;
      shape9.brush.Color:=clWhite;
    end
    else if(VariablesUn.giCapVoltage>=3)and(VariablesUn.giCapVoltage<3.5)then
    begin
      shape6.brush.Color:=cllime;
      shape7.brush.Color:=cllime;
      shape8.brush.Color:=clWhite;
      shape9.brush.Color:=clWhite;
    end
    else if(VariablesUn.giCapVoltage>=3.5)and(VariablesUn.giCapVoltage<4)then
    begin
      shape6.brush.Color:=cllime;
      shape7.brush.Color:=cllime;
      shape8.brush.Color:=cllime;
      shape9.brush.Color:=clWhite;
    end
    else if(VariablesUn.giCapVoltage>=4)then
    begin
      shape6.brush.Color:=cllime;
      shape7.brush.Color:=cllime;
      shape8.brush.Color:=cllime;
      shape9.brush.Color:=cllime;
    end
    else if(VariablesUn.giCapVoltage=2.5)then
    begin
      shape6.brush.Color:=clred;
      shape7.brush.Color:=clred;
      shape8.brush.Color:=clred;
      shape9.brush.Color:=clred;
    end;
    if(VariablesUn.giSignalStrength>-95)and(VariablesUn.giSignalStrength<-80)then
    begin
      shape13.brush.Color:=clred;
      shape12.brush.Color:=clWhite;
      shape11.brush.Color:=clWhite;
      shape10.brush.Color:=clWhite;
    end
    else if(VariablesUn.giSignalStrength>=-80)and(VariablesUn.giSignalStrength<-70)then
    begin
      shape13.brush.Color:=clyellow;
      shape12.brush.Color:=clyellow;
      shape11.brush.Color:=clWhite;
      shape10.brush.Color:=clWhite;
    end
    else if(VariablesUn.giSignalStrength>=-70)and(VariablesUn.giSignalStrength<-50)then
    begin
      shape13.brush.Color:=cllime;
      shape12.brush.Color:=cllime;
      shape11.brush.Color:=cllime;
      shape10.brush.Color:=clWhite;
    end
    else if(VariablesUn.giSignalStrength>=-50)and(VariablesUn.giSignalStrength<0)then
    begin
      shape13.brush.Color:=cllime;
      shape12.brush.Color:=cllime;
      shape11.brush.Color:=cllime;
      shape10.brush.Color:=cllime;
    end
    else
    begin
      shape13.brush.Color:=clred;
      shape12.brush.Color:=clred;
      shape11.brush.Color:=clred;
      shape10.brush.Color:=clred;
    end;
    loopcount:=10000;
    while loopcount>0 do
    begin
      variablesUn.Delay(10);
      loopcount:=loopcount-10;
      if IQ3PassLoopFlag then
        Exit;
    end;

  end;
end;

//Updates non channel parameters on form load
Procedure TIQ3Configfm.ShowUID;
var
  loopString:string;
  I,ii:Integer;
  comboUIDList:TStringList;
begin
  comboUIDList:=TStringList.Create;
  cmbUidMWC.Clear;
  try
  begin
    if not (DataUn.dmDataModule.dsIQ3s.State = dsInactive) then
    begin
      DataUn.dmDataModule.dsIQ3s.Filtered := False;
      DataUn.dmDataModule.dsIQ3s.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
      DataUn.dmDataModule.dsIQ3s.Filtered := True;
    end;
    Setlength(viUIDOnShow,0);
//    if(DataUn.dmDataModule.dsIQ3s.RecordCount=0)then
//    begin
//      ShowMessage('No MWCs Found After Scan. Returning to MTR page.');
//      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('MTR');
//      cmbDeviceSelectorChange(nil);
//      result:= false;
//      exit;
//    end;
//    result:= True;
    while not dmDataModule.dsIQ3s.Eof do
    begin
     //cmbUidMWC.Items.Add(IntToStr(DataUn.dmDataModule.dsIQ3s.FieldByName('UID').AsInteger));
     comboUIDList.add(IntToStr(DataUn.dmDataModule.dsIQ3s.FieldByName('UID').AsInteger));
     SetLength(viUIDOnShow,Length(viUIDOnShow)+1);
     viUIDOnShow[Length(viUIDOnShow)-1] := DataUn.dmDataModule.dsIQ3s.FieldByName('UID').AsInteger;
     dmDataModule.dsIQ3s.Next;
    end;
    SortStringListUIDS(comboUIDList);
    cmbUidMWC.Items.Assign(comboUIDList);
    if(cmbUidMWC.Items.Count >0) then
    begin
      cmbUidMWC.ItemIndex := 0;
      VariablesUn.gsUIDCrntMWC := cmbUidMWC.Text;

      DataUn.dmDataModule.dsIQ3s.Filtered := False;
      DataUn.dmDataModule.dsIQ3s.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMWC.text;
      DataUn.dmDataModule.dsIQ3s.Filtered := True;
      pnlAdapterRevMWC.Caption :=  DataUn.dmDataModule.dsIQ3s.FieldByName('AdapterFirmware').AsString;
      pnlFirmwareRevMWC.Caption :=  DataUn.dmDataModule.dsIQ3s.FieldByName('BoardFirmware').AsString;
      pnBaseRevMWC.Caption := DataUn.dmDataModule.dsIQ3s.FieldByName('BaseFirmRev').AsString;
      if(DataUn.dmDataModule.dsIQ3s.FieldByName('FwdAddrFormat').AsString='IS')then
      begin
        IQ3Configfm.cmbFwdAddrFormat.ItemIndex:=0;
      end
      else if(DataUn.dmDataModule.dsIQ3s.FieldByName('FwdAddrFormat').AsString='LS')then
      begin
        IQ3Configfm.cmbFwdAddrFormat.ItemIndex:=1;
      end;
      if(DataUn.dmDataModule.dsIQ3s.FieldByName('EnableMessages').AsString='1')then
      begin
        IQ3Configfm.cmbSTSAC.ItemIndex:=0;
      end
      else
      begin
        IQ3Configfm.cmbSTSAC.ItemIndex:=1;
      end;
    end;
    DataUn.dmDataModule.dsIQ3s.Filtered := False;
  end;
  except
  on E: Exception do
  begin
    MessageDlg('TIQMainFm.ShowProject: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;

procedure TIQ3Configfm.teAliasChange(Sender: TObject);
begin
  teAlias.Brush.Color:=clLime;
  teAlias.Refresh;
  btUpdateParams.Color:=clLime;
end;

procedure TIQ3Configfm.teAliasDblClick(Sender: TObject);
begin
  NewProjectFm.rh_ShowLast:=rhShowMWC;
  NewProjectFm.Show;
  Hide;
end;

procedure TIQ3Configfm.teAliasKeyPress(Sender: TObject; var Key: Char);
begin
//  if key = #$D then
//  begin
//    txtAliasEnterKey(self);
//  end;
end;

procedure TIQ3Configfm.txtAliasEnterKey(Sender: TObject);
var
  vsAlias,vscommand:String;
begin
  try
    giUID :=  StrToInt(cmbUIDMWC.text);
    vsAlias:=teAlias.Text;
    vsAlias := vsAlias + StringOfChar ( Char(#0), 12 - Length(vsAlias) );
    vsCommand:=VariablesUn.UID_NAME+'.'+vsAlias;
    ComFm.SetGenericParam(giUID,vsCommand, nil);
    Delay(200);

    DataUn.dmDataModule.dsIQ3s.Filtered := False;
    DataUn.dmDataModule.dsIQ3s.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidMWC.text;
    DataUn.dmDataModule.dsIQ3s.Filtered := True;
    DataUn.dmDataModule.dsIQ3s.edit;
    DataUn.dmDataModule.dsIQ3s.FieldByName('Alias').AsString:=teAlias.Text;
    DataUn.dmDataModule.dsIQ3s.MergeChangeLog;


  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.teAliasExit: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  End;

end;

procedure TIQ3Configfm.teDeviceUIDMWCEnter(Sender: TObject);
begin
  if(teDeviceUIDMWC.Text='-')then
    teDeviceUIDMWC.Clear;
end;

procedure TIQ3Configfm.teDeviceUIDMWCExit(Sender: TObject);
begin
  if(length(teDeviceUIDMWC.text)=0)then
  begin
    teDeviceUIDMWC.text:= '-';
  end;
end;

procedure TIQ3Configfm.teDeviceUIDMWCKeyPress(Sender: TObject; var Key: Char);
begin
//(Key in [#8, '0'..'9'])then
  if not (Key in [#8, '0'..'9',#46]) then
  begin
    // Discard the key
    Key := #0;
  end;
end;



procedure TIQ3Configfm.teHysteresisMWCEnter(Sender: TObject);
begin
  if(teHysteresisMWC.Text='-')then
    teHysteresisMWC.Clear;
end;

procedure TIQ3Configfm.teHysteresisMWCExit(Sender: TObject);
begin
  if(length(teHysteresisMWC.text)=0)then
  begin
    teHysteresisMWC.text:= '-';
  end;
end;

procedure TIQ3Configfm.teHysteresisMWCKeyPress(Sender: TObject; var Key: Char);
begin
//(Key in [#8, '0'..'9'])then

  if not (Key in [#8, '0'..'9']) then
  begin
    // Discard the key
    Key := #0;
  end;
  if Key <> #0 then
  begin
    teHysteresisMWC.Color:=clLime;
    btUpdateParams.Color:=clLime;
  end;
end;

procedure TIQ3Configfm.teMTAInhibitMWCEnter(Sender: TObject);
begin
  if(teMTAInhibitMWC.Text='-')then
    teMTAInhibitMWC.Clear;
  if(teSwitchAutoReturn.Text='-')then
    teSwitchAutoReturn.Clear;
end;

procedure TIQ3Configfm.teMTAInhibitMWCExit(Sender: TObject);
begin
  if(length(teMTAInhibitMWC.text)=0)then
  begin
    teMTAInhibitMWC.text:= '-';
  end;
  if(length(teSwitchAutoReturn.text)=0)then
  begin
    teSwitchAutoReturn.text:= '-';
  end;
end;

procedure TIQ3Configfm.teMTAInhibitMWCKeyPress(Sender: TObject; var Key: Char);
begin
//  if not (Key = '.') and (Key in [#8, '0'..'9'])then

  if not (Key in [#8, '0'..'9']) then
  begin
    // Discard the key
    Key := #0;
  end;
  if Key <> #0 then
  begin
    teMTAInhibitMWC.Color:=clLime;
    btUpdateParams.Color:=clLime;
  end;

end;

procedure TIQ3Configfm.SwitchOccKeyPress(Sender: TObject; var Key: Char);
begin

  if not (Key in [#8, '0'..'9']) then
  begin
    // Discard the key
    Key := #0;
  end;
  if Key <> #0 then
  begin
    if sender = teOccUnTime then
    begin
      teOccUnTime.Color:=clLime;
    end
    else if sender = teOccTime then
    begin
      teOccTime.Color:=clLime;
    end;
    btUpdateParams.Color:=clLime;
  end;
end;

procedure TIQ3Configfm.teNetSwitchUID2KeyPress(Sender: TObject; var Key: Char);
begin

  if not (Key in [#8, '0'..'9']) then
  begin
    // Discard the key
    Key := #0;
  end;
  if Key <> #0 then
  begin
    teNetSwitchUID2.Color:=clLime;
    btUpdateParams.Color:=clLime;
  end;
end;

procedure TIQ3Configfm.teNightInMWCEnter(Sender: TObject);
begin
  if(teNightInMWC.Text='-')then
    teNightInMWC.Clear;
end;

procedure TIQ3Configfm.teNightInMWCExit(Sender: TObject);
begin
  if(length(teNightInMWC.text)=0)then
  begin
    teNightInMWC.text:= '-';
  end;
end;

procedure TIQ3Configfm.teNightInMWCKeyPress(Sender: TObject; var Key: Char);
begin
  //  if not (Key = '.') and (Key in [#8, '0'..'9'])then

  if not (Key in [#8, '0'..'9']) then
  begin
    // Discard the key
    Key := #0;
  end;
  if Key <> #0 then
  begin
    teNightInMWC.Color:=clLime;
    btUpdateParams.Color:=clLime;
  end;
end;

procedure TIQ3Configfm.teNightOutMWCEnter(Sender: TObject);
begin
  if(teNightOutMWC.Text='-')then
    teNightOutMWC.Clear;
end;

procedure TIQ3Configfm.teNightOutMWCExit(Sender: TObject);
begin
  if(length(teNightOutMWC.text)=0)then
  begin
    teNightOutMWC.text:= '-';
  end;
end;

procedure TIQ3Configfm.teNightOutMWCKeyPress(Sender: TObject; var Key: Char);
begin
//  if not (Key = '.') and (Key in [#8, '0'..'9'])then

  if not (Key in [#8, '0'..'9']) then
  begin
    // Discard the key
    Key := #0;
  end;
  if Key <> #0 then
  begin
    teNightOutMWC.Color:=clLime;
    btUpdateParams.Color:=clLime;
  end;
end;

procedure TIQ3Configfm.teNightTHMWCEnter(Sender: TObject);
begin
  if(teNightTHMWC.Text='-')then
    teNightTHMWC.Clear;
end;

procedure TIQ3Configfm.teNightTHMWCExit(Sender: TObject);
begin
  if(length(teNightTHMWC.text)=0)then
  begin
    teNightTHMWC.text:= '-';
  end;
end;

procedure TIQ3Configfm.teNightTHMWCKeyPress(Sender: TObject; var Key: Char);
begin
//  if not (Key = '.') and (Key in [#8, '0'..'9'])then

  if not (Key in [#8, '0'..'9']) then
  begin
    // Discard the key
    Key := #0;
  end;
  if Key <> #0 then
  begin
    teNightTHMWC.Color:=clLime;
    btUpdateParams.Color:=clLime;
  end;
end;

procedure TIQ3Configfm.teShadeDownMWCEnter(Sender: TObject);
begin
  if(teShadeDownMWC.Text='-')then
    teShadeDownMWC.Clear;
end;

procedure TIQ3Configfm.teShadeDownMWCExit(Sender: TObject);
begin
  if(length(teShadeDownMWC.text)=0)then
  begin
    teShadeDownMWC.text:= '-';
  end;
end;

procedure TIQ3Configfm.teShadeDwnMWCKeyPress(Sender: TObject; var Key: Char);
begin
//  if not (Key = '.') and (Key in [#8, '0'..'9'])then

  if not (Key in [#8, '0'..'9']) then
  begin
    // Discard the key
    Key := #0;
  end;
  if Key <> #0 then
  begin
    teShadeDownMWC.Color:=clLime;
    btUpdateParams.Color:=clLime;
  end;
end;

procedure TIQ3Configfm.teShadeUpMWCEnter(Sender: TObject);
begin
  if(teShadeUpMWC.Text='-')then
    teShadeUpMWC.Clear;
end;

procedure TIQ3Configfm.teShadeUpMWCExit(Sender: TObject);
begin
if(length(teShadeUpMWC.text)=0)then
  begin
    teShadeUpMWC.text:= '-';
  end;
end;

procedure TIQ3Configfm.teShadeUpMWCKeyPress(Sender: TObject; var Key: Char);
begin
//(Key in [#8, '0'..'9'])then

  if not (Key in [#8, '0'..'9']) then
  begin
    // Discard the key
    Key := #0;
  end;
  if Key <> #0 then
  begin
    teShadeUpMWC.Color:=clLime;
    btUpdateParams.Color:=clLime;
  end;
end;

procedure TIQ3Configfm.teTH1MWCEnter(Sender: TObject);
begin
  if(teTH1MWC.Text='-')then
    teTH1MWC.Clear;
end;

procedure TIQ3Configfm.teTH1MWCExit(Sender: TObject);
begin
  if (StrToInt(teTh1MWC.text)>=StrToInt(teTh2MWC.text)) or (StrToInt(teTh1MWC.text)>=StrToInt(teTh3MWC.text)) or (StrToInt(teTh1MWC.text)>=StrToInt(teTh4MWC.text))then
  begin
    showMessage('Threshold1 must be the lowest value!');
    teTh1MWC.SetFocus;
  end;
  if(length(teTH1MWC.text)=0)then
  begin
    teTH1MWC.text:= '-';
  end;
end;

procedure TIQ3Configfm.teTH1MWCKeyPress(Sender: TObject; var Key: Char);
begin
  //(Key in [#8, '0'..'9'])then
  if not (Key in [#8, '0'..'9']) then
  begin
    // Discard the key
    Key := #0;
  end;
  if Key<>#0 then
  begin
    if sender = teTH1MWC then
    begin
      teTH1MWC.Color:=clLime;
    end;
    btUpdateParams.Color:=clLime;
    if sender = teZGN2Timeout then
    begin
      teZGN2Timeout.Color:=clLime;
    end;
  end;
end;

procedure TIQ3Configfm.teTH2MWCEnter(Sender: TObject);
begin

  if(teTH2MWC.Text='-')then
    teTH2MWC.Clear;
end;

procedure TIQ3Configfm.teTH2MWCExit(Sender: TObject);
begin
  if (StrToInt(teTH2MWC.text)>=StrToInt(teTH3MWC.text)) or (StrToInt(teTH2MWC.text)>=StrToInt(teTH4MWC.text))then
  begin
    showMessage('Threshold2 must be lower than Th3 and Th4!');
    teTH2MWC.SetFocus;
  end
  else if (StrToInt(teTH2MWC.text)<=StrToInt(teTH1MWC.text))then
  begin
    showMessage('Threshold2 must be higher than Th1!');
    teTH2MWC.SetFocus;
  end;
  if(length(teTH2MWC.text)=0)then
  begin
    teTH2MWC.text:= '-';
  end;
end;

procedure TIQ3Configfm.teTH2MWCKeyPress(Sender: TObject; var Key: Char);
begin
//(Key in [#8, '0'..'9'])then

  if not (Key in [#8, '0'..'9']) then
  begin
    // Discard the key
    Key := #0;
  end;
  if Key<>#0 then
  begin
    teTH2MWC.Color:=clLime;
    btUpdateParams.Color:=clLime;

  end;

end;



procedure TIQ3Configfm.teTH3MWCEnter(Sender: TObject);
begin
  if(teTH3MWC.Text='-')then
    teTH3MWC.Clear;
end;

procedure TIQ3Configfm.teTH3MWCExit(Sender: TObject);
begin
  if(StrToInt(teTH3MWC.text)>=StrToInt(teTH4MWC.text))then
  begin
    showMessage('Threshold3 must be lower than Th4!');
    teTH3MWC.SetFocus;
  end
  else if (StrToInt(teTH3MWC.text)<=StrToInt(teTH1MWC.text))or(StrToInt(teTH3MWC.text)<=StrToInt(teTH2MWC.text)) then
  begin
    showMessage('Threshold3 must be higher than Th1 and Th2!');
    teTH3MWC.SetFocus;
  end;
  if(length(teTH3MWC.text)=0)then
  begin
    teTH3MWC.text:= '-';
  end;
end;

procedure TIQ3Configfm.teTH3MWCKeyPress(Sender: TObject; var Key: Char);
begin
//(Key in [#8, '0'..'9'])then

  if not (Key in [#8, '0'..'9']) then
  begin
    // Discard the key
    Key := #0;
  end;
  if Key<>#0 then
  begin
    btUpdateParams.Color:=clLime;
    teTH3MWC.Color:=clLime;
  end;
end;

procedure TIQ3Configfm.teTH4MWCEnter(Sender: TObject);
begin
  if(teTH4MWC.Text='-')then
    teTH4MWC.Clear;
end;

procedure TIQ3Configfm.teTH4MWCExit(Sender: TObject);
begin
  if (StrToInt(teTH1MWC.text)>=StrToInt(teTH4MWC.text)) or (StrToInt(teTH4MWC.text)<=StrToInt(teTH3MWC.text)) or (StrToInt(teTH4MWC.text)<=StrToInt(teTH2MWC.text))then
  begin
    showMessage('Threshold4 must be the Highest value!');
    teTH4MWC.SetFocus;
  end;
  if(length(teTH4MWC.text)=0)then
  begin
    teTH4MWC.text:= '-';
  end;
end;

procedure TIQ3Configfm.teTH4MWCKeyPress(Sender: TObject; var Key: Char);
begin
//(Key in [#8, '0'..'9'])then

  if not (Key in [#8, '0'..'9']) then
  begin
    // Discard the key
    Key := #0;
  end;
  if Key<>#0 then
  begin
    btUpdateParams.Color:=clLime;
    teTH4MWC.Color:=clLime;;
  end;
end;

procedure TIQ3Configfm.teUIDMWCKeyPress(Sender: TObject; var Key: Char);
begin
  if not (Key in [#8, '0'..'9']) then
  begin
    // Discard the key
    Key := #0;
  end;
  if Key <> #0 then
  begin
    teUIDMWC.Color:=clLime;
    btUpdateUIDIQ3.Color:=clLime;
  end;
end;





procedure TIQ3Configfm.ZGNMWCKeyPress(Sender: TObject; var Key: Char);
begin
//(Key in [#8, '0'..'9'])then
  if not (Key in [#8, '0'..'9',#46]) then
  begin
    // Discard the key
    Key := #0;
  end;
  if (sender=teZGNMWC) and (Key <> #0) then
  begin
    teZGNMWC.Color:=clLime;
    btUpdateParams.Color:=clLime;
  end
  else if (sender=teZGN1Switch) and (Key <> #0) then
  begin
    teZGN1Switch.Color:=clLime;
    btUpdateParams.Color:=clLime;
  end
  else if (sender=teZGN2Switch) and (Key <> #0) then
  begin
    teZGN2Switch.Color:=clLime;
    btUpdateParams.Color:=clLime;
  end
  else if (sender=teZGNOcc) and (Key <> #0) then
  begin
    teZGNOcc.Color:=clLime;
    btUpdateParams.Color:=clLime;
  end;
end;

//Convert Check boxes to binary
Function TIQ3Configfm.ConvertShadePosToBin():String;
var
  value:string;
begin
  Value:='';
  if(cbRetractOption.Checked)then
  begin
    Value:='1';
  end
  else
  begin
    Value:='0';
  End;
  if(cmbNightShadePos.ItemIndex=5)then
  begin
    Value:=Value+'0';
  end
  else
  begin
    Value:=Value+'1';
  end;
  value:=value+'0';
  If(cbdownMWC.Checked)then
  begin
    Value:=Value+'1';
  end
  else
  begin
    Value:=Value+'0';
  end;
  If(cbPos3MWC.Checked)then
  begin
    Value:=Value+'1';
  end
  else
  begin
    Value:=Value+'0';
  end;
  If(cbPos2MWC.Checked)then
  begin
    Value:=Value+'1';
  end
  else
  begin
    Value:=Value+'0';
  end;
  If(cbPos1MWC.Checked)then
  begin
    Value:=Value+'1';
  end
  else
  begin
    Value:=Value+'0';
  end;
  If(cbupMWC.Checked)then
  begin
    Value:=Value+'1';
  end
  else
  begin
    Value:=Value+'0';
  end;
  Result := Value;
end;


end.
