unit MWCDefaultChooserUn;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons,math,ComUn,variablesUn;

type
  TMWCDefaultChooserfm = class(TForm)
    btnDefaultA: TButton;
    btnDefaultB: TButton;
    Button3: TButton;
    procedure btnDefaultAClick(Sender: TObject);
    procedure btnDefaultBClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    //procedure FormHide(Sender: TObject);

  private
    { Private declarations }
  public
    DeFaultToLoad:Integer;
  end;

var
  MWCDefaultChooserfm: TMWCDefaultChooserfm;

implementation

{$R *.dfm}


procedure TMWCDefaultChooserfm.btnDefaultAClick(Sender: TObject);
begin
  DeFaultToLoad:=1;
  close;
end;

procedure TMWCDefaultChooserfm.btnDefaultBClick(Sender: TObject);
begin
  DeFaultToLoad:=2;
  close;
end;

procedure TMWCDefaultChooserfm.Button3Click(Sender: TObject);
begin
  DeFaultToLoad:=3;
  close;
end;
procedure TMWCDefaultChooserfm.FormShow(Sender: TObject);
begin
  Top:=VariablesUn.giTop;
  Left := VariablesUn.giLeft;
end;

//procedure TMWCDefaultChooserfm.FormHide(Sender: TObject);
//begin
//  if not(InRange(DeFaultToLoad,1,3))then
//  begin
//    DeFaultToLoad:=0;
//  end;
//end;

end.
