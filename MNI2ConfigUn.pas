unit MNI2ConfigUn;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.ExtCtrls, Vcl.Grids, Vcl.StdCtrls,StrUtils,Data.DB,Datasnap.DBClient,
  VariablesUn,DataUn,NewProjectUn,MWCDisplayUn,MNIConfigUn,MWCConfigUn,ComUn,DiscoverUn,AboutUn,ScreenSetupUn,
  Vcl.Imaging.jpeg, Vcl.Imaging.pngimage,ClipBrd,math, JvComponentBase,MotorMaintenanceDisplay,
  JvCaptionButton,PowerPanelConfigUn;
type
    TIPEGrid = class(TStringGrid);  // to expose InPlaceEditor
type
  TMNI2ConfigFm = class(TForm)
    Shape14           : TShape;
    Label12           : TLabel;
    imgMenu: TImage;
    cmbProject        : TComboBox;
    pnlMenuBar        : TPanel;
    cmbUid             : TComboBox;
    txtAlias: TEdit;
    btUpdateUID: TPanel;
    ScrollBox1: TScrollBox;
    pnlVirtMotors: TPanel;
    sgVirtualMotors: TStringGrid;
    Label1: TLabel;
    teUID: TEdit;
    Panel3: TPanel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label27: TLabel;
    Label30: TLabel;
    shpcbSWEnabled: TShape;
    shpcbSWFWDEnabled: TShape;
    shpcmbSWPerson: TShape;
    shpcmbSWMode: TShape;
    cmbSwitch: TComboBox;
    teSWFWDAddr: TEdit;
    cmbSWMode: TComboBox;
    cbSWEnabled: TCheckBox;
    cbSWFWDEnabled: TCheckBox;
    cmbSWPerson: TComboBox;
    btUpdateParams: TPanel;
    cmbSWLed: TComboBox;
    Label2: TLabel;
    shpLED: TShape;
    btnMenu: TImage;
    btnFind: TImage;
    btnInfo: TImage;
    btnSettings: TImage;
    btnExit: TImage;
    pnlPushData: TPanel;
    cmbMotorSelector: TComboBox;
    btnConfig: TImage;
    btnControl: TImage;
    btnLink: TButton;
    teLinkCount: TEdit;
    lblLinked: TLabel;
    imgRefresh: TImage;
    pnlHembarAlign: TPanel;
    shpcbHBCorrection: TShape;
    cbHBCorrection: TCheckBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    teTravel: TEdit;
    shpTeTravel: TShape;
    Label6: TLabel;
    JvCaptionButton1: TJvCaptionButton;
    btnLimitSetting: TImage;
    cmbDeviceSelector: TComboBox;
    Function  WaitforGenericResponseConfirmation(UID:Integer;Command:String;rh:TNotifyEvent;ResendTMR:Integer):Boolean;
    procedure imgMenuMouseEnter(Sender: TObject);
    procedure btnFindMouseEnter(Sender: TObject);
    procedure btnInfoMouseEnter(Sender: TObject);
    procedure btnSettingsMouseEnter(Sender: TObject);
    procedure btnExitMouseEnter(Sender: TObject);
    procedure pnlMenuBarMouseLeave(Sender: TObject);
    procedure cmbDeviceSelectorChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure cmbUidChange(Sender: TObject);
    procedure FormMouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure sgVirtualMotorsMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure sgVirtualMotorsMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure sgVirtualMotorsKeyPress(Sender: TObject; var Key: Char);
    procedure sgVirtualMotorsSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure btUpdateParamsClick(Sender: TObject);
    procedure sgVirtualMotorsEnter(Sender: TObject);
    procedure ScrollBox1MouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure sgVirtualMotorsExit(Sender: TObject);
    procedure cmbSwitchChange(Sender: TObject);
    procedure btUpdateUIDClick(Sender: TObject);
    procedure teUIDKeyPress(Sender: TObject; var Key: Char);
    procedure teUIDExit(Sender: TObject);
    procedure btUpdateParamsMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btUpdateParamsMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure cmbProjectChange(Sender: TObject);
    procedure btnMenuMouseEnter(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure btnFindClick(Sender: TObject);
    procedure btnInfoClick(Sender: TObject);
    procedure btnSettingsClick(Sender: TObject);
    procedure teUIDChange(Sender: TObject);
    procedure teSWFWDAddrChange(Sender: TObject);
    procedure cbSWEnabledClick(Sender: TObject);
    procedure cmbSWPersonChange(Sender: TObject);
    procedure cbSWFWDEnabledClick(Sender: TObject);
    procedure cmbSWModeChange(Sender: TObject);
    procedure cmbSWLedChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure txtAliasChange(Sender: TObject);
    procedure pnlPushDataClick(Sender: TObject);
    procedure txtAliasEnterKey(Sender: TObject);
    procedure btnConfigMouseEnter(Sender: TObject);
    procedure btnControlMouseEnter(Sender: TObject);
    procedure txtAliasDblClick(Sender: TObject);
    procedure btnLinkClick(Sender: TObject);
    procedure imgRefreshClick(Sender: TObject);
    procedure txtAliasKeyPress(Sender: TObject; var Key: Char);
    procedure sgVirtualMotorsKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cbHBCorrectionClick(Sender: TObject);
    procedure teTravelChange(Sender: TObject);
    procedure teTravelKeyPress(Sender: TObject; var Key: Char);
    procedure teTravelExit(Sender: TObject);
    procedure JvCaptionButton1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnLimitSettingMouseEnter(Sender: TObject);
    procedure btnLimitSettingClick(Sender: TObject);
  private
    { Private declarations }
    flgIDChange   :boolean;
    loadFlag      :Boolean;
    LastCol       :Integer;
    LastRow       :Integer;
    LastCellStr   :String;
    vsOldAlias    :String;
    passFlag      :Boolean;
    CanSelect     :Boolean;
    ScrollPostion :Integer;
    EnterSGFlag   :boolean;
    SwitchPerson  :Integer;
    LastUID       :Integer;
    UIDList       :TStringList;
    SomfyIDList       :TStringList;
    giUID:integer;
    procedure ComboBox_AutoWidth(const theComboBox: TCombobox);
    Function  isStrhex(s:String):boolean;
    Function  GetDisabledMotorsIntConfig:Integer;
    Function  GetDisabledMotorsIntDS:Integer;
    Function  GetEnabledFWDMOIntConfig:Integer;
    Function  GetEnabledFWDMOIntDS:Integer;
    Function  GetPortModeIntConfig:Integer;
    Function  GetPortModeIntDS:Integer;
    Function  GetSwPersonIndex(SwPerson:String):Integer;
    Procedure ClearGreen;
    Procedure StopEvents;
    Procedure StartEvents;
    Procedure AddNewRecordSet(UID,NodeID:String;MotorIndex:Integer);
    Procedure disableForm(Disable:Boolean);
    Procedure LoadData;
    Procedure WaitforGenericResponse(UID:Integer;Command:String;rh:TNotifyEvent;ResendTime:Integer);
    Procedure rhMsgRecieved(sender: TObject);
    Procedure LoadUID();
    Procedure LoadProjects;
    Procedure SortStringListUIDS(cmbUIDList:TStringList);
    Const
      MNIRspSpeed:Integer=500;
      SMFYRspSpeed:Integer= 1000;
  public
    { Public declarations }
    rh_ShowMain:TNotifyEvent;
    //rh_ShowMWCConfig:TNotifyEvent;
    rh_showIQMLC2Config:TNotifyEvent;
    procedure rhShowMNI2(Sender: TObject);
    procedure GetVirtualMotorsStatus(Sender: TObject);
    procedure rhShowMNI2Config(Sender: TObject);
    procedure rhPortOpend(Sender: TObject);
    procedure rhPortClosed(Sender: TObject);
  end;

var
  MNI2ConfigFm: TMNI2ConfigFm;


implementation

{$R *.dfm}

uses LimitSettingUn, IQ3RFDisplayUn;

Procedure TMNI2ConfigFm.SortStringListUIDS(cmbUIDList:TStringList);
var
  I:Integer;
  First,Second:Integer;
  Swap:Boolean;
  TempStr:String;
begin
  Swap:=true;
  while Swap do
  begin
    Swap:=False;
    for I := 0 to cmbUIDList.Count-2 do
    begin
      First:=StrToInt(cmbUIDList[I]);
      Second:=StrToInt(cmbUIDList[I+1]);
      if First > Second then
      begin
        TempStr:=cmbUIDList[I];
        cmbUIDList[I]:=cmbUIDList[I+1];
        cmbUIDList[I+1]:=TempStr;
        Swap:=true;
      end;
    end;
  end;
end;

procedure TMNI2ConfigFm.LoadData;
begin
  DataUn.dmDataModule.dsMNI2.Filtered := False;
  DataUn.dmDataModule.dsMNI2.Filter := 'ProjectName = ' + QuotedStr(cmbProject.Text) +  ' and UID = ' + cmbUid.Text;
  DataUn.dmDataModule.dsMNI2.Filtered := True;
  if DataUn.dmDataModule.dsMNI2.eof then
  begin
    showMessage('Error: Empty Dataset');
  end;

  StopEvents;
  txtAlias.OnChange:=nil;
  txtAlias.Text:=DataUn.dmDataModule.dsMNI2.FieldByName('Alias').AsString;
  txtAlias.OnChange:=txtAliasChange;
  cbHBCorrection.Checked:= DataUn.dmDataModule.dsMNI2.FieldByName('StopAlignEnabled').AsBoolean;
  teTravel.Text:= DataUn.dmDataModule.dsMNI2.FieldByName('StopAlignTravel').AsString;
  if cmbSwitch.Text='1' then
  begin
    cbSWEnabled.Checked:=DataUn.dmDataModule.dsMNI2.FieldByName('SwPort1Enabled').AsBoolean;
    cmbSWPerson.ItemIndex:=GetSwPersonIndex(DataUn.dmDataModule.dsMNI2.FieldByName('Sw1BtnPerson').AsString);
    cbSWFWDEnabled.Checked:=DataUn.dmDataModule.dsMNI2.FieldByName('SwPort1FWDEnabled').AsBoolean;
    teSWFWDAddr.Text:=DataUn.dmDataModule.dsMNI2.FieldByName('SwPort1FWDAddr').AsString;
    cmbSWMode.ItemIndex:=DataUn.dmDataModule.dsMNI2.FieldByName('SwPort1Mode').AsInteger;
    cmbSWLed.ItemIndex:=ord(DataUn.dmDataModule.dsMNI2.FieldByName('LEDsw1').AsBoolean);
  end
  else if cmbSwitch.Text='2' then
  begin
    cbSWEnabled.Checked:=DataUn.dmDataModule.dsMNI2.FieldByName('SwPort2Enabled').AsBoolean;
    cmbSWPerson.ItemIndex :=GetSwPersonIndex(DataUn.dmDataModule.dsMNI2.FieldByName('SW2BtnPerson').AsString);
    cbSWFWDEnabled.Checked:=DataUn.dmDataModule.dsMNI2.FieldByName('SwPort2FWDEnabled').AsBoolean;
    teSWFWDAddr.Text:=DataUn.dmDataModule.dsMNI2.FieldByName('SwPort2FWDAddr').AsString;
    cmbSWMode.ItemIndex:=DataUn.dmDataModule.dsMNI2.FieldByName('SwPort2Mode').AsInteger;
    cmbSWLed.ItemIndex:=ord(DataUn.dmDataModule.dsMNI2.FieldByName('LEDsw2').AsBoolean);
  end
  else if cmbSwitch.Text='3' then
  begin
    cbSWEnabled.Checked:=DataUn.dmDataModule.dsMNI2.FieldByName('SwPort3Enabled').AsBoolean;
    cmbSWPerson.ItemIndex :=GetSwPersonIndex(DataUn.dmDataModule.dsMNI2.FieldByName('SW3BtnPerson').AsString);
    cbSWFWDEnabled.Checked:=DataUn.dmDataModule.dsMNI2.FieldByName('SwPort3FWDEnabled').AsBoolean;
    teSWFWDAddr.Text:=DataUn.dmDataModule.dsMNI2.FieldByName('SwPort3FWDAddr').AsString;
    cmbSWMode.ItemIndex:=DataUn.dmDataModule.dsMNI2.FieldByName('SwPort3Mode').AsInteger;
    cmbSWLed.ItemIndex:=ord(DataUn.dmDataModule.dsMNI2.FieldByName('LEDsw3').AsBoolean);
  end
  else if cmbSwitch.Text='4' then
  begin
    cbSWEnabled.Checked:=DataUn.dmDataModule.dsMNI2.FieldByName('SwPort4Enabled').AsBoolean;
    cmbSWPerson.ItemIndex :=GetSwPersonIndex(DataUn.dmDataModule.dsMNI2.FieldByName('SW4BtnPerson').AsString);
    cbSWFWDEnabled.Checked:=DataUn.dmDataModule.dsMNI2.FieldByName('SwPort4FWDEnabled').AsBoolean;
    teSWFWDAddr.Text:=DataUn.dmDataModule.dsMNI2.FieldByName('SwPort4FWDAddr').AsString;
    cmbSWMode.ItemIndex:=DataUn.dmDataModule.dsMNI2.FieldByName('SwPort4Mode').AsInteger;
    cmbSWLed.ItemIndex:=ord(DataUn.dmDataModule.dsMNI2.FieldByName('LEDsw4').AsBoolean);
  end;
  StartEvents;

end;

Function TMNI2ConfigFm.GetSwPersonIndex(SwPerson:String):Integer;
var
  swPersonInt:Integer;
begin
  swPersonInt:=StrToInt(swPerson);
  case swPersonInt of
        9:
        Begin
          GetSwPersonIndex:= 0;
        End;
        17:
        Begin
          GetSwPersonIndex:=1;
        End;
        33:
        Begin
          GetSwPersonIndex:=2;
        End;
        18:
        Begin
          GetSwPersonIndex:=3;
        End;
        34:
        Begin
          GetSwPersonIndex:=4;
        End;
        4:
        Begin
          GetSwPersonIndex:=5;
        End;
        68:
        Begin
          GetSwPersonIndex:=6;
        End;
        132:
        Begin
          GetSwPersonIndex:=7;
        End;
      end;
end;

//Changes Color of Com Indicator lights
procedure TMNI2ConfigFm.rhPortOpend(Sender: TObject);
begin
  giComPortOn := True;
  Shape14.Pen.Color := clLime;
end;

//Changes Color of Com Indicator lights
procedure TMNI2ConfigFm.rhPortClosed(Sender: TObject);
begin
  giComPortOn := False;
  Shape14.Pen.Color := clRed;
end;

procedure TMNI2ConfigFm.btnConfigMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TMNI2ConfigFm.btnControlMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TMNI2ConfigFm.btnExitClick(Sender: TObject);
begin
  DataUn.dmDataModule.dsMotors.Filtered := False;
  DataUn.dmDataModule.dsMWCChannels.Filtered := False;
  DataUn.dmDataModule.dsMWCs.Filtered := False;
  DataUn.dmDataModule.dsMNI.Filtered := False;
  DataUn.dmDataModule.dsMNI2.Filtered := False;
  DataUn.dmDataModule.dsIQMLC2.Filtered := False;
  DataUn.dmDataModule.dsPowerPanel.Filtered := False;

  DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
  DataUn.dmDataModule.dsMWCs.SaveToFile(VariablesUn.gsDataPath + 'dsMWCs.txt', dfXML);
  DataUn.dmDataModule.dsMWCChannels.SaveToFile(VariablesUn.gsDataPath + 'dsMWCChannels.txt', dfXML);
  DataUn.dmDataModule.dsMNI.SaveToFile(VariablesUn.gsDataPath + 'dsMNI.txt', dfXML);
  DataUn.dmDataModule.dsMNI2.SaveToFile(VariablesUn.gsDataPath + 'dsMNI2.txt', dfXML);
  DataUn.dmDataModule.dsIQMLC2.SaveToFile(VariablesUn.gsDataPath + 'dsIQMLC2.txt', dfXML);
  DataUn.dmDataModule.dsPowerPanel.SaveToFile(VariablesUn.gsDataPath + 'dsPowerPanel.txt', dfXML);
  pnlMenuBar.Visible := False;
  Application.Terminate;

end;

procedure TMNI2ConfigFm.btnExitMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TMNI2ConfigFm.btnFindClick(Sender: TObject);
begin
  discoverfm.rh_ShowMDC:= rhShowMNI2Config;
  pnlMenuBar.Visible := False;
  Hide;
  DiscoverFm.Show;
end;

procedure TMNI2ConfigFm.rhShowMNI2(Sender: TObject);
begin
  MNI2Configfm.show;
end;

procedure TMNI2ConfigFm.btnFindMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TMNI2ConfigFm.btnInfoClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  Hide;
  AboutFm.rh_ShowForm := rhShowMNI2Config;
  AboutFm.Show;
end;

procedure TMNI2ConfigFm.btnInfoMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TMNI2ConfigFm.btnLimitSettingClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  LimitSettingFm.rh_showmain := rhShowMNI2;
  Hide;
  LimitSettingFm.Show;
end;

procedure TMNI2ConfigFm.btnLimitSettingMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := true;
end;

procedure TMNI2ConfigFm.btnLinkClick(Sender: TObject);
begin
  VariablesUn.ClearData;
  //giSentCom := StrToInt(VariablesUn.MNI2_PushData);
  screen.cursor:=crHourglass;
  sgVirtualMotors.Enabled:=false;
  cmbProject.enabled:=false;
  txtAlias.enabled:=false;
  imgMenu.enabled:=false;
  cmbUID.enabled:=false;
  teUID.enabled:=false;
  btUpdateUID.enabled:=false;
  btUpdateParams.enabled:=false;
  cmbDeviceSelector.enabled:=false;
  pnlPushData.Enabled:=false;

  ComFm.SetGenericParam(StrToInt(cmbUid.Text), VariablesUn.MDC_Link+'.0.0', rhMsgRecieved);
  if not WaitforGenericResponseConfirmation(StrToInt(cmbUid.Text), VariablesUn.MDC_Link+'.0.0', rhMsgRecieved,1000) then
  begin
    ShowMessage('Failed: Link Command');

  end;
  pnlPushData.Enabled:=True;
  cmbDeviceSelector.enabled:=True;
  btUpdateParams.enabled:=True;
  btUpdateUID.enabled:=True;
  teUID.enabled:=True;
  imgMenu.enabled:=True;
  cmbUID.enabled:=True;
  txtAlias.enabled:=True;
  sgVirtualMotors.Enabled:=true;
  cmbProject.enabled:=true;
  screen.cursor:=crDefault;
end;

procedure TMNI2ConfigFm.btnMenuMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

Function TMNI2ConfigFm.WaitforGenericResponseConfirmation(UID:Integer;Command:String;rh:TNotifyEvent;ResendTMR:Integer):Boolean;
var
  PauseCount:Integer;
begin
  passFlag:=false;
  PauseCount:=0;
  while not PassFlag do
  begin
    PauseCount:=PauseCount+1;
    delay(1);
    if (PauseCount=ResendTMR) or (PauseCount=(ResendTMR*2)) then
    begin
      ComFm.SetGenericParam(UID,Command,rh);
    end;
    if PauseCount>ResendTMR*3 then
    begin
      Result:=false;
      ShowMessage('Failed Command');
      Exit;
    end;
  end;
  Result:=True;
end;

procedure TMNI2ConfigFm.AddNewRecordSet(UID,NodeID:String;MotorIndex:Integer);
Var
  UIDInt:Integer;
begin
  try
    UIDInt:= StrToInt(UID);

    if DataUn.dmDataModule.dsMotors.State = dsInactive then
    begin
      DataUn.dmDataModule.dsMotors.CreateDataSet;
      DataUn.dmDataModule.dsMotors.Active := True;
    end;

    DataUn.dmDataModule.dsMotors.Filtered := False;
    DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsMotors.Filtered := True;

    DataUn.dmDataModule.dsMotors.Insert;
    DataUn.dmDataModule.dsMotors.FieldByName('ProjectName').AsString := VariablesUn.gsProjectName;
    DataUn.dmDataModule.dsMotors.FieldByName('UID').AsInteger := UIDInt;
    DataUn.dmDataModule.dsMotors.FieldByName('Address1').AsString := '-1';
    DataUn.dmDataModule.dsMotors.FieldByName('Address2').AsString := '-1';
    DataUn.dmDataModule.dsMotors.FieldByName('Address3').AsString := '-1';
    DataUn.dmDataModule.dsMotors.FieldByName('Address4').AsString := '-1';
    DataUn.dmDataModule.dsMotors.FieldByName('Address5').AsString := '-1';
    DataUn.dmDataModule.dsMotors.FieldByName('Address6').AsString := '-1';
    DataUn.dmDataModule.dsMotors.FieldByName('Address7').AsString := '-1';
    DataUn.dmDataModule.dsMotors.FieldByName('Address8').AsString := '-1';
    DataUn.dmDataModule.dsMotors.FieldByName('BlackOut').AsString := '-1';
    DataUn.dmDataModule.dsMotors.FieldByName('BoType').AsString := '-1';
    DataUn.dmDataModule.dsMotors.FieldByName('BlackOutMode').asInteger := 0;
    DataUn.dmDataModule.dsMotors.FieldByName('Alias').AsString := '';
    DataUn.dmDataModule.dsMotors.FieldByName('Direction').AsString := '-1';
    DataUn.dmDataModule.dsMotors.FieldByName('Inhibit').AsString := '-1';
    DataUn.dmDataModule.dsMotors.FieldByName('Start').AsString := '-1';
    DataUn.dmDataModule.dsMotors.FieldByName('Led').AsString := '-1';
    DataUn.dmDataModule.dsMotors.FieldByName('Maint').AsString := '-1';
    DataUn.dmDataModule.dsMotors.FieldByName('MtaInhibit').AsString := '-1';
    DataUn.dmDataModule.dsMotors.FieldByName('Revision').AsString := '-1';
    DataUn.dmDataModule.dsMotors.FieldByName('AutoReturn').AsInteger:=0;
    if (hexaToInt(LeftStr(NodeID,2))>=19)and(hexaToInt(LeftStr(NodeID,2))<=21) then
    begin
      if MotorIndex<=4 then
      begin
        DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsInteger:=205;
      end
      else
      begin
        DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsInteger:=77;
      end;
    end
    else if (hexaToInt(LeftStr(NodeID,2))>=06)and(hexaToInt(LeftStr(NodeID,2))<=7) then
    begin
      if MotorIndex<=4 then
      begin
        DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsInteger:=207;
      end
      else
      begin
        DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsInteger:=79;
      end;
    end
    else if (hexaToInt(LeftStr(NodeID,2))>=10)and(hexaToInt(LeftStr(NodeID,2))<=17) then
    begin
      if MotorIndex<=4 then
      begin
        DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsInteger:=206;
      end
      else
      begin
        DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsInteger:=78;
      end;
    end;
    DataUn.dmDataModule.dsMotors.FieldByName('HostUID').AsInteger:=StrToInt(CMBUid.Text);
    DataUn.dmDataModule.dsMotors.FieldByName('MotorNumber').AsInteger:=0;
    DataUn.dmDataModule.dsMotors.FieldByName('MotorPerson').AsString:='-1';
    DataUn.dmDataModule.dsMotors.Post;

  except
  on E: Exception do
  begin
    //imgMenu.OnMouseEnter := imgMenuMouseEnter;
    Screen.Cursor := crDefault;
    MessageDlg('TMNI2ConfigFm.AddNewRecordSet: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;

procedure TMNI2ConfigFm.btUpdateParamsClick(Sender: TObject);
var
  I,J,K:Integer;
  vsCommand,vsData1,vsData2,vsData3,AliasText:String;
  intDS,intConfig,intLED:Integer;
  SWtemp,cansel:boolean;
begin
  try
    //  Check for 6 chars
    btUpdateParams.SetFocus;
    btUpdateParams.OnClick:=nil;
    sgVirtualMotorsExit(nil);
    giUID:=StrToInt(cmbUid.Text);
    for I := 1 to 20 do
    begin
      if sgVirtualMotors.Cells[2,I]<>'Disabled' then
      begin
        if sgVirtualMotors.Cells[2,I].Length<6 then
        begin
          ShowMessage('Node ID in row '+intToStr(I)+' Needs 6 Characters: Fix Before Programming');
          Exit;
        end;
      end;
    end;
    //Check for duplicate UIDS on a single MDC
    for I := 1 to 20 do
    begin
      for J := 1 to 20 do
      begin
        if (I<>J) and (sgVirtualMotors.Cells[1,J]<>'Disabled') and (sgVirtualMotors.Cells[1,I]<>'Disabled')then
        begin
          if sgVirtualMotors.Cells[1,I]= sgVirtualMotors.Cells[1,J]then
          begin
            ShowMessage('Fault: Duplicate Mechonet UID '+sgVirtualMotors.Cells[1,I]);
            Exit;
          end;
        end;
      end;
    end;
    //Check for duplicate
    for I := 1 to 20 do
    begin
      if (sgVirtualMotors.Cells[1,I]<>'Disabled')then
      begin
        if dmDataModule.DuplicateUIDCheckMDC(sgVirtualMotors.Cells[1,I],cmbUID.Text) then
        begin
          ShowMEssage('Error: UID '+sgVirtualMotors.Cells[1,I]+' already exist. Choose Different ID.');
          exit;
        end;
      end;
    end;
    //Check for proper somfy node id range and dry contact range(for CK)
    for I := 1 to 20 do
    begin
      if sgVirtualMotors.Cells[2,I]<>'Disabled' then
      begin
        outputdebugstring(Pchar(IntToStr(hexaToInt(LeftStr(sgVirtualMotors.Cells[2,I],2)))));
        if not (hexaToInt(LeftStr(sgVirtualMotors.Cells[2,I],2))>=6) or not (hexaToInt(LeftStr(sgVirtualMotors.Cells[2,I],2))<=7) then
        begin
          if not (hexaToInt(LeftStr(sgVirtualMotors.Cells[2,I],2))>=10) or not (hexaToInt(LeftStr(sgVirtualMotors.Cells[2,I],2))<=17) then
          begin
            if not (hexaToInt(LeftStr(sgVirtualMotors.Cells[2,I],2))>=19) or not (hexaToInt(LeftStr(sgVirtualMotors.Cells[2,I],2))<=21) then
            begin
              if not (hexaToInt(LeftStr(sgVirtualMotors.Cells[2,I],2))=0) or not (hexaToInt(RightStr(sgVirtualMotors.Cells[2,I],2))>=10) or not (hexaToInt(RightStr(sgVirtualMotors.Cells[2,I],2))<=13)or not (hexaToInt(copy(sgVirtualMotors.Cells[2,I], 3, 2))=0) then
              begin
                ShowMessage('Invalid Node ID: Motor # '+IntToStr(I));
                Exit;
              end;
            end;
          end;
        end;
      end;
    end;
    sgVirtualMotors.OnExit(self);
    for I := 0 to 19 do
    Begin
      //if A  UID has changed change UID on MechoNEt and also in motor record set
      if sgVirtualMotors.Cells[1,I+1]<>UIDList[I] then
      begin
        if UIDList[I]<>'Disabled' then   //If old one was enabled
        begin
          DataUn.dmDataModule.dsMotors.Filtered := False;
          DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName)+' and UID = ' + UIDList[I];
          DataUn.dmDataModule.dsMotors.Filtered := True;
          if (sgVirtualMotors.Cells[1,I+1]<>'Disabled') and (sgVirtualMotors.Cells[1,I+1]<>'') then
          begin
            if not (DataUn.dmDataModule.dsMotors.Eof) then
            begin
              DataUn.dmDataModule.dsMotors.Edit;
              DataUn.dmDataModule.dsMotors.FieldByName('UID').AsInteger:=StrToInt(sgVirtualMotors.Cells[1,I+1]);
              DataUn.dmDataModule.dsMotors.MergeChangeLog;
            end;
            UIDList[I]:=sgVirtualMotors.Cells[1,I+1];
          end
          else
          begin
            if not (DataUn.dmDataModule.dsMotors.Eof) then
            begin
              DataUn.dmDataModule.dsMotors.Edit;
              DataUn.dmDataModule.dsMotors.Delete;
              DataUn.dmDataModule.dsMotors.MergeChangeLog;

            end;
            UIDList[I]:='Disabled';
            //SomfyIDList[I]='Disabled'
          end;
        end
        else     //If old one was Disabled then create record if somfy id present
        begin
          UIDList[I]:=sgVirtualMotors.Cells[1,I+1];
          if (sgVirtualMotors.Cells[1,I+1]<>'Disabled') and (sgVirtualMotors.Cells[2,I+1]<>'Disabled')then
          begin
            AddNewRecordSet(UIDList[I],sgVirtualMotors.Cells[2,I+1],I+1);
          end;
        end;

        //Set Mechonet UIDS
        VariablesUn.ClearData;
        giSentCom := StrToInt(LeftStr(VariablesUn.MDC_AdapterRev,3));  //Get UIDS
        if (sgVirtualMotors.Cells[1,I+1]='Disabled') or (sgVirtualMotors.Cells[1,I+1]='')  then
        begin
          vsCommand:=VariablesUn.MDC_AdapterRev+'.0.'+IntToStr(I)+'.255.255';
        end
        else
        begin
          vsCommand:=VariablesUn.MDC_AdapterRev+'.0.'+IntToStr(I)+'.'+IntToStr(hi(StrToInt(sgVirtualMotors.Cells[1,I+1])))+'.'+IntToStr(lo(StrToInt(sgVirtualMotors.Cells[1,I+1])));
        end;

        ComFm.SetGenericParam(giUID,vsCommand,rhMsgRecieved);
        if not WaitforGenericResponseConfirmation(giUID,vsCommand,rhMsgRecieved,MNIRspSpeed)then
        begin
          ShowMessage('Failed: Set Mecho ID');
        end;
      end;
      //check and update if Somfy node ID has changed
      if sgVirtualMotors.Cells[2,I+1]<>SomfyIDList[I] then
      begin
        //Set Somfy IDs
        if (sgVirtualMotors.Cells[2,I+1]<>'Disabled')  then
        begin
          if (SomfyIDList[I]='Disabled') and (UIDList[I]<>'Disabled') then    //if changing from disabled NodeID and has UID
          begin
             AddNewRecordSet(UIDList[I],sgVirtualMotors.Cells[2,I+1],I+1);
          end
          else if (UIDList[I]<>'Disabled') then  //If editing valid Node ID and has UID
          begin
            DataUn.dmDataModule.dsMotors.Filtered := False;
            DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName)+' and UID = ' + UIDList[I];
            DataUn.dmDataModule.dsMotors.Filtered := True;
            if not DataUn.dmDataModule.dsMotors.Eof then
            begin
              DataUn.dmDataModule.dsMotors.Edit;
              if (hexaToInt(LeftStr(sgVirtualMotors.Cells[2,I+1],2))>=6) and (hexaToInt(LeftStr(sgVirtualMotors.Cells[2,I+1],2))<=7) then
              begin
                if I<4 then
                begin
                  DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsInteger:= 207;
                end
                else
                begin
                  DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsInteger:= 79;
                end;
              end
              else if  (hexaToInt(LeftStr(sgVirtualMotors.Cells[2,I+1],2))>=10) and  (hexaToInt(LeftStr(sgVirtualMotors.Cells[2,I+1],2))<=17) then
              begin
                if I<4 then
                begin
                  DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsInteger:= 206;
                end
                else
                begin
                  DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsInteger:= 78;
                end;
              end
              else if (hexaToInt(LeftStr(sgVirtualMotors.Cells[2,I+1],2))>=19) and  (hexaToInt(LeftStr(sgVirtualMotors.Cells[2,I+1],2))<=21) then
              begin
                if I<4 then
                begin
                  DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsInteger:= 205;
                end
                else
                begin
                  DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsInteger:= 77;
                end;
              end;
              DataUn.dmDataModule.dsMotors.MergeChangeLog;



            end;
          end;
          vsData1:=IntToStr(hexaToInt(LeftStr(sgVirtualMotors.Cells[2,I+1],2)));
          vsData2:=IntToStr(hexaToInt(LeftStr(RightStr(sgVirtualMotors.Cells[2,I+1],4),2)));
          vsData3:=IntToStr(hexaToInt(RightStr(sgVirtualMotors.Cells[2,I+1],2)));
          SomfyIDList[I]:=sgVirtualMotors.Cells[2,I+1];
        end
        else
        begin
          vsData1:='255';
          vsData2:='255';
          vsData3:='255';

          //Delete Record From Motor Form unless UID is disabled. Then just update device and grid
          if (sgVirtualMotors.Cells[2,I+1]='') or  (sgVirtualMotors.Cells[2,I+1]='Disabled') and (UIDList[I]<>'Disabled') then
          begin
            DataUn.dmDataModule.dsMotors.Filtered := False;
            DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName)+' and UID = ' + UIDList[I];
            DataUn.dmDataModule.dsMotors.Filtered := True;
            if not DataUn.dmDataModule.dsMotors.Eof then
            begin
              DataUn.dmDataModule.dsMotors.Edit;
              DataUn.dmDataModule.dsMotors.Delete;
              DataUn.dmDataModule.dsMotors.MergeChangeLog;
            end;
          end;
          SomfyIDList[I]:='FFFFFF';
        end;
        vsCommand:=VariablesUn.MDC_SUID+'.0.'+IntToStr(I)+'.'+vsData1+'.'+vsData2+'.'+vsData3;
        VariablesUn.ClearData;
        giSentCom := StrToInt(LeftStr(VariablesUn.MDC_SUID,3));
        ComFm.SetGenericParam(giUID,vsCommand, rhMsgRecieved);
        if not WaitforGenericResponseConfirmation(giUID,vsCommand, rhMsgRecieved,MNIRspSpeed)then
        begin
          ShowMessage('Failed: Set Node ID');
        end;
      end;



    End;
    //Stop Alignment Enable
    DataUn.dmDataModule.dsMNI2.Filtered := False;
    DataUn.dmDataModule.dsMNI2.Filter := 'ProjectName = ' + QuotedStr(cmbProject.Text) +  ' and UID = ' + cmbUid.Text;
    DataUn.dmDataModule.dsMNI2.Filtered := True;
    if cbHBCorrection.Checked <> DataUn.dmDataModule.dsMNI2.FieldByName('StopAlignEnabled').AsBoolean then
    begin
      VariablesUn.ClearData;
      giSentCom := StrToInt(LeftStr(VariablesUn.MDC_StopAlign,3));
      if cbHBCorrection.Checked then
      begin
        vsCommand:= VariablesUn.MDC_StopAlign+'.1.1';
        ComFm.SetGenericParam(giUID,vsCommand,rhMsgRecieved);
      end
      else
      begin
        vsCommand:= VariablesUn.MDC_StopAlign+'.1.0';
        ComFm.SetGenericParam(giUID,vsCommand,rhMsgRecieved);
      end;
      if WaitforGenericResponseConfirmation(giUID, vsCommand , rhMsgRecieved, MNIRspSpeed) then
      begin
        shpcbHBCorrection.Brush.Color:=clLime;
        shpcbHBCorrection.Visible:=false;
      end
      else
      begin
        ShowMessage('Failed: Stop Alignment Enable');
        shpcbHBCorrection.Brush.Color:=clRed;
      end;
    end;

    //Stop Alignment Travel Percentage
    DataUn.dmDataModule.dsMNI2.Filtered := False;
    DataUn.dmDataModule.dsMNI2.Filter := 'ProjectName = ' + QuotedStr(cmbProject.Text) +  ' and UID = ' + cmbUid.Text;
    DataUn.dmDataModule.dsMNI2.Filtered := True;
    if teTravel.Text <> DataUn.dmDataModule.dsMNI2.FieldByName('StopAlignTravel').AsString then
    begin
      VariablesUn.ClearData;
      giSentCom := StrToInt(LeftStr(VariablesUn.MDC_StopAlign,3));
      ComFm.SetGenericParam(giUID, VariablesUn.MDC_StopAlign+'.0.'+FloatToStr(StrToFloat(teTravel.Text)*10),rhMsgRecieved);

      if WaitforGenericResponseConfirmation(giUID, VariablesUn.MDC_StopAlign+'.0.'+FloatToStr(StrToFloat(teTravel.Text)*10),rhMsgRecieved,MNIRspSpeed) then
      begin
        shpteTravel.Brush.Color:=clLime;
        shpteTravel.Visible:=false;
      end
      else
      begin
        ShowMessage('Failed: Stop Alignment Travel');
        shpteTravel.Brush.Color:=clRed;
      end;
    end;

    //SW PORT Button Personality
    DataUn.dmDataModule.dsMNI2.Filtered := False;
    DataUn.dmDataModule.dsMNI2.Filter := 'ProjectName = ' + QuotedStr(cmbProject.Text) +  ' and UID = ' + cmbUid.Text;
    DataUn.dmDataModule.dsMNI2.Filtered := True;
    if(cmbSWPerson.Text='3T') then
    begin
      SwitchPerson:=9;
    end
    else if(cmbSWPerson.Text='3L') then
    begin
      SwitchPerson:=17;
    end
    else if(cmbSWPerson.Text='3M') then
    begin
      SwitchPerson:=33;
    end
    else if(cmbSWPerson.Text='2L') then
    begin
      SwitchPerson:=18;
    end
    else if(cmbSWPerson.Text='2M') then
    begin
      SwitchPerson:=34;
    end
    else if(cmbSWPerson.Text='1A') then
    begin
      SwitchPerson:=4;
    end
    else if(cmbSWPerson.Text='1B') then
    begin
      SwitchPerson:=68;
    end
    else if(cmbSWPerson.Text='1C') then
    begin
      SwitchPerson:=132;
    end;
    case StrToInt(cmbSwitch.Text) of
      1:
      begin
        if DataUn.dmDataModule.dsMNI2.FieldByName('SW1BtnPerson').AsInteger<>SwitchPerson then
        begin
          VariablesUn.ClearData;
          giSentCom := StrToInt(VariablesUn.UID_BP);
          ComFm.SetGenericParam(giUID, VariablesUn.UID_BP+'.'+cmbSwitch.Text+'.0.'+IntToStr(SwitchPerson), rhMsgRecieved);
          if WaitforGenericResponseConfirmation(giUID, VariablesUn.UID_BP+'.'+cmbSwitch.Text+'.0.'+IntToStr(SwitchPerson), rhMsgRecieved,MNIRspSpeed) then
          begin
            shpcmbSWPerson.Brush.Color:=clLime;
            shpcmbSWPerson.Visible:=false;
          end
          else
          begin
            ShowMessage('Failed: Set Button Personality SW1');
            shpcmbSWPerson.Brush.Color:=clRed;
          end;
        end;
      end;
      2:
      begin
        if DataUn.dmDataModule.dsMNI2.FieldByName('SW2BtnPerson').AsInteger<>SwitchPerson then
        begin
          VariablesUn.ClearData;
          giSentCom := StrToInt(VariablesUn.UID_BP);
          ComFm.SetGenericParam(giUID, VariablesUn.UID_BP+'.'+cmbSwitch.Text+'.0.'+IntToStr(SwitchPerson), rhMsgRecieved);
          if WaitforGenericResponseConfirmation(giUID, VariablesUn.UID_BP+'.'+cmbSwitch.Text+'.0.'+IntToStr(SwitchPerson), rhMsgRecieved,MNIRspSpeed) then
          begin
            shpcmbSWPerson.Brush.Color:=clLime;
            shpcmbSWPerson.Visible:=false;
          end
          else
          begin
            ShowMessage('Failed: Set Button Personality SW2');
            shpcmbSWPerson.Brush.Color:=clRed;
          end;
        end;
      end;
      3:
      begin
        if DataUn.dmDataModule.dsMNI2.FieldByName('SW3BtnPerson').AsInteger<>SwitchPerson then
        begin
          VariablesUn.ClearData;
          giSentCom := StrToInt(VariablesUn.UID_BP);
          ComFm.SetGenericParam(giUID, VariablesUn.UID_BP+'.'+cmbSwitch.Text+'.0.'+IntToStr(SwitchPerson), rhMsgRecieved);
          if WaitforGenericResponseConfirmation(giUID, VariablesUn.UID_BP+'.'+cmbSwitch.Text+'.0.'+IntToStr(SwitchPerson), rhMsgRecieved,MNIRspSpeed) then
          begin
            shpcmbSWPerson.Brush.Color:=clLime;
            shpcmbSWPerson.Visible:=false;
          end
          else
          begin
            ShowMessage('Failed: Set Button Personality SW3');
            shpcmbSWPerson.Brush.Color:=clRed;
          end;

        end;
      end;
      4:
      begin
        if DataUn.dmDataModule.dsMNI2.FieldByName('SW4BtnPerson').AsInteger<>SwitchPerson then
        begin
          VariablesUn.ClearData;
          giSentCom := StrToInt(VariablesUn.UID_BP);
          ComFm.SetGenericParam(giUID, VariablesUn.UID_BP+'.'+cmbSwitch.Text+'.0.'+IntToStr(SwitchPerson), rhMsgRecieved);
          if WaitforGenericResponseConfirmation(giUID, VariablesUn.UID_BP+'.'+cmbSwitch.Text+'.0.'+IntToStr(SwitchPerson), rhMsgRecieved,MNIRspSpeed)then
          begin
            shpcmbSWPerson.Brush.Color:=clLime;
            shpcmbSWPerson.Visible:=false;
          end
          else
          begin
            ShowMessage('Failed: Set Button Personality SW4');
            shpcmbSWPerson.Brush.Color:=clRed;
          end;
        end;
      end;
    end;
     // Switch ZGN
    DataUn.dmDataModule.dsMNI2.Filtered := False;
    DataUn.dmDataModule.dsMNI2.Filter := 'ProjectName = ' + QuotedStr(cmbProject.Text) +  ' and UID = ' + cmbUid.Text;
    DataUn.dmDataModule.dsMNI2.Filtered := True;
    case StrToInt(cmbSwitch.Text) of
      1:
      begin
        if DataUn.dmDataModule.dsMNI2.FieldByName('SwPort1FWDAddr').AsString<> teSWFWDAddr.Text then
        begin
          VariablesUn.ClearData;
          giSentCom := StrToInt(VariablesUn.UID_LS1_ADDR);
          ComFm.SetGenericParam(giUID, VariablesUn.UID_LS1_ADDR+'.'+ teSWFWDAddr.Text, rhMsgRecieved);
          if WaitforGenericResponseConfirmation(giUID, VariablesUn.UID_LS1_ADDR+'.'+ teSWFWDAddr.Text, rhMsgRecieved,MNIRspSpeed)then
          begin
            teSWFWDAddr.Color:=clWhite;

          end
          else
          begin
            ShowMessage('Failed: Set ZGN SW1');
             teSWFWDAddr.Color:=clRed;
          end;
        end;
      end;
      2:
      begin
        if DataUn.dmDataModule.dsMNI2.FieldByName('SwPort2FWDAddr').AsString<> teSWFWDAddr.Text then
        begin
          VariablesUn.ClearData;
          giSentCom := StrToInt(VariablesUn.UID_LS2_ADDR);
          ComFm.SetGenericParam(giUID, VariablesUn.UID_LS2_ADDR+'.'+ teSWFWDAddr.Text, rhMsgRecieved);
          if WaitforGenericResponseConfirmation(giUID, VariablesUn.UID_LS2_ADDR+'.'+ teSWFWDAddr.Text, rhMsgRecieved,MNIRspSpeed) then
          begin
            teSWFWDAddr.Color:=clWhite;

          end
          else
          begin
             teSWFWDAddr.Color:=clRed;
          end;
        end;
      end;
      3:
      begin
        if DataUn.dmDataModule.dsMNI2.FieldByName('SwPort3FWDAddr').AsString<> teSWFWDAddr.Text then
        begin
          VariablesUn.ClearData;
          giSentCom := StrToInt(VariablesUn.UID_LS3_ADDR);
          ComFm.SetGenericParam(giUID, VariablesUn.UID_LS3_ADDR+'.'+ teSWFWDAddr.Text, rhMsgRecieved);
          if WaitforGenericResponseConfirmation(giUID, VariablesUn.UID_LS3_ADDR+'.'+ teSWFWDAddr.Text, rhMsgRecieved,MNIRspSpeed) then
          begin
            teSWFWDAddr.Color:=clWhite;

          end
          else
          begin
             teSWFWDAddr.Color:=clRed;
          end;
        end;
      end;
      4:
      begin
        if DataUn.dmDataModule.dsMNI2.FieldByName('SwPort4FWDAddr').AsString<> teSWFWDAddr.Text then
        begin
          VariablesUn.ClearData;
          giSentCom := StrToInt(VariablesUn.UID_LS4_ADDR);
          ComFm.SetGenericParam(giUID, VariablesUn.UID_LS4_ADDR+'.'+ teSWFWDAddr.Text, rhMsgRecieved);
          if WaitforGenericResponseConfirmation(giUID, VariablesUn.UID_LS4_ADDR+'.'+ teSWFWDAddr.Text, rhMsgRecieved,MNIRspSpeed) then
          begin
            teSWFWDAddr.Color:=clWhite;

          end
          else
          begin
             teSWFWDAddr.Color:=clRed;
          end;
        end;
      end;
    end;
    //All Ports Enabled
    DataUn.dmDataModule.dsMNI2.Filtered := False;
    DataUn.dmDataModule.dsMNI2.Filter := 'ProjectName = ' + QuotedStr(cmbProject.Text) +  ' and UID = ' + cmbUid.Text;
    DataUn.dmDataModule.dsMNI2.Filtered := True;
    intDS:= GetDisabledMotorsIntDS;
    intConfig:= GetDisabledMotorsIntConfig;
    if(intDS<>intConfig) then
    begin
      VariablesUn.ClearData;
      giSentCom := StrToInt(VariablesUn.UID_LSE);
      ComFm.SetGenericParam(giUID, VariablesUn.UID_LSE+'.0.0.'+IntToStr(intConfig), rhMsgRecieved);
      if WaitforGenericResponseConfirmation(giUID, VariablesUn.UID_LSE+'.0.0.'+IntToStr(intConfig), rhMsgRecieved,MNIRspSpeed)then
      begin
        shpcbSWEnabled.Brush.color:=clLime;
        shpcbSWEnabled.Visible:=false;
      end
      else
      begin
        shpcbSWEnabled.Brush.Color:=clRed;
      end;
    end;

    //All Ports Fwd Enabled
    DataUn.dmDataModule.dsMNI2.Filtered := False;
    DataUn.dmDataModule.dsMNI2.Filter := 'ProjectName = ' + QuotedStr(cmbProject.Text) +  ' and UID = ' + cmbUid.Text;
    DataUn.dmDataModule.dsMNI2.Filtered := True;
    intDS:= GetEnabledFWDMOIntDS;
    intConfig:= GetEnabledFWDMOIntConfig;
    if(intDS<>intConfig) then
    begin
      VariablesUn.ClearData;
      giSentCom := StrToInt(VariablesUn.UID_LSF);
      ComFm.SetGenericParam(giUID, VariablesUn.UID_LSF+'.0.0.'+IntToStr(intConfig), rhMsgRecieved);
      if WaitforGenericResponseConfirmation(giUID, VariablesUn.UID_LSF+'.0.0.'+IntToStr(intConfig), rhMsgRecieved,MNIRspSpeed) then
      begin
        shpcbSWFWDEnabled.Brush.Color:=clLime;
        shpcbSWFWDEnabled.Visible:=false;
      end
      else
      begin
        shpcbSWFWDEnabled.Brush.Color:=clRed;
      end;
    end;

    //All Ports Mode
    DataUn.dmDataModule.dsMNI2.Filtered := False;
    DataUn.dmDataModule.dsMNI2.Filter := 'ProjectName = ' + QuotedStr(cmbProject.Text) +  ' and UID = ' + cmbUid.Text;
    DataUn.dmDataModule.dsMNI2.Filtered := True;
    intDS:= GetPortModeIntDS;
    intConfig:= GetPortModeIntConfig;
    if(intDS<>intConfig) then
    begin
      VariablesUn.ClearData;
      giSentCom := StrToInt(VariablesUn.UID_PortMode);
      ComFm.SetGenericParam(giUID, VariablesUn.UID_PortMode+'.0.0.'+IntToStr(intConfig), rhMsgRecieved);
      if WaitforGenericResponseConfirmation(giUID, VariablesUn.UID_PortMode+'.0.0.'+IntToStr(intConfig), rhMsgRecieved,MNIRspSpeed)then
      begin
        shpcmbSWMode.Brush.Color:=clLime;
        shpcmbSWMode.Visible:=false;
      end
      else
      begin
        shpcmbSWMode.Brush.Color:=clRed;
      end;
    end;

    //Alias
    DataUn.dmDataModule.dsMNI2.Filtered := False;
    DataUn.dmDataModule.dsMNI2.Filter := 'ProjectName = ' + QuotedStr(cmbProject.Text) +  ' and UID = ' + cmbUid.Text;
    DataUn.dmDataModule.dsMNI2.Filtered := True;
    if(txtAlias.Text<>DataUn.dmDataModule.dsMNI2.FieldByName('Alias').AsString) then
    begin
      AliasText:= txtAlias.text;
      for I := 0 to 12-Length(AliasText) do
      begin
        AliasText:=AliasText+Char(#32);
      end;

      //setLength(AliasText,12);

      VariablesUn.ClearData;
      giSentCom := StrToInt(VariablesUn.UID_NAME);
      ComFm.SetGenericParam(giUID, VariablesUn.UID_NAME+'.'+AliasText, rhMsgRecieved);
      if WaitforGenericResponseConfirmation(giUID, VariablesUn.UID_NAME+'.'+AliasText, rhMsgRecieved,MNIRspSpeed)then
      begin
        txtAlias.Color:=clWhite;
      end
      else
      begin
        //ShowMessage('Failed: Alias');
        txtAlias.Color:=clRed;
      end;
    end
    else
    begin
      txtAlias.Brush.Color:=clWhite;
    end;

    //FeedBackLED
    DataUn.dmDataModule.dsMNI2.Filtered := False;
    DataUn.dmDataModule.dsMNI2.Filter := 'ProjectName = ' + QuotedStr(cmbProject.Text) +  ' and UID = ' + cmbUid.Text;
    DataUn.dmDataModule.dsMNI2.Filtered := True;



    giSentCom := StrToInt(VariablesUn.UID_LED);
    intLED:=0;
    if cmbSWLed.Text='ON' then
    begin
      SWtemp:=true;
    end
    else
    begin
      SWtemp:=false;
    end;

    if (DataUn.dmDataModule.dsMNI2.FieldByName('LEDsw'+cmbSwitch.Text).AsBoolean)<>SWtemp then
    begin
      if (DataUn.dmDataModule.dsMNI2.FieldByName('LEDsw1').AsBoolean) and (cmbSwitch.Text<>'1')then
      begin
        intLED:=VariablesUn.Set_a_Bit(intLED,0);
      end;
      if (DataUn.dmDataModule.dsMNI2.FieldByName('LEDsw2').AsBoolean) and (cmbSwitch.Text<>'2') then
      begin
        intLED:=VariablesUn.Set_a_Bit(intLED,1);
      end;
      if (DataUn.dmDataModule.dsMNI2.FieldByName('LEDsw3').AsBoolean) and (cmbSwitch.Text<>'3') then
      begin
        intLED:=VariablesUn.Set_a_Bit(intLED,2);
      end;
      if (DataUn.dmDataModule.dsMNI2.FieldByName('LEDsw4').AsBoolean) and (cmbSwitch.Text<>'4') then
      begin
        intLED:=VariablesUn.Set_a_Bit(intLED,3);
      end;

      if cmbSWLed.Text='ON' then
      begin
        intLED:=VariablesUn.Set_a_Bit(intLED,StrToInt(cmbSwitch.text)-1);
      end;


      ComFm.SetGenericParam(giUID, VariablesUn.UID_LED+'.0.0.'+IntToStr(intLED), rhMsgRecieved);
      if WaitforGenericResponseConfirmation(giUID, VariablesUn.UID_LED+'.0.0.'+IntToStr(intLED), rhMsgRecieved,MNIRspSpeed) then
      begin
        shpLED.Brush.Color:=clLime;
        shpLED.Visible:=false;
      end
      else
      begin
        //ShowMessage('Failed: Feedback LED');
        shpLED.Brush.Color:=clRed;
      end;
    end;


    if flgIDChange<>false then
    begin
      GetVirtualMotorsStatus(Nil);
      flgIDChange:=false;
    end;
    //sgVirtualMotors.Selection:= TGridRect(Rect(-1, -1, -1, -1));
    //sgVirtualMotors.clear;
    //sgVirtualMotors.Invalidate;
    //sgVirtualMotors.Selection:= TGridRect(Rect(-1, -1, -1, -1));
//    sgVirtualMotors.Row:=1;
//    sgVirtualMotors.Col:=1;
//    cansel:=false;
//    sgVirtualMotorsSelectCell(self,1,1,cansel);
//
//    sgVirtualMotors.SetFocus;
    btUpdateParams.Color:=clBtnFace;
    loadData;
  finally
     btUpdateParams.OnClick:=btUpdateParamsClick;
  end;
end;

procedure TMNI2ConfigFm.btUpdateParamsMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Sender is TPanel
  then with TPanel(Sender)
       do BevelOuter := bvLowered;
end;

procedure TMNI2ConfigFm.btUpdateParamsMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
if Sender is TPanel
  then with TPanel(Sender)
       do BevelOuter := bvRaised;
end;

procedure TMNI2ConfigFm.disableForm(Disable:Boolean);
begin
  if Disable then
  begin
    Scrollbox1.Enabled:=false;
    btUpdateParams.enabled:=false;
    imgMenu.enabled:=false;
    cmbProject.Enabled:=false;
    txtAlias.enabled:=false;
    cmbDeviceSelector.enabled:=false;
    btUpdateUID.enabled:=false;
    teUID.Enabled:=false;
    cmbUID.Enabled:=False;
  end
  else
  begin
    Scrollbox1.Enabled:=True;
    btUpdateParams.enabled:=True;
    imgMenu.Enabled:=True;
    cmbProject.Enabled:=True;
    txtAlias.enabled:=True;
    cmbDeviceSelector.enabled:=True;
    btUpdateUID.enabled:=True;
    teUID.Enabled:=True;
    cmbUID.Enabled:=True;
  end;

end;

procedure TMNI2ConfigFm.btUpdateUIDClick(Sender: TObject);
var
  I,I2,CMBIndex,DuplicateFlg:Integer;
begin

  if dmDataModule.DuplicateUIDCheck(teUID.Text) then
  begin
    ShowMEssage('Error: UID already exist. Choose Different ID.');
    exit;
  end;
  Screen.Cursor := crHourGlass;
  teUID.OnChange:=nil;
  disableForm(True);


  if (teUID.Text<>'') and (teUID.Text<>IntToStr(lastUID))then
  begin
//    SetLength(viUIDs,0);
//    giSentCom := StrToInt(LeftStr(VariablesUn.MNI_GetUIDs,1));
//    //giUID:=StrToInt(cmbUIDMNI.Text);
//    ComFm.GetGenericStatus(65280, VariablesUn.MNI_GetUIDs, rhMsgRecieved);
//    timerDelayUid:=0;
//    VariablesUn.delay(255);
//    While(timerDelayUid>0) do
//    begin
//      timerDelayUid:=0;
//      VariablesUn.delay(6000);
//    end;
//    //Set UID
//    DuplicateFlg:=0;
//
//    for I := 0 to Length(viUIDs)-1 do
//    begin
//      if(viUIDs[i]=StrToInt(teUID.text)) then
//      begin
//        DuplicateFlg:=1;
//        break;
//      end;
//    end;

    if VariablesUn.isnumber(teUID.text) then
    begin
      giUID := StrToInt(teUID.Text);
      giSentCom := StrToInt(VariablesUn.UID_ChangeUID);
      ComFm.SetGenericParam(lastUID, VariablesUn.UID_ChangeUID+'.0.'+IntToStr(Hi(StrToInt(teUID.text)))+'.'+IntToStr(Lo(StrToInt(teUID.text))), rhMsgRecieved);  //Sets UID
      if WaitforGenericResponseConfirmation(lastUID, VariablesUn.UID_ChangeUID+'.0.'+IntToStr(Hi(StrToInt(teUID.text)))+'.'+IntToStr(Lo(StrToInt(teUID.text))), rhMsgRecieved,MNIRspSpeed) then
      begin
        teUID.Color:=clwhite;
      end
      else
      begin
        teUID.Color:=clRed;
        exit;
      end;
      btUpdateParams.Color:=clbtnface;
    end;
    cmbUID.Clear;
    //LastUID:=StrToInt(teUID.text);
    DataUn.dmDataModule.dsMNI2.Filtered := False;
    DataUn.dmDataModule.dsMNI2.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);//' and UID = ' +IntToStr(LastUID);
    DataUn.dmDataModule.dsMNI2.Filtered := True;
    for I := 0 to DataUn.dmDataModule.dsMNI2.RecordCount-1 do
    begin
      cmbUID.AddItem(IntToStr(DataUn.dmDataModule.dsMNI2.FieldByName('UID').AsInteger),Nil);
      DataUn.dmDataModule.dsMNI2.Next;
    end;
    cmbUID.ItemIndex:=cmbUID.Items.IndexOf(teUID.text);

    DataUn.dmDataModule.dsMotors.Filtered := False;
    DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) + ' and HostUID = ' +IntToStr(LastUID);
    DataUn.dmDataModule.dsMotors.Filtered := True;

    while not DataUn.dmDataModule.dsMotors.Eof do
    begin
      DataUn.dmDataModule.dsMotors.edit;
      DataUn.dmDataModule.dsMotors.FieldByName('HostUID').AsInteger:= StrToInt(teUID.text);
      DataUn.dmDataModule.dsMotors.MergeChangeLog;

      //DataUn.dmDataModule.dsMotors.next;
    end;
    DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
    LastUID:=StrToInt(teUID.text);
    btUpdateUID.Color:=clbtnface;
  end
  else
  begin
    showMessage('UID Update Fail: No UID Entered!');
    btUpdateUID.Color:=clbtnface;
    teUID.Color:=clwhite;
  end;


  DisableForm(false);
  teUID.Clear;

  self.Enabled:=true;
  Screen.Cursor := crDefault;
  teUID.OnChange:=teUIDChange;
end;

Function TMNI2ConfigFm.GetDisabledMotorsIntDS:Integer;
var Binary:String;
begin
   if DataUn.dmDataModule.dsMNI2.FieldByName('SwPort1Enabled').AsBoolean then
  begin
    Binary:='0';
  end
  else
  begin
    Binary:='1';
  end;
  if DataUn.dmDataModule.dsMNI2.FieldByName('SwPort2Enabled').AsBoolean then
  begin
    Binary:=Binary+'0';
  end
  else
  begin
    Binary:=Binary+'1';
  end;
  if DataUn.dmDataModule.dsMNI2.FieldByName('SwPort3Enabled').AsBoolean then
  begin
    Binary:=Binary+'0';
  end
  else
  begin
    Binary:=Binary+'1';
  end;
  if DataUn.dmDataModule.dsMNI2.FieldByName('SwPort4Enabled').AsBoolean then
  begin
    Binary:=Binary+'0';
  end
  else
  begin
    Binary:=Binary+'1';
  end;

  Binary:=Binary+'1111';
  GetDisabledMotorsIntDS:=VariablesUn.BinToInt(ansireversestring(Binary));
end;

Function TMNI2ConfigFm.GetDisabledMotorsIntConfig:Integer;
var Binary:String;
begin
  if cmbSwitch.Text='1' then
  begin
    if(cbSWEnabled.checked)then
    begin
      Binary:='0';
    end
    else
    begin
      Binary:='1';
    end;
  end
  else
  begin
    if DataUn.dmDataModule.dsMNI2.FieldByName('SwPort1Enabled').AsBoolean then
    begin
      Binary:='0';
    end
    else
    begin
      Binary:='1';
    end;
  end;
  if cmbSwitch.Text='2' then
  begin
    if(cbSWEnabled.checked)then
    begin
      Binary:=Binary+'0';
    end
    else
    begin
      Binary:=Binary+'1';
    end;
  end
  else
  begin
    if DataUn.dmDataModule.dsMNI2.FieldByName('SwPort2Enabled').AsBoolean then
    begin
      Binary:=Binary+'0';
    end
    else
    begin
      Binary:=Binary+'1';
    end;
  end;
  if cmbSwitch.Text='3' then
  begin
    if(cbSWEnabled.checked)then
    begin
      Binary:=Binary+'0';
    end
    else
    begin
      Binary:=Binary+'1';
    end;
  end
  else
  begin
    if DataUn.dmDataModule.dsMNI2.FieldByName('SwPort3Enabled').AsBoolean then
    begin
      Binary:=Binary+'0';
    end
    else
    begin
      Binary:=Binary+'1';
    end;
  end;
  if cmbSwitch.Text='4' then
  begin
    if(cbSWEnabled.checked)then
    begin
      Binary:=Binary+'0';
    end
    else
    begin
      Binary:=Binary+'1';
    end;
  end
  else
  begin
    if DataUn.dmDataModule.dsMNI2.FieldByName('SwPort4Enabled').AsBoolean then
    begin
      Binary:=Binary+'0';
    end
    else
    begin
      Binary:=Binary+'1';
    end;
  end;
  Binary:=Binary+'1111';
  GetDisabledMotorsIntConfig:=VariablesUn.BinToInt(ansireversestring(Binary))
end;

Function TMNI2ConfigFm.GetEnabledFWDMOIntConfig:Integer;
var Binary:String;
begin
  if cmbSwitch.Text='1' then
  begin
    if(cbSWFWDEnabled.checked)then
    begin
      Binary:='1';
    end
    else
    begin
      Binary:='0';
    end;
  end
  else
  begin
    if DataUn.dmDataModule.dsMNI2.FieldByName('SwPort1FWDEnabled').AsBoolean then
    begin
      Binary:='1';
    end
    else
    begin
      Binary:='0';
    end;
  end;
  if cmbSwitch.Text='2' then
  begin
    if(cbSWFWDEnabled.checked)then
    begin
      Binary:=Binary+'1';
    end
    else
    begin
      Binary:=Binary+'0';
    end;
  end
  else
  begin
    if DataUn.dmDataModule.dsMNI2.FieldByName('SwPort2FWDEnabled').AsBoolean then
    begin
      Binary:=Binary+'1';
    end
    else
    begin
      Binary:=Binary+'0';
    end;
  end;
  if cmbSwitch.Text='3' then
  begin
    if(cbSWFWDEnabled.checked)then
    begin
      Binary:=Binary+'1';
    end
    else
    begin
      Binary:=Binary+'0';
    end;
  end
  else
  begin
    if DataUn.dmDataModule.dsMNI2.FieldByName('SwPort3FWDEnabled').AsBoolean then
    begin
      Binary:=Binary+'1';
    end
    else
    begin
      Binary:=Binary+'0';
    end;
  end;
  if cmbSwitch.Text='4' then
  begin
    if(cbSWFWDEnabled.checked)then
    begin
      Binary:=Binary+'1';
    end
    else
    begin
      Binary:=Binary+'0';
    end;
  end
  else
  begin
    if DataUn.dmDataModule.dsMNI2.FieldByName('SwPort4FWDEnabled').AsBoolean then
    begin
      Binary:=Binary+'1';
    end
    else
    begin
      Binary:=Binary+'0';
    end;
  end;
  Binary:=Binary+'0000';

  GetEnabledFWDMOIntConfig:=VariablesUn.BinToInt(ansireversestring(Binary));
end;

Function TMNI2ConfigFm.GetEnabledFWDMOIntDS:Integer;
var Binary:String;
begin
  if DataUn.dmDataModule.dsMNI2.FieldByName('SwPort1FWDEnabled').AsBoolean then
  begin
    Binary:='1';
  end
  else
  begin
    Binary:='0';
  end;
  if DataUn.dmDataModule.dsMNI2.FieldByName('SwPort2FWDEnabled').AsBoolean then
  begin
    Binary:=Binary+'1';
  end
  else
  begin
    Binary:=Binary+'0';
  end;
  if DataUn.dmDataModule.dsMNI2.FieldByName('SwPort3FWDEnabled').AsBoolean then
  begin
    Binary:=Binary+'1';
  end
  else
  begin
    Binary:=Binary+'0';
  end;
  if DataUn.dmDataModule.dsMNI2.FieldByName('SwPort4FWDEnabled').AsBoolean then
  begin
    Binary:=Binary+'1';
  end
  else
  begin
    Binary:=Binary+'0';
  end;
  Binary:=Binary+'0000';

  GetEnabledFWDMOIntDS:=VariablesUn.BinToInt(ansireversestring(Binary));
end;

Function TMNI2ConfigFm.GetPortModeIntConfig:Integer;
var Binary:String;
begin
  if cmbSwitch.Text='1' then
  begin
    Binary:= IntToStr(cmbSWMode.ItemIndex);
  end
  else
  begin
    Binary:=IntToStr(DataUn.dmDataModule.dsMNI2.FieldByName('SwPort1Mode').AsInteger);
  end;
  if cmbSwitch.Text='2' then
  begin
    Binary:= Binary+IntToStr(cmbSWMode.ItemIndex);
  end
  else
  begin
    Binary:=Binary+IntToStr(DataUn.dmDataModule.dsMNI2.FieldByName('SwPort2Mode').AsInteger);
  end;
  if cmbSwitch.Text='3' then
  begin
    Binary:= Binary+IntToStr(cmbSWMode.ItemIndex);
  end
  else
  begin
    Binary:=Binary+IntToStr(DataUn.dmDataModule.dsMNI2.FieldByName('SwPort3Mode').AsInteger);
  end;
  if cmbSwitch.Text='4' then
  begin
    Binary:= Binary+IntToStr(cmbSWMode.ItemIndex);
  end
  else
  begin
    Binary:=Binary+IntToStr(DataUn.dmDataModule.dsMNI2.FieldByName('SwPort4Mode').AsInteger);
  end;
  Binary:=Binary+'0000';
  GetPortModeIntConfig:=VariablesUn.BinToInt(ansireversestring(Binary));
end;

Function TMNI2ConfigFm.GetPortModeIntDS:Integer;
var Binary:String;
begin
  Binary:=IntToStr( DataUn.dmDataModule.dsMNI2.FieldByName('SwPort1Mode').AsInteger);
  Binary:=Binary+IntToStr( DataUn.dmDataModule.dsMNI2.FieldByName('SwPort2Mode').AsInteger);
  Binary:=Binary+IntToStr( DataUn.dmDataModule.dsMNI2.FieldByName('SwPort3Mode').AsInteger);
  Binary:=Binary+IntToStr( DataUn.dmDataModule.dsMNI2.FieldByName('SwPort4Mode').AsInteger);
  Binary:=Binary+'0000';
  GetPortModeIntDS:=VariablesUn.BinToInt(Binary)
end;

procedure TMNI2ConfigFm.cbHBCorrectionClick(Sender: TObject);
begin
  shpcbHBCorrection.Visible:=true;
  btUpdateParams.Color:=cllime;
end;

procedure TMNI2ConfigFm.cbSWEnabledClick(Sender: TObject);
begin
  shpcbSWEnabled.Visible:=true;
  btUpdateParams.Color:=cllime;
end;

procedure TMNI2ConfigFm.cbSWFWDEnabledClick(Sender: TObject);
begin
  shpcbSWFWDEnabled.Visible:=true;
  btUpdateParams.Color:=cllime;
end;

procedure TMNI2ConfigFm.cmbDeviceSelectorChange(Sender: TObject);
var
  I:Integer;
begin
  VariablesUn.giTop := Top;
  VariablesUn.giLeft := Left;
  if(cmbDeviceSelector.Text='MWC')then
  begin

    DataUn.dmDataModule.dsMWCChannels.Filtered := False;
    DataUn.dmDataModule.dsMWCChannels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsMWCChannels.Filtered := True;

    if not dmDataModule.dsMWCChannels.Eof then
    begin

      VariablesUn.giTop:=top;
      variablesUn.giLeft:=Left;
      Hide;
      MWCDisplayfm.show;
    end
    else
    begin
      ShowMessage('No MWC Devices Found');
      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('MDC');
    end;
  end
  else if (cmbDeviceSelector.Text='iQ3-DC-RF') then
    begin
      DataUn.dmDataModule.dsIQ3s.Filtered := False;
      DataUn.dmDataModule.dsIQ3s.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
      DataUn.dmDataModule.dsIQ3s.Filtered := True;

      if not dmDataModule.dsIQ3s.Eof then
      begin

        VariablesUn.giTop:=top;
        variablesUn.giLeft:=Left;
        IQ3RFDisplayfm.Show;
        Hide;
        //MWCConfigfm.Show;
      end
      else
      begin
        ShowMessage('No iQ3 Devices Found');
        cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('MTR');
      end;
    end
  else if(cmbDeviceSelector.Text='MTR')then
  begin
    VariablesUn.giTop:=top;
    variablesUn.giLeft:=Left;
    if Assigned (rh_ShowMain) then
      rh_ShowMain(Self);
    Hide;
    VariablesUn.PassLoopFlag:=True;
  end
  else if(cmbDeviceSelector.Text='MNI')then
  begin
    DataUn.dmDataModule.dsMNI.Filtered := False;
    DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsMNI.Filtered := True;

    if not dmDataModule.dsMNI.Eof then
    begin

      Hide;
      MNIconfigFm.Show;
      VariablesUn.giTop:=top;
      variablesUn.giLeft:=Left;
    end
    else
    begin
      ShowMessage('No MNI Devices Found');
      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('MDC');
    end;


  end
  else if(cmbDeviceSelector.Text='IQMLC2')then
  begin
    DataUn.dmDataModule.dsIQMLC2.Filtered := False;
    DataUn.dmDataModule.dsIQMLC2.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsIQMLC2.Filtered := True;

    if not dmDataModule.dsIQMLC2.Eof then
    begin
      Hide;
      VariablesUn.giTop := Top;
      VariablesUn.giLeft := Left;
      if Assigned (rh_showIQMLC2Config) then
        rh_showIQMLC2Config(self);
      //IQMLC2configFm.Show;
    end
    else
    begin
      ShowMessage('No IQMLC2 Devices Found');
      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('MDC');
    end;
  end
  else if(cmbDeviceSelector.Text='PowerPanel')then
  begin
    DataUn.dmDataModule.dsPowerPanel.Filtered := False;
    DataUn.dmDataModule.dsPowerPanel.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsPowerPanel.Filtered := True;

    if not dmDataModule.dsPowerPanel.Eof then
    begin
      Hide;
      VariablesUn.giTop := Top;
      VariablesUn.giLeft := Left;
      PowerPanelConfigFm.Show;
    end
    else
    begin
      ShowMessage('No IQ2-DC Power Panel Devices Found');
      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('MDC');
    end;
  end
  else if(cmbDeviceSelector.Text='Motor Maint')then
  begin
    DataUn.dmDataModule.dsMotors.Filtered := False;
    DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) + ' and MotorType = 69 or MotorType = 66';
    DataUn.dmDataModule.dsMotors.Filtered := True;

    if not dmDataModule.dsMotors.Eof then
    begin
      VariablesUn.giTop := Top;
      VariablesUn.giLeft := Left;
      Hide;
      MotorMaintenanceDisplayFm.Show;
      MotorMaintenanceDisplayFm.cmbUidMM.ItemIndex:= 0;
      MotorMaintenanceDisplayFm.cmbUidMM.OnChange(self);
    end
    else
    begin
      ShowMessage('No Motors Found');
      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('MDC');
    end;
  end;
end;

procedure TMNI2ConfigFm.cmbSWLedChange(Sender: TObject);
begin
  shpLED.Visible:=true;
  btUpdateParams.Color:=cllime;
end;

procedure TMNI2ConfigFm.cmbProjectChange(Sender: TObject);
begin
  DataUn.dmDataModule.dsMNI2.Filtered := False;
  DataUn.dmDataModule.dsMNI2.Filter := 'ProjectName = ' + QuotedStr(cmbProject.Text) +  ' and UID = ' + cmbUID.Text;
  DataUn.dmDataModule.dsMNI2.Filtered := True;

  if not (DataUn.dmDataModule.dsMNI2.Eof) then
  begin
    DataUn.dmDataModule.dsMNI2.Edit;
    DataUn.dmDataModule.dsMNI2.FieldByName('Alias').AsString := txtAlias.Text;
    VariablesUn.gsAlias := txtAlias.Text;
    DataUn.dmDataModule.dsMNI2.Post;
    DataUn.dmDataModule.dsMNI2.MergeChangeLog;
  end;
  DataUn.dmDataModule.dsMNI2.Filtered := False;
  DataUn.dmDataModule.dsMNI2.Filter := 'ProjectName = ' + QuotedStr(cmbProject.Text);
  DataUn.dmDataModule.dsMNI2.Filtered := True;
  loadUID;
  GetVirtualMotorsStatus(self);
  loaddata;

end;

procedure TMNI2ConfigFm.cmbSwitchChange(Sender: TObject);
begin
  clearGreen;
  loadData;

end;

Procedure TMNI2ConfigFm.StopEvents;
begin
  cbHBCorrection.OnClick:=nil;
  teTravel.OnChange:=nil;
  cbSWEnabled.OnClick:=nil;
  cmbSWPerson.OnChange:=nil;
  cbSWFWDEnabled.OnClick:=nil;
  teSWFWDAddr.OnChange:=nil;
  cmbSWMode.OnChange:=nil;
  cmbSWLed.OnChange:=nil;
end;

Procedure TMNI2ConfigFm.ClearGreen;
begin
  shpcbSWEnabled.Pen.Color:=clLime;
  shpcbSWEnabled.Visible:=false;

  shpcmbSWPerson.Pen.Color:=clLime;
  shpcmbSWPerson.Visible:=false;

  shpcbSWFWDEnabled.Pen.Color:=clLime;
  shpcbSWFWDEnabled.Visible:=false;

  teSWFWDAddr.Color:=clWhite;

  shpcmbSWMode.Pen.Color:=cllime;
  shpcmbSWMode.Visible:=false;

  shpLed.Pen.Color:=cllime;
  shpLed.Visible:=false;

  shpcbHBCorrection.Pen.Color:=cllime;
  shpcbHBCorrection.Visible:=false;

  shpteTravel.Pen.Color:=cllime;
  shpteTravel.Visible:=false;
end;

Procedure TMNI2ConfigFm.StartEvents;
begin
  cbSWEnabled.OnClick:=cbSWEnabledClick;
  cmbSWPerson.OnChange:=cmbSWPersonChange;
  cbSWFWDEnabled.OnClick:=cbSWFWDEnabledClick;
  teSWFWDAddr.OnChange:=teSWFWDAddrChange;
  cmbSWMode.OnChange:=cmbSWModeChange;
  cmbSWLed.OnChange:=cmbSWLedChange;
  cbHBCorrection.OnClick:=cbHBCorrectionClick;
  teTravel.OnChange:=teTravelChange;
end;

procedure TMNI2ConfigFm.cmbSWModeChange(Sender: TObject);
begin
  shpcmbSWMode.Visible:=true;
  btUpdateParams.Color:=cllime;
end;

procedure TMNI2ConfigFm.cmbSWPersonChange(Sender: TObject);
begin
  shpcmbSWPerson.Visible:=true;
  btUpdateParams.Color:=cllime;
end;

procedure TMNI2ConfigFm.cmbUidChange(Sender: TObject);
var
  I:Integer;
begin
  GetVirtualMotorsStatus(self);
  LastUID:=StrToInt(cmbUid.text);
  LoadData;
end;

procedure TMNI2ConfigFm.GetVirtualMotorsStatus(Sender: TObject);
var
  I:Integer;
  canselect:boolean;
begin
  giUID:=StrToInt(cmbUid.Text);
  UIDList.Clear;
  SomfyIDList.Clear;
  canselect:=true;
  sgVirtualMotors.Selection := TGridRect(Rect(-1, -1, -1, -1));
  sgVirtualMotors.Options:= sgVirtualMotors.Options - [goRowSelect];
  for I := 0 to 19 do
  Begin
    //Gets Mechonet UIDS
    VariablesUn.ClearData;
    giSentCom := StrToInt(LeftStr(VariablesUn.MDC_AdapterRev,3));  //Get UIDS
    ComFm.GetGenericStatus(giUID, VariablesUn.MDC_AdapterRev+'.0.'+IntToStr(I), rhMsgRecieved);
    WaitforGenericResponse(giUID, VariablesUn.MDC_AdapterRev+'.0.'+IntToStr(I), rhMsgRecieved,MNIRspSpeed);
    if VariablesUn.DataMidLowConvert<>65535 then
    begin
      sgVirtualMotors.Cells[1,I+1]:=IntToStr(VariablesUn.DataMidLowConvert);
      UIDList.Add(IntToStr(VariablesUn.DataMidLowConvert));
    end
    else
    begin
      sgVirtualMotors.Cells[1,I+1]:='Disabled';
      UIDList.Add('Disabled');
    end;


    //gets Somfy UIDS
    VariablesUn.ClearData;
    giSentCom := StrToInt(LeftStr(VariablesUn.MDC_SUID,3));  //Get UIDS
    ComFm.GetGenericStatus(giUID, VariablesUn.MDC_SUID+'.0.'+IntToStr(I), rhMsgRecieved);
    WaitforGenericResponse(giUID, VariablesUn.MDC_SUID+'.0.'+IntToStr(I), rhMsgRecieved,MNIRspSpeed);
    if(IntToHex(VariablesUn.giDataLH,2)+IntToHex(VariablesUn.giDataLM,2)+IntToHex(VariablesUn.giDataLL,2)<>'FFFFFF')then
    begin
      sgVirtualMotors.Cells[2,I+1]:=IntToHex(VariablesUn.giDataLH,2)+IntToHex(VariablesUn.giDataLM,2)+IntToHex(VariablesUn.giDataLL,2);
      SomfyIDList.Add(IntToHex(VariablesUn.giDataLH,2)+IntToHex(VariablesUn.giDataLM,2)+IntToHex(VariablesUn.giDataLL,2));
    end
    else
    begin
      sgVirtualMotors.Cells[2,I+1]:='Disabled';
      SomfyIDList.Add('Disabled');
    end;
  End;
  //sgVirtualMotorsSelectCell(self,0,0,canselect);
  //sgVirtualMotors.col:=1;
  //sgVirtualMotors.row:=0;
  //sgVirtualMotors.ClearSelections;

  if loadFlag then
  begin
    LastCellStr:=sgVirtualMotors.cells[1,1];
    loadFlag:=False;
  end;
end;

procedure TMNI2ConfigFm.WaitforGenericResponse(UID:Integer;Command:String;rh:TNotifyEvent;ResendTime:Integer);
var
  PauseCount:Integer;
begin
  passFlag:=false;
  PauseCount:=0;
  while not PassFlag do
  begin
      PauseCount:=PauseCount+1;
      delay(10);
      if (PauseCount=ResendTime) or (PauseCount=ResendTime*2) then
        ComFm.GetGenericStatus(UID,Command,rh);
      if PauseCount>ResendTime*3 then
      begin
        ShowMessage('Failed Command');
        Break;
      end;
  end;
end;

procedure TMNI2ConfigFm.rhMsgRecieved(sender: TObject);
var
  BinHolder:string;
  I: Integer;
begin
  try
    if (giSentCom = VariablesUn.giCommand) and (giUID = VariablesUn.giUID_rx) then
    begin
      case VariablesUn.giCommand of
       11:
        begin
          DataUn.dmDataModule.dsMNI2.Filtered := False;
          DataUn.dmDataModule.dsMNI2.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUID.text;
          DataUn.dmDataModule.dsMNI2.Filtered := True;
          DataUn.dmDataModule.dsMNI2.Edit;
          for I := 0 to DataUn.dmDataModule.dsMNI2.RecordCount-1 do
          begin
            DataUn.dmDataModule.dsMNI2.FieldByName('UID').AsString:=teUID.text;
          end;
          DataUn.dmDataModule.dsMNI2.MergeChangeLog;
          DataUn.dmDataModule.dsMNI2.SaveToFile(VariablesUn.gsDataPath +'dsMNI2.txt', dfXML);
          PassFlag:=True;
        end;
       23:
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsMNI2.Filtered := False;
          DataUn.dmDataModule.dsMNI2.Filter := 'ProjectName = ' + QuotedStr(cmbProject.Text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsMNI2.Filtered := True;
          dmDataModule.dsMNI2.Edit;
          DataUn.dmDataModule.dsMNI2.FieldByName('LEDsw1').AsBoolean:=VariablesUn.GetBit(variablesUn.giDataL,0);
          DataUn.dmDataModule.dsMNI2.FieldByName('LEDsw2').AsBoolean:=VariablesUn.GetBit(variablesUn.giDataL,1);
          DataUn.dmDataModule.dsMNI2.FieldByName('LEDsw3').AsBoolean:=VariablesUn.GetBit(variablesUn.giDataL,2);
          DataUn.dmDataModule.dsMNI2.FieldByName('LEDsw4').AsBoolean:=VariablesUn.GetBit(variablesUn.giDataL,3);
          DataUn.dmDataModule.dsMNI2.MergeChangeLog;

        end;
       27:                   // Port Enabled
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsMNI2.Filtered := False;
          DataUn.dmDataModule.dsMNI2.Filter := 'ProjectName = ' + QuotedStr(cmbProject.Text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsMNI2.Filtered := True;
          dmDataModule.dsMNI2.Edit;
          DataUn.dmDataModule.dsMNI2.FieldByName('SwPort1Enabled').AsBoolean:=not VariablesUn.GetBit(VariablesUn.giDataL,0);
          DataUn.dmDataModule.dsMNI2.FieldByName('SwPort2Enabled').AsBoolean:=not VariablesUn.GetBit(VariablesUn.giDataL,1);
          DataUn.dmDataModule.dsMNI2.FieldByName('SwPort3Enabled').AsBoolean:=not VariablesUn.GetBit(VariablesUn.giDataL,2);
          DataUn.dmDataModule.dsMNI2.FieldByName('SwPort4Enabled').AsBoolean:=not VariablesUn.GetBit(VariablesUn.giDataL,3);

          DataUn.dmDataModule.dsMNI2.MergeChangeLog;

        end;
       28:                   // Port FWD Enabled
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsMNI2.Filtered := False;
          DataUn.dmDataModule.dsMNI2.Filter := 'ProjectName = ' + QuotedStr(cmbProject.Text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsMNI2.Filtered := True;
          dmDataModule.dsMNI2.Edit;
          DataUn.dmDataModule.dsMNI2.FieldByName('SwPort1FWDEnabled').AsBoolean:= VariablesUn.GetBit(VariablesUn.giDataL,0);
          DataUn.dmDataModule.dsMNI2.FieldByName('SwPort2FWDEnabled').AsBoolean:= VariablesUn.GetBit(VariablesUn.giDataL,1);
          DataUn.dmDataModule.dsMNI2.FieldByName('SwPort3FWDEnabled').AsBoolean:= VariablesUn.GetBit(VariablesUn.giDataL,2);
          DataUn.dmDataModule.dsMNI2.FieldByName('SwPort4FWDEnabled').AsBoolean:= VariablesUn.GetBit(VariablesUn.giDataL,3);

          DataUn.dmDataModule.dsMNI2.MergeChangeLog;

        end;
       29:   // Button Personality
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsMNI2.Filtered := False;
          DataUn.dmDataModule.dsMNI2.Filter := 'ProjectName = ' + QuotedStr(cmbProject.Text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsMNI2.Filtered := True;
          dmDataModule.dsMNI2.Edit;
          if(variablesUn.giDataH=1) then
          begin
            DataUn.dmDataModule.dsMNI2.FieldByName('SW1BtnPerson').AsString := IntToStr(VariablesUn.giDataL);
            DataUn.dmDataModule.dsMNI2.MergeChangeLog;
          end
          else if(variablesUn.giDataH=2) then
          begin
            DataUn.dmDataModule.dsMNI2.FieldByName('SW2BtnPerson').AsString := IntToStr(VariablesUn.giDataL);
            DataUn.dmDataModule.dsMNI2.MergeChangeLog;
          end
          else if(variablesUn.giDataH=3) then
          begin
            DataUn.dmDataModule.dsMNI2.FieldByName('SW3BtnPerson').AsString := IntToStr(VariablesUn.giDataL);
            DataUn.dmDataModule.dsMNI2.MergeChangeLog;
          end
          else if(variablesUn.giDataH=4) then
          begin
            DataUn.dmDataModule.dsMNI2.FieldByName('SW4BtnPerson').AsString := IntToStr(VariablesUn.giDataL);
            DataUn.dmDataModule.dsMNI2.MergeChangeLog;
          end;
        end;
       36:                   // Port Mode
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsMNI2.Filtered := False;
          DataUn.dmDataModule.dsMNI2.Filter := 'ProjectName = ' + QuotedStr(cmbProject.Text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsMNI2.Filtered := True;
          dmDataModule.dsMNI2.Edit;
          DataUn.dmDataModule.dsMNI2.FieldByName('SwPort1Mode').AsInteger:=ord(VariablesUn.GetBit(VariablesUn.giDataL,0));
          DataUn.dmDataModule.dsMNI2.FieldByName('SwPort2Mode').AsInteger:=ord(VariablesUn.GetBit(VariablesUn.giDataL,1));
          DataUn.dmDataModule.dsMNI2.FieldByName('SwPort3Mode').AsInteger:=ord(VariablesUn.GetBit(VariablesUn.giDataL,2));
          DataUn.dmDataModule.dsMNI2.FieldByName('SwPort4Mode').AsInteger:=ord(VariablesUn.GetBit(VariablesUn.giDataL,3));

          DataUn.dmDataModule.dsMNI2.MergeChangeLog;
        end;
       44:                   // switch 1 ZGN
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsMNI2.Filtered := False;
          DataUn.dmDataModule.dsMNI2.Filter := 'ProjectName = ' + QuotedStr(cmbProject.Text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsMNI2.Filtered := True;
          dmDataModule.dsMNI2.Edit;
          DataUn.dmDataModule.dsMNI2.FieldByName('SwPort1FWDAddr').AsString := VariablesUn.BuildZGN();
          DataUn.dmDataModule.dsMNI2.MergeChangeLog;

        end;
       45:                   // switch 2 ZGN
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsMNI2.Filtered := False;
          DataUn.dmDataModule.dsMNI2.Filter := 'ProjectName = ' + QuotedStr(cmbProject.Text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsMNI2.Filtered := True;
          dmDataModule.dsMNI2.Edit;
          DataUn.dmDataModule.dsMNI2.FieldByName('SwPort2FWDAddr').AsString := VariablesUn.BuildZGN();
          DataUn.dmDataModule.dsMNI2.MergeChangeLog;

        end;
       46:                   // switch 3 ZGN
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsMNI2.Filtered := False;
          DataUn.dmDataModule.dsMNI2.Filter := 'ProjectName = ' + QuotedStr(cmbProject.Text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsMNI2.Filtered := True;
          dmDataModule.dsMNI2.Edit;
          DataUn.dmDataModule.dsMNI2.FieldByName('SwPort3FWDAddr').AsString := VariablesUn.BuildZGN();
          DataUn.dmDataModule.dsMNI2.MergeChangeLog;

        end;
       47:                   // switch 4 ZGN
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsMNI2.Filtered := False;
          DataUn.dmDataModule.dsMNI2.Filter := 'ProjectName = ' + QuotedStr(cmbProject.Text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsMNI2.Filtered := True;
          dmDataModule.dsMNI2.Edit;
          DataUn.dmDataModule.dsMNI2.FieldByName('SwPort4FWDAddr').AsString := VariablesUn.BuildZGN();
          DataUn.dmDataModule.dsMNI2.MergeChangeLog;

        end;
       85:                   // Alias
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsMNI2.Filtered := False;
          DataUn.dmDataModule.dsMNI2.Filter := 'ProjectName = ' + QuotedStr(cmbProject.Text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsMNI2.Filtered := True;
          dmDataModule.dsMNI2.Edit;
          DataUn.dmDataModule.dsMNI2.FieldByName('Alias').AsString := Trim(VariablesUn.gsMotorType);
          DataUn.dmDataModule.dsMNI2.MergeChangeLog;
        end;
       199,149:
        begin
          case VariablesUn.giDataH of
            0:
            begin
              VariablesUn.giDataM:=VariablesUn.giDataLH;
              VariablesUn.giDataL:=VariablesUn.giDataLM;
              passFlag:=true;
            end;
            1,2:
            begin
              passFlag:=true;
            end;
            13:
            begin
              teLinkCount.Text:=IntToStr(VariablesUn.giDataL);
              PassFlag:=True;
            end;
            29:
            begin
              if VariablesUn.giDataM = 0 then
              begin
                DataUn.dmDataModule.dsMNI2.Filtered := False;
                DataUn.dmDataModule.dsMNI2.Filter := 'ProjectName = ' + QuotedStr(cmbProject.Text) +  ' and UID = ' + IntToStr(giUID);
                DataUn.dmDataModule.dsMNI2.Filtered := True;
                dmDataModule.dsMNI2.Edit;
                DataUn.dmDataModule.dsMNI2.FieldByName('StopAlignTravel').AsString := floatToStr(VariablesUn.giDataL/10);
                DataUn.dmDataModule.dsMNI2.MergeChangeLog;
                teTravel.Text:=floatToStr(VariablesUn.giDataL/10);
              end
              else
              begin
                DataUn.dmDataModule.dsMNI2.Filtered := False;
                DataUn.dmDataModule.dsMNI2.Filter := 'ProjectName = ' + QuotedStr(cmbProject.Text) +  ' and UID = ' + IntToStr(giUID);
                DataUn.dmDataModule.dsMNI2.Filtered := True;
                dmDataModule.dsMNI2.Edit;
                if VariablesUn.giDataL = 1 then
                begin
                  DataUn.dmDataModule.dsMNI2.FieldByName('StopAlignEnabled').AsBoolean := true;
                  cbHBCorrection.Checked:=true;
                end
                else
                begin
                  DataUn.dmDataModule.dsMNI2.FieldByName('StopAlignEnabled').AsBoolean := False;
                  cbHBCorrection.Checked:=False;
                end;
                DataUn.dmDataModule.dsMNI2.MergeChangeLog;
              end;


              PassFlag:=True;
            end;
          end;
        end;
      end;
    end;
    except
    on E: Exception do
    begin
      MessageDlg('TMNI2ConfigFm.rhMsgRecieved: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TMNI2ConfigFm.rhShowMNI2Config(Sender: TObject);
begin
  show;
end;

procedure TMNI2ConfigFm.ScrollBox1MouseWheel(Sender: TObject;
  Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint;
  var Handled: Boolean);
var
  I: Integer;
begin
  scrollbox1.SetFocus;
  Handled := PtInRect(ScrollBox1.ClientRect, ScrollBox1.ScreenToClient(MousePos));
  if Handled then
    for I := 1 to Mouse.WheelScrollLines do
    try
      if WheelDelta > 0 then
        ScrollBox1.Perform(WM_VSCROLL, SB_LINEUP, 0)
      else
        ScrollBox1.Perform(WM_VSCROLL, SB_LINEDOWN, 0);
    finally
      ScrollBox1.Perform(WM_VSCROLL, SB_ENDSCROLL, 0);
    end;

end;

procedure TMNI2ConfigFm.sgVirtualMotorsEnter(Sender: TObject);
begin
  ScrollPostion:=Scrollbox1.VertScrollBar.Position;
  EnterSGFlag:=true;
end;

procedure TMNI2ConfigFm.sgVirtualMotorsExit(Sender: TObject);
var
  I:Integer;
begin
  if(sgVirtualMotors.cells[LastCol,LastRow]='')then
  begin
    sgVirtualMotors.cells[LastCol,LastRow]:='Disabled';
  end
  else
  begin
    for I := 1 to 20 do
      begin
        if (I<>LastRow)and(sgVirtualMotors.cells[2,I]<>'Disabled') then
        begin
          if (sgVirtualMotors.cells[2,LastRow]=sgVirtualMotors.cells[2,I]) and (sgVirtualMotors.cells[2,I]<>'Disabled') then
          begin
            Showmessage('Duplicate Node UIDs are not allowed');
            sgVirtualMotors.cells[LastCol,LastRow]:=LastCellStr;
            break;
          end;
          if (sgVirtualMotors.cells[1,LastRow]=sgVirtualMotors.cells[1,I]) and (sgVirtualMotors.cells[2,I]<>'Disabled') and (sgVirtualMotors.cells[2,LastRow]<>'Disabled')then
          begin
            Showmessage('Duplicate MechoNet UIDs are not allowed');
            sgVirtualMotors.cells[LastCol,LastRow]:='Disabled';
            break;
          end;

        end;
      end;
  end;


end;

procedure TMNI2ConfigFm.sgVirtualMotorsKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_DELETE then
  begin
    btUpdateParams.Color:=cllime;
  end;
  if ((ssCtrl in Shift) and (Key = 86))or ((ssShift in Shift) and (Key = 45)) then
  begin
    if sgVirtualMotors.Col=2 then
    begin
      if Clipboard.HasFormat(CF_Text) then
      begin
        if isStrHex(Clipboard.AsText) then
        begin
          sgVirtualMotors.Cells[sgVirtualMotors.col,sgVirtualMotors.Row]:= UpperCase(Clipboard.AsText);
        end
        else
        begin
          ShowMessage('Invalid hex value');
        end;
      end;
    end
    else
    begin
      if Clipboard.HasFormat(CF_Text) then
      begin
        if isnumber(Clipboard.AsText) then
        begin
          if StrToInt(Clipboard.AsText)<>0 then
          begin
            if StrToInt(Clipboard.AsText)<65535 then
            begin
              sgVirtualMotors.Cells[sgVirtualMotors.col,sgVirtualMotors.Row]:= Clipboard.AsText;
            end
            else
            begin
              ShowMessage('Invalid UID: value cannot be > 65535');
            end;
          end
          else
          begin
            ShowMessage('Invalid UID: value cannot = 0');
          end;

        end
        else
        begin
          ShowMessage('Invalid UID: must be number');
        end;
      end;
    end;
  end;
end;
Function TMNI2ConfigFm.isStrhex(s:String):boolean;
var
  i:Integer;
begin
  for i := 1 to Length(s) do
  begin
    if not (s[i] in ['0'..'9','A'..'F','a'..'f']) then
    begin
      Result := False;
      Exit;
    end;
  end;
  Result := true;
end;

procedure TMNI2ConfigFm.JvCaptionButton1Click(Sender: TObject);
begin
  Application.HelpContext(9);
end;

procedure TMNI2ConfigFm.sgVirtualMotorsKeyPress(Sender: TObject; var Key: Char);
var
  SelTextLength : integer;
begin
  //btUpdateParams.Color:=cllime;
  flgIDChange:=true;
  if (sgVirtualMotors.Col=1) then
  begin
    SelTextLength := TIPEGrid(sgVirtualMotors).InPlaceEditor.SelLength;

    if (Key in ['0'..'9'])then
    begin
      if sgVirtualMotors.Cells[sgVirtualMotors.Col,sgVirtualMotors.Row]='Disabled' then
      begin
        sgVirtualMotors.Cells[sgVirtualMotors.Col,sgVirtualMotors.Row]:='';
      end;
      if (SelTextLength<=5)and(SelTextLength>=1) then
      begin
        sgVirtualMotors.Cells[sgVirtualMotors.Col,sgVirtualMotors.Row]:='';
      end
      else if StrToInt(sgVirtualMotors.Cells[sgVirtualMotors.Col,sgVirtualMotors.Row]+key)>65535 then
      begin
        // Discard the key
        Key := #0;
        ShowMessage('Error: Mechonet ID cannot Exceed 65535');
      end;
//      else if StrToInt(sgVirtualMotors.Cells[sgVirtualMotors.Col,sgVirtualMotors.Row]+key)=65535 then
//      begin
//        Key := #0;
//        sgVirtualMotors.Cells[sgVirtualMotors.Col,sgVirtualMotors.Row]:='Disabled';
//      end;

    end
    else if not (Key = #8) then
    begin
      Key := #0;
    end;
    if key<>#0 then
    begin
      btUpdateParams.Color:=cllime;
    end;
//    if not (Key = #8) then
//    begin
//      if(sgVirtualMotors.Cells[sgVirtualMotors.Col,sgVirtualMotors.Row].Length>=5) then
//      begin
//        Key := #0;
//      end;
//    end;
  end
  else if (sgVirtualMotors.Col=2) then
  begin
    if not (Key in [#8, '0'..'9','A'..'F','a'..'f'])then
    begin
      // Discard the key
      Key := #0;
      exit;
    end;
    btUpdateParams.Color:=cllime;
    if not (Key = #8) then
    begin
      if TIPEGrid(sgVirtualMotors).InPlaceEditor.SelLength=6 then
      begin
        sgVirtualMotors.Cells[sgVirtualMotors.Col,sgVirtualMotors.Row]:='';
        Key:=upcase(Key);
      end
      else if(sgVirtualMotors.Cells[sgVirtualMotors.Col,sgVirtualMotors.Row].Length>=6) then
      begin
        Key := #0;
      end
      else
      begin
        Key:=upcase(Key);
      end;
    end;
  end
  else
  begin
    Key := #0;
  end;


end;

procedure TMNI2ConfigFm.sgVirtualMotorsMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
  var
  I: Integer;
begin
  scrollbox1.SetFocus;
  Handled := PtInRect(ScrollBox1.ClientRect, ScrollBox1.ScreenToClient(MousePos));
  if Handled then
    for I := 1 to Mouse.WheelScrollLines do
    try
      ScrollBox1.Perform(WM_VSCROLL, SB_LINEDOWN, 0);
    finally
      ScrollBox1.Perform(WM_VSCROLL, SB_ENDSCROLL, 0);
    end;
end;

procedure TMNI2ConfigFm.sgVirtualMotorsMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
var
  I: Integer;
begin
  scrollbox1.SetFocus;
  Handled := PtInRect(ScrollBox1.ClientRect, ScrollBox1.ScreenToClient(MousePos));
  if Handled then
  begin
    for I := 1 to Mouse.WheelScrollLines do
    try
        ScrollBox1.Perform(WM_VSCROLL, SB_LINEUP, 0)
    finally
      ScrollBox1.Perform(WM_VSCROLL, SB_ENDSCROLL, 0);
    end;
  end;
end;

procedure TMNI2ConfigFm.sgVirtualMotorsSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
var
  I: Integer;
begin
  if EnterSGFlag then
  begin
    Scrollbox1.VertScrollBar.Position:=ScrollPostion;
    EnterSGFlag:=false;
  end;
  if(sgVirtualMotors.cells[ACol,ARow]='Disabled')then
  begin
    sgVirtualMotors.cells[ACol,ARow]:='';
  end;


  if LastCol=2 then
  begin

    if(sgVirtualMotors.cells[LastCol,LastRow]='')then
    begin
      sgVirtualMotors.cells[LastCol,LastRow]:='Disabled';
    end
    else// if sgVirtualMotors.cells[ACol,Arow]<>'' then
    begin
      for I := 1 to 20 do
      begin
        if (I<>LastRow)and(sgVirtualMotors.cells[2,I]<>'Disabled') then
        begin
          if (sgVirtualMotors.cells[2,LastRow]=sgVirtualMotors.cells[2,I]) and (sgVirtualMotors.cells[2,I]<>'Disabled') then
          begin
            Showmessage('Duplicate Node UIDs are not allowed');
            sgVirtualMotors.cells[LastCol,LastRow]:=LastCellStr;
            break;
          end;
          if (sgVirtualMotors.cells[1,LastRow]=sgVirtualMotors.cells[1,I]) and (sgVirtualMotors.cells[1,I]<>'Disabled') then
          begin
            Showmessage('Duplicate MechoNet UIDs are not allowed');
            sgVirtualMotors.cells[LastCol,LastRow]:='Disabled';
            break;
          end;

        end;
      end;
    end;


  end;
  if LastCol=1 then
  begin
    if sgVirtualMotors.cells[LastCol,LastRow]<>'Disabled' then
    begin
      if sgVirtualMotors.cells[LastCol,LastRow]='' then
      begin
        sgVirtualMotors.cells[LastCol,LastRow]:='Disabled';
      end
      else if StrToInt(sgVirtualMotors.cells[LastCol,LastRow])>65535 then
      begin
        ShowMessage('UIDs can not be larger than 65535');
        sgVirtualMotors.cells[LastCol,LastRow]:=LastCellStr;
      end;
    end;
//    if(sgVirtualMotors.cells[LastCol,LastRow]='')then
//    begin
//      sgVirtualMotors.cells[LastCol,LastRow]:=LastCellStr;
//    end
    if (sgVirtualMotors.cells[2,LastRow]<>'Disabled') and (sgVirtualMotors.cells[1,LastRow]<>'Disabled') then
    begin

      for I := 1 to 20 do
      begin
        if I<>LastRow then
        begin
          if (sgVirtualMotors.cells[LastCol,LastRow]=sgVirtualMotors.cells[1,I]) and (sgVirtualMotors.cells[1,I]<>'Disabled') then
          begin
            Showmessage('Duplicate MechoNet UIDs are not allowed');
            if LastCellStr='' then
            begin
              sgVirtualMotors.cells[LastCol,LastRow]:='Disabled';
            end
            else
            begin
              sgVirtualMotors.cells[LastCol,LastRow]:=LastCellStr;
            end;
            break;
          end;

        end;

      end;
    end;
  end;
  LastCellStr:=sgVirtualMotors.cells[ACol,ARow];
  LastCol:=Acol;
  LastRow:=ARow;

end;

procedure TMNI2ConfigFm.teSWFWDAddrChange(Sender: TObject);
begin
  teSWFWDAddr.color:=cllime;
  btUpdateParams.Color:=cllime;
end;

procedure TMNI2ConfigFm.teTravelChange(Sender: TObject);
begin
  shpteTravel.Visible:=true;
  btUpdateParams.Color:=cllime;
end;

procedure TMNI2ConfigFm.teTravelExit(Sender: TObject);
begin

  if Trim(teTravel.Text)='' then
  begin
    teTravel.Text:='0.1';
  end
  else if StrToFloat(teTravel.Text)>25.5 then
  begin
    ShowMessage('Travel value cannot exceed 25.5%');
    teTravel.Text:='25.5'
  end
  else if StrToFloat(teTravel.Text)< 0.1 then
  begin
    ShowMessage('Travel value cannot be lower than 0.1%');
    teTravel.Text:='0.1';
  end;
  teTravel.Text:=FloatToStr(RoundTo(StrToFloat(teTravel.Text),-1));
end;

procedure TMNI2ConfigFm.teTravelKeyPress(Sender: TObject; var Key: Char);
begin
  if not (Key in [#8, '0'..'9','.'])then
  begin
    // Discard the key
    Key := #0;
  end;

end;

procedure TMNI2ConfigFm.teUIDChange(Sender: TObject);
begin
  teUID.color:=cllime;
  btUpdateUID.Color:=clLime;
end;

procedure TMNI2ConfigFm.teUIDExit(Sender: TObject);
begin
  if Length(teUID.Text)=5 then
  begin
    if StrToInt(teUID.Text)>65535 then
    begin
      Showmessage('Value cannot exceed 65535');
      teUID.SetFocus;
    end;
  end;
end;

procedure TMNI2ConfigFm.teUIDKeyPress(Sender: TObject; var Key: Char);
begin
  if not (Key in [#8, '0'..'9'])then
  begin
    // Discard the key
    Key := #0;
  end;
end;

procedure TMNI2ConfigFm.txtAliasChange(Sender: TObject);
begin
  txtAlias.Brush.Color:=cllime;
  btUpdateParams.Color:=cllime;
end;

procedure TMNI2ConfigFm.txtAliasDblClick(Sender: TObject);
begin
  NewProjectFm.rh_ShowLast:=rhShowMNI2;
  NewProjectFm.Show;
  Hide;
end;

procedure TMNI2ConfigFm.txtAliasEnterKey(Sender: TObject);
var
  vsAlias,vsCommand:String;
  I:Integer;
begin
    try
    if VariablesUn.IsNumber(cmbUID.text) then
    begin
      if vsOldAlias<>trim(txtAlias.text) then
      begin

        vsOldAlias:=trim(txtAlias.text);
        giUID :=  StrToInt(cmbUID.text);
        vsAlias:=txtAlias.Text;
        vsAlias := vsAlias + StringOfChar ( Char(#0), 12 - Length(vsAlias) );
        vsCommand:=VariablesUn.UID_NAME+'.'+vsAlias;
        ComFm.SetGenericParam(giUID,vsCommand, nil);
        Delay(200);


        DataUn.dmDataModule.dsMNI2.Filtered := False;
        DataUn.dmDataModule.dsMNI2.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt;
        DataUn.dmDataModule.dsMNI2.Filtered := True;
        if not (DataUn.dmDataModule.dsMNI2.Eof) then
        begin
          DataUn.dmDataModule.dsMNI2.Edit;
          DataUn.dmDataModule.dsMNI2.FieldByName('Alias').AsString := txtAlias.Text;
          DataUn.dmDataModule.dsMNI2.Post;
          DataUn.dmDataModule.dsMNI2.MergeChangeLog;
          DataUn.dmDataModule.dsMNI2.SaveToFile(VariablesUn.gsDataPath + 'dsMNI.txt', dfXML);

        end;
      end;
    end
    else
    begin
      txtAlias.Text := '';
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.lblAliasChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  End;

end;

procedure TMNI2ConfigFm.txtAliasKeyPress(Sender: TObject; var Key: Char);
begin
//  if key = #$D then
//  begin
//    txtAliasEnterKey(self);
//  end;
end;

procedure TMNI2ConfigFm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  btnExitClick(self);
end;

procedure TMNI2ConfigFm.FormCreate(Sender: TObject);
begin
  UIDList:=TStringList.Create;
  SomfyIDList:=TStringList.Create;
  ComboBox_AutoWidth(cmbDeviceSelector);
end;

//Allows combox  dropdown to size to oversized string items
procedure TMNI2ConfigFm.ComboBox_AutoWidth(const theComboBox: TCombobox);
const
  HORIZONTAL_PADDING = 4;
var
  itemsFullWidth: integer;
  idx: integer;
  itemWidth: integer;
begin
  itemsFullWidth := 0;
  // get the max needed with of the items in dropdown state
  for idx := 0 to -1 + theComboBox.Items.Count do
  begin
    itemWidth := theComboBox.Canvas.TextWidth(theComboBox.Items[idx]);
    Inc(itemWidth, 2 * HORIZONTAL_PADDING);
    if (itemWidth > itemsFullWidth) then itemsFullWidth := itemWidth;
  end;
  itemsFullWidth:=itemsFullWidth+20;
    // set the width of drop down if needed
  if (itemsFullWidth > theComboBox.Width) then
  begin
    //check if there would be a scroll bar
    if theComboBox.DropDownCount < theComboBox.Items.Count then
      itemsFullWidth := itemsFullWidth + GetSystemMetrics(SM_CXVSCROLL);
    SendMessage(theComboBox.Handle, CB_SETDROPPEDWIDTH, itemsFullWidth, 0);
  end;
end;

Procedure TMNI2ConfigFm.FormHide(Sender: TObject);
begin
  VariablesUn.giTop:=top;
  VariablesUn.giLeft:=left;
  flgIDChange:=False;
end;

procedure TMNI2ConfigFm.FormMouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
var
  I: Integer;
begin
  scrollbox1.SetFocus;
  Handled := PtInRect(ScrollBox1.ClientRect, ScrollBox1.ScreenToClient(MousePos));
  if Handled then
    for I := 1 to Mouse.WheelScrollLines do
    try
      if WheelDelta > 0 then
        ScrollBox1.Perform(WM_VSCROLL, SB_LINEUP, 0)
      else
        ScrollBox1.Perform(WM_VSCROLL, SB_LINEDOWN, 0);
    finally
      ScrollBox1.Perform(WM_VSCROLL, SB_ENDSCROLL, 0);
    end;

end;

procedure TMNI2ConfigFm.FormShow(Sender: TObject);
var
  I: Integer;
begin
  try
    flgIDChange:=False;
    sgVirtualMotors.cells[0,0]:='#';
    sgVirtualMotors.cells[1,0]:='UID';
    sgVirtualMotors.cells[2,0]:='Node ID';
    if VariablesUn.gbShowSTA = true then
    begin
      btnLink.Visible:=true;
      teLinkCount.Visible:=true;
      lblLinked.Visible:=true;
    end;

    for I := 1 to 20 do
    begin
      sgVirtualMotors.cells[0,I]:=IntToStr(I);
    end;
    cmbDeviceSelector.ItemIndex:=cmbDeviceSelector.Items.IndexOf('MDC');
    LoadProjects;
    LoadUID;
    LoadData;
    LastUID:=StrToInt(cmbUID.Text);
    loadFlag:=true;
    LastRow:=1;
    LastCol:=1;
    top:=VariablesUn.giTop;
    left:=VariablesUn.giLeft;
    vsOldAlias:=txtAlias.Text;
  except
    on E: Exception do
    begin
      MessageDlg('TMNI2ConfigFm.FormShow: ' + E.Message, mtWarning, [mbOk], 0);
      //ZGNMoveUp := false;
    end;
  End;
end;

Procedure TMNI2ConfigFm.LoadUID();
var
  I:Integer;
  comboUIDList:TStringList;
begin
  comboUIDList:=TStringList.Create;
  cmbUid.Clear;
  begin
    if not (DataUn.dmDataModule.dsMNI2.State = dsInactive) then
    begin
      DataUn.dmDataModule.dsMNI2.Filtered := False;
      DataUn.dmDataModule.dsMNI2.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
      DataUn.dmDataModule.dsMNI2.Filtered := True;
    end;
    while not dmDataModule.dsMNI2.Eof do
    begin
     //cmbUid.Items.Add(IntToStr(DataUn.dmDataModule.dsMNI2.FieldByName('UID').AsInteger));
     comboUIDList.Add(IntToStr(DataUn.dmDataModule.dsMNI2.FieldByName('UID').AsInteger));
     dmDataModule.dsMNI2.Next;
    end;
    SortStringListUIDS(comboUIDList);
    cmbUid.items.assign(comboUIDList);
    cmbUid.ItemIndex:=0;
  end;
//  cmbUidChange(nil);
end;

procedure TMNI2ConfigFm.pnlPushDataClick(Sender: TObject);
begin
  VariablesUn.ClearData;
  //giSentCom := StrToInt(VariablesUn.MNI2_PushData);
  screen.cursor:=crHourglass;
  sgVirtualMotors.Enabled:=false;
  cmbProject.enabled:=false;
  txtAlias.enabled:=false;
  imgMenu.enabled:=false;
  cmbUID.enabled:=false;
  teUID.enabled:=false;
  btUpdateUID.enabled:=false;
  btUpdateParams.enabled:=false;
  cmbDeviceSelector.enabled:=false;
  pnlPushData.Enabled:=false;
  //MNI2ConfigFM.Enabled:=false;


  if cmbMotorSelector.Text='ALL' then
  begin
    ComFm.SetGenericParam(StrToInt(cmbUid.Text), VariablesUn.MDC_PushData+'.0.255', rhMsgRecieved);
    if not WaitforGenericResponseConfirmation(StrToInt(cmbUid.Text), VariablesUn.MDC_PushData+'.0.255', rhMsgRecieved,4000) then
    begin
      ShowMessage('Failed: Push All Data');

    end;
  end
  else
  begin
    ComFm.SetGenericParam(StrToInt(cmbUid.Text), VariablesUn.MDC_PushData+'.0.'+IntToStr(StrToInt(cmbMotorSelector.text)-1), rhMsgRecieved);
    if not WaitforGenericResponseConfirmation(StrToInt(cmbUid.Text), VariablesUn.MDC_PushData+'.0.'+IntToStr(StrToInt(cmbMotorSelector.text)-1), rhMsgRecieved,4000) then
    begin
      ShowMessage('Failed: Push All '+cmbMotorSelector.text);

    end;
  end;
  //MNI2ConfigFM.Enabled:=True;
  pnlPushData.Enabled:=True;
  cmbDeviceSelector.enabled:=True;
  btUpdateParams.enabled:=True;
  btUpdateUID.enabled:=True;
  teUID.enabled:=True;
  imgMenu.enabled:=True;
  cmbUID.enabled:=True;
  txtAlias.enabled:=True;
  sgVirtualMotors.Enabled:=true;
  cmbProject.enabled:=true;
  screen.cursor:=crDefault;
end;

Procedure TMNI2ConfigFm.LoadProjects;
var
  I:Integer;
begin
  cmbProject.Items.clear;
  //Polpulate project if it exists
  if DataUn.dmDataModule.dsProject.State <> dsInactive then
  begin
    DataUn.dmDataModule.dsProject.Filtered := False;
    DataUn.dmDataModule.dsProject.First;
    while not DataUn.dmDataModule.dsProject.eof do
    begin
      if (DataUn.dmDataModule.dsProject.FieldByName('Last').AsInteger = 1) then
         cmbProject.Items.Insert(0, DataUn.dmDataModule.dsProject.FieldByName('ProjectName').AsString)
      else
         cmbProject.Items.Add(DataUn.dmDataModule.dsProject.FieldByName('ProjectName').AsString);

      DataUn.dmDataModule.dsProject.Next;
    end;
    DataUn.dmDataModule.dsProject.Filtered := False;
    DataUn.dmDataModule.dsProject.First;
    for I := 0 to cmbProject.Items.Count-1 do
    begin
      if cmbproject.text = VariablesUn.gsProjectName then
      begin
        break;
      end
      else
      begin
        cmbproject.ItemIndex:=I;
      end;
    end;
  End;
end;


procedure TMNI2ConfigFm.btnSettingsClick(Sender: TObject);
begin
  VariablesUn.gitop:=top;
  VariablesUn.giLeft:=left;
  ScreenSetupFm.rh_showmain := rhShowMNI2;
  pnlMenuBar.Visible := False;
  Hide;
  ScreenSetupFm.Show;
end;

procedure TMNI2ConfigFm.btnSettingsMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TMNI2ConfigFm.imgMenuMouseEnter(Sender: TObject);
begin
  if imgMenu.enabled then
  Begin
    pnlMenuBar.Visible := True;
  End;
end;

procedure TMNI2ConfigFm.imgRefreshClick(Sender: TObject);
begin
  GetVirtualMotorsStatus(self);
  LastUID:=StrToInt(cmbUid.text);
  LoadData;
end;

procedure TMNI2ConfigFm.pnlMenuBarMouseLeave(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

end.



