unit DiscoverUn;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, ComUn, VariablesUn,
  Vcl.Buttons, Vcl.ComCtrls, DataUn, Datasnap.DBClient, Data.DB, Vcl.Tabs;

type
  TDiscoverFm = class(TForm)
    imgMenu: TImage;
    pnlLights: TPanel;
    Shape4: TShape;
    Shape3: TShape;
    Shape2: TShape;
    Shape5: TShape;
    Shape1: TShape;
    lblProjectName: TLabel;
    lblAlias: TLabel;
    btnDiscover: TSpeedButton;
    pbDisplayDelay: TProgressBar;
    tmDelay: TTimer;
    cmbUID1: TComboBox;
    lblUIDA: TLabel;
    cmbUID2: TComboBox;
    lblUIDB: TLabel;
    pnlMenuBar: TPanel;
    Image3: TImage;
    btnExit: TSpeedButton;
    btnFind: TSpeedButton;
    btnMain: TSpeedButton;
    btnConfig: TSpeedButton;
    btnHelp: TSpeedButton;
    btnInfo: TSpeedButton;
    cmbZGN1: TComboBox;
    cmbZGN2: TComboBox;
    lblZGNA: TLabel;
    lblZGNB: TLabel;
    btnSave: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnDiscoverClick(Sender: TObject);
    procedure tmDelayTimer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure imgMenuMouseEnter(Sender: TObject);
    procedure btnFindClick(Sender: TObject);
    procedure pnlMenuBarMouseEnter(Sender: TObject);
    procedure pnlMenuBarMouseLeave(Sender: TObject);
    procedure btnExitMouseEnter(Sender: TObject);
    procedure btnFindMouseEnter(Sender: TObject);
    procedure btnMainClick(Sender: TObject);
    procedure btnMainMouseEnter(Sender: TObject);
    procedure cmbUID1Change(Sender: TObject);
    procedure cmbUID2Change(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure btnConfigClick(Sender: TObject);
    procedure btnHelpClick(Sender: TObject);
    procedure btnInfoClick(Sender: TObject);
    procedure FormClick(Sender: TObject);
  private
    { Private declarations }
    giDspDelay: Integer;
    giComPortOn: Boolean;
    giSentCom: Integer;
    giUID: Integer;
    gbSaveProject: Boolean;
    procedure rhUIDRecieved(sender: TObject);
    procedure PopulateDataset;
    procedure rhStatusRecieved(sender: TObject);
    procedure GetStatus;
    procedure ClearData;
    procedure PopulateZGN;
  public
    { Public declarations }
    rh_ShowMain: TNotifyEvent;
    procedure rhPortClosed(Sender: TObject);
    procedure rhPortOpend(Sender: TObject);
  end;

var
  DiscoverFm: TDiscoverFm;

implementation

{$R *.dfm}

procedure TDiscoverFm.btnSaveClick(Sender: TObject);
begin
  DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
  gbSaveProject := False;
  lblProjectName.Color := clWhite;
end;

procedure TDiscoverFm.ClearData;
begin
  VariablesUn.giDataH := -1;                                             // High byte
  VariablesUn.giDataM := -1;                                             // Middle byte
  VariablesUn.giDataL := -1;                                             // Low byte
  VariablesUn.giUID_rx := -1;                                            // UID;
  VariablesUn.giCommand := -1;                                           // Command
end;

procedure TDiscoverFm.cmbUID1Change(Sender: TObject);
begin
  PopulateZGN;
end;

procedure TDiscoverFm.cmbUID2Change(Sender: TObject);
begin
  PopulateZGN
end;

procedure TDiscoverFm.FormClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TDiscoverFm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  rh_ShowMain := nil;
  //ComUn.dmComModule.rh_PortOpendDisc := nil;
  //ComUn.dmComModule.rh_PortClosedDisc := nil;
end;

procedure TDiscoverFm.FormCreate(Sender: TObject);
begin
  pbDisplayDelay.Max := 150;
  pbDisplayDelay.Step := 1;
  giComPortOn := False;
  gbSaveProject := False;
end;

procedure TDiscoverFm.FormHide(Sender: TObject);
begin
  VariablesUn.giTop := Top;
  VariablesUn.giLeft := Left;
end;

procedure TDiscoverFm.FormShow(Sender: TObject);
begin
  Top := VariablesUn.giTop;
  Left := VariablesUn.giLeft;

  pnlMenuBar.Visible := False;
  lblProjectName.Caption := VariablesUn.gsProjectName;
  cmbUID1.Clear;
  cmbUID2.Clear;
  cmbZGN1.Clear;
  cmbZGN2.Clear;
  cmbUID1.Enabled := False;
  cmbUID2.Enabled := False;
  cmbZGN1.Enabled := False;
  cmbZGN2.Enabled := False;
  btnSave.Enabled := False;
  gbSaveProject := False;
  lblProjectName.Color := clWhite;

  if giComPortOn then
    btnDiscover.Click;
end;

procedure TDiscoverFm.GetStatus;
var vsHi, vsLo, vsCommand: String;
    i: Integer;
begin
  try
    if cmbUID1.Items.Count > 0 then
    begin
      for i := 0 to cmbUID1.Items.Count - 1 do
      begin
        cmbUID1.ItemIndex := i;
        cmbUID2.ItemIndex := i;
        giUID :=  StrToInt(cmbUID1.Text);

        if (DataUn.dmDataModule.dsMotors.State = dsEdit) then
        begin
          DataUn.dmDataModule.dsMotors.Post;
          dmDataModule.dsMotors.MergeChangeLog;
        end;

        DataUn.dmDataModule.dsMotors.Filtered := False;
        DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + cmbUID1.Text;
        DataUn.dmDataModule.dsMotors.Filtered := True;
        if not dmDataModule.dsMotors.Eof then
        begin
          vsHi := IntToStr(Hi(StrToInt(cmbUID1.Text)));
          vsLo := IntToStr(Lo(StrToInt(cmbUID1.Text)));
          dmDataModule.dsMotors.Edit;
          {
          ClearData;
          giSentCom := StrToInt(VariablesUn.UID_TYPE);
          vsCommand := '?' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_TYPE + '.0.0.1';
          vsCommand := vsCommand + #13;
          ComFm.SendCommand(vsCommand, rhStatusRecieved);
          VariablesUn.delay(200);

          ClearData;
          giSentCom := StrToInt(VariablesUn.UID_NAME);
          vsCommand := '?' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_NAME + '.0.0.1';
          vsCommand := vsCommand + #13;
          ComFm.SendCommand(vsCommand, rhStatusRecieved);
          VariablesUn.delay(200);

          ClearData;
          giSentCom := StrToInt(VariablesUn.UID_IQMotorSet);
          vsCommand := '?' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_IQMotorSet + '.0.0.1';
          vsCommand := vsCommand + #13;
          ComFm.SendCommand(vsCommand, rhStatusRecieved);
          VariablesUn.delay(200);
          }
          ClearData;
          giSentCom := StrToInt(VariablesUn.UID_LS1_ADDR);
          vsCommand := '?' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_LS1_ADDR + '.0.0.1';
          vsCommand := vsCommand + #13;
          ComFm.SendCommand(vsCommand, rhStatusRecieved);
          VariablesUn.delay(200);

          ClearData;
          giSentCom := StrToInt(VariablesUn.UID_LS1_BUS1);
          vsCommand := '?' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_LS1_BUS1 + '.0.0.1';
          vsCommand := vsCommand + #13;
          ComFm.SendCommand(vsCommand, rhStatusRecieved);
          VariablesUn.delay(200);

          ClearData;
          giSentCom := StrToInt(VariablesUn.UID_LS1_BUS2);
          vsCommand := '?' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_LS1_BUS2 + '.0.0.1';
          vsCommand := vsCommand + #13;
          ComFm.SendCommand(vsCommand, rhStatusRecieved);
          VariablesUn.delay(200);

          ClearData;
          giSentCom := StrToInt(VariablesUn.UID_LS1_BUS3);
          vsCommand := '?' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_LS1_BUS3 + '.0.0.1';
          vsCommand := vsCommand + #13;
          ComFm.SendCommand(vsCommand, rhStatusRecieved);
          VariablesUn.delay(200);

          ClearData;
          giSentCom := StrToInt(VariablesUn.UID_LS1_BUS4);
          vsCommand := '?' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_LS1_BUS4 + '.0.0.1';
          vsCommand := vsCommand + #13;
          ComFm.SendCommand(vsCommand, rhStatusRecieved);
          VariablesUn.delay(200);

          ClearData;
          giSentCom := StrToInt(VariablesUn.UID_LS1_BUS5);
          vsCommand := '?' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_LS1_BUS5 + '.0.0.1';
          vsCommand := vsCommand + #13;
          ComFm.SendCommand(vsCommand, rhStatusRecieved);
          VariablesUn.delay(200);

          ClearData;
          giSentCom := StrToInt(VariablesUn.UID_LS1_BUS6);
          vsCommand := '?' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_LS1_BUS6 + '.0.0.1';
          vsCommand := vsCommand + #13;
          ComFm.SendCommand(vsCommand, rhStatusRecieved);
          VariablesUn.delay(200);

          ClearData;
          giSentCom := StrToInt(VariablesUn.UID_LS1_BUS7);
          vsCommand := '?' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_LS1_BUS7 + '.0.0.1';
          vsCommand := vsCommand + #13;
          ComFm.SendCommand(vsCommand, rhStatusRecieved);
          VariablesUn.delay(200);

          ClearData;
          giSentCom := StrToInt(VariablesUn.UID_LS1_BUS8);
          vsCommand := '?' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_LS1_BUS8 + '.0.0.1';
          vsCommand := vsCommand + #13;
          ComFm.SendCommand(vsCommand, rhStatusRecieved);
          VariablesUn.delay(200);
          {
          ClearData;
          giSentCom := StrToInt(VariablesUn.UID_BLCKOUT);
          vsCommand := '?' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_BLCKOUT + '.0.0.1';
          vsCommand := vsCommand + #13;
          ComFm.SendCommand(vsCommand, rhStatusRecieved);
          VariablesUn.delay(200);

          ClearData;
          giSentCom := StrToInt(VariablesUn.UID_BLCKOUT_ADR);
          vsCommand := '?' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_BLCKOUT_ADR + '.0.0.1';
          vsCommand := vsCommand + #13;
          ComFm.SendCommand(vsCommand, rhStatusRecieved);
          VariablesUn.delay(200);

          ClearData;
          giSentCom := StrToInt(VariablesUn.UID_FR);
          vsCommand := '?' + vsHi + '.' + vsLo + '.' + VariablesUn.UID_FR + '.0.0.1';
          vsCommand := vsCommand + #13;
          ComFm.SendCommand(vsCommand, rhStatusRecieved);
          }
        end;
      end;
     end;
  except
  on E: Exception do
  begin
    imgMenu.OnMouseEnter := imgMenuMouseEnter;
    dmDataModule.dsMotors.Cancel;
    Screen.Cursor := crDefault;
    MessageDlg('TDiscoverFm.GetStatus: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;

procedure TDiscoverFm.imgMenuMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TDiscoverFm.pnlMenuBarMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TDiscoverFm.pnlMenuBarMouseLeave(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TDiscoverFm.PopulateDataset;
var viProjectId, i: Integer;
begin
  try
    if DataUn.dmDataModule.dsMotors.State = dsInactive then
    begin
      DataUn.dmDataModule.dsMotors.CreateDataSet;
      DataUn.dmDataModule.dsMotors.Active := True;
    end;

    DataUn.dmDataModule.dsMotors.Filtered := False;
    DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsMotors.Filtered := True;

    while not dmDataModule.dsMotors.Eof do
    begin
      dmDataModule.dsMotors.Edit;
      DataUn.dmDataModule.dsMotors.Delete;
    end;
    dmDataModule.dsMotors.MergeChangeLog;
    DataUn.dmDataModule.dsMotors.Filtered := False;

    for i := 0 to cmbUID1.Items.Count - 1 do
    begin
       DataUn.dmDataModule.dsMotors.Insert;
       DataUn.dmDataModule.dsMotors.FieldByName('ProjectName').AsString := VariablesUn.gsProjectName;
       DataUn.dmDataModule.dsMotors.FieldByName('UID').AsInteger := StrToInt(cmbUID1.Items[i]);
       DataUn.dmDataModule.dsMotors.FieldByName('Address1').AsString := '-1';
       DataUn.dmDataModule.dsMotors.FieldByName('Address2').AsString := '-1';
       DataUn.dmDataModule.dsMotors.FieldByName('Address3').AsString := '-1';
       DataUn.dmDataModule.dsMotors.FieldByName('Address4').AsString := '-1';
       DataUn.dmDataModule.dsMotors.FieldByName('Address5').AsString := '-1';
       DataUn.dmDataModule.dsMotors.FieldByName('Address6').AsString := '-1';
       DataUn.dmDataModule.dsMotors.FieldByName('Address7').AsString := '-1';
       DataUn.dmDataModule.dsMotors.FieldByName('Address8').AsString := '-1';
       DataUn.dmDataModule.dsMotors.FieldByName('BlackOut').AsString := '-1';
       DataUn.dmDataModule.dsMotors.FieldByName('LVPOrt').AsString := '-1';
       DataUn.dmDataModule.dsMotors.Post;
       gbSaveProject := True;
    end;
  except
  on E: Exception do
  begin
    imgMenu.OnMouseEnter := imgMenuMouseEnter;
    Screen.Cursor := crDefault;
    MessageDlg('TDiscoverFm.PopulateDataset: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;

procedure TDiscoverFm.PopulateZGN;
var i: Integer;
begin
  try
  cmbZGN1.Clear;
  cmbZGN2.Clear;

  if (cmbUID1.ItemIndex <> -1) then
  begin
    DataUn.dmDataModule.dsMotors.Filtered := False;
    DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + cmbUID1.Text;
    DataUn.dmDataModule.dsMotors.Filtered := True;
    if not dmDataModule.dsMotors.Eof then
    begin
      cmbZGN1.Items.Add(DataUn.dmDataModule.dsMotors.FieldByName('Address1').AsString);
      cmbZGN1.Items.Add(DataUn.dmDataModule.dsMotors.FieldByName('Address2').AsString);
      cmbZGN1.Items.Add(DataUn.dmDataModule.dsMotors.FieldByName('Address3').AsString);
      cmbZGN1.Items.Add(DataUn.dmDataModule.dsMotors.FieldByName('Address4').AsString);
      cmbZGN1.Items.Add(DataUn.dmDataModule.dsMotors.FieldByName('Address5').AsString);
      cmbZGN1.Items.Add(DataUn.dmDataModule.dsMotors.FieldByName('Address6').AsString);
      cmbZGN1.Items.Add(DataUn.dmDataModule.dsMotors.FieldByName('Address7').AsString);
      cmbZGN1.Items.Add(DataUn.dmDataModule.dsMotors.FieldByName('Address8').AsString);
      cmbZGN1.Items.Add(DataUn.dmDataModule.dsMotors.FieldByName('LVPOrt').AsString);
    end;
  end;
  if (cmbUID2.ItemIndex <> -1) then
  begin
    DataUn.dmDataModule.dsMotors.Filtered := False;
    DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + cmbUID2.Text;
    DataUn.dmDataModule.dsMotors.Filtered := True;
    if not dmDataModule.dsMotors.Eof then
    begin
      cmbZGN2.Items.Add(DataUn.dmDataModule.dsMotors.FieldByName('Address1').AsString);
      cmbZGN2.Items.Add(DataUn.dmDataModule.dsMotors.FieldByName('Address2').AsString);
      cmbZGN2.Items.Add(DataUn.dmDataModule.dsMotors.FieldByName('Address3').AsString);
      cmbZGN2.Items.Add(DataUn.dmDataModule.dsMotors.FieldByName('Address4').AsString);
      cmbZGN2.Items.Add(DataUn.dmDataModule.dsMotors.FieldByName('Address5').AsString);
      cmbZGN2.Items.Add(DataUn.dmDataModule.dsMotors.FieldByName('Address6').AsString);
      cmbZGN2.Items.Add(DataUn.dmDataModule.dsMotors.FieldByName('Address7').AsString);
      cmbZGN2.Items.Add(DataUn.dmDataModule.dsMotors.FieldByName('Address8').AsString);
      cmbZGN2.Items.Add(DataUn.dmDataModule.dsMotors.FieldByName('LVPOrt').AsString);
    end;
  end;
  if cmbZGN1.Items.Count > 0 then
  begin
    cmbZGN1.ItemIndex := 0;
    cmbZGN1.Enabled := True;
  end;

  if cmbZGN2.Items.Count > 0 then
  begin
    cmbZGN2.ItemIndex := 0;
    cmbZGN2.Enabled := True;
  end;

  DataUn.dmDataModule.dsMotors.Filtered := False;
  except
  on E: Exception do
  begin
    imgMenu.OnMouseEnter := imgMenuMouseEnter;
    Screen.Cursor := crDefault;
    MessageDlg('TDiscoverFm.PopulateZGN: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;

procedure TDiscoverFm.rhUIDRecieved;
var vsMsg: String;
begin
   cmbUID1.Items.Add(IntToStr(VariablesUn.giUID_rx));
   cmbUID1.ItemIndex := cmbUID1.Items.Count - 1;

   cmbUID2.Items.Add(IntToStr(VariablesUn.giUID_rx));
   cmbUID2.ItemIndex := cmbUID2.Items.Count - 1;
end;

procedure TDiscoverFm.btnExitClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  if not (gbSaveProject) or not (MessageDlg('Do you want to save the changes?', mtWarning, [mbYes, mbNo], 0) = mrYes) then
  begin
    Application.Terminate;
  end;
end;

procedure TDiscoverFm.btnExitMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TDiscoverFm.btnFindClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TDiscoverFm.btnFindMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TDiscoverFm.btnMainClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  if not (gbSaveProject) or not (MessageDlg('Do you want to save the changes?', mtWarning, [mbYes, mbNo], 0) = mrYes) then
  begin
    lblProjectName.Color := clWhite;
    DiscoverFm.Hide;
    if Assigned (rh_ShowMain) then
    rh_ShowMain(Self);
  end;
end;

procedure TDiscoverFm.btnMainMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TDiscoverFm.rhPortClosed(Sender: TObject);
begin
 giComPortOn := False;
 Shape1.Brush.Color := clGray;
 Shape2.Brush.Color := clGray;
 Shape3.Brush.Color := clGray;
 Shape4.Brush.Color := clGray;
 Shape5.Brush.Color := clGray;
 btnDiscover.Enabled := False;
end;

procedure TDiscoverFm.rhPortOpend(Sender: TObject);
begin
 giComPortOn := True;
 Shape1.Brush.Color := clLime;
 Shape2.Brush.Color := clLime;
 Shape3.Brush.Color := clLime;
 Shape4.Brush.Color := clLime;
 Shape5.Brush.Color := clLime;
 btnDiscover.Enabled := True;
end;

procedure TDiscoverFm.rhStatusRecieved;
begin
  try
    if (DataUn.dmDataModule.dsMotors.State = dsEdit) and (giSentCom = VariablesUn.giCommand) and (giUID = VariablesUn.giUID_rx) then
    begin
      case VariablesUn.giCommand of
        44:                   // LV Port
        begin
          DataUn.dmDataModule.dsMotors.FieldByName('LVPOrt').AsString := VariablesUn.BuildZGN;
        end;
        50:                   // Address 1
        begin
          DataUn.dmDataModule.dsMotors.FieldByName('Address1').AsString := VariablesUn.BuildZGN;
        end;
        54:                   // Address 2
        begin
          DataUn.dmDataModule.dsMotors.FieldByName('Address2').AsString := VariablesUn.BuildZGN;
        end;
        58:                   // Address 3
        begin
          DataUn.dmDataModule.dsMotors.FieldByName('Address3').AsString := VariablesUn.BuildZGN;
        end;
        62:                   // Address 4
        begin
          DataUn.dmDataModule.dsMotors.FieldByName('Address4').AsString := VariablesUn.BuildZGN;
        end;
        66:                   // Address 5
        begin
          DataUn.dmDataModule.dsMotors.FieldByName('Address5').AsString := VariablesUn.BuildZGN;
        end;
        70:                   // Address 6
        begin
          DataUn.dmDataModule.dsMotors.FieldByName('Address6').AsString := VariablesUn.BuildZGN;
        end;
        74:                   // Address 7
        begin
          DataUn.dmDataModule.dsMotors.FieldByName('Address7').AsString := VariablesUn.BuildZGN;
        end;
        78:                   // Address 8
        begin
          DataUn.dmDataModule.dsMotors.FieldByName('Address8').AsString := VariablesUn.BuildZGN;
        end;
      end;
    end;
    ComFm.rh_ReturnHandler := nil;
  except
  on E: Exception do
  begin
    ComFm.rh_ReturnHandler := nil;
    dmDataModule.dsMotors.Cancel;
    imgMenu.OnMouseEnter := imgMenuMouseEnter;
    Screen.Cursor := crDefault;
    MessageDlg('TDiscoverFm.rhStatusRecieved: ' + E.Message, mtWarning, [mbOk], 0);
  end;
 end;
end;

procedure TDiscoverFm.btnConfigClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TDiscoverFm.btnDiscoverClick(Sender: TObject);
begin
  try
    cmbUID1.Clear;
    cmbUID2.Clear;
    cmbZGN1.Clear;
    cmbZGN2.Clear;
    cmbUID1.Enabled := False;
    cmbUID2.Enabled := False;
    cmbZGN1.Enabled := False;
    cmbZGN2.Enabled := False;
    btnSave.Enabled := False;
    cmbUID1.Sorted := False;
    cmbUID2.Sorted := False;
    lblProjectName.Color := clWhite;
    pbDisplayDelay.Style := pbstNormal;
    ClearData;
    giSentCom := 1;
    Screen.Cursor := crDefault;
    gbSaveProject := False;

    ComFm.SendCommand(VariablesUn.GetUIDs, rhUIDRecieved);
    btnDiscover.Enabled := False;
    giDspDelay := 150;
    pbDisplayDelay.Position := 0;
    pbDisplayDelay.Visible := True;
    imgMenu.OnMouseEnter := Nil;
    pbDisplayDelay.StepIt;
    tmDelay.Enabled := True;
 except
 on E: Exception do
 begin
  imgMenu.OnMouseEnter := imgMenuMouseEnter;

  MessageDlg('TDiscoverFm.btnDiscoverClick: ' + E.Message, mtWarning, [mbOk], 0);
 end;
 End;
  //ComUn.dmComModule.Delay(15000);
end;

procedure TDiscoverFm.btnHelpClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TDiscoverFm.btnInfoClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TDiscoverFm.tmDelayTimer(Sender: TObject);
begin
  Dec(giDspDelay);
  pbDisplayDelay.StepIt;

  if giDspDelay < 1 then
  begin
    Screen.Cursor := crHourGlass;
    ComFm.rh_ReturnHandler := nil;
    tmDelay.Enabled := False;
    pbDisplayDelay.Visible := False;
    pbDisplayDelay.Style := pbstMarquee;
    pbDisplayDelay.Visible := True;

    PopulateDataset;

    if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
    begin
      GetStatus;
    end;

    pbDisplayDelay.Visible := False;
    imgMenu.OnMouseEnter := imgMenuMouseEnter;
    if giComPortOn then
        btnDiscover.Enabled := True;

    if (DataUn.dmDataModule.dsMotors.State = dsEdit) then
    begin
       DataUn.dmDataModule.dsMotors.Post;
       dmDataModule.dsMotors.MergeChangeLog;
    end;

    DataUn.dmDataModule.dsMotors.Filtered := False;

    cmbUID1.Sorted := True;
    cmbUID2.Sorted := True;

    PopulateZGN;

    if cmbUID1.Items.Count > 0 then
    begin
      cmbUID1.ItemIndex := 0;
      cmbUID1.Enabled := True;
      btnSave.Enabled := True;
      lblProjectName.Color := $00CCFFFF;
    end;

    if cmbUID2.Items.Count > 0 then
    begin
      cmbUID2.ItemIndex := 0;
      cmbUID2.Enabled := True;
    end;
    Screen.Cursor := crDefault;
  end;
end;

end.
