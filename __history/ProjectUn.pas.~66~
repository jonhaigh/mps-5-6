unit ProjectUn;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, ComUn, VariablesUn, DiscoverUn,
  Vcl.Buttons, DataUn, Datasnap.DBClient, Data.DB;

type
  TProjectFm = class(TForm)
    cbProject: TComboBox;
    Image1: TImage;
    Panel1: TPanel;
    Shape4: TShape;
    Shape3: TShape;
    Shape2: TShape;
    Shape5: TShape;
    Shape1: TShape;
    lblAlias: TLabel;
    lblProjectName: TLabel;
    btnSave: TButton;
    Label2: TLabel;
    cbComPort: TComboBox;
    SpeedButton1: TSpeedButton;
    btnAuto: TButton;
    pnlMenuBar: TPanel;
    Image3: TImage;
    btnExit: TSpeedButton;
    btnFind: TSpeedButton;
    btnMain: TSpeedButton;
    btnConfig: TSpeedButton;
    btnHelp: TSpeedButton;
    btnInfo: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure Image1MouseEnter(Sender: TObject);
    procedure pnlMenuBarMouseEnter(Sender: TObject);
    procedure pnlMenuBarMouseLeave(Sender: TObject);
    procedure btnMainMouseEnter(Sender: TObject);
    procedure btnConfigMouseEnter(Sender: TObject);
    procedure btnFindMouseEnter(Sender: TObject);
    procedure btnHelpClick(Sender: TObject);
    procedure btnInfoClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure btnMainClick(Sender: TObject);
    procedure btnConfigClick(Sender: TObject);
    procedure btnFindClick(Sender: TObject);
    procedure btnExitMouseEnter(Sender: TObject);
    procedure FormClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure cbProjectChange(Sender: TObject);
    procedure btnAutoClick(Sender: TObject);
  private
    { Private declarations }
    procedure ShowProject;
    procedure SelectProject;
  public
    { Public declarations }
    rh_ShowMain: TNotifyEvent;
    procedure rhPortClosed(Sender: TObject);
    procedure rhPortOpend(Sender: TObject);
  end;

var
  ProjectFm: TProjectFm;

implementation

{$R *.dfm}

procedure TProjectFm.btnAutoClick(Sender: TObject);
begin
  btnMainClick(nil);
end;

procedure TProjectFm.btnConfigClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TProjectFm.btnConfigMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TProjectFm.btnHelpClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TProjectFm.btnInfoClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TProjectFm.FormClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TProjectFm.FormDestroy(Sender: TObject);
begin
    rh_ShowMain := nil;
end;

procedure TProjectFm.FormHide(Sender: TObject);
begin
  VariablesUn.giTop := Top;
  VariablesUn.giLeft := Left;
end;

procedure TProjectFm.FormShow(Sender: TObject);
begin
  Top := VariablesUn.giTop;
  Left := VariablesUn.giLeft;
  Screen.Cursor := crDefault;
  ShowProject;
  lblProjectName.Caption := VariablesUn.gsProjectName;
end;

procedure TProjectFm.Image1MouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TProjectFm.pnlMenuBarMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TProjectFm.pnlMenuBarMouseLeave(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TProjectFm.rhPortClosed(Sender: TObject);
begin
 Shape1.Brush.Color := clGray;
 Shape2.Brush.Color := clGray;
 Shape3.Brush.Color := clGray;
 Shape4.Brush.Color := clGray;
 Shape5.Brush.Color := clGray;
end;

procedure TProjectFm.rhPortOpend(Sender: TObject);
begin
 Shape1.Brush.Color := clLime;
 Shape2.Brush.Color := clLime;
 Shape3.Brush.Color := clLime;
 Shape4.Brush.Color := clLime;
 Shape5.Brush.Color := clLime;
end;

procedure TProjectFm.SelectProject;
begin
//
end;

procedure TProjectFm.ShowProject;
begin
  try
   cbProject.OnChange := nil;
   cbProject.Items.Clear;
   if not (DataUn.dmDataModule.dsProject.State = dsInactive) then
   begin
    DataUn.dmDataModule.dsProject.First;
    while not DataUn.dmDataModule.dsProject.eof do
    begin
      if (DataUn.dmDataModule.dsProject.FieldByName('Last').AsInteger = 1) then
        cbProject.Items.Insert(0, DataUn.dmDataModule.dsProject.FieldByName('ProjectName').AsString)
      else
        cbProject.Items.Add(DataUn.dmDataModule.dsProject.FieldByName('ProjectName').AsString);

      DataUn.dmDataModule.dsProject.Next;
    end;
   end;
   if cbProject.Items.Count > 0 then
     cbProject.ItemIndex := 0;

   VariablesUn.gsProjectName := cbProject.Text;
   cbProject.OnChange := cbProjectChange;
 except
 on E: Exception do
 begin
  cbProject.OnChange := cbProjectChange;
  MessageDlg('TProjectFm.ShowProject: ' + E.Message, mtWarning, [mbOk], 0);
 end;
 End;
end;

procedure TProjectFm.btnExitClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  Application.Terminate;
end;

procedure TProjectFm.btnExitMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TProjectFm.btnFindClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  Hide;
  DiscoverFm.Show;
end;

procedure TProjectFm.btnFindMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TProjectFm.btnMainClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  ProjectFm.Hide;
  if Assigned (rh_ShowMain) then
     rh_ShowMain(Self);
end;

procedure TProjectFm.btnMainMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TProjectFm.cbProjectChange(Sender: TObject);
begin
  ActiveControl := nil;
end;

end.
