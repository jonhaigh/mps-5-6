unit ScreenSetupUn;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, ComUn, VariablesUn, DiscoverUn,
  Vcl.Buttons, DataUn, Datasnap.DBClient, Data.DB, ConfigScreen;

type
  TScreenSetupFm = class(TForm)
    Panel1: TPanel;
    Shape4: TShape;
    Shape3: TShape;
    Shape2: TShape;
    Shape5: TShape;
    Shape1: TShape;
    lblAlias: TLabel;
    lblProjectName: TLabel;
    rgControl: TRadioGroup;
    rgType: TRadioGroup;
    Label1: TLabel;
    Label2: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Image1: TImage;
    btnSetup: TImage;
    Image2: TImage;
    pnlMenuBar: TPanel;
    Image3: TImage;
    btnMain: TImage;
    btnConfig: TImage;
    btnFind: TImage;
    btnHelp: TImage;
    btnInfo: TImage;
    btnExit: TImage;
    lblPassword: TLabel;
    txtPassword: TEdit;
    btnEnter: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure Image1MouseEnter(Sender: TObject);
    procedure pnlMenuBarMouseEnter(Sender: TObject);
    procedure pnlMenuBarMouseLeave(Sender: TObject);
    procedure btnMainMouseEnter(Sender: TObject);
    procedure btnConfigMouseEnter(Sender: TObject);
    procedure btnFindMouseEnter(Sender: TObject);
    procedure btnHelpClick(Sender: TObject);
    procedure btnInfoClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure btnMainClick(Sender: TObject);
    procedure btnConfigClick(Sender: TObject);
    procedure btnFindClick(Sender: TObject);
    procedure btnExitMouseEnter(Sender: TObject);
    procedure FormClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure rgControlClick(Sender: TObject);
    procedure rgTypeClick(Sender: TObject);
    procedure btnInfoMouseEnter(Sender: TObject);
    procedure btnSetupClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure txtPasswordKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure txtPasswordChange(Sender: TObject);
    procedure btnEnterClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    giAtmptCount: Integer;
    procedure rhShowSetup(Sender: TObject);
  public
    { Public declarations }
    rh_ShowMain: TNotifyEvent;
    procedure rhPortClosed(Sender: TObject);
    procedure rhPortOpend(Sender: TObject);
  end;

var
  ScreenSetupFm: TScreenSetupFm;

implementation
uses AboutUn;
{$R *.dfm}

procedure TScreenSetupFm.btnConfigClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  IQMainFm.Hide;
  ConfigScreenFm.Show;
  ConfigScreenFm.btnGetStatusClick(nil); //ConfigScreenFm.GetMotorStatus;
end;

procedure TScreenSetupFm.btnConfigMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TScreenSetupFm.btnHelpClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TScreenSetupFm.btnInfoClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  Hide;
  AboutFm.rh_ShowForm := rhShowSetup;
  AboutFm.Show;
end;

procedure TScreenSetupFm.btnInfoMouseEnter(Sender: TObject);
begin
    pnlMenuBar.Visible := True;
end;

procedure TScreenSetupFm.FormClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TScreenSetupFm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := False;
    btnExitClick(Nil);
end;

procedure TScreenSetupFm.FormCreate(Sender: TObject);
begin
  giAtmptCount := 0;
end;

procedure TScreenSetupFm.FormDestroy(Sender: TObject);
begin
   rh_ShowMain := nil;
end;

procedure TScreenSetupFm.FormHide(Sender: TObject);
begin
  VariablesUn.giTop := Top;
  VariablesUn.giLeft := Left;
end;

procedure TScreenSetupFm.FormShow(Sender: TObject);
begin
try
  Top := VariablesUn.giTop;
  Left := VariablesUn.giLeft;

  if  btnEnter.Visible then
  begin
    txtPassword.Text := '';
    btnEnter.Enabled := False;
  end;

  rgControl.OnClick := nil;
  Screen.Cursor := crDefault;
  lblProjectName.Caption := VariablesUn.gsProjectName;
  rgControl.Hide;
  rgControl.ItemIndex := VariablesUn.giAdrType;
  rgControl.Show;
  rgType.Hide;
  rgType.ItemIndex := VariablesUn.giCmdType;
  rgType.Show;
finally
  rgControl.OnClick := rgControlClick;
end;
end;

procedure TScreenSetupFm.Image1MouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TScreenSetupFm.pnlMenuBarMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TScreenSetupFm.pnlMenuBarMouseLeave(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TScreenSetupFm.rgControlClick(Sender: TObject);
begin
  try
  if not (DataUn.dmDataModule.dsProject.State = dsInactive) then
  begin
    DataUn.dmDataModule.dsProject.Filtered := False;
    DataUn.dmDataModule.dsProject.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption);
    DataUn.dmDataModule.dsProject.Filtered := True;
    if not dmDataModule.dsProject.Eof then
    begin
      dmDataModule.dsProject.Edit;
      DataUn.dmDataModule.dsProject.FieldByName('AdrType').AsInteger := rgControl.ItemIndex;
      dmDataModule.dsProject.Post;
      dmDataModule.dsProject.MergeChangeLog;
      DataUn.dmDataModule.dsProject.SaveToFile(VariablesUn.gsDataPath + 'dsProject.txt', dfXML);
    end;
    DataUn.dmDataModule.dsProject.Filtered := False;
  end;
  VariablesUn.giAdrType := rgControl.ItemIndex;
  except
  on E: Exception do
  begin
    MessageDlg('TScreenSetupFm.rgControlClick: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;

procedure TScreenSetupFm.rgTypeClick(Sender: TObject);
begin
  try
  if not (DataUn.dmDataModule.dsProject.State = dsInactive) then
  begin
    DataUn.dmDataModule.dsProject.Filtered := False;
    DataUn.dmDataModule.dsProject.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption);
    DataUn.dmDataModule.dsProject.Filtered := True;
    if not dmDataModule.dsProject.Eof then
    begin
      dmDataModule.dsProject.Edit;
      DataUn.dmDataModule.dsProject.FieldByName('CmdType').AsInteger := rgType.ItemIndex;
      dmDataModule.dsProject.Post;
      dmDataModule.dsProject.MergeChangeLog;
      DataUn.dmDataModule.dsProject.SaveToFile(VariablesUn.gsDataPath + 'dsProject.txt', dfXML);
    end;
    DataUn.dmDataModule.dsProject.Filtered := False;
  end;
  VariablesUn.giCmdType := rgType.ItemIndex;
  except
  on E: Exception do
  begin
    MessageDlg('TScreenSetupFm.rgTypeClick: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;

procedure TScreenSetupFm.rhPortClosed(Sender: TObject);
begin
 Shape1.Brush.Color := clRed;;
 Shape2.Brush.Color := clRed;;
 Shape3.Brush.Color := clRed;;
 Shape4.Brush.Color := clRed;;
 Shape5.Brush.Color := clRed;;
end;

procedure TScreenSetupFm.rhPortOpend(Sender: TObject);
begin
 Shape1.Brush.Color := clLime;
 Shape2.Brush.Color := clLime;
 Shape3.Brush.Color := clLime;
 Shape4.Brush.Color := clLime;
 Shape5.Brush.Color := clLime;

 if not VariablesUn.gbUSBfound then
 begin
  Shape1.Brush.Color := clRed;;
  Shape2.Brush.Color := clRed;;
  Shape3.Brush.Color := clRed;;
  Shape4.Brush.Color := clRed;;
  Shape5.Brush.Color := clRed;;
 end;
end;

procedure TScreenSetupFm.rhShowSetup(Sender: TObject);
begin
  Show;
end;

procedure TScreenSetupFm.txtPasswordChange(Sender: TObject);
begin
  if txtPassword.Text <> '' then
    btnEnter.Enabled := True
  else
    btnEnter.Enabled := False;
end;

procedure TScreenSetupFm.txtPasswordKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if Key = 13 then
      btnEnterClick(nil);
end;

procedure TScreenSetupFm.btnEnterClick(Sender: TObject);
begin
  if txtPassword.Text = 'mssadmin' then
  begin
   VariablesUn.gbShowSTA := True;
   lblPassword.Caption := 'Access Level:';
   txtPassword.Text := 'Admin';
   txtPassword.Color := clBlack;
   txtPassword.Font.Color := clWhite;
   txtPassword.ReadOnly := True;
   btnEnter.Visible := False;
   ActiveControl := nil;
  end
  else
  begin
    if giAtmptCount < 3 then
    begin
      Inc(giAtmptCount);
      MessageDlg('Invalid password!', mtError, [mbOK], 0);
      txtPassword.SelectAll;
    end
    else
    begin
      txtPassword.Text := '';
      txtPassword.Enabled := False;
      btnEnter.Enabled := False;
      MessageDlg('Invalid password! Administrator Account has been locked out.', mtWarning, [mbOK], 0);
    end;
  end;
end;

procedure TScreenSetupFm.btnExitClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  Application.Terminate;
end;

procedure TScreenSetupFm.btnExitMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TScreenSetupFm.btnFindClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  Hide;
  DiscoverFm.Show;
end;

procedure TScreenSetupFm.btnFindMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TScreenSetupFm.btnMainClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  ScreenSetupFm.Hide;
  if Assigned (rh_ShowMain) then
     rh_ShowMain(Self);
end;

procedure TScreenSetupFm.btnMainMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TScreenSetupFm.btnSetupClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  Hide;
  AboutFm.rh_ShowForm := rhShowSetup;
  AboutFm.Show;
end;

end.
