unit NewProjectUn;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, AdSelCom, ComUn, DataUn, Datasnap.DBClient,
  Data.DB, System.UITypes, VariablesUn, Vcl.Buttons, DiscoverUn, AboutUn;

type
  TNewProjectFm = class(TForm)
    Image1: TImage;
    Panel1: TPanel;
    Shape4: TShape;
    Shape3: TShape;
    Shape2: TShape;
    Shape5: TShape;
    Shape1: TShape;
    lblAlias: TLabel;
    lblProjectName: TLabel;
    btnSave: TButton;
    Label1: TLabel;
    txtNewProject: TEdit;
    Label2: TLabel;
    cbComPort: TComboBox;
    pnlMenuBar: TPanel;
    Image3: TImage;
    btnExit: TSpeedButton;
    btnFind: TSpeedButton;
    btnMain: TSpeedButton;
    btnConfig: TSpeedButton;
    btnHelp: TSpeedButton;
    btnInfo: TSpeedButton;
    btnCancel: TButton;
    procedure FormShow(Sender: TObject);
    procedure cbComPortChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure txtNewProjectChange(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure Image1MouseEnter(Sender: TObject);
    procedure btnMainMouseEnter(Sender: TObject);
    procedure btnConfigMouseEnter(Sender: TObject);
    procedure btnFindMouseEnter(Sender: TObject);
    procedure btnHelpMouseEnter(Sender: TObject);
    procedure btnInfoMouseEnter(Sender: TObject);
    procedure btnExitMouseEnter(Sender: TObject);
    procedure pnlMenuBarMouseEnter(Sender: TObject);
    procedure pnlMenuBarMouseLeave(Sender: TObject);
    procedure btnMainClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure btnFindClick(Sender: TObject);
    procedure btnConfigClick(Sender: TObject);
    procedure btnHelpClick(Sender: TObject);
    procedure btnInfoClick(Sender: TObject);
    procedure FormClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
  private
    { Private declarations }
    procedure FindComPort;
  public
    { Public declarations }
    rh_ShowMain: TNotifyEvent;
    procedure rhPortOpend(Sender: TObject);
    procedure rhPortClosed(Sender: TObject);
  end;

var
  NewProjectFm: TNewProjectFm;

implementation

{$R *.dfm}

procedure TNewProjectFm.btnCancelClick(Sender: TObject);
begin
  btnMainClick(nil);
end;

procedure TNewProjectFm.btnConfigClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TNewProjectFm.btnConfigMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TNewProjectFm.btnHelpClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TNewProjectFm.btnHelpMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TNewProjectFm.btnInfoClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TNewProjectFm.btnInfoMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TNewProjectFm.btnSaveClick(Sender: TObject);
begin
  try
  if DataUn.dmDataModule.dsProject.State = dsInactive then
  begin
    DataUn.dmDataModule.dsProject.CreateDataSet;
    DataUn.dmDataModule.dsProject.Active := True;
  end;
  {
  DataUn.dmDataModule.dsProject.Filtered := False;
  DataUn.dmDataModule.dsProject.Filter := 'Last = ' + IntToStr(1);
  DataUn.dmDataModule.dsProject.Filtered := True;
  }

  DataUn.dmDataModule.dsProject.Filtered := False;
  DataUn.dmDataModule.dsProject.Filter := 'ProjectName = ' + QuotedStr(txtNewProject.Text);
  DataUn.dmDataModule.dsProject.Filtered := True;

  if Not DataUn.dmDataModule.dsProject.Eof then
  begin
    DataUn.dmDataModule.dsProject.Filtered := False;
    MessageDlg('A project with this name already exists!', mtWarning, [mbOk], 0);
    exit;
  end;
  DataUn.dmDataModule.dsProject.Filtered := False;

  DataUn.dmDataModule.dsProject.First;

  while not dmDataModule.dsProject.Eof do
  begin
    dmDataModule.dsProject.Edit;
    DataUn.dmDataModule.dsProject.FieldByName('Last').AsInteger := 0;
    DataUn.dmDataModule.dsProject.Post;
    DataUn.dmDataModule.dsProject.Next;
  end;
  dmDataModule.dsProject.MergeChangeLog;
  //DataUn.dmDataModule.dsProject.Filtered := False;

  if not (length(txtNewProject.Text) = 0) then
  begin
     if not (cbComPort.ItemIndex = -1) then
     begin
       DataUn.dmDataModule.dsProject.Insert;
       DataUn.dmDataModule.dsProject.FieldByName('ProjectName').AsString := txtNewProject.Text;
       DataUn.dmDataModule.dsProject.FieldByName('Last').AsInteger := 1;
       DataUn.dmDataModule.dsProject.FieldByName('ComPort').AsString := cbComPort.Text;
       DataUn.dmDataModule.dsProject.FieldByName('AdrType').AsInteger := 2;
       DataUn.dmDataModule.dsProject.FieldByName('CmdType').AsInteger := 0;
       DataUn.dmDataModule.dsProject.Post;
       DataUn.dmDataModule.dsProject.SaveToFile(VariablesUn.gsDataPath + 'dsProject.txt', dfXML);
       VariablesUn.gsProjectName := txtNewProject.Text;
       lblProjectName.Caption := txtNewProject.Text;
       txtNewProject.Color := clWhite;
       NewProjectFm.Hide;
       if Assigned (rh_ShowMain) then
          rh_ShowMain(Self);
     end
     else
     begin
       MessageDlg('Please select a COM Port.', mtWarning, [mbOk], 0);
     end;
  end
  else
  begin
    MessageDlg('Please enter the project name.', mtWarning, [mbOk], 0);
  end;
  except
  on E: Exception do
  begin
    MessageDlg('TNewProjectFm.btnSaveClick: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;

procedure TNewProjectFm.cbComPortChange(Sender: TObject);
var vsPortNumber: String;
    viPos, viPort: Integer;
begin
  try
    ActiveControl := nil;
    viPos := pos('COM', cbComPort.Text);
    if viPos > 0 then
    begin
      vsPortNumber := Copy(cbComPort.Text, viPos + 3, 50);
      viPort := StrToInt(vsPortNumber);
      if IsPortAvailable(viPort) then
      begin
        ComFm.ComPort.Open := false;
        VariablesUn.Delay(100);
        ComFm.ComPort.ComNumber := viPort;
        ComFm.ComPort.Baud := 9600;

        if  not ComFm.ComPort.Open then
        begin
           ComFm.ComPort.Open := True;
           VariablesUn.Delay(100);
        end;
        txtNewProject.Color := $00A4FFFF;
        //ComUn.dmComModule.ComPort.ProcessCommunications;
        VariablesUn.giPortIndexOld := cbComPort.ItemIndex;
      end
      else
      begin
        cbComPort.ItemIndex := VariablesUn.giPortIndexOld;
        //MessageDlg('The selected port is in use. Please select another port.', mtWarning, [mbOk], 0);
      end;
    end;
  except
  on E: Exception do
  begin
    MessageDlg('TNewProjectFm.cbComPortChange: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;

procedure TNewProjectFm.FindComPort;
var
  I : Integer;
  S : string;
  begin
  try
    cbComPort.Items.Clear;
    for I := 1 to 50 do
    begin
     if IsPortAvailable(I) then
     begin
       S := Format('COM%d', [I]);
       //if ComportInUse= True then S:=S+'  [In use]';
       cbComPort.Items.Add(S);
     end;
    end;
  except
  on E: Exception do
  begin
    MessageDlg('TNewProjectFm.FindComPort: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;

procedure TNewProjectFm.FormClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TNewProjectFm.FormCreate(Sender: TObject);
begin
  ShowPortsInUse := False;
  FindComPort;
end;

procedure TNewProjectFm.FormDestroy(Sender: TObject);
begin
    rh_ShowMain := nil;
end;

procedure TNewProjectFm.FormHide(Sender: TObject);
begin
  VariablesUn.giTop := Top;
  VariablesUn.giLeft := Left;
end;

procedure TNewProjectFm.FormShow(Sender: TObject);
begin
  Top := VariablesUn.giTop;
  Left := VariablesUn.giLeft;

  Screen.Cursor := crDefault;
  pnlMenuBar.Visible := False;
  lblProjectName.Caption := 'New Project';
  txtNewProject.Text := 'New Project';
  txtNewProject.Color := clWhite;
end;

procedure TNewProjectFm.Image1MouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TNewProjectFm.pnlMenuBarMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TNewProjectFm.pnlMenuBarMouseLeave(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TNewProjectFm.rhPortClosed(Sender: TObject);
begin
 Shape1.Brush.Color := clGray;
 Shape2.Brush.Color := clGray;
 Shape3.Brush.Color := clGray;
 Shape4.Brush.Color := clGray;
 Shape5.Brush.Color := clGray;
end;

procedure TNewProjectFm.rhPortOpend(Sender: TObject);
begin
 Shape1.Brush.Color := clLime;
 Shape2.Brush.Color := clLime;
 Shape3.Brush.Color := clLime;
 Shape4.Brush.Color := clLime;
 Shape5.Brush.Color := clLime;
end;

procedure TNewProjectFm.btnExitClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  if (txtNewProject.Color = clWhite) or not (MessageDlg('Do you want to save the changes?', mtWarning, [mbYes, mbNo], 0) = mrYes) then
  begin
    Application.Terminate;
  end;
end;

procedure TNewProjectFm.btnExitMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TNewProjectFm.btnFindClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  if (txtNewProject.Color = clWhite) or not (MessageDlg('Do you want to save the changes?', mtWarning, [mbYes, mbNo], 0) = mrYes) then
  begin
    NewProjectFm.Hide;
    DiscoverFm.Show;
  end
  else
    btnSave.SetFocus;
end;

procedure TNewProjectFm.btnFindMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TNewProjectFm.btnMainClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  if (txtNewProject.Color = clWhite) or not (MessageDlg('Do you want to save the changes?', mtWarning, [mbYes, mbNo], 0) = mrYes) then
  begin
    NewProjectFm.Hide;
    if Assigned (rh_ShowMain) then
     rh_ShowMain(Self);
  end
  else
    btnSave.SetFocus;
  //NewProjectFm.Hide;
end;

procedure TNewProjectFm.btnMainMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TNewProjectFm.txtNewProjectChange(Sender: TObject);
begin
  txtNewProject.Color := $00A4FFFF;
end;

end.
