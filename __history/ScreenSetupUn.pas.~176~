unit ScreenSetupUn;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, ComUn, VariablesUn, DiscoverUn,
  Vcl.Buttons, DataUn, Datasnap.DBClient, Data.DB, ConfigScreen,NewProjectUn,
  Vcl.Imaging.pngimage,adselcom;

type
  TScreenSetupFm = class(TForm)
    lblAlias: TLabel;
    lblProjectName: TLabel;
    rgControl: TRadioGroup;
    rgType: TRadioGroup;
    Label1: TLabel;
    Label2: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Image1: TImage;
    btnSetup: TImage;
    Image2: TImage;
    pnlMenuBar: TPanel;
    Image3: TImage;
    btnFind: TImage;
    btnInfo: TImage;
    btnExit: TImage;
    lblPassword: TLabel;
    txtPassword: TEdit;
    btnEnter: TSpeedButton;
    Label3: TLabel;
    txtNewProject: TEdit;
    cbComPort: TComboBox;
    Label4: TLabel;
    Image4: TImage;
    btnSave: TButton;
    Shape14: TShape;
    cmbZGNFormat: TComboBox;
    Label5: TLabel;
    procedure FormShow(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure Image1MouseEnter(Sender: TObject);
    procedure pnlMenuBarMouseEnter(Sender: TObject);
    procedure pnlMenuBarMouseLeave(Sender: TObject);
    procedure btnMainMouseEnter(Sender: TObject);
    procedure btnConfigMouseEnter(Sender: TObject);
    procedure btnFindMouseEnter(Sender: TObject);
    procedure btnHelpClick(Sender: TObject);
    procedure btnInfoClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure btnMainClick(Sender: TObject);
    procedure btnConfigClick(Sender: TObject);
    procedure btnFindClick(Sender: TObject);
    procedure btnExitMouseEnter(Sender: TObject);
    procedure FormClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure rgControlClick(Sender: TObject);
    procedure rgTypeClick(Sender: TObject);
    procedure btnInfoMouseEnter(Sender: TObject);
    procedure btnSetupClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure txtPasswordKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure txtPasswordChange(Sender: TObject);
    procedure btnEnterClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure txtNewProjectChange(Sender: TObject);
    procedure cbComPortChange(Sender: TObject);
    procedure ComPortSelect;
    procedure Image2Click(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure FindComPort;
    procedure SaveProject;
    procedure cmbZGNFormatChange(Sender: TObject);
  private
    { Private declarations }
    giAtmptCount: Integer;
    gbSavePort: Boolean;
    procedure rhShowSetup(Sender: TObject);
  public
    { Public declarations }
    rh_ShowMain: TNotifyEvent;
    procedure rhPortClosed(Sender: TObject);
    procedure rhPortOpend(Sender: TObject);
  end;

var
  ScreenSetupFm: TScreenSetupFm;

implementation
uses AboutUn;
{$R *.dfm}

procedure TScreenSetupFm.SaveProject;
begin
  try
  if not (DataUn.dmDataModule.dsProject.State = dsInactive) then
  begin
    Screen.Cursor := crHourGlass;
    DataUn.dmDataModule.dsProject.Filtered := False;
    DataUn.dmDataModule.dsProject.Filter :=  'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsProject.Filtered := True;


    if not (length(txtNewProject.Text) = 0) then
    begin
       if not (cbComPort.ItemIndex = -1) then
       begin
        if not DataUn.dmDataModule.dsProject.Eof then
        begin
           DataUn.dmDataModule.dsProject.Edit;
           DataUn.dmDataModule.dsProject.FieldByName('ProjectName').AsString := txtNewProject.Text;
           DataUn.dmDataModule.dsProject.FieldByName('ComPort').AsString := cbComPort.Text;
           DataUn.dmDataModule.dsProject.Post;
           dmDataModule.dsProject.MergeChangeLog;
           if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
           begin
           {
              DataUn.dmDataModule.dsMotors.Filtered := False;
              DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
              DataUn.dmDataModule.dsMotors.Filtered := True;
           }
              DataUn.dmDataModule.dsMotors.Filtered := False;
              DataUn.dmDataModule.dsMotors.First;
              while not dmDataModule.dsMotors.Eof do
              begin
                if (DataUn.dmDataModule.dsMotors.FieldByName('ProjectName').AsString = VariablesUn.gsProjectName) then
                begin
                  dmDataModule.dsMotors.Edit;
                  DataUn.dmDataModule.dsMotors.FieldByName('ProjectName').AsString := txtNewProject.Text;
                  DataUn.dmDataModule.dsMotors.Post;
                end;
                DataUn.dmDataModule.dsMotors.Next;
              end;
              dmDataModule.dsMotors.MergeChangeLog;
              //DataUn.dmDataModule.dsMotors.Filtered := False;
           end;
           DataUn.dmDataModule.dsProject.Filtered := False;
           DataUn.dmDataModule.dsProject.SaveToFile(VariablesUn.gsDataPath + 'dsProject.txt', dfXML);
           DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
           VariablesUn.gsProjectName := txtNewProject.Text;
           lblProjectName.Caption := VariablesUn.gsProjectName;
           gbSavePort := False;
           txtNewProject.Color := clWhite;
           pnlMenuBar.Visible := False;
           Screen.Cursor := crDefault;
           Hide;

           if Assigned (rh_ShowMain) then
              rh_ShowMain(Self);
        end
        else
        begin
          MessageDlg('Cannot find project!', mtWarning, [mbOk], 0);
        end;
       end
       else
       begin
         MessageDlg('Please select a COM Port.', mtWarning, [mbOk], 0);
       end;
    end
    else
    begin
      MessageDlg('Please enter the project name.', mtWarning, [mbOk], 0);
    end;
    Screen.Cursor := crDefault;
  end;
  except
  on E: Exception do
  begin
    Screen.Cursor := crDefault;
    MessageDlg('TSettingsFm.SaveProject: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;

procedure TScreenSetupFm.FindComPort;
var
  I : Integer;
  S: string;
  begin
  try
    cbComPort.Items.Clear;
    for I := 1 to 50 do
    begin
     if IsPortAvailable(I) then
     begin
       S := Format('COM%d', [I]);
       //if ComportInUse= True then S:=S+'  [In use]';
       cbComPort.Items.Add(S);
     end;
    end;
  except
  on E: Exception do
  begin
    MessageDlg('TProjectFm.FindComPort: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;

procedure TScreenSetupFm.btnSaveClick(Sender: TObject);
begin
  if gbSavePort then
  begin
    SaveProject;
  end;
end;

procedure TScreenSetupFm.cbComPortChange(Sender: TObject);
begin
  ComPortSelect;
end;

procedure TScreenSetupFm.cmbZGNFormatChange(Sender: TObject);
begin
  DataUn.dmDataModule.dsProject.Filtered := False;
  DataUn.dmDataModule.dsProject.Filter :=  'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
  DataUn.dmDataModule.dsProject.Filtered := True;
  DataUn.dmDataModule.dsProject.edit;

  DataUn.dmDataModule.dsProject.FieldByName('ZGNG').AsInteger:=cmbZGNFormat.ItemIndex;
  DataUn.dmDataModule.dsProject.MergeChangeLog;
end;

procedure TScreenSetupFm.ComPortSelect;
var vsPortNumber: String;
    viPos, viPort: Integer;
begin
  try
    ActiveControl := nil;
    viPos := pos('COM', cbComPort.Text);
    if viPos > 0 then
    begin
      vsPortNumber := Copy(cbComPort.Text, viPos + 3, 50);
      viPort := StrToInt(vsPortNumber);
      if IsPortAvailable(viPort) then
      begin
        ComFm.ComPort.Open := false;
        VariablesUn.Delay(200);
        ComFm.ComPort.ComNumber := viPort;
        ComFm.ComPort.Baud := 9600;

        if  not ComFm.ComPort.Open then
        begin
          if VariablesUn.gsComPortUSB = cbComPort.Text then
            VariablesUn.gbUSBfound := True
          else
            VariablesUn.gbUSBfound := False;

           ComFm.ComPort.Open := True;
           VariablesUn.Delay(100);
        end;
        gbSavePort := True;
        txtNewProject.Color := $00A4FFFF;
        //ComUn.dmComModule.ComPort.ProcessCommunications;
        VariablesUn.giPortIndexOld := cbComPort.ItemIndex;
      end
      else
      begin
        cbComPort.ItemIndex := VariablesUn.giPortIndexOld;
        gbSavePort := True;
        txtNewProject.Color := $00A4FFFF;
        //MessageDlg('The selected port is in use. Please select another port.', mtWarning, [mbOk], 0);
      end;
    end;
    NewProjectFm.cbComPort.ItemIndex := cbComPort.ItemIndex;
  except
  on E: Exception do
  begin
    MessageDlg('TProjectFm.ComPortSelect: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;

procedure TScreenSetupFm.txtNewProjectChange(Sender: TObject);
begin
  gbSavePort := True;
  txtNewProject.Color := $00A4FFFF;
end;

procedure TScreenSetupFm.btnConfigClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  ScreenSetupFm.Hide;
  ConfigScreenFm.Show;
  ConfigScreenFm.btnGetStatusClick(nil);
end;

procedure TScreenSetupFm.btnConfigMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TScreenSetupFm.btnHelpClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TScreenSetupFm.btnInfoClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  Hide;
  AboutFm.rh_ShowForm := rhShowSetup;
  AboutFm.Show;
end;

procedure TScreenSetupFm.btnInfoMouseEnter(Sender: TObject);
begin
    pnlMenuBar.Visible := True;
end;

procedure TScreenSetupFm.FormClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TScreenSetupFm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := False;
    btnExitClick(Nil);
end;

procedure TScreenSetupFm.FormCreate(Sender: TObject);
begin
  giAtmptCount := 0;
end;

procedure TScreenSetupFm.FormDestroy(Sender: TObject);
begin
   rh_ShowMain := nil;
end;

procedure TScreenSetupFm.FormHide(Sender: TObject);
begin
  VariablesUn.giTop := Top;
  VariablesUn.giLeft := Left;
end;

procedure TScreenSetupFm.FormShow(Sender: TObject);
begin
  try
    Top := VariablesUn.giTop;
    Left := VariablesUn.giLeft;

    if  btnEnter.Visible then
    begin
      txtPassword.Text := '';
      btnEnter.Enabled := False;
    end;

    rgControl.OnClick := nil;
    Screen.Cursor := crDefault;
    lblProjectName.Caption := VariablesUn.gsProjectName;
    rgControl.Hide;
    rgControl.ItemIndex := VariablesUn.giAdrType;
    rgControl.Show;
    rgType.Hide;
    rgType.ItemIndex := VariablesUn.giCmdType;
    rgType.Show;

    lblProjectName.Caption := VariablesUn.gsProjectName;
    txtNewProject.Text := VariablesUn.gsProjectName;
    gbSavePort := False;
    txtNewProject.Color := clWhite;
  finally
    rgControl.OnClick := rgControlClick;
  end;
end;

procedure TScreenSetupFm.Image1MouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TScreenSetupFm.pnlMenuBarMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TScreenSetupFm.pnlMenuBarMouseLeave(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TScreenSetupFm.rgControlClick(Sender: TObject);
begin
  try
  if not (DataUn.dmDataModule.dsProject.State = dsInactive) then
  begin
    DataUn.dmDataModule.dsProject.Filtered := False;
    DataUn.dmDataModule.dsProject.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption);
    DataUn.dmDataModule.dsProject.Filtered := True;
    if not dmDataModule.dsProject.Eof then
    begin
      dmDataModule.dsProject.Edit;
      DataUn.dmDataModule.dsProject.FieldByName('AdrType').AsInteger := rgControl.ItemIndex;
      dmDataModule.dsProject.Post;
      dmDataModule.dsProject.MergeChangeLog;
      DataUn.dmDataModule.dsProject.SaveToFile(VariablesUn.gsDataPath + 'dsProject.txt', dfXML);
    end;
    DataUn.dmDataModule.dsProject.Filtered := False;
  end;
  VariablesUn.giAdrType := rgControl.ItemIndex;
  except
  on E: Exception do
  begin
    MessageDlg('TScreenSetupFm.rgControlClick: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;

procedure TScreenSetupFm.Image2Click(Sender: TObject);
var vsPortNumber: String;
    i, viPort, viPos: Integer;
begin
try
  Screen.Cursor := crHourGlass;
  VariablesUn.gsComPortUSB := ComFm.FindUSB;
  if (VariablesUn.gsComPortUSB <> '-1') and (VariablesUn.gsComPortUSB <> '') then
  begin
    viPos := pos('COM', VariablesUn.gsComPortUSB);
    vsPortNumber := Copy(VariablesUn.gsComPortUSB, viPos + 3, 50);
    if VariablesUn.IsNumber(vsPortNumber) then
    begin
      viPort := StrToInt(vsPortNumber);
      ComFm.ComPort.Open := false;
      VariablesUn.Delay(200);
      if IsPortAvailable(viPort) then
      begin
        ComFm.ComPort.ComNumber := viPort;
        ComFm.ComPort.Baud := 9600;

        VariablesUn.gbUSBfound := True;

        ComFm.ComPort.Open := True;
        VariablesUn.Delay(100);

        for i := 0 to cbComPort.Items.Count - 1 do
        begin
          if cbComPort.Items[i] = VariablesUn.gsComPortUSB then
          begin
            cbComPort.ItemIndex := i;
          end;
        end;

        if cbComPort.Text <> VariablesUn.gsComPortUSB then
        begin
          cbComPort.Items.Add(VariablesUn.gsComPortUSB);
          cbComPort.ItemIndex :=  cbComPort.Items.Count - 1;
        end;

        gbSavePort := True;
        txtNewProject.Color := $00A4FFFF;
        //ComUn.dmComModule.ComPort.ProcessCommunications;
        VariablesUn.giPortIndexOld := cbComPort.ItemIndex;
        MessageDlg('Found USB adapter (' + VariablesUn.gsComPortUSB + ').', mtInformation, [mbOk], 0);
      end
      else
        MessageDlg('Can not open port ' + VariablesUn.gsComPortUSB  + '. Check if USB adapter is connected and not in use by another application.', mtWarning, [mbOk], 0);
    end
    else
     MessageDlg('Can not find USB adapter.', mtWarning, [mbOk], 0);
  end
  else
    MessageDlg('Can not find USB adapter.', mtWarning, [mbOk], 0);

  Screen.Cursor := crDefault;
  except
  on E: Exception do
  begin
    Screen.Cursor := crDefault;
    MessageDlg('TSettingsFm.Image2Click: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;

procedure TScreenSetupFm.rgTypeClick(Sender: TObject);
begin
  try
  if not (DataUn.dmDataModule.dsProject.State = dsInactive) then
  begin
    DataUn.dmDataModule.dsProject.Filtered := False;
    DataUn.dmDataModule.dsProject.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption);
    DataUn.dmDataModule.dsProject.Filtered := True;
    if not dmDataModule.dsProject.Eof then
    begin
      dmDataModule.dsProject.Edit;
      DataUn.dmDataModule.dsProject.FieldByName('CmdType').AsInteger := rgType.ItemIndex;
      dmDataModule.dsProject.Post;
      dmDataModule.dsProject.MergeChangeLog;
      DataUn.dmDataModule.dsProject.SaveToFile(VariablesUn.gsDataPath + 'dsProject.txt', dfXML);
    end;
    DataUn.dmDataModule.dsProject.Filtered := False;
  end;
  VariablesUn.giCmdType := rgType.ItemIndex;
  except
  on E: Exception do
  begin
    MessageDlg('TScreenSetupFm.rgTypeClick: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;

procedure TScreenSetupFm.rhPortClosed(Sender: TObject);
begin
 Shape14.pen.Color := clRed;
end;

procedure TScreenSetupFm.rhPortOpend(Sender: TObject);
begin
 Shape14.pen.Color := clLime;

end;

procedure TScreenSetupFm.rhShowSetup(Sender: TObject);
begin
  Show;
end;

procedure TScreenSetupFm.txtPasswordChange(Sender: TObject);
begin
  if txtPassword.Text <> '' then
    btnEnter.Enabled := True
  else
    btnEnter.Enabled := False;
end;

procedure TScreenSetupFm.txtPasswordKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if Key = 13 then
      btnEnterClick(nil);
end;

procedure TScreenSetupFm.btnEnterClick(Sender: TObject);
begin
  if txtPassword.Text = 'mssadmin' then
  begin
   VariablesUn.gbShowSTA := True;
   txtPassword.PasswordChar := #0;
   lblPassword.Caption := 'Access Level:';
   txtPassword.Text := 'Admin';
   txtPassword.Color := clBlack;
   txtPassword.Font.Color := clWhite;
   txtPassword.ReadOnly := True;
   btnEnter.Visible := False;
   ActiveControl := nil;
  end
  else
  begin
    if giAtmptCount < 3 then
    begin
      Inc(giAtmptCount);
      MessageDlg('Invalid password!', mtError, [mbOK], 0);
      txtPassword.SelectAll;
    end
    else
    begin
      txtPassword.Text := '';
      txtPassword.Enabled := False;
      btnEnter.Enabled := False;
      MessageDlg('Invalid password! Administrator account has been locked out.', mtWarning, [mbOK], 0);
    end;
  end;
end;

procedure TScreenSetupFm.btnExitClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  Application.Terminate;
end;

procedure TScreenSetupFm.btnExitMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TScreenSetupFm.btnFindClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  Hide;
  DiscoverFm.Show;
end;

procedure TScreenSetupFm.btnFindMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TScreenSetupFm.btnMainClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  ScreenSetupFm.Hide;
  if Assigned (rh_ShowMain) then
     rh_ShowMain(Self);
end;

procedure TScreenSetupFm.btnMainMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TScreenSetupFm.btnSetupClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  Hide;
  AboutFm.rh_ShowForm := rhShowSetup;
  AboutFm.Show;
end;

end.
