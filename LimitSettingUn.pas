unit LimitSettingUn;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, JvComponentBase, JvCaptionButton,Datasnap.DBClient,
  Data.DB,DataUn,StrUtils,
  Vcl.ExtCtrls, Vcl.Imaging.jpeg, Vcl.StdCtrls, Vcl.Imaging.pngimage;

type
  TLimitSettingFm = class(TForm)
    JvCaptionButton1: TJvCaptionButton;
    Shape14: TShape;
    Label1: TLabel;
    btnToggleDirection: TButton;
    btnSetLowerLimit: TButton;
    btnSetUpperLimit: TButton;
    btnStart: TImage;
    btnWink: TImage;
    imgUp: TImage;
    imgIS1: TImage;
    imgIS2: TImage;
    imgIS3: TImage;
    imgDn: TImage;
    pnlHeader: TPanel;
    btnBack: TImage;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    cmbMotors: TComboBox;
    tmrUp: TTimer;
    tmrDown: TTimer;
    lblUpperLimitStatus: TLabel;
    lblLowerLimitStatus: TLabel;
    lblAlias: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure JvCaptionButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnBackClick(Sender: TObject);
    procedure btnStartClick(Sender: TObject);
    procedure rhLimitStatusRecieved(sender: TObject);
    procedure btnToggleDirectionClick(Sender: TObject);
    procedure tmrUpTimer(Sender: TObject);
    procedure tmrDownTimer(Sender: TObject);
    procedure imgUpMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure imgUpMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure imgDnMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure imgDnMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btnSetUpperLimitClick(Sender: TObject);
    procedure btnSetLowerLimitClick(Sender: TObject);
    procedure btnWinkClick(Sender: TObject);
    procedure imgIS1Click(Sender: TObject);
    procedure imgIS2Click(Sender: TObject);
    procedure imgIS3Click(Sender: TObject);
    procedure cmbMotorsSelect(Sender: TObject);
    procedure cmbMotorsChange(Sender: TObject);
  private
    mPassFlag:boolean;
    mSentCom:Integer;
    mUID:integer;
    mLimitStartFlag: Boolean;
    mCreepStepSize: Integer;

    function GetAddress(vsAddress: String): String;
    function GetAddressInt(vsAddress: String): Integer;
    function WaitforSetResponse(UID:Integer;Command:String;rh:TNotifyEvent;Resend:Integer):bool;
    function WaitforQueryResponse(UID:Integer;Command:String;rh:TNotifyEvent;Resend:Integer):bool;
    Function GetUID(MixedUID:String):String;

    procedure CalModeOff;
    Procedure SortStringListUIDS(UIDList:TStringList);
    procedure PopulateUidCmb;
    procedure GetLimitStatus;
    { Private declarations }
  public
    rh_ShowMain: TNotifyEvent;
    procedure rhPortClosedLimitSetting(Sender: TObject);
    procedure rhPortOpenLimitSetting(Sender: TObject);
  end;

var
  LimitSettingFm: TLimitSettingFm;

implementation

{$R *.dfm}

uses VariablesUn, MTRConfigScreen, MTRMainUn, DiscoverUn, MWCConfigUn, AboutUn,
  ScreenSetupUn, ComUn;



procedure TLimitSettingFm.btnSetLowerLimitClick(Sender: TObject);
begin
  if (cmbMotors.ItemIndex>=0)and(Trim(cmbMotors.Text)<>'')  then
  begin

    mSentCom := StrToInt(VariablesUn.lmt_SetLower);
    mUID:=GetAddressInt(cmbMotors.Text);
//    ComFm.SetGenericParam(mUID, VariablesUn.lmt_SetLower+'.0.0.0',rhLimitStatusRecieved);
    if(WaitforSetResponse(mUID,  VariablesUn.lmt_SetLower+'.0.0.0', rhLimitStatusRecieved,500))then
    begin
      btnSetLowerLimit.Visible:=false;
      mLimitStartFlag:=false;
      lblLowerLimitStatus.Caption:='SET';
      ShowMessage('Limit set.');
    end
    else
    begin
      ShowMessage('Set lower limit failed.');
    end;
  end;
end;

procedure TLimitSettingFm.btnSetUpperLimitClick(Sender: TObject);
begin
  if (cmbMotors.ItemIndex>=0)and(Trim(cmbMotors.Text)<>'')  then
  begin

    mSentCom := StrToInt(VariablesUn.lmt_SetUpper);
    mUID:=GetAddressInt(cmbMotors.Text);
//    ComFm.SetGenericParam(mUID, VariablesUn.lmt_SetUpper+'.0.0.0',rhLimitStatusRecieved);
    if(WaitforSetResponse(mUID,  VariablesUn.lmt_SetUpper+'.0.0.0', rhLimitStatusRecieved,500))then
    begin
      btnSetUpperLimit.visible:=false;
      btnSetLowerLimit.visible:=true;
      lblUpperLimitStatus.Caption:='SET';
    end
    else
    begin
      ShowMessage('Set upper limit failed.');
    end;
  end;
end;

procedure TLimitSettingFm.btnStartClick(Sender: TObject);
begin
  if (cmbMotors.ItemIndex>=0)and(Trim(cmbMotors.Text)<>'')  then
  begin

    mSentCom := StrToInt(VariablesUn.lmt_StartStop);
    mUID:=GetAddressInt(cmbMotors.Text);
//    ComFm.SetGenericParam(mUID, VariablesUn.lmt_StartStop+'.0.0.1',rhLimitStatusRecieved);
    if(WaitforSetResponse(mUID,  VariablesUn.lmt_StartStop+'.0.0.1', rhLimitStatusRecieved,500))then
    begin
      mLimitStartFlag:=true;
      ShowMessage('Limit setting started.');
      btnSetUpperLimit.Visible:=true;
      lblLowerLimitStatus.Caption:='NOT SET';
      lblUpperLimitStatus.Caption:='NOT SET';
//      shpcmbSerialPerson.Brush.Color :=clLime;
//      shpcmbSerialPerson.Visible:=false;
    end
    else
    begin
      ShowMessage('Limit setting failed.');
//      shpcmbSerialPerson.Brush.Color :=ClRed;
    end;
  end;
end;

procedure TLimitSettingFm.CalModeOff;
begin
  if (cmbMotors.ItemIndex>=0)and(Trim(cmbMotors.Text)<>'')  then
  begin

    mSentCom := StrToInt(VariablesUn.lmt_StartStop);
    mUID:=GetAddressInt(cmbMotors.Text);
    ComFm.SetGenericParam(mUID, VariablesUn.lmt_StartStop+'.0.0.0',rhLimitStatusRecieved);
    delay(100);
  end;
end;
procedure TLimitSettingFm.btnToggleDirectionClick(Sender: TObject);
begin
  if btnToggleDirection.Caption = 'Normal' then
  begin

    if (cmbMotors.ItemIndex>=0)and(Trim(cmbMotors.Text)<>'')  then
    begin

      mSentCom := StrToInt(VariablesUn.UID_Reverse);
      mUID:=GetAddressInt(cmbMotors.Text);
//      ComFm.SetGenericParam(mUID, VariablesUn.UID_Reverse+'.0.0.1',rhLimitStatusRecieved);
      if(WaitforSetResponse(mUID,  VariablesUn.UID_Reverse+'.0.0.1', rhLimitStatusRecieved,500))then
      begin
        btnToggleDirection.Caption := 'Reverse';
      end
      else
      begin
        ShowMessage('Change Ddirection failed.');
      end;
    end;
  end
  else
  begin

    if (cmbMotors.ItemIndex>=0)and(Trim(cmbMotors.Text)<>'')  then
    begin

      mSentCom := StrToInt(VariablesUn.UID_Reverse);
      mUID:=GetAddressInt(cmbMotors.Text);
//      ComFm.SetGenericParam(mUID, VariablesUn.UID_Reverse+'.0.0.0',rhLimitStatusRecieved);
      if(WaitforSetResponse(mUID,  VariablesUn.UID_Reverse+'.0.0.0', rhLimitStatusRecieved,500))then
      begin
        btnToggleDirection.Caption := 'Normal';
      end
      else
      begin
        ShowMessage('Change Ddirection failed.');
      end;
    end;
  end;

end;

procedure TLimitSettingFm.btnWinkClick(Sender: TObject);
begin
   mSentCom := StrToInt(VariablesUn.UID_WINK);
   mUID:=GetAddressInt(cmbMotors.Text);
   ComFm.SetGenericParam(mUID, VariablesUn.UID_WINK+'.0.0.2',rhLimitStatusRecieved);
end;

procedure TLimitSettingFm.cmbMotorsChange(Sender: TObject);
begin
    if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
    begin
      DataUn.dmDataModule.dsMotors.Filtered := False;
      DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + GetUID(cmbMotors.Text);
      DataUn.dmDataModule.dsMotors.Filtered := True;
    end;
    while not DataUn.dmDataModule.dsMotors.Eof do
    begin
       lblAlias.Caption:= DataUn.dmDataModule.dsMotors.FieldByName('Alias').AsString;
       DataUn.dmDataModule.dsMotors.Next;
    end;
end;

procedure TLimitSettingFm.cmbMotorsSelect(Sender: TObject);
begin
  GetLimitStatus
end;

procedure TLimitSettingFm.GetLimitStatus;
begin
  if (cmbMotors.ItemIndex>=0)and(Trim(cmbMotors.Text)<>'')  then
  begin

    mSentCom := StrToInt(VariablesUn.UID_IQMotorSet);
    mUID:=GetAddressInt(cmbMotors.Text);
    if(WaitforQueryResponse(mUID,  VariablesUn.UID_IQMotorSet+'.0.0.0', rhLimitStatusRecieved,500))then
    begin
      btnToggleDirection.Caption := 'Normal';
    end
    else
    begin
      ShowMessage('Change Ddirection failed.');
    end;
  end;
end;

procedure TLimitSettingFm.PopulateUidCmb;
var DeviceType,ComboBoxString:String;
  I: Integer;
  ComboList:TStringList;
  InsertFlag:Boolean;
  FirstFlag:Boolean;
  LastUID:Integer;
begin
  FirstFlag:=true;
  ComboList:=Tstringlist.Create;
  try
    if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
    begin
      DataUn.dmDataModule.dsMotors.Filtered := False;
      DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
      DataUn.dmDataModule.dsMotors.Filtered := True;
    end;
    ComboList.Sorted:=true;
    Combolist.Duplicates:=dupIgnore;
    while not dmDataModule.dsMotors.Eof do
    begin

      if DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='66' then
      begin
        if DataUn.dmDataModule.dsMotors.FieldByName('FirmwareRev').AsString='8.0' then
        begin
          DeviceType:='IQ2';
          ComboBoxString:=IntToStr(DataUn.dmDataModule.dsMotors.FieldByName('UID').AsInteger)+' : '+DeviceType;
          mCreepStepSize:=50;
          ComboList.Add(ComboBoxString);
        end;
      end
      else if DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='69' then
      begin
        DeviceType:='IQ2-DC';
        ComboBoxString:=IntToStr(DataUn.dmDataModule.dsMotors.FieldByName('UID').AsInteger)+' : '+DeviceType;
        mCreepStepSize:=50;
        ComboList.Add(ComboBoxString);
      end
      else if DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='70' then
      begin
        DeviceType:='iQ3-DC';
        ComboBoxString:=IntToStr(DataUn.dmDataModule.dsMotors.FieldByName('UID').AsInteger)+' : '+DeviceType;
        mCreepStepSize:=20;
        ComboList.Add(ComboBoxString);
      end;

      dmDataModule.dsMotors.Next;
    end;
    ComboList.Sorted:=False;
    SortStringListUIDS(ComboList);
    cmbMotors.items.Assign(ComboList);
    if cmbMotors.Items.Count > 0 then
    begin
      cmbMotors.Enabled := True;
      if VariablesUn.gsUIDCrnt<>'' then
      begin
        for I := 0 to cmbMotors.Items.Count-1 do
        begin
          if Pos(VariablesUn.gsUIDCrnt,cmbMotors.Items.Create[I])>0 then
          begin
            cmbMotors.ItemIndex:=I;
            CalModeOff;
            GetLimitStatus;
            break;
          end;
          if I = cmbMotors.Items.Count-1 then
          begin
            cmbMotors.ItemIndex:=0;
            VariablesUn.gsUIDCrnt:=cmbMotors.Items.Create[0];
          end;
        end;
      end
      else
      begin
        cmbMotors.ItemIndex := 0;
      end;
//      cmbMotorsChange(cmbMotors);
//      if (VariablesUn.giAdrType <> 0) and (VariablesUn.giAdrType <> 1) then
//      begin
//        lblUID.Visible := True;
//        cmbMotors.Visible := True;
//        cmbposition.Visible:=true;
//        label2.Visible:=true;
//        VariablesUn.giAdrType := 2;
//        txtAlias.ReadOnly := False;
//        imgSetpointSetOn.Hint := 'Set Intermediate Set Points';
//        if giComPortOn then
//        begin
//          imgSetpointSetOn.Enabled := True;
//        end;
//      end;
//
//      if giComPortOn then
//      begin
//        ButtonsEnable;
//      end;
    end
    else
    begin
      cmbMotors.Enabled := False;
//      ButtonsDisable;
    end;
    DataUn.dmDataModule.dsMotors.Filtered := False;
    cmbMotorsChange(self);
  except
  on E: Exception do
  begin
    MessageDlg('TIQMainFm.ShowProject: ' + E.Message, mtWarning, [mbOk], 0);
  end;
  End;
end;
Function TLimitSettingFm.GetUID(MixedUID:String):String;
var SpacePosition:Integer;
begin
  try
    SpacePosition:=pos(' ', MixedUID)-1;
    if SpacePosition >0 then
    begin
      GetUID:=LeftStr(MixedUID,SpacePosition);
    end
    else
    begin
      GetUID:=MixedUID;
    end;
  except
    on E: Exception do
    begin
//      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.GetUID: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;



procedure TLimitSettingFm.imgDnMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin

  if mLimitStartFlag then
  begin
    if (cmbMotors.ItemIndex>=0)and(Trim(cmbMotors.Text)<>'')  then
    begin

      tmrDown.Enabled:=true;
    end;
  end
  else
  begin
    if (cmbMotors.ItemIndex>=0)and(Trim(cmbMotors.Text)<>'')  then
    begin

      mSentCom := StrToInt(VariablesUn.UID_DN);
      mUID:=GetAddressInt(cmbMotors.Text);
      ComFm.SetGenericParam(mUID, VariablesUn.UID_DN+'.0.0.0',rhLimitStatusRecieved);
    end;
  end;
end;


procedure TLimitSettingFm.imgDnMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  tmrDown.Enabled:=false;
end;

procedure TLimitSettingFm.imgIS1Click(Sender: TObject);
begin
  mSentCom := StrToInt(VariablesUn.UID_G1);
  mUID:=GetAddressInt(cmbMotors.Text);
  ComFm.SetGenericParam(mUID, VariablesUn.UID_G1+'.0.0.0',rhLimitStatusRecieved);
end;

procedure TLimitSettingFm.imgIS2Click(Sender: TObject);
begin
  mSentCom := StrToInt(VariablesUn.UID_G2);
  mUID:=GetAddressInt(cmbMotors.Text);
  ComFm.SetGenericParam(mUID, VariablesUn.UID_G2+'.0.0.0',rhLimitStatusRecieved);
end;

procedure TLimitSettingFm.imgIS3Click(Sender: TObject);
begin
  mSentCom := StrToInt(VariablesUn.UID_G3);
  mUID:=GetAddressInt(cmbMotors.Text);
  ComFm.SetGenericParam(mUID, VariablesUn.UID_G3+'.0.0.0',rhLimitStatusRecieved);
end;

procedure TLimitSettingFm.imgUpMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin

  if mLimitStartFlag then
  begin
    if (cmbMotors.ItemIndex>=0)and(Trim(cmbMotors.Text)<>'')  then
    begin

      tmrUp.Enabled:=true;
    end;
  end
  else
  begin
    if (cmbMotors.ItemIndex>=0)and(Trim(cmbMotors.Text)<>'')  then
    begin

      mSentCom := StrToInt(VariablesUn.UID_TOP);
      mUID:=GetAddressInt(cmbMotors.Text);
      ComFm.SetGenericParam(mUID, VariablesUn.UID_TOP+'.0.0.0',rhLimitStatusRecieved);
    end;
  end;
end;

procedure TLimitSettingFm.imgUpMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  tmrUp.Enabled:=false;
end;

Procedure TLimitSettingFm.SortStringListUIDS(UIDList:TStringList);
var
  I:Integer;
  First,Second:Integer;
  Swap:Boolean;
  TempStr:String;
begin
  try
    Swap:=true;
    while Swap do
    begin
      Swap:=False;
      for I := 0 to UIDList.Count-2 do
      begin
        First:=StrToInt(GetUID(UIDList[I]));
        Second:=StrToInt(GetUID(UIDList[I+1]));
        if First > Second then
        begin
          TempStr:=UIDList[I];
          UIDList[I]:=UIDList[I+1];
          UIDList[I+1]:=TempStr;
          Swap:=true;
        end;
      end;
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TLimitSettingFm.SortStringListUIDS: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;

end;

procedure TLimitSettingFm.tmrDownTimer(Sender: TObject);
begin
  mSentCom := StrToInt(VariablesUn.UID_CreepDN);
  mUID:=GetAddressInt(cmbMotors.Text);
  ComFm.SetGenericParam(mUID, VariablesUn.UID_CreepDN+'.0.0.'+IntToStr(mCreepStepSize),rhLimitStatusRecieved);
end;

procedure TLimitSettingFm.tmrUpTimer(Sender: TObject);
begin

    mSentCom := StrToInt(VariablesUn.UID_CreepUp);
    mUID:=GetAddressInt(cmbMotors.Text);

    ComFm.SetGenericParam(mUID, VariablesUn.UID_CreepUp+'.0.0.'+IntToStr(mCreepStepSize),rhLimitStatusRecieved);
//    if(WaitforSetResponse(mUID,  VariablesUn.UID_CreepUp+'.0.0.10', rhLimitStatusRecieved,500))then
//    begin
//      mLimitStartFlag:=true;
//      ShowMessage('Limit setting started.');
//      shpcmbSerialPerson.Brush.Color :=clLime;
//      shpcmbSerialPerson.Visible:=false;
//    end
//    else
//    begin
//      ShowMessage('Limit setting failed.');
//      shpcmbSerialPerson.Brush.Color :=ClRed;
//    end;
end;

function TLimitSettingFm.WaitforQueryResponse(UID:Integer;Command:String;rh:TNotifyEvent;Resend:Integer):bool;
var
  PauseCount:Integer;
begin
  try
    mPassFlag:=false;
    PauseCount:=0;
    ComFm.GetGenericStatus(UID,Command,rh);
    while not mPassFlag do
    begin
      PauseCount:=PauseCount+1;
      delay(1);
      if (PauseCount=Resend) or (PauseCount=Resend*2) then
      begin
        ComFm.GetGenericStatus(UID,Command,rh);
      end;
      if PauseCount>Resend*3 then
      begin
        ShowMessage('Failed Command');
        result:=false;
        exit;
      end;
    end;
    result:=true;
  except
    on E: Exception do
    begin
      MessageDlg('TLimitSettingFm.WaitforQueryResponse: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;
function TLimitSettingFm.WaitforSetResponse(UID:Integer;Command:String;rh:TNotifyEvent;Resend:Integer):bool;
var
  PauseCount:Integer;
begin
  try
    mPassFlag:=false;
    PauseCount:=0;
    ComFm.SetGenericParam(UID,Command,rh);
    while not mPassFlag do
    begin
      PauseCount:=PauseCount+1;
      delay(1);
      if (PauseCount=Resend) or (PauseCount=Resend*2) then
      begin
        ComFm.SetGenericParam(UID,Command,rh);
      end;
      if PauseCount>Resend*3 then
      begin
        ShowMessage('Failed Command');
        result:=false;
        exit;
      end;
    end;
    result:=true;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.WaitforSetResponse: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;
procedure TLimitSettingFm.rhLimitStatusRecieved(sender: TObject);
var
  BinStr,DataSetField:String;
  I:Integer;
begin
  try
     if (mSentCom = VariablesUn.giCommand) and (mUID = VariablesUn.giUID_rx) then
    begin
      case VariablesUn.giCommand of
        106,43,107,108:
        begin
          mPassFlag:=true;
        end;
        42:
        begin
          if VariablesUn.GetBit(VariablesUn.giDataH,1) then
          begin
            lblUpperLimitStatus.Caption:='SET';
            lblLowerLimitStatus.Caption:='SET';
          end
          else
          begin
            lblUpperLimitStatus.Caption:='NOT SET';
            lblLowerLimitStatus.Caption:='NOT SET';
          end;
          mPassFlag:=true;
        end;
      end;
    end;

  finally

  end;
end;

function TLimitSettingFm.GetAddress(vsAddress: String): String;
var viPos: Integer;
begin
  //viPos := LastDelimiter(':', vsAddress);
  viPos := LastDelimiter(' ', vsAddress);
  if viPos > 0 then
  begin
    Result := Copy(vsAddress, viPos + 1, 50)
  end
  else
  begin
    Result := vsAddress;
  end;
end;
function TLimitSettingFm.GetAddressInt(vsAddress: String): Integer;
var viPos: Integer;
begin
  //viPos := LastDelimiter(':', vsAddress);
  viPos := Pos(' ', vsAddress)-1;
  Result := StrToInt((vsAddress.Substring(0, viPos)));

end;

procedure TLimitSettingFm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if (cmbMotors.ItemIndex>=0)and(Trim(cmbMotors.Text)<>'')  then
  begin

    mSentCom := StrToInt(VariablesUn.lmt_StartStop);
    mUID:=GetAddressInt(cmbMotors.Text);
//  set to run mode
    ComFm.SetGenericParam(mUID, VariablesUn.lmt_StartStop+'.0.0.0',rhLimitStatusRecieved);
  end;

  DataUn.dmDataModule.dsMotors.Filtered := False;
  DataUn.dmDataModule.dsMWCChannels.Filtered := False;
  DataUn.dmDataModule.dsMWCs.Filtered := False;
  DataUn.dmDataModule.dsMNI.Filtered := False;
  DataUn.dmDataModule.dsMNI2.Filtered := False;
  DataUn.dmDataModule.dsIQMLC2.Filtered := False;
  DataUn.dmDataModule.dsPowerPanel.Filtered := False;

  DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
  DataUn.dmDataModule.dsMWCs.SaveToFile(VariablesUn.gsDataPath + 'dsMWCs.txt', dfXML);
  DataUn.dmDataModule.dsMWCChannels.SaveToFile(VariablesUn.gsDataPath + 'dsMWCChannels.txt', dfXML);
  DataUn.dmDataModule.dsMNI.SaveToFile(VariablesUn.gsDataPath + 'dsMNI.txt', dfXML);
  DataUn.dmDataModule.dsMNI2.SaveToFile(VariablesUn.gsDataPath + 'dsMNI2.txt', dfXML);
  DataUn.dmDataModule.dsIQMLC2.SaveToFile(VariablesUn.gsDataPath + 'dsIQMLC2.txt', dfXML);
  DataUn.dmDataModule.dsPowerPanel.SaveToFile(VariablesUn.gsDataPath + 'dsPowerPanel.txt', dfXML);
//  pnlMenuBar.Visible := False;

  Application.Terminate;
end;



procedure TLimitSettingFm.FormShow(Sender: TObject);
begin

  cmbMotors.Items.Clear;
  Top:=VariablesUn.giTop;
  Left := VariablesUn.giLeft;
  PopulateUidCmb;
  //get direction
  DataUn.dmDataModule.dsMotors.Filtered := False;
  DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + GetUID(cmbMotors.Text);
  DataUn.dmDataModule.dsMotors.Filtered := True;
  if not DataUn.dmDataModule.dsMotors.Eof then
  begin
    if (DataUn.dmDataModule.dsMotors.FieldByName('Direction').AsString='Reverse')then
    begin
      btnToggleDirection.caption:='Reverse';
    end
    else
    begin
      btnToggleDirection.caption:='Normal';
    end;
  end;
end;



procedure TLimitSettingFm.btnBackClick(Sender: TObject);
begin
  if (cmbMotors.ItemIndex>=0)and(Trim(cmbMotors.Text)<>'')  then
  begin

    mSentCom := StrToInt(VariablesUn.lmt_StartStop);
    mUID:=GetAddressInt(cmbMotors.Text);
//  set to run mode
    ComFm.SetGenericParam(mUID, VariablesUn.lmt_StartStop+'.0.0.0',rhLimitStatusRecieved);
  end;

  VariablesUn.giTop:=Top;
  VariablesUn.giLeft:=Left;
  LimitSettingFm.Hide;
  if Assigned (rh_ShowMain) then
     rh_ShowMain(Self);
end;

procedure TLimitSettingFm.JvCaptionButton1Click(Sender: TObject);
begin
  Application.HelpContext(4);
end;

procedure TLimitSettingFm.rhPortClosedLimitSetting(Sender: TObject);
begin
//  ButtonsDisable;
//  giComPortOn := False;
  if shape14<>nil then
  begin
    shape14.Pen.Color:=clRed;
  end;
//  VariablesUn.gbPowerOn:=True;
//  variablesUn.gbUSBFound:=False;
end;

procedure TLimitSettingFm.rhPortOpenLimitSetting(Sender: TObject);
begin
// if cmbMotors.Items.Count > 0 then
// begin
//    ButtonsEnable;
// end;

// giComPortOn := True;
  if shape14<>nil then
  begin
    shape14.Pen.Color:=clLime;
  end;
// VariablesUn.gbPowerOn:=False;
// variablesUn.gbUSBFound:=True;
end;

end.
