unit ASCIIUn;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ComUn, VariablesUn, DataUn, Vcl.StdCtrls,System.StrUtils;

type
  TASCIIFm = class(TForm)
    btnSendASCII: TButton;
    teSendASCII: TEdit;
    lbReceivedMessage: TListBox;
    Button1: TButton;
    procedure btnSendASCIIClick(Sender: TObject);
    procedure teSendASCIIKeyPress(Sender: TObject; var Key: Char);
    procedure ListBox2File(sFile: String; oList: TListBox);
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    procedure rhMessageRecieved(Sender: TObject);
  end;

var
  ASCIIFm: TASCIIFm;

implementation

{$R *.dfm}

procedure TASCIIFm.btnSendASCIIClick(Sender: TObject);
begin
  lbReceivedMessage.Items.Add(teSendASCII.Text);
  if(lbReceivedMessage.count>100)then
  begin
    lbReceivedMessage.items.delete(0);
  end;
  lbReceivedMessage.TopIndex := -1 + lbReceivedMessage.Items.Count;
  ComFm.SendASCII(teSendASCII.Text,rhMessageRecieved);
end;

procedure TASCIIFm.rhMessageRecieved(Sender: TObject);
var
  HolderString:String;
begin
  if gsmessage.Length>40 then
  begin
    while(gsmessage.Length>40)do
    begin
      HolderString:=leftstr(gsmessage,40);
      lbReceivedMessage.Items.Add(HolderString);
      delete(gsmessage,1,40);
    end;
    lbReceivedMessage.Items.Add(gsmessage);
  end
  else
  begin
    lbReceivedMessage.Items.Add(gsmessage);
  end;
  if(lbReceivedMessage.count>100)then
  begin
    while(lbReceivedMessage.count>100)do
    begin
      lbReceivedMessage.items.delete(0);
    end;
  end;
  lbReceivedMessage.TopIndex := -1 + lbReceivedMessage.Items.Count;
end;

procedure TASCIIFm.teSendASCIIKeyPress(Sender: TObject; var Key: Char);
begin
  if(key=#$D)then
  begin
    btnSendASCIIClick(nil);
  end;
end;

procedure TASCIIFm.Button1Click(Sender: TObject);
var
  saveDialog : TSaveDialog;
begin
  saveDialog := TSaveDialog.Create(self);
    saveDialog.Title := 'Save Channel Template';
    saveDialog.InitialDir := GetCurrentDir;
    saveDialog.Filter := 'Text files only|*.txt';
    saveDialog.DefaultExt := 'txt';
    saveDialog.FilterIndex := 1;
    if saveDialog.Execute then
    begin
      ListBox2File(saveDialog.FileName, lbReceivedMessage);
    end;

    saveDialog.Free;
end;

procedure TASCIIFm.FormCreate(Sender: TObject);
begin
  //SendMessage(lbReceivedMessage.Handle, LB_SETHORIZONTALEXTENT, 317, 0);
end;

procedure TASCIIFm.FormShow(Sender: TObject);
begin
  //SendMessage(lbReceivedMessage.Handle, LB_SETHORIZONTALEXTENT, 317, 0);
end;

procedure TASCIIFm.ListBox2File(sFile: String; oList: TListBox);
var
  fFile: TextFile;
  x: Integer;
begin
 AssignFile(fFile,sFile);
 Rewrite(fFile);
 x:=0;
 While x <> oList.Items.Count do
begin
 writeln(fFile,oList.Items[x]);
 inc(x);
end;
 CloseFile(fFile);
end;

end.
