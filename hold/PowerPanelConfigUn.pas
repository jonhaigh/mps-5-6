unit PowerPanelConfigUn;

interface

uses
  StrUtils,Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids, Vcl.ComCtrls, Data.Bind.EngExt,
  Vcl.Bind.DBEngExt,Data.DB,Datasnap.DBClient,
  System.Bindings.Outputs, Vcl.Bind.Editors, Data.Bind.Components,VariablesUn,
  System.Generics.Collections,DiscoverUn,AboutUn,NewProjectUn,
  Vcl.Imaging.jpeg, Vcl.Buttons, JvComponentBase, JvCaptionButton,System.UITypes,
  MotorMaintenanceDisplay;

type
  TPowerPanelConfigFm = class(TForm)
    Shape14: TShape;
    cmbDeviceSelector: TComboBox;
    pnlMenuBar: TPanel;
    cmbProjectPP: TComboBox;
    ScrollBox1: TScrollBox;
    lblUidPP: TLabel;
    cmbUidPP: TComboBox;
    pnlSerialParams: TPanel;
    lblSerialTitle: TLabel;
    pnlSwPort: TPanel;
    Image2: TImage;
    lblSwPortTitle: TLabel;
    lblSwPortMode: TLabel;
    teSWFWDAddr: TEdit;
    lblSwZgn: TLabel;
    cmbSWMode: TComboBox;
    lblSwPortEnabled: TLabel;
    cbSWEnabled: TCheckBox;
    lblSwPortFwdEnabled: TLabel;
    cbSWFWDEnabled: TCheckBox;
    pnlFirmwareRev: TPanel;
    Label23: TLabel;
    Label25: TLabel;
    teFirmwareRev: TEdit;
    txtAliasPP: TEdit;
    btUpdateParams: TPanel;
    cmbSWPerson: TComboBox;
    lblSwPortPerson: TLabel;
    teUIDPP: TEdit;
    btUpdateUID: TPanel;
    cbSerialEnabled: TCheckBox;
    lblSerialEnable: TLabel;
    shpcbSWEnabled: TShape;
    shpcbSWFWDEnabled: TShape;
    shpcmbSWPerson: TShape;
    shpcmbSWMode: TShape;
    shpcbSerialEnabled: TShape;
    btnExit: TImage;
    btnSettings: TImage;
    btnInfo: TImage;
    btnFind: TImage;
    btnMenu: TImage;
    btnConfig: TImage;
    btnControl: TImage;
    JvCaptionButton1: TJvCaptionButton;
    lblSerialBaud: TLabel;
    cmbSerialBaud: TComboBox;
    shpcmbSerialBaud: TShape;
    pnlMasterSw: TPanel;
    lblMasterSwTitle: TLabel;
    lblMasterSwMode: TLabel;
    lblMasterSwZgn: TLabel;
    lblMasterSwEnabled: TLabel;
    lblMasterSwFwdEnabled: TLabel;
    lblMasterSwPerson: TLabel;
    shpcbMasterSwEnabled: TShape;
    shpcbMasterSwFwdEnabled: TShape;
    shpcmbMasterSwPerson: TShape;
    shpcmbMasterSwMode: TShape;
    teMasterSwFwdAddr: TEdit;
    cmbMasterSwMode: TComboBox;
    cbMasterSwEnabled: TCheckBox;
    cbMasterSwFwdEnabled: TCheckBox;
    cmbMasterSwPerson: TComboBox;
    procedure FormShow(Sender: TObject);
    procedure cmbDeviceSelectorChange(Sender: TObject);
    procedure btnFindClick(Sender: TObject);
    procedure btnFindMouseEnter(Sender: TObject);
    procedure btnInfoMouseEnter(Sender: TObject);
    procedure btnSettingsMouseEnter(Sender: TObject);
    procedure btnExitMouseEnter(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure btnSettingsClick(Sender: TObject);
    procedure btnInfoClick(Sender: TObject);
    procedure Image2MouseEnter(Sender: TObject);
    procedure pnlMenuBarMouseLeave(Sender: TObject);
    procedure cmbUidPPChange(Sender: TObject);
    procedure cmbSwitchChange(Sender: TObject);
    procedure btUpdateParamsClick(Sender: TObject);
    procedure FormMouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure btUpdateUIDClick(Sender: TObject);
    procedure btUpdateParamsMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btUpdateParamsMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormHide(Sender: TObject);
    procedure cmbProjectPPChange(Sender: TObject);
    procedure txtAliasPPDblClick(Sender: TObject);
    procedure cbSerialEnabledClick(Sender: TObject);
    procedure cmbSerialBaudChange(Sender: TObject);
    procedure cbSWEnabledClick(Sender: TObject);
    procedure cmbSWPersonChange(Sender: TObject);
    procedure cbSWFWDEnabledClick(Sender: TObject);
    procedure cmbSWModeChange(Sender: TObject);
    procedure teSWFWDAddrChange(Sender: TObject);
    procedure txtAliasPPChange(Sender: TObject);
    procedure btnInfoMCWMouseEnter(Sender: TObject);
    procedure txtAliasPPEnterKey(Sender: TObject);
    procedure btnControlMouseEnter(Sender: TObject);
    procedure teUIDPPChange(Sender: TObject);
    procedure teUIDPPExit(Sender: TObject);
    procedure JvCaptionButton1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure teUIDPPKeyPress(Sender: TObject; var Key: Char);
    procedure cbMasterSwEnabledClick(Sender: TObject);
    procedure cmbMasterSwPersonChange(Sender: TObject);
    procedure cbMasterSwFwdEnabledClick(Sender: TObject);
    procedure teMasterSwFwdAddrChange(Sender: TObject);
    procedure cmbMasterSwModeChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    giSentCom :Integer;
    giUID     :Integer;
    LastUID:Integer;
    viUIDs : Array of Integer;
    timerDelayUid:Integer;
    vsOldAlias:String;
    PassFlag:boolean;
    procedure ComboBox_AutoWidth(const theComboBox: TCombobox);
    procedure DisableForm(Disable:Boolean);
    Procedure LoadProjects;
    Procedure LoadUID();
    procedure WaitforGenericResponse(UID:Integer;Command:String;rh:TNotifyEvent);
    Procedure ClearEdits;
    procedure Split(Delimiter: Char; Str: string; ListOfStrings: TStringlist);
    procedure PPUpdateParam;
    Procedure SortStringListUIDS(UIDList:TStringList);
    Function  WaitforGenericResponseConfirmation(UID:Integer;Command:String;rh:TNotifyEvent):boolean;
    Function  SingleEditTest:boolean;
    Function  GetDisabledMotorsIntConfig:Integer;
    Function  GetDisabledSwitchesIntDS:Integer;
    Function  GetEnabledFWDMOIntDS:Integer;
    Function  GetEnabledFWDMOIntConfig:Integer;
    Function  GetPortModeIntConfig:Integer;
    Function  GetPortModeIntDS:Integer;
    Function  ConvertBaudRate:String;
  public
    rh_ShowMain: TNotifyEvent;
    rh_ShowMNIDisplay:TNotifyEvent;
    rh_ShowMNI2Config: TNotifyEvent;
    rh_showIQMLC2Config: TNotifyEvent;
    procedure rhShowPP(Sender: TObject);
    procedure rhPortOpend(Sender: TObject);
    procedure rhPortClosed(Sender: TObject);
    procedure rhPPStatusRecieved(sender: TObject);
  end;

var
  PowerPanelConfigFm: TPowerPanelConfigFm;

implementation

{$R *.dfm}

uses ComUn, DataUn, MWCDisplayUn, ScreenSetupUn, MWCConfigUn, MNI2ConfigUn;

Procedure TPowerPanelConfigFm.SortStringListUIDS(UIDList:TStringList);
var
  I:Integer;
  First,Second:Integer;
  Swap:Boolean;
  TempStr:String;
begin
  Swap:=true;
  while Swap do
  begin
    Swap:=False;
    for I := 0 to UIDList.Count-2 do
    begin
      First:=StrToInt(UIDList[I]);
      Second:=StrToInt(UIDList[I+1]);
      if First > Second then
      begin
        TempStr:=UIDList[I];
        UIDList[I]:=UIDList[I+1];
        UIDList[I+1]:=TempStr;
        Swap:=true;
      end;
    end;
  end;
end;

//Changes Color of Com Indicator lights
procedure TPowerPanelConfigFm.rhPortOpend(Sender: TObject);
begin
  giComPortOn := True;
  Shape14.Pen.Color := clLime;
end;
//Changes Color of Com Indicator lights
procedure TPowerPanelConfigFm.rhPortClosed(Sender: TObject);
begin
  giComPortOn := False;
  Shape14.Pen.Color := clRed;
end;

Procedure TPowerPanelConfigFm.LoadUID();
var
  I:Integer;
  comboUIDList:TStringList;
begin
  comboUIDList:=TStringList.Create;
  cmbUidPP.Clear;
  begin
    if not (DataUn.dmDataModule.dsPowerPanel.State = dsInactive) then
    begin
      DataUn.dmDataModule.dsPowerPanel.Filtered := False;
      DataUn.dmDataModule.dsPowerPanel.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
      DataUn.dmDataModule.dsPowerPanel.Filtered := True;
    end;
    while not dmDataModule.dsPowerPanel.Eof do
    begin
     comboUIDList.Add(IntToStr(DataUn.dmDataModule.dsPowerPanel.FieldByName('UID').AsInteger));
     dmDataModule.dsPowerPanel.Next;
    end;
    SortStringListUIDS(comboUIDList);
    cmbUidPP.Items.Assign(comboUIDList);
    cmbUidPP.ItemIndex:=0;
  end;
  cmbUidPPChange(nil);
end;


//splits any string with delimiter='.'
procedure TPowerPanelConfigFm.Split(Delimiter: Char; Str: string; ListOfStrings: TStringlist) ;
begin
   ListOfStrings.Clear;
   ListOfStrings.Delimiter       := Delimiter;
   ListOfStrings.StrictDelimiter := True; // Requires D2006 or newer.
   ListOfStrings.DelimitedText   := Str;
end;

procedure TPowerPanelConfigFm.teMasterSwFwdAddrChange(Sender: TObject);
begin
  teMasterSwFwdAddr.Brush.Color:=clLime;
  btUpdateParams.Color:=clLime;
end;

procedure TPowerPanelConfigFm.teSWFWDAddrChange(Sender: TObject);
begin
  teSWFWDAddr.Brush.Color:=clLime;
  btUpdateParams.Color:=clLime;
end;

procedure TPowerPanelConfigFm.teUIDPPChange(Sender: TObject);
begin
  teUIDPP.color:=cllime;
  btUpdateUID.Color:=clLime;
end;

procedure TPowerPanelConfigFm.teUIDPPExit(Sender: TObject);
begin
  if Length(teUIDPP.Text)=5 then
  begin
    if StrToInt(teUIDPP.Text)>65535 then
    begin
      Showmessage('Value cannot exceed 65535');
      teUIDPP.SetFocus;
    end;
  end;
end;

procedure TPowerPanelConfigFm.teUIDPPKeyPress(Sender: TObject; var Key: Char);
begin
  if not CharInSet(ANSIChar(Key),[#8, '0'..'9']) then
  begin
    Key := #0;
  end;
end;

procedure TPowerPanelConfigFm.txtAliasPPChange(Sender: TObject);
begin
  txtAliasPP.Brush.Color:=clLime;
  txtAliasPP.Refresh;
  btUpdateParams.Color:=clLime;
end;

procedure TPowerPanelConfigFm.txtAliasPPDblClick(Sender: TObject);
begin

   NewProjectFm.rh_ShowLast:=rhShowPP;
   NewProjectFm.Show;
   Hide;
end;

procedure TPowerPanelConfigFm.txtAliasPPEnterKey(Sender: TObject);
var
  vsAlias,vsCommand:String;
  I:Integer;
begin
  try
    if VariablesUn.IsNumber(cmbUidPP.text) then
    begin
      if vsOldAlias<>trim(txtAliasPP.text) then
      begin

        vsOldAlias:=trim(txtAliasPP.text);
        giUID :=  StrToInt(cmbUidPP.text);
        vsAlias:=txtAliasPP.Text;
        vsAlias := vsAlias + StringOfChar ( Char(#32), 12 - Length(vsAlias) );
        vsCommand:=VariablesUn.UID_NAME+'.'+vsAlias;
        ComFm.SetGenericParam(giUID,vsCommand, rhPPStatusRecieved);
        Delay(200);


        DataUn.dmDataModule.dsPowerPanel.Filtered := False;
        DataUn.dmDataModule.dsPowerPanel.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidPP.Text;
        DataUn.dmDataModule.dsPowerPanel.Filtered := True;
        if not (DataUn.dmDataModule.dsPowerPanel.Eof) then
        begin
          DataUn.dmDataModule.dsPowerPanel.Edit;
          DataUn.dmDataModule.dsPowerPanel.FieldByName('Alias').AsString := txtAliasPP.Text;
          DataUn.dmDataModule.dsPowerPanel.Post;
          DataUn.dmDataModule.dsPowerPanel.MergeChangeLog;
          DataUn.dmDataModule.dsPowerPanel.SaveToFile(VariablesUn.gsDataPath + 'dsPowerPanel.txt', dfXML);

        end;

        for I := 1 to 4 do
          begin
            DataUn.dmDataModule.dsMotors.Filtered := False;
            DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt+ ' and MotorNumber = '+IntToStr(I);
            DataUn.dmDataModule.dsMotors.Filtered := True;
            if not (DataUn.dmDataModule.dsMotors.Eof) then
            begin
              DataUn.dmDataModule.dsMotors.Edit;
              DataUn.dmDataModule.dsMotors.FieldByName('Alias').AsString := txtAliasPP.Text;
              VariablesUn.gsAlias := txtAliasPP.Text;
              DataUn.dmDataModule.dsMotors.Post;
              DataUn.dmDataModule.dsMotors.MergeChangeLog;
              DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);

            end;
          end;

      end;
    end
    else
    begin
      txtAliasPP.Text := '';
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.lblAliasChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  End;

end;

procedure TPowerPanelConfigFm.btnControlMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TPowerPanelConfigFm.btnExitClick(Sender: TObject);
begin
  DataUn.dmDataModule.dsMotors.Filtered := False;
  DataUn.dmDataModule.dsMWCChannels.Filtered := False;
  DataUn.dmDataModule.dsMWCs.Filtered := False;
  DataUn.dmDataModule.dsMNI.Filtered := False;
  DataUn.dmDataModule.dsMNI2.Filtered := False;
  DataUn.dmDataModule.dsIQMLC2.Filtered := False;
  DataUn.dmDataModule.dsPowerPanel.Filtered := False;

  DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
  DataUn.dmDataModule.dsMWCs.SaveToFile(VariablesUn.gsDataPath + 'dsMWCs.txt', dfXML);
  DataUn.dmDataModule.dsMWCChannels.SaveToFile(VariablesUn.gsDataPath + 'dsMWCChannels.txt', dfXML);
  DataUn.dmDataModule.dsMNI.SaveToFile(VariablesUn.gsDataPath + 'dsMNI.txt', dfXML);
  DataUn.dmDataModule.dsMNI2.SaveToFile(VariablesUn.gsDataPath + 'dsMNI2.txt', dfXML);
  DataUn.dmDataModule.dsIQMLC2.SaveToFile(VariablesUn.gsDataPath + 'dsIQMLC2.txt', dfXML);
  DataUn.dmDataModule.dsPowerPanel.SaveToFile(VariablesUn.gsDataPath + 'dsPowerPanel.txt', dfXML);
  pnlMenuBar.Visible := False;
  Application.Terminate;
end;

procedure TPowerPanelConfigFm.rhShowPP(Sender: TObject);
begin
  show;
end;

procedure TPowerPanelConfigFm.btnExitMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TPowerPanelConfigFm.btnFindClick(Sender: TObject);
begin
  VariablesUn.gitop:=top;
  VariablesUn.giLeft:=left;
  discoverfm.rh_ShowPowerPanel:= rhShowPP;
  pnlMenuBar.Visible := False;
  Hide;
  DiscoverFm.Show;
end;

procedure TPowerPanelConfigFm.btnFindMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TPowerPanelConfigFm.btnInfoClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  Hide;
  AboutFm.rh_ShowForm := rhShowPP;
  AboutFm.Show;
end;

procedure TPowerPanelConfigFm.btnInfoMCWMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TPowerPanelConfigFm.btnInfoMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TPowerPanelConfigFm.rhPPStatusRecieved(sender: TObject);
var
  BinStr,DataSetField:String;
  I:Integer;
begin
  try
    if (giSentCom = VariablesUn.giCommand) and (giUID = VariablesUn.giUID_rx) then
    begin
      case VariablesUn.giCommand of
       11:
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsPowerPanel.Filtered := False;
          DataUn.dmDataModule.dsPowerPanel.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidPP.text;
          DataUn.dmDataModule.dsPowerPanel.Filtered := True;
          DataUn.dmDataModule.dsPowerPanel.Edit;
          for I := 0 to DataUn.dmDataModule.dsPowerPanel.RecordCount-1 do
          begin
            DataUn.dmDataModule.dsPowerPanel.FieldByName('UID').AsString:=teUIDPP.text;
          end;
          DataUn.dmDataModule.dsPowerPanel.MergeChangeLog;
          if DataUn.dmDataModule.dsSwitches.Active=true then
          begin
            DataUn.dmDataModule.dsSwitches.Filtered := False;
            DataUn.dmDataModule.dsSwitches.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidPP.text;
            DataUn.dmDataModule.dsSwitches.Filtered := True;
            DataUn.dmDataModule.dsSwitches.Edit;
            for I := 0 to DataUn.dmDataModule.dsSwitches.RecordCount-1 do
            begin
              DataUn.dmDataModule.dsSwitches.FieldByName('UID').AsString:=teUIDPP.text;
              DataUn.dmDataModule.dsSwitches.Next;
            end;
            DataUn.dmDataModule.dsSwitches.MergeChangeLog;
            DataUn.dmDataModule.dsSwitches.SaveToFile(VariablesUn.gsDataPath +'dsSwitch.txt', dfXML);
          end;
          DataUn.dmDataModule.dsPowerPanel.SaveToFile(VariablesUn.gsDataPath +'dsPowerPanel.txt', dfXML);
          DataUn.dmDataModule.dsMotors.Filtered := False;
          DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidPP.text;
          DataUn.dmDataModule.dsMotors.Filtered := True;

          while  not DataUn.dmDataModule.dsMotors.eof do
          begin
            DataUn.dmDataModule.dsMotors.Edit;
            DataUn.dmDataModule.dsMotors.FieldByName('UID').AsString:=teUIDPP.text;
            DataUn.dmDataModule.dsMotors.MergeChangeLog;
          end;

          DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath +'dsMotors.txt', dfXML);
          teUIDPP.color:=clWhite;
          btUpdateUID.Color:=clBtnFace;
        end;
       27:                   // Port Enabled
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsPowerPanel.Filtered := False;
          DataUn.dmDataModule.dsPowerPanel.Filter := 'ProjectName = ' + QuotedStr(cmbProjectPP.text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsPowerPanel.Filtered := True;
          dmDataModule.dsPowerPanel.Edit;
          if VariablesUn.GetBit(VariablesUn.giDataL,0)then
          begin
            DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort1Enabled').AsBoolean := False;
          end
          else
          begin
            DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort1Enabled').AsBoolean := true;
          end;
          if VariablesUn.GetBit(VariablesUn.giDataL,1)then
          begin
            DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort2Enabled').AsBoolean := False;
          end
          else
          begin
            DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort2Enabled').AsBoolean := true;
          end;
          if VariablesUn.GetBit(VariablesUn.giDataL,2)then
          begin
            DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort3Enabled').AsBoolean := False;
          end
          else
          begin
            DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort3Enabled').AsBoolean := true;
          end;
          if VariablesUn.GetBit(VariablesUn.giDataL,3)then
          begin
            DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort4Enabled').AsBoolean := False;
          end
          else
          begin
            DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort4Enabled').AsBoolean := true;
          end;


          DataUn.dmDataModule.dsPowerPanel.MergeChangeLog;

        end;
       28:                   // Port FWD Enabled
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsPowerPanel.Filtered := False;
          DataUn.dmDataModule.dsPowerPanel.Filter := 'ProjectName = ' + QuotedStr(cmbProjectPP.text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsPowerPanel.Filtered := True;
          dmDataModule.dsPowerPanel.Edit;
          if VariablesUn.GetBit(VariablesUn.giDataL,0)then
          begin
            DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort1FWDEnabled').AsBoolean := True;
          end
          else
          begin
            DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort1FWDEnabled').AsBoolean := False;
          end;
          if VariablesUn.GetBit(VariablesUn.giDataL,1)then
          begin
            DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort2FWDEnabled').AsBoolean := True;
          end
          else
          begin
            DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort2FWDEnabled').AsBoolean := False;
          end;
          if VariablesUn.GetBit(VariablesUn.giDataL,2)then
          begin
            DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort3FWDEnabled').AsBoolean := True;
          end
          else
          begin
            DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort3FWDEnabled').AsBoolean := False;
          end;
          if VariablesUn.GetBit(VariablesUn.giDataL,3)then
          begin
            DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort4FWDEnabled').AsBoolean := True;
          end
          else
          begin
            DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort4FWDEnabled').AsBoolean := False;
          end;


          DataUn.dmDataModule.dsPowerPanel.MergeChangeLog;

        end;
       29:                   // Button Personality
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsPowerPanel.Filtered := False;
          DataUn.dmDataModule.dsPowerPanel.Filter := 'ProjectName = ' + QuotedStr(cmbProjectPP.text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsPowerPanel.Filtered := True;
          dmDataModule.dsPowerPanel.Edit;
          if(variablesUn.giDataH=1) then
          begin
            DataUn.dmDataModule.dsPowerPanel.FieldByName('SW1BtnPerson').AsString := IntToStr(VariablesUn.giDataL);
            DataUn.dmDataModule.dsPowerPanel.MergeChangeLog;
          end
          else if(variablesUn.giDataH=2) then
          begin
            DataUn.dmDataModule.dsPowerPanel.FieldByName('SW2BtnPerson').AsString := IntToStr(VariablesUn.giDataL);
            DataUn.dmDataModule.dsPowerPanel.MergeChangeLog;
          end
          else if(variablesUn.giDataH=3) then
          begin
            DataUn.dmDataModule.dsPowerPanel.FieldByName('SW3BtnPerson').AsString := IntToStr(VariablesUn.giDataL);
            DataUn.dmDataModule.dsPowerPanel.MergeChangeLog;
          end
          else if(variablesUn.giDataH=4) then
          begin
            DataUn.dmDataModule.dsPowerPanel.FieldByName('SW4BtnPerson').AsString := IntToStr(VariablesUn.giDataL);
            DataUn.dmDataModule.dsPowerPanel.MergeChangeLog;
          end;

        end;
       36:                   // Port Mode
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsPowerPanel.Filtered := False;
          DataUn.dmDataModule.dsPowerPanel.Filter := 'ProjectName = ' + QuotedStr(cmbProjectPP.text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsPowerPanel.Filtered := True;
          dmDataModule.dsPowerPanel.Edit;
          if VariablesUn.GetBit(VariablesUn.giDataL,0)then
          begin
            DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort1Mode').AsInteger := 1;
          end
          else
          begin
            DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort1Mode').AsInteger := 0;
          end;
          if VariablesUn.GetBit(VariablesUn.giDataL,1)then
          begin
            DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort2Mode').AsInteger := 1;
          end
          else
          begin
            DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort2Mode').AsInteger := 0;
          end;
          if VariablesUn.GetBit(VariablesUn.giDataL,2)then
          begin
            DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort3Mode').AsInteger := 1;
         end
          else
          begin
            DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort3Mode').AsInteger := 0;
          end;
          if VariablesUn.GetBit(VariablesUn.giDataL,3)then
          begin
            DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort4Mode').AsInteger := 1;
          end
          else
          begin
            DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort4Mode').AsInteger := 0;
          end;

          DataUn.dmDataModule.dsPowerPanel.MergeChangeLog;

        end;
       44:                   // switch 1 ZGN
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsPowerPanel.Filtered := False;
          DataUn.dmDataModule.dsPowerPanel.Filter := 'ProjectName = ' + QuotedStr(cmbProjectPP.text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsPowerPanel.Filtered := True;
          dmDataModule.dsPowerPanel.Edit;
          DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort1FWDAddr').AsString := VariablesUn.BuildZGN();
          DataUn.dmDataModule.dsPowerPanel.MergeChangeLog;

        end;
       45:                   // switch 2 ZGN
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsPowerPanel.Filtered := False;
          DataUn.dmDataModule.dsPowerPanel.Filter := 'ProjectName = ' + QuotedStr(cmbProjectPP.text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsPowerPanel.Filtered := True;
          dmDataModule.dsPowerPanel.Edit;
          DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort2FWDAddr').AsString := VariablesUn.BuildZGN();
          DataUn.dmDataModule.dsPowerPanel.MergeChangeLog;

        end;
       46:                   // switch 3 ZGN
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsPowerPanel.Filtered := False;
          DataUn.dmDataModule.dsPowerPanel.Filter := 'ProjectName = ' + QuotedStr(cmbProjectPP.text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsPowerPanel.Filtered := True;
          dmDataModule.dsPowerPanel.Edit;
          DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort3FWDAddr').AsString := VariablesUn.BuildZGN();
          DataUn.dmDataModule.dsPowerPanel.MergeChangeLog;

        end;
       47:                   // switch 4 ZGN
        begin
         PassFlag:=True;
          DataUn.dmDataModule.dsPowerPanel.Filtered := False;
          DataUn.dmDataModule.dsPowerPanel.Filter := 'ProjectName = ' + QuotedStr(cmbProjectPP.text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsPowerPanel.Filtered := True;
          dmDataModule.dsPowerPanel.Edit;
          DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort4FWDAddr').AsString := VariablesUn.BuildZGN();
          DataUn.dmDataModule.dsPowerPanel.MergeChangeLog;

        end;
       85:                   // Alias
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsPowerPanel.Filtered := False;
          DataUn.dmDataModule.dsPowerPanel.Filter := 'ProjectName = ' + QuotedStr(cmbProjectPP.text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsPowerPanel.Filtered := True;
          dmDataModule.dsPowerPanel.Edit;
          DataUn.dmDataModule.dsPowerPanel.FieldByName('Alias').AsString := Trim(VariablesUn.gsMotorType);
          DataUn.dmDataModule.dsPowerPanel.MergeChangeLog;

        end;

        134:                   // Serial Settings
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsPowerPanel.Filtered := False;
          DataUn.dmDataModule.dsPowerPanel.Filter := 'ProjectName = ' + QuotedStr(cmbProjectPP.text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsPowerPanel.Filtered := True;
          dmDataModule.dsPowerPanel.Edit;
          if(variablesUn.giDataH=2)then  //Baud Rate
          begin
              DataUn.dmDataModule.dsPowerPanel.FieldByName('SerialBaud').AsInteger := variablesUn.giDataL;
          end
          else if(variablesUn.giDataH=7)then  //Serial Deisable
          begin
            DataUn.dmDataModule.dsPowerPanel.FieldByName('SerialDisable').AsInteger := variablesUn.giDataL;
          end
          else if(variablesUn.giDataH=9)then  //Physical Layer
          begin
            if(VariablesUn.GetBit(variablesUn.giDataL,0))then
            begin
              DataUn.dmDataModule.dsPowerPanel.FieldByName('SerialReceiver').AsBoolean := True;
            end;
            if(VariablesUn.GetBit(variablesUn.giDataL,1))then
            begin
              DataUn.dmDataModule.dsPowerPanel.FieldByName('SerialTransmitter').AsBoolean := True;
            end;
            BinStr:=VariablesUn.IntToBin(variablesUn.giDataL);
            BinStr:=LeftStr(BinStr,6);
            BinStr:=RightStr(BinStr,4);
            DataUn.dmDataModule.dsPowerPanel.FieldByName('PLPerson').AsInteger:=VariablesUn.BinToInt(BinStr);
          end
          else if(variablesUn.giDataH=10)then  //Out Going Message Structure
          begin
            if(VariablesUn.GetBit(variablesUn.giDataL,0))then
            begin
              DataUn.dmDataModule.dsPowerPanel.FieldByName('SerialUIDInZGN').AsInteger := 1;
            end
            else
            begin
              DataUn.dmDataModule.dsPowerPanel.FieldByName('SerialUIDInZGN').AsInteger := 0;
            end;
            if(VariablesUn.GetBit(variablesUn.giDataL,1))then
            begin
              DataUn.dmDataModule.dsPowerPanel.FieldByName('SerialRoutingField').AsInteger := 1;
            end
            else
            begin
              DataUn.dmDataModule.dsPowerPanel.FieldByName('SerialRoutingField').AsInteger := 0;
            end;
            if(VariablesUn.GetBit(variablesUn.giDataL,2))then
            begin
              DataUn.dmDataModule.dsPowerPanel.FieldByName('SerialCheckSum').AsInteger := 1;
            end
            else
            begin
              DataUn.dmDataModule.dsPowerPanel.FieldByName('SerialCheckSum').AsInteger := 0;
            end;
          end;

          DataUn.dmDataModule.dsPowerPanel.MergeChangeLog;

        end;

       139:                   // Alias
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsPowerPanel.Filtered := False;
          DataUn.dmDataModule.dsPowerPanel.Filter := 'ProjectName = ' + QuotedStr(cmbProjectPP.text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsPowerPanel.Filtered := True;
          dmDataModule.dsPowerPanel.Edit;
          DataUn.dmDataModule.dsPowerPanel.FieldByName('SystemMode').AsInteger := VariablesUn.giDataL;
          DataUn.dmDataModule.dsPowerPanel.MergeChangeLog;

        end;
       202:                   // BaseCode Rev    SerialOutMess
        begin
          PassFlag:=True;
          DataUn.dmDataModule.dsPowerPanel.Filtered := False;
          DataUn.dmDataModule.dsPowerPanel.Filter := 'ProjectName = ' + QuotedStr(cmbProjectPP.text) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsPowerPanel.Filtered := True;
          dmDataModule.dsPowerPanel.Edit;
          DataUn.dmDataModule.dsPowerPanel.FieldByName('FirmwareRev').AsString := VariablesUn.BuildFirmwareRev;
          DataUn.dmDataModule.dsPowerPanel.MergeChangeLog;

        end;


      end;
    end
    else if giSentCom=1 then
    begin

      SetLength(viUIDs,Length(viUIDs)+1);
      viUIDs[Length(viUIDs)-1] := VariablesUn.giUID_rx;
      timerDelayUid:=timerDelayUid+1;

    end;
  except
  on E: Exception do
  begin
    ComFm.rh_ReturnHandler := nil;
    dmDataModule.dsPowerPanel.Cancel;
    Screen.Cursor := crDefault;
    MessageDlg('TDiscoverFm.rhStatusRecieved: ' + E.Message, mtWarning, [mbOk], 0);
  end;
 end;
end;



procedure TPowerPanelConfigFm.cbMasterSwEnabledClick(Sender: TObject);
begin
  shpcbMasterSwEnabled.Visible:=true;
  btUpdateParams.Color:=clLime;
end;

procedure TPowerPanelConfigFm.cbMasterSwFwdEnabledClick(Sender: TObject);
begin
  shpcbMasterSwFwdEnabled.Visible:=true;
  btUpdateParams.Color:=clLime;
end;

procedure TPowerPanelConfigFm.cbSerialEnabledClick(Sender: TObject);
begin
  shpcbSerialEnabled.Visible:=true;
  btUpdateParams.Color:=clLime;
end;

procedure TPowerPanelConfigFm.cbSWEnabledClick(Sender: TObject);
begin
  shpcbSWEnabled.Visible:=true;
  btUpdateParams.Color:=clLime;
end;

procedure TPowerPanelConfigFm.cbSWFWDEnabledClick(Sender: TObject);
begin
  shpcbSWFWDEnabled.Visible:=true;
  btUpdateParams.Color:=clLime;
end;

procedure TPowerPanelConfigFm.btUpdateParamsClick(Sender: TObject);
var
  AliasText,SerialBaud,ZgnFormat:String;
  intDS,IntConfig,SwitchPerson:Integer;
  lslZGNparts:TStringList;
begin
  lslZGNparts:=TStringList.Create;
  DataUn.dmDataModule.dsPowerPanel.Filtered := False;
  DataUn.dmDataModule.dsPowerPanel.Filter := 'ProjectName = ' + QuotedStr(cmbProjectPP.Text) +  ' and UID = ' + cmbUidPP.Text;
  DataUn.dmDataModule.dsPowerPanel.Filtered := True;
  giUID:=StrToInt(cmbUidPP.Text);
  //Serial Params

 if (cbSerialEnabled.Checked=true) and (DataUn.dmDataModule.dsPowerPanel.FieldByName('SerialDisable').AsInteger=1)then
  begin
    VariablesUn.ClearData;
    giSentCom := StrToInt(LeftStr(VariablesUn.MNI_SerialEnable,3));
    ComFm.SetGenericParam(giUID, VariablesUn.MNI_SerialEnable+'.0.0', rhPPStatusRecieved);
    if WaitforGenericResponseConfirmation(giUID, VariablesUn.MNI_SerialEnable+'.0.0', rhPPStatusRecieved)then
    begin
      shpcbSerialEnabled.Brush.Color:=clLime;
      shpcbSerialEnabled.Visible:=false;
    end
    else
    begin
      shpcbSerialEnabled.Brush.Color:=clRed;
    end;
  end
  else if (cbSerialEnabled.Checked=false) and (DataUn.dmDataModule.dsPowerPanel.FieldByName('SerialDisable').AsInteger=0)then
  begin
    VariablesUn.ClearData;
    giSentCom := StrToInt(LeftStr(VariablesUn.MNI_SerialEnable,3));
    ComFm.SetGenericParam(giUID, VariablesUn.MNI_SerialEnable+'.0.1', rhPPStatusRecieved);
    if WaitforGenericResponseConfirmation(giUID, VariablesUn.MNI_SerialEnable+'.0.1', rhPPStatusRecieved) then
    begin
      shpcbSerialEnabled.Brush.Color:=clLime;
      shpcbSerialEnabled.Visible:=false;
    end
    else
    begin
      shpcbSerialEnabled.Brush.Color:=clRed;
    end;
  end;

  //Serial Baud
  SerialBaud:=IntToStr(cmbSerialBaud.ItemIndex+1);
  if SerialBaud<>DataUn.dmDataModule.dsPowerPanel.FieldByName('SerialBaud').AsString then
  begin
    giSentCom := StrToInt(LeftStr(VariablesUn.MNI_SerialBaud,3));
    ComFm.SetGenericParam(giUID, VariablesUn.MNI_SerialBaud+'.0.'+SerialBaud, rhPPStatusRecieved);
    if WaitforGenericResponseConfirmation(giUID, VariablesUn.MNI_SerialBaud+'.0.'+SerialBaud, rhPPStatusRecieved)then
    begin
      shpcmbSerialBaud.Brush.Color :=clLime;
      shpcmbSerialBaud.Visible:=false;
    end
    else
    begin
      shpcmbSerialBaud.Brush.Color :=ClRed;
    end;
  end;

  //SW PORT Button Personality
  SwitchPerson:=0;
  if(cmbSWPerson.Text='3T') then
  begin
    SwitchPerson:=9;
  end
  else if(cmbSWPerson.Text='3L') then
  begin
    SwitchPerson:=17;
  end
  else if(cmbSWPerson.Text='3M') then
  begin
    SwitchPerson:=33;
  end
  else if(cmbSWPerson.Text='2L') then
  begin
    SwitchPerson:=18;
  end
  else if(cmbSWPerson.Text='2M') then
  begin
    SwitchPerson:=34;
  end
  else if(cmbSWPerson.Text='1A') then
  begin
    SwitchPerson:=4;
  end
  else if(cmbSWPerson.Text='1B') then
  begin
    SwitchPerson:=68;
  end
  else if(cmbSWPerson.Text='1C') then
  begin
    SwitchPerson:=132;
  end;
  if DataUn.dmDataModule.dsPowerPanel.FieldByName('SW1BtnPerson').AsInteger<>SwitchPerson then
  begin
    VariablesUn.ClearData;
    giSentCom := StrToInt(VariablesUn.UID_BP);
    ComFm.SetGenericParam(giUID, VariablesUn.UID_BP+'.1.0.'+IntToStr(SwitchPerson), rhPPStatusRecieved);
    if WaitforGenericResponseConfirmation(giUID, VariablesUn.UID_BP+'.1.0.'+IntToStr(SwitchPerson), rhPPStatusRecieved) then
    begin
      shpcmbSWPerson.Brush.Color:=clLime;
      shpcmbSWPerson.Visible:=false;
    end
    else
    begin
      shpcmbSWPerson.Brush.Color:=clRed;
    end;
  end;

  // Switch ZGN
  if not VariablesUn.IsZGN(teSWFWDAddr.Text) and not (VariablesUn.IsG('.', Trim(teSWFWDAddr.Text), lslZGNparts))then
  begin
    teSWFWDAddr.Color := clRed;
    teSWFWDAddr.SetFocus;
    MessageDlg('Please enter a valid SW Port ZGN Address: n.n.n (n=0-255). Be advised �0� and �255� will act as a master address.', mtWarning, [mbOK], 0);
  end
  else
  begin
    if VariablesUn.IsG('.', Trim(teSWFWDAddr.Text), lslZGNparts) then
    begin
      ZgnFormat:='255.'+Trim(teSWFWDAddr.Text)+'.255';
    end
    else
    begin
      ZgnFormat:=Trim(teSWFWDAddr.Text);
    end;
    if DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort1FWDAddr').AsString<> teSWFWDAddr.Text then
    begin
      VariablesUn.ClearData;
      giSentCom := StrToInt(VariablesUn.UID_LS1_ADDR);
      ComFm.SetGenericParam(giUID, VariablesUn.UID_LS1_ADDR+'.'+ ZgnFormat, rhPPStatusRecieved);
      if WaitforGenericResponseConfirmation(giUID, VariablesUn.UID_LS1_ADDR+'.'+ ZgnFormat, rhPPStatusRecieved)then
      begin
        teSWFWDAddr.Color:=clWhite;

      end
      else
      begin
         teSWFWDAddr.Color:=clRed;
      end;
    end;
  end;

  //Master SW Button Personality
  SwitchPerson:=0;
  if(cmbMasterSwPerson.Text='3T') then
  begin
    SwitchPerson:=9;
  end
  else if(cmbMasterSwPerson.Text='3L') then
  begin
    SwitchPerson:=17;
  end
  else if(cmbMasterSwPerson.Text='3M') then
  begin
    SwitchPerson:=33;
  end
  else if(cmbMasterSwPerson.Text='2L') then
  begin
    SwitchPerson:=18;
  end
  else if(cmbMasterSwPerson.Text='2M') then
  begin
    SwitchPerson:=34;
  end
  else if(cmbMasterSwPerson.Text='1A') then
  begin
    SwitchPerson:=4;
  end
  else if(cmbMasterSwPerson.Text='1B') then
  begin
    SwitchPerson:=68;
  end
  else if(cmbMasterSwPerson.Text='1C') then
  begin
    SwitchPerson:=132;
  end;
  if DataUn.dmDataModule.dsPowerPanel.FieldByName('SW4BtnPerson').AsInteger<>SwitchPerson then
  begin
    VariablesUn.ClearData;
    giSentCom := StrToInt(VariablesUn.UID_BP);
    ComFm.SetGenericParam(giUID, VariablesUn.UID_BP+'.4.0.'+IntToStr(SwitchPerson), rhPPStatusRecieved);
    if WaitforGenericResponseConfirmation(giUID, VariablesUn.UID_BP+'.4.0.'+IntToStr(SwitchPerson), rhPPStatusRecieved) then
    begin
      shpcmbMasterSwPerson.Brush.Color:=clLime;
      shpcmbMasterSwPerson.Visible:=false;
    end
    else
    begin
      shpcmbMasterSwPerson.Brush.Color:=clRed;
    end;
  end;

  //Master Switch ZGN
  if not VariablesUn.IsZGN(teMasterSwFwdAddr.Text) and not (VariablesUn.IsG('.', Trim(teMasterSwFwdAddr.Text), lslZGNparts))then
  begin
    teMasterSwFwdAddr.Color := clRed;
    teMasterSwFwdAddr.SetFocus;
    MessageDlg('Please enter a valid Master Switch ZGN Address: n.n.n (n=0-255). Be advised �0� and �255� will act as a master address.', mtWarning, [mbOK], 0);
  end
  else
  begin
    if VariablesUn.IsG('.', Trim(teMasterSwFwdAddr.Text), lslZGNparts) then
    begin
      ZgnFormat:='255.'+Trim(teMasterSwFwdAddr.Text)+'.255';
    end
    else
    begin
      ZgnFormat:=Trim(teMasterSwFwdAddr.Text);
    end;
    if DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort4FWDAddr').AsString<> teMasterSwFwdAddr.Text then
    begin
      VariablesUn.ClearData;
      giSentCom := StrToInt(VariablesUn.UID_LS4_ADDR);
      ComFm.SetGenericParam(giUID, VariablesUn.UID_LS4_ADDR+'.'+ZgnFormat, rhPPStatusRecieved);
      if WaitforGenericResponseConfirmation(giUID, VariablesUn.UID_LS4_ADDR+'.'+ ZgnFormat, rhPPStatusRecieved)then
      begin
        teMasterSwFwdAddr.Color:=clWhite;

      end
      else
      begin
         teMasterSwFwdAddr.Color:=clRed;
      end;
    end;
  end;

  //All Ports Enabled
  intDS:= GetDisabledSwitchesIntDS;
  intConfig:= GetDisabledMotorsIntConfig;
  if(intDS<>intConfig) then
  begin
    VariablesUn.ClearData;
    giSentCom := StrToInt(VariablesUn.UID_LSE);
    ComFm.SetGenericParam(giUID, VariablesUn.UID_LSE+'.0.0.'+IntToStr(intConfig), rhPPStatusRecieved);
    if WaitforGenericResponseConfirmation(giUID, VariablesUn.UID_LSE+'.0.0.'+IntToStr(intConfig), rhPPStatusRecieved)then
    begin
      shpcbSWEnabled.Brush.color:=clLime;
      shpcbSWEnabled.Visible:=false;
      shpcbMasterSwEnabled.Brush.color:=clLime;
      shpcbMasterSwEnabled.Visible:=false;
    end
    else
    begin
      shpcbSWEnabled.Brush.Color:=clRed;
      shpcbMasterSwEnabled.Brush.color:=clRed;
    end;
  end;

  //All Ports Fwd Enabled
  intDS:= GetEnabledFWDMOIntDS;
  intConfig:= GetEnabledFWDMOIntConfig;
  if(intDS<>intConfig) then
  begin
    VariablesUn.ClearData;
    giSentCom := StrToInt(VariablesUn.UID_LSF);
    ComFm.SetGenericParam(giUID, VariablesUn.UID_LSF+'.0.0.'+IntToStr(intConfig), rhPPStatusRecieved);
    if WaitforGenericResponseConfirmation(giUID, VariablesUn.UID_LSF+'.0.0.'+IntToStr(intConfig), rhPPStatusRecieved) then
    begin
      shpcbSWFWDEnabled.Brush.Color:=clLime;
      shpcbSWFWDEnabled.Visible:=false;
    end
    else
    begin
      shpcbSWFWDEnabled.Brush.Color:=clRed;
    end;
  end;

  //All Ports Mode
  intDS:= GetPortModeIntDS;
  intConfig:= GetPortModeIntConfig;
  if(intDS<>intConfig) then
  begin
    VariablesUn.ClearData;
    giSentCom := StrToInt(VariablesUn.UID_PortMode);
    ComFm.SetGenericParam(giUID, VariablesUn.UID_PortMode+'.0.0.'+IntToStr(intConfig), rhPPStatusRecieved);
    if WaitforGenericResponseConfirmation(giUID, VariablesUn.UID_PortMode+'.0.0.'+IntToStr(intConfig), rhPPStatusRecieved)then
    begin
      shpcmbSWMode.Brush.Color:=clLime;
      shpcmbSWMode.Visible:=false;
    end
    else
    begin
      shpcmbSWMode.Brush.Color:=clRed;
    end;
  end;

  //Alias
  if(txtAliasPP.Text<>DataUn.dmDataModule.dsPowerPanel.FieldByName('Alias').AsString) then
  begin
    AliasText:= txtAliasPP.text;
    AliasText:=AliasText+ StringOfChar ( Char(#32), 12 - Length(AliasText) );
    setLength(AliasText,12);
    VariablesUn.ClearData;
    giSentCom := StrToInt(VariablesUn.UID_NAME);
    ComFm.SetGenericParam(giUID, VariablesUn.UID_NAME+'.'+AliasText, rhPPStatusRecieved);
    if WaitforGenericResponseConfirmation(giUID, VariablesUn.UID_NAME+'.'+AliasText, rhPPStatusRecieved)then
    begin
      txtAliasPP.Color:=clWhite;
    end
    else
    begin
      txtAliasPP.Color:=clRed;
    end;
  end
  else
  begin
    txtAliasPP.Brush.Color:=clWhite;
  end;
//  if SingleEditTest then
//  begin
  btUpdateParams.Color:=clBtnFace;
//  end;
  PPUpdateParam;
  //refresh checkboxs
  cbMasterSwEnabled.Refresh;
  cbMasterSwFwdEnabled.Refresh;
  cbSwEnabled.Refresh;
  cbSwFwdEnabled.Refresh;
  cbSerialEnabled.Refresh;
  teSWFWDAddr.Refresh;
  teMasterSwFwdAddr.Refresh;

end;

procedure TPowerPanelConfigFm.btUpdateParamsMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
if Sender is TPanel
  then with TPanel(Sender)
       do BevelOuter := bvLowered;
end;

procedure TPowerPanelConfigFm.btUpdateParamsMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
if Sender is TPanel
  then with TPanel(Sender)
       do BevelOuter := bvRaised;
end;
procedure TPowerPanelConfigFm.disableForm(Disable:Boolean);
begin
  if Disable then
  begin
    btupdateParams.enabled:=false;
    btUpdateUID.enabled:=false;
    teUIDPP.enabled:=false;
    cmbUidPP.enabled:=false;
    image2.enabled:=false;
    cmbDeviceSelector.enabled:=false;
    txtAliasPP.enabled:=false;
    cmbProjectPP.Enabled:=false;
  end
  else
  begin
    btupdateParams.enabled:=True;
    btUpdateUID.enabled:=True;
    teUIDPP.enabled:=True;
    cmbUidPP.enabled:=True;
    image2.enabled:=True;
    cmbDeviceSelector.enabled:=True;
    txtAliasPP.enabled:=True;
    cmbProjectPP.Enabled:=true;
  end;

end;
procedure TPowerPanelConfigFm.btUpdateUIDClick(Sender: TObject);
var
  I:Integer;
begin
  if dmDataModule.DuplicateUIDCheck(teUIDPP.Text) then
  begin
    ShowMEssage('Error: UID already exist. Choose Different ID.');
    exit;
  end;

  teUIDPP.OnChange:=nil;
  Screen.Cursor := crHourGlass;
  disableForm(True);
  if teUIDPP.Text<>'' then
  begin
    if(VariablesUn.isnumber(teUIDPP.text))and(teUIDPP.text<>IntToStr(LastUID))then
    begin
      giUID := StrToInt(teUIDPP.Text);
      giSentCom := StrToInt(VariablesUn.UID_ChangeUID);
      ComFm.SetGenericParam(lastUID, VariablesUn.UID_ChangeUID+'.0.'+IntToStr(Hi(StrToInt(teUIDPP.text)))+'.'+IntToStr(Lo(StrToInt(teUIDPP.text))), rhPPStatusRecieved);  //Sets UID
      if WaitforGenericResponseConfirmation(lastUID, VariablesUn.UID_ChangeUID+'.0.'+IntToStr(Hi(StrToInt(teUIDPP.text)))+'.'+IntToStr(Lo(StrToInt(teUIDPP.text))), rhPPStatusRecieved) then
      begin
        teUIDPP.Color:=clwhite;
      end
      else
      begin
        teUIDPP.Color:=clRed;
      end;
      btUpdateParams.Color:=clbtnface;
    end;
    cmbUidPP.Clear;
    LastUID:=StrToInt(teUIDPP.text);
    DataUn.dmDataModule.dsPowerPanel.Filtered := False;
    DataUn.dmDataModule.dsPowerPanel.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsPowerPanel.Filtered := True;
    for I := 0 to DataUn.dmDataModule.dsPowerPanel.RecordCount-1 do
    begin
      cmbUidPP.AddItem(IntToStr(DataUn.dmDataModule.dsPowerPanel.FieldByName('UID').AsInteger),Nil);
      DataUn.dmDataModule.dsPowerPanel.Next;
    end;
    cmbUidPP.ItemIndex:=cmbUidPP.Items.IndexOf(teUIDPP.text);
  end
  else
  begin
    showMessage('UID Update Fail: No UID Entered!');
    btUpdateParams.Color:=clbtnface;
    teUIDPP.Color:=clwhite;
  end;

  DisableForm(false);
  teUIDPP.Clear;

  teUIDPP.OnChange:=teUIDPPChange;
  self.Enabled:=true;
  Screen.Cursor := crDefault;
end;

Function TPowerPanelConfigFm.GetDisabledSwitchesIntDS:Integer;
var Binary:String;
begin
  Binary:='0100';
  if DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort4Enabled').AsBoolean then
  begin
    Binary:=Binary+'0';
  end
  else
  begin
    Binary:=Binary+'1';
  end;
  Binary:=Binary+'11';
  if DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort1Enabled').AsBoolean then
  begin
    Binary:=Binary+'0';
  end
  else
  begin
    Binary:=Binary+'1';
  end;
  GetDisabledSwitchesIntDS:=VariablesUn.BinToInt(Binary);
end;

Function TPowerPanelConfigFm.GetDisabledMotorsIntConfig:Integer;
var Binary:String;
begin
  Binary:='0100';
  if(cbMasterSwEnabled.checked)then
  begin
    Binary:=Binary+'0';
  end
  else
  begin
    Binary:=Binary+'1';
  end;
  Binary:=Binary+'11';

  if(cbSwEnabled.checked)then
  begin
    Binary:=Binary+'0';
  end
  else
  begin
    Binary:=Binary+'1';
  end;
  GetDisabledMotorsIntConfig:=VariablesUn.BinToInt(Binary)
end;

Function TPowerPanelConfigFm.GetEnabledFWDMOIntConfig:Integer;
var Binary:String;
begin
  Binary:='0000';
  if(cbMasterSwFwdEnabled.checked)then
  begin
    Binary:=Binary+'1';
  end
  else
  begin
    Binary:=Binary+'0';
  end;
  Binary:=Binary+'00';
  if(cbSwFwdEnabled.checked)then
  begin
    Binary:=Binary+'1';
  end
  else
  begin
    Binary:=Binary+'0';
  end;
  GetEnabledFWDMOIntConfig:=VariablesUn.BinToInt(Binary);
end;

Function TPowerPanelConfigFm.GetEnabledFWDMOIntDS:Integer;
var Binary:String;
begin
  Binary:='0000';
  if DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort4FWDEnabled').AsBoolean then
  begin
    Binary:=Binary+'1';
  end
  else
  begin
    Binary:=Binary+'0';
  end;
  Binary:=Binary+'00';
  if DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort1FWDEnabled').AsBoolean then
  begin
    Binary:=Binary+'1';
  end
  else
  begin
    Binary:=Binary+'0';
  end;

  GetEnabledFWDMOIntDS:=VariablesUn.BinToInt(Binary);
end;

Function TPowerPanelConfigFm.GetPortModeIntConfig:Integer;
var Binary:String;
begin
  Binary:='0100';
  Binary:=Binary+IntToStr(cmbMasterSwMode.ItemIndex);
  Binary:=Binary+'11';
  Binary:=Binary+IntToStr(cmbSwMode.ItemIndex);
  GetPortModeIntConfig:=VariablesUn.BinToInt(Binary);
end;

Function TPowerPanelConfigFm.GetPortModeIntDS:Integer;
var Binary:String;
begin
  Binary:='0100';
  Binary:=Binary+IntToStr( DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort4Mode').AsInteger);
  Binary:=Binary+'11';
  Binary:=Binary+IntToStr( DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort1Mode').AsInteger);
  GetPortModeIntDS:=VariablesUn.BinToInt(Binary)
end;

procedure TPowerPanelConfigFm.WaitforGenericResponse(UID:Integer;Command:String;rh:TNotifyEvent);
var
  PauseCount:Integer;
begin
  passFlag:=false;
  PauseCount:=0;
  while not PassFlag do
  begin
      PauseCount:=PauseCount+1;
      delay(10);
      if (PauseCount=100) or (PauseCount=200) then
        ComFm.GetGenericStatus(UID,Command,rh);
      if PauseCount>300 then
      begin

        ShowMessage('Failed Command');
        Break;
      end;
  end;
end;

Function TPowerPanelConfigFm.WaitforGenericResponseConfirmation(UID:Integer;Command:String;rh:TNotifyEvent):Boolean;
var
  PauseCount:Integer;
begin
  passFlag:=false;
  PauseCount:=0;
  while not PassFlag do
  begin
      PauseCount:=PauseCount+1;
      delay(10);
      if (PauseCount=100) or (PauseCount=200) then
        ComFm.GetGenericStatus(UID,Command,rh);
      if PauseCount>300 then
      begin
        Result:=false;
        ShowMessage('Failed Command');
        Exit;
      end;
  end;
  Result:=True;
end;

procedure TPowerPanelConfigFm.cmbDeviceSelectorChange(Sender: TObject);
begin
  VariablesUn.giTop := Top;
  VariablesUn.giLeft := Left;
  if(cmbDeviceSelector.Text='MWC')then
  begin

    DataUn.dmDataModule.dsMWCChannels.Filtered := False;
    DataUn.dmDataModule.dsMWCChannels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsMWCChannels.Filtered := True;

    if not dmDataModule.dsMWCChannels.Eof then
    begin

      VariablesUn.giTop:=top;
      variablesUn.giLeft:=Left;
      Hide;
      MWCDisplayfm.show;
    end
    else
    begin
      ShowMessage('No MWC Devices Found');
      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('MNI');
    end;
  end
  else if(cmbDeviceSelector.Text='MTR')then
  begin
    VariablesUn.giTop:=top;
    variablesUn.giLeft:=Left;
    if Assigned (rh_ShowMain) then
      rh_ShowMain(Self);
    Hide;
    VariablesUn.PassLoopFlag:=True;
  end
  else if(cmbDeviceSelector.Text='MDC')then
  begin
    DataUn.dmDataModule.dsMNI2.Filtered := False;
    DataUn.dmDataModule.dsMNI2.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsMNI2.Filtered := True;

    if not dmDataModule.dsMNI2.Eof then
    begin
      PowerPanelConfigFm.Hide;
      MNI2configFm.Show;
      VariablesUn.giTop := Top;
      VariablesUn.giLeft := Left;
      MNI2ConfigFm.GetVirtualMotorsStatus(self);
    end
    else
    begin
      ShowMessage('No MDC Devices Found');
      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('MNI');
    end;
  end
  else if(cmbDeviceSelector.Text='IQMLC2')then
  begin
    DataUn.dmDataModule.dsIQMLC2.Filtered := False;
    DataUn.dmDataModule.dsIQMLC2.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsIQMLC2.Filtered := True;

    if not dmDataModule.dsIQMLC2.Eof then
    begin
      Hide;
      VariablesUn.giTop := Top;
      VariablesUn.giLeft := Left;
      if Assigned (rh_showIQMLC2Config) then
        rh_showIQMLC2Config(self);
      //IQMLC2configFm.Show;
    end
    else
    begin
      ShowMessage('No IQMLC2 Devices Found');
      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('PowerPanel');
    end;
  end
  else if(cmbDeviceSelector.Text='Motor Maint')then
  begin
    DataUn.dmDataModule.dsMotors.Filtered := False;
    DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) + ' and MotorType = 69 or MotorType = 66';
    DataUn.dmDataModule.dsMotors.Filtered := True;

    if not dmDataModule.dsMotors.Eof then
    begin
      VariablesUn.giTop := Top;
      VariablesUn.giLeft := Left;
      while not dmDataModule.dsMotors.Eof do
      begin
        if cmbUIDPP.Text=IntToStr(DataUn.dmDataModule.dsMotors.FieldByName('UID').AsInteger) then
        begin
          Hide;
          MotorMaintenanceDisplayFm.Show;
          MotorMaintenanceDisplayFm.cmbUidMM.ItemIndex:= MotorMaintenanceDisplayFm.cmbUidMM.Items.IndexOf(cmbUIDPP.Text);
          MotorMaintenanceDisplayFm.cmbUidMM.OnChange(self);
          exit;
        end;
        dmDataModule.dsMotors.Next;
      end;
      Hide;
      MotorMaintenanceDisplayFm.Show;
      MotorMaintenanceDisplayFm.cmbUidMM.ItemIndex:= 0;
      MotorMaintenanceDisplayFm.cmbUidMM.OnChange(self);
    end
    else
    begin
      ShowMessage('No Motors Found');
      cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('MTR');
    end;
  end;
end;





procedure TPowerPanelConfigFm.cmbMasterSwModeChange(Sender: TObject);
begin
  shpcmbMasterSwMode.Visible:=true;
  btUpdateParams.Color:=clLime;
end;

procedure TPowerPanelConfigFm.cmbMasterSwPersonChange(Sender: TObject);
begin
  shpcmbMasterSwPerson.Visible:=true;
  btUpdateParams.Color:=clLime;
end;

Function TPowerPanelConfigFm.SingleEditTest:boolean;
begin
  if (shpcbSerialEnabled.visible)then
  begin
    SingleEditTest:=false;
  end
  else if (shpcmbSerialBaud.visible) then
  begin
    SingleEditTest:=false;
  end
  else if (shpcbSWEnabled.visible)then
  begin
    SingleEditTest:=false;
  end
  else if (shpcmbSWPerson.visible) then
  begin
    SingleEditTest:=false;
  end
  else if (shpcbSWFWDEnabled.visible) then
  begin
    SingleEditTest:=false;
  end
  else if (shpcmbSWMode.visible) then
  begin
    SingleEditTest:=false;
  end
  else if (teSWFWDAddr.Brush.Color=cllime) then
  begin
    SingleEditTest:=false;
  end
  else
  begin
    SingleEditTest:=true;
  end;
end;

Procedure TPowerPanelConfigFm.ClearEdits;
begin

  shpcbSerialEnabled.visible:=False;
  shpcmbSerialBaud.visible:=False;
  shpcbSWEnabled.visible:=False;
  shpcmbSWPerson.visible:=False;
  shpcbSWFWDEnabled.visible:=False;
  shpcmbSWMode.visible:=False;

  shpcbMasterSwEnabled.visible:=False;
  shpcmbMasterSwPerson.visible:=False;
  shpcbMasterSwFwdEnabled.visible:=False;
  shpcmbMasterSwMode.visible:=False;


  shpcbSerialEnabled.Brush.Color:=clLime;
  shpcmbSerialBaud.Brush.Color:=clLime;
  shpcbSWEnabled.Brush.Color:=clLime;
  shpcmbSWPerson.Brush.Color:=clLime;
  shpcbSWFWDEnabled.Brush.Color:=clLime;
  shpcmbSWMode.Brush.Color:=clLime;
  teSWFWDAddr.Brush.Color:=clWhite;

  shpcbMasterSwEnabled.Brush.Color:=clLime;
  shpcmbMasterSwPerson.Brush.Color:=clLime;
  shpcbMasterSwFwdEnabled.Brush.Color:=clLime;
  shpcmbMasterSwMode.Brush.Color:=clLime;
  teMasterSwFwdAddr.Brush.Color:=clWhite;

  btUpdateParams.Color:=clBtnFace;
end;

procedure TPowerPanelConfigFm.cmbProjectPPChange(Sender: TObject);
begin
  DataUn.dmDataModule.dsPowerPanel.Filtered := False;
  DataUn.dmDataModule.dsPowerPanel.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidPP.Text;
  DataUn.dmDataModule.dsPowerPanel.Filtered := True;

  if not (DataUn.dmDataModule.dsPowerPanel.Eof) then
  begin
    DataUn.dmDataModule.dsPowerPanel.Edit;
    DataUn.dmDataModule.dsPowerPanel.FieldByName('Alias').AsString := txtAliasPP.Text;
    VariablesUn.gsAlias := txtAliasPP.Text;
    DataUn.dmDataModule.dsPowerPanel.Post;
    DataUn.dmDataModule.dsPowerPanel.MergeChangeLog;
  end;
  DataUn.dmDataModule.dsPowerPanel.Filtered := False;
  DataUn.dmDataModule.dsPowerPanel.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
  DataUn.dmDataModule.dsPowerPanel.Filtered := True;
  loadUID;

end;

procedure TPowerPanelConfigFm.cmbSerialBaudChange(Sender: TObject);
begin
  shpcmbSerialBaud.Visible:=true;
  btUpdateParams.Color:=clLime;
end;

procedure TPowerPanelConfigFm.cmbSwitchChange(Sender: TObject);
begin
  DataUn.dmDataModule.dsPowerPanel.Filtered := False;
  DataUn.dmDataModule.dsPowerPanel.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidPP.Text;
  DataUn.dmDataModule.dsPowerPanel.Filtered := True;
  cbSWEnabled.Checked:= DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort1Enabled').AsBoolean;
  cbSWFWDEnabled.Checked:= DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort1FWDEnabled').AsBoolean;
  if DataUn.dmDataModule.dsPowerPanel.FieldByName('SW1BtnPerson').AsInteger<>-1 then
  begin
  case DataUn.dmDataModule.dsPowerPanel.FieldByName('SW1BtnPerson').AsInteger of
    9:
    Begin
      cmbSWPerson.ItemIndex:= 0;
    End;
    17:
    Begin
      cmbSWPerson.ItemIndex:=1;
    End;
    33:
    Begin
      cmbSWPerson.ItemIndex:=2;
    End;
    18:
    Begin
      cmbSWPerson.ItemIndex:=3;
    End;
    34:
    Begin
      cmbSWPerson.ItemIndex:=4;
    End;
    4:
    Begin
      cmbSWPerson.ItemIndex:=5;
    End;
    68:
    Begin
      cmbSWPerson.ItemIndex:=6;
    End;
    132:
    Begin
      cmbSWPerson.ItemIndex:=7;
    End;
  end;

  end;
  if DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort1FWDAddr').AsString<>'-1' then
  begin
    teSWFWDAddr.Text:=DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort1FWDAddr').AsString;
  end;
  if DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort1Mode').AsInteger<>-1 then
  begin
    cmbSWMode.ItemIndex:=DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort1Mode').AsInteger;
  end;

  shpcbSWEnabled.Visible:=false;
  shpcmbSWPerson.Visible:=false;
  shpcbSWFWDEnabled.Visible:=false;
  teSWFWDAddr.Brush.Color:=clWhite;
  btUpdateParams.Color:=clWhite;
  shpcmbSWMode.Visible:=false;


end;

procedure TPowerPanelConfigFm.cmbSWModeChange(Sender: TObject);
begin
  shpcmbSWMode.Visible:=true;
  btUpdateParams.Color:=clLime;
end;

procedure TPowerPanelConfigFm.cmbSWPersonChange(Sender: TObject);
begin
  shpcmbSWPerson.Visible:=true;
  btUpdateParams.Color:=clLime;
end;

Function TPowerPanelConfigFm.ConvertBaudRate:String;
begin
  case DataUn.dmDataModule.dsPowerPanel.FieldByName('SerialPerson').AsInteger of
    1:
    begin
      ConvertBaudRate:='1200';
    end;
    2:
    begin
      ConvertBaudRate:='2400';
    end;
    3:
    begin
      ConvertBaudRate:='4800';
    end;
    4:
    begin
      ConvertBaudRate:='9600';
    end;
    5:
    begin
      ConvertBaudRate:='19200';
    end;
    6:
    begin
      ConvertBaudRate:='38400';
    end;
    7:
    begin
      ConvertBaudRate:='57600';
    end;
    8:
    begin
      ConvertBaudRate:='115200';
    end;
  end;
end;

procedure TPowerPanelConfigFm.cmbUidPPChange(Sender: TObject);
begin

    DataUn.dmDataModule.dsPowerPanel.Filtered := False;
    DataUn.dmDataModule.dsPowerPanel.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidPP.Text;
    DataUn.dmDataModule.dsPowerPanel.Filtered := True;

    DataUn.dmDataModule.dsPowerPanel.MergeChangeLog;

    //Alias
    txtAliasPP.OnChange:=nil;
    txtAliasPP.Text:=DataUn.dmDataModule.dsPowerPanel.FieldByName('Alias').AsString;
    txtAliasPP.OnChange:=txtAliasPPChange;
    //Serial Enabled
    if DataUn.dmDataModule.dsPowerPanel.FieldByName('SerialDisable').AsInteger=1 then
    begin
      cbSerialEnabled.checked:=False;
    end
    else
    begin
      cbSerialEnabled.checked:=True;
    end;

    //Serial Baud
    if DataUn.dmDataModule.dsPowerPanel.FieldByName('SerialBaud').AsInteger<>-1 then
    begin
      cmbSerialBaud.ItemIndex:=DataUn.dmDataModule.dsPowerPanel.FieldByName('SerialBaud').AsInteger-1;
    end;

    //SWITCH PARAMS
    cbSWEnabled.Checked:= DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort1Enabled').AsBoolean;
    cbSWFWDEnabled.Checked:= DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort1FWDEnabled').AsBoolean;
    if DataUn.dmDataModule.dsPowerPanel.FieldByName('SW1BtnPerson').AsInteger<>-1 then
    begin
      case DataUn.dmDataModule.dsPowerPanel.FieldByName('SW1BtnPerson').AsInteger of
        9:
        Begin
          cmbSWPerson.ItemIndex:= 0;
        End;
        17:
        Begin
          cmbSWPerson.ItemIndex:=1;
        End;
        33:
        Begin
          cmbSWPerson.ItemIndex:=2;
        End;
        18:
        Begin
          cmbSWPerson.ItemIndex:=3;
        End;
        34:
        Begin
          cmbSWPerson.ItemIndex:=4;
        End;
        4:
        Begin
          cmbSWPerson.ItemIndex:=5;
        End;
        68:
        Begin
          cmbSWPerson.ItemIndex:=6;
        End;
        132:
        Begin
          cmbSWPerson.ItemIndex:=7;
        End;

      end;

        cmbSWPerson.ItemIndex:=DataUn.dmDataModule.dsPowerPanel.FieldByName('SW1BtnPerson').AsInteger;
    end;
    if DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort1FWDAddr').AsString<>'-1' then
    begin
      teSWFWDAddr.Text:=DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort1FWDAddr').AsString;
    end;
    if DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort1Mode').AsInteger<>-1 then
    begin
      cmbSWMode.ItemIndex:=DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort1Mode').AsInteger;
    end;
    //Master Switch fields
    cbMasterSWEnabled.Checked:= DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort4Enabled').AsBoolean;
    cbMasterSwFwdEnabled.Checked:= DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort4FWDEnabled').AsBoolean;
    if DataUn.dmDataModule.dsPowerPanel.FieldByName('SW4BtnPerson').AsInteger<>-1 then
    begin
      case DataUn.dmDataModule.dsPowerPanel.FieldByName('SW4BtnPerson').AsInteger of
        9:
        Begin
          cmbMasterSwPerson.ItemIndex:= 0;
        End;
        17:
        Begin
          cmbMasterSwPerson.ItemIndex:=1;
        End;
        33:
        Begin
          cmbMasterSwPerson.ItemIndex:=2;
        End;
        18:
        Begin
          cmbMasterSwPerson.ItemIndex:=3;
        End;
        34:
        Begin
          cmbMasterSwPerson.ItemIndex:=4;
        End;
        4:
        Begin
          cmbMasterSwPerson.ItemIndex:=5;
        End;
        68:
        Begin
          cmbMasterSwPerson.ItemIndex:=6;
        End;
        132:
        Begin
          cmbMasterSwPerson.ItemIndex:=7;
        End;

      end;

        cmbMasterSwPerson.ItemIndex:=DataUn.dmDataModule.dsPowerPanel.FieldByName('SW4BtnPerson').AsInteger;
    end;
    if DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort4FWDAddr').AsString<>'-1' then
    begin
      teMasterSwFwdAddr.Text:=DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort4FWDAddr').AsString;
    end;
    if DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort4Mode').AsInteger<>-1 then
    begin
      cmbMasterSwMode.ItemIndex:=DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort4Mode').AsInteger;
    end;


    //Firmware
    teFirmwareRev.Text:=  DataUn.dmDataModule.dsPowerPanel.FieldByName('FirmwareRev').AsString;
    LastUID:=StrToInt(cmbUidPP.text);
    ClearEdits;
  //End;

end;

procedure TPowerPanelConfigFm.PPUpdateParam;
begin
  //Switch Port
  DataUn.dmDataModule.dsPowerPanel.Filtered := False;
  DataUn.dmDataModule.dsPowerPanel.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + cmbUidPP.Text;
  DataUn.dmDataModule.dsPowerPanel.Filtered := True;

  cbSWEnabled.Checked:= DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort1Enabled').AsBoolean;
  cbSWFWDEnabled.Checked:= DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort1FWDEnabled').AsBoolean;
  if DataUn.dmDataModule.dsPowerPanel.FieldByName('SW1BtnPerson').AsInteger<>-1 then
  begin
    case DataUn.dmDataModule.dsPowerPanel.FieldByName('SW1BtnPerson').AsInteger of
      9:
      Begin
        cmbSWPerson.ItemIndex:= 0;
      End;
      17:
      Begin
        cmbSWPerson.ItemIndex:=1;
      End;
      33:
      Begin
        cmbSWPerson.ItemIndex:=2;
      End;
      18:
      Begin
        cmbSWPerson.ItemIndex:=3;
      End;
      34:
      Begin
        cmbSWPerson.ItemIndex:=4;
      End;
      4:
      Begin
        cmbSWPerson.ItemIndex:=5;
      End;
      68:
      Begin
        cmbSWPerson.ItemIndex:=6;
      End;
      132:
      Begin
        cmbSWPerson.ItemIndex:=7;
      End;

    end;

  end;
  if DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort1FWDAddr').AsString<>'-1' then
  begin
    teSWFWDAddr.Text:=DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort1FWDAddr').AsString;
  end;
  if DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort1Mode').AsInteger<>-1 then
  begin
    cmbSWMode.ItemIndex:=DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort1Mode').AsInteger;
  end;

  //Master Switch
  cbMasterSwEnabled.Checked:= DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort4Enabled').AsBoolean;
  cbMasterSwFwdEnabled.Checked:= DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort4FWDEnabled').AsBoolean;
  if DataUn.dmDataModule.dsPowerPanel.FieldByName('SW4BtnPerson').AsInteger<>-1 then
  begin
    case DataUn.dmDataModule.dsPowerPanel.FieldByName('SW4BtnPerson').AsInteger of
      9:
      Begin
        cmbMasterSwPerson.ItemIndex:= 0;
      End;
      17:
      Begin
        cmbMasterSwPerson.ItemIndex:=1;
      End;
      33:
      Begin
        cmbMasterSwPerson.ItemIndex:=2;
      End;
      18:
      Begin
        cmbMasterSwPerson.ItemIndex:=3;
      End;
      34:
      Begin
        cmbMasterSwPerson.ItemIndex:=4;
      End;
      4:
      Begin
        cmbMasterSwPerson.ItemIndex:=5;
      End;
      68:
      Begin
        cmbMasterSwPerson.ItemIndex:=6;
      End;
      132:
      Begin
        cmbMasterSwPerson.ItemIndex:=7;
      End;

    end;

  end;
  if DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort4FWDAddr').AsString<>'-1' then
  begin
    teMasterSwFwdAddr.Text:=DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort4FWDAddr').AsString;
  end;
  if DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort4Mode').AsInteger<>-1 then
  begin
    cmbMasterSwMode.ItemIndex:=DataUn.dmDataModule.dsPowerPanel.FieldByName('SwPort4Mode').AsInteger;
  end;

  ClearEdits;
end;

procedure TPowerPanelConfigFm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  btnExitClick(self);
end;

procedure TPowerPanelConfigFm.FormCreate(Sender: TObject);
begin
  ComboBox_AutoWidth(cmbDeviceSelector);
end;

procedure TPowerPanelConfigFm.FormHide(Sender: TObject);
begin
  VariablesUn.giTop := Top;
  VariablesUn.giLeft := Left;
end;

//Allows combox  dropdown to size to oversized string items
procedure TPowerPanelConfigFm.ComboBox_AutoWidth(const theComboBox: TCombobox);
const
  HORIZONTAL_PADDING = 4;
var
  itemsFullWidth: integer;
  idx: integer;
  itemWidth: integer;
begin
  itemsFullWidth := 0;
  // get the max needed with of the items in dropdown state
  for idx := 0 to -1 + theComboBox.Items.Count do
  begin
    itemWidth := theComboBox.Canvas.TextWidth(theComboBox.Items[idx]);
    Inc(itemWidth, 2 * HORIZONTAL_PADDING);
    if (itemWidth > itemsFullWidth) then itemsFullWidth := itemWidth;
  end;
  itemsFullWidth:=itemsFullWidth+20;
    // set the width of drop down if needed
  if (itemsFullWidth > theComboBox.Width) then
  begin
    //check if there would be a scroll bar
    if theComboBox.DropDownCount < theComboBox.Items.Count then
      itemsFullWidth := itemsFullWidth + GetSystemMetrics(SM_CXVSCROLL);
    SendMessage(theComboBox.Handle, CB_SETDROPPEDWIDTH, itemsFullWidth, 0);
  end;
end;

procedure TPowerPanelConfigFm.FormMouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
var
  I: Integer;
begin
  scrollbox1.SetFocus;
  Handled := PtInRect(ScrollBox1.ClientRect, ScrollBox1.ScreenToClient(MousePos));
  if Handled then
    for I := 1 to Mouse.WheelScrollLines do
    try
      if WheelDelta > 0 then
        ScrollBox1.Perform(WM_VSCROLL, SB_LINEUP, 0)
      else
        ScrollBox1.Perform(WM_VSCROLL, SB_LINEDOWN, 0);
    finally
      ScrollBox1.Perform(WM_VSCROLL, SB_ENDSCROLL, 0);
    end;
end;

Procedure TPowerPanelConfigFm.LoadProjects;
var
  I:Integer;
begin
  cmbProjectPP.Items.clear;
  //Polpulate project if it exists
  if DataUn.dmDataModule.dsProject.State <> dsInactive then
  begin
    DataUn.dmDataModule.dsProject.Filtered := False;
    DataUn.dmDataModule.dsProject.First;
    while not DataUn.dmDataModule.dsProject.eof do
    begin
      if (DataUn.dmDataModule.dsProject.FieldByName('Last').AsInteger = 1) then
         cmbProjectPP.Items.Insert(0, DataUn.dmDataModule.dsProject.FieldByName('ProjectName').AsString)
      else
         cmbProjectPP.Items.Add(DataUn.dmDataModule.dsProject.FieldByName('ProjectName').AsString);

      DataUn.dmDataModule.dsProject.Next;
    end;
    DataUn.dmDataModule.dsProject.Filtered := False;
    DataUn.dmDataModule.dsProject.First;
    for I := 0 to cmbProjectPP.Items.Count-1 do
    begin
      if cmbProjectPP.text = VariablesUn.gsProjectName then
      begin
        break;
      end
      else
      begin
        cmbProjectPP.ItemIndex:=I;
      end;
    end;
  End;
end;

procedure TPowerPanelConfigFm.FormShow(Sender: TObject);
begin
  Top:=VariablesUn.giTop;
  Left:=VariablesUn.giLeft;
  cmbDeviceSelector.ItemIndex:=cmbDeviceSelector.Items.IndexOf('PowerPanel');;

  ComFm.rh_PowerPanelConfigPortOpend := rhPortOpend;
  ComFm.rh_PowerPanelConfigPortClosed := rhPortClosed;
  LoadProjects;
  LoadUID;
  LastUID:=StrToInt(cmbUidPP.Text);
  ClearEdits;
  cmbUidPPChange(nil);
  cmbSwitchChange(Nil);
  vsOldAlias:=txtAliasPP.Text;
end;

procedure TPowerPanelConfigFm.btnSettingsClick(Sender: TObject);
begin
  VariablesUn.gitop:=top;
  VariablesUn.giLeft:=left;
  ScreenSetupFm.rh_showmain := rhShowPP;
  pnlMenuBar.Visible := False;
  Hide;
  ScreenSetupFm.Show;
end;

procedure TPowerPanelConfigFm.btnSettingsMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;



procedure TPowerPanelConfigFm.Image2MouseEnter(Sender: TObject);
begin
  if image2.enabled then
  Begin
    pnlMenuBar.Visible := True;
  End;
end;

procedure TPowerPanelConfigFm.JvCaptionButton1Click(Sender: TObject);
begin
  Application.HelpContext(8);
end;

procedure TPowerPanelConfigFm.pnlMenuBarMouseLeave(Sender: TObject);
begin
  pnlMenuBar.Visible := false;
end;

end.
