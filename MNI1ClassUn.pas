unit MNI1ClassUn;

interface

uses
        Winapi.Windows, Forms, System.SysUtils, comUn, System.Classes, StrUtils,dialogs;

type
        TMNI1ClassUn=Class(TObject)
private
        //MNI2 DC2 Mix
        stUID:Integer;
        stBaseRev:String;
        stAppRev:String;
        stMotorPS0to255:Array[1..4] of String;
        stEncoderPS:Array[1..4] of String;
        stMotorPsPercent:Array[1..4] of String;
        stStopMode:String;
        stFeedbackLED:String;
        stMTAInhibitMode:Integer;
        stInhibitFlg:Integer;
        stMotorMaintenance:Integer;
        stPortDisable:Integer;
        stPortAddrFwd:Integer;
        stSwitchBtnPerson:Array[1..4] of Integer;
        stPortMode:Integer;
        stSetPointPS:string;
        stReverseRoll:Integer;
        stSwPort1FwdAddr:String;
        stSwPort2FwdAddr:String;
        stSwPort3FwdAddr:String;
        stSwPort4FwdAddr:String;
        stMotor1Addr1:String;
        stMotor2Addr1:String;
        stMotor3Addr1:String;
        stMotor4Addr1:String;
        stMotor1Addr2:String;
        stMotor2Addr2:String;
        stMotor3Addr2:String;
        stMotor4Addr2:String;
        stMotor1Addr3:String;
        stMotor2Addr3:String;
        stMotor3Addr3:String;
        stMotor4Addr3:String;
        stMotor1Addr4:String;
        stMotor2Addr4:String;
        stMotor3Addr4:String;
        stMotor4Addr4:String;
        stMotor1Addr5:String;
        stMotor2Addr5:String;
        stMotor3Addr5:String;
        stMotor4Addr5:String;
        stMotor1Addr6:String;
        stMotor2Addr6:String;
        stMotor3Addr6:String;
        stMotor4Addr6:String;
        stMotor1Addr7:String;
        stMotor2Addr7:String;
        stMotor3Addr7:String;
        stMotor4Addr7:String;
        stMotor1Addr8:String;
        stMotor2Addr8:String;
        stMotor3Addr8:String;
        stMotor4Addr8:String;
        stInternalRAM:String;
        stEEPROM:String;
        stBOModeandType:String;
        stMotor1BOAddr:string;
        stMotor2BOAddr:string;
        stMotor3BOAddr:string;
        stMotor4BOAddr:string;
        stMoveUpCounter:String;
        stMoveDownCounter:String;
        stAlias:String;
        stHostPortBaud:String;
        stMotorPortPerson:String;
        stSerialPortStatus:Boolean;
        stSerialPortBaid:String;
        stSerialPortPL:String;
        stSerialPortOutMSG:String;
        stIRPortPerson:String;
        stIRPortTargetAddr:String;
        stIRPortFWDAddr:String;
        stDeviceStatus:String;
        stHardwareRev:String;
        stBaseCode:String;
        stReset:String;
        stReadFlash:String;
        //DC2 only
        stAutoPortMode:String;
        stIQ2Setup:String;
        stReadWriteEEPROM:String;
        stIQ2Alias:String;
        stWriteData:String;
        stMotorCreepUp:String;
        stMotorCreepDN:String;
        stEnterExitCal:String;
        stSetTopLimit:String;
        stSetBotLmit:String;
        stStopOffsetDN:String;
        stStopOffsetUp:String;
        stMinTravel:String;



        const
        //Standard MNI cmds
        stSetUIDCmd:String = '11.';
        stGetFirmwareCmd:String = '12.';
        stGoToTopCmd:String = '13.';
        stGoToPS1Cmd:String = '14.';
        stGoToPS2Cmd:String = '15.';
        stGoToPS3Cmd:String = '16.';
        stGoToPSCmd:String = '17.';
        stGoToBottom:String = '18.';
        stMotor1PS0to255Cmd:String = '19.1.';
        stMotor2PS0to255Cmd:String = '19.2.';
        stMotor3PS0to255Cmd:String = '19.3.';
        stMotor4PS0to255Cmd:String = '19.4.';
        stMotor1PSEncoderCmd:String = '20.1.';
        stMotor2PSEncoderCmd:String = '20.2.';
        stMotor3PSEncoderCmd:String = '20.3.';
        stMotor4PSEncoderCmd:String = '20.4.';
        stMotor1PSPercentCmd:String = '21.1.';
        stMotor2PSPercentCmd:String = '21.2.';
        stMotor3PSPercentCmd:String = '21.3.';
        stMotor4PSPercentCmd:String = '21.4.';
        stStopModeCmd:String = '22.';
        stFeedbackLEDCmd:String = '23.';
        stMTAInhibitCmd:String = '24.';
        stMotorInhibitFlgCmd:String = '25.';
        stMotorMaintenanceCmd:String = '26.';
        stPortDisableCmd:String = '27.';
        stPortFwdAddrCmd:String = '28.';
        stSWPort1BtnPersonCmd:String = '29.1.';
        stSWPort2BtnPersonCmd:String = '29.2.';
        stSWPort3BtnPersonCmd:String = '29.3.';
        stSWPort4BtnPersonCmd:String = '29.4.';
        stSetPS1toCrntPSCmd:String = '31.';
        stSetPS2toCrntPSCmd:String = '32.';
        stSetPS3toCrntPSCmd:String = '33.';
        stStopMotorCmd:String = '34.';
        stPortModeCmd:String = '36.';
        stSetPSCmd:String = '37.';
        stReverseRollCmd:String = '43.';

        stSWPort1FwdAddrCmd:String = '44.';
        stSWPort2FwdAddrCmd:String = '45.';
        stSWPort3FwdAddrCmd:String = '46.';
        stSWPort4FwdAddrCmd:String = '47.';
        stMotor1Bus1Cmd:String = '50.';
        stMotor2Bus1Cmd:String = '51.';
        stMotor3Bus1Cmd:String = '52.';
        stMotor4Bus1Cmd:String = '53.';
        stMotor1Bus2Cmd:String = '54.';
        stMotor2Bus2Cmd:String = '55.';
        stMotor3Bus2Cmd:String = '56.';
        stMotor4Bus2Cmd:String = '57.';
        stMotor1Bus3Cmd:String = '58.';
        stMotor2Bus3Cmd:String = '59.';
        stMotor3Bus3Cmd:String = '60.';
        stMotor4Bus3Cmd:String = '61.';
        stMotor1Bus4Cmd:String = '62.';
        stMotor2Bus4Cmd:String = '63.';
        stMotor3Bus4Cmd:String = '64.';
        stMotor4Bus4Cmd:String = '65.';
        stMotor1Bus5Cmd:String = '66.';
        stMotor2Bus5Cmd:String = '67.';
        stMotor3Bus5Cmd:String = '68.';
        stMotor4Bus5Cmd:String = '69.';
        stMotor1Bus6Cmd:String = '70.';
        stMotor2Bus6Cmd:String = '71.';
        stMotor3Bus6Cmd:String = '72.';
        stMotor4Bus6Cmd:String = '73.';
        stMotor1Bus7Cmd:String = '74.';
        stMotor2Bus7Cmd:String = '75.';
        stMotor3Bus7Cmd:String = '76.';
        stMotor4Bus7Cmd:String = '77.';
        stMotor1Bus8Cmd:String = '78.';
        stMotor2Bus8Cmd:String = '79.';
        stMotor3Bus8Cmd:String = '80.';
        stMotor4Bus8Cmd:String = '81.';
        stInternalRAMCmd:String = '83.';
        stEEPROMCmd:String = '84.';
        stBOModeAndTypeCmd:String = '115.';
        stMotor1BOAddrCmd:String = '116.';
        stIQMotorWinkCmd:String = '117.';
        stAliasCmd:String = '121.';
        stMotor2BOAddrCmd:String = '122.';
        stMotor3BOAddrCmd:String = '123.';
        stMotor4BOAddrCmd:String = '124.';
        stHostBaudCmd:String = '131.2.';
        stMotorPersonCmd:String = '132.1.';
        stSerialBaudCmd:String = '134.2.';
        stSerialConfigCmd:String = '134.7.';
        stSerialPLCmd:String = '134.9.';
        stSerialOutStructCmd:String = '134.10.';
        stIRPersonCmd:String = '135.1.';
        stIRTargAddrCmd:String = '135.16.';
        stIRFwdAddrCmd:String = '137.';
        stDeviceStatusCmd:String= '200.';
        stHardwareRevCmd:String= '201.';
        stBaseCodeRevCmd:String= '202.';
        stResetCmd:String= '203.';
        stEraseFlashCmd:String= '204.';
        stWriteFlashCmd:String= '205.';
        stReadFlashCmd:String= '206.';
        stMemSizeCmd:String= '207.';

        //Standard DC2 Cmds
        //Ramp up, Ramp down
        stAutoPortModeCmd:String='30.';
        stIQ2SetupCmd:String='42.';
        stReadWriteEEPROMCmd:String='82';
        stIQ2AliasCmd:String='85.';
        stWriteDataCMD:String='103.';
        stMotorCreepUpCmd:String='104.';
        stMotorCreepDNCmd:String='105.';
        stEnterExitCalCmd:String='106.';
        stSetTopLimitCmd:String='107.';
        stSetBotLmitCmd:String='108.';
        stStopOffsetDNCmd:String='110.';
        stStopOffsetUpCmd:String='111.';
        stMinTravelCmd:String='112.';
        stMoveUPCounterCmd:String='118.';
        stMoveDownCounterCmd:String='119.';



public
        Constructor   Create();
        property UID : Integer
        Read stUID Write stUID;

        property BaseRev :String
        Write stBaseRev ;


//        stAppRev:String;
//        stMotorPS0to255:Array[1..4] of String;
//        stEncoderPS:Array[1..4] of String;
//        stMotorPsPercent:Array[1..4] of String;
//        stStopMode:String;
//        stFeedbackLED:String;
//        stMTAInhibitMode:Integer;
//        stInhibitFlg:Integer;
//        stMotorMaintenance:Integer;
//        stPortDisable:Integer;
//        stPortAddrFwd:Integer;
//        stSwitchBtnPerson:Array[1..4] of Integer;
//        stPortMode:Integer;
//        stSetPointPS:string;
//        stReverseRoll:Integer;
//        stSwPort1FwdAddr:String;
//        stSwPort2FwdAddr:String;
//        stSwPort3FwdAddr:String;
//        stSwPort4FwdAddr:String;
//        stMotor1Addr1:String;
//        stMotor2Addr1:String;
//        stMotor3Addr1:String;
//        stMotor4Addr1:String;
//        stMotor1Addr2:String;
//        stMotor2Addr2:String;
//        stMotor3Addr2:String;
//        stMotor4Addr2:String;
//        stMotor1Addr3:String;
//        stMotor2Addr3:String;
//        stMotor3Addr3:String;
//        stMotor4Addr3:String;
//        stMotor1Addr4:String;
//        stMotor2Addr4:String;
//        stMotor3Addr4:String;
//        stMotor4Addr4:String;
//        stMotor1Addr5:String;
//        stMotor2Addr5:String;
//        stMotor3Addr5:String;
//        stMotor4Addr5:String;
//        stMotor1Addr6:String;
//        stMotor2Addr6:String;
//        stMotor3Addr6:String;
//        stMotor4Addr6:String;
//        stMotor1Addr7:String;
//        stMotor2Addr7:String;
//        stMotor3Addr7:String;
//        stMotor4Addr7:String;
//        stMotor1Addr8:String;
//        stMotor2Addr8:String;
//        stMotor3Addr8:String;
//        stMotor4Addr8:String;
//        stInternalRAM:String;
//        stEEPROM:String;
//        stBOModeandType:String;
//        stMotor1BOAddr:string;
//        stMotor2BOAddr:string;
//        stMotor3BOAddr:string;
//        stMotor4BOAddr:string;
//        stMoveUpCounter:String;
//        stMoveDownCounter:String;
//        stAlias:String;
//        stHostPortBaud:String;
//        stMotorPortPerson:String;
//        stSerialPortStatus:Boolean;
//        stSerialPortBaid:String;
//        stSerialPortPL:String;
//        stSerialPortOutMSG:String;
//        stIRPortPerson:String;
//        stIRPortTargetAddr:String;
//        stIRPortFWDAddr:String;
//        stDeviceStatus:String;
//        stHardwareRev:String;
//        stBaseCodde:String;
//        stReset:String;
//        stReadFlash:String;
//        //DC2 only
//        stAutoPortMode:String;
//        stIQ2Setup:String;
//        stReadWriteEEPROM:String;
//        stIQ2Alias:String;
//        stWriteData:String;
//        stMotorCreepUp:String;
//        stMotorCreepDN:String;
//        stEnterExitCal:String;
//        stSetTopLimit:String;
//        stSetBotLmit:String;
//        stStopOffsetDN:String;
//        stStopOffsetUp:String;
//        stMinTravel:String;


published
End;

implementation

Constructor   TMNI1ClassUn.Create();
begin

end;

end.
