unit MTRConfigScreen;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.UITypes,System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms,
  Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, newProjectUn, ComUn, VariablesUn,
  DataUn, Datasnap.DBClient, Data.DB, Vcl.Buttons, Vcl.Imaging.jpeg,
  Vcl.Imaging.pngimage,ScreenSetupUn,AboutUn, MWCConfigUn, MWCDisplayUn,
  StrUtils,MNI2configUn,IQMLC2configUn, Vcl.Samples.Spin,
  JvComponentBase, JvCaptionButton, PowerPanelConfigUn,MotorMaintenanceDisplay;

type
  TConfigScreenFm = class(TForm)
    lblProjectName: TLabel;
    Label1: TLabel;
    sbMain: TScrollBox;
    cmbUID: TComboBox;
    Image1: TImage;
    Image2: TImage;
    imgWinkUp: TImage;
    btnUIDc: TImage;
    lblAlias: TEdit;
    btnUID: TSpeedButton;
    btnUIDs: TImage;
    cmbDeviceSelector: TComboBox;
    Shape14: TShape;
    Image4: TImage;
    cmbMotorSelector: TComboBox;
    lblMotorNumber: TLabel;
    cmbDCMotors: TComboBox;
    btnMotorPersonS: TImage;
    btnMotorPersonC: TImage;
    Label19: TLabel;
    teHostUID: TEdit;
    pnlSwitch: TPanel;
    btnFwdC: TImage;
    btnFwdS: TImage;
    btnTypeS: TImage;
    btnTypeC: TImage;
    btnPortC: TImage;
    btnPortS: TImage;
    btnEnabledS: TImage;
    btnEnabledC: TImage;
    cbSWEnabled: TCheckBox;
    cmbPortMode: TComboBox;
    cmbLVtype: TComboBox;
    cmbFwd: TComboBox;
    Label25: TLabel;
    Label24: TLabel;
    Label23: TLabel;
    pnlBO: TPanel;
    Label17: TLabel;
    cmbBOMode: TComboBox;
    btnBOModeS: TImage;
    btnBOModeC: TImage;
    btnBOc: TImage;
    btnBOs: TImage;
    cmbBOtype: TComboBox;
    Label26: TLabel;
    Label12: TLabel;
    txtBOzgn: TEdit;
    btnSaveBO: TImage;
    btnCancelBO: TImage;
    pnlMasterUID: TPanel;
    lblMasterUIDIQ2: TLabel;
    teMasterUIDIQ2: TEdit;
    btnSaveMasterUID: TImage;
    btnCancelMasterUID: TImage;
    pnlDCSpeed: TPanel;
    btnRampPeriodUpS: TImage;
    Label27: TLabel;
    Label28: TLabel;
    btnSlowSpeedS: TImage;
    btnSlowSpeedC: TImage;
    Label20: TLabel;
    btnUpRollerSpeedS: TImage;
    btnUpRollerSpeedC: TImage;
    teUpSpeed: TEdit;
    teRampPeriodUP: TEdit;
    teSlowSpeed: TEdit;
    pnlTopZGN: TPanel;
    btnA1: TSpeedButton;
    btnA4: TSpeedButton;
    Label16: TLabel;
    btnA3: TSpeedButton;
    Label15: TLabel;
    btnA2: TSpeedButton;
    Label14: TLabel;
    Label13: TLabel;
    btnCancel1: TImage;
    btnCancel2: TImage;
    btnCancel3: TImage;
    btnCancel4: TImage;
    txtA4: TEdit;
    txtA3: TEdit;
    txtA2: TEdit;
    txtA1: TEdit;
    pnlBotZgn: TPanel;
    btnA8: TSpeedButton;
    lblst4: TLabel;
    btnA7: TSpeedButton;
    lblST3: TLabel;
    btnA6: TSpeedButton;
    lblST2: TLabel;
    btnA5: TSpeedButton;
    lblST1: TLabel;
    btnCancel5: TImage;
    btnCancel6: TImage;
    btnCancel7: TImage;
    btnCancel8: TImage;
    txtA8: TEdit;
    txtA7: TEdit;
    txtA6: TEdit;
    txtA5: TEdit;
    pnlLV: TPanel;
    Label22: TLabel;
    txtLVzgn: TEdit;
    btnCancelLV: TImage;
    btnLV: TSpeedButton;
    pnlProgramWink: TPanel;
    btnSave: TImage;
    imgWinkUpZGN: TImage;
    Image5: TImage;
    pnlSettings: TPanel;
    lblMTAInb: TLabel;
    lblMaint: TLabel;
    lblLed: TLabel;
    lblUniform: TLabel;
    lblInhibitFlag: TLabel;
    lblDirection: TLabel;
    lblSystem: TLabel;
    Label5: TLabel;
    Label4: TLabel;
    lblMtrType: TLabel;
    Label2: TLabel;
    btnDirectionS: TImage;
    btnDirectionC: TImage;
    btnInhibitS: TImage;
    btnInhibitC: TImage;
    btnUniformS: TImage;
    btnUniformC: TImage;
    btnLedS: TImage;
    btnLedC: TImage;
    btnMaintS: TImage;
    btnMaintC: TImage;
    btnMTAs: TImage;
    btnMTAc: TImage;
    lblMode: TLabel;
    cmbMTAInb: TComboBox;
    cmbMaint: TComboBox;
    cmbLed: TComboBox;
    cmbUniform: TComboBox;
    cmbInhibit: TComboBox;
    cmbDirection: TComboBox;
    pnlReturnToAuto: TPanel;
    lblAutoReturnIQ22: TLabel;
    teAutoReturnIQ2: TEdit;
    lblAutoReturnIQ21: TLabel;
    lblAutoReturnIQ2: TLabel;
    btnAutoReturnS: TImage;
    btnAutoReturnC: TImage;
    pnlFirmware: TPanel;
    Label3: TLabel;
    txtRevision: TEdit;
    pnlMenuBar: TPanel;
    btnMenu: TImage;
    btnFind: TImage;
    btnInfo: TImage;
    btnExit: TImage;
    btnSettings: TImage;
    btnConfig: TImage;
    btnControl: TImage;
    Label18: TLabel;
    teUpSpeedFine: TEdit;
    teDNSpeed: TEdit;
    btnDNRollerSpeedC: TImage;
    btnDNRollerSpeedS: TImage;
    Label30: TLabel;
    Label33: TLabel;
    Label29: TLabel;
    Label34: TLabel;
    teDNSpeedFine: TEdit;
    Label35: TLabel;
    teSlowSpeedFine: TEdit;
    Label32: TLabel;
    btnRampPeriodUpC: TImage;
    Label31: TLabel;
    teRampPeriodDn: TEdit;
    btnRampPeriodDnS: TImage;
    btnRampPeriodDnC: TImage;
    JvCaptionButton1: TJvCaptionButton;
    btnAliasS: TImage;
    btnAliasC: TImage;
    btnLEDOn: TButton;
    btnLEDOff: TButton;
    pnlIQ2DCSpeed: TPanel;
    btnIQ2DCRampPeriodUpS: TImage;
    lblIQ2DCRampUp: TLabel;
    lblIQ2DCSlowUpSpeed: TLabel;
    btnIQ2DCSlowUpSpeedS: TImage;
    btnIQ2DCSlowUpSpeedC: TImage;
    lblIQ2DcUpSpeed: TLabel;
    btnIQ2DCUpRollerSpeedS: TImage;
    btnIQ2DCUpRollerSpeedC: TImage;
    btnIQ2DCDNRollerSpeedC: TImage;
    btnIQ2DCDNRollerSpeedS: TImage;
    lblIQ2DcDnSpeed: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    btnIQ2DCRampPeriodUpC: TImage;
    lblIQ2DCRampDn: TLabel;
    btnIQ2DCRampPeriodDnS: TImage;
    btnIQ2DCRampPeriodDnC: TImage;
    teIQ2DCUpSpeed: TEdit;
    teIQ2DCRampUpTimeFine: TEdit;
    teIQ2DCSlowUpSpeed: TEdit;
    teIQ2DCUpSpeedFine: TEdit;
    teIQ2DCDnSpeed: TEdit;
    teIQ2DCDnSpeedFine: TEdit;
    teIQ2DCSlowUpSpeedFine: TEdit;
    teIQ2DCRampDnTimeFine: TEdit;
    lblIQ2DCSlowUpSpeed2: TLabel;
    btnIQ2DCSlowDnSpeedC: TImage;
    btnIQ2DCSlowDnSpeedS: TImage;
    lblIQ2DCSlowDnSpeed: TLabel;
    lblIQ2DCSlowDnSpeed2: TLabel;
    teIQ2DCSlowDnSpeed: TEdit;
    teIQ2DCSlowDnSpeedFine: TEdit;
    Label9: TLabel;
    teIQ2DCRampUpTime: TEdit;
    Label7: TLabel;
    teIQ2DCRampDnTime: TEdit;
    Label8: TLabel;
    pnlMaintParams: TPanel;
    lblInternalThreshold: TLabel;
    lblMotionThreshold: TLabel;
    teMotionThreshold: TEdit;
    lblThermalThresholdUnit: TLabel;
    lblExternalThreshold: TLabel;
    teExternalThreshold: TEdit;
    btnExternalThresholdS: TImage;
    btnExternalThresholdC: TImage;
    btnMotionThresholdS: TImage;
    btnMotionThresholdC: TImage;
    Label6: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    teInternalThreshold: TEdit;
    btnInternalThresholdS: TImage;
    btnInternalThresholdC: TImage;
    Label41: TLabel;
    Label21: TLabel;
    btnLimitSetting: TImage;
    procedure btnSaveLVClick(Sender: TObject);
    procedure btnAutoReturn(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure Image1MouseEnter(Sender: TObject);
    procedure pnlMenuBarMouseEnter(Sender: TObject);
    procedure pnlMenuBarMouseLeave(Sender: TObject);
    procedure btnMainMouseEnter(Sender: TObject);
    procedure btnConfigMouseEnter(Sender: TObject);
    procedure btnFindMouseEnter(Sender: TObject);
    procedure btnHelpClick(Sender: TObject);
    procedure btnInfoClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure btnMainClick(Sender: TObject);
    procedure btnConfigClick(Sender: TObject);
    procedure btnFindClick(Sender: TObject);
    procedure btnExitMouseEnter(Sender: TObject);
    procedure FormClick(Sender: TObject);
    procedure btnHelpMouseEnter(Sender: TObject);
    procedure btnInfoMouseEnter(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure sbMainClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure imgWinkUpMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure imgWinkDnMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure imgWinkDnMouseLeave(Sender: TObject);
    procedure imgWinkDnMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure imgWinkUpClick(Sender: TObject);
    procedure imgWinkDnClick(Sender: TObject);
    procedure btnGetStatusClick(Sender: TObject);
    procedure cmbDirectionChange(Sender: TObject);
    procedure cmbInhibitChange(Sender: TObject);
    procedure cmbUniformChange(Sender: TObject);
    procedure cmbLedChange(Sender: TObject);
    procedure cmbMaintChange(Sender: TObject);
    procedure cmbMTAInbChange(Sender: TObject);
    procedure btnA1Click(Sender: TObject);
    procedure btnA2Click(Sender: TObject);
    procedure btnA3Click(Sender: TObject);
    procedure btnA4Click(Sender: TObject);
    procedure txtA1Change(Sender: TObject);
    procedure txtA2Change(Sender: TObject);
    procedure txtA3Change(Sender: TObject);
    procedure txtA4Change(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure cmbType1Change(Sender: TObject);
    procedure imgWinkUpZGNMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure imgWinkUpZGNMouseLeave(Sender: TObject);
    procedure imgWinkUpZGNMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure imgWinkDnZgnMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure imgWinkDnZgnMouseLeave(Sender: TObject);
    procedure imgWinkDnZgnMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure imgWinkUpZGNClick(Sender: TObject);
    procedure imgWinkDnZgnClick(Sender: TObject);
    procedure cmbPortModeChange(Sender: TObject);
    procedure cmbLVtypeChange(Sender: TObject);
    procedure cmbFwdChange(Sender: TObject);
    procedure cmbBOtypeChange(Sender: TObject);
    procedure txtLVzgnChange(Sender: TObject);
    procedure txtBOzgnChange(Sender: TObject);
    procedure btnLVClick(Sender: TObject);
    //procedure btnBOClick(Sender: TObject);
    procedure cmbTypeLV1Change(Sender: TObject);
    //procedure btnSaveLVClick(Sender: TObject);
    procedure btnSetupClick(Sender: TObject);
    procedure cmbUIDChange(Sender: TObject);
    procedure btnUIDcClick(Sender: TObject);
    procedure btnUIDsClick(Sender: TObject);
    procedure txtA1Click(Sender: TObject);
    procedure txtA2Click(Sender: TObject);
    procedure txtA3Click(Sender: TObject);
    procedure txtA4Click(Sender: TObject);
    procedure txtLVzgnClick(Sender: TObject);
    procedure btnDirectionCClick(Sender: TObject);
    procedure btnInhibitCClick(Sender: TObject);
    procedure btnUniformCClick(Sender: TObject);
    procedure btnLedCClick(Sender: TObject);
    procedure btnMaintCClick(Sender: TObject);
    procedure btnMTAcClick(Sender: TObject);
    procedure btnDirectionSClick(Sender: TObject);
    procedure cmbUIDExit(Sender: TObject);
    procedure btnInhibitSClick(Sender: TObject);
    procedure btnUniformSClick(Sender: TObject);
    procedure btnLedSClick(Sender: TObject);
    procedure btnMaintSClick(Sender: TObject);
    procedure btnMTAsClick(Sender: TObject);
    procedure btnPortSClick(Sender: TObject);
    procedure btnPortCClick(Sender: TObject);
    procedure btnTypeCClick(Sender: TObject);
    procedure btnFwdCClick(Sender: TObject);
    procedure btnBOcClick(Sender: TObject);
    procedure btnTypeSClick(Sender: TObject);
    procedure btnFwdSClick(Sender: TObject);
    procedure btnBOsClick(Sender: TObject);
    procedure lblAliasChange(Sender: TObject);
    procedure btnUIDClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure txtA5Change(Sender: TObject);
    procedure txtA6Change(Sender: TObject);
    procedure txtA7Change(Sender: TObject);
    procedure txtA8Change(Sender: TObject);
    procedure btnA5Click(Sender: TObject);
    procedure btnA6Click(Sender: TObject);
    procedure btnA7Click(Sender: TObject);
    procedure btnA8Click(Sender: TObject);
    procedure txtA5Click(Sender: TObject);
    procedure txtA6Click(Sender: TObject);
    procedure txtA7Click(Sender: TObject);
    procedure txtA8Click(Sender: TObject);
    procedure btnCancel1Click(Sender: TObject);
    procedure btnCancel2Click(Sender: TObject);
    procedure btnCancel3Click(Sender: TObject);
    procedure btnCancel4Click(Sender: TObject);
    procedure btnCancel5Click(Sender: TObject);
    procedure btnCancel6Click(Sender: TObject);
    procedure btnCancel7Click(Sender: TObject);
    procedure btnCancel8Click(Sender: TObject);
    procedure btnCancelLVClick(Sender: TObject);
    procedure btnCancelBOClick(Sender: TObject);
    procedure btnSaveBOClick(Sender: TObject);
    procedure cmbDeviceSelectorChange(Sender: TObject);
    procedure btnLoadControlClick(Sender: TObject);
    procedure Split(Delimiter: Char; Str: string; ListOfStrings: TStrings) ;
    function ConvertZGN(ZGN:String):String;
    procedure teAutoReturnIQ2Change(Sender: TObject);
    procedure teMasterUIDIQ2Change(Sender: TObject);
    procedure btnSaveMasterUIDClick(Sender: TObject);
    procedure cmbBOModeChange(Sender: TObject);
    procedure btnBOModeSClick(Sender: TObject);
    procedure btnSettingsClick(Sender: TObject);
    procedure cmbMotorSelectorChange(Sender: TObject);
    procedure btnEnabledCClick(Sender: TObject);
    procedure btnEnabledSClick(Sender: TObject);
    procedure cbSWEnabledClick(Sender: TObject);
    procedure FormMouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure cmbDCMotorsChange(Sender: TObject);
    procedure btnMotorPersonCClick(Sender: TObject);
    procedure btnMotorPersonSClick(Sender: TObject);
    procedure btnDCSpeedSClick(Sender: TObject);
    procedure btnDCSpeedCClick(Sender: TObject);
    procedure teUpSpeedKeyPress(Sender: TObject; var Key: Char);
    procedure teSlowSpeedKeyPress(Sender: TObject; var Key: Char);
    procedure teUpSpeedChange(Sender: TObject);
    procedure teSlowSpeedChange(Sender: TObject);
    procedure teRampPeriodUPChange(Sender: TObject);
    procedure teRampPeriodUPKeyPress(Sender: TObject; var Key: Char);
    procedure btnControlMouseEnter(Sender: TObject);
    procedure teDNSpeedChange(Sender: TObject);
    procedure teDNSpeedKeyPress(Sender: TObject; var Key: Char);
    procedure teIQ2DCUpSpeedExit(Sender: TObject);
    procedure teDNSpeedExit(Sender: TObject);
    procedure teSlowSpeedExit(Sender: TObject);
    procedure txtAliasEnterKey(Sender: TObject);
    procedure lblAliasDblClick(Sender: TObject);
    procedure lblAliasKeyPress(Sender: TObject; var Key: Char);
    procedure Shape14MouseEnter(Sender: TObject);
    procedure lblAliasMouseEnter(Sender: TObject);
    procedure lblProjectNameMouseEnter(Sender: TObject);
    procedure teDNSpeedFineChange(Sender: TObject);
    procedure teSlowSpeedFineChange(Sender: TObject);
    procedure teUpSpeedFineExit(Sender: TObject);
    procedure teDNSpeedFineExit(Sender: TObject);
    procedure teSlowSpeedFineExit(Sender: TObject);
    procedure teRampPeriodUPExit(Sender: TObject);
    procedure teRampPeriodDnChange(Sender: TObject);
    procedure teRampPeriodDnExit(Sender: TObject);
    procedure teRampPeriodDnKeyPress(Sender: TObject; var Key: Char);
    procedure JvCaptionButton1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    function  IsIQMLC2:Boolean;
    function  IsIQMLC2EVB:Boolean;
    function  IsIQMLC2Old:Boolean;
    procedure btnAliasCClick(Sender: TObject);
    procedure lblAliasClick(Sender: TObject);
    procedure btnLEDOnClick(Sender: TObject);
    procedure btnLEDOffClick(Sender: TObject);
    procedure teIQ2DCUpSpeedEnter(Sender: TObject);
    procedure teUpSpeedExit(Sender: TObject);
    procedure teIQ2DCUpSpeedChange(Sender: TObject);
    procedure teIQ2DCUpSpeedFineChange(Sender: TObject);
    procedure teIQ2DCUpSpeedFineEnter(Sender: TObject);
    procedure teIQ2DCUpSpeedFineExit(Sender: TObject);
    procedure teIQ2DCDnSpeedChange(Sender: TObject);
    procedure teIQ2DCDnSpeedEnter(Sender: TObject);
    procedure teIQ2DCDnSpeedExit(Sender: TObject);
    procedure teIQ2DCDnSpeedFineChange(Sender: TObject);
    procedure teIQ2DCDnSpeedFineEnter(Sender: TObject);
    procedure teIQ2DCDnSpeedFineExit(Sender: TObject);
    procedure teIQ2DCSlowUpSpeedChange(Sender: TObject);
    procedure teIQ2DCSlowUpSpeedEnter(Sender: TObject);
    procedure teIQ2DCSlowUpSpeedExit(Sender: TObject);
    procedure teIQ2DCSlowDnSpeedChange(Sender: TObject);
    procedure teIQ2DCSlowDnSpeedEnter(Sender: TObject);
    procedure teIQ2DCSlowDnSpeedExit(Sender: TObject);
    procedure teIQ2DCRampUpTimeEnter(Sender: TObject);
    procedure teIQ2DCRampUpTimeChange(Sender: TObject);
    procedure teIQ2DCRampUpTimeExit(Sender: TObject);
    procedure teIQ2DCRampDnTimeChange(Sender: TObject);
    procedure teIQ2DCRampDnTimeEnter(Sender: TObject);
    procedure teIQ2DCRampDnTimeExit(Sender: TObject);
    procedure teIQ2DCSlowUpSpeedFineChange(Sender: TObject);
    procedure teIQ2DCSlowUpSpeedFineEnter(Sender: TObject);
    procedure teIQ2DCSlowUpSpeedFineExit(Sender: TObject);
    procedure teIQ2DCSlowDnSpeedFineChange(Sender: TObject);
    procedure teIQ2DCSlowDnSpeedFineEnter(Sender: TObject);
    procedure teIQ2DCSlowDnSpeedFineExit(Sender: TObject);
    procedure teIQ2DCRampUpTimeFineChange(Sender: TObject);
    procedure teIQ2DCRampUpTimeFineEnter(Sender: TObject);
    procedure teIQ2DCRampUpTimeFineExit(Sender: TObject);
    procedure teIQ2DCRampDnTimeFineChange(Sender: TObject);
    procedure teIQ2DCRampDnTimeFineEnter(Sender: TObject);
    procedure teIQ2DCRampDnTimeFineExit(Sender: TObject);
    procedure teIQ2DCSpeedKeyPress(Sender: TObject; var Key: Char);
    procedure btnRefreshAccelClick(Sender: TObject);
    procedure teInternalThresholdChange(Sender: TObject);
    procedure teExternalThresholdChange(Sender: TObject);
    procedure teMotionThresholdChange(Sender: TObject);
    procedure btnInternalThresholdSClick(Sender: TObject);
    procedure btnExternalThresholdSClick(Sender: TObject);
    procedure btnInternalThresholdCClick(Sender: TObject);
    procedure btnExternalThresholdCClick(Sender: TObject);
    procedure btnMotionThresholdCClick(Sender: TObject);
    procedure btnMotionThresholdSClick(Sender: TObject);
    procedure btnLimitSettingClick(Sender: TObject);
    procedure btnLimitSettingMouseEnter(Sender: TObject);
    //procedure btnControlClick(Sender: TObject);
  private
    { Private declarations }
    mInternalThreshold,mExternalThreshold:Integer;
    gsA1: String;
    gsA2: String;
    gsA3: String;
    gsA4: String;
    gsA5: String;
    gsA6: String;
    gsA7: String;
    gsA8: String;
    gsLV: String;
    gsBO: String;
    giComPortOn: Boolean;
    giSentCom: Integer;
    giUID: Integer;
    giPosition: Integer;
    gbSaveZGN: Boolean;
    gbSaveLV: Boolean;
    gbSaveUID: Boolean;
    gbUIDfound: Boolean;
//    MotorDirectionBits:Integer;
    MotorInhibFlagBits:Integer;
    MotorInhibitModeBits:Integer;
    MotorMaintBits:Integer;
    MotorLEDBits:Integer;
    MotorBOModeBits:Integer;
    MotorBOTypeBits:Integer;
    SwitchFwdBits:Integer;
    SwitchPortModeBits:Integer;
    SwitchEnabledBits:Integer;
    MotorChangeFlg:boolean;
    LastHostUID:Integer;
    vsOldAlias:String;
    PassFlag:boolean;
    DCSpeedUpLimit:Integer;
    DCSpeedLowerLimit:Integer;
    DCSFinespeedLowerLimit:Integer;
    IQ2DCSpeedLimitHi:Integer;
    IQ2DCSpeedLimitLo:Integer;
    IQ2DCRampLimitHi:Integer;
    IQ2DCRampLimitLoVar:Variant;
    IQ2DCRampLimitLoDec:Integer;
    IQ2DCSlowSpeedLimitHi:Integer;
    IQ2DCSlowSpeedLimitLo:Integer;
    flgHaltStatus:boolean;
    mOldIQ2DCUpValue: string;
    mOldIQ2DCUpFineValue: string;
    mOldIQ2DCDnValue: string;
    mOldIQ2DCDnFineValue: string;
    mOldIQ2DCSlowUpValue: string;
    mOldIQ2DCSlowDnValue: string;
    mOldIQ2DCRampUpValue: string;
    mOldIQ2DCRampDnValue: string;
    mOldIQ2DCSlowUpSpeedFineValue: string;
    mOldIQ2DCSlowDnSpeedFineValue: string;
    mOldIQ2DCRampUpSpeedFineValue: string;
    mOldIQ2DCRampDnSpeedFineValue: string;
//    Procedure SendToDCMotors(TimeInt:Integer);
    //Allows combox  dropdown to size to oversized string items
    procedure ComboBox_AutoWidth(const theComboBox: TCombobox);
    function WaitforSetResponse(UID:Integer;Command:String;rh:TNotifyEvent;Resend:Integer):bool;
    procedure WaitforResponse(UID:Integer;Command:String;rh:TNotifyEvent;Resend:Integer);
    function WaitforGenericResponse(UID:Integer;Command:String;rh:TNotifyEvent;ResendTime:Integer):bool;
    procedure ButtonsEnable;
    procedure ButtonsDisable;
    procedure UIDWink(viWinkCount: Integer);
    procedure UIDMoveUp;
    procedure UIDMoveDwn;
    procedure UIDStop;
    procedure rhMotorStatusRecieved(sender: TObject);
    procedure ClearScreen;
    procedure SaveZGN;
    procedure SaveLV;
//    procedure SelectType;
//    procedure SelectLVType;
    procedure rhEnable;
    procedure rhDisable;
    procedure ZGNWink(viWinkCount: Integer);
    procedure rhShowConfig(Sender: TObject);
    procedure ShowUID;
    procedure rhUIDRecieved(sender: TObject);
    procedure SaveDsbl;
    procedure GetAlias;
    procedure SaveParameters;
    procedure DisplayParameters;
    procedure GetMotorConnected;
    procedure rhMotorConnectedRcvd(sender: TObject);
    procedure ChangeStyleList;
    procedure ShowAddress;
    procedure HideAddress;
    Function  isIQ2DC:Boolean;
    Function  isIQ3DC:Boolean;
    Procedure SortStringListUIDS(UIDList:TStringList);
    Function  BuildIQMLC2Setup1MessageforLSIS:String;
    Function  BuildIQMLC2Setup1MessageforMaintenance:String;
    Function  BuildIQMLC2Setup1MessageforMTAInhibit:String;
    Function  is50DC:Boolean;
    Function  is50AC:Boolean;
    Function  isSt30:Boolean;
    Function  isSW:Boolean;
    Function  isDC:Boolean;
    Function IsMNI:Boolean;
    Function ZGNMoveUp: Boolean;
    Function ZGNMoveDwn: Boolean;
    Function ZGNStop: Boolean;
    Function GtoZGN2(vsG: String): String;
    Function GetUID(MixedUID:String):String;
  public
    { Public declarations }
    rh_ShowMain: TNotifyEvent;
    rh_ShowFind: TNotifyEvent;
    procedure rhPortClosed(Sender: TObject);
    procedure rhPortOpend(Sender: TObject);
    procedure GetMotorStatus;
  end;

var
  ConfigScreenFm: TConfigScreenFm;
  vbCheckBoxPass:boolean;
  vbSkipNSM:boolean;
  vbSkipAutoReturnUpdate:boolean;

implementation

//uses AboutUn, MWCConfigUn, MWCDisplayUn;


uses MNIConfigUn, LimitSettingUn, IQ3RFDisplayUn;{$R *.dfm}

procedure TConfigScreenFm.btnDCSpeedSClick(Sender: TObject);  //no 199
var
  vsCommand:String;
begin
  try
    vsCommand:='';
    if Sender=btnUpRollerSpeedS then
    begin
      if (StrToInt(teUpSpeed.Text)>DCSpeedUpLimit)or(StrToInt(teUpSpeed.Text)<DCSpeedLowerLimit) then
      begin
        ShowMessage('UP SPD Coarse must be between '+IntToStr(DCSpeedLowerLimit)+' and '+IntToStr(DCSpeedUpLimit)+' before saving to motor.');
        exit;
      end
      else if Trim(teUpSpeedFine.Text)='' then
      begin
        ShowMessage('UP SPD Fine must be between '+IntToStr(DCSpeedLowerLimit)+' and '+IntToStr(DCSpeedUpLimit)+' before saving to motor.');
        exit;
      end
      else
      begin
        vsCommand:=VariablesUn.DC1_UpRollerSpeed+'.'+teUpSpeedFine.Text+'.'+teUpSpeed.Text;
      end;
    end
    else if Sender=btnIQ2DCUpRollerSpeedS then
    begin
      if (Trim(teIQ2DCUpSpeedFine.Text)='') or (Trim(teIQ2DCUpSpeed.Text)='') then
      begin
        ShowMessage('UP SPD fields cannot be left blank.');
        exit;
      end
      else if (StrToFloat(teIQ2DCUpSpeed.Text+'.'+teIQ2DCUpSpeedFine.Text)>IQ2DCSpeedLimitHi)or(StrToFloat(teIQ2DCUpSpeed.Text+'.'+teIQ2DCUpSpeedFine.Text)<IQ2DCSpeedLimitLo) then
      begin
        ShowMessage('UP SPD must be between '+IntToStr(IQ2DCSpeedLimitHi)+'.0 and '+IntToStr(IQ2DCSpeedLimitLo)+'.0 before saving to motor.');
        exit;
      end
      else
      begin
        vsCommand:=VariablesUn.IQ2DC_UpRollerSpeed+'.'+IntToStr(Hi(StrToInt(teIQ2DCUpSpeed.Text+teIQ2DCUpSpeedFine.Text)))+'.'+IntToStr(Lo(StrToInt(teIQ2DCUpSpeed.Text+teIQ2DCUpSpeedFine.Text)));
      end;
    end
    else if Sender=btnDnRollerSpeedS then
    begin
      if (StrToInt(teDnSpeed.Text)>DCSpeedUpLimit)or(StrToInt(teDNSpeed.Text)<DCSpeedLowerLimit) then
      begin
        ShowMessage('DN SPD Coarse must be between '+IntToStr(DCSpeedLowerLimit)+' and '+IntToStr(DCSpeedUpLimit)+' before saving to motor.');
        exit;
      end
      else if Trim(teDnSpeedFine.Text)='' then
      begin
        ShowMessage('DN SPD Fine must be between '+IntToStr(DCSpeedLowerLimit)+' and '+IntToStr(DCSpeedUpLimit)+' before saving to motor.');
        exit;
      end
      else
      begin
        vsCommand:=VariablesUn.DC1_DnRollerSpeed+'.'+teDnSpeedFine.Text+'.'+teDnSpeed.Text;
      end;
    end
    else if Sender = btnIQ2DCDNRollerSpeedS then
    begin
      if (Trim(teIQ2DCDnSpeed.Text)='') or (Trim(teIQ2DCDnSpeedFine.Text)='') then
      begin
        ShowMessage('DN SPD fields cannot be left blank.');
        exit;
      end
      else if (StrToFloat(teIQ2DCDnSpeed.Text+'.'+teIQ2DCDNSpeedFine.Text)>IQ2DCSpeedLimitHi)or(StrToFloat(teIQ2DCDnSpeed.Text+'.'+teIQ2DCDNSpeedFine.Text)<IQ2DCSpeedLimitLo) then
      begin
        ShowMessage('DN SPD must be between '+IntToStr(IQ2DCSpeedLimitHi)+'.0 and '+IntToStr(IQ2DCSpeedLimitLo)+'.0 before saving to motor.');
        exit;
      end
      else
      begin
        vsCommand:=VariablesUn.IQ2DC_DnRollerSpeed+'.'+IntToStr(Hi(StrToInt(teIQ2DCDnSpeed.Text+teIQ2DCDnSpeedFine.Text)))+'.'+IntToStr(Lo(StrToInt(teIQ2DCDnSpeed.Text+teIQ2DCDnSpeedFine.Text)));
      end;

    end
    else if Sender=btnRampPeriodUpS then
    begin
      vsCommand:=VariablesUn.DC1_RampPeriod+'.0.'+teRampPeriodUP.Text;
    end
    else if Sender=btnIQ2DCRampPeriodUpS then
    begin
      if (Trim(teIQ2DCRampUpTime.Text)='') or (Trim(teIQ2DCRampUpTimeFine.Text)='') then
      begin
        ShowMessage('Ramp Up fields cannot be left blank.');
        exit;
      end
      else if (StrToCurr(teIQ2DCRampUpTime.Text+'.'+teIQ2DCRampUpTimeFine.Text)>IQ2DCRampLimitHi)or(StrToCurr(teIQ2DCRampUpTime.Text+'.'+teIQ2DCRampUpTimeFine.Text)<IQ2DCRampLimitLoVar) then
      begin
        ShowMessage('Ramp Up must be between '+IntToStr(IQ2DCRampLimitHi)+'.0 and '+LeftStr(VarToStr(IQ2DCRampLimitLoVar),3)+' before saving to motor.');
        exit;
      end
      else
      begin
        vsCommand:=VariablesUn.IQ2DC_UpRampTime+'.0.'+IntToStr(StrToInt(teIQ2DCRampUpTime.Text+teIQ2DCRampUpTimeFine.Text));
      end;
    end
    else if Sender=btnRampPeriodDnS then
    begin
      vsCommand:=VariablesUn.DC1_RampPeriod+'.1.'+teRampPeriodDN.Text;
    end
    else if Sender=btnIQ2DCRampPeriodDnS then
    begin
      if (Trim(teIQ2DCRampDnTime.Text)='') or (Trim(teIQ2DCRampDnTimeFine.Text)='') then
      begin
        ShowMessage('Ramp Dn fields cannot be left blank.');
        exit;
      end
      else if (StrToFloat(teIQ2DCRampDnTime.Text+'.'+teIQ2DCRampDnTimeFine.Text)>IQ2DCRampLimitHi)or(StrToFloat(teIQ2DCRampDnTime.Text+'.'+teIQ2DCRampDnTimeFine.Text)<IQ2DCRampLimitLoVar) then
      begin
        ShowMessage('Ramp Dn must be between '+IntToStr(IQ2DCRampLimitHi)+'.0 and '+LeftStr(VarToStr(IQ2DCRampLimitLoVar),3)+' before saving to motor.');
        exit;
      end
      else
      begin
        vsCommand:=VariablesUn.IQ2DC_DnRampTime+'.0.'+IntToStr(StrToInt(teIQ2DCRampDnTime.Text+teIQ2DCRampDnTimeFine.Text));
      end;
    end
    else if Sender=btnSlowSpeedS then
    begin
      if (StrToInt(teSlowSpeed.Text)>DCSpeedUpLimit)or(StrToInt(teSLowSpeed.Text)<DCSpeedLowerLimit) then
      begin
        ShowMessage('Slow SPD Coarse must be between '+IntToStr(DCSpeedLowerLimit)+' and '+IntToStr(DCSpeedUpLimit)+' before saving to motor.');
        exit;
      end
      else if Trim(teSlowSpeedFine.Text)='' then
      begin
        ShowMessage('UP SPD Fine must be between '+IntToStr(DCSpeedLowerLimit)+' and '+IntToStr(DCSpeedUpLimit)+' before saving to motor.');
        exit;
      end
      else
      begin
        vsCommand:=VariablesUn.DC1_SlowSpeed+'.'+teSlowSpeedFine.Text+'.'+teSlowSpeed.Text;
      end;
    end
    else if Sender=btnIQ2DCSlowUpSpeedS then
    begin
      if (Trim(teIQ2DCSlowUpSpeed.Text)='') or (Trim(teIQ2DCSlowUpSpeedFine.Text)='') then
      begin
        ShowMessage('Slow Up Spd fields cannot be left blank.');
        exit;
      end
      else if (StrToFloat(teIQ2DCSlowUpSpeed.Text+'.'+teIQ2DCSlowUpSpeedFine.Text)>IQ2DCSlowSpeedLimitHi)or(StrToFloat(teIQ2DCSlowUpSpeed.Text+'.'+teIQ2DCSlowUpSpeedFine.Text)<IQ2DCSlowSpeedLimitLo) then
      begin
        ShowMessage('Slow Up Spd must be between '+IntToStr(IQ2DCSlowSpeedLimitHi)+'.0 and '+IntToStr(IQ2DCSlowSpeedLimitLo)+'.0 before saving to motor.');
        exit;
      end
      else
      begin
        vsCommand:=VariablesUn.IQ2DC_UpSlowSpeed+'.'+IntToStr(Hi(StrToInt(teIQ2DCSlowUpSpeed.Text+teIQ2DCSlowUpSpeedFine.Text)))+'.'+IntToStr(Lo(StrToInt(teIQ2DCSlowUpSpeed.Text+teIQ2DCSlowUpSpeedFine.Text)));
      end;
    end
    else if Sender=btnIQ2DCSlowDnSpeedS then
    begin
      if (Trim(teIQ2DCSlowDnSpeed.Text)='') or (Trim(teIQ2DCSlowDnSpeedFine.Text)='') then
      begin
        ShowMessage('Slow Dn Spd fields cannot be left blank.');
        exit;
      end
      else if (StrToFloat(teIQ2DCSlowDnSpeed.Text+'.'+teIQ2DCSlowDnSpeedFine.Text)>IQ2DCSlowSpeedLimitHi)or(StrToFloat(teIQ2DCSlowDnSpeed.Text+'.'+teIQ2DCSlowDnSpeedFine.Text)<IQ2DCSlowSpeedLimitLo) then
      begin
        ShowMessage('Slow Dn Spd must be between '+IntToStr(IQ2DCSlowSpeedLimitHi)+'.0 and '+IntToStr(IQ2DCSlowSpeedLimitLo)+'.0 before saving to motor.');
        exit;
      end
      else
      begin
        vsCommand:=VariablesUn.IQ2DC_DnSlowSpeed+'.'+IntToStr(Hi(StrToInt(teIQ2DCSlowDnSpeed.Text+teIQ2DCSlowDnSpeedFine.Text)))+'.'+IntToStr(Lo(StrToInt(teIQ2DCSlowDnSpeed.Text+teIQ2DCSlowDnSpeedFine.Text)));
      end;
    end;


    giUID :=  StrToInt(GetUID(cmbUID.text));
    if vsCommand<>'' then
    begin
      //WaitforSetResponse(giUID, vsCommand, rhMotorStatusRecieved(),100);
      ComFm.SetGenericParam(giUID, vsCommand, nil);
      Delay(200);
      btnGetStatusClick(nil);

    end
    else
    begin
//      ShowMessage('UP SPD Fine must be between '+IntToStr(DCSpeedLowerLimit)+' and '+IntToStr(DCSpeedUpLimit)+' before saving to motor.');
    end;

  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnDCSpeedSClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;



Procedure TConfigScreenFm.SortStringListUIDS(UIDList:TStringList);
var
  I:Integer;
  First,Second:Integer;
  Swap:Boolean;
  TempStr:String;
begin
  try
    Swap:=true;
    while Swap do
    begin
      Swap:=False;
      for I := 0 to UIDList.Count-2 do
      begin
        First:=StrToInt(GetUID(UIDList[I]));
        Second:=StrToInt(GetUID(UIDList[I+1]));
        if First > Second then
        begin
          TempStr:=UIDList[I];
          UIDList[I]:=UIDList[I+1];
          UIDList[I+1]:=TempStr;
          Swap:=true;
        end;
      end;
    end;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.SortStringListUIDS: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;

end;

procedure TConfigScreenFm.WaitforResponse(UID:Integer;Command:String;rh:TNotifyEvent;Resend:Integer);
var
  PauseCount:Integer;
begin
  try
    passFlag:=false;
    PauseCount:=0;
    ComFm.GetStatus(UID,Command,rh);
    while not PassFlag do
    begin
        PauseCount:=PauseCount+1;
        delay(1);
        if (PauseCount=Resend) or (PauseCount=Resend*2) then
          ComFm.GetStatus(UID,Command,rh);
        if PauseCount>Resend*3 then
        begin
          ShowMessage('Failed Command');
          Break;
        end;
    end;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.SortStringListUIDS: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

function TConfigScreenFm.WaitforSetResponse(UID:Integer;Command:String;rh:TNotifyEvent;Resend:Integer):bool;
var
  PauseCount:Integer;
begin
  try
    passFlag:=false;
    PauseCount:=0;
    ComFm.SetGenericParam(UID,Command,rh);
    while not PassFlag do
    begin
      PauseCount:=PauseCount+1;
      delay(1);
      if (PauseCount=Resend) or (PauseCount=Resend*2) then
      begin
        ComFm.SetGenericParam(UID,Command,rh);
      end;
      if PauseCount>Resend*3 then
      begin
        ShowMessage('Failed Command');
        result:=false;
        exit;
      end;
    end;
    result:=true;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.WaitforSetResponse: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

Function TConfigScreenFm.GetUID(MixedUID:String):String;
var SpacePosition:Integer;
begin
  try
    SpacePosition:=pos(' ', MixedUID)-1;
    if SpacePosition >0 then
    begin
      GetUID:=LeftStr(MixedUID,SpacePosition);
    end
    else
    begin
      GetUID:=MixedUID;
    end;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.GetUID: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnCancel1Click(Sender: TObject);
begin
  try
    btnA1Click(nil);
    btnA1.Caption := '>';
    btnA1.Visible := True;
    btnCancel1.Visible := False;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.btnCancel1Click: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;
//splits any string with delimiter='.'
procedure TConfigScreenFm.Split(Delimiter: Char; Str: string; ListOfStrings: TStrings) ;
begin
  try
    ListOfStrings.Clear;
    ListOfStrings.Delimiter       := Delimiter;
    ListOfStrings.StrictDelimiter := True; // Requires D2006 or newer.
    ListOfStrings.DelimitedText   := Str;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.Split: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.cmbBOModeChange(Sender: TObject);
begin
  try
    btnBOModeS.Visible := True;
    btnBOModeC.Visible := True;
    ActiveControl := nil;
    sbMain.SetFocus;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.cmbBOModeChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

Function TConfigScreenFm.ConvertZGN(ZGN:String):String;
var
  OutPutList: TStringList;
begin
  try
    DataUn.dmDataModule.dsProject.Filtered := False;
    DataUn.dmDataModule.dsProject.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
    DataUn.dmDataModule.dsProject.Filtered := True;
    if not(DataUn.dmDataModule.dsProject.EOF)then
    begin
      OutPutList := TStringList.Create;
      if(DataUn.dmDataModule.dsProject.FieldByName('ZGNG').asInteger=1)then
      begin
        Split('.', Trim(ZGN), OutPutList);
        if(OutPutList[0]='255')and(OutPutList[2]='255')then
        begin
          result:=OutPutList[1];
        end
        else
        begin
          result:=zgn;
        end;
      end
      else
      begin
        result:=zgn;
      end;
    end
    else
    begin
      showmessage('Please Create a Project Before Continuing!');
    end;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.ConvertZGN: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnCancel2Click(Sender: TObject);
begin
  try
    btnA2Click(nil);
    btnA2.Caption := '>';
    btnA2.Visible := True;
    btnCancel2.Visible := False;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.btnCancel2Click: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnCancel3Click(Sender: TObject);
begin
  try
    btnA3Click(nil);
    btnA3.Caption := '>';
    btnA3.Visible := True;
    btnCancel3.Visible := False;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.btnCancel3Click: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnCancel4Click(Sender: TObject);
begin
  try
    btnA4Click(nil);
    btnA4.Caption := '>';
    btnA4.Visible := True;
    btnCancel4.Visible := False;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.btnCancel4Click: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnCancel5Click(Sender: TObject);
begin
  try
    btnA5Click(nil);
    btnA5.Caption := '>';
    btnA5.Visible := True;
    btnCancel5.Visible := False;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.btnCancel5Click: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnCancel6Click(Sender: TObject);
begin
  try
    btnA6Click(nil);
    btnA6.Caption := '>';
    btnA6.Visible := True;
    btnCancel6.Visible := False;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.btnCancel6Click: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnCancel7Click(Sender: TObject);
begin
  try
    btnA7Click(nil);
    btnA7.Caption := '>';
    btnA7.Visible := True;
    btnCancel7.Visible := False;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.btnCancel7Click: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnCancel8Click(Sender: TObject);
begin
  try
    btnA8Click(nil);
    btnA8.Caption := '>';
    btnA8.Visible := True;
    btnCancel8.Visible := False;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.btnCancel8Click: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnCancelBOClick(Sender: TObject);
begin
  try
    //btnBOClick(nil);
    btnSaveBo.Visible:=false;
    btnCancelBO.Visible := False;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.btnCancelBOClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnLoadControlClick(Sender: TObject);
begin
  try
    flgHaltStatus:=true;
    if Assigned(rh_ShowMain) then
    begin
      rh_ShowMain(Self);
    end;
    hide;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.btnCancelClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnCancelLVClick(Sender: TObject);
begin
  try
    btnLVClick(nil);
    btnLV.Caption := '>';
    btnLV.Visible := True;
    btnCancelLV.Visible := False;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.btnCancelLVClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnConfigClick(Sender: TObject);
begin
  try
    pnlMenuBar.Visible := False;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.btnConfigClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnConfigMouseEnter(Sender: TObject);
begin
  try
    pnlMenuBar.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.btnConfigMouseEnter: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnControlMouseEnter(Sender: TObject);
begin
  try
    pnlMenuBar.Visible := True;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnControlMouseEnter: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnDirectionCClick(Sender: TObject);
begin
  try
   btnGetStatusClick(nil);
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.btnDirectionCClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnDirectionSClick(Sender: TObject);    //no 199
var
    vsCommand:String;
    lowByte:Integer;
begin
  try
    if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
    begin
      giUID :=  StrToInt(GetUID(cmbUID.text));
      if (ismni)or(IsIQMLC2) then
      begin
        lowByte:=0;

        if((StrToInt(cmbMotorSelector.text))=1)then
        begin
          DataUn.dmDataModule.dsMotors.Filtered := False;
          DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt+ ' and MotorNumber = 2';
          DataUn.dmDataModule.dsMotors.Filtered := True;
          if not DataUn.dmDataModule.dsMotors.Eof then
          begin
            if (DataUn.dmDataModule.dsMotors.FieldByName('Direction').AsString='Reverse')then
            begin
              lowByte:=VariablesUn.Set_a_Bit(lowByte,1)
            end;
          end;
          DataUn.dmDataModule.dsMotors.Filtered := False;
          DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt+ ' and MotorNumber = 3';
          DataUn.dmDataModule.dsMotors.Filtered := True;
          if not DataUn.dmDataModule.dsMotors.Eof then
          begin
            if (DataUn.dmDataModule.dsMotors.FieldByName('Direction').AsString='Reverse')then
            begin
              lowByte:=VariablesUn.Set_a_Bit(lowByte,2)
            end;
          end;
          DataUn.dmDataModule.dsMotors.Filtered := False;
          DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt+ ' and MotorNumber = 4';
          DataUn.dmDataModule.dsMotors.Filtered := True;
          if not DataUn.dmDataModule.dsMotors.Eof then
          begin
            if (DataUn.dmDataModule.dsMotors.FieldByName('Direction').AsString='Reverse')then
            begin
              lowByte:=VariablesUn.Set_a_Bit(lowByte,3)
            end;
          end;
          if cmbDirection.ItemIndex=1 then
          begin
            lowByte:=VariablesUn.Set_a_Bit(lowByte,(StrToInt(cmbMotorSelector.text)-1))
          end;
        end
        else if((StrToInt(cmbMotorSelector.text))=2)then
        begin
          DataUn.dmDataModule.dsMotors.Filtered := False;
          DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt+ ' and MotorNumber = 1';
          DataUn.dmDataModule.dsMotors.Filtered := True;
          if not DataUn.dmDataModule.dsMotors.Eof then
          begin
            if (DataUn.dmDataModule.dsMotors.FieldByName('Direction').AsString='Reverse')then
            begin
              lowByte:=VariablesUn.Set_a_Bit(lowByte,0)
            end;
          end;
          DataUn.dmDataModule.dsMotors.Filtered := False;
          DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt+ ' and MotorNumber = 3';
          DataUn.dmDataModule.dsMotors.Filtered := True;
          if not DataUn.dmDataModule.dsMotors.Eof then
          begin
            if (DataUn.dmDataModule.dsMotors.FieldByName('Direction').AsString='Reverse')then
            begin
              lowByte:=VariablesUn.Set_a_Bit(lowByte,2)
            end;
          end;
          DataUn.dmDataModule.dsMotors.Filtered := False;
          DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt+ ' and MotorNumber = 4';
          DataUn.dmDataModule.dsMotors.Filtered := True;
          if not DataUn.dmDataModule.dsMotors.Eof then
          begin
            if (DataUn.dmDataModule.dsMotors.FieldByName('Direction').AsString='Reverse')then
            begin
              lowByte:=VariablesUn.Set_a_Bit(lowByte,3)
            end;
          end;
          if cmbDirection.ItemIndex=1 then
          begin
            lowByte:=VariablesUn.Set_a_Bit(lowByte,(StrToInt(cmbMotorSelector.text)-1))
          end;
        end
        else if((StrToInt(cmbMotorSelector.text))=3)then
        begin
          DataUn.dmDataModule.dsMotors.Filtered := False;
          DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt+ ' and MotorNumber = 1';
          DataUn.dmDataModule.dsMotors.Filtered := True;
          if not DataUn.dmDataModule.dsMotors.Eof then
          begin
            if (DataUn.dmDataModule.dsMotors.FieldByName('Direction').AsString='Reverse')then
            begin
              lowByte:=VariablesUn.Set_a_Bit(lowByte,0)
            end;
          end;
          DataUn.dmDataModule.dsMotors.Filtered := False;
          DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt+ ' and MotorNumber = 2';
          DataUn.dmDataModule.dsMotors.Filtered := True;
          if not DataUn.dmDataModule.dsMotors.Eof then
          begin
            if (DataUn.dmDataModule.dsMotors.FieldByName('Direction').AsString='Reverse')then
            begin
              lowByte:=VariablesUn.Set_a_Bit(lowByte,1)
            end;
          end;
          DataUn.dmDataModule.dsMotors.Filtered := False;
          DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt+ ' and MotorNumber = 4';
          DataUn.dmDataModule.dsMotors.Filtered := True;
          if not DataUn.dmDataModule.dsMotors.Eof then
          begin
            if (DataUn.dmDataModule.dsMotors.FieldByName('Direction').AsString='Reverse')then
            begin
              lowByte:=VariablesUn.Set_a_Bit(lowByte,3)
            end;
          end;
          if cmbDirection.ItemIndex=1 then
          begin
            lowByte:=VariablesUn.Set_a_Bit(lowByte,(StrToInt(cmbMotorSelector.text)-1))
          end;
        end
        else if((StrToInt(cmbMotorSelector.text))=4)then
        begin
          DataUn.dmDataModule.dsMotors.Filtered := False;
          DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt+ ' and MotorNumber = 1';
          DataUn.dmDataModule.dsMotors.Filtered := True;
          if not DataUn.dmDataModule.dsMotors.Eof then
          begin
            if (DataUn.dmDataModule.dsMotors.FieldByName('Direction').AsString='Reverse')then
            begin
              lowByte:=VariablesUn.Set_a_Bit(lowByte,0)
            end;
          end;
          DataUn.dmDataModule.dsMotors.Filtered := False;
          DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt+ ' and MotorNumber = 2';
          DataUn.dmDataModule.dsMotors.Filtered := True;
          if not DataUn.dmDataModule.dsMotors.Eof then
          begin
            if (DataUn.dmDataModule.dsMotors.FieldByName('Direction').AsString='Reverse')then
            begin
              lowByte:=VariablesUn.Set_a_Bit(lowByte,1)
            end;
          end;
          DataUn.dmDataModule.dsMotors.Filtered := False;
          DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt+ ' and MotorNumber = 3';
          DataUn.dmDataModule.dsMotors.Filtered := True;
          if not DataUn.dmDataModule.dsMotors.Eof then
          begin
            if (DataUn.dmDataModule.dsMotors.FieldByName('Direction').AsString='Reverse')then
            begin
              lowByte:=VariablesUn.Set_a_Bit(lowByte,2)
            end;
          end;
          if cmbDirection.ItemIndex=1 then
          begin
            lowByte:=VariablesUn.Set_a_Bit(lowByte,(StrToInt(cmbMotorSelector.text)-1))
          end;
        end;
        vsCommand:=VariablesUn.UID_Reverse+'.0.0.'+IntToStr(lowByte);

//        if cmbDirection.ItemIndex=0 then
//        begin
//
//          vsCommand:=VariablesUn.UID_Reverse+'.0.0.'+IntToStr(VariablesUn.Clear_a_Bit(MotorDirectionBits,StrToInt(cmbMotorSelector.text)-1));
//        end
//        else
//        begin
//          vsCommand:=VariablesUn.UID_Reverse+'.0.0.'+IntToStr(VariablesUn.Set_a_Bit(MotorDirectionBits,StrToInt(cmbMotorSelector.text)-1));
//        end;
        ComFm.SetGenericParam(giUID,vsCommand, nil);
      end
      else
      begin
        ComFm.SetMotorStatus(giUID, VariablesUn.UID_Reverse, 0, 0, cmbDirection.ItemIndex, nil);
      end;
      Delay(200);
      btnGetStatusClick(nil);
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnDirectionSClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnHelpClick(Sender: TObject);
begin
  try
    pnlMenuBar.Visible := False;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.btnHelpClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnHelpMouseEnter(Sender: TObject);
begin
  try
    pnlMenuBar.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.btnHelpMouseEnter: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnInfoClick(Sender: TObject);
begin
  try
    pnlMenuBar.Visible := False;
    Hide;
    AboutFm.rh_ShowForm := rhShowConfig;
    AboutFm.Show;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.btnInfoClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnInfoMouseEnter(Sender: TObject);
begin
  try
    pnlMenuBar.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.btnInfoMouseEnter: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnInhibitCClick(Sender: TObject);
begin
  try
    btnGetStatusClick(nil);
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.btnInhibitCClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnInhibitSClick(Sender: TObject); //no 199
var vsCommand:String;
begin
  try
    if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
    begin
      giUID :=  StrToInt(GetUID(cmbUID.text));
      if (ismni)or(IsIQMLC2) then
      begin
        if cmbInhibit.ItemIndex=0 then
        begin
          vsCommand:=VariablesUn.UID_MTA_Flag+'.0.0.'+IntToStr(VariablesUn.Clear_a_Bit(MotorInhibFlagBits,StrToInt(cmbMotorSelector.text)-1));
        end
        else
        begin
          vsCommand:=VariablesUn.UID_MTA_Flag+'.0.0.'+IntToStr(VariablesUn.Set_a_Bit(MotorInhibFlagBits,StrToInt(cmbMotorSelector.text)-1));
        end;
        ComFm.SetGenericParam(giUID,vsCommand, nil);
      end
      else
      begin
        ComFm.SetMotorStatus(giUID, VariablesUn.UID_MTA_Flag, 0, 0, cmbInhibit.ItemIndex, nil);
      end;
      Delay(400);
      btnGetStatusClick(nil);
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnInhibitSClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnInternalThresholdCClick(Sender: TObject);
begin
  try
  btnGetStatusClick(nil);
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.btnInternalThresholdCClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnInternalThresholdSClick(Sender: TObject);
begin
  try
    if teInternalThreshold.Text<>'' then
    begin
      if StrToInt(teInternalThreshold.Text)>255 then
      begin
        MessageDlg('Value Cannot Exceed 255' , mtWarning, [mbOk], 0);
        teInternalThreshold.Text:='255'

      end
      else
      begin
        VariablesUn.ClearData;
        giSentCom := StrToInt(VariablesUn.IQ2DC_Cutoff_Thresholds);
        WaitforSetResponse(giUID, VariablesUn.IQ2DC_Cutoff_Thresholds+'.1.'+teInternalThreshold.text+'.'+IntToStr(mExternalThreshold), rhMotorStatusRecieved,200);
        btnGetStatusClick(nil);
      end;
    end
    else
    begin
      MessageDlg('Value Cannot be empty' , mtWarning, [mbOk], 0);
      teInternalThreshold.Text:='0'
    end;

  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnInternalThresholdSClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

//procedure TConfigScreenFm.btnIQ2DCRefreshDataClick(Sender: TObject);
//begin
//  //Get Internal Current
//  VariablesUn.ClearData;
//  giSentCom := StrToInt(LeftStr(VariablesUn.IQ2DC_Internal_Temp_Current,3));
//  WaitforResponse(giUID, VariablesUn.IQ2DC_Internal_Temp_Current+'.0.0', rhMotorStatusRecieved,200);
//  if flgHaltStatus then
//  begin
//    flgHaltStatus:=false;
//    Exit;
//  end;
//
//  //Get Internal Ave
//  VariablesUn.ClearData;
//  giSentCom := StrToInt(LeftStr(VariablesUn.IQ2DC_Internal_Temp_Ave,3));
//  WaitforResponse(giUID, VariablesUn.IQ2DC_Internal_Temp_Ave+'.0.0', rhMotorStatusRecieved,200);
//  if flgHaltStatus then
//  begin
//    flgHaltStatus:=false;
//    Exit;
//  end;
//
//   //Get Internal Min
//  VariablesUn.ClearData;
//  giSentCom := StrToInt(LeftStr(VariablesUn.IQ2DC_Internal_Temp_Min,3));
//  WaitforResponse(giUID, VariablesUn.IQ2DC_Internal_Temp_Min+'.0.0', rhMotorStatusRecieved,200);
//  if flgHaltStatus then
//  begin
//    flgHaltStatus:=false;
//    Exit;
//  end;
//
//   //Get Internal Max
//  VariablesUn.ClearData;
//  giSentCom := StrToInt(LeftStr(VariablesUn.IQ2DC_Internal_Temp_Max,3));
//  WaitforResponse(giUID, VariablesUn.IQ2DC_Internal_Temp_Max+'.0.0', rhMotorStatusRecieved,200);
//  if flgHaltStatus then
//  begin
//    flgHaltStatus:=false;
//    Exit;
//  end;
//
//  //Get external Current
//  VariablesUn.ClearData;
//  giSentCom := StrToInt(LeftStr(VariablesUn.IQ2DC_External_Temp_Current,3));
//  WaitforResponse(giUID, VariablesUn.IQ2DC_External_Temp_Current+'.0.0', rhMotorStatusRecieved,200);
//  if flgHaltStatus then
//  begin
//    flgHaltStatus:=false;
//    Exit;
//  end;
//
//  //Get external Ave
//  VariablesUn.ClearData;
//  giSentCom := StrToInt(LeftStr(VariablesUn.IQ2DC_External_Temp_Ave,3));
//  WaitforResponse(giUID, VariablesUn.IQ2DC_External_Temp_Ave+'.0.0', rhMotorStatusRecieved,200);
//  if flgHaltStatus then
//  begin
//    flgHaltStatus:=false;
//    Exit;
//  end;
//
//   //Get external Min
//  VariablesUn.ClearData;
//  giSentCom := StrToInt(LeftStr(VariablesUn.IQ2DC_External_Temp_Min,3));
//  WaitforResponse(giUID, VariablesUn.IQ2DC_External_Temp_Min+'.0.0', rhMotorStatusRecieved,200);
//  if flgHaltStatus then
//  begin
//    flgHaltStatus:=false;
//    Exit;
//  end;
//
//   //Get external Max
//  VariablesUn.ClearData;
//  giSentCom := StrToInt(LeftStr(VariablesUn.IQ2DC_External_Temp_Max,3));
//  WaitforResponse(giUID, VariablesUn.IQ2DC_External_Temp_Max+'.0.0', rhMotorStatusRecieved,200);
//  if flgHaltStatus then
//  begin
//    flgHaltStatus:=false;
//    Exit;
//  end;
//end;

procedure TConfigScreenFm.btnLedCClick(Sender: TObject);
begin
  try
    btnGetStatusClick(nil);
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnInhibitSClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnLedSClick(Sender: TObject);
var
vsCommand:String;
begin
  try
    if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
    begin
      giUID :=  StrToInt(GetUID(cmbUID.text));
      if (ismni)or(IsIQMLC2) then
      begin
        if cmbLed.ItemIndex=0 then
        begin
          vsCommand:=VariablesUn.UID_LED+'.0.0.'+IntToStr(VariablesUn.Clear_a_Bit(MotorLEDBits,StrToInt(cmbMotorSelector.text)-1));
        end
        else
        begin
          vsCommand:=VariablesUn.UID_LED+'.0.0.'+IntToStr(VariablesUn.Set_a_Bit(MotorLEDBits,StrToInt(cmbMotorSelector.text)-1));
        end;
        ComFm.SetGenericParam(giUID,vsCommand, nil);
      end
      else
      begin
        ComFm.SetMotorStatus(giUID, VariablesUn.UID_LED, 0, 0, cmbLed.ItemIndex, nil);
      end;
      Delay(200);
      btnGetStatusClick(nil);
    end;
    except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnLedSClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnLimitSettingClick(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
  LimitSettingFm.rh_showmain := rhShowConfig;
  Hide;
  LimitSettingFm.Show;
end;

procedure TConfigScreenFm.btnLimitSettingMouseEnter(Sender: TObject);
begin
 try
    pnlMenuBar.Visible := True;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnLimitSettingMouseEnter: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnLVClick(Sender: TObject);
begin
  try
    rhDisable;
    if  btnLV.Caption = '>' then
    begin
      gsLV := txtLVzgn.Text;
      btnLV.Caption := '<';
      txtLVzgn.ReadOnly := False;
      if txtLVzgn.Visible then
        txtLVzgn.SetFocus;

      txtLVzgn.Color := $00BBEECC;


    end
    else
    begin
      txtLVzgn.ClearSelection;
      txtLVzgn.Text := gsLV;
      btnLV.Caption := '>';
      txtLVzgn.ReadOnly := True;
      txtLVzgn.Color := clWhite;

      if not (txtA1.Color = $00A4FFFF) and not (txtA2.Color = $00A4FFFF) and not (txtA3.Color = $00A4FFFF) and not (txtA4.Color = $00A4FFFF) and not(txtA5.Color = $00A4FFFF) and not(txtA6.Color = $00A4FFFF) and not (txtA7.Color = $00A4FFFF) and not (txtA8.Color = $00A4FFFF) then
      begin
        gbSaveZGN := False;
        btnSave.Visible := False;
      end;
    end;
    rhEnable;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnLVClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.FormClick(Sender: TObject);
begin
  try
    pnlMenuBar.Visible := False;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.FormClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  btnExitClick(self);
end;

procedure TConfigScreenFm.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  try
    CanClose := False;
    btnExitClick(Nil);
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.FormCloseQuery: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.FormCreate(Sender: TObject);
begin
  try
    sbMain.VertScrollBar.Range := 1200;
    gbUIDfound := False;
    HideAddress;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.FormCreate: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
  ComboBox_AutoWidth(cmbDeviceSelector);
end;

//Allows combox  dropdown to size to oversized string items
procedure TConfigScreenFm.ComboBox_AutoWidth(const theComboBox: TCombobox);
const
  HORIZONTAL_PADDING = 4;
var
  itemsFullWidth: integer;
  idx: integer;
  itemWidth: integer;
begin
  itemsFullWidth := 0;
  // get the max needed with of the items in dropdown state
  for idx := 0 to -1 + theComboBox.Items.Count do
  begin
    itemWidth := theComboBox.Canvas.TextWidth(theComboBox.Items[idx]);
    Inc(itemWidth, 2 * HORIZONTAL_PADDING);
    if (itemWidth > itemsFullWidth) then itemsFullWidth := itemWidth;
  end;
  itemsFullWidth:=itemsFullWidth+20;
    // set the width of drop down if needed
  if (itemsFullWidth > theComboBox.Width) then
  begin
    //check if there would be a scroll bar
    if theComboBox.DropDownCount < theComboBox.Items.Count then
      itemsFullWidth := itemsFullWidth + GetSystemMetrics(SM_CXVSCROLL);
    SendMessage(theComboBox.Handle, CB_SETDROPPEDWIDTH, itemsFullWidth, 0);
  end;
end;

procedure TConfigScreenFm.FormDestroy(Sender: TObject);
begin
  try
    rh_ShowMain := nil;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.FormDestroy: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.FormHide(Sender: TObject);
begin
  try
    AboutFm.Hide;
    rhDisable;
    VariablesUn.giTop := Top;
    VariablesUn.giLeft := Left;
    gbSaveUID := False;
    gbSaveZGN := False;
    gbSaveLV := False;

    ChangeStyleList;

    txtRevision.font.Color := clBlack;
    txtA1.font.Color := clBlack;
    txtA2.font.Color := clBlack;
    txtA3.font.Color := clBlack;
    txtA4.font.Color := clBlack;
    txtA5.font.Color := clBlack;
    txtA6.font.Color := clBlack;
    txtA7.font.Color := clBlack;
    txtA8.font.Color := clBlack;
    txtLVzgn.font.Color := clBlack;
    txtBOzgn.font.Color := clBlack;

    btnA1.Caption := '>';
    txtA1.Text := '';
    txtA1.ReadOnly := True;
    txtA1.Color := clWhite;

    btnA2.Caption := '>';
    txtA2.Text := '';
    txtA2.ReadOnly := True;
    txtA2.Color := clWhite;

    btnA3.Caption := '>';
    txtA3.Text := '';
    txtA3.ReadOnly := True;
    txtA3.Color := clWhite;

    btnA4.Caption := '>';
    txtA4.Text := '';
    txtA4.ReadOnly := True;
    txtA4.Color := clWhite;

    btnA5.Caption := '>';
    txtA5.Text := '';
    txtA5.ReadOnly := True;
    txtA5.Color := clWhite;

    btnA6.Caption := '>';
    txtA6.Text := '';
    txtA6.ReadOnly := True;
    txtA6.Color := clWhite;

    btnA7.Caption := '>';
    txtA7.Text := '';
    txtA7.ReadOnly := True;
    txtA7.Color := clWhite;

    btnA8.Caption := '>';
    txtA8.Text := '';
    txtA8.ReadOnly := True;
    txtA8.Color := clWhite;

    btnLV.Caption := '>';
    txtLVzgn.Text := '';
    txtLVzgn.ReadOnly := True;
    txtLVzgn.Color := clWhite;

//    btnBO.Caption := '>';
    txtBOzgn.Text := '';
    //txtBOzgn.ReadOnly := True;
    txtBOzgn.Color := clWhite;

    btnUIDs.Visible := False;
    btnUIDc.Visible := False;

    btnSave.Visible := False;
    btnCancel1.Visible := False;
    btnCancel2.Visible := False;
    btnCancel3.Visible := False;
    btnCancel4.Visible := False;
    btnCancel5.Visible := False;
    btnCancel6.Visible := False;
    btnCancel7.Visible := False;
    btnCancel8.Visible := False;
    btnCancelLV.Visible := False;
    btnCancelBO.Visible := False;
    SaveDsbl;
//    SendToDCMotors(0);

    rhEnable;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.FormHide: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.FormMouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
var
  I: Integer;
begin
  try
    Handled := PtInRect(sbMain.ClientRect, sbMain.ScreenToClient(MousePos));
    if Handled then
    begin
      for I := 1 to Mouse.WheelScrollLines do
      try
        if WheelDelta > 0 then
          sbMain.Perform(WM_VSCROLL, SB_LINEUP, 0)
        else
          sbMain.Perform(WM_VSCROLL, SB_LINEDOWN, 0);
      finally
        sbMain.Perform(WM_VSCROLL, SB_ENDSCROLL, 0);
        sbMain.Repaint;
        sbmain.Refresh;
      end;
    end;

  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.FormMouseWheel: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.FormShow(Sender: TObject);
begin
  try
    cmbDeviceSelector.ItemIndex:=0;
    Top := VariablesUn.giTop;
    Left := VariablesUn.giLeft;
    flgHaltStatus:=false;
    VariablesUn.gbUseStAddress := True;

    gsA1 := '';
    gsA2 := '';
    gsA3 := '';
    gsA4 := '';
    gsA5 := '';
    gsA6 := '';
    gsA7 := '';
    gsA8 := '';
    gsLV := '';
    gsBO := '';

    if VariablesUn.gbShowSTA then
      ShowAddress;

    Screen.Cursor := crDefault;
    gbSaveUID := False;
    gbSaveZGN := False;
    gbSaveLV := False;
    lblAlias.OnChange := nil;
    GetAlias;
    lblAlias.OnChange := lblAliasChange;
    vsOldAlias:=trim(lblAlias.Text);
    ClearScreen;
    lblProjectName.Caption := VariablesUn.gsProjectName;
    //sbMain.VertScrollBar.Position := 0;
    giPosition := 0;
    //Gets UIDs
    ShowUID;
    IQ2DCSpeedLimitHi:=28;
    IQ2DCSpeedLimitLo:=10;
    IQ2DCRampLimitHi:=2;
    IQ2DCRampLimitLoVar:=0.2;
    IQ2DCRampLimitLoDec:=2;
    IQ2DCSlowSpeedLimitHi:=28;
    IQ2DCSlowSpeedLimitLo:=10;
    DCSFinespeedLowerLimit:=0;
    if VariablesUn.gsUIDCrnt='' then
    begin
      VariablesUn.gsUIDCrnt:='0';
    end;
    DataUn.dmDataModule.dsMotors.Filtered := False;
    DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt;
    DataUn.dmDataModule.dsMotors.Filtered := True;
    if not DataUn.dmDataModule.dsMotors.Eof then
    begin
      if (DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='74')then
      begin
        cmbMotorSelector.Visible:=true;
        cmbDCMotors.Visible:=true;
        lblMotorNumber.Visible:=true;
      end
      else if (DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='65')then
      begin
        cmbMotorSelector.Visible:=true;
        cmbDCMotors.Visible:=false;
        lblMotorNumber.Visible:=true;
      end
      else if (DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='77') or (DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='205') then//st50dc
      begin
        DCSpeedUpLimit:=25;   //Limit speed input for DC motors
        DCSpeedLowerLimit:=10;
        cmbMotorSelector.Visible:=false;
        cmbDCMotors.Visible:=false;
        lblMotorNumber.Visible:=false;
      end
      else if (DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='79')  or (DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='207') then//st30
      begin
        DCSpeedUpLimit:=28;
        DCSpeedLowerLimit:=6;
        cmbMotorSelector.Visible:=false;
        cmbDCMotors.Visible:=false;
        lblMotorNumber.Visible:=false;
      end
      else
      begin

        cmbMotorSelector.Visible:=false;
        cmbDCMotors.Visible:=false;
        lblMotorNumber.Visible:=false;
      end;
//      if isDC then
//      begin
//        SendToDCMotors(60);
//      end;
    end;

    if VariablesUn.IsNumber(GetUID(cmbUID.text)) and giComPortOn then
    begin
     ButtonsEnable;
    end
    else
      ButtonsDisable;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.FormShow: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;

end;

Function TConfigScreenFm.is50AC:Boolean;
begin
  try
    DataUn.dmDataModule.dsMotors.Filtered := False;
    DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt;
    DataUn.dmDataModule.dsMotors.Filtered := True;
    if not DataUn.dmDataModule.dsMotors.Eof then
    begin
      if (DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='78') or (DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='206') then
      begin
        is50AC:=True;
      end
      else
      begin
        is50AC:=false;
      end;
    end
    else
    begin
      is50AC:=false;
    end;
  except
    on E: Exception do
    begin
      is50AC:=false;
      MessageDlg('TConfigScreenFm.is50AC: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;

end;

Function TConfigScreenFm.is50DC:Boolean;
begin
  try
    DataUn.dmDataModule.dsMotors.Filtered := False;
    DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt;
    DataUn.dmDataModule.dsMotors.Filtered := True;
    if not DataUn.dmDataModule.dsMotors.Eof then
    begin
      if (DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='77') or (DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='205') then
      begin
        is50DC:=True;
      end
      else
      begin
        is50DC:=false;
      end;
    end
    else
    begin
      is50DC:=false;
    end;
  except
    on E: Exception do
    begin
      is50DC:=false;
      MessageDlg('TConfigScreenFm.is50DC: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

Function TConfigScreenFm.isSt30:Boolean;
begin
  try
    DataUn.dmDataModule.dsMotors.Filtered := False;
    DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt;
    DataUn.dmDataModule.dsMotors.Filtered := True;
    if not DataUn.dmDataModule.dsMotors.Eof then
    begin
      if (DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='79') or (DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='207') then
      begin
        isSt30:=True;
      end
      else
      begin
        isSt30:=false;
      end;
    end
    else
    begin
      isSt30:=false;
    end;
  except
    on E: Exception do
    begin
      isSt30:=false;
      MessageDlg('TConfigScreenFm.isSt30: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

Function TConfigScreenFm.isDC:Boolean;
begin
  try
    if (pos('DC50',cmbUID.text)<>0) or (pos('ST30',cmbUID.text)<>0) or (pos('AC50',cmbUID.text)<>0)then
    begin
//    (pos('iQ3-DC',cmbUID.text)<>0) or
      IsDC:=True;
    end
    else
    begin
      IsDC:=False;
    end;
  except
    on E: Exception do
    begin
      IsDC:=False;
      MessageDlg('TConfigScreenFm.isDC: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;
Function TConfigScreenFm.isIQ2DC:Boolean;
begin
  try
    if (pos('IQ2-DC',cmbUID.text)<>0)then
    begin
      isIQ2DC:=True;
    end
    else
    begin
      isIQ2DC:=False;
    end;
  except
    on E: Exception do
    begin
      isIQ2DC:=False;
      MessageDlg('TConfigScreenFm.isIQ2DC: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

Function TConfigScreenFm.isIQ3DC:Boolean;
begin
  try
    if (pos('iQ3-DC',cmbUID.text)<>0)then
    begin
      isIQ3DC:=True;
    end
    else
    begin
      isIQ3DC:=False;
    end;
  except
    on E: Exception do
    begin
      isIQ3DC:=False;
      MessageDlg('TConfigScreenFm.isIQ3DC: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

Function TConfigScreenFm.isSW:Boolean;
begin
  try
    if pos('SW',cmbUID.text)<>0 then
    begin
      isSW:=True;
    end
    else
    begin
      isSW:=False;
    end;
  except
    on E: Exception do
    begin
      isSW:=False;
      MessageDlg('TConfigScreenFm.isSW: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.JvCaptionButton1Click(Sender: TObject);
begin
  Application.HelpContext(6);
end;

procedure TConfigScreenFm.GetAlias;
begin
  try
    if VariablesUn.IsNumber(VariablesUn.gsUIDCrnt) and not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
    begin
      DataUn.dmDataModule.dsMotors.Filtered := False;
      DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt;
      DataUn.dmDataModule.dsMotors.Filtered := True;

      if not DataUn.dmDataModule.dsMotors.Eof then
      begin
        lblAlias.OnChange := nil;
        lblAlias.Text := Trim(DataUn.dmDataModule.dsMotors.FieldByName('Alias').AsString);
        lblAlias.OnChange := lblAliasChange;
      end;
    end
    else
    begin
       lblAlias.OnChange := nil;
       lblAlias.Text := '-';
       lblAlias.OnChange := lblAliasChange;
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.GetAlias: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.GetMotorConnected;
begin
  try
    if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
    begin
      giUID := StrToInt(GetUID(cmbUID.text));
      giSentCom := StrToInt(VariablesUn.UID_FR);
      if (isMNI) or (isIQMLC2) then
      begin
//        ComFm.GetGenericStatus(giUID, VariablesUn.UID_FR+'.128.0.0', rhMotorConnectedRcvd);
        WaitforGenericResponse(giUID, VariablesUn.UID_FR+'.128.0.0', rhMotorConnectedRcvd,100);
      end
      else
      begin
        //ComFm.GetGenericStatus(giUID, VariablesUn.UID_FR+'.0.0.0', rhMotorConnectedRcvd);
        WaitforGenericResponse(giUID, VariablesUn.UID_FR+'.0.0.0', rhMotorConnectedRcvd,100);
      end;
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.GetMotorConnected: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

//Procedure TConfigScreenFm.SendToDCMotors(TimeInt:Integer);
//begin
//  try
//    if isDC then
//    begin
//      if cmbUID.text<>'' then
//      begin
//        DataUn.dmDataModule.dsMotors.Filtered := False;
//        DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt;
//        DataUn.dmDataModule.dsMotors.Filtered := True;
//
//        giUID := DataUn.dmDataModule.dsMotors.FieldByName('HostUID').AsInteger;
//        LastHostUID:=DataUn.dmDataModule.dsMotors.FieldByName('HostUID').AsInteger;
//        giSentCom := StrToInt(LeftStr(VariablesUn.MDC_HaltTimer,3));
//        ComFm.SetGenericParam(giUID, VariablesUn.MDC_HaltTimer+'.0.'+IntToStr(TimeInt) , rhMotorStatusRecieved);
//        WaitforSetResponse(giUID, VariablesUn.MDC_HaltTimer+'.0.'+IntToStr(TimeInt), rhMotorStatusRecieved,100);
//      end;
//    end
//    else
//    begin
//      if LastHostUID<>0 then
//      begin
//        giUID := LastHostUID;
//        giSentCom := StrToInt(LeftStr(VariablesUn.MWC_HaltTimer,3));
//        ComFm.SetGenericParam(giUID, VariablesUn.MWC_HaltTimer+'.0.'+IntToStr(TimeInt) , rhMotorStatusRecieved);
//        WaitforSetResponse(giUID, VariablesUn.MWC_HaltTimer+'.0.'+IntToStr(TimeInt), rhMotorStatusRecieved,100);
//      end;
//    end;
//
//  except
//    on E: Exception do
//    begin
//      MessageDlg('TConfigScreenFm.SendToDCMotors: ' + E.Message, mtWarning, [mbOk], 0);
//    end;
//  end;
//end;

function TConfigScreenFm.WaitforGenericResponse(UID:Integer;Command:String;rh:TNotifyEvent;ResendTime:Integer):bool;
var
  PauseCount:Integer;
begin
  try
    passFlag:=false;
    PauseCount:=0;
    ComFm.GetGenericStatus(UID,Command,rh);
    while not PassFlag do
    begin
      PauseCount:=PauseCount+1;
      delay(10);
      if (PauseCount=ResendTime) or (PauseCount=ResendTime*2) then
        ComFm.GetGenericStatus(UID,Command,rh);
      if PauseCount>ResendTime*2 then
      begin
        if Command=VariablesUn.UID_NAME then
          ShowMessage('Failed Command');
        result:=false;
        exit;
      end;
    end;
    result:=true;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.WaitforGenericResponse: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

function TConfigScreenFm.IsIQMLC2Old:Boolean;
begin
  DataUn.dmDataModule.dsIQMLC2.Filtered := False;
  DataUn.dmDataModule.dsIQMLC2.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.caption) +  ' and UID = ' + GetUID(cmbUID.text);
  DataUn.dmDataModule.dsIQMLC2.Filtered := True;
  if(DataUn.dmDataModule.dsIQMLC2.FieldByName('FirmwareRev').AsString = '8.8')then
  begin
    result:=True;
  end
  else
  begin
    if(DataUn.dmDataModule.dsIQMLC2.FieldByName('FirmwareRev').AsString = '0.3')then
    begin
      result:=True;
    end
    else
    begin
      result:=False;
    end;
  end
end;
function TConfigScreenFm.IsIQMLC2EVB:Boolean;
begin
  DataUn.dmDataModule.dsIQMLC2.Filtered := False;
  DataUn.dmDataModule.dsIQMLC2.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.caption) +  ' and UID = ' + GetUID(cmbUID.text);
  DataUn.dmDataModule.dsIQMLC2.Filtered := True;
  if(DataUn.dmDataModule.dsIQMLC2.FieldByName('FirmwareRev').AsString = 'A.5')then
  begin
    result:=True;
  end
  else
  begin
    result:=False;
  end
end;

function TConfigScreenFm.IsIQMLC2:Boolean;
var
  I,DelimiterCount:Integer;
  tempStr:String;
begin
//  DelimiterCount:=0;
//  for I := 1 to Length(cmbUID.text) do
//  begin
//    if cmbUID.text[I] = ':' then
//    begin
//      inc(DelimiterCount);
//    end;
//  end;
//  if DelimiterCount>1 then
  if(pos('IQMLC2',cmbUID.text)>0)then
  begin
    IsIQMLC2:=true;
  end
  else
  begin

//    if not (pos(' ',cmbUID.text)=0) then
//    begin
//      tempStr:=LeftStr(cmbUID.text,pos(' ',cmbUID.text)-1);
//    end
//    else
//    begin
//      tempStr:=cmbUID.text;
//    end;
    dmDataModule.dsIQMLC2.Filtered := False;
    dmDataModule.dsIQMLC2.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + gsUIDCrnt;
    dmDataModule.dsIQMLC2.Filtered := True;
    if dmDataModule.dsIQMLC2.eof then
    begin
      IsIQMLC2:=false;
    end
    else
    begin
      IsIQMLC2:=true;
    end;
  end;
end;

//Grabs all Data for any motor and organizes the scrollbox view
procedure TConfigScreenFm.GetMotorStatus;
var
  I,ScrollHeight,Iq2DcHighByte: Integer;
begin
  try
    Iq2DcHighByte:=0;
    ScrollHeight:=0;
    outputdebugstring(pchar('TOP '+inttostr(sbmain.VertScrollBar.Position)));
    if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
    begin
      rhDisable;
      giUID :=  StrToInt(GetUID(cmbUID.text));
      ClearScreen;
      SaveDsbl;

      VariablesUn.ClearData;
      giSentCom := StrToInt(VariablesUn.UID_FR);  //get Firmware
      //ComFm.GetStatus(giUID, VariablesUn.UID_FR, rhMotorStatusRecieved);
      WaitforResponse(giUID, VariablesUn.UID_FR, rhMotorStatusRecieved,500);
      if flgHaltStatus then
      begin
        flgHaltStatus:=false;
        Exit;
      end;
      //Adjust Display for MNI only
      if (isMNI)or(isIQMLC2) then
      begin
        Label23.Visible:=True;
        Label24.Visible:=True;
        Label18.Visible:=True;
        Label25.Visible:=True;
        label3.Visible:=True;
        lblUniform.Visible:=True;
        label21.Visible:=True;
        Label19.Visible:=False;

        cmbPortMode.Visible:=True;
        cbSWEnabled.Visible:=True;
        cmbLVtype.Visible:=True;
        cmbFwd.Visible:=True;
        cmbLed.Visible:=True;
        cmbUniform.Visible:=True;

        pnlLV.Visible:=True;
        pnlIQ2DCSpeed.Visible:=false;

        txtRevision.Visible:=True;
        teHostUID.Visible:=False;

        btnEnabledS.Visible:=False;
        btnEnabledC.Visible:=False;

        pnlDCSpeed.Visible:=False;
        pnlIQ2DCSpeed.Visible:=false;
        pnlFirmware.visible:=True;
        pnlSwitch.Visible:=true;
        pnlReturnToAuto.Visible:=False;
        pnlMaintParams.Visible:=false;

        if isIQMLC2 then
        begin
          pnlBO.Visible:=false;
          cmbled.Visible:=false;
        end
        else
        begin
          pnlBO.Visible:=true;
        end;
        if not isIQMLC2Old then //skip if old IQMLC2 use setup command
        begin          //JH IQMLC2
          btnLEDOn.Visible:=true;
          btnLEDOff.Visible:=true;
          VariablesUn.ClearData;
          giSentCom := StrToInt(VariablesUn.UID_LSE);  //get Enabled Switches
          //ComFm.GetStatus(giUID, VariablesUn.UID_LSE, rhMotorStatusRecieved);
          WaitforResponse(giUID, VariablesUn.UID_LSE, rhMotorStatusRecieved,100);
          lblMode.Visible:=true;
          label4.Visible:=true;
          lblSystem.Visible:=true;
          label5.Visible:=true;
          //cmbled.Visible:=true;
          lblUniform.Visible:=true;
        end
        else
        begin
          //cmbled.Visible:=false;
          btnLEDOn.Visible:=false;
          btnLEDOff.Visible:=false;
          lblUniform.Visible:=false;
          lblMode.Visible:=false;
          label4.Visible:=false;
          lblSystem.Visible:=false;
          label5.Visible:=false;
        end;

        if flgHaltStatus then
        begin
          flgHaltStatus:=false;
          Exit;
        end;

        //VariablesUn.delay(100);

//        if VariablesUn.gbShowSTA then
//        begin
//          pnlLV.Top:= pnlTopZGN.Height+pnlBotZGN.Height-SbMain.VertScrollBar.Position;
//          pnlProgramWink.Top:= pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height-SbMain.VertScrollBar.Position;
//          pnlSettings.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height-SbMain.VertScrollBar.Position;
//          pnlFirmware.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height-SbMain.VertScrollBar.Position;;
//          pnlSwitch.top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlFirmware.Height-SbMain.VertScrollBar.Position;;
//          if not isIQMLC2 then
//          begin
//            pnlBO.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlFirmware.Height+pnlSwitch.Height-SbMain.VertScrollBar.Position;
//          end;
////          pnlFirmware.Top:=548+pnlBotZGN.Height+SbMain.VertScrollBar.Position;
////          pnlSwitch.top:=582+pnlBotZGN.Height-SbMain.VertScrollBar.Position;
////          pnlBO.Top:=768+pnlBotZGN.Height-SbMain.VertScrollBar.Position;
//        end
//        else
//        begin
//
//          pnlLV.Top:= pnlTopZGN.Height-SbMain.VertScrollBar.Position;
//          pnlProgramWink.Top:= pnlTopZGN.Height+pnlLV.Height-SbMain.VertScrollBar.Position;
//          pnlSettings.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height-SbMain.VertScrollBar.Position;
//          pnlFirmware.Top:= pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height-SbMain.VertScrollBar.Position;
//          pnlSwitch.top:= pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlFirmware.Height-SbMain.VertScrollBar.Position;
//          if not isIQMLC2 then
//          begin
//            pnlBO.Top:= pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlFirmware.Height+pnlSwitch.Height-SbMain.VertScrollBar.Position;
//          end;
////          pnlFirmware.Top:=548-SbMain.VertScrollBar.Position;
////          pnlSwitch.top:=582-SbMain.VertScrollBar.Position;
////          pnlBO.Top:=768-SbMain.VertScrollBar.Position;
//        end;
      end
      else if (isIQ2DC) or (isIQ3DC) then //if dc motor
      begin
        lblUniform.Visible:=true;
        lblSystem.Visible:=true;
        label5.Visible:=true;
        lblMode.Visible:=true;
        label4.Visible:=true;
        Label23.Visible:=True;
        Label24.Visible:=True;
        Label25.Visible:=True;
        label3.Visible:=True;
        lblUniform.Visible:=True;
        label21.Visible:=True;
        Label19.Visible:=False;
        Label35.visible:=True;
        Label30.visible:=True;
        Label27.visible:=True;
        Label31.visible:=True;

        btnLEDOn.Visible:=false;
        btnLEDOff.Visible:=false;
        btnEnabledS.Visible:=False;
        btnEnabledC.Visible:=False;

        cmbled.Visible:=true;
        cmbPortMode.Visible:=True;
        cmbLVtype.Visible:=True;
        cmbFwd.Visible:=True;
        cmbLed.Visible:=True;

        txtLVzgn.Visible:=True;
        txtRevision.Visible:=True;
        teHostUID.Visible:=False;
        teDNSpeedFine.visible:=True;
        teDNSpeed.visible:=True;
        teRampPeriodUP.visible:=True;
        teRampPeriodDn.visible:=True;

        pnlBO.Visible:=true;
        pnlSettings.Visible:=True;
        pnlProgramWink.Visible:=true;
        pnlFirmware.visible:=True;
        pnlSwitch.Visible:=true;
        pnlDCSpeed.Visible:=false;
        pnlIQ2DCSpeed.Visible:=true;
        pnlLV.Visible:=True;
        pnlSwitch.Visible:=true;
        pnlReturnToAuto.Visible:=true;
        pnlMaintParams.Visible:=true;
        pnlMasterUID.Visible:=false;

//        //Align Panels
//        if VariablesUn.gbShowSTA then
//        begin
//          pnlLV.Top:= pnlTopZGN.Height+pnlBotZGN.Height-SbMain.VertScrollBar.Position;
//          pnlProgramWink.Top:= pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height-SbMain.VertScrollBar.Position;
//          pnlSettings.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height-SbMain.VertScrollBar.Position;
//          pnlFirmware.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height-SbMain.VertScrollBar.Position;
//          pnlIQ2DCSpeed.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlFirmware.Height-SbMain.VertScrollBar.Position;
//          //pnlIQ2DCTemp.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlFirmware.Height+pnlIQ2DCSpeed.Height-SbMain.VertScrollBar.Position;
//          pnlSwitch.top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlFirmware.Height+pnlIQ2DCSpeed.Height-SbMain.VertScrollBar.Position;
//          pnlBO.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlFirmware.Height+pnlIQ2DCSpeed.Height+pnlSwitch.Height-SbMain.VertScrollBar.Position;
//          pnlReturnToAuto.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlFirmware.Height+pnlIQ2DCSpeed.Height+pnlSwitch.Height+pnlBO.Height-SbMain.VertScrollBar.Position;
//
//
//        end
//        else
//        begin
//          pnlLV.Top:= pnlTopZGN.Height-SbMain.VertScrollBar.Position;
//          pnlProgramWink.Top:= pnlTopZGN.Height+pnlLV.Height-SbMain.VertScrollBar.Position;
//          pnlSettings.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height-SbMain.VertScrollBar.Position;
//          pnlFirmware.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height-SbMain.VertScrollBar.Position;
//          pnlIQ2DCSpeed.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlFirmware.Height-SbMain.VertScrollBar.Position;
//          //pnlIQ2DCTemp.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlFirmware.Height+pnlIQ2DCSpeed.Height-SbMain.VertScrollBar.Position;
//          pnlSwitch.top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlFirmware.Height+pnlIQ2DCSpeed.Height-SbMain.VertScrollBar.Position;
//          pnlBO.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlFirmware.Height+pnlIQ2DCSpeed.Height+pnlSwitch.Height-SbMain.VertScrollBar.Position;
//          //pnlReturnToAuto.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlIQ2DCSpeed.Height+pnlSwitch.Height+pnlBO.Height-SbMain.VertScrollBar.Position;
//          pnlReturnToAuto.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlFirmware.Height+pnlIQ2DCSpeed.Height+pnlSwitch.Height+pnlBO.Height-SbMain.VertScrollBar.Position;
//
////          pnlBO.Top:=670;
//        end;
      end
      else if isDC then //if dc motor
      begin
        //Remove uniform objects
        btnLEDOn.Visible:=false;
        btnLEDOff.Visible:=false;
        btnUniformS.Visible:=False;
        btnUniformC.Visible:=False;
        btnLedC.Visible:=False;
        btnLedS.Visible:=False;

        cmbled.Visible:=true;
        cmbUniform.Visible:=False;
        cmbLed.Visible:=False;

        lblUniform.Visible:=true;
        lblSystem.Visible:=true;
        label5.Visible:=true;
        lblMode.Visible:=true;
        label4.Visible:=true;
        lblUniform.Visible:=False;
        label3.Visible:=False;
        label21.Visible:=False;
        lblUniform.Visible:=False;
        Label19.Visible:=True;

        pnlBO.Visible:=true;
        pnlReturnToAuto.Visible:=True;
        pnlIQ2DCSpeed.Visible:=false;
        pnlMaintParams.Visible:=false;

        txtRevision.Visible:=False;
        teHostUID.Visible:=True;

        if not is50AC then
        begin
          pnlDCSpeed.Visible:=true;
          if isst30 then
          begin
            //remove speed down objects
            teDNSpeedFine.visible:=False;
            Label35.visible:=False;
            teDNSpeed.visible:=False;
            Label30.visible:=False;
            //remove ramp rate
            Label27.visible:=False;
            teRampPeriodUP.visible:=False;
            Label31.visible:=False;
            teRampPeriodDn.visible:=False;
          end
          else
          begin
            //Add speed down objects
            teDNSpeedFine.visible:=True;
            Label35.visible:=True;
            teDNSpeed.visible:=True;
            Label30.visible:=True;
            //add ramp rate
            Label27.visible:=True;
            teRampPeriodUP.visible:=True;
            Label31.visible:=True;
            teRampPeriodDn.visible:=True;
          end;
        end
        else
        begin
          pnlDCSpeed.Visible:=False;
        end;
        pnlFirmware.visible:=False;
        pnlMasterUID.Visible:=false;
        if isSW then
        begin
          pnlLV.Visible:=True;
          pnlSwitch.Visible:=true;
        end
        else
        begin
          pnlSwitch.Visible:=false;
          pnlLV.Visible:=False;
        end;
//        //Align Panels
//        if VariablesUn.gbShowSTA then
//        begin
//          pnlLV.Top:= pnlTopZGN.Height+pnlBotZGN.Height-SbMain.VertScrollBar.Position;
//          pnlProgramWink.Top:= pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height-SbMain.VertScrollBar.Position;
//          pnlSettings.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height-SbMain.VertScrollBar.Position;
//          if not is50AC then
//          begin
//            pnlDCSpeed.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height-SbMain.VertScrollBar.Position;
//            if isSW then
//            begin
//              pnlSwitch.top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlDCSpeed.Height-SbMain.VertScrollBar.Position;
//              pnlBO.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlDCSpeed.Height+pnlSwitch.Height-SbMain.VertScrollBar.Position;
//              pnlReturnToAuto.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlDCSpeed.Height+pnlSwitch.Height+pnlBO.Height-SbMain.VertScrollBar.Position;
//            end
//            else
//            begin
//              pnlBO.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlDCSpeed.Height-SbMain.VertScrollBar.Position;
//              pnlReturnToAuto.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlDCSpeed.Height+pnlBO.Height-SbMain.VertScrollBar.Position;
//            end;
//          end
//          else
//          begin
//            //pnlDCSpeed.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height-SbMain.VertScrollBar.Position;
//            if isSW then
//            begin
//              pnlSwitch.top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height-SbMain.VertScrollBar.Position;
//              pnlBO.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlSwitch.Height-SbMain.VertScrollBar.Position;
//              pnlReturnToAuto.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlSwitch.Height+pnlBO.Height-SbMain.VertScrollBar.Position;
//            end
//            else
//            begin
//              pnlBO.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+-SbMain.VertScrollBar.Position;
//              pnlReturnToAuto.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlBO.Height-SbMain.VertScrollBar.Position;
//            end;
//          end;
//
////          pnlBO.Top:=670+pnlBotZGN.Height;
////          pnlDCSpeed.Top:=679;
//        end
//        else
//        begin
//          pnlLV.Top:= pnlTopZGN.Height-SbMain.VertScrollBar.Position;
//          pnlProgramWink.Top:= pnlTopZGN.Height+pnlLV.Height-SbMain.VertScrollBar.Position;
//          pnlSettings.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height-SbMain.VertScrollBar.Position;
//          if not is50AC then
//          begin
//            pnlDCSpeed.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height-SbMain.VertScrollBar.Position;
//            if isSW then
//            begin
//              pnlSwitch.top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlDCSpeed.Height-SbMain.VertScrollBar.Position;
//              pnlBO.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlDCSpeed.Height+pnlSwitch.Height-SbMain.VertScrollBar.Position;
//              //pnlReturnToAuto.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlDCSpeed.Height+pnlSwitch.Height+pnlBO.Height-SbMain.VertScrollBar.Position;
//              pnlReturnToAuto.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlDCSpeed.Height+pnlSwitch.Height+pnlBO.Height-SbMain.VertScrollBar.Position;
//            end
//            else
//            begin
//              pnlBO.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlDCSpeed.Height-SbMain.VertScrollBar.Position;
//              pnlReturnToAuto.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlDCSpeed.Height+pnlBO.Height-SbMain.VertScrollBar.Position;
//            end;
//          end
//          else
//          begin
//            //pnlDCSpeed.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height-SbMain.VertScrollBar.Position;
//            if isSW then
//            begin
//              pnlSwitch.top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height-SbMain.VertScrollBar.Position;
//              pnlBO.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlSwitch.Height-SbMain.VertScrollBar.Position;
//              //pnlReturnToAuto.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlDCSpeed.Height+pnlSwitch.Height+pnlBO.Height-SbMain.VertScrollBar.Position;
//              pnlReturnToAuto.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlSwitch.Height+pnlBO.Height-SbMain.VertScrollBar.Position;
//            end
//            else
//            begin
//              pnlBO.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height-SbMain.VertScrollBar.Position;
//              pnlReturnToAuto.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlBO.Height-SbMain.VertScrollBar.Position;
//            end;
//          end;
////          pnlBO.Top:=670;
//        end;
      end
      else
      begin
        btnEnabledS.Visible:=False;
        btnEnabledC.Visible:=False;
        btnLEDOn.Visible:=false;
        btnLEDOff.Visible:=false;

        txtLVzgn.Visible:=True;
        txtRevision.Visible:=True;

        cmbled.Visible:=true;
        cmbPortMode.Visible:=True;
        cmbLVtype.Visible:=True;
        cmbFwd.Visible:=True;
        cmbLed.Visible:=True;

        lblUniform.Visible:=true;
        lblSystem.Visible:=true;
        label5.Visible:=true;
        lblMode.Visible:=true;
        label4.Visible:=true;
        Label23.Visible:=True;
        Label24.Visible:=True;
        Label25.Visible:=True;
        label3.Visible:=True;
        lblUniform.Visible:=True;
        label21.Visible:=True;
        Label19.Visible:=False;

        teHostUID.Visible:=False;

        pnlDCSpeed.Visible:=False;
        pnlIQ2DCSpeed.Visible:=False;
        pnlMaintParams.Visible:=false;

        pnlSettings.Visible:=True;
        pnlProgramWink.Visible:=true;
        pnlFirmware.visible:=True;
        pnlSwitch.Visible:=true;
        pnlBO.Visible:=true;

        if((StrToFloat(VariablesUn.BuildFirmwareRev))<7.7) then
        begin
//          Label18.Visible:=False;
//          cbSWEnabled.Visible:=False;
          pnlReturnToAuto.Visible:=False;
        end
        else
        begin
          pnlReturnToAuto.Visible:=true;
//          Label18.Visible:=True;
//          cbSWEnabled.Visible:=True;
          VariablesUn.ClearData;
          giSentCom := StrToInt(VariablesUn.UID_LSE);  //get Enabled Switches
          //ComFm.GetStatus(giUID, VariablesUn.UID_LSE, rhMotorStatusRecieved);
          WaitforResponse(giUID, VariablesUn.UID_LSE, rhMotorStatusRecieved,100);
          if flgHaltStatus then
          begin
            flgHaltStatus:=false;
            Exit;
          end;
          //VariablesUn.delay(100);
        end;

//        if VariablesUn.gbShowSTA then
//        begin
//          pnlLV.Top:= pnlTopZGN.Height+pnlBotZGN.Height-SbMain.VertScrollBar.Position;
//          pnlProgramWink.Top:= pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height-SbMain.VertScrollBar.Position;
//          pnlSettings.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height-SbMain.VertScrollBar.Position;
//          pnlFirmware.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height-SbMain.VertScrollBar.Position;;
//          pnlSwitch.top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlFirmware.Height-SbMain.VertScrollBar.Position;;
//          pnlBO.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlFirmware.Height+pnlSwitch.Height-SbMain.VertScrollBar.Position;
//        end
//        else
//        begin
//
//          pnlLV.Top:= pnlTopZGN.Height-SbMain.VertScrollBar.Position;
//          pnlProgramWink.Top:= pnlTopZGN.Height+pnlLV.Height-SbMain.VertScrollBar.Position;
//          pnlSettings.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height-SbMain.VertScrollBar.Position;
//          pnlFirmware.Top:= pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height-SbMain.VertScrollBar.Position;
//          pnlSwitch.top:= pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlFirmware.Height-SbMain.VertScrollBar.Position;
//          pnlBO.Top:= pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlFirmware.Height+pnlSwitch.Height-SbMain.VertScrollBar.Position;
////          pnlFirmware.Top:=548;
////          pnlSwitch.top:=582;
////          pnlBO.Top:=768;
//        end;
      end;

      if isDC then
      begin
        lblAlias.OnChange:=nil;
        VariablesUn.ClearData;
        giSentCom := StrToInt(VariablesUn.UID_NAME);  //get Alias
        //ComFm.GetStatus(giUID, VariablesUn.UID_NAME, rhMotorStatusRecieved);
        WaitforResponse(giUID, VariablesUn.UID_NAME, rhMotorStatusRecieved,1000);
        if flgHaltStatus then
        begin
          flgHaltStatus:=false;
          Exit;
        end;
        lblAlias.OnChange:=lblAliasChange;
        //get Host UID
        DataUn.dmDataModule.dsMotors.Filtered := False;
        DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt;
        DataUn.dmDataModule.dsMotors.Filtered := True;
        teHostUID.Text:=IntToStr(DataUn.dmDataModule.dsMotors.FieldByName('HostUID').AsInteger);
        if flgHaltStatus then
        begin
          flgHaltStatus:=false;
          Exit;
        end;
      end
      else
      begin
        VariablesUn.ClearData;
        giSentCom := StrToInt(VariablesUn.UID_NAME);  //get Alias
        //ComFm.GetStatus(giUID, VariablesUn.UID_NAME, rhMotorStatusRecieved);
        WaitforResponse(giUID, VariablesUn.UID_NAME, rhMotorStatusRecieved,100);
        if flgHaltStatus then
        begin
          flgHaltStatus:=false;
          Exit;
        end;
        //get OLD MNI data
        if isIQMLC2 then
        begin
          VariablesUn.ClearData;
          giSentCom := StrToInt(VariablesUn.UID_MS1);
          //ComFm.GetStatus(giUID, VariablesUn.UID_MS1, rhMotorStatusRecieved);
          WaitforResponse(giUID, VariablesUn.UID_MS1, rhMotorStatusRecieved,100);
          if flgHaltStatus then
          begin
            flgHaltStatus:=false;
            Exit;
          end;

          VariablesUn.ClearData;
          giSentCom := StrToInt(VariablesUn.UID_MS2);
          //ComFm.GetStatus(giUID, VariablesUn.UID_MS2, rhMotorStatusRecieved);
          WaitforResponse(giUID, VariablesUn.UID_MS2, rhMotorStatusRecieved,100);
          if flgHaltStatus then
          begin
            flgHaltStatus:=false;
            Exit;
          end;
        end;
      end;
      //VariablesUn.delay(100);

      if not gbSaveZGN then
      begin
        if (isMNI)or(isIQMLC2)then
        begin
          if not isIQMLC2 then
          begin
            VariablesUn.ClearData;  //cmd 132.1 Dry Contact Motor Personality
            giSentCom := StrToInt(leftStr(VariablesUn.MNI_MotorPerson,3));
//            ComFm.GetGenericStatus(giUID, VariablesUn.MNI_MotorPerson+'.'+cmbMotorSelector.text+'.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.MNI_MotorPerson+'.'+cmbMotorSelector.text+'.0', rhMotorStatusRecieved,100);
          end;
          //VariablesUn.delay(100);
          if flgHaltStatus then
          begin
            flgHaltStatus:=false;
            Exit;
          end;
          if not isIQMLC2Old then
          begin
            VariablesUn.ClearData;  //cmd 28 Switch Forwarding
            giSentCom := StrToInt(VariablesUn.UID_LSF);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LSF+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LSF+'.0.0.0', rhMotorStatusRecieved,100);
          end;
          //VariablesUn.delay(100);
          if flgHaltStatus then
          begin
            flgHaltStatus:=false;
            Exit;
          end;

          if cmbMotorSelector.text='1' then
          begin
            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS1_BUS5);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS1_BUS5+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS1_BUS5+'.0.0.0', rhMotorStatusRecieved,100);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;

            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS1_BUS6);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS1_BUS6+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS1_BUS6+'.0.0.0', rhMotorStatusRecieved,100);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;

            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS1_BUS7);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS1_BUS7+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS1_BUS7+'.0.0.0', rhMotorStatusRecieved,100);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;

            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS1_BUS8);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS1_BUS8+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS1_BUS8+'.0.0.0', rhMotorStatusRecieved,100);
  //          VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;

            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS1_BUS1);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS1_BUS1+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS1_BUS1+'.0.0.0', rhMotorStatusRecieved,100);
  //          VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;

            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS1_BUS2);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS1_BUS2+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS1_BUS2+'.0.0.0', rhMotorStatusRecieved,100);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;

            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS1_BUS3);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS1_BUS3+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS1_BUS3+'.0.0.0', rhMotorStatusRecieved,100);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;

            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS1_BUS4);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS1_BUS4+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS1_BUS4+'.0.0.0', rhMotorStatusRecieved,100);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;

            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS1_ADDR);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS1_ADDR+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS1_ADDR+'.0.0.0', rhMotorStatusRecieved,100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;
            //VariablesUn.delay(100);
          end
          else if cmbMotorSelector.text='2' then
          begin
            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS2_BUS5);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS2_BUS5+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS2_BUS5+'.0.0.0', rhMotorStatusRecieved,100);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;

            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS2_BUS6);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS2_BUS6+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS2_BUS6+'.0.0.0', rhMotorStatusRecieved,100);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;

            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS2_BUS7);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS2_BUS7+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS2_BUS7+'.0.0.0', rhMotorStatusRecieved,100);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;

            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS2_BUS8);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS2_BUS8+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS2_BUS8+'.0.0.0', rhMotorStatusRecieved,100);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;

            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS2_BUS1);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS2_BUS1+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS2_BUS1+'.0.0.0', rhMotorStatusRecieved,100);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;

            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS2_BUS2);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS2_BUS2+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS2_BUS2+'.0.0.0', rhMotorStatusRecieved,100);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;

            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS2_BUS3);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS2_BUS3+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS2_BUS3+'.0.0.0', rhMotorStatusRecieved,100);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;

            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS2_BUS4);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS2_BUS4+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS2_BUS4+'.0.0.0', rhMotorStatusRecieved,100);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;

            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS2_ADDR);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS2_ADDR+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS2_ADDR+'.0.0.0', rhMotorStatusRecieved,100);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;

          end
          else if cmbMotorSelector.text='3' then
          begin
            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS3_BUS5);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS3_BUS5+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS3_BUS5+'.0.0.0', rhMotorStatusRecieved,100);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;

            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS3_BUS6);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS3_BUS6+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS3_BUS6+'.0.0.0', rhMotorStatusRecieved,100);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;

            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS3_BUS7);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS3_BUS7+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS3_BUS7+'.0.0.0', rhMotorStatusRecieved,100);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;

            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS3_BUS8);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS3_BUS8+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS3_BUS8+'.0.0.0', rhMotorStatusRecieved,100);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;

            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS3_BUS1);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS3_BUS1+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS3_BUS1+'.0.0.0', rhMotorStatusRecieved,100);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;

            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS3_BUS2);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS3_BUS2+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS3_BUS2+'.0.0.0', rhMotorStatusRecieved,100);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;

            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS3_BUS3);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS3_BUS3+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS3_BUS3+'.0.0.0', rhMotorStatusRecieved,100);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;

            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS3_BUS4);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS3_BUS4+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS3_BUS4+'.0.0.0', rhMotorStatusRecieved,100);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;

            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS3_ADDR);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS3_ADDR+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS3_ADDR+'.0.0.0', rhMotorStatusRecieved,100);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;
          end
          else if cmbMotorSelector.text='4' then
          begin
            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS4_BUS5);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS4_BUS5+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS4_BUS5+'.0.0.0', rhMotorStatusRecieved,100);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;

            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS4_BUS6);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS4_BUS6+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS4_BUS6+'.0.0.0', rhMotorStatusRecieved,100);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;

            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS4_BUS7);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS4_BUS7+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS4_BUS7+'.0.0.0', rhMotorStatusRecieved,100);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;

            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS4_BUS8);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS4_BUS8+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS4_BUS8+'.0.0.0', rhMotorStatusRecieved,100);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;

            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS4_BUS1);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS4_BUS1+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS4_BUS1+'.0.0.0', rhMotorStatusRecieved,100);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;

            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS4_BUS2);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS4_BUS2+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS4_BUS2+'.0.0.0', rhMotorStatusRecieved,100);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;

            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS4_BUS3);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS4_BUS3+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS4_BUS3+'.0.0.0', rhMotorStatusRecieved,100);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;

            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS4_BUS4);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS4_BUS4+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS4_BUS4+'.0.0.0', rhMotorStatusRecieved,100);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;

            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS4_ADDR);
//            ComFm.GetGenericStatus(giUID, VariablesUn.UID_LS4_ADDR+'.0.0.0', rhMotorStatusRecieved);
            WaitforGenericResponse(giUID, VariablesUn.UID_LS4_ADDR+'.0.0.0', rhMotorStatusRecieved,100);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;

          end;
        end
        else
        begin
          VariablesUn.ClearData;
          giSentCom := StrToInt(VariablesUn.UID_LS1_BUS5);
          //ComFm.GetStatus(giUID, VariablesUn.UID_LS1_BUS5, rhMotorStatusRecieved);
          WaitforResponse(giUID, VariablesUn.UID_LS1_BUS5, rhMotorStatusRecieved,100);
          //VariablesUn.delay(100);
          if flgHaltStatus then
          begin
            flgHaltStatus:=false;
            Exit;
          end;

          VariablesUn.ClearData;
          giSentCom := StrToInt(VariablesUn.UID_LS1_BUS6);
          //ComFm.GetStatus(giUID, VariablesUn.UID_LS1_BUS6, rhMotorStatusRecieved);
          WaitforResponse(giUID, VariablesUn.UID_LS1_BUS6, rhMotorStatusRecieved,100);
          //VariablesUn.delay(100);
          if flgHaltStatus then
          begin
            flgHaltStatus:=false;
            Exit;
          end;

          VariablesUn.ClearData;
          giSentCom := StrToInt(VariablesUn.UID_LS1_BUS7);
          //ComFm.GetStatus(giUID, VariablesUn.UID_LS1_BUS7, rhMotorStatusRecieved);
          WaitforResponse(giUID, VariablesUn.UID_LS1_BUS7, rhMotorStatusRecieved,100);
          //VariablesUn.delay(100);
          if flgHaltStatus then
          begin
            flgHaltStatus:=false;
            Exit;
          end;

          VariablesUn.ClearData;
          giSentCom := StrToInt(VariablesUn.UID_LS1_BUS8);
          //ComFm.GetStatus(giUID, VariablesUn.UID_LS1_BUS8, rhMotorStatusRecieved);
          WaitforResponse(giUID, VariablesUn.UID_LS1_BUS8, rhMotorStatusRecieved,100);
          //VariablesUn.delay(100);
          if flgHaltStatus then
          begin
            flgHaltStatus:=false;
            Exit;
          end;

          VariablesUn.ClearData;
          giSentCom := StrToInt(VariablesUn.UID_LS1_BUS1);
          //ComFm.GetStatus(giUID, VariablesUn.UID_LS1_BUS1, rhMotorStatusRecieved);
          WaitforResponse(giUID, VariablesUn.UID_LS1_BUS1, rhMotorStatusRecieved,100);
          //VariablesUn.delay(100);
          if flgHaltStatus then
          begin
            flgHaltStatus:=false;
            Exit;
          end;

          VariablesUn.ClearData;
          giSentCom := StrToInt(VariablesUn.UID_LS1_BUS2);
          //ComFm.GetStatus(giUID, VariablesUn.UID_LS1_BUS2, rhMotorStatusRecieved);
          WaitforResponse(giUID, VariablesUn.UID_LS1_BUS2, rhMotorStatusRecieved,100);
          //VariablesUn.delay(100);
          if flgHaltStatus then
          begin
            flgHaltStatus:=false;
            Exit;
          end;

          VariablesUn.ClearData;
          giSentCom := StrToInt(VariablesUn.UID_LS1_BUS3);
          //ComFm.GetStatus(giUID, VariablesUn.UID_LS1_BUS3, rhMotorStatusRecieved);
          WaitforResponse(giUID, VariablesUn.UID_LS1_BUS3, rhMotorStatusRecieved,100);
          //VariablesUn.delay(100);
          if flgHaltStatus then
          begin
            flgHaltStatus:=false;
            Exit;
          end;

          VariablesUn.ClearData;
          giSentCom := StrToInt(VariablesUn.UID_LS1_BUS4);
          //ComFm.GetStatus(giUID, VariablesUn.UID_LS1_BUS4, rhMotorStatusRecieved);
          WaitforResponse(giUID, VariablesUn.UID_LS1_BUS4, rhMotorStatusRecieved,100);
          //VariablesUn.delay(100);
          if (isSW) or not (isdc) then
          begin
            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_LS1_ADDR);
            //ComFm.GetStatus(giUID, VariablesUn.UID_LS1_ADDR, rhMotorStatusRecieved);
            WaitforResponse(giUID, VariablesUn.UID_LS1_ADDR, rhMotorStatusRecieved,100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;
          end;
        end;

        if not vbSkipNSM then
        begin
          pnlMasterUID.Visible:=True;
          pnlReturnToAuto.Visible:=True;
//          if VariablesUn.gbShowSTA then
//          begin
//            pnlReturnToAuto.top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlFirmware.Height+pnlSwitch.Height+pnlBO.Height-SbMain.VertScrollBar.Position;
//            pnlMasterUID.top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlFirmware.Height+pnlSwitch.Height+pnlBO.Height+pnlReturnToAuto.Height-SbMain.VertScrollBar.Position;
//          end
//          else
//          begin
//            pnlReturnToAuto.top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlFirmware.Height+pnlSwitch.Height+pnlBO.Height-SbMain.VertScrollBar.Position;
//            pnlMasterUID.top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlFirmware.Height+pnlSwitch.Height+pnlBO.Height+pnlReturnToAuto.Height-SbMain.VertScrollBar.Position;
//
//          end;

          VariablesUn.ClearData;
          giSentCom := StrToInt(VariablesUn.UID_AutoReturn);
          vbSkipAutoReturnUpdate:=True;
          //ComFm.GetStatus(giUID, VariablesUn.UID_AutoReturn, rhMotorStatusRecieved);
          WaitforResponse(giUID, VariablesUn.UID_AutoReturn, rhMotorStatusRecieved,200);
          if flgHaltStatus then
          begin
            flgHaltStatus:=false;
            Exit;
          end;

          VariablesUn.ClearData;
          giSentCom := StrToInt(VariablesUn.UID_MasterUID);
          vbSkipAutoReturnUpdate:=True;
          //ComFm.GetStatus(giUID, VariablesUn.UID_MasterUID, rhMotorStatusRecieved);
          WaitforResponse(giUID, VariablesUn.UID_MasterUID, rhMotorStatusRecieved,200);
          if flgHaltStatus then
          begin
            flgHaltStatus:=false;
            Exit;
          end;

        end
        else if isdc or isIQ3dc then
        begin
          pnlReturnToAuto.Visible:=True;
//          if VariablesUn.gbShowSTA then
//          begin
//            //pnlReturnToAuto.top:=778+pnlBotZGN.Height;
//            pnlReturnToAuto.top:=pnlBO.Top+pnlBO.height+pnlBotZGN.Height;
//          end
//          else
//          begin
////            pnlReturnToAuto.top:=778;
//            pnlReturnToAuto.top:=pnlBO.Top+pnlBO.height;
//          end;


          VariablesUn.ClearData;
          giSentCom := StrToInt(VariablesUn.UID_AutoReturn);
          vbSkipAutoReturnUpdate:=True;
          //ComFm.GetStatus(giUID, VariablesUn.UID_AutoReturn, rhMotorStatusRecieved);
          WaitforResponse(giUID, VariablesUn.UID_AutoReturn, rhMotorStatusRecieved,200);
          if flgHaltStatus then
          begin
            flgHaltStatus:=false;
            Exit;
          end;
        end
        else
        begin
          pnlMasterUID.Visible:=false;

        end;
      end;

      VariablesUn.ClearData;
      if not isIQMLC2Old then
      begin
        giSentCom := StrToInt(VariablesUn.UID_TYPE);
        //ComFm.GetStatus(giUID, VariablesUn.UID_TYPE, rhMotorStatusRecieved);
        WaitforResponse(giUID, VariablesUn.UID_TYPE, rhMotorStatusRecieved,200);
      end;
      if flgHaltStatus then
      begin
        flgHaltStatus:=false;
        Exit;
      end;
      //VariablesUn.delay(100);

      if (isMNI)or(isIQMLC2) then     //Need to get all switch data like this command
      begin
        if not isIQMLC2Old then
        begin
          if not (isIQMLC2) then
          begin
            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.MNI_MNISettings);
            //ComFm.GetStatus(giUID, VariablesUn.MNI_MNISettings, rhMotorStatusRecieved);
            WaitforResponse(giUID, VariablesUn.MNI_MNISettings, rhMotorStatusRecieved,200);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;
          end
          else
          begin
            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_NU);
            //ComFm.GetStatus(giUID, VariablesUn.UID_NU, rhMotorStatusRecieved);
            WaitforResponse(giUID, VariablesUn.UID_NU, rhMotorStatusRecieved,200);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;
          end;

          VariablesUn.ClearData;
          giSentCom := StrToInt(VariablesUn.UID_MTA_IN);
          //ComFm.GetStatus(giUID, VariablesUn.UID_MTA_IN, rhMotorStatusRecieved);
          WaitforResponse(giUID, VariablesUn.UID_MTA_IN, rhMotorStatusRecieved,200);
          //VariablesUn.delay(100);
          if flgHaltStatus then
          begin
            flgHaltStatus:=false;
            Exit;
          end;


          VariablesUn.ClearData;
          giSentCom := StrToInt(VariablesUn.UID_Reverse);
          //ComFm.GetStatus(giUID, VariablesUn.UID_Reverse, rhMotorStatusRecieved);
          WaitforResponse(giUID, VariablesUn.UID_Reverse, rhMotorStatusRecieved,200);
          //VariablesUn.delay(100);
          if flgHaltStatus then
          begin
            flgHaltStatus:=false;
            Exit;
          end;
          //IS/LS
          VariablesUn.ClearData;
          giSentCom := StrToInt(VariablesUn.UID_PortMode);
          //ComFm.GetStatus(giUID, VariablesUn.UID_PortMode, rhMotorStatusRecieved);
          WaitforResponse(giUID, VariablesUn.UID_PortMode, rhMotorStatusRecieved,200);
          //VariablesUn.delay(100);
          if flgHaltStatus then
          begin
            flgHaltStatus:=false;
            Exit;
          end;
          //Inhibit Flag
          VariablesUn.ClearData;
          giSentCom := StrToInt(VariablesUn.UID_MTA_Flag);
          //ComFm.GetStatus(giUID, VariablesUn.UID_MTA_Flag, rhMotorStatusRecieved);
          WaitforResponse(giUID, VariablesUn.UID_MTA_Flag, rhMotorStatusRecieved,200);
          //VariablesUn.delay(100);
          if flgHaltStatus then
          begin
            flgHaltStatus:=false;
            Exit;
          end;
          //LED INFO
          VariablesUn.ClearData;
          giSentCom := StrToInt(VariablesUn.UID_LED);
          //ComFm.GetStatus(giUID, VariablesUn.UID_LED, rhMotorStatusRecieved);
          WaitforResponse(giUID, VariablesUn.UID_LED, rhMotorStatusRecieved,200);
          //VariablesUn.delay(100);
          if flgHaltStatus then
          begin
            flgHaltStatus:=false;
            Exit;
          end;

          if not(isIQMLC2()) then
          begin
            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_Main);
            //ComFm.GetStatus(giUID, VariablesUn.UID_Main, rhMotorStatusRecieved);
            WaitforResponse(giUID, VariablesUn.UID_Main, rhMotorStatusRecieved,200);
            //VariablesUn.delay(100);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;
          end;

          VariablesUn.ClearData;
          giSentCom := StrToInt(VariablesUn.UID_BP);
//          ComFm.GetGenericStatus(giUID, VariablesUn.UID_BP+'.'+cmbMotorSelector.text+'.0.0', rhMotorStatusRecieved);
          WaitforGenericResponse(giUID, VariablesUn.UID_BP+'.'+cmbMotorSelector.text+'.0.0', rhMotorStatusRecieved,200);
          //VariablesUn.delay(100);
          if flgHaltStatus then
          begin
            flgHaltStatus:=false;
            Exit;
          end;
        end;
      end
      else
      begin
        VariablesUn.ClearData;
        giSentCom := StrToInt(VariablesUn.UID_IQMotorSet);
        //ComFm.GetStatus(giUID, VariablesUn.UID_IQMotorSet, rhMotorStatusRecieved);
        WaitforResponse(giUID, VariablesUn.UID_IQMotorSet, rhMotorStatusRecieved,200);
        if flgHaltStatus then
        begin
          flgHaltStatus:=false;
          Exit;
        end;
        //VariablesUn.delay(100);
      end;

      if isDC then
      begin
        //UpRollerSpeed
        teUpSpeed.OnChange:=Nil;
        teUpSpeedFine.OnChange:=Nil;
        VariablesUn.ClearData;
        giSentCom := StrToInt(LeftStr(VariablesUn.DC1_UpRollerSpeed,3));
        //ComFm.GetStatus(giUID, VariablesUn.DC1_UpRollerSpeed, rhMotorStatusRecieved);
        WaitforResponse(giUID, VariablesUn.DC1_UpRollerSpeed, rhMotorStatusRecieved,200);
        teUpSpeed.OnChange:=teUpSpeedChange;
        teUpSpeedFine.OnChange:=teUpSpeedChange;
        if flgHaltStatus then
        begin
          flgHaltStatus:=false;
          Exit;
        end;

        //DnRollerSpeed
        teDnSpeed.OnChange:=Nil;
        teDnSpeedFine.OnChange:=Nil;
        VariablesUn.ClearData;
        giSentCom := StrToInt(LeftStr(VariablesUn.DC1_DnRollerSpeed,3));
        //ComFm.GetStatus(giUID, VariablesUn.DC1_DnRollerSpeed, rhMotorStatusRecieved);
        WaitforResponse(giUID, VariablesUn.DC1_DnRollerSpeed, rhMotorStatusRecieved,200);
        teDnSpeed.OnChange:=teDNSpeedChange;
        teDnSpeedFine.OnChange:=teDNSpeedChange;
        if flgHaltStatus then
        begin
          flgHaltStatus:=false;
          Exit;
        end;

        //SLowSPeed
        teSlowSpeed.OnChange:=Nil;
        teSlowSpeedFine.OnChange:=Nil;
        VariablesUn.ClearData;
        giSentCom := StrToInt(LeftStr(VariablesUn.DC1_SlowSpeed,3));
        //ComFm.GetStatus(giUID, VariablesUn.DC1_SlowSpeed, rhMotorStatusRecieved);
        WaitforResponse(giUID, VariablesUn.DC1_SlowSpeed, rhMotorStatusRecieved,200);
        teSlowSpeed.OnChange:=teSlowSpeedChange;
        teSlowSpeedFine.OnChange:=teSlowSpeedChange;
        if flgHaltStatus then
        begin
          flgHaltStatus:=false;
          Exit;
        end;

        //RampPeriodUp
        teRampPeriodUP.OnChange:=Nil;
        VariablesUn.ClearData;
        giSentCom := StrToInt(LeftStr(VariablesUn.DC1_RampPeriod,3));
//        ComFm.GetGenericStatus(giUID, VariablesUn.DC1_RampPeriod+'.0.0', rhMotorStatusRecieved);
        WaitforGenericResponse(giUID, VariablesUn.DC1_RampPeriod+'.0.0', rhMotorStatusRecieved,200);
        teRampPeriodUP.OnChange:=teRampPeriodUpChange;
        if flgHaltStatus then
        begin
          flgHaltStatus:=false;
          Exit;
        end;

        //RampPeriodDn
        teRampPeriodDn.OnChange:=Nil;
        VariablesUn.ClearData;
        giSentCom := StrToInt(LeftStr(VariablesUn.DC1_RampPeriod,3));
        //ComFm.GetStatus(giUID, VariablesUn.DC1_RampPeriod+'.1.0', rhMotorStatusRecieved);
        WaitforResponse(giUID, VariablesUn.DC1_RampPeriod+'.1.0', rhMotorStatusRecieved,200);
        teRampPeriodDn.OnChange:=teRampPeriodDnChange;
        if flgHaltStatus then
        begin
          flgHaltStatus:=false;
          Exit;
        end;
      end;

      if (isIQ2DC) or (isIQ3DC) then
      begin
        //UpRollerSpeed
        teIQ2DCUpSpeed.OnChange:=Nil;
        teIQ2DCUpSpeedFine.OnChange:=Nil;
        VariablesUn.ClearData;
        giSentCom := StrToInt(LeftStr(VariablesUn.IQ2DC_UpRollerSpeed,3));
        //ComFm.GetStatus(giUID, VariablesUn.IQ2DC_UpRollerSpeed, rhMotorStatusRecieved);
        WaitforResponse(giUID, VariablesUn.IQ2DC_UpRollerSpeed, rhMotorStatusRecieved,200);
        teIQ2DCUpSpeed.OnChange:=teIQ2DCUpSpeedChange;
        teIQ2DCUpSpeedFine.OnChange:=teIQ2DCUpSpeedChange;
        if flgHaltStatus then
        begin
          flgHaltStatus:=false;
          Exit;
        end;

        //DnRollerSpeed
        teIQ2DCDnSpeed.OnChange:=Nil;
        teIQ2DCDnSpeedFine.OnChange:=Nil;
        VariablesUn.ClearData;
        giSentCom := StrToInt(LeftStr(VariablesUn.IQ2DC_DnRollerSpeed,3));
        //ComFm.GetStatus(giUID, VariablesUn.IQ2DC_DnRollerSpeed, rhMotorStatusRecieved);
        WaitforResponse(giUID, VariablesUn.IQ2DC_DnRollerSpeed, rhMotorStatusRecieved,200);
        teIQ2DCDnSpeed.OnChange:=teIQ2DCDNSpeedChange;
        teIQ2DCDnSpeedFine.OnChange:=teIQ2DCDNSpeedChange;
        if flgHaltStatus then
        begin
          flgHaltStatus:=false;
          Exit;
        end;

        //Add with new Ross Firmware

        //UpSlowSpeed
        teIQ2DCSlowUpSpeed.OnChange:=Nil;
        teIQ2DCSlowUpSpeedFine.OnChange:=Nil;
        VariablesUn.ClearData;
        giSentCom := StrToInt(LeftStr(VariablesUn.IQ2DC_UpSlowSpeed,3));
//        ComFm.GetStatus(giUID, VariablesUn.IQ2DC_UpSlowSpeed, rhMotorStatusRecieved);
        WaitforResponse(giUID, VariablesUn.IQ2DC_UpSlowSpeed, rhMotorStatusRecieved,200);
        teIQ2DCSlowUpSpeed.OnChange:=teIQ2DCSlowUpSpeedChange;
        teIQ2DCSlowUpSpeedFine.OnChange:=teIQ2DCSlowUpSpeedChange;
        if flgHaltStatus then
        begin
          flgHaltStatus:=false;
          Exit;
        end;

        //DnSlowSpeed
        teIQ2DCSlowDnSpeed.OnChange:=Nil;
        teIQ2DCSlowDnSpeedFine.OnChange:=Nil;
        VariablesUn.ClearData;
        giSentCom := StrToInt(LeftStr(VariablesUn.IQ2DC_DnSlowSpeed,3));
//        ComFm.GetStatus(giUID, VariablesUn.IQ2DC_DnSlowSpeed, rhMotorStatusRecieved);
        WaitforResponse(giUID, VariablesUn.IQ2DC_DnSlowSpeed, rhMotorStatusRecieved,200);
        teIQ2DCSlowDnSpeed.OnChange:=teIQ2DCSlowDnSpeedChange;
        teIQ2DCSlowDnSpeedFine.OnChange:=teIQ2DCSlowDnSpeedChange;
        if flgHaltStatus then
        begin
          flgHaltStatus:=false;
          Exit;
        end;

        //RampPeriodUp
        teIQ2DCRampUpTime.OnChange:=Nil;
        teIQ2DCRampUpTimeFine.OnChange:=Nil;
        VariablesUn.ClearData;
        giSentCom := StrToInt(LeftStr(VariablesUn.IQ2DC_UpRampTime,3));
        //ComFm.GetGenericStatus(giUID, VariablesUn.IQ2DC_UpRampTime+'.0.0', rhMotorStatusRecieved);
        WaitforResponse(giUID, VariablesUn.IQ2DC_UpRampTime+'.0.0', rhMotorStatusRecieved,200);
        teIQ2DCRampUpTime.OnChange:=teIQ2DCRampUpTimeChange;
        teIQ2DCRampUpTimeFine.OnChange:=teIQ2DCRampUpTimeFineChange;
        if flgHaltStatus then
        begin
          flgHaltStatus:=false;
          Exit;
        end;

        //RampPeriodDn
        teIQ2DCRampDnTime.OnChange:=Nil;
        teIQ2DCRampDnTimeFine.OnChange:=Nil;
        VariablesUn.ClearData;
        giSentCom := StrToInt(LeftStr(VariablesUn.IQ2DC_DnRampTime,3));
       // ComFm.GetStatus(giUID, VariablesUn.IQ2DC_DnRampTime+'.1.0', rhMotorStatusRecieved);
        WaitforResponse(giUID, VariablesUn.IQ2DC_DnRampTime+'.1.0', rhMotorStatusRecieved,200);
        teIQ2DCRampDnTime.OnChange:=teIQ2DCRampDnTimeChange;
        teIQ2DCRampDnTimeFine.OnChange:=teIQ2DCRampDnTimeFineChange;
        if flgHaltStatus then
        begin
          flgHaltStatus:=false;
          Exit;
        end;

        VariablesUn.ClearData;
        giSentCom := StrToInt(VariablesUn.UID_AutoReturn);
        teAutoReturnIQ2.OnChange:=nil;
//        vbSkipAutoReturnUpdate:=True;
        //ComFm.GetStatus(giUID, VariablesUn.UID_AutoReturn, rhMotorStatusRecieved);
        WaitforResponse(giUID, VariablesUn.UID_AutoReturn, rhMotorStatusRecieved,200);
        if flgHaltStatus then
        begin
          flgHaltStatus:=false;
          Exit;
        end;
        teAutoReturnIQ2.OnChange:=teAutoReturnIQ2Change;

        //Get Thermistor params
        teInternalThreshold.OnChange:=nil;
        teExternalThreshold.OnChange:=nil;
        VariablesUn.ClearData;
        giSentCom := StrToInt(VariablesUn.IQ2DC_Cutoff_Thresholds);
        if WaitforGenericResponse(giUID, VariablesUn.IQ2DC_Cutoff_Thresholds+'.1.0.0', rhMotorStatusRecieved,200) then
        begin
          teInternalThreshold.Text := IntToStr(VariablesUn.giDataM);
          teExternalThreshold.Text := IntToStr(VariablesUn.giDataL);
        end;
        teInternalThreshold.OnChange:=teInternalThresholdChange;
        teExternalThreshold.OnChange:=teExternalThresholdChange;
        if flgHaltStatus then
        begin
          flgHaltStatus:=false;
          Exit;
        end;

        //Get Accelerometer params
        teMotionThreshold.OnChange:=Nil;
        VariablesUn.ClearData;
        giSentCom := StrToInt(LeftStr(VariablesUn.IQ2DC_Cutoff_Thresholds,3));
        if WaitforGenericResponse(giUID, VariablesUn.IQ2DC_Cutoff_Thresholds+'.0.0.0', rhMotorStatusRecieved,200) then
        begin
          teMotionThreshold.Text := IntToStr(VariablesUn.DataMidLowConvert);
        end;
        teMotionThreshold.OnChange:=teMotionThresholdChange;
        if flgHaltStatus then
        begin
          flgHaltStatus:=false;
          Exit;
        end;


      end;


      if not gbSaveLV then
      begin
        if (isMNI) then     //Blackout data for all motors
        begin
          if cmbMotorSelector.Text='1' then
          begin
            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_BLCKOUT_ADR);
            //ComFm.GetStatus(giUID, VariablesUn.UID_BLCKOUT_ADR, rhMotorStatusRecieved);
            WaitforResponse(giUID, VariablesUn.UID_BLCKOUT_ADR, rhMotorStatusRecieved,200);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;
            //VariablesUn.delay(200);
          end
          else if cmbMotorSelector.Text='2' then
          begin
            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_BLCKOUT2_ADR);
            //ComFm.GetStatus(giUID, VariablesUn.UID_BLCKOUT2_ADR, rhMotorStatusRecieved);
            WaitforResponse(giUID, VariablesUn.UID_BLCKOUT2_ADR, rhMotorStatusRecieved,200);
            //VariablesUn.delay(200);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;
          end
          else if cmbMotorSelector.Text='3' then
          begin
            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_BLCKOUT3_ADR);
            //ComFm.GetStatus(giUID, VariablesUn.UID_BLCKOUT3_ADR, rhMotorStatusRecieved);
            WaitforResponse(giUID, VariablesUn.UID_BLCKOUT3_ADR, rhMotorStatusRecieved,200);
            //VariablesUn.delay(200);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;
          end
          else if cmbMotorSelector.Text='4' then
          begin
            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_BLCKOUT4_ADR);
            //ComFm.GetStatus(giUID, VariablesUn.UID_BLCKOUT4_ADR, rhMotorStatusRecieved);
            WaitforResponse(giUID, VariablesUn.UID_BLCKOUT4_ADR, rhMotorStatusRecieved,200);
            //VariablesUn.delay(200);
            if flgHaltStatus then
            begin
              flgHaltStatus:=false;
              Exit;
            end;
          end;
        end
        else
        begin
          if not isIQMLC2 then
          begin
            VariablesUn.ClearData;
            giSentCom := StrToInt(VariablesUn.UID_BLCKOUT_ADR);
            //ComFm.GetStatus(giUID, VariablesUn.UID_BLCKOUT_ADR, rhMotorStatusRecieved);
            WaitforResponse(giUID, VariablesUn.UID_BLCKOUT_ADR, rhMotorStatusRecieved,200);
          end;
          //VariablesUn.delay(200);
          if flgHaltStatus then
          begin
            flgHaltStatus:=false;
            Exit;
          end;
        end;
      end;
      if not isIQMLC2 then
      begin
        VariablesUn.ClearData;
        giSentCom := StrToInt(VariablesUn.UID_BLCKOUT);
        //ComFm.GetStatus(giUID, VariablesUn.UID_BLCKOUT, rhMotorStatusRecieved);
        WaitforResponse(giUID, VariablesUn.UID_BLCKOUT, rhMotorStatusRecieved,200);
        if flgHaltStatus then
        begin
          flgHaltStatus:=false;
          Exit;
        end;
      end;
      //VariablesUn.delay(200);
  //
  //    VariablesUn.ClearData;
  //    giSentCom := StrToInt(VariablesUn.UID_NAME);
  //    ComFm.GetStatus(giUID, VariablesUn.UID_NAME, rhMotorStatusRecieved);
  //    VariablesUn.delay(200);

      rhEnable;
      for I := 0 to sbmain.ControlCount-1 do
      begin
        if (sbmain.Controls[I].visible) and (sbmain.Controls[I].ClassType=tPanel)then
        begin
          ScrollHeight:=ScrollHeight+sbmain.Controls[I].Height;
        end;

      end;
      sbmain.VertScrollBar.Range:=ScrollHeight+40;
      //sbmain.VertScrollBar.Range:=ScrollHeight+200;
      outputdebugstring(pchar('BOTTOM'+inttostr(sbmain.VertScrollBar.Position)));

    end;
  except
    on E: Exception do
    begin
      Screen.Cursor := crDefault;
      //gbSaveZGN := False;
      rhEnable;
      MessageDlg('TConfigScreenFm.GetMotorStatus: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

function TConfigScreenFm.IsMNI:Boolean;
var
  tempStr:String;
begin
  try
    if pos('MNI',cmbUID.text)=0 then
    begin
//      if cmbMotorSelector.Visible=false then
//      begin
//
//      end
//      else
//      begin
//        IsMNI:=True;
//      end;
//      if not (pos(' ',cmbUID.text)=0) then
//      begin
//        tempStr:=LeftStr(cmbUID.text,pos(' ',cmbUID.text)-1);
//      end
//      else
//      begin
//        tempStr:=cmbUID.text;
//      end;
      dmDataModule.dsMNI.Filtered := False;
      dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + gsUIDCrnt;
      dmDataModule.dsMNI.Filtered := True;
      if dmDataModule.dsMNI.eof then
      begin
        IsMNI:=false;
      end
      else
      begin
        IsMNI:=true;
      end;
    end
    else
    begin
      IsMNI:=True;
    end;
  except
    on E: Exception do
    begin
      IsMNI:=false;
      MessageDlg('TConfigScreenFm.IsMNI: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

function TConfigScreenFm.GtoZGN2(vsG: String): String;
var
  GtoZGN2:String;
begin
  try
    GtoZGN2 := '255.' + vsG + '.255';
    result:=GtoZGN2;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.GtoZGN2: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.HideAddress;
begin
  try
    pnlBotZGN.Visible:=false;
//    pnlLV.Top:=128;
//    pnlProgramWink.Top:=162;
//    pnlSettings.Top:=216;
//    pnlDCSpeed.Top:=545;
//    pnlFirmWare.top:=667;
//    pnlSwitch.top:=701;
//    pnlBO.Top:=887;
//    pnlReturnToAuto.Top:=1000;
//    pnlMasterUID.Top:=1040;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.HideAddress: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.Image1MouseEnter(Sender: TObject);
begin
  try
    pnlMenuBar.Visible := True;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.Image1MouseEnter: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnSettingsClick(Sender: TObject);
begin
  try
    ScreenSetupFm.rh_showmain := rhShowConfig;
    pnlMenubar.Visible := False;
    Hide;
    ScreenSetupFm.Show;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.Image6Click: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.imgWinkDnClick(Sender: TObject);
begin
  try
  UIDWink(2);
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.imgWinkDnClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.imgWinkDnMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  try
    if TImage(Sender).Enabled then
    begin
      TImage(Sender).Visible := False;
      imgWinkUp.Visible := False;
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.imgWinkDnMouseDown: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.imgWinkDnMouseLeave(Sender: TObject);
begin
  try
    TImage(Sender).Visible := True;
    imgWinkUp.Visible := True;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.imgWinkDnMouseLeave: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.imgWinkDnMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  try
    TImage(Sender).Visible := True;
    imgWinkUp.Visible := True;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.imgWinkDnMouseUp: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.imgWinkDnZgnClick(Sender: TObject);
begin
  try
    ZGNWink(2);
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.imgWinkDnZgnClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.imgWinkDnZgnMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  try
    if TImage(Sender).Enabled then
    begin
      TImage(Sender).Visible := False;
      imgWinkUpZGN.Visible := False;
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.imgWinkDnZgnMouseDown: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.imgWinkDnZgnMouseLeave(Sender: TObject);
begin
  try
    TImage(Sender).Visible := True;
    imgWinkUpZGN.Visible := True;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.imgWinkDnZgnMouseLeave: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.imgWinkDnZgnMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  try
    TImage(Sender).Visible := True;
    imgWinkUpZGN.Visible := True;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.imgWinkDnZgnMouseUp: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.imgWinkUpClick(Sender: TObject);
begin
  try
    try
      UIDWink(2);
    finally
      imgWinkUp.Visible := True;
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.imgWinkUpClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.imgWinkUpMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  try
    if TImage(Sender).Enabled then
    begin
      TImage(Sender).Visible := False;
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.imgWinkUpMouseDown: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.imgWinkUpZGNClick(Sender: TObject);
begin
  try
    try
      ZGNWink(2);
    finally
      imgWinkUpZGN.Visible := True;
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.imgWinkUpZGNClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.imgWinkUpZGNMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  try
    if TImage(Sender).Enabled then
    begin
      TImage(Sender).Visible := False;
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.imgWinkUpZGNMouseDown: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.imgWinkUpZGNMouseLeave(Sender: TObject);
begin
  try
    TImage(Sender).Visible := True;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.imgWinkUpZGNMouseLeave: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.imgWinkUpZGNMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  try
    TImage(Sender).Visible := True;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.imgWinkUpZGNMouseUp: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.lblAliasChange(Sender: TObject);
begin
  btnAliasS.Visible := True;
  btnAliasC.Visible := True;
  //lblAlias.Color:=clLime;
end;

procedure TConfigScreenFm.lblAliasClick(Sender: TObject);
begin
  lblAlias.SelectAll;
end;

procedure TConfigScreenFm.lblAliasDblClick(Sender: TObject);
begin
   NewProjectFm.rh_ShowLast := rhShowConfig;
   NewProjectFm.Show;
   Hide;
end;

procedure TConfigScreenFm.lblAliasKeyPress(Sender: TObject; var Key: Char);
begin
//  if key = #$D then
//  begin
//    txtAliasEnterKey(self);
//  end;
end;

procedure TConfigScreenFm.lblAliasMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TConfigScreenFm.lblProjectNameMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TConfigScreenFm.txtAliasEnterKey(Sender: TObject);
var
  vsAlias,vsCommand:String;
  I:Integer;
begin
  try
    if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
    begin
      if vsOldAlias<>trim(lblAlias.text) then
      begin
        vsOldAlias:=trim(lblAlias.text);
        giUID :=  StrToInt(GetUID(cmbUID.text));
        vsAlias:=lblAlias.Text;
        vsAlias := vsAlias + StringOfChar ( Char(#32), 12 - Length(vsAlias) );
        giSentCom:=StrToInt(VariablesUn.UID_NAME);
        vsCommand:=VariablesUn.UID_NAME+'.'+vsAlias;
        //ComFm.SetGenericParam(giUID,vsCommand, rhMotorStatusRecieved);
        WaitforSetResponse(giUID, vsCommand, rhMotorStatusRecieved,1000);
        //Delay(200);
        if (isMNI)or(isIQMLC2) then
        begin
          for I := 1 to 4 do
          begin
            DataUn.dmDataModule.dsMotors.Filtered := False;
            DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt+ ' and MotorNumber = '+IntToStr(I);
            DataUn.dmDataModule.dsMotors.Filtered := True;
            if not (DataUn.dmDataModule.dsMotors.Eof) then
            begin
              DataUn.dmDataModule.dsMotors.Edit;
              DataUn.dmDataModule.dsMotors.FieldByName('Alias').AsString := lblAlias.Text;
              VariablesUn.gsAlias := lblAlias.Text;
              DataUn.dmDataModule.dsMotors.Post;
              DataUn.dmDataModule.dsMotors.MergeChangeLog;
              DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);

            end;
          end;
          DataUn.dmDataModule.dsMNI.Filtered := False;
          DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt;
          DataUn.dmDataModule.dsMNI.Filtered := True;
          if not (DataUn.dmDataModule.dsMNI.Eof) then
          begin
            DataUn.dmDataModule.dsMNI.Edit;
            DataUn.dmDataModule.dsMNI.FieldByName('Alias').AsString := lblAlias.Text;
            DataUn.dmDataModule.dsMNI.Post;
            DataUn.dmDataModule.dsMNI.MergeChangeLog;
            DataUn.dmDataModule.dsMNI.SaveToFile(VariablesUn.gsDataPath + 'dsMNI.txt', dfXML);

          end;
        end
        else
        begin
          DataUn.dmDataModule.dsMotors.Filtered := False;
          DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt;
          DataUn.dmDataModule.dsMotors.Filtered := True;

          if not (DataUn.dmDataModule.dsMotors.Eof) then
          begin
            DataUn.dmDataModule.dsMotors.Edit;
            DataUn.dmDataModule.dsMotors.FieldByName('Alias').AsString := lblAlias.Text;
            VariablesUn.gsAlias := lblAlias.Text;
            DataUn.dmDataModule.dsMotors.Post;
            DataUn.dmDataModule.dsMotors.MergeChangeLog;
            DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
          end;

          DataUn.dmDataModule.dsMotors.Filtered := False;
        end;
      end;
    end
    else
      lblAlias.Text := '';
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.lblAliasChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  End;

end;

procedure TConfigScreenFm.UIDMoveDwn;
var vsHi, vsLo, vsCommand: String;
begin
  try
    if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
    begin
      vsHi := IntToStr(Hi(StrToInt(GetUID(cmbUID.text))));
      vsLo := IntToStr(Lo(StrToInt(GetUID(cmbUID.text))));
      if (isMNI)or(isIQMLC2) then
      begin
        if CmbMotorSelector.Text = '1' then
        begin
          vsCommand := '>' + vsHi + '.' + vsLo + '.' + UID_DN + '.0.0.1';
        end
        else if CmbMotorSelector.Text = '2' then
        begin
          vsCommand := '>' + vsHi + '.' + vsLo + '.' + UID_DN + '.0.0.2';
        end
        else if CmbMotorSelector.Text = '3' then
        begin
          vsCommand := '>' + vsHi + '.' + vsLo + '.' + UID_DN + '.0.0.4';
        end
        else if CmbMotorSelector.Text = '4' then
        begin
          vsCommand := '>' + vsHi + '.' + vsLo + '.' + UID_DN + '.0.0.8';
        end;
      end
      else
      begin
        vsCommand := '>' + vsHi + '.' + vsLo + '.' + UID_DN + '.0.0.0';
      end;
      ComFm.SendCommand(vsCommand, nil);
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.UIDMoveDwn: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.UIDMoveUp;
var vsHi, vsLo, vsCommand: String;
begin
  try
    if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
    begin
      vsHi := IntToStr(Hi(StrToInt(GetUID(cmbUID.text))));
      vsLo := IntToStr(Lo(StrToInt(GetUID(cmbUID.text))));
      if (isMNI)or(isIQMLC2) then
      begin
        if CmbMotorSelector.Text = '1' then
        begin
          vsCommand := '>' + vsHi + '.' + vsLo + '.' + UID_TOP + '.0.0.1';
        end
        else if CmbMotorSelector.Text = '2' then
        begin
          vsCommand := '>' + vsHi + '.' + vsLo + '.' + UID_TOP + '.0.0.2';
        end
        else if CmbMotorSelector.Text = '3' then
        begin
          vsCommand := '>' + vsHi + '.' + vsLo + '.' + UID_TOP + '.0.0.4';
        end
        else if CmbMotorSelector.Text = '4' then
        begin
          vsCommand := '>' + vsHi + '.' + vsLo + '.' + UID_TOP + '.0.0.8';
        end;
      end
      else
      begin
        vsCommand := '>' + vsHi + '.' + vsLo + '.' + UID_TOP + '.0.0.0';
      end;
      ComFm.SendCommand(vsCommand, nil);
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.UIDMoveUp: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.pnlMenuBarMouseEnter(Sender: TObject);
begin
  try
    pnlMenuBar.Visible := True;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.pnlMenuBarMouseEnter: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.pnlMenuBarMouseLeave(Sender: TObject);
begin
  try
    pnlMenuBar.Visible := False;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.pnlMenuBarMouseLeave: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.rhPortClosed(Sender: TObject);
begin
  try
    giComPortOn := False;
    ButtonsDisable;
    Shape14.pen.Color := clRed;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.rhPortClosed: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.rhPortOpend(Sender: TObject);
begin
  try
    giComPortOn := True;
    Shape14.pen.Color := clLime;
    if VariablesUn.IsNumber(GetUID(cmbUID.text)) and gbUIDfound then
      ButtonsEnable;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.rhPortOpend: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.rhShowConfig(Sender: TObject);
begin
  try
    Show;
    btnGetStatusClick(nil)
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.rhShowConfig: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.rhUIDRecieved(sender: TObject);
var I: Integer;
begin
  try
    if (giSentCom = VariablesUn.giCommand) and (giUID = VariablesUn.giUID_rx) then
    begin
      cmbUID.OnChange := nil;
      if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
      begin
        if (isMNI)or(isIQMLC2) then
        begin
          for I := 1 to 4 do
          begin
            DataUn.dmDataModule.dsMotors.Filtered := False;
            DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + VariablesUn.gsUIDCrnt + ' and MotorNumber = '+IntToStr(I);
            DataUn.dmDataModule.dsMotors.Filtered := True;

            if Not DataUn.dmDataModule.dsMotors.Eof then
            begin
              DataUn.dmDataModule.dsMotors.Edit;
              DataUn.dmDataModule.dsMotors.FieldByName('UID').AsInteger := giUID;
              DataUn.dmDataModule.dsMotors.Post;
            end;
            DataUn.dmDataModule.dsMotors.MergeChangeLog;
            DataUn.dmDataModule.dsMotors.Filtered := False;
          end;

          if(isIQMLC2)then
          begin
            DataUn.dmDataModule.dsIQMLC2.Filtered := False;
            DataUn.dmDataModule.dsIQMLC2.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + VariablesUn.gsUIDCrnt;
            DataUn.dmDataModule.dsIQMLC2.Filtered := True;
            if Not DataUn.dmDataModule.dsIQMLC2.Eof then
            begin
              DataUn.dmDataModule.dsIQMLC2.Edit;
              DataUn.dmDataModule.dsIQMLC2.FieldByName('UID').AsInteger := giUID;
              DataUn.dmDataModule.dsIQMLC2.Post;
              DataUn.dmDataModule.dsIQMLC2.MergeChangeLog;
            end;
            DataUn.dmDataModule.dsIQMLC2.Filtered := False;
          end;
          if (isMNI)then
          begin
            DataUn.dmDataModule.dsMNI.Filtered := False;
            DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + VariablesUn.gsUIDCrnt;
            DataUn.dmDataModule.dsMNI.Filtered := True;

            if Not DataUn.dmDataModule.dsMNI.Eof then
            begin
              DataUn.dmDataModule.dsMNI.Edit;
              DataUn.dmDataModule.dsMNI.FieldByName('UID').AsInteger := giUID;
              DataUn.dmDataModule.dsMNI.Post;
            end;
            dmDataModule.dsMNI.MergeChangeLog;
            DataUn.dmDataModule.dsMNI.Filtered := False;
          end;
        end
        else
        begin
          DataUn.dmDataModule.dsMotors.Filtered := False;
          DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + VariablesUn.gsUIDCrnt;
          DataUn.dmDataModule.dsMotors.Filtered := True;
          if Not DataUn.dmDataModule.dsMotors.Eof then
          begin

            if DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='70' then
            begin
              DataUn.dmDataModule.dsIQ3s.Filtered := False;
              DataUn.dmDataModule.dsIQ3s.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + VariablesUn.gsUIDCrnt;
              DataUn.dmDataModule.dsIQ3s.Filtered := True;
              DataUn.dmDataModule.dsIQ3s.Edit;
              DataUn.dmDataModule.dsIQ3s.FieldByName('UID').AsInteger := giUID;
              DataUn.dmDataModule.dsIQ3s.Post;
              DataUn.dmDataModule.dsIQ3s.MergeChangeLog;
              for i := 1 to 8 do
              begin
                DataUn.dmDataModule.dsIQ3Channels.Filtered := False;
                DataUn.dmDataModule.dsIQ3Channels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt + ' and ChannelID = ' + IntToStr(i);
                DataUn.dmDataModule.dsIQ3Channels.Filtered := True;
                DataUn.dmDataModule.dsIQ3Channels.Edit;
                DataUn.dmDataModule.dsIQ3Channels.FieldByName('UID').AsInteger := giUID;
                DataUn.dmDataModule.dsIQ3Channels.Post;
                DataUn.dmDataModule.dsIQ3Channels.MergeChangeLog;
              end;
            end;
            DataUn.dmDataModule.dsMotors.Edit;
            DataUn.dmDataModule.dsMotors.FieldByName('UID').AsInteger := giUID;


          end;
          DataUn.dmDataModule.dsMotors.MergeChangeLog;
          DataUn.dmDataModule.dsMotors.Filtered := False;
        end;

        VariablesUn.gsUIDCrnt := IntToStr(giUID);
        ShowUID;
        DataUn.dmDataModule.dsIQ3Channels.SaveToFile(VariablesUn.gsDataPath + 'dsIQ3Channels.txt', dfXML);
        DataUn.dmDataModule.dsIQ3s.SaveToFile(VariablesUn.gsDataPath + 'dsIQ3s.txt', dfXML);
        DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
        DataUn.dmDataModule.dsMNI.SaveToFile(VariablesUn.gsDataPath + 'dsMNI.txt', dfXML);
        DataUn.dmDataModule.dsIQMLC2.SaveToFile(VariablesUn.gsDataPath + 'dsIQMLC2.txt', dfXML);
      end;

      //cmbUID.Text := VariablesUn.gsUIDCrnt;
      //ChangeStyleList;

//      if cmbUID.Items.Count > 0 then
//      begin
//        viIndex := cmbUID.Items.IndexOf(VariablesUn.gsUIDCrnt);
//        if viIndex >= 0 then
//          cmbUID.ItemIndex := viIndex
//        else
//          cmbUID.ItemIndex := -1;
//      end;

      gbSaveUID := False;
    end;
    cmbUID.OnChange := cmbUIDChange;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.rhUIDRecieved: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.SaveDsbl;
begin
  try
    btnMotorPersonS.Visible := False;
    btnMotorPersonC.Visible := False;

    btnEnabledS.Visible:=false;
    btnEnabledC.Visible:=false;


    btnDirectionS.Visible := False;
    btnDirectionC.Visible := False;

    btnInhibitS.Visible := False;
    btnInhibitC.Visible := False;

    btnUniformS.Visible := False;
    btnUniformC.Visible := False;

    btnLedS.Visible := False;
    btnLedC.Visible := False;

    btnMaintS.Visible := False;
    btnMaintC.Visible := False;

    btnMTAs.Visible := False;
    btnMTAc.Visible := False;

    btnEnabledS.Visible := False;
    btnEnabledC.Visible := False;

    btnPortS.Visible := False;
    btnPortC.Visible := False;

    btnTypeS.Visible := False;
    btnTypeC.Visible := False;

    btnFwdS.Visible := False;
    btnFwdC.Visible := False;

    btnBOs.Visible := False;
    btnBOc.Visible := False;

    btnBOModeS.Visible := False;
    btnBOModeC.Visible := False;

    btnAutoReturnS.Visible := False;
    btnAutoReturnC.Visible := False;

    btnUpRollerSpeedS.Visible := False;
    btnUpRollerSpeedC.Visible := False;

    btnDNRollerSpeedS.Visible := False;
    btnDNRollerSpeedC.Visible := False;

    btnSlowSpeedS.Visible := False;
    btnSlowSpeedC.Visible := False;

    btnRampPeriodUpS.Visible := False;
    btnRampPeriodUpC.Visible := False;

    btnRampPeriodDnS.Visible := False;
    btnRampPeriodDnC.Visible := False;

    btnInternalThresholdS.Visible := False;
    btnInternalThresholdC.Visible := False;

    btnExternalThresholdS.Visible := False;
    btnExternalThresholdC.Visible := False;

    btnMotionThresholdS.Visible := False;
    btnMotionThresholdC.Visible := False;
    //

    btnIQ2DCUpRollerSpeedS.Visible := False;
    btnIQ2DCUpRollerSpeedC.Visible := False;

    btnIQ2DCDNRollerSpeedS.Visible := False;
    btnIQ2DCDNRollerSpeedC.Visible := False;

    btnIQ2DCSlowUpSpeedS.Visible := False;
    btnIQ2DCSlowUpSpeedC.Visible := False;

    btnIQ2DCSlowDnSpeedS.Visible := False;
    btnIQ2DCSlowDnSpeedC.Visible := False;

    btnIQ2DCRampPeriodUpS.Visible := False;
    btnIQ2DCRampPeriodUpC.Visible := False;

    btnIQ2DCRampPeriodDnS.Visible := False;
    btnIQ2DCRampPeriodDnC.Visible := False;

  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.SaveDsbl: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.SaveLV;
var
  lslZGNparts:TStringList;
begin
  lslZGNparts:=TStringList.Create;
  try

    if VariablesUn.IsZGN(txtBOzgn.Text) then
    begin
      if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
      begin
        giUID :=  StrToInt(GetUID(cmbUID.text));
        if (isMNI)or(isIQMLC2) then
        begin
          if cmbMotorSelector.text='1' then
          begin
            ComFm.SetMotorStatus(giUID, VariablesUn.UID_BLCKOUT_ADR, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            //WaitforSetResponse(giUID, VariablesUn.UID_BLCKOUT_ADR, rhMotorStatusRecieved,200);
          end
          else if cmbMotorSelector.text='2' then
          begin
            ComFm.SetMotorStatus(giUID, VariablesUn.UID_BLCKOUT2_ADR, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
          end
          else if cmbMotorSelector.text='3' then
          begin
            ComFm.SetMotorStatus(giUID, VariablesUn.UID_BLCKOUT3_ADR, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
          end
          else if cmbMotorSelector.text='4' then
          begin
            ComFm.SetMotorStatus(giUID, VariablesUn.UID_BLCKOUT4_ADR, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
          end;
        end
        else
        begin
          ComFm.SetMotorStatus(giUID, VariablesUn.UID_BLCKOUT_ADR, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
        end;
        Delay(300);
      end;
    end
    else if VariablesUn.IsG('.', Trim(txtBOzgn.Text), lslZGNparts) then
    begin
      if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
      begin
        giUID := StrToInt(GetUID(cmbUID.text));
        if (isMNI)or(isIQMLC2) then
        begin
          if cmbMotorSelector.text='1' then
          begin
            ComFm.SetMotorStatus(giUID, VariablesUn.UID_BLCKOUT_ADR, 255, StrToInt(trim(txtBOzgn.Text)), 255, nil);
          end
          else if cmbMotorSelector.text='2' then
          begin
            ComFm.SetMotorStatus(giUID, VariablesUn.UID_BLCKOUT2_ADR, 255, StrToInt(trim(txtBOzgn.Text)), 255, nil);
          end
          else if cmbMotorSelector.text='3' then
          begin
            ComFm.SetMotorStatus(giUID, VariablesUn.UID_BLCKOUT3_ADR, 255, StrToInt(trim(txtBOzgn.Text)), 255, nil);
          end
          else if cmbMotorSelector.text='4' then
          begin
            ComFm.SetMotorStatus(giUID, VariablesUn.UID_BLCKOUT4_ADR, 255, StrToInt(trim(txtBOzgn.Text)), 255, nil);
          end;
        end
        else
        begin
          ComFm.SetMotorStatus(giUID, VariablesUn.UID_BLCKOUT_ADR, 255, StrToInt(trim(txtBOzgn.Text)), 255, nil);
        end;
        Delay(300);
      end;

    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.SaveLV: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.SaveParameters;
begin
  try
    if not (DataUn.dmDataModule.dsMotors.State = dsInactive) and (VariablesUn.IsNumber(GetUID(cmbUID.text))) then
    begin
      DataUn.dmDataModule.dsMotors.Filtered := False;
      DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text);
      DataUn.dmDataModule.dsMotors.Filtered := True;

      If not DataUn.dmDataModule.dsMotors.Eof then
      begin
        DataUn.dmDataModule.dsMotors.Edit;
        if not (cmbDirection.Text = '') then
          DataUn.dmDataModule.dsMotors.FieldByName('Direction').AsString := cmbDirection.Text;

        if not (cmbInhibit.Text = '') then
          DataUn.dmDataModule.dsMotors.FieldByName('Inhibit').AsString := cmbInhibit.Text;

        if not (cmbUniform.Text = '') then
          DataUn.dmDataModule.dsMotors.FieldByName('Start').AsString := cmbUniform.Text;

        if not (cmbLed.Text = '') then
          DataUn.dmDataModule.dsMotors.FieldByName('Led').AsString := cmbLed.Text;

        if not (cmbMaint.Text = '') then
          DataUn.dmDataModule.dsMotors.FieldByName('Maint').AsString := cmbMaint.Text;

        if not (cmbMTAInb.Text = '') then
          DataUn.dmDataModule.dsMotors.FieldByName('MtaInhibit').AsString := cmbMTAInb.Text;

        if not (txtRevision.Text = '') then
          DataUn.dmDataModule.dsMotors.FieldByName('Revision').AsString := txtRevision.Text;

        if not (cmbPortMode.Text = '') then
          DataUn.dmDataModule.dsMotors.FieldByName('Port').AsString := cmbPortMode.Text;

        if not (cmbLVtype.Text = '') then
          DataUn.dmDataModule.dsMotors.FieldByName('LvType').AsString := cmbLVtype.Text;

        if not (cmbFwd.Text = '') then
          DataUn.dmDataModule.dsMotors.FieldByName('Forward').AsString := cmbFwd.Text;

        if not (cmbBOtype.Text = '') then
          DataUn.dmDataModule.dsMotors.FieldByName('BoType').AsString := cmbBOtype.Text;

        DataUn.dmDataModule.dsMotors.Post;
        dmDataModule.dsMotors.MergeChangeLog;
        DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
      end;

      DataUn.dmDataModule.dsMotors.Filtered := False;
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.SaveParameters: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;


procedure TConfigScreenFm.SaveZGN;
var
  lslZGNparts:TStringList;
begin
  lslZGNparts:=TStringList.Create;
  try
    if txtA1.Color = $00A4FFFF then
    begin
      if VariablesUn.IsZGN(txtA1.Text) then
      begin
        if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
        begin
          giUID := StrToInt(GetUID(cmbUID.text));
          if (isMNI)or(isIQMLC2) then
          begin
            if cmbMotorSelector.Text='1' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_BUS5, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end
            else if cmbMotorSelector.Text='2' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS2_BUS5, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end
            else if cmbMotorSelector.Text='3' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS3_BUS5, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end
            else if cmbMotorSelector.Text='4' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS4_BUS5, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end;
            Delay(300);
          end
          else
          begin
            ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_BUS5, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
          end;
          Delay(300);
        end;
      end
      else if VariablesUn.IsG('.', Trim(txtA1.Text), lslZGNparts) then
      begin
        if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
        begin
          giUID := StrToInt(GetUID(cmbUID.text));
          if (isMNI)or(isIQMLC2) then
          begin
            if cmbMotorSelector.Text='1' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_BUS5, 255, StrToInt(trim(txtA1.Text)), 255, nil);
            end
            else if cmbMotorSelector.Text='2' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS2_BUS5, 255, StrToInt(trim(txtA1.Text)), 255, nil);
            end
            else if cmbMotorSelector.Text='3' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS3_BUS5, 255, StrToInt(trim(txtA1.Text)), 255, nil);
            end
            else if cmbMotorSelector.Text='4' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS4_BUS5, 255, StrToInt(trim(txtA1.Text)), 255, nil);
            end;
            Delay(300);
          end
          else
          begin
            ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_BUS5, 255, StrToInt(trim(txtA1.Text)), 255, nil);
          end;
          Delay(300);
        end;
      end;

    end;
    if txtA2.Color = $00A4FFFF then
    begin
      if VariablesUn.IsZGN(txtA2.Text) then
      begin
        if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
        begin
          giUID := StrToInt(GetUID(cmbUID.text));
          if (isMNI)or(isIQMLC2) then
          begin
            if cmbMotorSelector.Text='1' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_BUS6, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end
            else if cmbMotorSelector.Text='2' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS2_BUS6, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end
            else if cmbMotorSelector.Text='3' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS3_BUS6, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end
            else if cmbMotorSelector.Text='4' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS4_BUS6, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end;
            Delay(300);
          end
          else
          begin

            ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_BUS6, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
          end;
          Delay(300);
        end;
      end
      else if VariablesUn.IsG('.', Trim(txtA2.Text), lslZGNparts) then
      begin
        if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
        begin
          giUID := StrToInt(GetUID(cmbUID.text));
          if (isMNI)or(isIQMLC2) then
          begin
            if cmbMotorSelector.Text='1' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_BUS6, 255, StrToInt(trim(txtA2.Text)), 255, nil);
            end
            else if cmbMotorSelector.Text='2' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS2_BUS6, 255, StrToInt(trim(txtA2.Text)), 255, nil);
            end
            else if cmbMotorSelector.Text='3' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS3_BUS6, 255, StrToInt(trim(txtA2.Text)), 255, nil);
            end
            else if cmbMotorSelector.Text='4' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS4_BUS6, 255, StrToInt(trim(txtA2.Text)), 255, nil);
            end;
            Delay(300);
          end
          else
          begin
            ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_BUS6, 255, StrToInt(trim(txtA2.Text)), 255, nil);
          end;
          Delay(300);
        end;
      end;
    end;

    if txtA3.Color = $00A4FFFF then
    begin
      if VariablesUn.IsZGN(txtA3.Text) then
      begin
        if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
        begin
          giUID := StrToInt(GetUID(cmbUID.text));
          if (isMNI)or(isIQMLC2) then
          begin
            if cmbMotorSelector.Text='1' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_BUS7, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end
            else if cmbMotorSelector.Text='2' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS2_BUS7, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end
            else if cmbMotorSelector.Text='3' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS3_BUS7, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end
            else if cmbMotorSelector.Text='4' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS4_BUS7, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end;
            Delay(300);
          end
          else
          begin
            ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_BUS7, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
          end;
          Delay(300);
        end;
      end
      else if VariablesUn.IsG('.', Trim(txtA3.Text), lslZGNparts) then
      begin
        if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
        begin
          giUID := StrToInt(GetUID(cmbUID.text));
          if (isMNI)or(isIQMLC2) then
          begin
            if cmbMotorSelector.Text='1' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_BUS7, 255, StrToInt(trim(txtA3.Text)), 255, nil);
            end
            else if cmbMotorSelector.Text='2' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS2_BUS7, 255, StrToInt(trim(txtA3.Text)), 255, nil);
            end
            else if cmbMotorSelector.Text='3' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS3_BUS7, 255, StrToInt(trim(txtA3.Text)), 255, nil);
            end
            else if cmbMotorSelector.Text='4' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS4_BUS7, 255, StrToInt(trim(txtA3.Text)), 255, nil);
            end;
            Delay(300);
          end
          else
          begin
            ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_BUS7, 255, StrToInt(trim(txtA3.Text)), 255, nil);
          end;
          Delay(300);
        end;
      end;
    end;

    if txtA4.Color = $00A4FFFF then
    begin
      if VariablesUn.IsZGN(txtA4.Text) then
      begin
        if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
        begin
          giUID := StrToInt(GetUID(cmbUID.text));
          if (isMNI)or(isIQMLC2) then
          begin
            if cmbMotorSelector.Text='1' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_BUS8, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end
            else if cmbMotorSelector.Text='2' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS2_BUS8, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end
            else if cmbMotorSelector.Text='3' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS3_BUS8, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end
            else if cmbMotorSelector.Text='4' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS4_BUS8, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end;
            Delay(300);
          end
          else
          begin
            ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_BUS8, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
          end;
          Delay(300);
        end;
      end
      else if VariablesUn.IsG('.', Trim(txtA4.Text), lslZGNparts) then
      begin
        if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
        begin
          giUID := StrToInt(GetUID(cmbUID.text));
          if (isMNI)or(isIQMLC2) then
          begin
            if cmbMotorSelector.Text='1' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_BUS8, 255, StrToInt(trim(txtA4.Text)), 255, nil);
            end
            else if cmbMotorSelector.Text='2' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS2_BUS8, 255, StrToInt(trim(txtA4.Text)), 255, nil);
            end
            else if cmbMotorSelector.Text='3' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS3_BUS8, 255, StrToInt(trim(txtA4.Text)), 255, nil);
            end
            else if cmbMotorSelector.Text='4' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS4_BUS8, 255, StrToInt(trim(txtA4.Text)), 255, nil);
            end;
            Delay(300);
          end
          else
          begin
            ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_BUS8, 255, StrToInt(trim(txtA4.Text)), 255, nil);
          end;
          Delay(300);
        end;
      end;
    end;

    if txtA5.Color = $00A4FFFF then
    begin
      if VariablesUn.IsZGN(txtA5.Text) then
      begin
        if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
        begin
          giUID := StrToInt(GetUID(cmbUID.text));
          if (isMNI)or(isIQMLC2) then
          begin
            if cmbMotorSelector.Text='1' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_BUS1, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end
            else if cmbMotorSelector.Text='2' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS2_BUS1, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end
            else if cmbMotorSelector.Text='3' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS3_BUS1, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end
            else if cmbMotorSelector.Text='4' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS4_BUS1, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end;
            Delay(300);
          end
          else
          begin
            ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_BUS1, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
          end;
          Delay(300);
        end;
      end
      else if VariablesUn.IsG('.', Trim(txtA5.Text), lslZGNparts) then
      begin
        if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
        begin
          giUID := StrToInt(GetUID(cmbUID.text));
          if (isMNI)or(isIQMLC2) then
          begin
            if cmbMotorSelector.Text='1' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_BUS1, 255, StrToInt(trim(txtA5.Text)), 255, nil);
            end
            else if cmbMotorSelector.Text='2' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS2_BUS1, 255, StrToInt(trim(txtA5.Text)), 255, nil);
            end
            else if cmbMotorSelector.Text='3' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS3_BUS1, 255, StrToInt(trim(txtA5.Text)), 255, nil);
            end
            else if cmbMotorSelector.Text='4' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS4_BUS1, 255, StrToInt(trim(txtA5.Text)), 255, nil);
            end;
            Delay(300);
          end
          else
          begin
            ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_BUS1, 255, StrToInt(trim(txtA5.Text)), 255, nil);
          end;
          Delay(300);
        end;
      end;
    end;

    if txtA6.Color = $00A4FFFF then
    begin
      if VariablesUn.IsZGN(txtA6.Text) then
      begin
        if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
        begin
          giUID := StrToInt(GetUID(cmbUID.text));
          if (isMNI)or(isIQMLC2) then
          begin
            if cmbMotorSelector.Text='1' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_BUS2, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end
            else if cmbMotorSelector.Text='2' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS2_BUS2, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end
            else if cmbMotorSelector.Text='3' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS3_BUS2, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end
            else if cmbMotorSelector.Text='4' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS4_BUS2, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end;
            Delay(300);
          end
          else
          begin
            ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_BUS2, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
          end;
          Delay(300);
        end;
      end
      else if VariablesUn.IsG('.', Trim(txtA6.Text), lslZGNparts) then
      begin
        if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
        begin
          giUID := StrToInt(GetUID(cmbUID.text));
          if (isMNI)or(isIQMLC2) then
          begin
            if cmbMotorSelector.Text='1' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_BUS2, 255, StrToInt(trim(txtA6.Text)), 255, nil);
            end
            else if cmbMotorSelector.Text='2' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS2_BUS2, 255, StrToInt(trim(txtA6.Text)), 255, nil);
            end
            else if cmbMotorSelector.Text='3' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS3_BUS2, 255, StrToInt(trim(txtA6.Text)), 255, nil);
            end
            else if cmbMotorSelector.Text='4' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS4_BUS2, 255, StrToInt(trim(txtA6.Text)), 255, nil);
            end;
            Delay(300);
          end
          else
          begin
            ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_BUS2, 255, StrToInt(trim(txtA6.Text)), 255, nil);
          end;
          Delay(300);
        end;
      end;
    end;

    if txtA7.Color = $00A4FFFF then
    begin
      if VariablesUn.IsZGN(txtA7.Text) then
      begin
        if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
        begin
          giUID := StrToInt(GetUID(cmbUID.text));
          if (isMNI)or(isIQMLC2) then
          begin
            if cmbMotorSelector.Text='1' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_BUS3, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end
            else if cmbMotorSelector.Text='2' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS2_BUS3, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end
            else if cmbMotorSelector.Text='3' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS3_BUS3, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end
            else if cmbMotorSelector.Text='4' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS4_BUS3, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end;
            Delay(300);
          end
          else
          begin
            ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_BUS3, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
          end;
          Delay(300);
        end;
      end
      else if VariablesUn.IsG('.', Trim(txtA7.Text), lslZGNparts) then
      begin
        if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
        begin
          giUID := StrToInt(GetUID(cmbUID.text));
          if (isMNI)or(isIQMLC2) then
          begin
            if cmbMotorSelector.Text='1' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_BUS3, 255, StrToInt(trim(txtA7.Text)), 255, nil);
            end
            else if cmbMotorSelector.Text='2' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS2_BUS3, 255, StrToInt(trim(txtA7.Text)), 255, nil);
            end
            else if cmbMotorSelector.Text='3' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS3_BUS3, 255, StrToInt(trim(txtA7.Text)), 255, nil);
            end
            else if cmbMotorSelector.Text='4' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS4_BUS3, 255, StrToInt(trim(txtA7.Text)), 255, nil);
            end;
            Delay(300);
          end
          else
          begin
            ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_BUS3, 255, StrToInt(trim(txtA7.Text)), 255, nil);
          end;
          Delay(300);
        end;
      end;
    end;

    if txtA8.Color = $00A4FFFF then
    begin
      if VariablesUn.IsZGN(txtA8.Text) then
      begin
        if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
        begin
          giUID := StrToInt(GetUID(cmbUID.text));
          if (isMNI)or(isIQMLC2) then
          begin
            if cmbMotorSelector.Text='1' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_BUS4, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end
            else if cmbMotorSelector.Text='2' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS2_BUS4, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end
            else if cmbMotorSelector.Text='3' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS3_BUS4, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end
            else if cmbMotorSelector.Text='4' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS4_BUS4, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end;
          end
          else
          begin
            ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_BUS4, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
          end;
          Delay(300);
        end;
      end
      else if VariablesUn.IsG('.', Trim(txtA8.Text), lslZGNparts) then
      begin
        if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
        begin
          giUID := StrToInt(GetUID(cmbUID.text));
          if (isMNI)or(isIQMLC2) then
          begin
            if cmbMotorSelector.Text='1' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_BUS4, 255, StrToInt(trim(txtA8.Text)), 255, nil);
            end
            else if cmbMotorSelector.Text='2' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS2_BUS4, 255, StrToInt(trim(txtA8.Text)), 255, nil);
            end
            else if cmbMotorSelector.Text='3' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS3_BUS4, 255, StrToInt(trim(txtA8.Text)), 255, nil);
            end
            else if cmbMotorSelector.Text='4' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS4_BUS4, 255, StrToInt(trim(txtA8.Text)), 255, nil);
            end;
          end
          else
          begin
            ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_BUS4, 255, StrToInt(trim(txtA8.Text)), 255, nil);
          end;
          Delay(300);
        end;
      end;
    end;

    if txtLVzgn.Color = $00A4FFFF then
    begin
      if VariablesUn.IsZGN(txtLVzgn.Text) then
      begin
        if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
        begin
          giUID :=  StrToInt(GetUID(cmbUID.text));
          if (isMNI)or(isIQMLC2) then
          begin
            if cmbMotorSelector.Text='1' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_ADDR, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end
            else if cmbMotorSelector.Text='2' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS2_ADDR, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end
            else if cmbMotorSelector.Text='3' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS3_ADDR, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end
            else if cmbMotorSelector.Text='4' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS4_ADDR, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
            end;
          end
          else
          begin
            ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_ADDR, VariablesUn.giZ, VariablesUn.giG, VariablesUn.giN, nil);
          end;
          Delay(300);
        end;
      end
      else if VariablesUn.IsG('.', Trim(txtLVzgn.Text), lslZGNparts) then
      begin
        if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
        begin
          giUID := StrToInt(GetUID(cmbUID.text));
          if (isMNI)or(isIQMLC2) then
          begin
            if cmbMotorSelector.Text='1' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_ADDR, 255, StrToInt(trim(txtLVzgn.Text)), 255, nil);
            end
            else if cmbMotorSelector.Text='2' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS2_ADDR, 255, StrToInt(trim(txtLVzgn.Text)), 255, nil);
            end
            else if cmbMotorSelector.Text='3' then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS3_ADDR, 255, StrToInt(trim(txtLVzgn.Text)), 255, nil);
            end
            else if cmbMotorSelector.Text='4'   then
            begin
              ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS4_ADDR, 255, StrToInt(trim(txtLVzgn.Text)), 255, nil);
            end;
          end
          else
          begin
            ComFm.SetMotorStatus(giUID, VariablesUn.UID_LS1_ADDR, 255, StrToInt(trim(txtLVzgn.Text)), 255, nil);
          end;
          Delay(300);
        end;
      end;
      if ismni or isIqmlc2 then
      begin
        DataUn.dmDataModule.dsMotors.Filtered := False;
        DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt;
        DataUn.dmDataModule.dsMotors.Filtered := True;
      end
      else
      begin

      end;
    end;
  except
  on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.SaveZGN: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.rhDisable;
begin
  try
    txtA1.OnChange := nil;
    txtA2.OnChange := nil;
    txtA3.OnChange := nil;
    txtA4.OnChange := nil;
    txtA5.OnChange := nil;
    txtA6.OnChange := nil;
    txtA7.OnChange := nil;
    txtA8.OnChange := nil;

    txtLVzgn.OnChange := nil;
    txtBOzgn.OnChange := nil;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.rhDisable: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.rhEnable;
begin
  try
    txtA1.OnChange := txtA1Change;
    txtA2.OnChange := txtA2Change;
    txtA3.OnChange := txtA3Change;
    txtA4.OnChange := txtA4Change;
    txtA5.OnChange := txtA5Change;
    txtA6.OnChange := txtA6Change;
    txtA7.OnChange := txtA7Change;
    txtA8.OnChange := txtA8Change;


    txtLVzgn.OnChange := txtLVzgnChange;
    txtBOzgn.OnChange := txtBOzgnChange;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.rhEnable: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.rhMotorConnectedRcvd;
begin
  try
    if (giSentCom = VariablesUn.giCommand) and (giUID = VariablesUn.giUID_rx) then
    begin
       if VariablesUn.giCommand = StrToInt(UID_FR) then
       begin
        passFlag:=true;
        gbUIDfound := True;
       end;
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.rhMotorConnectedRcvd: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.rhMotorStatusRecieved(sender: TObject);
var
  BinHolder:string;
  I:Integer;
begin
  try
    if (giSentCom = VariablesUn.giCommand) and (giUID = VariablesUn.giUID_rx) then
    begin
      rhDisable;
      case VariablesUn.giCommand of
        22:
        begin
          PassFlag:=True;

          dmDataModule.dsIQMLC2.Filtered := False;
          dmDataModule.dsIQMLC2.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + IntToStr(giUID);
          dmDataModule.dsIQMLC2.Filtered := True;
          dmDataModule.dsIQMLC2.Edit;
          if(VariablesUn.giDataL=1)then
          begin
            DataUn.dmDataModule.dsIQMLC2.FieldByName('StopMode').AsString := 'Normal';
            cmbUniform.ItemIndex:=1;
          end
          else
          begin
            DataUn.dmDataModule.dsIQMLC2.FieldByName('StopMode').AsString := 'Reverse';
            cmbUniform.ItemIndex:=0;
          end;
          DataUn.dmDataModule.dsIQMLC2.MergeChangeLog;
        end;
        23:
        begin
          MotorLEDBits:=VariablesUn.giDataL;
          if VariablesUn.GetBit(VariablesUn.giDataL, StrToInt(cmbMotorSelector.Text)-1) then
          begin
              cmbLed.ItemIndex := 1;
          end
          else
          begin
              cmbLed.ItemIndex := 0;
          end;
          PassFlag:=True;
        end;
        24:
        begin

          dmDataModule.dsMotors.Edit;

          MotorInhibitModeBits:=VariablesUn.giDataL;
          if(isIQMLC2)then
          begin

            if(VariablesUn.giDataL=1)then
            begin

              cmbMTAInb.ItemIndex:=1;
              for I := 1 to 4 do
              begin
                dmDataModule.dsMotors.Filtered := False;
                dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + IntToStr(giUID) +  ' and MotorNumber = '+IntToStr(I);
                dmDataModule.dsMotors.Filtered := True;
                dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('MtaInhibit').AsString:='ENA';
                dmDataModule.dsMotors.MergeChangeLog;
              end;
            end
            else
            begin
              cmbMTAInb.ItemIndex:=0;
              for I := 1 to 4 do
              begin
                dmDataModule.dsMotors.Filtered := False;
                dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + IntToStr(giUID) +  ' and MotorNumber = '+IntToStr(I);
                dmDataModule.dsMotors.Filtered := True;
                dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('MtaInhibit').AsString:='DIS';
                dmDataModule.dsMotors.MergeChangeLog;
              end;
            end;
          end;
          PassFlag:=True;
        end;
        25:
        begin
          MotorInhibFlagBits:=VariablesUn.giDataL;
          if VariablesUn.GetBit(VariablesUn.giDataL, StrToInt(cmbMotorSelector.Text)-1) then
          begin
            cmbInhibit.ItemIndex := 1;
          end
          else
          begin
            cmbInhibit.ItemIndex := 0;
          end;
          PassFlag:=True;
        end;
        26:
        begin
          MotorMaintBits:=VariablesUn.giDataL;
          if VariablesUn.GetBit(VariablesUn.giDataL, 5) then
          begin
            cmbMaint.ItemIndex := 1
          end
          else
          begin
            cmbMaint.ItemIndex := 0;
          end;
          PassFlag:=True;
        end;
        27:       //Switch Enabled
        begin
          SwitchEnabledBits:=VariablesUn.giDataL;
          if (isMNI)or(isIQMLC2) then
          begin
            DataUn.dmDataModule.dsMNI.Filtered := False;
            DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text);
            DataUn.dmDataModule.dsMNI.Filtered := True;
            DataUn.dmDataModule.dsMNI.Edit;

            if VariablesUn.GetBit(VariablesUn.giDataL,0)then
            begin
              DataUn.dmDataModule.dsMNI.FieldByName('SwPort1Enabled').AsBoolean := False;
            end
            else
            begin
              DataUn.dmDataModule.dsMNI.FieldByName('SwPort1Enabled').AsBoolean := true;
            end;
            if VariablesUn.GetBit(VariablesUn.giDataL,1)then
            begin
              DataUn.dmDataModule.dsMNI.FieldByName('SwPort2Enabled').AsBoolean := False;
            end
            else
            begin
              DataUn.dmDataModule.dsMNI.FieldByName('SwPort2Enabled').AsBoolean := true;
            end;
            if VariablesUn.GetBit(VariablesUn.giDataL,2)then
            begin
              DataUn.dmDataModule.dsMNI.FieldByName('SwPort3Enabled').AsBoolean := False;
            end
            else
            begin
              DataUn.dmDataModule.dsMNI.FieldByName('SwPort3Enabled').AsBoolean := true;
            end;
            if VariablesUn.GetBit(VariablesUn.giDataL,3)then
            begin
              DataUn.dmDataModule.dsMNI.FieldByName('SwPort4Enabled').AsBoolean := False;
            end
            else
            begin
              DataUn.dmDataModule.dsMNI.FieldByName('SwPort4Enabled').AsBoolean := true;
            end;
            if VariablesUn.GetBit(VariablesUn.giDataL,6)then
            begin
              DataUn.dmDataModule.dsMNI.FieldByName('IRPortEnabled').AsBoolean := False;
            end
            else
            begin
              DataUn.dmDataModule.dsMNI.FieldByName('IRPortEnabled').AsBoolean := true;
            end;
            if VariablesUn.GetBit(VariablesUn.giDataL,7)then
            begin
              DataUn.dmDataModule.dsMNI.FieldByName('AuxPortEnabled').AsBoolean := False;
            end
            else
            begin
              DataUn.dmDataModule.dsMNI.FieldByName('AuxPortEnabled').AsBoolean := true;
            end;
            DataUn.dmDataModule.dsMNI.MergeChangeLog;
            cbSwEnabled.OnClick:=nil;
            if VariablesUn.GetBit(SwitchEnabledBits,cmbMotorSelector.ItemIndex) then
            begin
              cbSwEnabled.Checked:=False;
            end
            else
            begin
              cbSwEnabled.Checked:=True;
            end;
            cbSwEnabled.OnClick:=cbSwEnabledClick;
          end
          else
          begin
            cbSwEnabled.OnClick:=nil;
            if VariablesUn.giDataL=0 then
            begin
              cbSwEnabled.Checked:=true;
            end
            else
            begin
              cbSwEnabled.Checked:=False;
            end;
            cbSwEnabled.OnClick:=cbSwEnabledClick;
          end;
          PassFlag:=True;
        end;
        28:                   // Network Switch Master     FWDing Enabled
        begin
          if (isMNI)or(isIQMLC2) then
          begin
            DataUn.dmDataModule.dsMNI.Filtered := False;
            DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text);
            DataUn.dmDataModule.dsMNI.Filtered := True;
            if not DataUn.dmDataModule.dsMNI.Eof then
            begin
              DataUn.dmDataModule.dsMNI.Edit;
              if VariablesUn.GetBit(VariablesUn.giDataL,0)then
              begin
                DataUn.dmDataModule.dsMNI.FieldByName('SwPort1FWDEnabled').AsBoolean := True;
              end
              else
              begin
                DataUn.dmDataModule.dsMNI.FieldByName('SwPort1FWDEnabled').AsBoolean := False;
              end;
              if VariablesUn.GetBit(VariablesUn.giDataL,1)then
              begin
                DataUn.dmDataModule.dsMNI.FieldByName('SwPort2FWDEnabled').AsBoolean := True;
              end
              else
              begin
                DataUn.dmDataModule.dsMNI.FieldByName('SwPort2FWDEnabled').AsBoolean := False;
              end;
              if VariablesUn.GetBit(VariablesUn.giDataL,2)then
              begin
                DataUn.dmDataModule.dsMNI.FieldByName('SwPort3FWDEnabled').AsBoolean := True;
              end
              else
              begin
                DataUn.dmDataModule.dsMNI.FieldByName('SwPort3FWDEnabled').AsBoolean := False;
              end;
              if VariablesUn.GetBit(VariablesUn.giDataL,3)then
              begin
                DataUn.dmDataModule.dsMNI.FieldByName('SwPort4FWDEnabled').AsBoolean := True;
              end
              else
              begin
                DataUn.dmDataModule.dsMNI.FieldByName('SwPort4FWDEnabled').AsBoolean := False;
              end;
              if VariablesUn.GetBit(VariablesUn.giDataL,6)then
              begin
                DataUn.dmDataModule.dsMNI.FieldByName('IRPortFWDEnabled').AsBoolean := True;
              end
              else
              begin
                DataUn.dmDataModule.dsMNI.FieldByName('IRPortFWDEnabled').AsBoolean := False;
              end;
              if VariablesUn.GetBit(VariablesUn.giDataL,7)then
              begin
                DataUn.dmDataModule.dsMNI.FieldByName('AuxPortFWDEnabled').AsBoolean := True;
              end
              else
              begin
                DataUn.dmDataModule.dsMNI.FieldByName('AuxPortFWDEnabled').AsBoolean := False;
              end;

              DataUn.dmDataModule.dsMNI.MergeChangeLog;
            end;

            SwitchFwdBits:=VariablesUn.giDataL;
            if VariablesUn.GetBit(VariablesUn.giDataL, StrToInt(cmbMotorSelector.text)-1) then
              cmbFwd.ItemIndex := 0
            else
              cmbFwd.ItemIndex := 1;

          end
          else
          begin
            DataUn.dmDataModule.dsMotors.Filtered := False;
            DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text);
            DataUn.dmDataModule.dsMotors.Filtered := True;
            DataUn.dmDataModule.dsMotors.Edit;
            DataUn.dmDataModule.dsMotors.FieldByName('PortAddrFwd').AsInteger:=VariablesUn.giDataL;
            DataUn.dmDataModule.dsMotors.MergeChangeLog;
            BinHolder:=IntToBin(DataUn.dmDataModule.dsMotors.FieldByName('PortAddrFwd').AsInteger);
          end;
          PassFlag:=True;
        end;
        29:            //Switch mode
        begin
          if (isMNI)or(isIQMLC2) then
          begin
            DataUn.dmDataModule.dsMNI.Filtered := False;
            DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text);
            DataUn.dmDataModule.dsMNI.Filtered := True;
            if not DataUn.dmDataModule.dsMNI.Eof then
            begin
              DataUn.dmDataModule.dsMNI.Edit;
              if(variablesUn.giDataH=1) then
              begin
                DataUn.dmDataModule.dsMNI.FieldByName('SW1BtnPerson').AsString := IntToStr(VariablesUn.giDataL);
                DataUn.dmDataModule.dsMNI.MergeChangeLog;
              end
              else if(variablesUn.giDataH=2) then
              begin
                DataUn.dmDataModule.dsMNI.FieldByName('SW2BtnPerson').AsString := IntToStr(VariablesUn.giDataL);
                DataUn.dmDataModule.dsMNI.MergeChangeLog;
              end
              else if(variablesUn.giDataH=3) then
              begin
                DataUn.dmDataModule.dsMNI.FieldByName('SW3BtnPerson').AsString := IntToStr(VariablesUn.giDataL);
                DataUn.dmDataModule.dsMNI.MergeChangeLog;
              end
              else if(variablesUn.giDataH=4) then
              begin
                DataUn.dmDataModule.dsMNI.FieldByName('SW4BtnPerson').AsString := IntToStr(VariablesUn.giDataL);
                DataUn.dmDataModule.dsMNI.MergeChangeLog;
              end;
            end;
          end;
          case VariablesUn.giDataL of
            9:
              cmbLVtype.ItemIndex := 0;
            17:
              cmbLVtype.ItemIndex := 1;
            33:
              cmbLVtype.ItemIndex := 2;
            18:
              cmbLVtype.ItemIndex := 3;
            34:
              cmbLVtype.ItemIndex := 4;
            4:
              cmbLVtype.ItemIndex := 5;
            68:
              cmbLVtype.ItemIndex := 6;
            132:
              cmbLVtype.ItemIndex := 7;
          end;
          PassFlag:=True;
        end;
        36:
        begin
          SwitchPortModeBits:=VariablesUn.giDataL;
          DataUn.dmDataModule.dsMNI.Filtered := False;
          DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + IntToStr(giUID);
          DataUn.dmDataModule.dsMNI.Filtered := True;
          dmDataModule.dsMNI.Edit;
          if VariablesUn.GetBit(VariablesUn.giDataL,0)then
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SwPort1Mode').AsInteger := 1;
          end
          else
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SwPort1Mode').AsInteger := 0;
          end;
          if VariablesUn.GetBit(VariablesUn.giDataL,1)then
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SwPort2Mode').AsInteger := 1;
          end
          else
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SwPort2Mode').AsInteger := 0;
          end;
          if VariablesUn.GetBit(VariablesUn.giDataL,2)then
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SwPort3Mode').AsInteger := 1;
          end
          else
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SwPort3Mode').AsInteger := 0;
          end;
          if VariablesUn.GetBit(VariablesUn.giDataL,3)then
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SwPort4Mode').AsInteger := 1;
          end
          else
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('SwPort4Mode').AsInteger := 0;
          end;
          if VariablesUn.GetBit(VariablesUn.giDataL,6)then
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('IRPortMode').AsInteger := 1;
          end
          else
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('IRPortMode').AsInteger := 0;
          end;
          if VariablesUn.GetBit(VariablesUn.giDataL,7)then
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('AuxPortMode').AsInteger := 1;
          end
          else
          begin
            DataUn.dmDataModule.dsMNI.FieldByName('AuxPortMode').AsInteger := 0;
          end;

          DataUn.dmDataModule.dsMNI.MergeChangeLog;
          PassFlag:=True;
        end;
        40:                   // Parameters 1
        begin
          PassFlag:=True;

          dmDataModule.dsIQMLC2.Filtered := False;
          dmDataModule.dsIQMLC2.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + IntToStr(giUID);
          dmDataModule.dsIQMLC2.Filtered := True;
          dmDataModule.dsIQMLC2.Edit;


          lblMtrType.Caption := 'IQMLC2';
          //Data H
          dmDataModule.dsMotors.Filtered := False;
          dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + IntToStr(giUID) +  ' and MotorNumber = 4';
          dmDataModule.dsMotors.Filtered := True;
          dmDataModule.dsMotors.Edit;
          if GetBit(giDataH,0)=True then
          begin
            DataUn.dmDataModule.dsMotors.FieldByName('Inhibit').AsString := 'ON';
            if cmbMotorSelector.text='4' then
            begin
              cmbInhibit.ItemIndex:=1;
            end;
          end
          else
          begin
            DataUn.dmDataModule.dsMotors.FieldByName('Inhibit').AsString := 'OFF';
            if cmbMotorSelector.text='4' then
            begin
              cmbInhibit.ItemIndex:=0;
            end;
          end;
          dmDataModule.dsMotors.MergeChangeLog;
          dmDataModule.dsMotors.Filtered := False;
          dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + IntToStr(giUID) +  ' and MotorNumber = 3';
          dmDataModule.dsMotors.Filtered := True;
          dmDataModule.dsMotors.Edit;

          if GetBit(giDataH,1)=True then
          begin
            DataUn.dmDataModule.dsMotors.FieldByName('Inhibit').AsString := 'ON';
            if cmbMotorSelector.text='3' then
            begin
              cmbInhibit.ItemIndex:=1;
            end;
          end
          else
          begin
            DataUn.dmDataModule.dsMotors.FieldByName('Inhibit').AsString := 'OFF';
            if cmbMotorSelector.text='3' then
            begin
              cmbInhibit.ItemIndex:=0;
            end;
          end;
          dmDataModule.dsMotors.MergeChangeLog;
          dmDataModule.dsMotors.Filtered := False;
          dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + IntToStr(giUID) +  ' and MotorNumber = 2';
          dmDataModule.dsMotors.Filtered := True;
          dmDataModule.dsMotors.Edit;
          if GetBit(giDataH,2)=True then
          begin
            DataUn.dmDataModule.dsMotors.FieldByName('Inhibit').AsString := 'ON';
            if cmbMotorSelector.text='2' then
            begin
              cmbInhibit.ItemIndex:=1;
            end;
          end
          else
          begin
            DataUn.dmDataModule.dsMotors.FieldByName('Inhibit').AsString := 'OFF';
            if cmbMotorSelector.text='2' then
            begin
              cmbInhibit.ItemIndex:=0;
            end;
          end;
          dmDataModule.dsMotors.MergeChangeLog;
          dmDataModule.dsMotors.Filtered := False;
          dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + IntToStr(giUID) +  ' and MotorNumber = 1';
          dmDataModule.dsMotors.Filtered := True;
          dmDataModule.dsMotors.Edit;
          if GetBit(giDataH,3)=True then
          begin
            DataUn.dmDataModule.dsMotors.FieldByName('Inhibit').AsString := 'ON';
            if cmbMotorSelector.text='1' then
            begin
              cmbInhibit.ItemIndex:=1;
            end;
          end
          else
          begin
            DataUn.dmDataModule.dsMotors.FieldByName('Inhibit').AsString := 'OFF';
            if cmbMotorSelector.text='1' then
            begin
              cmbInhibit.ItemIndex:=0;
            end;
          end;
          dmDataModule.dsMotors.MergeChangeLog;
          //
          if GetBit(giDataH,4)=True then
          begin
            DataUn.dmDataModule.dsIQMLC2.FieldByName('MasterPortEnabled').AsBoolean := False;
          end
          else
          begin
            DataUn.dmDataModule.dsIQMLC2.FieldByName('MasterPortEnabled').AsBoolean := True;
          end;
          if GetBit(giDataH,5)=True then
          begin
            DataUn.dmDataModule.dsIQMLC2.FieldByName('AutoPortEnabled').AsBoolean := False;
          end
          else
          begin
            DataUn.dmDataModule.dsIQMLC2.FieldByName('AutoPortEnabled').AsBoolean := True;
          end;
          if GetBit(giDataH,6)=True then
          begin

            cmbMTAInb.ItemIndex:=1;
            for I := 1 to 4 do
            begin
              dmDataModule.dsMotors.Filtered := False;
              dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + IntToStr(giUID) +  ' and MotorNumber = '+IntToStr(I);
              dmDataModule.dsMotors.Filtered := True;
              dmDataModule.dsMotors.Edit;
              DataUn.dmDataModule.dsMotors.FieldByName('MtaInhibit').AsString:='ENA';
              dmDataModule.dsMotors.MergeChangeLog;
            end;
          end
          else
          begin

            cmbMTAInb.ItemIndex:=0;
            for I := 1 to 4 do
            begin
              dmDataModule.dsMotors.Filtered := False;
              dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + IntToStr(giUID) +  ' and MotorNumber = '+IntToStr(I);
              dmDataModule.dsMotors.Filtered := True;
              dmDataModule.dsMotors.Edit;
              DataUn.dmDataModule.dsMotors.FieldByName('MtaInhibit').AsString:='DIS';
              dmDataModule.dsMotors.MergeChangeLog;
            end;
          end;
          if GetBit(giDataH,7)=True then
          begin
            DataUn.dmDataModule.dsIQMLC2.FieldByName('AutoPortMode').AsString := 'IS';
          end
          else
          begin
            DataUn.dmDataModule.dsIQMLC2.FieldByName('AutoPortMode').AsString := 'LS';
          end;

          //Data M
          //maintenance mode motor 4
          MotorMaintBits:=VariablesUn.giDataM;
          if(cmbMotorSelector.Text='4')then
          begin
            DataUn.dmDataModule.dsMotors.Filtered := False;
            DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + IntToStr(giUID) + ' and MotorNumber = 4';
            DataUn.dmDataModule.dsMotors.Filtered := True;
            dmDataModule.dsMotors.Edit;
            if GetBit(giDataM,0)=True then
            begin
              DataUn.dmDataModule.dsMotors.FieldByName('Maint').AsString := 'ENA';;
              cmbMaint.ItemIndex:= 1;
            end
            else
            begin
              DataUn.dmDataModule.dsMotors.FieldByName('Maint').AsString := 'DIS';;
              cmbMaint.ItemIndex:= 0;
            end;
            DataUn.dmDataModule.dsMotors.MergeChangeLog;

          end;
          //maintenance mode motor 3
          if(cmbMotorSelector.Text='3')then
          begin
            DataUn.dmDataModule.dsMotors.Filtered := False;
            DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + IntToStr(giUID) + ' and MotorNumber = 3';
            DataUn.dmDataModule.dsMotors.Filtered := True;
            dmDataModule.dsMotors.Edit;
            if GetBit(giDataM,1)=True then
            begin
              DataUn.dmDataModule.dsMotors.FieldByName('Maint').AsString := 'ENA';
              cmbMaint.ItemIndex:= 1;
            end
            else
            begin
              DataUn.dmDataModule.dsMotors.FieldByName('Maint').AsString := 'DIS';
              cmbMaint.ItemIndex:= 0;
            end;
            DataUn.dmDataModule.dsMotors.MergeChangeLog;
          end;
          //maintenance mode motor 2
          if(cmbMotorSelector.Text='2')then
          begin
            DataUn.dmDataModule.dsMotors.Filtered := False;
            DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + IntToStr(giUID) + ' and MotorNumber = 2';
            DataUn.dmDataModule.dsMotors.Filtered := True;
            dmDataModule.dsMotors.Edit;
            if GetBit(giDataM,2)=True then
            begin
              DataUn.dmDataModule.dsMotors.FieldByName('Maint').AsString := 'ENA';
              cmbMaint.ItemIndex:= 1;
            end
            else
            begin
              DataUn.dmDataModule.dsMotors.FieldByName('Maint').AsString := 'DIS';;
              cmbMaint.ItemIndex:= 0;
            end;
            DataUn.dmDataModule.dsMotors.MergeChangeLog;
          end;
          //maintenance mode motor 1
          if(cmbMotorSelector.Text='1')then
          begin
            DataUn.dmDataModule.dsMotors.Filtered := False;
            DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + IntToStr(giUID) + ' and MotorNumber = 1';
            DataUn.dmDataModule.dsMotors.Filtered := True;
            dmDataModule.dsMotors.Edit;
            if GetBit(giDataM,3)=True then
            begin
              DataUn.dmDataModule.dsMotors.FieldByName('Maint').AsString := 'ENA';
              cmbMaint.ItemIndex:= 1;
            end
            else
            begin
              DataUn.dmDataModule.dsMotors.FieldByName('Maint').AsString := 'DIS';
              cmbMaint.ItemIndex:= 0;
            end;
            DataUn.dmDataModule.dsMotors.MergeChangeLog;
          end;
          if(cmbMotorSelector.Text='1')then
          begin
            if GetBit(giDataM,4)=True then
            begin
              DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort1Enabled').AsBoolean := False;
              cbSWEnabled.OnClick:=nil;
              cbSWEnabled.Checked:=false;
              cbSWEnabled.OnClick:=cbSWEnabledClick;
            end
            else
            begin
              DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort1Enabled').AsBoolean := True;
              cbSWEnabled.OnClick:=nil;
              cbSWEnabled.Checked:=true;
              cbSWEnabled.OnClick:=cbSWEnabledClick;
            end;
          end;
          if(cmbMotorSelector.Text='2')then
          begin
            if GetBit(giDataM,5)=True then
            begin
              DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort2Enabled').AsBoolean := False;
              cbSWEnabled.OnClick:=nil;
              cbSWEnabled.Checked:=false;
              cbSWEnabled.OnClick:=cbSWEnabledClick;
            end
            else
            begin
              DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort2Enabled').AsBoolean := True;
              cbSWEnabled.OnClick:=nil;
              cbSWEnabled.Checked:=true;
              cbSWEnabled.OnClick:=cbSWEnabledClick;
            end;
          end;
          if(cmbMotorSelector.Text='3')then
          begin
            if GetBit(giDataM,6)=True then
            begin
              DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort3Enabled').AsBoolean := False;
              cbSWEnabled.OnClick:=nil;
              cbSWEnabled.Checked:=false;
              cbSWEnabled.OnClick:=cbSWEnabledClick;
            end
            else
            begin
              DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort3Enabled').AsBoolean := True;
              cbSWEnabled.OnClick:=nil;
              cbSWEnabled.Checked:=true;
              cbSWEnabled.OnClick:=cbSWEnabledClick;
            end;
          end;
          if(cmbMotorSelector.Text='4')then
          begin
            if GetBit(giDataM,7)=True then
            begin
              DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort4Enabled').AsBoolean := False;
              cbSWEnabled.OnClick:=nil;
              cbSWEnabled.Checked:=false;
              cbSWEnabled.OnClick:=cbSWEnabledClick;
            end
            else
            begin
              DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort4Enabled').AsBoolean := True;
              cbSWEnabled.OnClick:=nil;
              cbSWEnabled.Checked:=true;
              cbSWEnabled.OnClick:=cbSWEnabledClick;
            end;
          end;

          //Data L
          if(cmbMotorSelector.Text='1')then
          begin
            if GetBit(giDataL,0)=True then
            begin
              DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort1FWDEnabled').AsBoolean := True;
              cmbFWD.ItemIndex:=0;
            end
            else
            begin
              DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort1FWDEnabled').AsBoolean := False;
              cmbFWD.ItemIndex:=1;
            end;
          end;
          if(cmbMotorSelector.Text='2')then
          begin
            if GetBit(giDataL,1)=True then
            begin
              DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort2FWDEnabled').AsBoolean := True;
              cmbFWD.ItemIndex:=0;
            end
            else
            begin
              DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort2FWDEnabled').AsBoolean := False;
              cmbFWD.ItemIndex:=1;
            end;
          end;
          if(cmbMotorSelector.Text='3')then
          begin
            if GetBit(giDataL,2)=True then
            begin
              DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort3FWDEnabled').AsBoolean := True;
              cmbFWD.ItemIndex:=0;
            end
            else
            begin
              DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort3FWDEnabled').AsBoolean := False;
              cmbFWD.ItemIndex:=1;
            end;
          end;
           if(cmbMotorSelector.Text='4')then
          begin
            if GetBit(giDataL,3)=True then
            begin
              DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort4FWDEnabled').AsBoolean := True;
              cmbFWD.ItemIndex:=0;
            end
            else
            begin
              DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort4FWDEnabled').AsBoolean := False;
              cmbFWD.ItemIndex:=1;
            end;
          end;

          if GetBit(giDataL,4)=True then
          begin
            DataUn.dmDataModule.dsIQMLC2.FieldByName('MasterPortFwdEnabled').AsBoolean := True;
          end
          else
          begin
            DataUn.dmDataModule.dsIQMLC2.FieldByName('MasterPortFwdEnabled').AsBoolean := False;
          end;
          if GetBit(giDataL,5)=True then
          begin
            DataUn.dmDataModule.dsIQMLC2.FieldByName('AutoPortFWDEnabled').AsBoolean := True;
          end
          else
          begin
            DataUn.dmDataModule.dsIQMLC2.FieldByName('AutoPortFWDEnabled').AsBoolean := False;
          end;
          if GetBit(giDataL,6)=True then
          begin
            DataUn.dmDataModule.dsIQMLC2.FieldByName('StopMode').AsString := 'Normal';
            cmbUniform.ItemIndex:=1;
          end
          else
          begin
            DataUn.dmDataModule.dsIQMLC2.FieldByName('StopMode').AsString := 'Uniform';
            cmbUniform.ItemIndex:=0;
          end;

          if GetBit(giDataL,7)=True then
          begin
            DataUn.dmDataModule.dsIQMLC2.FieldByName('SwModeAll').AsString := 'IS';
            cmbPortMode.ItemIndex:=1;
          end
          else
          begin
            DataUn.dmDataModule.dsIQMLC2.FieldByName('SwModeAll').AsString := 'LS';
            cmbPortMode.ItemIndex:=0;
          end;

          DataUn.dmDataModule.dsIQMLC2.MergeChangeLog;


        end;
        41:                   // Parameters 2
        begin
          PassFlag:=True;
          dmDataModule.dsIQMLC2.Filtered := False;
          dmDataModule.dsIQMLC2.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + IntToStr(giUID);
          dmDataModule.dsIQMLC2.Filtered := True;
          dmDataModule.dsIQMLC2.Edit;


          if(cmbMotorSelector.Text='1')then
          begin
            DataUn.dmDataModule.dsMotors.Filtered := False;
            DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + IntToStr(giUID) + ' and MotorNumber = 1';
            DataUn.dmDataModule.dsMotors.Filtered := True;
            dmDataModule.dsMotors.Edit;
            if GetBit(giDataM,0)=True then
            begin
              DataUn.dmDataModule.dsMotors.FieldByName('Direction').AsString := 'Reverse';
              cmbDirection.ItemIndex:=1;
            end
            else
            begin
              DataUn.dmDataModule.dsMotors.FieldByName('Direction').AsString := 'Regular';
              cmbDirection.ItemIndex:=0;
            end;
            dmDataModule.dsMotors.MergeChangeLog;
          end;
          if(cmbMotorSelector.Text='2')then
          begin
            DataUn.dmDataModule.dsMotors.Filtered := False;
            DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + IntToStr(giUID) + ' and MotorNumber = 2';
            DataUn.dmDataModule.dsMotors.Filtered := True;
            dmDataModule.dsMotors.Edit;
            if GetBit(giDataM,1)=True then
            begin
              DataUn.dmDataModule.dsMotors.FieldByName('Direction').AsString := 'Reverse';
              cmbDirection.ItemIndex:=1;
            end
            else
            begin
              DataUn.dmDataModule.dsMotors.FieldByName('Direction').AsString := 'Regular';
              cmbDirection.ItemIndex:=0;
            end;
            DataUn.dmDataModule.dsMotors.MergeChangeLog;
          end;
          if(cmbMotorSelector.Text='3')then
          begin
            DataUn.dmDataModule.dsMotors.Filtered := False;
            DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + IntToStr(giUID) + ' and MotorNumber = 3';
            DataUn.dmDataModule.dsMotors.Filtered := True;
            dmDataModule.dsMotors.Edit;
            if GetBit(giDataM,2)=True then
            begin
              DataUn.dmDataModule.dsMotors.FieldByName('Direction').AsString := 'Reverse';
              cmbDirection.ItemIndex:=1;
            end
            else
            begin
              DataUn.dmDataModule.dsMotors.FieldByName('Direction').AsString := 'Regular';
              cmbDirection.ItemIndex:=0;
            end;
            DataUn.dmDataModule.dsMotors.MergeChangeLog;
          end;
          if(cmbMotorSelector.Text='4')then
          begin
            DataUn.dmDataModule.dsMotors.Filtered := False;
            DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + IntToStr(giUID) + ' and MotorNumber = 4';
            DataUn.dmDataModule.dsMotors.Filtered := True;
            dmDataModule.dsMotors.Edit;
            if GetBit(giDataM,3)=True then
            begin
              DataUn.dmDataModule.dsMotors.FieldByName('Direction').AsString := 'Reverse';
              cmbDirection.ItemIndex:=1;
            end
            else
            begin
              DataUn.dmDataModule.dsMotors.FieldByName('Direction').AsString := 'Regular';
              cmbDirection.ItemIndex:=0;
            end;
            DataUn.dmDataModule.dsMotors.MergeChangeLog;
          end;

          dmDataModule.dsIQMLC2.FieldByName('SwBtnPerson').AsString := IntToStr(giDataL);
           case VariablesUn.giDataL of
            9:
              cmbLVtype.ItemIndex := 0;
            17:
              cmbLVtype.ItemIndex := 1;
            33:
              cmbLVtype.ItemIndex := 2;
            18:
              cmbLVtype.ItemIndex := 3;
            34:
              cmbLVtype.ItemIndex := 4;
            4:
              cmbLVtype.ItemIndex := 5;
            68:
              cmbLVtype.ItemIndex := 6;
            132:
              cmbLVtype.ItemIndex := 7;
          end;
          dmDataModule.dsIQMLC2.MergeChangeLog;
//          //Motor Data to Motor Client Dataset
//          DataUn.dmDataModule.dsMotors.Filtered := False;
//          DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + IntToStr(giUID) + ' and MotorNumber = 1';
//          DataUn.dmDataModule.dsMotors.Filtered := True;
//          dmDataModule.dsMotors.Edit;
//          DataUn.dmDataModule.dsMotors.FieldByName('LVPort').AsString := VariablesUn.BuildZGN();
//          DataUn.dmDataModule.dsMotors.MergeChangeLog;


        end;
        42:                   // Motor Status
        begin
          //Low Byte
          if VariablesUn.giDataL >= 0 then
          begin
            if VariablesUn.GetBit(VariablesUn.giDataL, 0) then
            begin
              cmbDirection.ItemIndex := 1;
            end
            else
            begin
              cmbDirection.ItemIndex := 0;
            end;

            if VariablesUn.GetBit(VariablesUn.giDataL, 1) then
            begin
              cmbUniform.ItemIndex := 1;
            end
            else
            begin
              cmbUniform.ItemIndex := 0;
            end;

            if VariablesUn.GetBit(VariablesUn.giDataL, 2) then
            begin
              cmbLed.ItemIndex := 1;
            end
            else
            begin
              cmbLed.ItemIndex := 0;
            end;

            cbSWEnabled.OnClick:=nil;
            cbSWEnabled.checked := VariablesUn.GetBit(VariablesUn.giDataL, 3);
            cbSWEnabled.OnClick:=cbSWEnabledClick;

            if VariablesUn.GetBit(VariablesUn.giDataL, 4) then
            begin
              cmbFwd.ItemIndex := 0;
            end
            else
            begin
              cmbFwd.ItemIndex := 1;
            end;

            if VariablesUn.GetBit(VariablesUn.giDataL, 5) then
            begin
              cmbMaint.ItemIndex := 1 ;
            end
            else
            begin
              cmbMaint.ItemIndex := 0;
            end;

            if VariablesUn.GetBit(VariablesUn.giDataL, 6) then
            begin
              cmbMTAInb.ItemIndex := 1;
            end
            else
            begin
              cmbMTAINB.ItemIndex := 0;
            end;

            if VariablesUn.GetBit(VariablesUn.giDataL, 7) then
            begin
              cmbPortMode.ItemIndex := 1;
            end
            else
            begin
              cmbPortMode.ItemIndex := 0;
            end;
            PassFlag:=True;
          end;
          if VariablesUn.giDataM >= 0 then
          begin
            case VariablesUn.giDataM of
              9:
                cmbLVtype.ItemIndex := 0;
              17:
                cmbLVtype.ItemIndex := 1;
              33:
                cmbLVtype.ItemIndex := 2;
              18:
                cmbLVtype.ItemIndex := 3;
              34:
                cmbLVtype.ItemIndex := 4;
              4:
                cmbLVtype.ItemIndex := 5;
              68:
                cmbLVtype.ItemIndex := 6;
              132:
                cmbLVtype.ItemIndex := 7;
            end;
          end;
          if VariablesUn.giDataH >= 0 then
          begin
            if VariablesUn.GetBit(VariablesUn.giDataH, 0) then
              cmbInhibit.ItemIndex := 1
            else
              cmbInhibit.ItemIndex := 0;

            if VariablesUn.GetBit(VariablesUn.giDataH, 4) then
              cmbBOtype.ItemIndex := 1
            else
              cmbBOtype.ItemIndex := 0;
          end;
          PassFlag:=True;
        end;
        43:
        begin

          //MotorDirectionBits:= VariablesUn.giDataL;
          if VariablesUn.GetBit(VariablesUn.giDataL, StrToInt(cmbMotorSelector.text)-1) then
          begin
            cmbDirection.ItemIndex := 1     //not on mni-1
          end
          else
          begin
            cmbDirection.ItemIndex := 0;
          end;
          PassFlag:=True;
        end;
        44:                   // LV Port
        begin
          txtLVzgn.Text := ConvertZGN(VariablesUn.BuildZGN);
          gsLV := txtLVzgn.Text;
          if (txtLVzgn.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsLV)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              if (isMNI)or(isIQMLC2) then
              begin
                DataUn.dmDataModule.dsMotors.Filtered := False;
                DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = ' + cmbMotorSelector.text;
                DataUn.dmDataModule.dsMotors.Filtered := True;
                if not dmDataModule.dsMotors.Eof then
                begin
                  DataUn.dmDataModule.dsMotors.Edit;
                  DataUn.dmDataModule.dsMotors.FieldByName('LVPort').AsString := gsLV;
                  DataUn.dmDataModule.dsMotors.Post;
                  DataUn.dmDataModule.dsMotors.MergeChangeLog;
                  DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                  txtLVzgn.Color := clWhite;

                end;
                DataUn.dmDataModule.dsMotors.Filtered := False;

                DataUn.dmDataModule.dsMNI.Filtered := False;
                DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text);
                DataUn.dmDataModule.dsMNI.Filtered := True;

                DataUn.dmDataModule.dsMNI.Edit;
                DataUn.dmDataModule.dsMNI.FieldByName('SwPort1FwdAddr').AsString := gsLV;
                DataUn.dmDataModule.dsMNI.MergeChangeLog;
                DataUn.dmDataModule.dsMNI.SaveToFile(VariablesUn.gsDataPath + 'dsMNI.txt', dfXML);
                DataUn.dmDataModule.dsMNI.Filtered := False;
              end
              else
              begin
                DataUn.dmDataModule.dsMotors.Filtered := False;
                DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text);
                DataUn.dmDataModule.dsMotors.Filtered := True;
                if not dmDataModule.dsMotors.Eof then
                begin
                  DataUn.dmDataModule.dsMotors.Edit;
                  DataUn.dmDataModule.dsMotors.FieldByName('LVPort').AsString := gsLV;
                  DataUn.dmDataModule.dsMotors.Post;
                  DataUn.dmDataModule.dsMotors.MergeChangeLog;
                  DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                  txtLVzgn.Color := clWhite;

                end;
                DataUn.dmDataModule.dsMotors.Filtered := False;
              end;
            end;
          end;
          PassFlag:=True;
        end;
        45:
        begin
          txtLVzgn.Text := ConvertZGN(VariablesUn.BuildZGN);
          gsLV := txtLVzgn.Text;
          if (txtLVzgn.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsLV)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              DataUn.dmDataModule.dsMotors.Filtered := False;
              DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = ' + cmbMotorSelector.text;
              DataUn.dmDataModule.dsMotors.Filtered := True;
              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('LVPort').AsString := gsLV;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtLVzgn.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;

              DataUn.dmDataModule.dsMNI.Filtered := False;
              DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text);
              DataUn.dmDataModule.dsMNI.Filtered := True;

              DataUn.dmDataModule.dsMNI.Edit;
              DataUn.dmDataModule.dsMNI.FieldByName('SwPort2FwdAddr').AsString := gsLV;
              DataUn.dmDataModule.dsMNI.MergeChangeLog;
              DataUn.dmDataModule.dsMNI.SaveToFile(VariablesUn.gsDataPath + 'dsMNI.txt', dfXML);
              DataUn.dmDataModule.dsMNI.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        46:
        begin
          txtLVzgn.Text := ConvertZGN(VariablesUn.BuildZGN);
          gsLV := txtLVzgn.Text;
          if (txtLVzgn.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsLV)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              DataUn.dmDataModule.dsMotors.Filtered := False;
              DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = ' + cmbMotorSelector.text;
              DataUn.dmDataModule.dsMotors.Filtered := True;
              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('LVPort').AsString := gsLV;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtLVzgn.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;

              DataUn.dmDataModule.dsMNI.Filtered := False;
              DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text);
              DataUn.dmDataModule.dsMNI.Filtered := True;

              DataUn.dmDataModule.dsMNI.Edit;
              DataUn.dmDataModule.dsMNI.FieldByName('SwPort3FwdAddr').AsString := gsLV;
              DataUn.dmDataModule.dsMNI.MergeChangeLog;
              DataUn.dmDataModule.dsMNI.SaveToFile(VariablesUn.gsDataPath + 'dsMNI.txt', dfXML);
              DataUn.dmDataModule.dsMNI.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        47:
        begin
          txtLVzgn.Text := ConvertZGN(VariablesUn.BuildZGN);
          gsLV := txtLVzgn.Text;
          if (txtLVzgn.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsLV)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              DataUn.dmDataModule.dsMotors.Filtered := False;
              DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = ' + cmbMotorSelector.text;
              DataUn.dmDataModule.dsMotors.Filtered := True;
              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('LVPort').AsString := gsLV;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtLVzgn.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;

              DataUn.dmDataModule.dsMNI.Filtered := False;
              DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text);
              DataUn.dmDataModule.dsMNI.Filtered := True;

              DataUn.dmDataModule.dsMNI.Edit;
              DataUn.dmDataModule.dsMNI.FieldByName('SwPort4FwdAddr').AsString := gsLV;
              DataUn.dmDataModule.dsMNI.MergeChangeLog;
              DataUn.dmDataModule.dsMNI.SaveToFile(VariablesUn.gsDataPath + 'dsMNI.txt', dfXML);
              DataUn.dmDataModule.dsMNI.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        50:                   // Address 1
        begin
          txtA5.Text := ConvertZGN(VariablesUn.BuildZGN);
          gsA5 := txtA5.Text;

          if (txtA5.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsA5)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              if (isMNI)or(isIQMLC2) then
              begin
                DataUn.dmDataModule.dsMotors.Filtered := False;
                DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 1';
                DataUn.dmDataModule.dsMotors.Filtered := True;


              end
              else
              begin
                DataUn.dmDataModule.dsMotors.Filtered := False;
                DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text);
                DataUn.dmDataModule.dsMotors.Filtered := True;
              end;
              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('Address1').AsString := gsA5;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtA5.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        51:                   // Motor 2 Address 1
        begin
          txtA5.Text := ConvertZGN(VariablesUn.BuildZGN);
          gsA5 := txtA5.Text;

          if (txtA5.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsA5)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin

              DataUn.dmDataModule.dsMotors.Filtered := False;
              DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 2';
              DataUn.dmDataModule.dsMotors.Filtered := True;

              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('Address1').AsString := gsA5;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtA5.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        52:                   // Motor 3 Address 1
        begin
          txtA5.Text := ConvertZGN(VariablesUn.BuildZGN);
          gsA5 := txtA5.Text;

          if (txtA5.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsA5)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin

              DataUn.dmDataModule.dsMotors.Filtered := False;
              DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 3';
              DataUn.dmDataModule.dsMotors.Filtered := True;

              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('Address1').AsString := gsA5;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtA5.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        53:                   // Motor 4 Address 1
        begin
          txtA5.Text := ConvertZGN(VariablesUn.BuildZGN);
          gsA5 := txtA5.Text;

          if (txtA5.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsA5)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin

              DataUn.dmDataModule.dsMotors.Filtered := False;
              DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 4';
              DataUn.dmDataModule.dsMotors.Filtered := True;

              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('Address1').AsString := gsA5;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtA5.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        54:                   // Motor1 Address 2
        begin
          txtA6.Text := ConvertZGN(VariablesUn.BuildZGN);
          gsA6 := txtA6.Text;
          if (txtA6.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsA6)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              if (isMNI)or(isIQMLC2) then
              begin
                DataUn.dmDataModule.dsMotors.Filtered := False;
                DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 1';
                DataUn.dmDataModule.dsMotors.Filtered := True;
              end
              else
              begin
                DataUn.dmDataModule.dsMotors.Filtered := False;
                DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text);
                DataUn.dmDataModule.dsMotors.Filtered := True;
              end;
              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('Address2').AsString := gsA6;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtA6.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        55:                   //Motor 2 Address 2
        begin
          txtA6.Text := ConvertZGN(VariablesUn.BuildZGN);
          gsA6 := txtA6.Text;
          if (txtA6.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsA6)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              DataUn.dmDataModule.dsMotors.Filtered := False;
              DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 2';
              DataUn.dmDataModule.dsMotors.Filtered := True;

              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('Address2').AsString := gsA6;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtA6.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        56:                   //Motor 3 Address 2
        begin
          txtA6.Text := ConvertZGN(VariablesUn.BuildZGN);
          gsA6 := txtA6.Text;
          if (txtA6.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsA6)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              DataUn.dmDataModule.dsMotors.Filtered := False;
              DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 3';
              DataUn.dmDataModule.dsMotors.Filtered := True;

              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('Address2').AsString := gsA6;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtA6.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        57:                   //Motor 4 Address 2
        begin
          txtA6.Text := ConvertZGN(VariablesUn.BuildZGN);
          gsA6 := txtA6.Text;
          if (txtA6.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsA6)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              DataUn.dmDataModule.dsMotors.Filtered := False;
              DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 4';
              DataUn.dmDataModule.dsMotors.Filtered := True;

              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('Address2').AsString := gsA6;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtA6.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        58:                   // Motor 1 Address 3
        begin
          txtA7.Text :=ConvertZGN(VariablesUn.BuildZGN);
          gsA7 := txtA7.Text;

          if (txtA7.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsA7)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              if (isMNI)or(isIQMLC2) then
              begin
                DataUn.dmDataModule.dsMotors.Filtered := False;
                DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 1';
                DataUn.dmDataModule.dsMotors.Filtered := True;
              end
              else
              begin
                DataUn.dmDataModule.dsMotors.Filtered := False;
                DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text);
                DataUn.dmDataModule.dsMotors.Filtered := True;
              end;
              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('Address3').AsString := gsA7;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtA7.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        59:                   // Motor 2 Address 3
        begin
          txtA7.Text :=ConvertZGN(VariablesUn.BuildZGN);
          gsA7 := txtA7.Text;

          if (txtA7.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsA7)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              DataUn.dmDataModule.dsMotors.Filtered := False;
              DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 2';
              DataUn.dmDataModule.dsMotors.Filtered := True;

              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('Address3').AsString := gsA7;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtA7.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        60:                   // Motor 3 Address 3
        begin
          txtA7.Text :=ConvertZGN(VariablesUn.BuildZGN);
          gsA7 := txtA7.Text;

          if (txtA7.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsA7)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              DataUn.dmDataModule.dsMotors.Filtered := False;
              DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 3';
              DataUn.dmDataModule.dsMotors.Filtered := True;

              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('Address3').AsString := gsA7;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtA7.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        61:                   // Motor 4 Address 3
        begin
          txtA7.Text :=ConvertZGN(VariablesUn.BuildZGN);
          gsA7 := txtA7.Text;

          if (txtA7.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsA7)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              DataUn.dmDataModule.dsMotors.Filtered := False;
              DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 4';
              DataUn.dmDataModule.dsMotors.Filtered := True;

              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('Address3').AsString := gsA7;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtA7.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        62:                   // Motor 1 Address 4
        begin
          txtA8.Text := ConvertZGN(VariablesUn.BuildZGN);
          gsA8 := txtA8.Text;

          if (txtA8.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsA8)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              if (isMNI)or(isIQMLC2) then
              begin
                DataUn.dmDataModule.dsMotors.Filtered := False;
                DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 1';
                DataUn.dmDataModule.dsMotors.Filtered := True;
              end
              else
              begin
                DataUn.dmDataModule.dsMotors.Filtered := False;
                DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text);
                DataUn.dmDataModule.dsMotors.Filtered := True;
              end;
              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('Address4').AsString := gsA8;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtA8.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        63:                   // Motor 2 Address 4
        begin
          txtA8.Text := ConvertZGN(VariablesUn.BuildZGN);
          gsA8 := txtA8.Text;

          if (txtA8.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsA8)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              DataUn.dmDataModule.dsMotors.Filtered := False;
              DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 2';
              DataUn.dmDataModule.dsMotors.Filtered := True;

              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('Address4').AsString := gsA8;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtA8.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        64:                   // Motor 3 Address 4
        begin
          txtA8.Text := ConvertZGN(VariablesUn.BuildZGN);
          gsA8 := txtA8.Text;

          if (txtA8.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsA8)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              DataUn.dmDataModule.dsMotors.Filtered := False;
              DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 3';
              DataUn.dmDataModule.dsMotors.Filtered := True;

              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('Address4').AsString := gsA8;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtA8.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        65:                   // Motor 4 Address 4
        begin
          txtA8.Text := ConvertZGN(VariablesUn.BuildZGN);
          gsA8 := txtA8.Text;

          if (txtA8.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsA8)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              DataUn.dmDataModule.dsMotors.Filtered := False;
              DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 4';
              DataUn.dmDataModule.dsMotors.Filtered := True;

              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('Address4').AsString := gsA8;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtA8.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        66:                   // Motor 1 Address 5
        begin
          txtA1.Text := ConvertZGN(VariablesUn.BuildZGN);
          gsA1 := txtA1.Text;

          if (txtA1.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsA1)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              if (isMNI)or(isIQMLC2) then
              begin
                DataUn.dmDataModule.dsMotors.Filtered := False;
                DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 1';
                DataUn.dmDataModule.dsMotors.Filtered := True;
              end
              else
              begin
                DataUn.dmDataModule.dsMotors.Filtered := False;
                DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text);
                DataUn.dmDataModule.dsMotors.Filtered := True;
              end;
              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('Address5').AsString := gsA1;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtA1.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        67:                   // Motor 2 Address 5
        begin
          txtA1.Text := ConvertZGN(VariablesUn.BuildZGN);
          gsA1 := txtA1.Text;

          if (txtA1.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsA1)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              DataUn.dmDataModule.dsMotors.Filtered := False;
              DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 2';
              DataUn.dmDataModule.dsMotors.Filtered := True;

              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('Address5').AsString := gsA1;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtA1.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        68:                   // Motor 3 Address 5
        begin
          txtA1.Text := ConvertZGN(VariablesUn.BuildZGN);
          gsA1 := txtA1.Text;

          if (txtA1.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsA1)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              DataUn.dmDataModule.dsMotors.Filtered := False;
              DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 3';
              DataUn.dmDataModule.dsMotors.Filtered := True;

              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('Address5').AsString := gsA1;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtA1.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        69:                   // Motor 4 Address 5
        begin
          txtA1.Text := ConvertZGN(VariablesUn.BuildZGN);
          gsA1 := txtA1.Text;

          if (txtA1.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsA1)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              DataUn.dmDataModule.dsMotors.Filtered := False;
              DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 4';
              DataUn.dmDataModule.dsMotors.Filtered := True;

              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('Address5').AsString := gsA1;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtA1.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        70:                   // Motor 1 Address 6
        begin
          txtA2.Text := ConvertZGN(VariablesUn.BuildZGN);
          gsA2 := txtA2.Text;



          if (txtA2.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsA2)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              if (isMNI)or(isIQMLC2) then
              begin
                DataUn.dmDataModule.dsMotors.Filtered := False;
                DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 1';
                DataUn.dmDataModule.dsMotors.Filtered := True;
              end
              else
              begin
                DataUn.dmDataModule.dsMotors.Filtered := False;
                DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text);
                DataUn.dmDataModule.dsMotors.Filtered := True;
              end;
              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('Address6').AsString := gsA2;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtA2.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        71:                   // Motor 2 Address 6
        begin
          txtA2.Text := ConvertZGN(VariablesUn.BuildZGN);
          gsA2 := txtA2.Text;



          if (txtA2.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsA2)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              DataUn.dmDataModule.dsMotors.Filtered := False;
              DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 2';
              DataUn.dmDataModule.dsMotors.Filtered := True;

              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('Address6').AsString := gsA2;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtA2.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        72:                   // Motor 3 Address 6
        begin
          txtA2.Text := ConvertZGN(VariablesUn.BuildZGN);
          gsA2 := txtA2.Text;



          if (txtA2.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsA2)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              DataUn.dmDataModule.dsMotors.Filtered := False;
              DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 3';
              DataUn.dmDataModule.dsMotors.Filtered := True;

              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('Address6').AsString := gsA2;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtA2.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        73:                   // Motor 4 Address 6
        begin
          txtA2.Text := ConvertZGN(VariablesUn.BuildZGN);
          gsA2 := txtA2.Text;

          if (txtA2.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsA2)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              DataUn.dmDataModule.dsMotors.Filtered := False;
              DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 4';
              DataUn.dmDataModule.dsMotors.Filtered := True;

              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('Address6').AsString := gsA2;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtA2.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        74:                   // Motor 1 Address 7
        begin
          txtA3.Text :=ConvertZGN(VariablesUn.BuildZGN);
          gsA3 := txtA3.Text;


          if (txtA3.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsA3)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              if (isMNI)or(isIQMLC2) then
              begin
                DataUn.dmDataModule.dsMotors.Filtered := False;
                DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 1';
                DataUn.dmDataModule.dsMotors.Filtered := True;
              end
              else
              begin
                DataUn.dmDataModule.dsMotors.Filtered := False;
                DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text);
                DataUn.dmDataModule.dsMotors.Filtered := True;
              end;
              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('Address7').AsString := gsA3;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtA3.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        75:                   // Motor 2 Address 7
        begin
          txtA3.Text :=ConvertZGN(VariablesUn.BuildZGN);
          gsA3 := txtA3.Text;


          if (txtA3.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsA3)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              DataUn.dmDataModule.dsMotors.Filtered := False;
              DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 2';
              DataUn.dmDataModule.dsMotors.Filtered := True;

              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('Address7').AsString := gsA3;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtA3.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        76:                   // Motor 3 Address 7
        begin
          txtA3.Text :=ConvertZGN(VariablesUn.BuildZGN);
          gsA3 := txtA3.Text;


          if (txtA3.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsA3)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              DataUn.dmDataModule.dsMotors.Filtered := False;
              DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 3';
              DataUn.dmDataModule.dsMotors.Filtered := True;

              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('Address7').AsString := gsA3;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtA3.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        77:                   // Motor 4 Address 7
        begin
          txtA3.Text :=ConvertZGN(VariablesUn.BuildZGN);
          gsA3 := txtA3.Text;


          if (txtA3.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsA3)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              DataUn.dmDataModule.dsMotors.Filtered := False;
              DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 4';
              DataUn.dmDataModule.dsMotors.Filtered := True;

              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('Address7').AsString := gsA3;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtA3.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        78:                   // Motor 1 Address 8
        begin
          txtA4.Text := ConvertZGN(VariablesUn.BuildZGN);
          gsA4 := txtA4.Text;


          if (txtA4.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsA4)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              if (isMNI)or(isIQMLC2) then
              begin
                DataUn.dmDataModule.dsMotors.Filtered := False;
                DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 1';
                DataUn.dmDataModule.dsMotors.Filtered := True;
              end
              else
              begin
                DataUn.dmDataModule.dsMotors.Filtered := False;
                DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text);
                DataUn.dmDataModule.dsMotors.Filtered := True;
              end;
              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('Address8').AsString := gsA4;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtA4.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        79:                   // Motor 2 Address 8
        begin
          txtA4.Text := ConvertZGN(VariablesUn.BuildZGN);
          gsA4 := txtA4.Text;


          if (txtA4.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsA4)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              DataUn.dmDataModule.dsMotors.Filtered := False;
              DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 2';
              DataUn.dmDataModule.dsMotors.Filtered := True;

              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('Address8').AsString := gsA4;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtA4.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        80:                   // Motor 3 Address 8
        begin
          txtA4.Text := ConvertZGN(VariablesUn.BuildZGN);
          gsA4 := txtA4.Text;


          if (txtA4.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsA4)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              DataUn.dmDataModule.dsMotors.Filtered := False;
              DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 3';
              DataUn.dmDataModule.dsMotors.Filtered := True;

              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('Address8').AsString := gsA4;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtA4.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        81:                   // Motor 4 Address 8
        begin
          txtA4.Text := ConvertZGN(VariablesUn.BuildZGN);
          gsA4 := txtA4.Text;


          if (txtA4.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsA4)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              DataUn.dmDataModule.dsMotors.Filtered := False;
              DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 4';
              DataUn.dmDataModule.dsMotors.Filtered := True;

              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('Address8').AsString := gsA4;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtA4.Color := clWhite;

              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        85:
        begin
          PassFlag:=True;
          lblAlias.Text := Trim(VariablesUn.gsMotorType);
          //showmessage('hello');

        end;
        88:
        begin
          DataUn.dmDataModule.dsMotors.Filtered := False;
          DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text);
          DataUn.dmDataModule.dsMotors.Filtered := True;
          DataUn.dmDataModule.dsMotors.Edit;
          DataUn.dmDataModule.dsMotors.FieldByName('AutoReturn').AsInteger := VariablesUn.giDataL;
          DataUn.dmDataModule.dsMotors.MergeChangeLog;
          teAutoReturnIQ2.Text:= IntToStr(VariablesUn.giDataL);
          PassFlag:=True;
        end;
        89:
        begin
          DataUn.dmDataModule.dsMotors.Filtered := False;
          DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text);
          DataUn.dmDataModule.dsMotors.Filtered := True;
          DataUn.dmDataModule.dsMotors.Edit;
          DataUn.dmDataModule.dsMotors.FieldByName('MasterUID').AsInteger := VariablesUn.DataMidLowConvert;
          DataUn.dmDataModule.dsMotors.MergeChangeLog;
          teMasterUIDIQ2.Text:= IntToStr(VariablesUn.DataMidLowConvert);
          teMasterUIDIQ2.color:=clwhite;
          PassFlag:=True;
        end;
        115:                   // Black Out type and Blackout Mode   MNI Update
        begin
          if (isMNI) then
          begin
            MotorBOModeBits:=VariablesUn.giDataL;
            MotorBOTypeBits:=VariablesUn.giDataM;
            DataUn.dmDataModule.dsMotors.Filtered := False;
            DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = '+cmbMotorSelector.Text;
            DataUn.dmDataModule.dsMotors.Filtered := True;


            DataUn.dmDataModule.dsMotors.Edit;
            DataUn.dmDataModule.dsMotors.FieldByName('BlackoutMode').AsInteger := Ord(VariablesUn.GetBit(VariablesUn.giDataL,(StrToInt(cmbMotorSelector.Text)-1)));
            DataUn.dmDataModule.dsMotors.FieldByName('BOType').AsString := IntToStr(Ord(VariablesUn.GetBit(VariablesUn.giDataM,StrToInt(cmbMotorSelector.Text)-1)));
            if VariablesUn.GetBit(VariablesUn.giDataM,StrToInt(cmbMotorSelector.Text)-1) then
              cmbBOtype.ItemIndex := 1
            else
              cmbBOtype.ItemIndex := 0;
            cmbBOMode.itemindex:= DataUn.dmDataModule.dsMotors.FieldByName('BlackoutMode').AsInteger;
          end
          else
          begin
            DataUn.dmDataModule.dsMotors.Filtered := False;
            DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text);
            DataUn.dmDataModule.dsMotors.Filtered := True;

            DataUn.dmDataModule.dsMotors.Edit;
            DataUn.dmDataModule.dsMotors.FieldByName('BlackoutMode').AsInteger := VariablesUn.giDataL;
            DataUn.dmDataModule.dsMotors.FieldByName('BOType').AsString := IntToStr(VariablesUn.giDataM);
            cmbBOMode.itemindex:= DataUn.dmDataModule.dsMotors.FieldByName('BlackoutMode').AsInteger;
          end;
          DataUn.dmDataModule.dsMotors.MergeChangeLog;
          PassFlag:=True;
        end;
        116:                   //Motor 1 Black Out Address
        begin
          txtBOzgn.Text := ConvertZGN(VariablesUn.BuildZGN);
          gsBO := txtBOzgn.Text;

          if (txtBOzgn.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsBO)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              if isMni then
              begin
                DataUn.dmDataModule.dsMotors.Filtered := False;
                DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 1';;
                DataUn.dmDataModule.dsMotors.Filtered := True;
              end
              else
              begin
                DataUn.dmDataModule.dsMotors.Filtered := False;
                DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text);
                DataUn.dmDataModule.dsMotors.Filtered := True;
              end;
              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('BlackOut').AsString := gsBO;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtBOzgn.Color := clWhite;
              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        122:                   // Motor2 Black Out Address
        begin
          txtBOzgn.Text := ConvertZGN(VariablesUn.BuildZGN);
          gsBO := txtBOzgn.Text;

          if (txtBOzgn.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsBO)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              DataUn.dmDataModule.dsMotors.Filtered := False;
                DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 2';;
                DataUn.dmDataModule.dsMotors.Filtered := True;
              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('BlackOut').AsString := gsBO;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtBOzgn.Color := clWhite;
              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        123:                   // Motor3 Black Out Address
        begin
          txtBOzgn.Text := ConvertZGN(VariablesUn.BuildZGN);
          gsBO := txtBOzgn.Text;

          if (txtBOzgn.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsBO)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              DataUn.dmDataModule.dsMotors.Filtered := False;
                DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 3';;
                DataUn.dmDataModule.dsMotors.Filtered := True;
              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('BlackOut').AsString := gsBO;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtBOzgn.Color := clWhite;
              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        124:                   // Motor4 Black Out Address
        begin
          txtBOzgn.Text := ConvertZGN(VariablesUn.BuildZGN);
          gsBO := txtBOzgn.Text;

          if (txtBOzgn.Color = $00A4FFFF) and (VariablesUn.IsZGN(gsBO)) then
          begin
            if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
            begin
              DataUn.dmDataModule.dsMotors.Filtered := False;
                DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = 4';
                DataUn.dmDataModule.dsMotors.Filtered := True;
              if not dmDataModule.dsMotors.Eof then
              begin
                DataUn.dmDataModule.dsMotors.Edit;
                DataUn.dmDataModule.dsMotors.FieldByName('BlackOut').AsString := gsBO;
                DataUn.dmDataModule.dsMotors.Post;
                DataUn.dmDataModule.dsMotors.MergeChangeLog;
                DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
                txtBOzgn.Color := clWhite;
              end;
              DataUn.dmDataModule.dsMotors.Filtered := False;
            end;
          end;
          PassFlag:=True;
        end;
        132:
        begin
          DataUn.dmDataModule.dsMotors.Filtered := False;
          DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = '+cmbMotorSelector.Text;
          DataUn.dmDataModule.dsMotors.Filtered := True;
          if not dmDataModule.dsMotors.Eof then
          begin
            DataUn.dmDataModule.dsMotors.Edit;
            DataUn.dmDataModule.dsMotors.FieldByName('MotorPerson').AsString := IntToStr(VariablesUn.giDataL);
            cmbDCMotors.ItemIndex:=VariablesUn.giDataL;
          end;
          PassFlag:=True;
        end;
        140:
        begin
          DataUn.dmDataModule.dsMotors.Filtered := False;
          DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = '+cmbMotorSelector.Text;
          DataUn.dmDataModule.dsMotors.Filtered := True;
          if not dmDataModule.dsMotors.Eof then
          begin
            DataUn.dmDataModule.dsMotors.Edit;
            DataUn.dmDataModule.dsMotors.FieldByName('MtaInhibit').AsString := IntToStr(Ord(VariablesUn.GetBit(VariablesUn.giDataH,StrToInt(cmbMotorSelector.Text)-1)));

            if VariablesUn.GetBit(VariablesUn.giDataH,StrToInt(cmbMotorSelector.Text)-1)then
              cmbMTAInb.ItemIndex := 1
            else
              cmbMTAINB.ItemIndex := 0;

            if VariablesUn.GetBit(VariablesUn.giDataH, StrToInt(cmbMotorSelector.Text)+3) then
              cmbUniform.ItemIndex := 1
            else
              cmbUniform.ItemIndex := 0;

            if VariablesUn.GetBit(VariablesUn.giDataM, StrToInt(cmbMotorSelector.Text)-1) then
              cmbPortMode.ItemIndex := 1
            else
              cmbPortMode.ItemIndex := 0;


            DataUn.dmDataModule.dsMotors.MergeChangeLog;
          end;
          PassFlag:=True;
        end;
        148:
        begin
          DataUn.dmDataModule.dsMotors.Filtered := False;
          DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text);
          DataUn.dmDataModule.dsMotors.Filtered := True;
          if not dmDataModule.dsMotors.Eof then
          begin
            DataUn.dmDataModule.dsMotors.Edit;
            DataUn.dmDataModule.dsMotors.FieldByName('AutoReturn').AsInteger := DataMidLowConvert();
            teAutoReturnIQ2.text:= IntToStr(DataMidLowConvert());
            DataUn.dmDataModule.dsMotors.MergeChangeLog;
          end;
          PassFlag:=True;
        end;
        151:
        begin
          case VariablesUn.giDataH of
            0:
            begin
              if isDC then
              begin
                teUpSpeed.Text:=IntToStr(VariablesUn.giDataL);
                teUpSpeed.Color:=clWhite;
                teUpSpeedFine.Text:=IntToStr(VariablesUn.giDataM);
                teUpSpeedFine.Color:=clWhite;
              end
              else //IQ2DC
              begin
                teIQ2DcDnSpeed.Text:=LeftStr(IntToStr(DataMidLowConvert()),2);;
                teIQ2DcDnSpeed.Color:=clWhite;
                teIQ2DcDnSpeedFine.Text:=RightStr(IntToStr(DataMidLowConvert()),1);
                teIQ2DcDnSpeedFine.Color:=clWhite;
              end;
            end;
            1:
            begin
              teSlowSpeed.Text:=IntToStr(VariablesUn.giDataL);
              teSlowSpeed.Color:=clWhite;
              teSlowSpeedFine.Text:=IntToStr(VariablesUn.giDataM);
              teSlowSpeedFine.Color:=clWhite;
            end;
            2:
            begin
              if VariablesUn.giDataM=0 then
              begin
                teRampPeriodUP.Text:=IntToStr(VariablesUn.giDataL);
                teRampPeriodUP.Color:=clWhite;
              end
              else
              begin
                teRampPeriodDn.Text:=IntToStr(VariablesUn.giDataL);
                teRampPeriodDn.Color:=clWhite;
              end;

            end;
            3:
            begin
              teDnSpeed.Text:=IntToStr(VariablesUn.giDataL);
              teDnSpeed.Color:=clWhite;
              teDnSpeedFine.Text:=IntToStr(VariablesUn.giDataM);
              teDnSpeedFine.Color:=clWhite;
            end;
          end;
          PassFlag:=True;
        end;
        152:
        begin

          teIQ2DcUpSpeed.Text:=LeftStr(IntToStr(DataMidLowConvert()),2);
          teIQ2DcUpSpeed.Color:=clWhite;
          teIQ2DcUpSpeedFine.Text:=RightStr(IntToStr(DataMidLowConvert()),1);
          teIQ2DcUpSpeedFine.Color:=clWhite;
          PassFlag:=True;
        end;
        153:
        begin
          teIQ2DcSlowDnSpeed.Text:=LeftStr(IntToStr(DataMidLowConvert()),2);
          teIQ2DcSlowDnSpeed.Color:=clWhite;
          teIQ2DcSlowDnSpeedFine.Text:=RightStr(IntToStr(DataMidLowConvert()),1);
          teIQ2DcSlowDnSpeedFine.Color:=clWhite;
          PassFlag:=True;
        end;
        154:
        begin
          teIQ2DcSlowUpSpeed.Text:=LeftStr(IntToStr(DataMidLowConvert()),2);
          teIQ2DcSlowUpSpeed.Color:=clWhite;
          teIQ2DcSlowUpSpeedFine.Text:=RightStr(IntToStr(DataMidLowConvert()),1);
          teIQ2DcSlowUpSpeedFine.Color:=clWhite;
          PassFlag:=True;
        end;
        155:
        begin
          if VariablesUn.giDataL>9 then
          begin
            teIQ2DcRampDnTime.Text:=LeftStr(IntToStr(VariablesUn.giDataL),1);
            teIQ2DcRampDnTimeFine.Text:=RightStr(IntToStr(VariablesUn.giDataL),1);
          end
          else
          begin
            teIQ2DcRampDnTime.Text:='0';
            teIQ2DcRampDnTimeFine.Text:=IntToStr(VariablesUn.giDataL);
          end;
          teIQ2DcRampDnTime.Color:=clWhite;
          teIQ2DcRampDnTimeFine.Color:=clWhite;
          PassFlag:=True;
        end;
        156:
        begin
          if VariablesUn.giDataL>9 then
          begin
            teIQ2DcRampUpTime.Text:=LeftStr(IntToStr(VariablesUn.giDataL),1);
            teIQ2DcRampUpTimeFine.Text:=RightStr(IntToStr(VariablesUn.giDataL),1);
          end
          else
          begin
            teIQ2DcRampUpTime.Text:='0';
            teIQ2DcRampUpTimeFine.Text:=IntToStr(VariablesUn.giDataL);
          end;
          teIQ2DcRampUpTime.Color:=clWhite;
          teIQ2DcRampUpTimeFine.Color:=clWhite;
          PassFlag:=True;
        end;
        164:
        begin
          if VariablesUn.giDataH=1 then
          begin
            mInternalThreshold:=VariablesUn.giDataM;
            mExternalThreshold:=VariablesUn.giDataL;
          end;
          PassFlag:=True;
        end;
        199:
        begin
          case VariablesUn.giDataH of
            8:
            begin
              PassFlag:=True;
            end;

          end;
          PassFlag:=True;
        end;
        200:                         // motor type
        begin
          case VariablesUn.giDataH of
            0:
            begin
             lblMode.Caption := 'Cal';
            end;
            1:
            begin
              lblMode.Caption := 'Run';
            end;
            2:
            begin
              lblMode.Caption := 'Boot';
            end;
            else
            begin
              lblMode.Caption := '???';
            end;
          end;

          case VariablesUn.giDataL of
            65:
            begin
             lblMtrType.Caption := 'IQMLC2';
            end;
            66:
            begin
              lblMtrType.Caption := 'IQ2';
            end;
            69:
            begin
              lblMtrType.Caption := 'IQ2-DC';
            end;
            70:
            begin
              lblMtrType.Caption := 'iQ3-DC';
            end;
            74:
            begin
              lblMtrType.Caption := 'MNI';
            end;
            77:
            begin
              lblMtrType.Caption := 'DC50';
            end;
            78:
            begin
              lblMtrType.Caption := 'AC50';
            end;
            79:
            begin
              lblMtrType.Caption := 'ST30';
            end;
            205:
            begin
              lblMtrType.Caption := 'DC50 SW';
            end;
            206:
            begin
              lblMtrType.Caption := 'AC50 SW';
            end;
            207:
            begin
              lblMtrType.Caption := 'ST30 SW';
            end;
            else
            begin
              lblMtrType.Caption := '???';
            end;
          end;

          case VariablesUn.giDataM of
            0:
            begin
             lblSystem.Caption := 'R Shade';
            end;
            1:
            begin
              lblSystem.Caption := 'Venetian Blind';
            end;
            2:
            begin
              lblSystem.Caption := '60mm IQ2 Motor';
            end;
            else
            begin
              lblSystem.Caption := '';
            end;
          end;
          PassFlag:=True;
        end;
        12:                  // motor revision
        begin
          //txtRevision.Text := IntToStr(VariablesUn.giDataM) + '.' + IntToStr(VariablesUn.giDataL);
          if pos('IQ2',cmbUID.text)>0 then
          begin
            if(giDatam=55)then
            begin
              if giDataL<=54 then
              begin
                vbSkipNSM:=True;
              end
              else
              begin
                vbSkipNSM:=False;
              end;
            end
            else if(giDatam>55)then
            begin
              vbSkipNSM:=False;
            end
            else
            begin
              vbSkipNSM:=True;
            end;
          end
          else
          begin
            vbSkipNSM:=True;
          end;
          txtRevision.Text := BuildFirmwareRev();
          passFlag:=true;
          SaveParameters;

        end;
      end;
    end;
    ComFm.rh_ReturnHandler := nil;
    rhEnable;
    except
    on E: Exception do
    begin
      rhEnable;
      //Screen.Cursor := crDefault;
      MessageDlg('TConfigScreenFm.rhMotorStatusRecieved: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.Shape14MouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := False;
end;

procedure TConfigScreenFm.ShowAddress;
begin
  try
    sbmain.VertScrollBar.Position:=0;
    pnlBotZGN.Visible:=true;
//    pnlLV.Top:=261;
//    pnlProgramWink.Top:=293;
//    pnlSettings.Top:=348;
//    if isdc then
//    begin
//      pnlDCSpeed.Top:=673;
//    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.ShowAddress: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.ShowUID;
var
    DeviceType,ComboBoxString:String;
    ComboUIDList:TStringList;
begin
  try
    ComboUIDList:=TStringList.Create;
    cmbUID.Style := csDropDownList;
    cmbUID.Clear;
    cmbUID.Enabled := False;
    gbSaveUID := False;
    cmbUID.OnChange := nil;

    if not (DataUn.dmDataModule.dsMotors.State = dsInactive) then
    begin
      DataUn.dmDataModule.dsMotors.Filtered := False;
      DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
      DataUn.dmDataModule.dsMotors.Filtered := True;
      ComboUIDList.Sorted:=true;
      ComboUIDList.Duplicates:=dupIgnore;
      while not dmDataModule.dsMotors.Eof do
      begin
        if DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='66' then
        begin
          DeviceType:='IQ2';
        end
        else if DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='69' then
        begin
          DeviceType:='IQ2-DC';
        end
        else if DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='70' then
        begin
          DeviceType:='iQ3-DC';
        end
        else if DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='77' then
        begin
          DeviceType:='DC50';
        end
        else if DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='78' then
        begin
          DeviceType:='AC50';
        end
        else if DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='79' then
        begin
          DeviceType:='ST30';
        end
        else if DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='205' then
        begin
          DeviceType:='DC50 SW';
        end
        else if DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='206' then
        begin
          DeviceType:='AC50 SW';
        end
        else if DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='207' then
        begin
          DeviceType:='ST30 SW';
        end
        else if DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='74' then
        begin
          DeviceType:='MNI';
        end
        else if DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='65' then
        begin
          DeviceType:='IQMLC2';
        end;
        ComboBoxString:=IntToStr(DataUn.dmDataModule.dsMotors.FieldByName('UID').AsInteger)+' : '+DeviceType;
        ComboUIDList.Add(ComboBoxString);
        //DeviceType:=DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString;
        //       if(cmbUID.Items.IndexOf(ComboBoxString)=-1 )then
        //       begin
        //         cmbUID.Items.Add(IntToStr(DataUn.dmDataModule.dsMotors.FieldByName('UID').AsInteger)+' : '+DeviceType);
        //       end;
        DataUn.dmDataModule.dsMotors.Next;
      end;
      ComboUIDList.Sorted:=False;
      SortStringListUIDS(ComboUIDList);
      cmbUID.items.Assign(ComboUIDList);
      ChangeStyleList;
    end;

    //if VariablesUn.IsNumber(VariablesUn.gsUIDCrnt) then
       //cmbUID.Text := VariablesUn.gsUIDCrnt;

    cmbUID.OnChange := cmbUIDChange;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.ShowUID: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.teAutoReturnIQ2Change(Sender: TObject);
begin
  try
    if vbSkipAutoReturnUpdate=false then
    begin
      teAutoReturnIQ2.Color := $00A4FFFF;
      btnAutoReturnS.Visible := True;
      btnAutoReturnC.Visible := True;
      lblAutoReturnIQ2.Visible:=false;
    end;
    vbSkipAutoReturnUpdate:=false;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.teAutoReturnIQ2Change: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.teDNSpeedChange(Sender: TObject);
begin
  try
    teDNSpeed.Color := $00A4FFFF;
    teDNSpeedFine.Color := $00A4FFFF;
    btnDNRollerSpeedS.Visible := True;
    btnDNRollerSpeedC.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.teDNSpeedChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.teDNSpeedExit(Sender: TObject);
begin
  if teDNSpeed.Text<>'' then
  begin

    if StrToInt(teDNSpeed.Text)<DCSpeedLowerLimit then
    begin
      //ShowMessage('Value Cannot go below '+intToStr(DCSpeedLowerLimit)+'. Change to Appropriate value');
      teDNSpeed.Text:=IntTOStr(DCSpeedLowerLimit);
    end;
  end
  else
  begin
    teDNSpeed.Text:=IntTOStr(DCSpeedLowerLimit);
  end;
end;

procedure TConfigScreenFm.teDNSpeedFineChange(Sender: TObject);
begin
  try
    teDnSpeed.Color := $00A4FFFF;
    teDnSpeedFine.Color := $00A4FFFF;
    btnDnRollerSpeedS.Visible := True;
    btnDnRollerSpeedC.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.teDNSpeedChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.teDNSpeedFineExit(Sender: TObject);
begin
  if teDNSpeedFine.Text<>'' then
  begin
    if StrToInt(teDNSpeedFine.Text)<DCSFinespeedLowerLimit then
    begin
      //ShowMessage('Value Cannot go below '+intToStr(DCSpeedLowerLimit)+'. Change to Appropriate value');
      teDNSpeedFine.Text:=IntTOStr(DCSFinespeedLowerLimit);
    end;
  end
  else
  begin
    teDNSpeedFine.Text:=IntTOStr(DCSFinespeedLowerLimit);
  end;
end;

procedure TConfigScreenFm.teDNSpeedKeyPress(Sender: TObject; var Key: Char);
begin
    try
    if (Key in ['0'..'9']) then
    begin
      // Discard the key
      if sender=teDNSpeed then
      begin
        if (teDNSpeed.SelText.Length=2)or(teDNSpeed.SelText.Length=1) then
        begin
          teDNSpeed.text:='';
        end;
        if StrToInt(teDNSpeed.Text+key)>DCSpeedUpLimit then
        begin
          ShowMessage('Value Cannot Exceed '+IntToStr(DCSpeedUpLimit));
          Key:=#0;
        end;
//        else if StrToInt(teUpSpeed.Text+key)<DCSpeedLowerLimit then
//        begin
//          ShowMessage('Value Cannot go below '+intToStr(DCSpeedLowerLimit));
//          Key:=#0;
//        end;
      end
      else
      begin
        if (teDnSpeedFine.SelText.Length=2)or(teDnSpeedFine.SelText.Length=1) then
        begin
          teDnSpeedFine.text:='';
        end
        else if StrToInt(teDnSpeedFine.Text+key)>99 then
        begin
          ShowMessage('Value Cannot Exceed 99');
          Key:=#0;
        end;
      end;
    end;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.teUpSpeedKeyPress: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;


procedure TConfigScreenFm.teExternalThresholdChange(Sender: TObject);
begin
  try
    btnExternalThresholdS.Visible := True;
    btnExternalThresholdC.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.teExternalThresholdChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.teMasterUIDIQ2Change(Sender: TObject);
begin
  try
    if vbSkipAutoReturnUpdate=false then
    begin
      teMasterUidIQ2.Color := $00A4FFFF;
      btnSaveMasterUID.Visible := True;
      btnCancelMasterUID.Visible := True;
    end;
    vbSkipAutoReturnUpdate:=false;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.teMasterUIDIQ2Change: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;



procedure TConfigScreenFm.teMotionThresholdChange(Sender: TObject);
begin
  try
    btnMotionThresholdS.Visible := True;
    btnMotionThresholdC.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.teMotionThresholdChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.teRampPeriodDnChange(Sender: TObject);
begin
  try
    teRampPeriodDN.Color := $00A4FFFF;
    btnRampPeriodDnS.Visible := True;
    btnRampPeriodDnC.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.teRampPeriodDnChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.teRampPeriodDnExit(Sender: TObject);
begin
  if teRampPeriodDn.Text='' then
  begin
    teRampPeriodDn.Text:='0';
  end;
end;

procedure TConfigScreenFm.teRampPeriodDnKeyPress(Sender: TObject;
  var Key: Char);
begin
  try
    if (Key in ['0'..'9']) then
    begin
      if (teRampPeriodDN.SelText.Length=3) or (teRampPeriodDN.SelText.Length=2) or (teRampPeriodDN.SelText.Length=1) then
      begin
        teRampPeriodDN.text:='';
      end
      else if StrToInt(teRampPeriodDn.Text+key)>255 then
      begin
        ShowMessage('Value Cannot Exceed 255');
        Key:=#0;
      end;
    end;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.teRampPeriodDnKeyPress: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.teRampPeriodUPChange(Sender: TObject);
begin
  try
    teRampPeriodUP.Color := $00A4FFFF;
    btnRampPeriodUpS.Visible := True;
    btnRampPeriodUpC.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.teRampPeriodChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.teRampPeriodUPExit(Sender: TObject);
begin
  if teRampPeriodUP.Text='' then
  begin
    teRampPeriodUP.Text:='0';
  end;

end;

procedure TConfigScreenFm.teRampPeriodUPKeyPress(Sender: TObject; var Key: Char);
begin
  try
    if (Key in ['0'..'9']) then
    begin
      if (teRampPeriodUP.SelText.Length=3) or (teRampPeriodUP.SelText.Length=2) or (teRampPeriodUP.SelText.Length=1) then
      begin
        teRampPeriodUP.text:='';
      end
      else if StrToInt(teRampPeriodUP.Text+key)>255 then
      begin
        ShowMessage('Value Cannot Exceed 255');
        Key:=#0;
      end;
    end;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.teRampPeriodUpKeyPress: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.teUpSpeedChange(Sender: TObject);
begin
  try
    teUpSpeed.Color := $00A4FFFF;
    teUpSpeedFine.Color := $00A4FFFF;
    btnUpRollerSpeedS.Visible := True;
    btnUpRollerSpeedC.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.teUpSpeedChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.teUpSpeedExit(Sender: TObject);
begin
  if teUpSpeed.Text<>'' then
  begin

    if StrToInt(teUpSpeed.Text)<DCSpeedLowerLimit then
    begin
      //ShowMessage('Value Cannot go below '+intToStr(DCSpeedLowerLimit)+'. Change to Appropriate value');
      teUpSpeed.Text:=IntTOStr(DCSpeedLowerLimit);
    end;
  end
  else
  begin
    teUpSpeed.Text:=IntTOStr(DCSpeedLowerLimit);
  end;
end;

procedure TConfigScreenFm.teInternalThresholdChange(Sender: TObject);
begin
  try
    btnInternalThresholdS.Visible := True;
    btnInternalThresholdC.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.teInternalThresholdChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.teIQ2DCDnSpeedChange(Sender: TObject);
begin
  try
    teIQ2DCDnSpeed.Color := $00A4FFFF;
    teIQ2DCDnSpeed.Color := $00A4FFFF;
    btnIQ2DCDnRollerSpeedS.Visible := True;
    btnIQ2DCDnRollerSpeedC.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.teIQ2DCDnSpeedChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.teIQ2DCDnSpeedEnter(Sender: TObject);
begin
  mOldIQ2DCDnValue:=teIQ2DCDnSpeed.Text;
end;

procedure TConfigScreenFm.teIQ2DCDnSpeedExit(Sender: TObject);
begin
  if teIQ2DCDnSpeed.Text<>'' then
  begin
    if StrToInt(teIQ2DCDnSpeed.Text)<IQ2DCSpeedLimitLo then
    begin
      ShowMessage('Value cannot go below '+IntToStr(IQ2DCSpeedLimitLo)+'.0.');
      teIQ2DCDnSpeed.Text:=IntToStr(IQ2DCSpeedLimitLo);
    end
    else if StrToInt(teIQ2DCDnSpeed.Text)>IQ2DCSpeedLimitHi then
    begin
      ShowMessage('Value cannot above '+IntToStr(IQ2DCSpeedLimitHi)+'.0.');
      teIQ2DCDnSpeed.Text:=IntToStr(IQ2DCSpeedLimitHi);
      teIQ2DCDnSpeedFine.Text:='0';
    end
    else if StrToInt(teIQ2DCDnSpeed.Text)=IQ2DCSpeedLimitHi then
    begin
      teIQ2DCDnSpeedFine.Text:='0';
    end;
  end
  else
  begin
    ShowMessage('Value Cannot be blank.');
    teIQ2DCDnSpeed.Text:=mOldIQ2DCDnValue;
  end;
end;

procedure TConfigScreenFm.teIQ2DCDnSpeedFineChange(Sender: TObject);
begin
  try
    teIQ2DCDnSpeedFine.Color := $00A4FFFF;
    teIQ2DCDnSpeedFine.Color := $00A4FFFF;
    btnIQ2DCDnRollerSpeedS.Visible := True;
    btnIQ2DCDnRollerSpeedC.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.teIQ2DCDnSpeedFineChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.teIQ2DCDnSpeedFineEnter(Sender: TObject);
begin
  mOldIQ2DCDnFineValue:=teIQ2DCDnSpeedFine.Text;
end;

procedure TConfigScreenFm.teIQ2DCDnSpeedFineExit(Sender: TObject);
begin
  if teIQ2DCDnSpeedFine.Text<>'' then
  begin
    if (teIQ2DCDnSpeed.Text=IntToStr(IQ2DCSpeedLimitHi)) and (teIQ2DCDnSpeedFine.Text<>'0')then
    begin
      ShowMessage('Value cannot above '+IntToStr(IQ2DCSpeedLimitHi)+'.0.');
      teIQ2DCDnSpeedFine.Text:='0';
    end;
  end
  else
  begin
    ShowMessage('Value Cannot be blank.');
    teIQ2DCDnSpeedFine.Text:=mOldIQ2DCDnFineValue;
  end;
end;

procedure TConfigScreenFm.teIQ2DCRampDnTimeChange(Sender: TObject);
begin
  try
    teIQ2DCRampDnTime.Color := $00A4FFFF;
    teIQ2DCRampDnTime.Color := $00A4FFFF;
    btnIQ2DCRampPeriodDnS.Visible := True;
    btnIQ2DCRampPeriodDnC.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.teIQ2DCRampDnTimeChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.teIQ2DCRampDnTimeEnter(Sender: TObject);
begin
  mOldIQ2DCRampDnValue:=teIQ2DCRampDnTime.Text;
end;

procedure TConfigScreenFm.teIQ2DCRampDnTimeExit(Sender: TObject);
begin
  if teIQ2DCRampDnTime.Text<>'' then
  begin
    if (teIQ2DCRampDnTime.Text=LeftStr(VarToStr(IQ2DCRampLimitLoVar),1))
      and (StrToInt(teIQ2DCRampDnTimeFine.Text)<IQ2DCRampLimitLoDec)then
    begin
      ShowMessage('Value cannot go below '+LeftStr(VarToStr(IQ2DCRampLimitLoVar),3)+'.');
      teIQ2DCRampDnTimeFine.Text:=IntToStr(IQ2DCRampLimitLoDec);
    end
    else if StrToInt(teIQ2DCRampDnTime.Text)>IQ2DCRampLimitHi then
    begin
      ShowMessage('Value cannot above '+IntToStr(IQ2DCRampLimitHi)+'.0.');
      teIQ2DCRampDnTime.Text:='0';
      teIQ2DCRampDnTimeFine.Text:=IntToStr(IQ2DCRampLimitLoDec);
    end
    else if StrToInt(teIQ2DCRampDnTime.Text)=IQ2DCRampLimitHi then
    begin
      teIQ2DCRampDnTimeFine.Text:='0';
    end;
  end
  else
  begin
    ShowMessage('Value Cannot be blank.');
    teIQ2DCRampDnTime.Text:=mOldIQ2DCRampUpValue;
  end;
end;

procedure TConfigScreenFm.teIQ2DCRampDnTimeFineChange(Sender: TObject);
begin
  try
    teIQ2DCRampDnTimeFine.Color := $00A4FFFF;
    teIQ2DCRampDnTimeFine.Color := $00A4FFFF;
    btnIQ2DCRampPeriodDnS.Visible := True;
    btnIQ2DCRampPeriodDnC.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.teIQ2DCRampDnTimeFine: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.teIQ2DCRampDnTimeFineEnter(Sender: TObject);
begin
  mOldIQ2DCRampDnSpeedFineValue:=teIQ2DCRampDnTimeFine.Text;
end;

procedure TConfigScreenFm.teIQ2DCRampDnTimeFineExit(Sender: TObject);
begin
  if teIQ2DCRampDnTimeFine.Text<>'' then
  begin
    if (teIQ2DCRampDnTime.Text=IntToStr(IQ2DCRampLimitHi)) and (teIQ2DCRampDnTimeFine.Text<>'0')then
    begin
      ShowMessage('Value cannot above '+IntToStr(IQ2DCRampLimitHi)+'.0.');
      teIQ2DCRampDnTimeFine.Text:='0';
    end
    else if (teIQ2DCRampDnTime.Text='0') and (StrToInt(teIQ2DCRampDnTimeFine.Text)<2)then
    begin
      ShowMessage('Value cannot go below '+LeftStr(VarToStr(IQ2DCRampLimitLoVar),3)+'.');
      teIQ2DCRampDnTimeFine.Text:='2';
    end;
  end
  else
  begin
    ShowMessage('Value Cannot be blank.');
    teIQ2DCRampDnTimeFine.Text:=mOldIQ2DCRampUpSpeedFineValue;
  end;
end;

procedure TConfigScreenFm.teIQ2DCRampUpTimeChange(Sender: TObject);
begin
  try
    teIQ2DCRampUpTime.Color := $00A4FFFF;
    teIQ2DCRampUpTime.Color := $00A4FFFF;
    btnIQ2DCRampPeriodUpS.Visible := True;
    btnIQ2DCRampPeriodUpC.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.teIQ2DCRampUpTimeChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.teIQ2DCRampUpTimeEnter(Sender: TObject);
begin
  mOldIQ2DCRampUpValue:=teIQ2DCRampUpTime.Text;
end;

procedure TConfigScreenFm.teIQ2DCRampUpTimeExit(Sender: TObject);
begin
  if teIQ2DCRampUpTime.Text<>'' then
  begin
    if (teIQ2DCRampUpTime.Text=LeftStr(VarToStr(IQ2DCRampLimitLoVar),1))
      and (StrToInt(teIQ2DCRampUpTimeFine.Text)<IQ2DCRampLimitLoDec)then
    begin
      ShowMessage('Value cannot go below '+LeftStr(VarToStr(IQ2DCRampLimitLoVar),3)+'.');
      teIQ2DCRampUpTimeFine.Text:=IntToStr(IQ2DCRampLimitLoDec);
    end
    else if StrToInt(teIQ2DCRampUpTime.Text)>IQ2DCRampLimitHi then
    begin
      ShowMessage('Value cannot above '+IntToStr(IQ2DCRampLimitHi)+'.0.');
      teIQ2DCRampUpTime.Text:='0';
      teIQ2DCRampUpTimeFine.Text:=IntToStr(IQ2DCRampLimitLoDec);
    end
    else if StrToInt(teIQ2DCRampUpTime.Text)=IQ2DCRampLimitHi then
    begin
      teIQ2DCRampUpTimeFine.Text:='0';
    end;
  end
  else
  begin
    ShowMessage('Value Cannot be blank.');
    teIQ2DCRampUpTime.Text:=mOldIQ2DCRampUpValue;
  end;
end;

procedure TConfigScreenFm.teIQ2DCRampUpTimeFineChange(Sender: TObject);
begin
  try
    teIQ2DCRampUpTimeFine.Color := $00A4FFFF;
    teIQ2DCRampUpTimeFine.Color := $00A4FFFF;
    btnIQ2DCRampPeriodUpS.Visible := True;
    btnIQ2DCRampPeriodUpC.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.teIQ2DCRampUpTimeFineChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.teIQ2DCRampUpTimeFineEnter(Sender: TObject);
begin
  mOldIQ2DCRampUpSpeedFineValue:=teIQ2DCRampUpTimeFine.Text;
end;

procedure TConfigScreenFm.teIQ2DCRampUpTimeFineExit(Sender: TObject);
begin
  if teIQ2DCRampUpTimeFine.Text<>'' then
  begin
    if (teIQ2DCRampUpTime.Text=IntToStr(IQ2DCRampLimitHi)) and (teIQ2DCRampUpTimeFine.Text<>'0')then
    begin
      ShowMessage('Value cannot above '+IntToStr(IQ2DCRampLimitHi)+'.0.');
      teIQ2DCRampUpTimeFine.Text:='0';
    end
    else if (teIQ2DCRampUpTime.Text='0') and (StrToInt(teIQ2DCRampUpTimeFine.Text)<2)then
    begin
      ShowMessage('Value cannot go below '+LeftStr(VarToStr(IQ2DCRampLimitLoVar),3)+'.');
      teIQ2DCRampUpTimeFine.Text:='2';
    end;
  end
  else
  begin
    ShowMessage('Value Cannot be blank.');
    teIQ2DCRampUpTimeFine.Text:=mOldIQ2DCRampUpSpeedFineValue;
  end;
end;

procedure TConfigScreenFm.teIQ2DCSlowDnSpeedChange(Sender: TObject);
begin
  try
    teIQ2DCSlowDnSpeed.Color := $00A4FFFF;
    teIQ2DCSlowDnSpeed.Color := $00A4FFFF;
    btnIQ2DCSlowDnSpeedS.Visible := True;
    btnIQ2DCSlowDnSpeedC.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.teIQ2DCSlowDnSpeedChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.teIQ2DCSlowDnSpeedEnter(Sender: TObject);
begin
  mOldIQ2DCSlowDnValue:=teIQ2DCSlowDnSpeed.Text;
end;

procedure TConfigScreenFm.teIQ2DCSlowDnSpeedExit(Sender: TObject);
begin
  if teIQ2DCSlowDnSpeed.Text<>'' then
  begin
    if StrToInt(teIQ2DCSlowDnSpeed.Text)<IQ2DCSlowSpeedLimitLo then
    begin
      ShowMessage('Value cannot go below '+IntToStr(IQ2DCSlowSpeedLimitLo)+'.0.');
      teIQ2DCSlowDnSpeed.Text:=IntToStr(IQ2DCSlowSpeedLimitLo);
    end
    else if StrToInt(teIQ2DCSlowDnSpeed.Text)>IQ2DCSlowSpeedLimitHi then
    begin
      ShowMessage('Value cannot above '+IntToStr(IQ2DCSlowSpeedLimitHi)+'.0.');
      teIQ2DCSlowDnSpeed.Text:=IntToStr(IQ2DCSlowSpeedLimitHi);
      teIQ2DCSlowDnSpeedFine.Text:='0';
    end
    else if StrToInt(teIQ2DCUpSpeed.Text)=IQ2DCSlowSpeedLimitHi then
    begin
      teIQ2DCSlowDnSpeedFine.Text:='0';
    end;
  end
  else
  begin
    ShowMessage('Value Cannot be blank.');
    teIQ2DCSlowDnSpeed.Text:=mOldIQ2DCSlowDnValue;
  end;
end;

procedure TConfigScreenFm.teIQ2DCSlowDnSpeedFineChange(Sender: TObject);
begin
  try
    teIQ2DCSlowDnSpeedFine.Color := $00A4FFFF;
    teIQ2DCSlowDnSpeedFine.Color := $00A4FFFF;
    btnIQ2DCSlowDnSpeedS.Visible := True;
    btnIQ2DCSlowDnSpeedC.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.teIQ2DCSlowDnSpeedFineChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.teIQ2DCSlowDnSpeedFineEnter(Sender: TObject);
begin
  mOldIQ2DCSlowDnSpeedFineValue:=teIQ2DCSlowDnSpeedFine.Text;
end;

procedure TConfigScreenFm.teIQ2DCSlowDnSpeedFineExit(Sender: TObject);
begin
  if teIQ2DCSlowDnSpeedFine.Text<>'' then
  begin
    if (teIQ2DCSlowDnSpeed.Text=IntToStr(IQ2DCSlowSpeedLimitHi)) and (teIQ2DCSlowDnSpeedFine.Text<>'0')then
    begin
      ShowMessage('Value cannot above '+IntToStr(IQ2DCSlowSpeedLimitHi)+'.0.');
      teIQ2DCSlowDnSpeedFine.Text:='0';
    end;
  end
  else
  begin
    ShowMessage('Value Cannot be blank.');
    teIQ2DCSlowDnSpeedFine.Text:=mOldIQ2DCSlowDnSpeedFineValue;
  end;
end;

procedure TConfigScreenFm.teIQ2DCSlowUpSpeedChange(Sender: TObject);
begin
  try
    teIQ2DCSlowUpSpeed.Color := $00A4FFFF;
    teIQ2DCSlowUpSpeed.Color := $00A4FFFF;
    btnIQ2DCSlowUpSpeedS.Visible := True;
    btnIQ2DCSlowUpSpeedC.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.teIQ2DCSlowUpSpeedChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.teIQ2DCSlowUpSpeedEnter(Sender: TObject);
begin
  mOldIQ2DCSlowUpValue:=teIQ2DCSlowUpSpeed.Text;
end;

procedure TConfigScreenFm.teIQ2DCSlowUpSpeedExit(Sender: TObject);
begin
  if teIQ2DCSlowUpSpeed.Text<>'' then
  begin
    if StrToInt(teIQ2DCSlowUpSpeed.Text)<IQ2DCSlowSpeedLimitLo then
    begin
      ShowMessage('Value cannot go below '+IntToStr(IQ2DCSlowSpeedLimitLo)+'.0.');
      teIQ2DCSlowUpSpeed.Text:=IntToStr(IQ2DCSlowSpeedLimitLo);
    end
    else if StrToInt(teIQ2DCSlowUpSpeed.Text)>IQ2DCSlowSpeedLimitHi then
    begin
      ShowMessage('Value cannot above '+IntToStr(IQ2DCSlowSpeedLimitHi)+'.0.');
      teIQ2DCSlowUpSpeed.Text:=IntToStr(IQ2DCSlowSpeedLimitHi);
      teIQ2DCSlowUpSpeedFine.Text:='0';
    end
    else if StrToInt(teIQ2DCUpSpeed.Text)=IQ2DCSlowSpeedLimitHi then
    begin
      teIQ2DCSlowUpSpeedFine.Text:='0';
    end;
  end
  else
  begin
    ShowMessage('Value Cannot be blank.');
    teIQ2DCSlowUpSpeed.Text:=mOldIQ2DCSlowUpValue;
  end;
end;

procedure TConfigScreenFm.teIQ2DCSlowUpSpeedFineChange(Sender: TObject);
begin
  try
    teIQ2DCSlowUpSpeedFine.Color := $00A4FFFF;
    teIQ2DCSlowUpSpeedFine.Color := $00A4FFFF;
    btnIQ2DCSlowUpSpeedS.Visible := True;
    btnIQ2DCSlowUpSpeedC.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.teIQ2DCSlowUpSpeedFineChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.teIQ2DCSlowUpSpeedFineEnter(Sender: TObject);
begin
  mOldIQ2DCSlowUpSpeedFineValue:=teIQ2DCSlowUpSpeedFine.Text;
end;

procedure TConfigScreenFm.teIQ2DCSlowUpSpeedFineExit(Sender: TObject);
begin
  if teIQ2DCSlowUpSpeedFine.Text<>'' then
  begin
    if (teIQ2DCSlowUpSpeed.Text=IntToStr(IQ2DCSlowSpeedLimitHi)) and (teIQ2DCSlowUpSpeedFine.Text<>'0')then
    begin
      ShowMessage('Value cannot above '+IntToStr(IQ2DCSlowSpeedLimitHi)+'.0.');
      teIQ2DCSlowUpSpeedFine.Text:='0';
    end;
  end
  else
  begin
    ShowMessage('Value Cannot be blank.');
    teIQ2DCSlowUpSpeedFine.Text:=mOldIQ2DCSlowUpSpeedFineValue;
  end;
end;

procedure TConfigScreenFm.teIQ2DCUpSpeedChange(Sender: TObject);
begin
  try
    teIQ2DCUpSpeed.Color := $00A4FFFF;
    teIQ2DCUpSpeed.Color := $00A4FFFF;
    btnIQ2DCUpRollerSpeedS.Visible := True;
    btnIQ2DCUpRollerSpeedC.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.teIQ2DCUpSpeedChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.teIQ2DCUpSpeedEnter(Sender: TObject);
begin
  mOldIQ2DCUpValue:=teIQ2DCUpSpeed.Text;
end;

procedure TConfigScreenFm.teIQ2DCUpSpeedExit(Sender: TObject);
begin
  if teIQ2DCUpSpeed.Text<>'' then
  begin
    if StrToInt(teIQ2DCUpSpeed.Text)<IQ2DCSpeedLimitLo then
    begin
      ShowMessage('Value cannot go below '+IntToStr(IQ2DCSpeedLimitLo)+'.0.');
      teIQ2DCUpSpeed.Text:=IntToStr(IQ2DCSpeedLimitLo);
    end
    else if StrToInt(teIQ2DCUpSpeed.Text)>IQ2DCSpeedLimitHi then
    begin
      ShowMessage('Value cannot above '+IntToStr(IQ2DCSpeedLimitHi)+'.0.');
      teIQ2DCUpSpeed.Text:=IntToStr(IQ2DCSpeedLimitHi);
      teIQ2DCUpSpeedFine.Text:='0';
    end
    else if StrToInt(teIQ2DCUpSpeed.Text)=IQ2DCSpeedLimitHi then
    begin
      teIQ2DCUpSpeedFine.Text:='0';
    end;
  end
  else
  begin
    ShowMessage('Value Cannot be blank.');
    teIQ2DCUpSpeed.Text:=mOldIQ2DCUpValue;
  end;
end;


procedure TConfigScreenFm.teIQ2DCUpSpeedFineChange(Sender: TObject);
begin
  try
    teIQ2DCUpSpeedFine.Color := $00A4FFFF;
    teIQ2DCUpSpeedFine.Color := $00A4FFFF;
    btnIQ2DCUpRollerSpeedS.Visible := True;
    btnIQ2DCUpRollerSpeedC.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.teIQ2DCUpSpeedFineChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.teIQ2DCUpSpeedFineEnter(Sender: TObject);
begin
  mOldIQ2DCUpFineValue:=teIQ2DCUpSpeedFine.Text;
end;

procedure TConfigScreenFm.teIQ2DCUpSpeedFineExit(Sender: TObject);
begin
  if teIQ2DCUpSpeedFine.Text<>'' then
  begin
    if (teIQ2DCUpSpeed.Text=IntToStr(IQ2DCSpeedLimitHi)) and (teIQ2DCUpSpeedFine.Text<>'0')then
    begin
      ShowMessage('Value cannot above '+IntToStr(IQ2DCSpeedLimitHi)+'.0.');
      teIQ2DCUpSpeedFine.Text:='0';
    end;
  end
  else
  begin
    ShowMessage('Value Cannot be blank.');
    teIQ2DCUpSpeedFine.Text:=mOldIQ2DCUpFineValue;
  end;

end;


procedure TConfigScreenFm.teIQ2DCSpeedKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (Length(TEdit(Sender).Text)=0) and (Key='0') then
  begin
    Key:=#0;
  end
  else if (TEdit(Sender).SelStart=0) and (Key='0') then
  begin
    Key:=#0;
  end;
end;

procedure TConfigScreenFm.teUpSpeedFineExit(Sender: TObject);
begin
  if teUpSpeedFine.Text<>'' then
  begin
    if StrToInt(teUpSpeedFine.Text)<DCSFinespeedLowerLimit then
    begin
      //ShowMessage('Value Cannot go below '+intToStr(DCSpeedLowerLimit)+'. Change to Appropriate value');
      teUpSpeedFine.Text:=IntTOStr(DCSFinespeedLowerLimit);
    end;
  end
  else
  begin
    teUpSpeedFine.Text:=IntTOStr(DCSFinespeedLowerLimit);
  end;
end;

procedure TConfigScreenFm.teUpSpeedKeyPress(Sender: TObject; var Key: Char);
begin
  try
    if (Key in ['0'..'9']) then
    begin
      // Discard the key
      if sender=teUpSpeed then
      begin
        if (teUpSpeed.SelText.Length=2)or(teUpSpeed.SelText.Length=1) then
        begin
          teUpSpeed.text:='';
        end;
        if StrToInt(teUpSpeed.Text+key)>DCSpeedUpLimit then
        begin
          ShowMessage('Value Cannot Exceed '+IntToStr(DCSpeedUpLimit));
          Key:=#0;
        end;
//        else if StrToInt(teUpSpeed.Text+key)<DCSpeedLowerLimit then
//        begin
//          ShowMessage('Value Cannot go below '+intToStr(DCSpeedLowerLimit));
//          Key:=#0;
//        end;
      end
      else
      begin
        if (teUpSpeedFine.SelText.Length=2)or(teUpSpeedFine.SelText.Length=1) then
        begin
          teUpSpeedFine.text:='';
        end
        else if StrToInt(teUpSpeedFine.Text+key)>99 then
        begin
          ShowMessage('Value Cannot Exceed 99');
          Key:=#0;
        end;
      end;
    end;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.teUpSpeedKeyPress: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.teSlowSpeedChange(Sender: TObject);
begin
  try
    teSlowSpeed.Color := $00A4FFFF;
    teSlowSpeedFine.Color := $00A4FFFF;
    btnSlowSpeedS.Visible := True;
    btnSlowSpeedC.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.teSlowSpeedChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.teSlowSpeedExit(Sender: TObject);
begin
  if teSlowSpeed.Text<>'' then
  begin
    if StrToInt(teSlowSpeed.Text)<DCSpeedLowerLimit then
    begin
      //ShowMessage('Value Cannot go below '+intToStr(DCSpeedLowerLimit)+'. Change to Appropriate value');
      teSlowSpeed.Text:=IntTOStr(DCSpeedLowerLimit);
    end;
  end
  else
  begin
    teSlowSpeed.Text:=IntTOStr(DCSpeedLowerLimit);
  end;
end;

procedure TConfigScreenFm.teSlowSpeedFineChange(Sender: TObject);
begin
    try
    teSlowSpeed.Color := $00A4FFFF;
    teSlowSpeedFine.Color := $00A4FFFF;
    btnSlowSpeedS.Visible := True;
    btnSlowSpeedC.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.teSlowSpeedChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.teSlowSpeedFineExit(Sender: TObject);
begin
  if teSlowSpeedFine.Text<>'' then
  begin
    if StrToInt(teSlowSpeedFine.Text)<DCSFinespeedLowerLimit then
    begin
      //ShowMessage('Value Cannot go below '+intToStr(DCSpeedLowerLimit)+'. Change to Appropriate value');
      teSlowSpeedFine.Text:=IntTOStr(DCSFinespeedLowerLimit);
    end;
  end
  else
  begin
    teSlowSpeedFine.Text:=IntTOStr(DCSFinespeedLowerLimit);
  end;
end;

procedure TConfigScreenFm.teSlowSpeedKeyPress(Sender: TObject; var Key: Char);
begin
  try
    if (Key in ['0'..'9']) then
    begin
      // Discard the key
      if sender = teSlowSpeed then
      begin
        if (teSlowSpeed.SelText.Length=2) or (teSlowSpeed.SelText.Length=1)then
        begin
          teSlowSpeed.text:='';
        end
        else if StrToInt(teSlowSpeed.Text+key)>DCSpeedUpLimit then
        begin
          ShowMessage('Value Cannot Exceed '+IntToStr(DCSpeedUpLimit));
          Key:=#0;

        end;
      end
      else
      begin
        if (teSlowSpeedFine.SelText.Length=2)or(teSlowSpeedFine.SelText.Length=1) then
        begin
          teSlowSpeedFine.text:='';
        end
        else if StrToInt(teSlowSpeedFine.Text+key)>99 then
        begin
          ShowMessage('Value Cannot Exceed 99');
          Key:=#0;
        end;
      end;
    end;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.teSlowSpeedKeyPress: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.txtA1Change(Sender: TObject);
begin
  try
    txtA1.Color := $00A4FFFF;
    gbSaveZGN := True;
    btnA1.Visible := False;
    btnCancel1.Visible := True;
    btnSave.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.txtA1Change: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.txtA1Click(Sender: TObject);
begin
  try
    if txtA1.Color = clWhite then
      txtA1.SelectAll;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.txtA1Click: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.txtA2Change(Sender: TObject);
begin
  try
    txtA2.Color := $00A4FFFF;
    gbSaveZGN := True;
    btnA2.Visible := False;
    btnCancel2.Visible := True;
    btnSave.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.txtA2Change: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.txtA2Click(Sender: TObject);
begin
  try
    if txtA2.Color = clWhite then
      txtA2.SelectAll;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.txtA2Click: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.txtA3Change(Sender: TObject);
begin
  try
    txtA3.Color := $00A4FFFF;
    gbSaveZGN := True;
    btnA3.Visible := False;
    btnCancel3.Visible := True;
    btnSave.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.txtA3Change: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.txtA3Click(Sender: TObject);
begin
  try
    if txtA3.Color = clWhite then
      txtA3.SelectAll;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.txtA3Click: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.txtA4Change(Sender: TObject);
begin
  try
    txtA4.Color := $00A4FFFF;
    gbSaveZGN := True;
    btnA4.Visible := False;
    btnCancel4.Visible := True;
    btnSave.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.txtA4Change: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.txtA4Click(Sender: TObject);
begin
  try
    if txtA4.Color = clWhite then
      txtA4.SelectAll;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.txtA4Click: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.txtA5Change(Sender: TObject);
begin
  try
    txtA5.Color := $00A4FFFF;
    gbSaveZGN := True;
    btnA5.Visible := False;
    btnCancel5.Visible := True;
    btnSave.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.txtA5Change: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.txtA5Click(Sender: TObject);
begin
  try
    if txtA5.Color = clWhite then
      txtA5.SelectAll;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.txtA5Click: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.txtA6Change(Sender: TObject);
begin
  try
    txtA6.Color := $00A4FFFF;
    gbSaveZGN := True;
    btnA6.Visible := False;
    btnCancel6.Visible := True;
    btnSave.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.txtA6Change: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.txtA6Click(Sender: TObject);
begin
  try
    if txtA6.Color = clWhite then
      txtA6.SelectAll;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.txtA6Click: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.txtA7Change(Sender: TObject);
begin
  try
    txtA7.Color := $00A4FFFF;
    gbSaveZGN := True;
    btnA7.Visible := False;
    btnCancel7.Visible := True;
    btnSave.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.txtA7Change: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.txtA7Click(Sender: TObject);
begin
  try
    if txtA7.Color = clWhite then
      txtA7.SelectAll;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.txtA7Click: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.txtA8Change(Sender: TObject);
begin
  try
    txtA8.Color := $00A4FFFF;
    gbSaveZGN := True;
    btnA8.Visible := False;
    btnCancel8.Visible := True;
    btnSave.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.txtA8Change: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.txtA8Click(Sender: TObject);
begin
  try
    if txtA8.Color = clWhite then
      txtA8.SelectAll;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.txtA8Click: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.txtBOzgnChange(Sender: TObject);
begin
  try
    //txtBOzgn.Color := $00A4FFFF;
    gbSaveLV := True;
    //btnBO.Visible := False;
    btnCancelBO.Visible := True;
    btnSaveBO.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.txtBOzgnChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.txtLVzgnChange(Sender: TObject);
begin
  try
    txtLVzgn.Color := $00A4FFFF;
    gbSaveZGN := True;
    btnLV.Visible := False;
    btnCancelLV.Visible := True;
    btnSave.Visible := True;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.txtLVzgnChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.txtLVzgnClick(Sender: TObject);
begin
  try
    if txtLVzgn.Color = clWhite then
      txtLVzgn.SelectAll;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.txtLVzgnClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnA1Click(Sender: TObject);
begin
  try
    try
      rhDisable;
      if  btnA1.Caption = '>' then
      begin
        gsA1 := txtA1.Text;
        btnA1.Caption := '<';
        txtA1.ReadOnly := False;
        if txtA1.Visible then
          txtA1.SetFocus;

        txtA1.Color := $00BBEECC;

      end
      else
      begin
        txtA1.ClearSelection;
        txtA1.Text := gsA1;
        btnA1.Caption := '>';
        txtA1.ReadOnly := True;
        txtA1.Color := clWhite;

        if not(txtA2.Color = $00A4FFFF) and not (txtA3.Color = $00A4FFFF) and not (txtA4.Color = $00A4FFFF) and not (txtLVzgn.Color = $00A4FFFF) and not(txtA5.Color = $00A4FFFF) and not(txtA6.Color = $00A4FFFF) and not (txtA7.Color = $00A4FFFF) and not (txtA8.Color = $00A4FFFF) then
        begin
          gbSaveZGN := False;
          btnSave.Visible := False;
        end;
      end;
    finally
      rhEnable;
    end;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.btnA1Click: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnA2Click(Sender: TObject);
begin
  try
    try
      rhDisable;
      if  btnA2.Caption = '>' then
      begin
        gsA2 := txtA2.Text;
        btnA2.Caption := '<';
        txtA2.ReadOnly := False;
        if txtA2.Visible then
          txtA2.SetFocus;

        txtA2.Color := $00BBEECC;
      end
      else
      begin
        txtA2.ClearSelection;
        txtA2.Text := gsA2;
        btnA2.Caption := '>';
        txtA2.ReadOnly := True;
        txtA2.Color := clWhite;

        if not(txtA1.Color = $00A4FFFF) and not (txtA3.Color = $00A4FFFF) and not (txtA4.Color = $00A4FFFF) and not (txtLVzgn.Color = $00A4FFFF) and not(txtA5.Color = $00A4FFFF) and not(txtA6.Color = $00A4FFFF) and not (txtA7.Color = $00A4FFFF) and not (txtA8.Color = $00A4FFFF) then
        begin
          gbSaveZGN := False;
          btnSave.Visible := False;
        end;
      end;
    finally
      rhEnable;
    end;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.btnA2Click: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnA3Click(Sender: TObject);
begin
  try
    try
      rhDisable;
      if  btnA3.Caption = '>' then
      begin
        gsA3 := txtA3.Text;
        btnA3.Caption := '<';
        txtA3.ReadOnly := False;
        if txtA3.Visible then
          txtA3.SetFocus;

        txtA3.Color := $00BBEECC;
      end
      else
      begin
        txtA3.ClearSelection;
        txtA3.Text := gsA3;
        btnA3.Caption := '>';
        txtA3.ReadOnly := True;
        txtA3.Color := clWhite;

        if not(txtA2.Color = $00A4FFFF) and not (txtA1.Color = $00A4FFFF) and not (txtA4.Color = $00A4FFFF) and not (txtLVzgn.Color = $00A4FFFF) and not(txtA5.Color = $00A4FFFF) and not(txtA6.Color = $00A4FFFF) and not (txtA7.Color = $00A4FFFF) and not (txtA8.Color = $00A4FFFF) then
        begin
          gbSaveZGN := False;
          btnSave.Visible := False;
        end;
      end;
    finally
      rhEnable;
    end;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.btnA3Click: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnA4Click(Sender: TObject);
begin
  try
    try
      rhDisable;
      if  btnA4.Caption = '>' then
      begin
        gsA4 := txtA4.Text;
        btnA4.Caption := '<';
        txtA4.ReadOnly := False;
        if txtA4.Visible then
          txtA4.SetFocus;

        txtA4.Color := $00BBEECC;


      end
      else
      begin
        txtA4.ClearSelection;
        txtA4.Text := gsA4;
        btnA4.Caption := '>';
        txtA4.ReadOnly := True;
        txtA4.Color := clWhite;

        if not(txtA2.Color = $00A4FFFF) and not (txtA3.Color = $00A4FFFF) and not (txtA1.Color = $00A4FFFF) and not (txtLVzgn.Color = $00A4FFFF) and not(txtA5.Color = $00A4FFFF) and not(txtA6.Color = $00A4FFFF) and not (txtA7.Color = $00A4FFFF) and not (txtA8.Color = $00A4FFFF) then
        begin
          gbSaveZGN := False;
          btnSave.Visible := False;
        end;
      end;
    finally
      rhEnable;
    end;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.btnA4Click: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnA5Click(Sender: TObject);
begin
  try
    try
      rhDisable;
      if  btnA5.Caption = '>' then
      begin
        gsA5 := txtA5.Text;
        btnA5.Caption := '<';
        txtA5.ReadOnly := False;
        if txtA5.Visible then
          txtA5.SetFocus;

        txtA5.Color := $00BBEECC;


      end
      else
      begin
        txtA5.ClearSelection;
        txtA5.Text := gsA5;
        btnA5.Caption := '>';
        txtA5.ReadOnly := True;
        txtA5.Color := clWhite;

        if not(txtA1.Color = $00A4FFFF) and not(txtA2.Color = $00A4FFFF) and not (txtA3.Color = $00A4FFFF) and not (txtA4.Color = $00A4FFFF) and not (txtLVzgn.Color = $00A4FFFF) and not(txtA6.Color = $00A4FFFF) and not (txtA7.Color = $00A4FFFF) and not (txtA8.Color = $00A4FFFF) then
        begin
          gbSaveZGN := False;
          btnSave.Visible := False;
        end;
      end;
    finally
      rhEnable;
    end;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.btnA5Click: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnA6Click(Sender: TObject);
begin
  try
    try
      rhDisable;
      if  btnA6.Caption = '>' then
      begin
        gsA6 := txtA6.Text;
        btnA6.Caption := '<';
        txtA6.ReadOnly := False;
        if txtA6.Visible then
          txtA6.SetFocus;

        txtA6.Color := $00BBEECC;
      end
      else
      begin
        txtA6.ClearSelection;
        txtA6.Text := gsA6;
        btnA6.Caption := '>';
        txtA6.ReadOnly := True;
        txtA6.Color := clWhite;

        if not(txtA1.Color = $00A4FFFF) and not(txtA2.Color = $00A4FFFF) and not (txtA3.Color = $00A4FFFF) and not (txtA4.Color = $00A4FFFF) and not (txtLVzgn.Color = $00A4FFFF) and not(txtA5.Color = $00A4FFFF) and not (txtA7.Color = $00A4FFFF) and not (txtA8.Color = $00A4FFFF) then
        begin
          gbSaveZGN := False;
          btnSave.Visible := False;
        end;
      end;
    finally
      rhEnable;
    end;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.btnA6Click: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnA7Click(Sender: TObject);
begin
  try
    try
      rhDisable;
      if  btnA7.Caption = '>' then
      begin
        gsA7 := txtA7.Text;
        btnA7.Caption := '<';
        txtA7.ReadOnly := False;
        if txtA7.Visible then
          txtA7.SetFocus;

        txtA7.Color := $00BBEECC;


      end
      else
      begin
        txtA7.ClearSelection;
        txtA7.Text := gsA7;
        btnA7.Caption := '>';
        txtA7.ReadOnly := True;
        txtA7.Color := clWhite;
        if not(txtA1.Color = $00A4FFFF) and not(txtA2.Color = $00A4FFFF) and not (txtA3.Color = $00A4FFFF) and not (txtA4.Color = $00A4FFFF) and not (txtLVzgn.Color = $00A4FFFF) and not(txtA6.Color = $00A4FFFF) and not (txtA5.Color = $00A4FFFF) and not (txtA8.Color = $00A4FFFF) then
        begin
          gbSaveZGN := False;
          btnSave.Visible := False;
        end;
      end;
    finally
      rhEnable;
    end;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.btnA7Click: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnA8Click(Sender: TObject);
begin
  try
    try
      rhDisable;
      if  btnA8.Caption = '>' then
      begin
        gsA8 := txtA8.Text;
        btnA8.Caption := '<';
        txtA8.ReadOnly := False;
        if txtA8.Visible then
          txtA8.SetFocus;

        txtA8.Color := $00BBEECC;


      end
      else
      begin
        txtA8.ClearSelection;
        txtA8.Text := gsA8;
        btnA8.Caption := '>';
        txtA8.ReadOnly := True;
        txtA8.Color := clWhite;

        if not(txtA1.Color = $00A4FFFF) and not(txtA2.Color = $00A4FFFF) and not (txtA3.Color = $00A4FFFF) and not (txtA4.Color = $00A4FFFF) and not (txtLVzgn.Color = $00A4FFFF) and not(txtA6.Color = $00A4FFFF) and not (txtA7.Color = $00A4FFFF) and not (txtA5.Color = $00A4FFFF) then
        begin
          gbSaveZGN := False;
          btnSave.Visible := False;
        end;
      end;
    finally
      rhEnable;
    end;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.btnA8Click: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnAliasCClick(Sender: TObject);
begin
  btnGetStatusClick(nil);
  btnAliasC.Visible := False;
  btnAliasS.Visible := False;
end;

procedure TConfigScreenFm.btnBOcClick(Sender: TObject);
begin
  try
    btnGetStatusClick(nil);
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.btnBOcClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

//procedure TConfigScreenFm.btnBOClick(Sender: TObject);
//begin
//  try
//    try
//      rhDisable;
//      if  btnBO.Caption = '>' then
//      begin
//        gsBO := txtBOzgn.Text;
//        btnBO.Caption := '<';
//        txtBOzgn.ReadOnly := False;
//        if txtBOzgn.Visible then
//          txtBOzgn.SetFocus;
//
//        txtBOzgn.Color := $00BBEECC;
//
//
//      end
//      else
//      begin
//        txtBOzgn.ClearSelection;
//        txtBOzgn.Text := gsBO;
//        btnBO.Caption := '>';
//        txtBOzgn.ReadOnly := True;
//        txtBOzgn.Color := clWhite;
//        gbSaveLV := False;
//        btnSaveBO.Visible := False;
//      end;
//    finally
//      rhEnable;
//    end;
//  except
//    on E: Exception do
//    begin
//      cmbUID.OnChange := cmbUIDChange;
//      MessageDlg('TConfigScreenFm.btnBOClick: ' + E.Message, mtWarning, [mbOk], 0);
//    end;
//  end;
//end;

procedure TConfigScreenFm.btnBOModeSClick(Sender: TObject);
var vsCommand:String;
begin
  try
    if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
    begin
      giUID :=  StrToInt(GetUID(cmbUID.text));
      if isMNI then
      begin
        DataUn.dmDataModule.dsMotors.Filtered := False;
        DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = '+cmbMotorSelector.Text;
        DataUn.dmDataModule.dsMotors.Filtered := True;
        if cmbBOMode.ItemIndex=0 then
        begin
          vsCommand:=VariablesUn.UID_BLCKOUT+'.0.'+DataUn.dmDataModule.dsMotors.FieldByName('BOType').AsString+'.'+IntToStr(VariablesUn.Clear_a_Bit(MotorBOModeBits,StrToInt(cmbMotorSelector.text)-1));
        end
        else
        begin
          vsCommand:=VariablesUn.UID_BLCKOUT+'.0.'+DataUn.dmDataModule.dsMotors.FieldByName('BOType').AsString+'.'+IntToStr(VariablesUn.Set_a_Bit(MotorBOModeBits,StrToInt(cmbMotorSelector.text)-1));
        end;
        ComFm.SetGenericParam(giUID,vsCommand, nil);
      end
      else
      begin
        DataUn.dmDataModule.dsMotors.Filtered := False;
        DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text);
        DataUn.dmDataModule.dsMotors.Filtered := True;
        giUID :=  StrToInt(GetUID(cmbUID.text));
        ComFm.SetMotorStatus(giUID, VariablesUn.UID_BLCKOUT, 0, StrToInt(DataUn.dmDataModule.dsMotors.FieldByName('BOType').AsString), cmbBOMode.ItemIndex, nil);
      end;
      Delay(200);
      btnGetStatusClick(nil);
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnBOModeSClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnBOsClick(Sender: TObject);
var vsCommand:String;
begin
  try
    if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
    begin
      giUID :=  StrToInt(GetUID(cmbUID.text));
      if isMNI then
      begin
        DataUn.dmDataModule.dsMotors.Filtered := False;
        DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +  ' and MotorNumber = '+cmbMotorSelector.Text;
        DataUn.dmDataModule.dsMotors.Filtered := True;
        if cmbBOType.ItemIndex=0 then
        begin
          vsCommand:=VariablesUn.UID_BLCKOUT+'.0.'+IntToStr(VariablesUn.Clear_a_Bit(MotorBOTypeBits,StrToInt(cmbMotorSelector.text)-1))+'.'+IntToStr(DataUn.dmDataModule.dsMotors.FieldByName('BlackoutMode').AsInteger);
        end
        else
        begin
          vsCommand:=VariablesUn.UID_BLCKOUT+'.0.'+IntToStr(VariablesUn.Set_a_Bit(MotorBOTypeBits,StrToInt(cmbMotorSelector.text)-1))+'.'+IntToStr(DataUn.dmDataModule.dsMotors.FieldByName('BlackoutMode').AsInteger);
        end;
        ComFm.SetGenericParam(giUID,vsCommand, nil);
      end
      else
      begin
        DataUn.dmDataModule.dsMotors.Filtered := False;
        DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text);
        DataUn.dmDataModule.dsMotors.Filtered := True;
        ComFm.SetMotorStatus(giUID, VariablesUn.UID_BLCKOUT, 0, cmbBOtype.ItemIndex, DataUn.dmDataModule.dsMotors.FieldByName('BlackoutMode').AsInteger, nil);
      end;
      Delay(200);
      btnGetStatusClick(nil);
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnBOsClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.UIDStop;
var vsHi, vsLo, vsCommand: String;
begin
  try
    if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
    begin
      vsHi := IntToStr(Hi(StrToInt(GetUID(cmbUID.text))));
      vsLo := IntToStr(Lo(StrToInt(GetUID(cmbUID.text))));
      if (ismni)or(IsIQMLC2) then
      begin
        if CmbMotorSelector.Text = '1' then
        begin
          vsCommand := '>' + vsHi + '.' + vsLo + '.' + UID_STOP + '.0.0.1';
        end
        else if CmbMotorSelector.Text = '2' then
        begin
          vsCommand := '>' + vsHi + '.' + vsLo + '.' + UID_STOP + '.0.0.2';
        end
        else if CmbMotorSelector.Text = '3' then
        begin
          vsCommand := '>' + vsHi + '.' + vsLo + '.' + UID_STOP + '.0.0.4';
        end
        else if CmbMotorSelector.Text = '4' then
        begin
          vsCommand := '>' + vsHi + '.' + vsLo + '.' + UID_STOP + '.0.0.8';
        end;
      end
      else
      begin
        vsCommand := '>' + vsHi + '.' + vsLo + '.' + UID_STOP + '.0.0.0';
      end;
      ComFm.SendCommand(vsCommand, nil);
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.UIDStop: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.UIDWink(viWinkCount: Integer);
var i: Integer;
begin
  try
    try
      Screen.Cursor := crHourGlass;
      for i := 1 to viWinkCount do
       begin
         UIDMoveUp;
         //Delay(1000);
         Delay(1500);
         UIDStop;
         Delay(500);
         UIDMoveDwn;
         Delay(1500);
         //Delay(1150);
         UIDStop;
         Delay(500);
       end;
    finally
      Screen.Cursor := crDefault;
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.UIDWink: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;


function TConfigScreenFm.ZGNMoveUp: Boolean;
var
  vsAddress, vsCommand, vsType: String;
  lslZGNparts:TStringList;
begin
  lslZGNparts:=TStringList.Create;
  try
    vsAddress := '';

    if (VariablesUn.giCmdType = 0) then
      vsType := '64'
    else
      vsType := '128';
    if txtA1.Focused then
    begin
      if VariablesUn.IsZGN(txtA1.Text) then
      begin
        vsAddress := txtA1.Text;
      end
      else if VariablesUn.IsG('.', Trim(txtA1.Text), lslZGNparts) then
      begin
        vsAddress := GtoZGN2(txtA1.Text);
      end;
    end
    else if txtA2.Focused then
      begin
      if VariablesUn.IsZGN(txtA2.Text) then
      begin
        vsAddress := txtA2.Text;
      end
      else if VariablesUn.IsG('.', Trim(txtA2.Text), lslZGNparts) then
      begin
        vsAddress := GtoZGN2(txtA2.Text);
      end;
    end
    else if txtA3.Focused then
    begin
      if VariablesUn.IsZGN(txtA3.Text) then
      begin
        vsAddress := txtA3.Text;
      end
      else if VariablesUn.IsG('.', Trim(txtA3.Text), lslZGNparts) then
      begin
        vsAddress := GtoZGN2(txtA3.Text);
      end;
    end
    else if txtA4.Focused then
    begin
      if VariablesUn.IsZGN(txtA4.Text) then
      begin
        vsAddress := txtA4.Text;
      end
      else if VariablesUn.IsG('.', Trim(txtA4.Text), lslZGNparts) then
      begin
        vsAddress := GtoZGN2(txtA4.Text);
      end;
    end
    else if txtA5.Focused then
    begin
      if VariablesUn.IsZGN(txtA5.Text) then
      begin
        vsAddress := txtA5.Text;
      end
      else if VariablesUn.IsG('.', Trim(txtA5.Text), lslZGNparts) then
      begin
        vsAddress := GtoZGN2(txtA5.Text);
      end;
    end
    else if txtA6.Focused then
    begin
      if VariablesUn.IsZGN(txtA6.Text) then
      begin
        vsAddress := txtA6.Text;
      end
      else if VariablesUn.IsG('.', Trim(txtA6.Text), lslZGNparts) then
      begin
        vsAddress := GtoZGN2(txtA6.Text);
      end;
    end
    else if txtA7.Focused then
    begin
      if VariablesUn.IsZGN(txtA7.Text) then
      begin
        vsAddress := txtA7.Text;
      end
      else if VariablesUn.IsG('.', Trim(txtA7.Text), lslZGNparts) then
      begin
        vsAddress := GtoZGN2(txtA7.Text);
      end;
    end
    else if txtA8.Focused then
    begin
      if VariablesUn.IsZGN(txtA8.Text) then
      begin
        vsAddress := txtA8.Text;
      end
      else if VariablesUn.IsG('.', Trim(txtA8.Text), lslZGNparts) then
      begin
        vsAddress := GtoZGN2(txtA8.Text);
      end;
    end
    else if txtLVzgn.Focused then
    begin
      if VariablesUn.IsZGN(txtLVzgn.Text) then
      begin
        vsAddress := txtLVzgn.Text;
      end
      else if VariablesUn.IsG('.', Trim(txtLVzgn.Text), lslZGNparts) then
      begin
        vsAddress := GtoZGN2(txtLVzgn.Text);
      end;
    end
    else
    begin
      ZGNMoveUp := false;
      exit;
    end;

    if not VariablesUn.IsZGN(vsAddress) then
    begin
      ZGNMoveUp := false;
      exit;
    end;
    vsCommand := '>' + vsType + '.0.' + VariablesUn.UID_TOP + '.' + IntToStr(VariablesUn.giZ) + '.' + IntToStr(VariablesUn.giG) + '.' + IntToStr(VariablesUn.giN);
    ComFm.SendCommand(vsCommand, nil);
    ZGNMoveUp := true;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.ZGNMoveUp: ' + E.Message, mtWarning, [mbOk], 0);
      ZGNMoveUp := false;
    end;
  End;
end;

function TConfigScreenFm.ZGNMoveDwn: Boolean;
var vsAddress, vsCommand, vsType: String;
    lslZGNparts:TStringList;
begin
  lslZGNparts:=TStringList.Create;
  try
    vsAddress := '';

    if (VariablesUn.giCmdType = 0) then
      vsType := '64'
    else
      vsType := '128';
      
    if txtA1.Focused then
    begin
      if VariablesUn.IsZGN(txtA1.Text) then
      begin
        vsAddress := txtA1.Text;
      end
      else if VariablesUn.IsG('.', Trim(txtA1.Text), lslZGNparts) then
      begin
        vsAddress := GtoZGN2(txtA1.Text);
      end;
    end
    else if txtA2.Focused then
    begin
      if VariablesUn.IsZGN(txtA2.Text) then
      begin
        vsAddress := txtA2.Text;
      end
      else if VariablesUn.IsG('.', Trim(txtA2.Text), lslZGNparts) then
      begin
        vsAddress := GtoZGN2(txtA2.Text);
      end;
    end
    else if txtA3.Focused then
    begin
      if VariablesUn.IsZGN(txtA3.Text) then
      begin
        vsAddress := txtA3.Text;
      end
      else if VariablesUn.IsG('.', Trim(txtA3.Text), lslZGNparts) then
      begin
        vsAddress := GtoZGN2(txtA3.Text);
      end;
    end
    else if txtA4.Focused then
    begin
      if VariablesUn.IsZGN(txtA4.Text) then
      begin
        vsAddress := txtA4.Text;
      end
      else if VariablesUn.IsG('.', Trim(txtA4.Text), lslZGNparts) then
      begin
        vsAddress := GtoZGN2(txtA4.Text);
      end;
    end
    else if txtA5.Focused then
    begin
      if VariablesUn.IsZGN(txtA5.Text) then
      begin
        vsAddress := txtA5.Text;
      end
      else if VariablesUn.IsG('.', Trim(txtA5.Text), lslZGNparts) then
      begin
        vsAddress := GtoZGN2(txtA5.Text);
      end;
    end
    else if txtA6.Focused then
    begin
      if VariablesUn.IsZGN(txtA6.Text) then
      begin
        vsAddress := txtA6.Text;
      end
      else if VariablesUn.IsG('.', Trim(txtA6.Text), lslZGNparts) then
      begin
        vsAddress := GtoZGN2(txtA6.Text);
      end;
    end
    else if txtA7.Focused then
    begin
      if VariablesUn.IsZGN(txtA7.Text) then
      begin
        vsAddress := txtA7.Text;
      end
      else if VariablesUn.IsG('.', Trim(txtA7.Text), lslZGNparts) then
      begin
        vsAddress := GtoZGN2(txtA7.Text);
      end;
    end
    else if txtA8.Focused then
    begin
      if VariablesUn.IsZGN(txtA8.Text) then
      begin
        vsAddress := txtA8.Text;
      end
      else if VariablesUn.IsG('.', Trim(txtA8.Text), lslZGNparts) then
      begin
        vsAddress := GtoZGN2(txtA8.Text);
      end;
    end
    else if txtLVzgn.Focused then
    begin
      if VariablesUn.IsZGN(txtLVzgn.Text) then
      begin
        vsAddress := txtLVzgn.Text;
      end
      else if VariablesUn.IsG('.', Trim(txtLVzgn.Text), lslZGNparts) then
      begin
        vsAddress := GtoZGN2(txtLVzgn.Text);
      end;
    end
    else
    begin
      ZGNMoveDwn := false;
      exit;
    end;

    if not VariablesUn.IsZGN(vsAddress) then
    begin
      ZGNMoveDwn := false;
      exit;
    end;
    vsCommand := '>' + vsType + '.0.' + VariablesUn.UID_DN + '.' + IntToStr(VariablesUn.giZ) + '.' + IntToStr(VariablesUn.giG) + '.' + IntToStr(VariablesUn.giN);
    ComFm.SendCommand(vsCommand, nil);
    ZGNMoveDwn := true;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.ZGNMoveDwn: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  End;
end;

function TConfigScreenFm.ZGNStop: Boolean;
var vsAddress, vsCommand, vsType: String;
  lslZGNparts:TStringList;
begin
  lslZGNparts:=TStringList.Create;
  try
    vsAddress := '';

    if (VariablesUn.giCmdType = 0) then
      vsType := '64'
    else
      vsType := '128';
      
    if txtA1.Focused then
    begin
      if VariablesUn.IsZGN(txtA1.Text) then
      begin
        vsAddress := txtA1.Text;
      end
      else if VariablesUn.IsG('.', Trim(txtA1.Text), lslZGNparts) then
      begin
        vsAddress := GtoZGN2(txtA1.Text);
      end;
    end
    else if txtA2.Focused then
    begin
      if VariablesUn.IsZGN(txtA2.Text) then
      begin
        vsAddress := txtA2.Text;
      end
      else if VariablesUn.IsG('.', Trim(txtA2.Text), lslZGNparts) then
      begin
         vsAddress := GtoZGN2(txtA2.Text);
      end;
    end
    else if txtA3.Focused then
    begin
      if VariablesUn.IsZGN(txtA3.Text) then
      begin
        vsAddress := txtA3.Text;
      end
      else if VariablesUn.IsG('.', Trim(txtA3.Text), lslZGNparts) then
      begin
         vsAddress := GtoZGN2(txtA3.Text);
      end;
    end
    else if txtA4.Focused then
    begin
      if VariablesUn.IsZGN(txtA4.Text) then
      begin
        vsAddress := txtA4.Text;
      end
      else if VariablesUn.IsG('.', Trim(txtA4.Text), lslZGNparts) then
      begin
         vsAddress := GtoZGN2(txtA4.Text);
      end;
    end
    else if txtA5.Focused then
    begin
      if VariablesUn.IsZGN(txtA5.Text) then
      begin
        vsAddress := txtA5.Text;
      end
      else if VariablesUn.IsG('.', Trim(txtA5.Text), lslZGNparts) then
      begin
         vsAddress := GtoZGN2(txtA5.Text);
      end;
    end
    else if txtA6.Focused then
    begin
      if VariablesUn.IsZGN(txtA6.Text) then
      begin
        vsAddress := txtA6.Text;
      end
      else if VariablesUn.IsG('.', Trim(txtA6.Text), lslZGNparts) then
      begin
         vsAddress := GtoZGN2(txtA6.Text);
      end;
    end
    else if txtA7.Focused then
    begin
      if VariablesUn.IsZGN(txtA7.Text) then
      begin
        vsAddress := txtA7.Text;
      end
      else if VariablesUn.IsG('.', Trim(txtA7.Text), lslZGNparts) then
      begin
         vsAddress := GtoZGN2(txtA7.Text);
      end;
    end
    else if txtA8.Focused then
    begin
      if VariablesUn.IsZGN(txtA8.Text) then
      begin
        vsAddress := txtA8.Text;
      end
      else if VariablesUn.IsG('.', Trim(txtA8.Text), lslZGNparts) then
      begin
         vsAddress := GtoZGN2(txtA8.Text);
      end;
    end
    else if txtLVzgn.Focused then
    begin
      if VariablesUn.IsZGN(txtLVzgn.Text) then
      begin
        vsAddress := txtLVzgn.Text;
      end
      else if VariablesUn.IsG('.', Trim(txtLVzgn.Text), lslZGNparts) then
      begin
         vsAddress := GtoZGN2(txtLVzgn.Text);
      end;
    end
    else
    begin
      ZGNStop := false;
      exit;
    end;

    if not VariablesUn.IsZGN(vsAddress) then
    begin
      //MessageDlg('Please enter a valid ZGN Address: n.n.n (n=0-255)', mtWarning, [mbOK], 0);
      ZGNStop := false;
      exit;
    end;
    vsCommand := '>' + vsType + '.0.' + VariablesUn.UID_Stop + '.' + IntToStr(VariablesUn.giZ) + '.' + IntToStr(VariablesUn.giG) + '.' + IntToStr(VariablesUn.giN);
    ComFm.SendCommand(vsCommand, nil);
    ZGNStop := true;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.ZGNStop: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  End;
end;

procedure TConfigScreenFm.ZGNWink(viWinkCount: Integer);
var i: Integer;
begin
  try
    try
      Screen.Cursor := crHourGlass;
      for i := 1 to viWinkCount do
       begin
         if not ZGNMoveUp then
         begin
          MessageDlg('Please select a valid address', mtWarning, [mbOK], 0);
          exit;
         end;
         Delay(1000);
         Delay(500);
         ZGNStop;
         Delay(500);
         ZGNMoveDwn;
         Delay(500);
         Delay(1150);
         ZGNStop;
         Delay(500);
       end;
    finally
      Screen.Cursor := crDefault;
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.ZGNWink: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  End;
end;

procedure TConfigScreenFm.sbMainClick(Sender: TObject);
begin
  try
    pnlMenuBar.Visible := False;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.sbMainClick: ' + E.Message, mtWarning, [mbOk], 0);
      //ZGNMoveUp := false;
    end;
  End;
end;

procedure TConfigScreenFm.btnEnabledCClick(Sender: TObject);
begin
  try
    btnGetStatusClick(nil);
    btnEnabledC.Visible := False;
    btnEnabledS.Visible := False;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnEnabledCClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  End;
end;

procedure TConfigScreenFm.btnEnabledSClick(Sender: TObject);
var
    vsCommand:String;
begin
  try
    btnEnabledC.Visible := False;
    btnEnabledS.Visible := False;
    if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
    begin
      giUID :=  StrToInt(GetUID(cmbUID.text));
      if (ismni)or(IsIQMLC2) then
      begin
        if cbSwEnabled.Checked then
        begin
          vsCommand:=VariablesUn.UID_LSE+'.0.0.'+IntToStr(VariablesUn.Clear_a_Bit(SwitchEnabledBits,StrToInt(cmbMotorSelector.text)-1));
        end
        else
        begin
          vsCommand:=VariablesUn.UID_LSE+'.0.0.'+IntToStr(VariablesUn.Set_a_Bit(SwitchEnabledBits,StrToInt(cmbMotorSelector.text)-1));
        end;
        ComFm.SetGenericParam(giUID,vsCommand, nil);
      end
      else
      begin
        if cbSwEnabled.Checked then
        begin
          ComFm.SetMotorStatus(giUID, VariablesUn.UID_LSE, 0, 0, 0, nil);
        end
        else
        begin
          ComFm.SetMotorStatus(giUID, VariablesUn.UID_LSE, 0, 0, 1, nil);
        end;
      end;
      Delay(400);
      btnGetStatusClick(nil);
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnEnabledSClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnExitClick(Sender: TObject);
begin
  try
    pnlMenuBar.Visible := False;
    DataUn.dmDataModule.dsMotors.Filtered := False;
    DataUn.dmDataModule.dsMWCChannels.Filtered := False;
    DataUn.dmDataModule.dsMWCs.Filtered := False;
    DataUn.dmDataModule.dsMNI.Filtered := False;
    DataUn.dmDataModule.dsMNI2.Filtered := False;
    DataUn.dmDataModule.dsIQMLC2.Filtered := False;
    DataUn.dmDataModule.dsPowerPanel.Filtered := False;

    DataUn.dmDataModule.dsMotors.SaveToFile(VariablesUn.gsDataPath + 'dsMotors.txt', dfXML);
    DataUn.dmDataModule.dsMWCs.SaveToFile(VariablesUn.gsDataPath + 'dsMWCs.txt', dfXML);
    DataUn.dmDataModule.dsMWCChannels.SaveToFile(VariablesUn.gsDataPath + 'dsMWCChannels.txt', dfXML);
    DataUn.dmDataModule.dsMNI.SaveToFile(VariablesUn.gsDataPath + 'dsMNI.txt', dfXML);
    DataUn.dmDataModule.dsMNI2.SaveToFile(VariablesUn.gsDataPath + 'dsMNI2.txt', dfXML);
    DataUn.dmDataModule.dsIQMLC2.SaveToFile(VariablesUn.gsDataPath + 'dsIQMLC2.txt', dfXML);
    DataUn.dmDataModule.dsPowerPanel.SaveToFile(VariablesUn.gsDataPath + 'dsPowerPanel.txt', dfXML);
    if not (gbSaveZGN) and not (gbSaveLV) and not (gbSaveUID)  or not (MessageDlg('Do you want to save the changes?', mtWarning, [mbYes, mbNo], 0) = mrYes) then
    begin
      //SendToDCMotors(0);
      configscreenFm.OnHide:=nil;
      Application.Terminate
    end
    else
    begin
      if gbSaveUID then
        cmbUID.SelectAll
      else if (gbSaveZGN) then
      begin
        sbMain.VertScrollBar.Position := 0;
        //btnSave.SetFocus;
      end
      else if (gbSaveLV) then
      begin
        sbMain.VertScrollBar.Position := 806;
        //btnSaveLV.SetFocus;
      end;

      sbMain.Repaint;
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnExitClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnExitMouseEnter(Sender: TObject);
begin
  try
    pnlMenuBar.Visible := True;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnExitMouseEnter: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnExternalThresholdCClick(Sender: TObject);
begin
  try
    btnGetStatusClick(nil);
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.btnExternalThresholdCClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnExternalThresholdSClick(Sender: TObject);
begin
  if teExternalThreshold.Text<>'' then
  begin
    if StrToInt(teExternalThreshold.Text)>255 then
    begin
      MessageDlg('Value Cannot Exceed 255' , mtWarning, [mbOk], 0);
      teExternalThreshold.Text:='255'

    end
    else
    begin
      VariablesUn.ClearData;
      giSentCom := StrToInt(VariablesUn.IQ2DC_Cutoff_Thresholds);
      WaitforSetResponse(giUID, VariablesUn.IQ2DC_Cutoff_Thresholds+'.1.'+IntToStr(mInternalThreshold)+'.'+teExternalThreshold.text, rhMotorStatusRecieved,200);
      btnGetStatusClick(nil);
    end;
  end
  else
  begin
    MessageDlg('Value Cannot be empty' , mtWarning, [mbOk], 0);
    teExternalThreshold.Text:='0'
  end;
end;

procedure TConfigScreenFm.btnFindClick(Sender: TObject);
begin
  try
    pnlMenuBar.Visible := False;
    if not (gbSaveZGN) and not (gbSaveLV) and not (gbSaveUID)  or not (MessageDlg('Do you want to save the changes?', mtWarning, [mbYes, mbNo], 0) = mrYes) then
    begin
      VariablesUn.gbGoToConfig := True;
      Hide;
      if Assigned (rh_ShowFind) then
       rh_ShowFind(Self);
    end
    else
    begin
      if gbSaveUID then
        cmbUID.SelectAll
      else if (gbSaveZGN) then
      begin
        sbMain.VertScrollBar.Position := 0;
        //btnSave.SetFocus;
      end
      else if (gbSaveLV) then
      begin
        sbMain.VertScrollBar.Position := 806;
        //btnSaveLV.SetFocus;
      end;

      sbMain.Repaint;
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnFindClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnFindMouseEnter(Sender: TObject);
begin
  try
    pnlMenuBar.Visible := True;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnFibtnFindMouseEnterndClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnFwdCClick(Sender: TObject);
begin
  btnGetStatusClick(nil);
end;

procedure TConfigScreenFm.btnFwdSClick(Sender: TObject);
var viIndex: Integer;
    vsCommand:String;
begin
  try
    if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
    begin
      giUID :=  StrToInt(GetUID(cmbUID.text));

      if cmbFwd.ItemIndex  = 0 then
        viIndex := 1
      else
        viIndex := 0;

      if (ismni)or(IsIQMLC2) then
      begin
        if cmbFwd.ItemIndex=0 then
        begin
          vsCommand:=VariablesUn.UID_LSF+'.0.0.'+IntToStr(VariablesUn.set_a_Bit(SwitchFwdBits,StrToInt(cmbMotorSelector.text)-1));
        end
        else
        begin
          vsCommand:=VariablesUn.UID_LSF+'.0.0.'+IntToStr(VariablesUn.Clear_a_Bit(SwitchFwdBits,StrToInt(cmbMotorSelector.text)-1));;
        end;
        ComFm.SetGenericParam(giUID,vsCommand, nil);
      end
      else
      begin
        ComFm.SetMotorStatus(giUID, VariablesUn.UID_LSF, 0, 0, viIndex, nil);
      end;
      Delay(200);
      btnGetStatusClick(nil);
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnFwdSClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnAutoReturn(Sender: TObject);
var
    vsCommand:String;
begin
  try
    if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
    begin
      giUID :=  StrToInt(GetUID(cmbUID.text));
      if isMNI then
      begin
        vsCommand:=VariablesUn.UID_AutoReturn+'.0.'+IntToStr(hi(StrToInt(teAutoReturnIQ2.text)))+'.'+IntToStr(Lo(StrToInt(teAutoReturnIQ2.text)));
        ComFm.SetGenericParam(giUID,vsCommand, nil);
      end
      else
      begin
        ComFm.SetMotorStatus(giUID, VariablesUn.UID_AutoReturn, 0, hi(StrToInt(teAutoReturnIQ2.text)), Lo(StrToInt(teAutoReturnIQ2.text)), rhMotorStatusRecieved);
      end;
      Delay(200);
      btnGetStatusClick(nil);
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnFwdSClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnGetStatusClick(Sender: TObject);
begin
  try
    try
      if not MotorChangeFlg then
      begin
        //sbMain.VertScrollBar.Position := 0;
        MotorChangeFlg:=False;
      end;
      Screen.Cursor := crHourGlass;
      ChangeStyleList;
      if giComPortOn then
      begin
        gbUIDfound := False;
        GetMotorConnected;
        VariablesUn.delay(500);
        if gbUIDfound then
        begin
          GetMotorStatus;
          ButtonsEnable;
        end
        else
        begin
          DisplayParameters;
          ShowMessage('Motor Not Found');
        end;
      end
      else
         DisplayParameters;
    finally
      Screen.Cursor := crDefault;
      //sbMain.VertScrollBar.Position := 0;
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnGetStatusClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnMainClick(Sender: TObject);
begin
  try
    pnlMenuBar.Visible := False;
    if not (gbSaveZGN) and not (gbSaveLV) and not (gbSaveUID) or not (MessageDlg('Do you want to save the changes?', mtWarning, [mbYes, mbNo], 0) = mrYes) then
    begin
      ConfigScreenFm.Hide;
      if Assigned (rh_ShowMain) then
        rh_ShowMain(Self);
    end
    else
    begin
      if gbSaveUID then
        cmbUID.SelectAll
      else if (gbSaveZGN) then
      begin
        sbMain.VertScrollBar.Position := 0;
        //btnSave.SetFocus;
      end
      else if (gbSaveLV) then
      begin
        sbMain.VertScrollBar.Position := 806;
        //btnSaveLV.SetFocus;
      end;

      sbMain.Repaint;
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnMainClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnMainMouseEnter(Sender: TObject);
begin
  pnlMenuBar.Visible := True;
end;

procedure TConfigScreenFm.btnMaintCClick(Sender: TObject);
begin
  btnGetStatusClick(nil);
end;

procedure TConfigScreenFm.btnMaintSClick(Sender: TObject);
var
    vsCommand:String;
begin
  try
    if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
    begin
      giUID :=  StrToInt(GetUID(cmbUID.text));
      if (ismni) then
      begin
        if cmbMaint.ItemIndex=0 then
        begin
          vsCommand:=VariablesUn.UID_Main+'.0.0.'+IntToStr(VariablesUn.Clear_a_Bit(MotorMaintBits,StrToInt(cmbMotorSelector.text)-1));
        end
        else
        begin
          vsCommand:=VariablesUn.UID_Main+'.0.0.'+IntToStr(VariablesUn.Set_a_Bit(MotorMaintBits,StrToInt(cmbMotorSelector.text)-1));
        end;
        ComFm.SetGenericParam(giUID,vsCommand, nil);
      end
      else if isIQMLC2 then
      begin
        vsCommand:=VariablesUn.UID_MS1+'.'+BuildIQMLC2Setup1MessageforMaintenance;
        ComFm.SetGenericParam(giUID,vsCommand, nil);
      end
      else
      begin
        ComFm.SetMotorStatus(giUID, VariablesUn.UID_Main, 0, 0, cmbMaint.ItemIndex, nil);
      end;
      Delay(200);
      btnGetStatusClick(nil);
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnMaintSClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnMotionThresholdCClick(Sender: TObject);
begin
  try
    btnGetStatusClick(nil);
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.btnMotionThresholdCClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnMotionThresholdSClick(Sender: TObject);
begin
  if teMotionThreshold.Text<>'' then
  begin
    if StrToInt(teMotionThreshold.Text)>1000 then
    begin
      MessageDlg('Value Cannot Exceed 1000' , mtWarning, [mbOk], 0);
      teMotionThreshold.Text:='1000'

    end
    else
    begin
      VariablesUn.ClearData;
      giSentCom := StrToInt(VariablesUn.IQ2DC_Cutoff_Thresholds);
      WaitforSetResponse(giUID, VariablesUn.IQ2DC_Cutoff_Thresholds+'.0.'+IntToStr(Hi(StrToInt(teMotionThreshold.text)))+'.'+IntToStr(Lo(StrToInt(teMotionThreshold.text))), rhMotorStatusRecieved,200);
      btnGetStatusClick(nil);
    end;
  end
  else
  begin
    MessageDlg('Value Cannot be empty' , mtWarning, [mbOk], 0);
    teMotionThreshold.Text:='0'
  end;
end;

procedure TConfigScreenFm.btnMotorPersonCClick(Sender: TObject);
begin
  try
    btnGetStatusClick(nil);
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnMotorPersonCClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnMotorPersonSClick(Sender: TObject);
var
    vsCommand:String;
begin
  try
    if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
    begin
      giUID :=  StrToInt(GetUID(cmbUID.text));
      vsCommand:=VariablesUn.MNI_MotorPerson+'.'+cmbMotorSelector.Text+'.'+IntToStr(cmbDCMotors.ItemIndex);
      ComFm.SetGenericParam(giUID,vsCommand, nil);
      Delay(200);
      btnGetStatusClick(nil);
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnMTAsClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnMTAcClick(Sender: TObject);
begin
  try
    btnGetStatusClick(nil);
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnMTAcClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnMTAsClick(Sender: TObject);
var
  vsCommand:String;
begin
  try
    if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
    begin
      giUID :=  StrToInt(GetUID(cmbUID.text));
      if (ismni)then
      begin
        if cmbMTAInb.ItemIndex=0 then
        begin
          vsCommand:=VariablesUn.UID_MTA_IN+'.0.0.'+IntToStr(VariablesUn.Clear_a_Bit(MotorInhibitModeBits,StrToInt(cmbMotorSelector.text)-1));
        end
        else
        begin
          vsCommand:=VariablesUn.UID_MTA_IN+'.0.0.'+IntToStr(VariablesUn.Set_a_Bit(MotorInhibitModeBits,StrToInt(cmbMotorSelector.text)-1));
        end;
        ComFm.SetGenericParam(giUID,vsCommand, nil);

      end
      else if IsIQMLC2 then
      begin
        vsCommand:=VariablesUn.UID_MS1+'.'+BuildIQMLC2Setup1MessageforMTAInhibit;
        ComFm.SetGenericParam(giUID,vsCommand, rhMotorStatusRecieved);
      end
      else
      begin
        ComFm.SetMotorStatus(giUID, VariablesUn.UID_MTA_IN, 0, 0, cmbMTAInb.ItemIndex, nil);
      end;
      Delay(200);
      btnGetStatusClick(nil);
    end;
  except
  on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnMTAsClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnPortCClick(Sender: TObject);
begin
  try
    btnGetStatusClick(nil);
  except
  on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnPortCClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnPortSClick(Sender: TObject);
var
    vsCommand:String;
begin
  try
    if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
    begin
      giUID :=  StrToInt(GetUID(cmbUID.text));
      if(IsIQMLC2Old) then
      begin
        vsCommand:=VariablesUn.UID_MS1+'.'+BuildIQMLC2Setup1MessageforLSIS;
        ComFm.SetGenericParam(giUID,vsCommand, nil);
      end
      else if (ismni)or (IsIQMLC2) then
      begin

        if cmbPortMode.ItemIndex=0 then
        begin
          vsCommand:=VariablesUn.UID_PortMode+'.0.0.'+IntToStr(VariablesUn.Clear_a_Bit(SwitchPortModeBits,StrToInt(cmbMotorSelector.text)-1));
        end
        else
        begin
          vsCommand:=VariablesUn.UID_PortMode+'.0.0.'+IntToStr(VariablesUn.Set_a_Bit(SwitchPortModeBits,StrToInt(cmbMotorSelector.text)-1));
        end;
        ComFm.SetGenericParam(giUID,vsCommand, nil);
      end
      else
      begin
        ComFm.SetMotorStatus(giUID, VariablesUn.UID_PortMode, 0, 0, cmbPortMode.ItemIndex, nil);
      end;
      Delay(200);
      btnGetStatusClick(nil);
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnPortSClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnRefreshAccelClick(Sender: TObject);
begin
  //Get Accel X Current
  VariablesUn.ClearData;
  giSentCom := StrToInt(LeftStr(VariablesUn.IQ2DC_Acc_x_Current,3));
  WaitforResponse(giUID, VariablesUn.IQ2DC_Acc_x_Current+'.0.0', rhMotorStatusRecieved,200);
  if flgHaltStatus then
  begin
    flgHaltStatus:=false;
    Exit;
  end;

  //Get Accel X Ave
  VariablesUn.ClearData;
  giSentCom := StrToInt(LeftStr(VariablesUn.IQ2DC_Acc_x_Ave,3));
  WaitforResponse(giUID, VariablesUn.IQ2DC_Acc_x_Ave+'.0.0', rhMotorStatusRecieved,200);
  if flgHaltStatus then
  begin
    flgHaltStatus:=false;
    Exit;
  end;

   //Get Accel X Min
  VariablesUn.ClearData;
  giSentCom := StrToInt(LeftStr(VariablesUn.IQ2DC_Acc_x_Min,3));
  WaitforResponse(giUID, VariablesUn.IQ2DC_Acc_x_Min+'.0.0', rhMotorStatusRecieved,200);
  if flgHaltStatus then
  begin
    flgHaltStatus:=false;
    Exit;
  end;

   //Get Accel X Max
  VariablesUn.ClearData;
  giSentCom := StrToInt(LeftStr(VariablesUn.IQ2DC_Acc_x_Max,3));
  WaitforResponse(giUID, VariablesUn.IQ2DC_Acc_x_Max+'.0.0', rhMotorStatusRecieved,200);
  if flgHaltStatus then
  begin
    flgHaltStatus:=false;
    Exit;
  end;

  //Get Accel Y Current
  VariablesUn.ClearData;
  giSentCom := StrToInt(LeftStr(VariablesUn.IQ2DC_Acc_y_Current,3));
  WaitforResponse(giUID, VariablesUn.IQ2DC_Acc_y_Current+'.0.0', rhMotorStatusRecieved,200);
  if flgHaltStatus then
  begin
    flgHaltStatus:=false;
    Exit;
  end;

  //Get Accel Y Ave
  VariablesUn.ClearData;
  giSentCom := StrToInt(LeftStr(VariablesUn.IQ2DC_Acc_y_Ave,3));
  WaitforResponse(giUID, VariablesUn.IQ2DC_Acc_y_Ave+'.0.0', rhMotorStatusRecieved,200);
  if flgHaltStatus then
  begin
    flgHaltStatus:=false;
    Exit;
  end;

   //Get Accel Y Min
  VariablesUn.ClearData;
  giSentCom := StrToInt(LeftStr(VariablesUn.IQ2DC_Acc_y_Min,3));
  WaitforResponse(giUID, VariablesUn.IQ2DC_Acc_y_Min+'.0.0', rhMotorStatusRecieved,200);
  if flgHaltStatus then
  begin
    flgHaltStatus:=false;
    Exit;
  end;

   //Get Accel Y Max
  VariablesUn.ClearData;
  giSentCom := StrToInt(LeftStr(VariablesUn.IQ2DC_Acc_y_Max,3));
  WaitforResponse(giUID, VariablesUn.IQ2DC_Acc_y_Max+'.0.0', rhMotorStatusRecieved,200);
  if flgHaltStatus then
  begin
    flgHaltStatus:=false;
    Exit;
  end;

  //Get Accel Z Current
  VariablesUn.ClearData;
  giSentCom := StrToInt(LeftStr(VariablesUn.IQ2DC_Acc_z_Current,3));
  WaitforResponse(giUID, VariablesUn.IQ2DC_Acc_z_Current+'.0.0', rhMotorStatusRecieved,200);
  if flgHaltStatus then
  begin
    flgHaltStatus:=false;
    Exit;
  end;

  //Get Accel Z Ave
  VariablesUn.ClearData;
  giSentCom := StrToInt(LeftStr(VariablesUn.IQ2DC_Acc_z_Ave,3));
  WaitforResponse(giUID, VariablesUn.IQ2DC_Acc_z_Ave+'.0.0', rhMotorStatusRecieved,200);
  if flgHaltStatus then
  begin
    flgHaltStatus:=false;
    Exit;
  end;

   //Get Accel Z Min
  VariablesUn.ClearData;
  giSentCom := StrToInt(LeftStr(VariablesUn.IQ2DC_Acc_z_Min,3));
  WaitforResponse(giUID, VariablesUn.IQ2DC_Acc_z_Min+'.0.0', rhMotorStatusRecieved,200);
  if flgHaltStatus then
  begin
    flgHaltStatus:=false;
    Exit;
  end;

   //Get Accel Z Max
  VariablesUn.ClearData;
  giSentCom := StrToInt(LeftStr(VariablesUn.IQ2DC_Acc_z_Max,3));
  WaitforResponse(giUID, VariablesUn.IQ2DC_Acc_z_Max+'.0.0', rhMotorStatusRecieved,200);
  if flgHaltStatus then
  begin
    flgHaltStatus:=false;
    Exit;
  end;
end;

Function TConfigScreenFm.BuildIQMLC2Setup1MessageforLSIS:String;
var
  gsMessage:String;
  gibyteH,gibyteM,gibyteL,I:Integer;
begin
  gibyteH:=0;
  gibyteM:=0;
  gibyteL:=0;
  dmDataModule.dsIQMLC2.Filtered := False;
  dmDataModule.dsIQMLC2.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + IntToStr(giUID);
  dmDataModule.dsIQMLC2.Filtered := True;

  //Data H
  for I := 4 Downto 1 do
  begin
    dmDataModule.dsMotors.Filtered := False;
    dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + IntToStr(giUID) +  ' and MotorNumber = '+IntToStr(I);
    dmDataModule.dsMotors.Filtered := True;
    if DataUn.dmDataModule.dsMotors.FieldByName('Inhibit').AsString='ON' then
    begin
      if I=4 then
      begin
        gibyteH:=VariablesUn.Set_a_Bit(gibyteH,0);
      end
      else if I=3 then
      begin
        gibyteH:=VariablesUn.Set_a_Bit(gibyteH,1);
      end
      else if I=2 then
      begin
        gibyteH:=VariablesUn.Set_a_Bit(gibyteH,2);
      end
      else if I=1 then
      begin
        gibyteH:=VariablesUn.Set_a_Bit(gibyteH,3);
      end;
    end;
  end;
  if not(DataUn.dmDataModule.dsIQMLC2.FieldByName('MasterPortEnabled').AsBoolean)then
  begin
    gibyteH:=VariablesUn.Set_a_Bit(gibyteH,4);
  end;
  if not(DataUn.dmDataModule.dsIQMLC2.FieldByName('AutoPortEnabled').AsBoolean)then
  begin
    gibyteH:=VariablesUn.Set_a_Bit(gibyteH,5);
  end;
  if DataUn.dmDataModule.dsIQMLC2.FieldByName('MtaInhibit').AsString='ENA' then
  begin
    gibyteH:=VariablesUn.Set_a_Bit(gibyteH,6);
  end;

  if not(DataUn.dmDataModule.dsIQMLC2.FieldByName('AutoPortMode').AsString='IS')then
  begin
    gibyteH:=VariablesUn.Set_a_Bit(gibyteH,7);
  end;

  //Data M
  for I := 4 Downto 1 do
  begin
    dmDataModule.dsMotors.Filtered := False;
    dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + IntToStr(giUID) +  ' and MotorNumber = '+IntToStr(I);
    dmDataModule.dsMotors.Filtered := True;
    if DataUn.dmDataModule.dsMotors.FieldByName('Maint').AsString = 'ENA' then
    begin
      if I=4 then
      begin
        gibyteM:=VariablesUn.Set_a_Bit(gibyteM,0);
      end
      else if I=3 then
      begin
        gibyteM:=VariablesUn.Set_a_Bit(gibyteM,1);
      end
      else if I=2 then
      begin
        gibyteM:=VariablesUn.Set_a_Bit(gibyteM,2);
      end
      else if I=1 then
      begin
        gibyteM:=VariablesUn.Set_a_Bit(gibyteM,3);
      end;
    end;
  end;
  if not DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort1Enabled').AsBoolean then
  begin
    gibyteM:=VariablesUn.Set_a_Bit(gibyteM,4);
  end;
  if not DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort2Enabled').AsBoolean then
  begin
    gibyteM:=VariablesUn.Set_a_Bit(gibyteM,5);
  end;
  if not DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort3Enabled').AsBoolean then
  begin
    gibyteM:=VariablesUn.Set_a_Bit(gibyteM,6);
  end;
  if not DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort4Enabled').AsBoolean then
  begin
    gibyteM:=VariablesUn.Set_a_Bit(gibyteM,7);
  end;


  //Data L
  if DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort1FWDEnabled').AsBoolean then
  begin
    gibyteL:=VariablesUn.Set_a_Bit(gibyteL,0);
  end;
  if  DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort2FWDEnabled').AsBoolean then
  begin
    gibyteL:=VariablesUn.Set_a_Bit(gibyteL,1);
  end;
  if  DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort3FWDEnabled').AsBoolean then
  begin
    gibyteL:=VariablesUn.Set_a_Bit(gibyteL,2);
  end;
  if  DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort4FWDEnabled').AsBoolean then
  begin
    gibyteL:=VariablesUn.Set_a_Bit(gibyteL,3);
  end;
  if  DataUn.dmDataModule.dsIQMLC2.FieldByName('MasterPortFwdEnabled').AsBoolean then
  begin
    gibyteL:=VariablesUn.Set_a_Bit(gibyteL,4);
  end;
  if  DataUn.dmDataModule.dsIQMLC2.FieldByName('AutoPortFwdEnabled').AsBoolean then
  begin
    gibyteL:=VariablesUn.Set_a_Bit(gibyteL,5);
  end;
  if  DataUn.dmDataModule.dsIQMLC2.FieldByName('StopMode').AsString='Normal' then
  begin
    gibyteL:=VariablesUn.Set_a_Bit(gibyteL,6);
  end;
  if cmbPortMode.text='IS' then
  begin
    gibyteL:=VariablesUn.Set_a_Bit(gibyteL,7);
  end;

//  if  DataUn.dmDataModule.dsIQMLC2.FieldByName('SwModeAll').AsString='IS' then
//  begin
//    VariablesUn.Set_a_Bit(7,gibyteL);
//  end;
  result := IntToStr(gibyteH)+'.'+IntToStr(gibyteM)+'.'+IntToStr(gibyteL);
end;

Function TConfigScreenFm.BuildIQMLC2Setup1MessageforMaintenance:String;
var
  gsMessage:String;
  gibyteH,gibyteM,gibyteL,I:Integer;
begin
  gibyteH:=0;
  gibyteM:=0;
  gibyteL:=0;
  dmDataModule.dsIQMLC2.Filtered := False;
  dmDataModule.dsIQMLC2.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + IntToStr(giUID);
  dmDataModule.dsIQMLC2.Filtered := True;

  //Data H
  for I := 4 Downto 1 do
  begin
    dmDataModule.dsMotors.Filtered := False;
    dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + IntToStr(giUID) +  ' and MotorNumber = '+IntToStr(I);
    dmDataModule.dsMotors.Filtered := True;
    if DataUn.dmDataModule.dsMotors.FieldByName('Inhibit').AsString='ON' then
    begin
      if I=4 then
      begin
        gibyteH:=VariablesUn.Set_a_Bit(gibyteH,0);
      end
      else if I=3 then
      begin
        gibyteH:=VariablesUn.Set_a_Bit(gibyteH,1);
      end
      else if I=2 then
      begin
        gibyteH:=VariablesUn.Set_a_Bit(gibyteH,2);
      end
      else if I=1 then
      begin
        gibyteH:=VariablesUn.Set_a_Bit(gibyteH,3);
      end;
    end;
  end;
  if not(DataUn.dmDataModule.dsIQMLC2.FieldByName('MasterPortEnabled').AsBoolean)then
  begin
    gibyteH:=VariablesUn.Set_a_Bit(gibyteH,4);
  end;
  if not(DataUn.dmDataModule.dsIQMLC2.FieldByName('AutoPortEnabled').AsBoolean)then
  begin
    gibyteH:=VariablesUn.Set_a_Bit(gibyteH,5);
  end;
  if DataUn.dmDataModule.dsIQMLC2.FieldByName('MtaInhibit').AsString='ENA' then
  begin
    gibyteH:=VariablesUn.Set_a_Bit(gibyteH,6);
  end;

  if not(DataUn.dmDataModule.dsIQMLC2.FieldByName('AutoPortMode').AsString='IS')then
  begin
    gibyteH:=VariablesUn.Set_a_Bit(gibyteH,7);
  end;

  //Data M
  for I := 4 Downto 1 do
  begin
    dmDataModule.dsMotors.Filtered := False;
    dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + IntToStr(giUID) +  ' and MotorNumber = '+IntToStr(I);
    dmDataModule.dsMotors.Filtered := True;
    if DataUn.dmDataModule.dsMotors.FieldByName('Maint').AsString = 'ENA' then
    begin
      if I=4 then
      begin
        gibyteM:=VariablesUn.Set_a_Bit(gibyteM,0);
      end
      else if I=3 then
      begin
        gibyteM:=VariablesUn.Set_a_Bit(gibyteM,1);
      end
      else if I=2 then
      begin
        gibyteM:=VariablesUn.Set_a_Bit(gibyteM,2);
      end
      else if I=1 then
      begin
        gibyteM:=VariablesUn.Set_a_Bit(gibyteM,3);
      end;
    end;
  end;
  if not DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort1Enabled').AsBoolean then
  begin
    gibyteM:=VariablesUn.Set_a_Bit(gibyteM,4);
  end;
  if not DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort2Enabled').AsBoolean then
  begin
    gibyteM:=VariablesUn.Set_a_Bit(gibyteM,5);
  end;
  if not DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort3Enabled').AsBoolean then
  begin
    gibyteM:=VariablesUn.Set_a_Bit(gibyteM,6);
  end;
  if not DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort4Enabled').AsBoolean then
  begin
    gibyteM:=VariablesUn.Set_a_Bit(gibyteM,7);
  end;


  //Data L
  if DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort1FWDEnabled').AsBoolean then
  begin
    gibyteL:=VariablesUn.Set_a_Bit(gibyteL,0);
  end;
  if  DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort2FWDEnabled').AsBoolean then
  begin
    gibyteL:=VariablesUn.Set_a_Bit(gibyteL,1);
  end;
  if  DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort3FWDEnabled').AsBoolean then
  begin
    gibyteL:=VariablesUn.Set_a_Bit(gibyteL,2);
  end;
  if  DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort4FWDEnabled').AsBoolean then
  begin
    gibyteL:=VariablesUn.Set_a_Bit(gibyteL,3);
  end;
  if  DataUn.dmDataModule.dsIQMLC2.FieldByName('MasterPortFwdEnabled').AsBoolean then
  begin
    gibyteL:=VariablesUn.Set_a_Bit(gibyteL,4);
  end;
  if  DataUn.dmDataModule.dsIQMLC2.FieldByName('AutoPortFwdEnabled').AsBoolean then
  begin
    gibyteL:=VariablesUn.Set_a_Bit(gibyteL,5);
  end;
  if  DataUn.dmDataModule.dsIQMLC2.FieldByName('StopMode').AsString='Normal' then
  begin
    gibyteL:=VariablesUn.Set_a_Bit(gibyteL,6);
  end;
  if  DataUn.dmDataModule.dsIQMLC2.FieldByName('SwModeAll').AsString='IS' then
  begin
    gibyteL:=VariablesUn.Set_a_Bit(gibyteL,7);
  end;

//  if  DataUn.dmDataModule.dsIQMLC2.FieldByName('SwModeAll').AsString='IS' then
//  begin
//    VariablesUn.Set_a_Bit(7,gibyteL);
//  end;
  result := IntToStr(gibyteH)+'.'+IntToStr(gibyteM)+'.'+IntToStr(gibyteL);
end;

Function TConfigScreenFm.BuildIQMLC2Setup1MessageforMTAInhibit:String;
var
  gsMessage:String;
  gibyteH,gibyteM,gibyteL,I:Integer;
begin
  gibyteH:=0;
  gibyteM:=0;
  gibyteL:=0;
  dmDataModule.dsIQMLC2.Filtered := False;
  dmDataModule.dsIQMLC2.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + IntToStr(giUID);
  dmDataModule.dsIQMLC2.Filtered := True;

  //Data H
  for I := 4 Downto 1 do
  begin
    dmDataModule.dsMotors.Filtered := False;
    dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + IntToStr(giUID) +  ' and MotorNumber = '+IntToStr(I);
    dmDataModule.dsMotors.Filtered := True;
    if DataUn.dmDataModule.dsMotors.FieldByName('Inhibit').AsString='ON' then
    begin
      if I=4 then
      begin
        gibyteH:=VariablesUn.Set_a_Bit(gibyteH,0);
      end
      else if I=3 then
      begin
        gibyteH:=VariablesUn.Set_a_Bit(gibyteH,1);
      end
      else if I=2 then
      begin
        gibyteH:=VariablesUn.Set_a_Bit(gibyteH,2);
      end
      else if I=1 then
      begin
        gibyteH:=VariablesUn.Set_a_Bit(gibyteH,3);
      end;
    end;
  end;
  if not(DataUn.dmDataModule.dsIQMLC2.FieldByName('MasterPortEnabled').AsBoolean)then
  begin
    gibyteH:=VariablesUn.Set_a_Bit(gibyteH,4);
  end;
  if not(DataUn.dmDataModule.dsIQMLC2.FieldByName('AutoPortEnabled').AsBoolean)then
  begin
    gibyteH:=VariablesUn.Set_a_Bit(gibyteH,5);
  end;


  if cmbMTAInb.Text='ENA' then
  begin
    gibyteH:=VariablesUn.Set_a_Bit(gibyteH,6);
  end;

  if not(DataUn.dmDataModule.dsIQMLC2.FieldByName('AutoPortMode').AsString='IS')then
  begin
    gibyteH:=VariablesUn.Set_a_Bit(gibyteH,7);
  end;

  //Data M
  for I := 4 Downto 1 do
  begin
    if(I<>StrToInt(cmbMotorSelector.text)) then
    begin
      dmDataModule.dsMotors.Filtered := False;
      dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + IntToStr(giUID) +  ' and MotorNumber = '+IntToStr(I);
      dmDataModule.dsMotors.Filtered := True;
      if DataUn.dmDataModule.dsMotors.FieldByName('Maint').AsString = 'ENA' then
      begin
        if I=4 then
        begin
          gibyteM:=VariablesUn.Set_a_Bit(gibyteM,0);
        end
        else if I=3 then
        begin
          gibyteM:=VariablesUn.Set_a_Bit(gibyteM,1);
        end
        else if I=2 then
        begin
          gibyteM:=VariablesUn.Set_a_Bit(gibyteM,2);
        end
        else if I=1 then
        begin
          gibyteM:=VariablesUn.Set_a_Bit(gibyteM,3);
        end;
      end;
    end
    else
    begin
      if cmbMaint.text = 'ENA' then
      begin
        gibyteM:=VariablesUn.Set_a_Bit(gibyteM,4-(StrToInt(cmbMotorSelector.text)));
      end;
    end;
  end;
  if not DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort1Enabled').AsBoolean then
  begin
    gibyteM:=VariablesUn.Set_a_Bit(gibyteM,4);
  end;
  if not DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort2Enabled').AsBoolean then
  begin
    gibyteM:=VariablesUn.Set_a_Bit(gibyteM,5);
  end;
  if not DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort3Enabled').AsBoolean then
  begin
    gibyteM:=VariablesUn.Set_a_Bit(gibyteM,6);
  end;
  if not DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort4Enabled').AsBoolean then
  begin
    gibyteM:=VariablesUn.Set_a_Bit(gibyteM,7);
  end;


  //Data L
  if DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort1FWDEnabled').AsBoolean then
  begin
    gibyteL:=VariablesUn.Set_a_Bit(gibyteL,0);
  end;
  if  DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort2FWDEnabled').AsBoolean then
  begin
    gibyteL:=VariablesUn.Set_a_Bit(gibyteL,1);
  end;
  if  DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort3FWDEnabled').AsBoolean then
  begin
    gibyteL:=VariablesUn.Set_a_Bit(gibyteL,2);
  end;
  if  DataUn.dmDataModule.dsIQMLC2.FieldByName('SwPort4FWDEnabled').AsBoolean then
  begin
    gibyteL:=VariablesUn.Set_a_Bit(gibyteL,3);
  end;
  if  DataUn.dmDataModule.dsIQMLC2.FieldByName('MasterPortFwdEnabled').AsBoolean then
  begin
    gibyteL:=VariablesUn.Set_a_Bit(gibyteL,4);
  end;
  if  DataUn.dmDataModule.dsIQMLC2.FieldByName('AutoPortFwdEnabled').AsBoolean then
  begin
    gibyteL:=VariablesUn.Set_a_Bit(gibyteL,5);
  end;
  if  DataUn.dmDataModule.dsIQMLC2.FieldByName('StopMode').AsString='Normal' then
  begin
    gibyteL:=VariablesUn.Set_a_Bit(gibyteL,6);
  end;
  if  DataUn.dmDataModule.dsIQMLC2.FieldByName('SwModeAll').AsString='IS' then
  begin
    gibyteL:=VariablesUn.Set_a_Bit(gibyteL,7);
  end;

//  if  DataUn.dmDataModule.dsIQMLC2.FieldByName('SwModeAll').AsString='IS' then
//  begin
//    VariablesUn.Set_a_Bit(7,gibyteL);
//  end;
  result := IntToStr(gibyteH)+'.'+IntToStr(gibyteM)+'.'+IntToStr(gibyteL);
end;

procedure TConfigScreenFm.btnDCSpeedCClick(Sender: TObject);
begin
  try
    btnGetStatusClick(nil);
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnDCSpeedCClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnSaveBOClick(Sender: TObject);
begin
  try
    btnSaveLVClick(Nil);
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnSaveBOClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnSaveClick(Sender: TObject);
var
  lslZGNparts:TStringList;
begin
  lslZGNparts:=TStringList.Create;
  try
    if not VariablesUn.IsZGN(txtA1.Text) and not (VariablesUn.IsG('.', Trim(txtA1.Text), lslZGNparts))then
    begin
      txtA1.Color := clRed;
      if txtA1.Visible then
      begin
        txtA1.SetFocus;
        MessageDlg('Please enter a valid ZGN Address: n.n.n (n=0-255). Be advised �0� and �255� will act as a master address.', mtWarning, [mbOK], 0);
        exit;
      end;
    end;
    if not VariablesUn.IsZGN(txtA2.Text) and not (VariablesUn.IsG('.', Trim(txtA2.Text), lslZGNparts))then
    begin
      txtA2.Color := clRed;
      if txtA2.Visible then
      begin
        txtA2.SetFocus;
        MessageDlg('Please enter a valid ZGN Address: n.n.n (n=0-255). Be advised �0� and �255� will act as a master address.', mtWarning, [mbOK], 0);
        exit;
      end;
    end;
    if not VariablesUn.IsZGN(txtA3.Text)  and not (VariablesUn.IsG('.', Trim(txtA3.Text), lslZGNparts))then
    begin
      txtA3.Color := clRed;
      if txtA3.Visible then
      begin
        txtA3.SetFocus;
        MessageDlg('Please enter a valid ZGN Address: n.n.n (n=0-255). Be advised �0� and �255� will act as a master address.', mtWarning, [mbOK], 0);
        exit;
      end;
    end;
    if not VariablesUn.IsZGN(txtA4.Text) and not (VariablesUn.IsG('.', Trim(txtA4.Text), lslZGNparts))then
    begin
      txtA4.Color := clRed;
      if txtA4.Visible then
      begin
        txtA4.SetFocus;
        MessageDlg('Please enter a valid ZGN Address: n.n.n (n=0-255). Be advised �0� and �255� will act as a master address.', mtWarning, [mbOK], 0);
        exit;
      end;
    end;
    if not VariablesUn.IsZGN(txtA5.Text) and not (VariablesUn.IsG('.', Trim(txtA5.Text), lslZGNparts))then
    begin
      txtA5.Color := clRed;
      if txtA5.Visible then
      begin
        txtA5.SetFocus;
        MessageDlg('Please enter a valid ZGN Address: n.n.n (n=0-255). Be advised �0� and �255� will act as a master address.', mtWarning, [mbOK], 0);
        exit;
      end;
    end;
    if not VariablesUn.IsZGN(txtA6.Text) and not (VariablesUn.IsG('.', Trim(txtA6.Text), lslZGNparts))then
    begin
      txtA6.Color := clRed;
      if txtA6.Visible then
      begin
        txtA6.SetFocus;
        MessageDlg('Please enter a valid ZGN Address: n.n.n (n=0-255). Be advised �0� and �255� will act as a master address.', mtWarning, [mbOK], 0);
        exit;
      end;
    end;
    if not VariablesUn.IsZGN(txtA7.Text) and not (VariablesUn.IsG('.', Trim(txtA7.Text), lslZGNparts))then
    begin
      txtA7.Color := clRed;
      if txtA7.Visible then
      begin
        txtA7.SetFocus;
        MessageDlg('Please enter a valid ZGN Address: n.n.n (n=0-255). Be advised �0� and �255� will act as a master address.', mtWarning, [mbOK], 0);
        exit;
      end;
    end;
    if not VariablesUn.IsZGN(txtA8.Text) and not (VariablesUn.IsG('.', Trim(txtA8.Text), lslZGNparts))then
    begin
      txtA8.Color := clRed;
      if txtA8.Visible then
      begin
        txtA8.SetFocus;
        MessageDlg('Please enter a valid ZGN Address: n.n.n (n=0-255). Be advised �0� and �255� will act as a master address.', mtWarning, [mbOK], 0);
        exit;
      end;
    end;

    if not VariablesUn.IsZGN(txtLVzgn.Text) and not (VariablesUn.IsG('.', Trim(txtLVzgn.Text), lslZGNparts)) and isSW then
    begin
      txtLVzgn.Color := clRed;
      if txtLVzgn.Visible then
      begin
        txtLVzgn.SetFocus;
        MessageDlg('Please enter a valid ZGN Address: n.n.n (n=0-255). Be advised �0� and �255� will act as a master address.', mtWarning, [mbOK], 0);
        exit;
      end;
    end;
    Screen.Cursor := crHourGlass;
    saveZGN;
    if gbSaveZGN then
    begin
      gbSaveZGN := False;
      btnSave.Visible := False;
      btnGetStatusClick(nil);
    end;
    Screen.Cursor := crDefault;
  except
    on E: Exception do
    begin
       Screen.Cursor := crDefault;
       gbSaveZGN := False;
       MessageDlg('TConfigScreenFm.btnSaveClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnSaveLVClick(Sender: TObject);
var
  lslZGNparts:TStringList;
begin
  lslZGNparts:=TStringList.Create;
  try
    if not VariablesUn.IsZGN(txtBOzgn.Text) and not (VariablesUn.IsG('.', Trim(txtBOzgn.Text), lslZGNparts))then
    begin
      txtBOzgn.Color := clRed;
      if txtBOzgn.Visible then
      begin
        txtBOzgn.SetFocus;
        MessageDlg('Please enter a valid ZGN Address: n.n.n (n=0-255). Be advised �0� and �255� will act as a master address.', mtWarning, [mbOK], 0);
        exit;
      end;
    end;

    Screen.Cursor := crHourGlass;

    saveLV;

    if gbSaveLV then
    begin
      gbSaveLV := False;
      btnSaveBO.Visible := False;
      //btnBO.Caption := '>';
      btnCancelBO.Visible := False;
      //btnBO.Visible := True;
      btnGetStatusClick(nil);
    end;

    Screen.Cursor := crDefault;
  except
    on E: Exception do
    begin
       Screen.Cursor := crDefault;
       gbSaveLV := False;
       MessageDlg('TConfigScreenFm.btnSaveLVClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnSaveMasterUIDClick(Sender: TObject);
begin
  try
    if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
    begin
      giUID :=  StrToInt(GetUID(cmbUID.text));
      ComFm.SetMotorStatus(giUID, UID_MasterUID, 0, hi(StrToInt(teMasterUIDIQ2.text)), Lo(StrToInt(teMasterUIDIQ2.text)), rhMotorStatusRecieved);
      Delay(200);
      btnGetStatusClick(nil);
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnSaveMasterUIDClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnSetupClick(Sender: TObject);
begin
  try
    pnlMenuBar.Visible := False;
    AboutFm.rh_ShowForm := rhShowConfig;
    AboutFm.Show;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnSetupClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnTypeCClick(Sender: TObject);
begin
  try
    btnGetStatusClick(nil);
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnTypeCClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnTypeSClick(Sender: TObject);
var viType: Integer;
    vsCommand:String;
begin
  try
    viType := 9;
    if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
    begin
      giUID :=  StrToInt(GetUID(cmbUID.text));
      case cmbLVtype.ItemIndex of
        0:
          viType := 9;
        1:
          viType := 17;
        2:
          viType := 33;
        3:
          viType := 18;
        4:
          viType := 34;
        5:
          viType := 4;
        6:
          viType := 68;
        7:
          viType := 132;
      end;

      if (ismni)or(IsIQMLC2) then
      begin
        vsCommand:=VariablesUn.UID_BP+'.'+cmbMotorSelector.text+'.0.'+IntToStr(viType);
        ComFm.SetGenericParam(giUID,vsCommand, nil);
      end
      else
      begin
        ComFm.SetMotorStatus(giUID, VariablesUn.UID_BP, 0, 0, viType, nil);
      end;
      Delay(200);
      btnGetStatusClick(nil);
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnTypeSClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnUIDcClick(Sender: TObject);
begin
  try
    ChangeStyleList;
    gbSaveUID := False;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnUIDcClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnUIDClick(Sender: TObject);
begin
  try
    if cmbUID.Items.Count > 0 then
    begin
      if  btnUID.Caption = '>' then
      begin
        btnUID.Caption := '<';
        cmbUID.Style := csDropDown;
        cmbUID.Color := $00BBEECC;
        cmbUID.Text := LeftStr(cmbUID.Text,pos(' ',cmbUID.Text)-1);
        cmbUID.SetFocus;
      end
      else
      begin
        showUID;
      end;
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnUIDClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnUIDsClick(Sender: TObject);
var i, viHi, viLo, viNewID, viOldId: Integer;
begin
  try
    if dmDataModule.DuplicateUIDCheck(cmbUID.Text) then
    begin
      ShowMEssage('Error: UID already exist. Choose Different ID.');
      ChangeStyleList;
      gbSaveUID := False;
      exit;
    end;

//    for i := 0 to cmbUID.Items.Count - 1 do
//      begin
//        if GetUID(cmbUID.Items[i])=cmbUID.Text then
//        begin
//          MessageDlg('UID already exists!', mtWarning, [mbOk], 0);
//          ChangeStyleList;
//
//          //cmbUID.Text := VariablesUn.gsUIDCrnt;
//          gbSaveUID := False;
//          exit;
//        end;
//    end;

    if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
    begin
      viNewID := StrToInt(GetUID(cmbUID.text));
      viHi := Hi(viNewID);
      viLo := Lo(viNewID);
      giUID := StrToInt(GetUID(cmbUID.text));
      viOldId := StrToInt(VariablesUn.gsUIDCrnt);
      giSentCom := StrToInt(VariablesUn.UID_ChangeUID);
      ComFm.SetMotorStatus(viOldId, VariablesUn.UID_ChangeUID, 0, viHi, viLo, rhUIDRecieved);
    end;
  except
    on E: Exception do
    begin
      cmbUID.OnChange := cmbUIDChange;
      MessageDlg('TConfigScreenFm.btnUIDsClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnUniformCClick(Sender: TObject);
begin
  try
    btnGetStatusClick(nil);
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnUniformCClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.btnUniformSClick(Sender: TObject);
var vsCommand:String;
begin
  try
    if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
    begin
      giUID :=  StrToInt(GetUID(cmbUID.text));
      if (ismni) then
      begin
        if cmbUniform.ItemIndex=0 then
        begin
          vsCommand:=VariablesUn.UID_NU+'.0.0.'+IntToStr(VariablesUn.Clear_a_Bit(MotorInhibFlagBits,StrToInt(cmbMotorSelector.text)-1));
        end
        else
        begin
          vsCommand:=VariablesUn.UID_NU+'.0.0.'+IntToStr(VariablesUn.Set_a_Bit(MotorInhibFlagBits,StrToInt(cmbMotorSelector.text)-1));
        end;
        ComFm.SetGenericParam(giUID,vsCommand, nil);
      end
      else
      begin
        ComFm.SetMotorStatus(giUID, VariablesUn.UID_NU, 0, 0, cmbUniform.ItemIndex, nil);
      end;
      Delay(200);
      btnGetStatusClick(nil);
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.btnUniformSClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;



procedure TConfigScreenFm.btnLEDOffClick(Sender: TObject);
var
  vsCommand:String;
begin
  giUID :=  StrToInt(GetUID(cmbUID.text));
  vsCommand:=VariablesUn.UID_NU+'.0.0.0';
  ComFm.SetGenericParam(giUID,vsCommand, nil);
end;

procedure TConfigScreenFm.btnLEDOnClick(Sender: TObject);
var
  vsCommand:String;
begin
  giUID :=  StrToInt(GetUID(cmbUID.text));
  vsCommand:=VariablesUn.UID_NU+'.0.0.1';
  ComFm.SetGenericParam(giUID,vsCommand, nil);
end;

procedure TConfigScreenFm.ButtonsDisable;
begin
  try
    try
      imgWinkUp.Enabled := False;
      imgWinkUpZGN.Enabled := False;
      cmbDirection.Enabled := False;
      cmbInhibit.Enabled := False;
      cmbUniform.Enabled := False;
      cmbLed.Enabled := False;
      cmbMaint.Enabled := False;
      cmbMTAInb.Enabled := False;
      btnSave.Enabled := False;
      cmbPortMode.Enabled := False;
      cmbLVtype.Enabled := False;
      cmbFwd.Enabled := False;
      cmbBOtype.Enabled := False;
      cmbBOMode.Enabled := False;
      //btnSaveLV.Enabled := False;
      //btnGetStatus.Enabled := False;
      btnUID.Enabled := False;

      btnA1.Enabled := False;
      btnA2.Enabled := False;
      btnA3.Enabled := False;
      btnA4.Enabled := False;
      btnA5.Enabled := False;
      btnA6.Enabled := False;
      btnA7.Enabled := False;
      btnA8.Enabled := False;

      btnLV.Enabled := False;
      //btnBO.Enabled := False;

      teAutoReturnIQ2.Enabled:=False;
      teMasterUIDIQ2.Enabled:=False;

      txtA1.ClearSelection;
      btnA1.Caption := '>';
      txtA1.ReadOnly := True;
      txtA1.Color := clWhite;

      txtA2.ClearSelection;
      btnA2.Caption := '>';
      txtA2.ReadOnly := True;
      txtA2.Color := clWhite;

      txtA3.ClearSelection;
      btnA3.Caption := '>';
      txtA3.ReadOnly := True;
      txtA3.Color := clWhite;

      txtA4.ClearSelection;
      btnA4.Caption := '>';
      txtA4.ReadOnly := True;
      txtA4.Color := clWhite;

      txtA5.ClearSelection;
      btnA5.Caption := '>';
      txtA5.ReadOnly := True;
      txtA5.Color := clWhite;

      txtA6.ClearSelection;
      btnA6.Caption := '>';
      txtA6.ReadOnly := True;
      txtA6.Color := clWhite;

      txtA7.ClearSelection;
      btnA7.Caption := '>';
      txtA7.ReadOnly := True;
      txtA7.Color := clWhite;

      txtA8.ClearSelection;
      btnA8.Caption := '>';
      txtA8.ReadOnly := True;
      txtA8.Color := clWhite;

      txtLVzgn.ClearSelection;
      btnLV.Caption := '>';
      txtLVzgn.ReadOnly := True;
      txtLVzgn.Color := clWhite;

      txtBOzgn.ClearSelection;
      //btnBO.Caption := '>';
      //txtBOzgn.ReadOnly := True;
      txtBOzgn.Color := clWhite;

      btnA1.Visible := True;
      btnA2.Visible := True;
      btnA3.Visible := True;
      btnA4.Visible := True;
      btnLV.Visible := True;
      //btnBO.Visible := True;

      if VariablesUn.gbShowSTA then
      begin
        btnA5.Visible := True;
        btnA6.Visible := True;
        btnA7.Visible := True;
        btnA8.Visible := True;
      end;

      btnCancel1.Visible := False;
      btnCancel2.Visible := False;
      btnCancel3.Visible := False;
      btnCancel4.Visible := False;
      btnCancel5.Visible := False;
      btnCancel6.Visible := False;
      btnCancel7.Visible := False;
      btnCancel8.Visible := False;
      btnCancelLV.Visible := False;
      btnCancelBO.Visible := False;


      btnSave.Visible := False;
      btnSaveBO.Visible := False;

      gbSaveZGN := False;
      gbSaveLV := False;
      gbSaveUID := False;

      rhDisable;

      ChangeStyleList;

      txtA1.Text := gsA1;
      txtA2.Text := gsA2;
      txtA3.Text := gsA3;
      txtA4.Text := gsA4;
      txtA5.Text := gsA5;
      txtA6.Text := gsA6;
      txtA7.Text := gsA7;
      txtA8.Text := gsA8;

      txtLVzgn.Text := gsLV;

      txtBOzgn.Text := gsBO;


      VariablesUn.IsZGN(txtBOzgn.Text);
    finally
      rhEnable;
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.ButtonsDisable: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.ButtonsEnable;
begin
  try
    imgWinkUp.Enabled := True;
    imgWinkUpZGN.Enabled := True;
    cmbDirection.Enabled := True;
    cmbInhibit.Enabled := True;
    cmbUniform.Enabled := True;
    cmbLed.Enabled := True;
    cmbMaint.Enabled := True;
    cmbMTAInb.Enabled := True;
    btnSave.Enabled := True;
    cmbPortMode.Enabled := True;
    cmbLVtype.Enabled := True;
    cmbFwd.Enabled := True;
    cmbBOtype.Enabled := True;
    cmbBOMode.Enabled := True;
    //btnSaveLV.Enabled := True;
    btnUID.Enabled := True;
    btnA1.Enabled := True;
    btnA2.Enabled := True;
    btnA3.Enabled := True;
    btnA4.Enabled := True;
    btnA5.Enabled := True;
    btnA6.Enabled := True;
    btnA7.Enabled := True;
    btnA8.Enabled := True;
    btnLV.Enabled := True;
    //btnBO.Enabled := True;

    teAutoReturnIQ2.Enabled:=true;
    teMasterUIDIQ2.Enabled:=true;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.ButtonsEnable: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;


procedure TConfigScreenFm.cbSWEnabledClick(Sender: TObject);
begin
  try
    btnEnabledS.Visible:=true;
    btnEnabledC.Visible:=true;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.cbSWEnabledClick: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.ChangeStyleList;
var I,viIndex,ColonPos: Integer;
begin
  try
    try
      cmbUID.Style := csDropDownList;
      cmbUID.Color := clWhite;
      btnUIDs.Visible := False;
      btnUIDc.Visible := False;
      btnUID.Visible := True;
      btnUID.Caption := '>';

      cmbUID.OnChange := nil;

      if cmbUID.Items.Count > 0 then
      begin
        cmbUID.Enabled := True;
        //viIndex := cmbUID.Items.IndexOf(VariablesUn.gsUIDCrnt);
        for I := 0 to cmbUID.Items.Count-1 do
        begin
          ColonPos:= Pos(':',cmbUID.Items[I]);
          if Pos(VariablesUn.gsUIDCrnt,LeftStr(cmbUID.Items[I],ColonPos))>0 then
          begin
            cmbUID.ItemIndex := I;
            break;
          end
          else
          begin
            cmbUID.ItemIndex := 0;
          end;
        end;
  //      if viIndex >= 0 then
  //        cmbUID.ItemIndex := viIndex
  //      else
  //        cmbUID.ItemIndex := 0;
      end;

      gbSaveUID := False;
      ActiveControl := nil;
    finally
      cmbUID.OnChange := cmbUIDChange;
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.ChangeStyleList: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.ClearScreen;
begin
  try
    try
      lblMtrType.Caption := '';
      txtRevision.Text := '';
      lblSystem.Caption := '';
      cmbDirection.ItemIndex := -1;
      cmbInhibit.ItemIndex := -1;
      cmbUniform.ItemIndex := -1;
      cmbLed.ItemIndex := -1;
      cmbMaint.ItemIndex := -1;
      cmbMTAInb.ItemIndex := -1;
      cmbPortMode.ItemIndex := -1;
      cmbLVtype.ItemIndex := -1;
      cmbFwd.ItemIndex := -1;
      cmbBOtype.ItemIndex := -1;
      cmbBOMode.ItemIndex := -1;
      txtRevision.font.Color := clBlack;
      txtA1.font.Color := clBlack;
      txtA2.font.Color := clBlack;
      txtA3.font.Color := clBlack;
      txtA4.font.Color := clBlack;
      txtA5.font.Color := clBlack;
      txtA6.font.Color := clBlack;
      txtA7.font.Color := clBlack;
      txtA8.font.Color := clBlack;
      txtLVzgn.font.Color := clBlack;
      txtBOzgn.font.Color := clBlack;
      teAutoReturnIQ2.Font.Color:=clblack;
      teMasterUIDIQ2.font.color:=clblack;
      rhDisable;
      ChangeStyleList;

      if not gbSaveZGN then
      begin
        txtA1.Text := '';
        btnA1.Caption := '>';
        btnA1.Visible := True;
        btnCancel1.Visible := False;
        txtA1.ReadOnly := True;
        if not(txtA1.Color = $00A4FFFF) then
        begin
          txtA1.Color := clWhite;
        end;

        txtA2.Text := '';
        btnA2.Caption := '>';
        btnA2.Visible := True;
        btnCancel2.Visible := False;
        txtA2.ReadOnly := True;
        if not (txtA2.Color = $00A4FFFF) then
        begin
          txtA2.Color := clWhite;
        end;

        txtA3.Text := '';
        btnA3.Caption := '>';
        btnA3.Visible := True;
        btnCancel3.Visible := False;
        txtA3.ReadOnly := True;
        if not (txtA3.Color = $00A4FFFF) then
        begin
          txtA3.Color := clWhite;
        end;

        txtA4.Text := '';
        btnA4.Caption := '>';
        btnA4.Visible := True;
        btnCancel4.Visible := False;
        txtA4.ReadOnly := True;
        if not (txtA4.Color = $00A4FFFF) then
        begin
          txtA4.Color := clWhite;
        end;

        txtA5.Text := '';
        btnA5.Caption := '>';
        if VariablesUn.gbShowSTA then
          btnA5.Visible := True;

        btnCancel5.Visible := False;
        txtA5.ReadOnly := True;
        if not(txtA5.Color = $00A4FFFF) then
        begin
          txtA5.Color := clWhite;
        end;

        txtA6.Text := '';
        btnA6.Caption := '>';
        if VariablesUn.gbShowSTA then
          btnA6.Visible := True;

        btnCancel6.Visible := False;
        txtA6.ReadOnly := True;
        if not (txtA6.Color = $00A4FFFF) then
        begin
          txtA6.Color := clWhite;
        end;

        txtA7.Text := '';
        btnA7.Caption := '>';
        if VariablesUn.gbShowSTA then
          btnA7.Visible := True;

        btnCancel7.Visible := False;
        txtA7.ReadOnly := True;
        if not (txtA7.Color = $00A4FFFF) then
        begin
          txtA7.Color := clWhite;
        end;

        txtA8.Text := '';
        btnA8.Caption := '>';
        if VariablesUn.gbShowSTA then
          btnA8.Visible := True;

        btnCancel8.Visible := False;
        txtA8.ReadOnly := True;
        if not (txtA8.Color = $00A4FFFF) then
        begin
          txtA8.Color := clWhite;
        end;
        txtLVzgn.Text := '';
        btnLV.Caption := '>';
        btnLV.Visible := True;
        btnCancelLV.Visible := False;
        txtLVzgn.ReadOnly := True;
        if not(txtLVzgn.Color = $00A4FFFF) then
        begin
          txtLVzgn.Color := clWhite;
        end;
        teAutoReturnIQ2.Color := clWhite;
        btnAutoreturnS.Visible:=false;
        btnAutoreturnC.Visible:=false;
        btnsaveMasterUID.visible:=false;
        btnCancelMAsterUID.visible:=false;

      end;

      if not gbSaveLV then
      begin
        txtBOzgn.Text := '';
        //btnBO.Caption := '>';
        //btnBO.Visible := True;
        btnCancelBO.Visible := False;
        //txtBOzgn.ReadOnly := True;
        if not (txtBOzgn.Color = $00A4FFFF) then
        begin
          txtBOzgn.Color := clWhite;
        end;
      end;

    finally
      rhEnable;
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.ClearScreen: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.cmbBOtypeChange(Sender: TObject);
begin
  try
    btnBOs.Visible := True;
    btnBOc.Visible := True;
    ActiveControl := nil;
    sbMain.SetFocus;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.cmbBOtypeChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.cmbDCMotorsChange(Sender: TObject);
begin
  try
    btnMotorPersonS.Visible:=true;
    btnMotorPersonc.Visible:=true;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.cmbDCMotorsChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.cmbDeviceSelectorChange(Sender: TObject);
begin
  try
    if(cmbDeviceSelector.Text='MWC')then
    begin
      DataUn.dmDataModule.dsMWCChannels.Filtered := False;
      DataUn.dmDataModule.dsMWCChannels.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
      DataUn.dmDataModule.dsMWCChannels.Filtered := True;

      if not dmDataModule.dsMWCChannels.Eof then
      begin

        MWCConfigfm.rh_ShowMain := rh_ShowMain;
        MWCDisplayfm.rh_ShowMain := rh_ShowMain;
        Hide;
        MWCDisplayfm.show;

        //MWCfm.Show;
      end
      else
      begin
        ShowMessage('No MWC Devices Found');
        cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('MTR');
      end;
    end
    else if (cmbDeviceSelector.Text='iQ3-DC-RF') then
    begin
      DataUn.dmDataModule.dsIQ3s.Filtered := False;
      DataUn.dmDataModule.dsIQ3s.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
      DataUn.dmDataModule.dsIQ3s.Filtered := True;

      if not dmDataModule.dsIQ3s.Eof then
      begin

        VariablesUn.giTop:=top;
        variablesUn.giLeft:=Left;
        IQ3RFDisplayfm.Show;
        Hide;
        //MWCConfigfm.Show;
      end
      else
      begin
        ShowMessage('No iQ3 Devices Found');
        cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('MTR');
      end;
    end
    else if(cmbDeviceSelector.Text='MNI')then
    begin
      DataUn.dmDataModule.dsMNI.Filtered := False;
      DataUn.dmDataModule.dsMNI.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
      DataUn.dmDataModule.dsMNI.Filtered := True;
      if not DataUn.dmDataModule.dsMNI.Eof then
      begin
        Hide;
        MNIConfigFm.Show;
        VariablesUn.giTop := Top;
        VariablesUn.giLeft := Left;
      end
      else
      begin
        ShowMessage('No MNI Devices Found');
        cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('MTR');
      end;
    end
    else if(cmbDeviceSelector.Text='MDC')then
    begin
      DataUn.dmDataModule.dsMNI2.Filtered := False;
      DataUn.dmDataModule.dsMNI2.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
      DataUn.dmDataModule.dsMNI2.Filtered := True;

      if not dmDataModule.dsMNI2.Eof then
      begin
        Hide;
        MNI2configFm.Show;
        VariablesUn.giTop := Top;
        VariablesUn.giLeft := Left;
        MNI2ConfigFm.GetVirtualMotorsStatus(self);
      end
      else
      begin
        ShowMessage('No MDC Devices Found');
        cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('MTR');
      end;
    end
    else if(cmbDeviceSelector.Text='IQMLC2')then
    begin
      DataUn.dmDataModule.dsIQMLC2.Filtered := False;
      DataUn.dmDataModule.dsIQMLC2.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
      DataUn.dmDataModule.dsIQMLC2.Filtered := True;

      if not dmDataModule.dsIQMLC2.Eof then
      begin
        Hide;
        VariablesUn.giTop := Top;
        VariablesUn.giLeft := Left;
        IQMLC2configFm.Show;
      end
      else
      begin
        ShowMessage('No IQMLC2 Devices Found');
        cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('MTR');
      end;
    end
    else if(cmbDeviceSelector.Text='PowerPanel')then
    begin
      DataUn.dmDataModule.dsPowerPanel.Filtered := False;
      DataUn.dmDataModule.dsPowerPanel.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName);
      DataUn.dmDataModule.dsPowerPanel.Filtered := True;

      if not dmDataModule.dsPowerPanel.Eof then
      begin
        Hide;
        VariablesUn.giTop := Top;
        VariablesUn.giLeft := Left;
        PowerPanelConfigFm.Show;
      end
      else
      begin
        ShowMessage('No IQ2-DC Power Panel Devices Found');
        cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('MTR');
      end;
    end
    else if(cmbDeviceSelector.Text='Motor Maint')then
    begin
      DataUn.dmDataModule.dsMotors.Filtered := False;
      DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) + ' and MotorType = 69 or MotorType = 66';
      DataUn.dmDataModule.dsMotors.Filtered := True;

      if not dmDataModule.dsMotors.Eof then
      begin
        VariablesUn.giTop := Top;
        VariablesUn.giLeft := Left;
        while not dmDataModule.dsMotors.Eof do
        begin
          if GetUID(cmbUID.Text)=IntToStr(DataUn.dmDataModule.dsMotors.FieldByName('UID').AsInteger) then
          begin
            Hide;
            MotorMaintenanceDisplayFm.Show;
            MotorMaintenanceDisplayFm.cmbUidMM.ItemIndex:= MotorMaintenanceDisplayFm.cmbUidMM.Items.IndexOf(GetUID(cmbUID.Text));
            MotorMaintenanceDisplayFm.cmbUidMM.OnChange(self);
            exit;
          end;
          dmDataModule.dsMotors.Next;
        end;
        Hide;
        MotorMaintenanceDisplayFm.Show;
        MotorMaintenanceDisplayFm.cmbUidMM.ItemIndex:= 0;
        MotorMaintenanceDisplayFm.cmbUidMM.OnChange(self);
      end
      else
      begin
        ShowMessage('No Motors Found');
        cmbDeviceSelector.ItemIndex:= cmbDeviceSelector.Items.IndexOf('MTR');
      end;
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.cmbDeviceSelectorChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.cmbDirectionChange(Sender: TObject);
begin
  try
    btnDirectionS.Visible := True;
    btnDirectionC.Visible := True;
    ActiveControl := nil;
    sbMain.SetFocus;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.cmbDirectionChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.cmbFwdChange(Sender: TObject);
begin
  try
    btnFwdS.Visible := True;
    btnFwdC.Visible := True;
    ActiveControl := nil;
    sbMain.SetFocus;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.cmbFwdChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.cmbInhibitChange(Sender: TObject);
begin
  try
    btnInhibitS.Visible := True;
    btnInhibitC.Visible := True;
    ActiveControl := nil;
    sbMain.SetFocus;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.cmbInhibitChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.cmbLedChange(Sender: TObject);
begin
  try
    btnLedS.Visible := True;
    btnLedC.Visible := True;
    ActiveControl := nil;
    sbMain.SetFocus;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.cmbLedChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.cmbLVtypeChange(Sender: TObject);
begin
  try
    btnTypeS.Visible := True;
    btnTypeC.Visible := True;
    ActiveControl := nil;
    sbMain.SetFocus;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.cmbLVtypeChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.cmbMaintChange(Sender: TObject);
begin
  try
    btnMaintS.Visible := True;
    btnMaintC.Visible := True;
    ActiveControl := nil;
    sbMain.SetFocus;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.cmbMaintChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.cmbMotorSelectorChange(Sender: TObject);
var
  ScrollPos:integer;
begin
  try

    MotorChangeFlg:=true;
    //ScrollPos:=SbMain.VertScrollBar.Position;
    //SbMain.VertScrollBar.Position:=0;
    cmbUIDChange(NIL);
    //SbMain.VertScrollBar.Position:=ScrollPos;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.cmbMotorSelectorChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.cmbMTAInbChange(Sender: TObject);
begin
  try
    btnMTAs.Visible := True;
    btnMTAc.Visible := True;
    ActiveControl := nil;
    sbMain.SetFocus;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.cmbMTAInbChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.cmbPortModeChange(Sender: TObject);
begin
  try
    btnPortS.Visible := True;
    btnPortC.Visible := True;
    ActiveControl := nil;
    sbMain.SetFocus;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.cmbPortModeChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.cmbType1Change(Sender: TObject);
begin
  try
    ActiveControl := nil;
    sbMain.SetFocus;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.cmbType1Change: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.cmbTypeLV1Change(Sender: TObject);
begin
  try
    ActiveControl := nil;
    sbMain.SetFocus;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.cmbTypeLV1Change: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.cmbUIDChange(Sender: TObject);
begin
  try
    outputdebugstring(pchar('Change TOP '+inttostr(sbmain.VertScrollBar.Position)));
    if (cmbUID.ItemIndex <> -1) then
    begin
      cmbUID.Style := csDropDownList;
      cmbUID.Color := clWhite;
      btnUIDs.Visible := False;
      btnUIDc.Visible := False;
      btnUID.Visible := True;
      btnUID.Caption := '>';
      gbSaveUID := False;
      ActiveControl := nil;
      cbSwEnabled.OnClick:=nil;


      if VariablesUn.IsNumber(GetUID(cmbUID.text)) then
      begin
        VariablesUn.gsUIDCrnt := GetUID(cmbUID.text);
        DataUn.dmDataModule.dsMotors.Filtered := False;
        DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(VariablesUn.gsProjectName) +  ' and UID = ' + VariablesUn.gsUIDCrnt;
        DataUn.dmDataModule.dsMotors.Filtered := True;
        if not DataUn.dmDataModule.dsMotors.Eof then
        begin
          if DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='74' then
          begin
            cmbMotorSelector.Visible:=true;

            lblMotorNumber.Visible:=true;
            cmbDCMotors.visible:=true;
          end
          else if DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='65' then
          begin
            cmbMotorSelector.Visible:=true;
            lblMotorNumber.Visible:=true;
            cmbDCMotors.visible:=false;
          end
          else if (DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='77') or (DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='205') then//st50dc
          begin
            DCSpeedUpLimit:=25;   //Limit speed input for DC motors
            DCSpeedLowerLimit:=10;
            cmbMotorSelector.Visible:=false;
            cmbDCMotors.Visible:=false;
            lblMotorNumber.Visible:=false;
          end
          else if (DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='79')  or (DataUn.dmDataModule.dsMotors.FieldByName('MotorType').AsString='207') then//st30
          begin
            DCSpeedUpLimit:=28;
            DCSpeedLowerLimit:=6;
            cmbMotorSelector.Visible:=false;
            cmbDCMotors.Visible:=false;
            lblMotorNumber.Visible:=false;
          end
          else
          begin
            cmbMotorSelector.Visible:=false;
            cmbDCMotors.visible:=false;
            cmbMotorSelector.ItemIndex:=0;
            lblMotorNumber.Visible:=false;
          end;
        end;
        GetAlias;
        btnGetStatusClick(Sender);
      end;
      cbSwEnabled.OnClick:=cbSWEnabledClick;
    end
    else
    begin
      If (VariablesUn.IsNumber(GetUID(cmbUID.text))) and ((StrToInt(GetUID(cmbUID.text)) > 0) and (StrToInt(GetUID(cmbUID.text)) < 10000)) then
      begin
        btnUID.Visible := False;
        btnUIDs.Visible := True;
        btnUIDc.Visible := True;
        cmbUID.Color := $00A4FFFF;
        gbSaveUID := True;
      end
      else
      begin
        if not (cmbUID.Text = '') then
        begin
          MessageDlg('Please enter a valid UID (1 - 9999).', mtWarning, [mbOK], 0);
          ChangeStyleList;
          {
          if VariablesUn.IsNumber(VariablesUn.gsUIDCrnt) then
            cmbUID.Text := VariablesUn.gsUIDCrnt
          else
            cmbUID.Text := '-1';
          }
          gbSaveUID := False;
        end
        else
        begin
          gbSaveUID := False;
          btnUIDs.Visible := False;
          btnUIDc.Visible := False;
        end;
      end;
    end;
    outputdebugstring(pchar('Change Bottom '+inttostr(sbmain.VertScrollBar.Position)));
//    if isDC then
//    begin
//      SendToDCMotors(60);
//    end
//    else
//    begin
//      SendToDCMotors(0);
//    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.cmbUIDChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.cmbUIDExit(Sender: TObject);
begin
  try
    if cmbUID.Text = '' then
    begin
      ChangeStyleList;
    end;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.cmbUIDExit: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.cmbUniformChange(Sender: TObject);
begin
  try
    btnUniformS.Visible := True;
    btnUniformC.Visible := True;
    ActiveControl := nil;
    sbMain.SetFocus;
  except
    on E: Exception do
    begin
      MessageDlg('TConfigScreenFm.cmbUniformChange: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TConfigScreenFm.DisplayParameters;
var i, viIndex: Integer;
    vsValue: String;
begin
  try
    viIndex := cmbUID.ItemIndex;
    if viIndex = -1 then
    begin
      for i := 0 to cmbUID.Items.Count - 1 do
      begin
        if pos(gsUIDCrnt,cmbUID.Items[i])>0  then
        begin
          viIndex := i;
        end;
      end;
    end;
    ClearScreen;
    btnUID.Enabled := False;

    if (isMNI)or(isIQMLC2) then
      begin
//        Label18.Visible:=True;
//        cbSWEnabled.Visible:=True;
        pnlLV.Visible:=True;
        cmbPortMode.Visible:=True;
        Label23.Visible:=True;
        Label24.Visible:=True;
        cmbLVtype.Visible:=True;
        cmbFwd.Visible:=True;
        Label25.Visible:=True;
        txtRevision.Visible:=True;
        label3.Visible:=True;
        cmbLed.Visible:=True;
        lblUniform.Visible:=True;
        label21.Visible:=True;
        Label19.Visible:=False;
        teHostUID.Visible:=False;
        //add uniform objects
        lblUniform.Visible:=True;
        cmbUniform.Visible:=True;
//        btnUniformS.Visible:=True;
//        btnUniformC.Visible:=True;

        if isIQMLC2 then
        begin
          pnlBO.Visible:=false;
        end
        else
        begin
          pnlBO.Visible:=true;
        end;
        if not isIQMLC2Old then //skip if old IQMLC2 use setup command
        begin          //JH IQMLC2
//          VariablesUn.ClearData;
//          giSentCom := StrToInt(VariablesUn.UID_LSE);  //get Enabled Switches
//          ComFm.GetStatus(giUID, VariablesUn.UID_LSE, rhMotorStatusRecieved);
//          WaitforResponse(giUID, VariablesUn.UID_LSE, rhMotorStatusRecieved,100);
          lblMode.Visible:=true;
          label4.Visible:=true;
          lblSystem.Visible:=true;
          label5.Visible:=true;
          cmbled.Visible:=true;
          lblUniform.Visible:=true;
        end
        else
        begin
          cmbled.Visible:=false;
          lblUniform.Visible:=false;
          lblMode.Visible:=false;
          label4.Visible:=false;
          lblSystem.Visible:=false;
          label5.Visible:=false;
        end;

        if flgHaltStatus then
        begin
          flgHaltStatus:=false;
          Exit;
        end;

        //VariablesUn.delay(100);
        btnEnabledS.Visible:=False;
        btnEnabledC.Visible:=False;
        pnlDCSpeed.Visible:=False;
        pnlFirmware.visible:=True;
        pnlSwitch.Visible:=true;
//        if VariablesUn.gbShowSTA then
//        begin
//          pnlLV.Top:= pnlTopZGN.Height+pnlBotZGN.Height-SbMain.VertScrollBar.Position;
//          pnlProgramWink.Top:= pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height-SbMain.VertScrollBar.Position;
//          pnlSettings.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height-SbMain.VertScrollBar.Position;
//          pnlFirmware.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height-SbMain.VertScrollBar.Position;;
//          pnlSwitch.top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlFirmware.Height-SbMain.VertScrollBar.Position;;
//          if not isIQMLC2 then
//          begin
//            pnlBO.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlFirmware.Height+pnlSwitch.Height-SbMain.VertScrollBar.Position;
//          end;
//        end
//        else
//        begin
//
//          pnlLV.Top:= pnlTopZGN.Height-SbMain.VertScrollBar.Position;
//          pnlProgramWink.Top:= pnlTopZGN.Height+pnlLV.Height-SbMain.VertScrollBar.Position;
//          pnlSettings.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height-SbMain.VertScrollBar.Position;
//          pnlFirmware.Top:= pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height-SbMain.VertScrollBar.Position;
//          pnlSwitch.top:= pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlFirmware.Height-SbMain.VertScrollBar.Position;
//          if not isIQMLC2 then
//          begin
//            pnlBO.Top:= pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlFirmware.Height+pnlSwitch.Height-SbMain.VertScrollBar.Position;
//          end;
//        end;
      end
      else if isDC then
      begin
        //Remove uniform objects
        cmbled.Visible:=true;
        lblUniform.Visible:=true;
        lblSystem.Visible:=true;
        label5.Visible:=true;
        lblMode.Visible:=true;
        label4.Visible:=true;
        lblUniform.Visible:=False;
        cmbUniform.Visible:=False;
        btnUniformS.Visible:=False;
        btnUniformC.Visible:=False;
        pnlBO.Visible:=true;
        txtRevision.Visible:=False;
        label3.Visible:=False;
        label21.Visible:=False;

        btnLedC.Visible:=False;
        btnLedS.Visible:=False;
        cmbLed.Visible:=False;
        lblUniform.Visible:=False;

        Label19.Visible:=True;
        teHostUID.Visible:=True;
        if not is50AC then
        begin
          pnlDCSpeed.Visible:=true;
          if isst30 then
          begin
            //remove speed down objects
            teDNSpeedFine.visible:=False;
            Label35.visible:=False;
            teDNSpeed.visible:=False;
            Label30.visible:=False;
            //remove ramp rate
            Label27.visible:=False;
            teRampPeriodUP.visible:=False;
            Label31.visible:=False;
            teRampPeriodDn.visible:=False;
          end
          else
          begin
            //Add speed down objects
            teDNSpeedFine.visible:=True;
            Label35.visible:=True;
            teDNSpeed.visible:=True;
            Label30.visible:=True;
            //add ramp rate
            Label27.visible:=True;
            teRampPeriodUP.visible:=True;
            Label31.visible:=True;
            teRampPeriodDn.visible:=True;
          end;
        end
        else
        begin
          pnlDCSpeed.Visible:=False;
        end;
        pnlFirmware.visible:=False;
        pnlMasterUID.Visible:=false;
        if isSW then
        begin
          pnlLV.Visible:=True;
          pnlSwitch.Visible:=true;
        end
        else
        begin
          pnlSwitch.Visible:=false;
          pnlLV.Visible:=False;
        end;
        //Align Panels
//        if VariablesUn.gbShowSTA then
//        begin
//          pnlLV.Top:= pnlTopZGN.Height+pnlBotZGN.Height-SbMain.VertScrollBar.Position;
//          pnlProgramWink.Top:= pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height-SbMain.VertScrollBar.Position;
//          pnlSettings.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height-SbMain.VertScrollBar.Position;
//          if not is50AC then
//          begin
//            pnlDCSpeed.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height-SbMain.VertScrollBar.Position;
//            if isSW then
//            begin
//              pnlSwitch.top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlDCSpeed.Height-SbMain.VertScrollBar.Position;
//              pnlBO.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlDCSpeed.Height+pnlSwitch.Height-SbMain.VertScrollBar.Position;
//              pnlReturnToAuto.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlDCSpeed.Height+pnlSwitch.Height+pnlBO.Height-SbMain.VertScrollBar.Position;
//            end
//            else
//            begin
//              pnlBO.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlDCSpeed.Height-SbMain.VertScrollBar.Position;
//              pnlReturnToAuto.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlDCSpeed.Height+pnlBO.Height-SbMain.VertScrollBar.Position;
//            end;
//          end
//          else
//          begin
//            //pnlDCSpeed.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height-SbMain.VertScrollBar.Position;
//            if isSW then
//            begin
//              pnlSwitch.top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height-SbMain.VertScrollBar.Position;
//              pnlBO.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlSwitch.Height-SbMain.VertScrollBar.Position;
//              pnlReturnToAuto.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlSwitch.Height+pnlBO.Height-SbMain.VertScrollBar.Position;
//            end
//            else
//            begin
//              pnlBO.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+-SbMain.VertScrollBar.Position;
//              pnlReturnToAuto.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlBO.Height-SbMain.VertScrollBar.Position;
//            end;
//          end;

//          pnlBO.Top:=670+pnlBotZGN.Height;
//          pnlDCSpeed.Top:=679;
//        end
//        else
//        begin
//          pnlLV.Top:= pnlTopZGN.Height-SbMain.VertScrollBar.Position;
//          pnlProgramWink.Top:= pnlTopZGN.Height+pnlLV.Height-SbMain.VertScrollBar.Position;
//          pnlSettings.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height-SbMain.VertScrollBar.Position;
//          if not is50AC then
//          begin
//            pnlDCSpeed.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height-SbMain.VertScrollBar.Position;
//            if isSW then
//            begin
//              pnlSwitch.top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlDCSpeed.Height-SbMain.VertScrollBar.Position;
//              pnlBO.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlDCSpeed.Height+pnlSwitch.Height-SbMain.VertScrollBar.Position;
//              //pnlReturnToAuto.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlDCSpeed.Height+pnlSwitch.Height+pnlBO.Height-SbMain.VertScrollBar.Position;
//              pnlReturnToAuto.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlDCSpeed.Height+pnlSwitch.Height+pnlBO.Height-SbMain.VertScrollBar.Position;
//            end
//            else
//            begin
//              pnlBO.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlDCSpeed.Height-SbMain.VertScrollBar.Position;
//              pnlReturnToAuto.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlDCSpeed.Height+pnlBO.Height-SbMain.VertScrollBar.Position;
//            end;
//          end
//          else
//          begin
//            //pnlDCSpeed.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height-SbMain.VertScrollBar.Position;
//            if isSW then
//            begin
//              pnlSwitch.top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height-SbMain.VertScrollBar.Position;
//              pnlBO.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlSwitch.Height-SbMain.VertScrollBar.Position;
//              //pnlReturnToAuto.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlDCSpeed.Height+pnlSwitch.Height+pnlBO.Height-SbMain.VertScrollBar.Position;
//              pnlReturnToAuto.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlSwitch.Height+pnlBO.Height-SbMain.VertScrollBar.Position;
//            end
//            else
//            begin
//              pnlBO.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height-SbMain.VertScrollBar.Position;
//              pnlReturnToAuto.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlBO.Height-SbMain.VertScrollBar.Position;
//            end;
//          end;
////          pnlBO.Top:=670;
//        end;
      end
      else
      begin
        cmbled.Visible:=true;
        lblUniform.Visible:=true;
        lblSystem.Visible:=true;
        label5.Visible:=true;
        lblMode.Visible:=true;
        label4.Visible:=true;
        btnEnabledS.Visible:=False;
        btnEnabledC.Visible:=False;
//        if(StrToFloat(VariablesUn.BuildFirmwareRev))<7.7 then
//        begin
//          Label18.Visible:=False;
//          cbSWEnabled.Visible:=False;
//
//
//        end
//        else
//        begin
//          Label18.Visible:=True;
//          cbSWEnabled.Visible:=True;
//          VariablesUn.ClearData;
//          giSentCom := StrToInt(VariablesUn.UID_LSE);  //get Enabled Switches
//          ComFm.GetStatus(giUID, VariablesUn.UID_LSE, rhMotorStatusRecieved);
//          WaitforResponse(giUID, VariablesUn.UID_LSE, rhMotorStatusRecieved,100);
//          if flgHaltStatus then
//          begin
//            flgHaltStatus:=false;
//            Exit;
//          end;
//          //VariablesUn.delay(100);
//        end;
        txtLVzgn.Visible:=True;
        cmbPortMode.Visible:=True;
        Label23.Visible:=True;
        Label24.Visible:=True;
        cmbLVtype.Visible:=True;
        cmbFwd.Visible:=True;
        Label25.Visible:=True;
        txtRevision.Visible:=True;
        label3.Visible:=True;
        cmbLed.Visible:=True;
        lblUniform.Visible:=True;
        label21.Visible:=True;
        Label19.Visible:=False;
        teHostUID.Visible:=False;
        pnlDCSpeed.Visible:=False;
        //
        pnlSettings.Visible:=True;
        pnlProgramWink.Visible:=true;
        //
        pnlFirmware.visible:=True;
        pnlSwitch.Visible:=true;
//        if VariablesUn.gbShowSTA then
//        begin
//          pnlLV.Top:= pnlTopZGN.Height+pnlBotZGN.Height-SbMain.VertScrollBar.Position;
//          pnlProgramWink.Top:= pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height-SbMain.VertScrollBar.Position;
//          pnlSettings.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height-SbMain.VertScrollBar.Position;
//          pnlFirmware.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height-SbMain.VertScrollBar.Position;;
//          pnlSwitch.top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlFirmware.Height-SbMain.VertScrollBar.Position;;
//          pnlBO.Top:=pnlTopZGN.Height+pnlBotZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlFirmware.Height+pnlSwitch.Height-SbMain.VertScrollBar.Position;
//        end
//        else
//        begin
//
//          pnlLV.Top:= pnlTopZGN.Height-SbMain.VertScrollBar.Position;
//          pnlProgramWink.Top:= pnlTopZGN.Height+pnlLV.Height-SbMain.VertScrollBar.Position;
//          pnlSettings.Top:=pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height-SbMain.VertScrollBar.Position;
//          pnlFirmware.Top:= pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height-SbMain.VertScrollBar.Position;
//          pnlSwitch.top:= pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlFirmware.Height-SbMain.VertScrollBar.Position;
//          pnlBO.Top:= pnlTopZGN.Height+pnlLV.Height+pnlProgramWink.Height+pnlSettings.Height+pnlFirmware.Height+pnlSwitch.Height-SbMain.VertScrollBar.Position;
////          pnlFirmware.Top:=548;
////          pnlSwitch.top:=582;
////          pnlBO.Top:=768;
//        end;
      end;

    if not (DataUn.dmDataModule.dsMotors.State = dsInactive) and (VariablesUn.IsNumber(GetUID(cmbUID.text))) then
    begin
      if isIQMLC2 or isMNI then
      begin
        DataUn.dmDataModule.dsMotors.Filtered := False;
        DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text) +' and MotorNumber = 1';
        DataUn.dmDataModule.dsMotors.Filtered := True;
      end
      else
      begin
        DataUn.dmDataModule.dsMotors.Filtered := False;
        DataUn.dmDataModule.dsMotors.Filter := 'ProjectName = ' + QuotedStr(lblProjectName.Caption) +  ' and UID = ' + GetUID(cmbUID.text);
        DataUn.dmDataModule.dsMotors.Filtered := True;
      end;

      If not DataUn.dmDataModule.dsMotors.Eof then
      begin
        rhDisable;
        //ClearScreen;
        ChangeStyleList;
        cmbUID.ItemIndex := viIndex;
        ButtonsDisable;
        ActiveControl := nil;

        rhDisable;

        vsValue := DataUn.dmDataModule.dsMotors.FieldByName('Address5').AsString;
        //VariablesUn.IsZGN(vsValue);
        //txtG1.Text := IntToStr(VariablesUn.giG);
        //jh
        //txtA1.Text := vsValue;
        txtA1.Text := ConvertZGN(vsValue);
        txtA1.font.Color := clRed;
        //txtG1.font.Color := clred;

        vsValue := DataUn.dmDataModule.dsMotors.FieldByName('Address6').AsString;
        //VariablesUn.IsZGN(vsValue);
        //txtG2.Text := IntToStr(VariablesUn.giG);
        txtA2.Text := ConvertZGN(vsValue);
        txtA2.font.Color := clRed;
        //txtG2.font.Color := clred;

        vsValue := DataUn.dmDataModule.dsMotors.FieldByName('Address7').AsString;
        //VariablesUn.IsZGN(vsValue);
        //txtG3.Text := IntToStr(VariablesUn.giG);
        txtA3.Text := ConvertZGN(vsValue);
        txtA3.font.Color := clRed;
        //txtG3.font.Color := clred;

        vsValue := DataUn.dmDataModule.dsMotors.FieldByName('Address8').AsString;
        //VariablesUn.IsZGN(vsValue);
        //txtG4.Text := IntToStr(VariablesUn.giG);
        txtA4.Text := ConvertZGN(vsValue);
        txtA4.font.Color := clRed;
        //txtG4.font.Color := clred;

        vsValue := DataUn.dmDataModule.dsMotors.FieldByName('Address1').AsString;
        //VariablesUn.IsZGN(vsValue);
        //txtG5.Text := IntToStr(VariablesUn.giG);
        txtA5.Text := ConvertZGN(vsValue);
        txtA5.font.Color := clRed;
        //txtG5.font.Color := clred;

        vsValue := DataUn.dmDataModule.dsMotors.FieldByName('Address2').AsString;
        //VariablesUn.IsZGN(vsValue);
        //txtG6.Text := IntToStr(VariablesUn.giG);
        txtA6.Text := ConvertZGN(vsValue);
        txtA6.font.Color := clRed;
        //txtG6.font.Color := clred;

        vsValue := DataUn.dmDataModule.dsMotors.FieldByName('Address3').AsString;
        //VariablesUn.IsZGN(vsValue);
        //txtG7.Text := IntToStr(VariablesUn.giG);
        txtA7.Text := ConvertZGN(vsValue);
        txtA7.font.Color := clRed;
        //txtG7.font.Color := clred;

        vsValue := DataUn.dmDataModule.dsMotors.FieldByName('Address4').AsString;
        //VariablesUn.IsZGN(vsValue);
        //txtG8.Text := IntToStr(VariablesUn.giG);
        txtA8.Text := ConvertZGN(vsValue);
        txtA8.font.Color := clRed;
        //txtG8.font.Color := clred;

        vsValue := DataUn.dmDataModule.dsMotors.FieldByName('LVPort').AsString;
        //VariablesUn.IsZGN(vsValue);
        //txtLVg.Text := IntToStr(VariablesUn.giG);
        txtLVzgn.Text := ConvertZGN(vsValue);
        txtLVzgn.font.Color := clRed;
        //txtLVg.font.Color := clred;
        if not isIQMLC2 then
        begin
          vsValue := DataUn.dmDataModule.dsMotors.FieldByName('BlackOut').AsString;
          //VariablesUn.IsZGN(vsValue);
          //txtBOg.Text := IntToStr(VariablesUn.giG);
          txtBOzgn.Text := ConvertZGN(vsValue);
          txtBOzgn.font.Color := clRed;
          //txtBOg.font.Color := clred;
        end;

        vsValue := DataUn.dmDataModule.dsMotors.FieldByName('Direction').AsString;
        for i := 0 to cmbDirection.Items.Count - 1 do
        begin
          if cmbDirection.Items[i] = vsValue then
            cmbDirection.ItemIndex := i;
        end;

        vsValue := DataUn.dmDataModule.dsMotors.FieldByName('Inhibit').AsString;
        for i := 0 to cmbInhibit.Items.Count - 1 do
        begin
          if cmbInhibit.Items[i] = vsValue then
            cmbInhibit.ItemIndex := i;
        end;

        vsValue := DataUn.dmDataModule.dsMotors.FieldByName('Start').AsString;
        for i := 0 to cmbUniform.Items.Count - 1 do
        begin
          if cmbUniform.Items[i] = vsValue then
            cmbUniform.ItemIndex := i;
        end;

        vsValue := DataUn.dmDataModule.dsMotors.FieldByName('Led').AsString;
        for i := 0 to cmbLed.Items.Count - 1 do
        begin
          if cmbLed.Items[i] = vsValue then
            cmbLed.ItemIndex := i;
        end;

        vsValue := DataUn.dmDataModule.dsMotors.FieldByName('Maint').AsString;
        for i := 0 to cmbMaint.Items.Count - 1 do
        begin
          if cmbMaint.Items[i] = vsValue then
            cmbMaint.ItemIndex := i;
        end;

        vsValue := DataUn.dmDataModule.dsMotors.FieldByName('MtaInhibit').AsString;
        for i := 0 to cmbMTAInb.Items.Count - 1 do
        begin
          if cmbMTAInb.Items[i] = vsValue then
            cmbMTAInb.ItemIndex := i;
        end;

        txtRevision.Text := DataUn.dmDataModule.dsMotors.FieldByName('Revision').AsString;
        txtRevision.font.Color := clRed;

        vsValue := DataUn.dmDataModule.dsMotors.FieldByName('Port').AsString;
        for i := 0 to cmbPortMode.Items.Count - 1 do
        begin
          if cmbPortMode.Items[i] = vsValue then
            cmbPortMode.ItemIndex := i;
        end;

        vsValue := DataUn.dmDataModule.dsMotors.FieldByName('LvType').AsString;
        for i := 0 to cmbLVtype.Items.Count - 1 do
        begin
          if cmbLVtype.Items[i] = vsValue then
            cmbLVtype.ItemIndex := i;
        end;

        vsValue := DataUn.dmDataModule.dsMotors.FieldByName('Forward').AsString;
        for i := 0 to cmbFwd.Items.Count - 1 do
        begin
          if cmbFwd.Items[i] = vsValue then
            cmbFwd.ItemIndex := i;
        end;

        if not isIQMLC2 then
        begin
          vsValue := DataUn.dmDataModule.dsMotors.FieldByName('BoType').AsString;
          for i := 0 to cmbBOtype.Items.Count - 1 do
          begin
            if cmbBOtype.Items[i] = vsValue then
              cmbBOtype.ItemIndex := i;
          end;
        end;

        vsValue := IntToStr(DataUn.dmDataModule.dsMotors.FieldByName('AutoReturn').AsInteger);
        teAutoReturnIQ2.text:=vsValue;

        if not isIQMLC2 then
        begin
          vsValue := IntToStr(DataUn.dmDataModule.dsMotors.FieldByName('MasterUID').AsInteger);
          teMasterUIDIQ2.text:=vsValue;
          //btnGetStatus.Enabled := True;
          rhEnable;
        end;
      end;

      DataUn.dmDataModule.dsMotors.Filtered := False;
    end;
  except
    on E: Exception do
    begin
      rhEnable;
      MessageDlg('TConfigScreenFm.DisplayParameters: ' + E.Message, mtWarning, [mbOk], 0);
    end;
  end;
end;

end.
